<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8" %>
<html lang="en" data-textdirection="rtl" class="loading">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />

      <title>مدیریت پایانه های فروش</title>

    <link rel="shortcut icon" type="image/x-icon" href="app-assets/images/ico/favicon.ico">
    <link rel="shortcut icon" type="image/png" href="app-assets/images/ico/favicon-32.png">

    <link rel="stylesheet" type="text/css" href="app-assets/css-rtl/bootstrap.css">
    <!-- font icons-->
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/extensions/sweetalert.css">
    <link rel="stylesheet" type="text/css" href="app-assets/fonts/icomoon.css">
    <link rel="stylesheet" type="text/css" href="app-assets/fonts/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/tools.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/extensions/pace.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/jsgrid/jsgrid-theme.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/jsgrid/jsgrid.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/datatable/datatables.min.css">

    <!-- END VENDOR CSS-->
    <!-- BEGIN ROBUST CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css-rtl/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css-rtl/app.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css-rtl/colors.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css-rtl/custom-rtl.css">
    <link rel="stylesheet" type="text/css" href="app-assets/ui-jquery.css">

    <link rel="stylesheet" type="text/css" href="app-assets/vendors/js/datepicker/bootstrap-datepicker.min.css">



    <!-- END ROBUST CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css-rtl/core/menu/menu-types/vertical-menu.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css-rtl/core/menu/menu-types/vertical-overlay-menu.css">
    <!-- END Page Level CSS-->
    <!-- BEGIN Custom CSS-->
    <link rel="stylesheet" type="text/css" href="assets/css/style-rtl.css">

    <script>var ctx = "${pageContext.request.contextPath}"</script>
    <!-- END Custom CSS-->
    <style>
      th,td,tr{
        font-size: 11px;
        padding: 0;!important;
      }
      .databletd{
        font-size: 11px;
        padding: 0;
        text-align: center;
        vertical-align: middle
      }
    </style>
    <script src="app-assets/js/scripts/jquery-1.11.1.min.js"></script>

    <script src="app-assets/vendors/js/datepicker/bootstrap-datepicker.js" ></script>
    <script src="app-assets/vendors/js/datepicker/bootstrap-datepicker.fa.js" ></script>

    <script>


        $(document).ready(function() {

            $("#requestDateFrom").datepicker({
                       isRTL: true,
                       dateFormat: "yy/mm/dd"
             });   $("#requestDateTo").datepicker({
                       isRTL: true,
                       dateFormat: "yy/mm/dd"
             });
         });

    </script>
  </head>
  <body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="vertical-layout vertical-menu 2-columns  fixed-navbar">

    <!-- navbar-fixed-top-->
    <jsp:directive.include file="inc/topmenu.jsp"/>

    <div class="fullscreen-search-overlay"></div>

    <!-- ////////////////////////////////////////////////////////////////////////////-->


    <!-- main menu-->
    <div data-scroll-to-active="true" class="main-menu menu-fixed menu-dark menu-accordion menu-shadow">
      <!-- main menu header-->
      <!-- / main menu header-->
      <!-- main menu content-->

      <!--Menu -->
      <jsp:directive.include file="inc/menu.jsp"/>
      <!--Menu -->

      <!-- /main menu content-->
    </div>
    <!-- / main menu-->

    <div class="app-content content container-fluid">
      <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
          <!-- Sales stats -->


          <section id="basic">

            <div class="row">
              <div class="col-xs-12">
                <div class="card">
                  <div class="card-header">
                    <h4 class="card-title">گزارش پایانه های فروش</h4>
                    <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                    <div class="heading-elements">
                      <ul class="list-inline mb-0">
                        <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                      </ul>
                    </div>
                  </div>
                  <div class="card-body collapse in">
                    <div class="card-block card-dashboard ">
                      <div class="row" style="line-height: 9px">
                      <div class="col-md-3">
                        <div class="form-group">
                          <label >
                            تاریخ درخواست از :
                          </label>
                          <input type="text" class="form-control" id="requestDateFrom" name="" >

                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label >
                           تاریخ درخواست تا :
                          </label>
                          <input type="text" class="form-control" id="requestDateTo" name="" >

                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label >
                            شماره مشتری :
                          </label>
                          <input type="text" class="form-control" id="clientNumber" name="" >

                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label >
                            كدپیگیری شركت :
                          </label>
                          <input type="text" class="form-control" id="pspTrackingCode" name="" >

                        </div>
                      </div>
                      </div>
                    <div class="row" style="line-height: 9px">
                      <div class="col-md-3">
                        <div class="form-group">
                          <label >
                           شماره حساب :
                          </label>
                          <input type="text" class="form-control" id="accountNo" name="" >

                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label >
                           شماره درخواست :
                          </label>
                          <input type="text" class="form-control" id="requestNumber" name="" >

                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label >
                            شماره پایانه :
                          </label>
                          <input type="text" class="form-control" id="pspTerminalCode" name="" >

                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label >
                            شماره پذیرنده :
                          </label>
                          <input type="text" class="form-control" id="pspAcceptanceCode" name="" >

                        </div>
                      </div>
                      </div>
                    <div class="row" style="line-height: 9px">
                      <div class="col-md-3">
                        <div class="form-group">
                          <label >
                           نام شركت :
                          </label>
                          <select class="form-control" id="listcompany_select">
                            <option value="">--انتخاب كنید--</option>

                          </select>

                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label >
                           كد شعبه :
                          </label>
                          <select class="form-control" id="branchId">
                            <option value="">--انتخاب كنید--</option>

                          </select>

                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label >
                            وضعیت درخواست :
                          </label>
                            <select class="form-control" id="requestStatusId">
                              <option value="">--انتخاب كنید--</option>
                              <option value="1">ارسال نشده</option>
                              <option value="2">ثبت شده</option>
                              <option value="3">رد شده</option>
                              <option value="4">نامعلوم</option>
                              <option value="5">تایید شده</option>
                            </select>
                        </div>
                      </div>
                      <div class="col-md-1"  style="margin-top: 15px">
                        <button class="btn btn-success" id="submit_report">جستجو</button>
                      </div>
                      <div class="col-md-2" style="margin-top: 15px">
                        <button class="btn btn-warning" id="claerform_report">پاكسازی فرم</button>

                      </div>
                      </div>

                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-xs-12">
                <div class="card">
                  <div class="card-header">
                    <h4 class="card-title">گزارش پایانه های فروش</h4>
                    <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                    <div class="heading-elements">
                      <ul class="list-inline mb-0">
                        <li>
                          <button class="btn btn-accent-2" id="submit_excel">گزارش اكسل<span class="icon-file-excel"></span></button>
                       </li>
                        <li><a data-action="expand"><i class="icon-expand2"></i></a></li>

                      </ul>
                    </div>
                  </div>

                  <div class="card-body collapse in">
                    <div class="card-block card-dashboard ">
                        <table  class="table table-striped table-bordered" id="reporttable">
                          <%--zero-configuration--%>
                          <thead class="thead-inverse" >
                          <tr >
                            <th style="text-align: center;padding: 10px;vertical-align: middle">تاریخ</th>
                            <th style="text-align: center;padding: 10px;vertical-align: middle">شماره حساب</th>
                            <th style="text-align: center;padding: 10px;vertical-align: middle">شماره مشتری</th>
                            <th style="text-align: center;padding: 10px;vertical-align: middle">شعبه</th>
                            <th style="text-align: center;padding: 10px;vertical-align: middle">نام شركت</th>
                            <th style="text-align: center;padding: 10px;vertical-align: middle">وضعیت درخواست</th>
                            <th style="text-align: center;padding: 10px;vertical-align: middle">شماره درخواست</th>
                            <th style="text-align: center;padding: 10px;vertical-align: middle">كدپیگیری شركت</th>
                            <th style="text-align: center;padding: 10px;vertical-align: middle">شماره پذیرنده</th>
                            <th style="text-align: center;padding: 10px;vertical-align: middle">شماره پایانه</th>
                            <th style="text-align: center">عملیات</th>
                          </tr>
                          </thead>
                          <tbody id="listrow">
<td>1</td>
<td>1</td>
<td>1</td>
<td>1</td>
<td>1</td>
<td>1</td>
<td>1</td>
<td>1</td>
<td>1</td>
<td>1</td>
<td>1</td>

                          </tbody>
                        </table>


                    </div>
                  </div>

                </div>
              </div>
            </div>


          </section>


        </div>
      </div>
    </div>
    <!-- ////////////////////////////////////////////////////////////////////////////-->
    <!-- BEGIN VENDOR JS-->
    <%--<script src="app-assets/js/core/libraries/jquery.js" type="text/javascript"></script>--%>
    <script src="app-assets/vendors/js/ui/tether.min.js" type="text/javascript"></script>
    <script src="app-assets/js/core/libraries/bootstrap.min.js" type="text/javascript"></script>
    <script src="app-assets/vendors/js/ui/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
    <script src="app-assets/vendors/js/ui/unison.min.js" type="text/javascript"></script>
    <script src="app-assets/vendors/js/ui/blockUI.min.js" type="text/javascript"></script>
    <script src="app-assets/vendors/js/ui/jquery.matchHeight-min.js" type="text/javascript"></script>
    <script src="app-assets/vendors/js/ui/jquery-sliding-menu.js" type="text/javascript"></script>
    <script src="app-assets/vendors/js/sliders/slick/slick.min.js" type="text/javascript"></script>
    <script src="app-assets/vendors/js/ui/screenfull.min.js" type="text/javascript"></script>
    <script src="app-assets/vendors/js/extensions/pace.min.js" type="text/javascript"></script>
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <script src="app-assets/vendors/js/modal/sweetalert.min.js" type="text/javascript"></script>
    <script src="app-assets/vendors/js/extensions/sweetalert.min.js" type="text/javascript"></script>
    <script src="app-assets/js/scripts/extensions/sweet-alerts.js" type="text/javascript"></script>
    <script src="app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="app-assets/vendors/js/tables/jsgrid/jsgrid.min.js" type="text/javascript"></script>
    <script src="app-assets/vendors/js/tables/jsgrid/jquery.validate.min.js" type="text/javascript"></script>
    <!-- END PAGE VENDOR JS-->
    <script src="app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="app-assets/js/core/libraries/jquery_ui/jquery-ui-jalali-datepicker.js" type="text/javascript"></script>
    <script src="app-assets/js/scripts/customizer.js" type="text/javascript"></script>


    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <!-- BEGIN ROBUST JS-->
    <script src="app-assets/js/core/app-menu.js" type="text/javascript"></script>
    <script src="app-assets/js/core/app.js" type="text/javascript"></script>
    <script src="app-assets/js/scripts/tools.js" type="text/javascript"></script>
    <!-- END ROBUST JS-->
    <script type="text/javascript" src="app-assets/js/scripts/ui/breadcrumbs-with-stats.js"></script>
    <script src="app-assets/js/scripts/sessions/jquery.session.js" type="text/javascript"></script>

    <script src="app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
    <script src="app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js" type="text/javascript"></script>
    <script src="app-assets/vendors/js/tables/datatable/buttons.bootstrap4.min.js" type="text/javascript"></script>
    <script src="app-assets/vendors/js/tables/jszip.min.js" type="text/javascript"></script>
    <script src="app-assets/vendors/js/tables/pdfmake.min.js" type="text/javascript"></script>
    <script src="app-assets/vendors/js/tables/vfs_fonts.js" type="text/javascript"></script>
    <script src="app-assets/vendors/js/tables/buttons.html5.min.js" type="text/javascript"></script>
    <script src="app-assets/vendors/js/tables/buttons.print.min.js" type="text/javascript"></script>
    <script src="app-assets/vendors/js/tables/buttons.colVis.min.js" type="text/javascript"></script>
    <script src="app-assets/vendors/js/tables/jsgrid/report.js" type="text/javascript"></script>

    <script type="text/javascript">





        var obj = localStorage.getItem("tablereport")


        if (obj == 100 || !obj || obj == "undefined") {



        }else{

            var obj = JSON.parse(localStorage.getItem("tablereport"))
            i=1
            while(i <= obj.length){
                // var trHTML_account_info_table = '<tr class="tr_remove_account_info">' +
                //     '<td style="font-size: 11px;padding: 0;text-align: center;vertical-align: middle">' + obj[i-1].requestDate + '</td>' +
                //     '<td style="font-size: 11px;padding: 0;text-align: center;vertical-align: middle">' + obj[i-1].accountNo + '</td>' +
                //     '<td style="font-size: 11px;padding: 0;text-align: center;vertical-align: middle">' + obj[i-1].clientNumber + '</td>' +
                //     '<td style="font-size: 11px;padding: 0;text-align: center;vertical-align: middle">' + obj[i-1].branch + '</td>' +
                //     '<td style="font-size: 11px;padding: 0;text-align: center;vertical-align: middle">' + obj[i-1].companyName + '</td>' +
                //     '<td style="font-size: 11px;padding: 0;text-align: center;vertical-align: middle">' + obj[i-1].requestStatus + '</td>' +
                //     '<td style="font-size: 11px;padding: 0;text-align: center;vertical-align: middle">' + obj[i-1].requestNumber + '</td>' +
                //     '<td style="font-size: 11px;padding: 0;text-align: center;vertical-align: middle">' + obj[i-1].pspTrackingCode + '</td>' +
                //     '<td style="font-size: 11px;padding: 0;text-align: center;vertical-align: middle">' + obj[i-1].pspAcceptanceCode + '</td>' +
                //     '<td style="font-size: 11px;padding: 0;text-align: center;vertical-align: middle">' + obj[i-1].pspTerminalCode + '</td>' +
                //     '<td><button class="btn btn-brown" onclick="report_detail('+obj[i-1].pspRequestId+')">  جزییات</button> </td>' +
                //     '<tr>';


                var trHTML_account_info_table = '<tr class="tr_remove_account_info">' +
                    '<td style="font-size: 11px;padding: 0;text-align: center;vertical-align: middle">1</td>' +
                    '<td style="font-size: 11px;padding: 0;text-align: center;vertical-align: middle">1</td>' +
                    '<td style="font-size: 11px;padding: 0;text-align: center;vertical-align: middle">1</td>' +
                    '<td style="font-size: 11px;padding: 0;text-align: center;vertical-align: middle">1</td>' +
                    '<td style="font-size: 11px;padding: 0;text-align: center;vertical-align: middle">1</td>' +
                    '<td style="font-size: 11px;padding: 0;text-align: center;vertical-align: middle">1</td>' +
                    '<td style="font-size: 11px;padding: 0;text-align: center;vertical-align: middle">1</td>' +
                    '<td style="font-size: 11px;padding: 0;text-align: center;vertical-align: middle">1</td>' +
                    '<td style="font-size: 11px;padding: 0;text-align: center;vertical-align: middle">1</td>' +
                    '<td style="font-size: 11px;padding: 0;text-align: center;vertical-align: middle">1</td>' +
                    '<td style="font-size: 11px;padding: 0;text-align: center;vertical-align: middle">1</td>' +

                    '<tr>';
                $('#listrow').append(trHTML_account_info_table);
                i++;
            }

        }







            // rowtable=JSON.parse(localStorage.getItem("tablereport"))









  </script>
  </body>
</html>
