<%--
  Created by IntelliJ IDEA.
  User: r.rahiminia
  Date: 7/15/2018
  Time: 6:14 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" type="text/css" href="../../app-assets/css-rtl/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../../app-assets/vendors/js/datepicker/bootstrap-datepicker.min.css">
</head>
<body>



<input id="customer_no" type="text">


<script
        src="https://code.jquery.com/jquery-3.3.1.js"
        integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
        crossorigin="anonymous"></script>
<script src="../../app-assets/vendors/js/datepicker/bootstrap-datepicker.js" ></script>
<script src="../../app-assets/vendors/js/datepicker/bootstrap-datepicker.fa.js" ></script>


<script src="../../app-assets/vendors/js/ui/tether.min.js" type="text/javascript"></script>
<script src="../../app-assets/js/core/libraries/bootstrap.min.js" type="text/javascript"></script>
<script src="../../app-assets/vendors/js/ui/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
<script src="../../app-assets/vendors/js/ui/unison.min.js" type="text/javascript"></script>
<script src="../../app-assets/vendors/js/ui/blockUI.min.js" type="text/javascript"></script>
<script src="../../app-assets/vendors/js/ui/jquery.matchHeight-min.js" type="text/javascript"></script>
<script src="../../app-assets/vendors/js/ui/jquery-sliding-menu.js" type="text/javascript"></script>
<script src="../../app-assets/vendors/js/sliders/slick/slick.min.js" type="text/javascript"></script>
<script src="../../app-assets/vendors/js/ui/screenfull.min.js" type="text/javascript"></script>
<script src="../../app-assets/vendors/js/extensions/pace.min.js" type="text/javascript"></script>
<script src="../../app-assets/vendors/js/forms/extended/inputmask/jquery.inputmask.bundle.min.js" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
<script src="../../app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>




<script src="../../app-assets/js/core/app-menu.js" type="text/javascript"></script>
<script src="../../app-assets/js/core/app.js" type="text/javascript"></script>
<script src="../../app-assets/js/scripts/forms/extended/form-inputmask.js" type="text/javascript"></script>
<script src="../../app-assets/vendors/js/extensions/sweetalert.min.js" type="text/javascript"></script>
<script src="../../app-assets/vendors/js/modal/sweetalert.min.js" type="text/javascript"></script>
<script src="../../app-assets/vendors/js/tables/jsgrid/RegisterTerminal.js" type="text/javascript"></script>
<script src="../../app-assets/js/scripts/sessions/jquery.session.js" type="text/javascript"></script>
<script src="../../app-assets/js/scripts/tools.js" type="text/javascript"></script>

<script>

    $(document).ready(function() {

        $("#customer_no").datepicker({
                                         isRTL: true,

                                         dateFormat: "yy/mm/dd"
                                     });

    });
</script>
</body>
</html>
