<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8" %>
<html lang="en" data-textdirection="rtl" class="loading">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>مدیریت پایانه های فروش</title>

    <link rel="shortcut icon" type="image/x-icon" href="app-assets/images/ico/favicon.ico">
    <link rel="shortcut icon" type="image/png" href="app-assets/images/ico/favicon-32.png">

    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css-rtl/bootstrap.css">
    <!-- font icons-->
    <link rel="stylesheet" type="text/css" href="app-assets/fonts/icomoon.css">
    <link rel="stylesheet" type="text/css" href="app-assets/fonts/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/tools.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/extensions/pace.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/jsgrid/jsgrid-theme.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/jsgrid/jsgrid.min.css">
    <!-- END VENDOR CSS-->
    <!-- BEGIN ROBUST CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css-rtl/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css-rtl/app.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css-rtl/colors.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css-rtl/custom-rtl.css">

    <!-- END ROBUST CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css-rtl/core/menu/menu-types/vertical-menu.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css-rtl/core/menu/menu-types/vertical-overlay-menu.css">
    <!-- END Page Level CSS-->
    <!-- BEGIN Custom CSS-->
    <link rel="stylesheet" type="text/css" href="assets/css/style-rtl.css">
    <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
    <script type="text/javascript" src="app-assets/libs/FileSaver/FileSaver.min.js"></script>
    <script type="text/javascript" src="app-assets/libs/jsPDF/jspdf.min.js"></script>
    <script type="text/javascript" src="app-assets/libs/jsPDF-AutoTable/jspdf.plugin.autotable.js"></script>
    <script type="text/javascript" src="app-assets/libs/html2canvas/html2canvas.min.js"></script>

    <!-- END Custom CSS-->
    <script type="text/javaScript">

        function doExport(selector, params) {
            var options = {
                //ignoreRow: [1,11,12,-2],
                //ignoreColumn: [0,-1],
                tableName: 'Countries',
                worksheetName: 'Countries by population'
            };

            $.extend(true, options, params);

            $(selector).tableExport(options);
        }

        function DoOnCellHtmlData(cell, row, col, data) {
            var result = "";
            if (data != "") {
                var html = $.parseHTML( data )

                $.each( html, function() {
                    if ( typeof $(this).html() === 'undefined' )
                        result += $(this).text();
                    else if ( $(this).is("input") )
                        result += $('#'+$(this).attr('id')).val();
                    else if ( ! $(this).hasClass('no_export') )
                        result += $(this).html();
                });
            }
            return result;
        }
    </script>
    <style>
      th{
        font-size: 11px;
      }
    </style>
  </head>
  <body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="vertical-layout vertical-menu 2-columns  fixed-navbar">

    <!-- navbar-fixed-top-->
    <jsp:directive.include file="inc/topmenu.jsp"/>

    <div class="fullscreen-search-overlay"></div>

    <!-- ////////////////////////////////////////////////////////////////////////////-->


    <!-- main menu-->
    <div data-scroll-to-active="true" class="main-menu menu-fixed menu-dark menu-accordion menu-shadow">
      <!-- main menu header-->
      <!-- / main menu header-->
      <!-- main menu content-->

      <!--Menu -->
      <jsp:directive.include file="inc/menu.jsp"/>
      <!--Menu -->

      <!-- /main menu content-->
    </div>
    <!-- / main menu-->

    <div class="app-content content container-fluid">
      <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
          <!-- Sales stats -->


          <section id="basic">


            <div class="row">
              <div class="col-xs-12">
                <div class="card">
                  <div class="card-header">
                    <h4 class="card-title">جزییات گزارش پایانه های فروش</h4>
                    <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                    <div class="heading-elements">
                      <ul class="list-inline mb-0">
                        <li>
                          <button class="btn btn-accent-2" id="export_ex">گزارش اكسل<span class="icon-file-excel"></span></button>

                        </li>
                        <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                      </ul>
                    </div>
                  </div>
                  <div class="card-body collapse in">
                    <div class="card-block card-dashboard ">
                      <div class="table-responsive" style="padding: 10px;" id="excelstyles">
                        <table class="table table-bordered">
                          <thead class="thead-inverse" >

                          <tr >

                            <th style="text-align: center;padding: 10px;vertical-align: middle">تاریخ</th>
                            <th style="text-align: center;padding: 10px;vertical-align: middle">زمان</th>
                            <th style="text-align: center;padding: 10px;vertical-align: middle">كد شعبه</th>
                            <th style="text-align: center;padding: 10px;vertical-align: middle">كد كاربر</th>
                            <th style="text-align: center;padding: 10px;vertical-align: middle">وضیعت درخواست</th>
                            <th style="text-align: center;padding: 10px;vertical-align: middle"> شماره درخواست </th>
                            <th style="text-align: center;padding: 10px;vertical-align: middle"> كد پیگیری </th>
                            <th style="text-align: center;padding: 10px;vertical-align: middle"> نوع رخداد </th>
                            <th style="text-align: center;padding: 10px;vertical-align: middle"> وضعیت </th>
                            <th style="text-align: center;padding: 10px;vertical-align: middle">توضیحات</th>

                          </tr>
                          </thead>
                          <tbody id="account_info_table">


                          </tbody>
                        </table>
                      </div>


                    </div>
                  </div>
                </div>
              </div>
            </div>


          </section>

        </div>
      </div>
    </div>
    <!-- ////////////////////////////////////////////////////////////////////////////-->



    <!-- BEGIN VENDOR JS-->
    <script src="app-assets/js/core/libraries/jquery.js" type="text/javascript"></script>
    <script src="app-assets/vendors/js/ui/tether.min.js" type="text/javascript"></script>
    <script src="app-assets/js/core/libraries/bootstrap.min.js" type="text/javascript"></script>
    <script src="app-assets/vendors/js/ui/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
    <script src="app-assets/vendors/js/ui/unison.min.js" type="text/javascript"></script>
    <script src="app-assets/vendors/js/ui/blockUI.min.js" type="text/javascript"></script>
    <script src="app-assets/vendors/js/ui/jquery.matchHeight-min.js" type="text/javascript"></script>
    <script src="app-assets/vendors/js/ui/jquery-sliding-menu.js" type="text/javascript"></script>
    <script src="app-assets/vendors/js/sliders/slick/slick.min.js" type="text/javascript"></script>
    <script src="app-assets/vendors/js/ui/screenfull.min.js" type="text/javascript"></script>
    <script src="app-assets/vendors/js/extensions/pace.min.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/extensions/sweetalert.css">

    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <script src="app-assets/vendors/js/modal/sweetalert.min.js" type="text/javascript"></script>
    <script src="app-assets/vendors/js/extensions/sweetalert.min.js" type="text/javascript"></script>
    <script src="app-assets/js/scripts/extensions/sweet-alerts.js" type="text/javascript"></script>
    <script src="app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="app-assets/vendors/js/tables/jsgrid/jsgrid.min.js" type="text/javascript"></script>
    <script src="app-assets/vendors/js/tables/jsgrid/jquery.validate.min.js" type="text/javascript"></script>
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN ROBUST JS-->
    <script src="app-assets/js/core/app-menu.js" type="text/javascript"></script>
    <script src="app-assets/js/core/app.js" type="text/javascript"></script>
    <script src="app-assets/js/scripts/tools.js" type="text/javascript"></script>

    <!-- END ROBUST JS-->


    <script src="app-assets/js/scripts/tools.js" type="text/javascript"></script>
    <script type="text/javascript" src="app-assets/js/scripts/ui/breadcrumbs-with-stats.js"></script>
    <script src="app-assets/vendors/js/tables/jsgrid/report_de.js" type="text/javascript"></script>
    <script src="app-assets/js/scripts/sessions/jquery.session.js" type="text/javascript"></script>
    <script type="text/javascript" src="app-assets/libs/tableExport.js"></script>

    <script type="text/javascript">

    </script>
  </body>
</html>
