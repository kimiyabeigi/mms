<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="secu" uri="http://java.sun.com/jsp/jstl/core" %>

<nav class="header-navbar navbar navbar-with-menu navbar-fixed-top navbar-semi-dark navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-header">
            <ul class="nav navbar-nav">
                <li class="nav-item mobile-menu hidden-md-up float-xs-left"><a class="nav-link nav-menu-main menu-toggle hidden-xs"><i class="icon-menu5 font-large-1"></i></a></li>
                <li class="nav-item"><a href="#" class="navbar-brand nav-link">
                    <img   width="40px" style="margin-top: -8px" data-expand="app-assets/images/logo/logo_l.png" data-collapse="app-assets/images/logo/logo_l.png" class="brand-logo">
                    <img src="app-assets/images/logo/logo_t.png" id="hidemenu12" width="80px" style="margin-top: -7px">
                </a></li>
                <li class="nav-item hidden-md-up float-xs-right"><a data-toggle="collapse" data-target="#navbar-mobile" class="nav-link open-navbar-container"><i class="icon-ellipsis pe-2x icon-icon-rotate-right-right"></i></a></li>
            </ul>
        </div>
        <div class="navbar-container content container-fluid">
            <div id="navbar-mobile" class="collapse navbar-toggleable-sm">
                <ul class="nav navbar-nav">
                    <li class="nav-item hidden-sm-down"><a class="nav-link nav-menu-main menu-toggle hidden-xs"><i class="icon-menu5"></i></a></li>
                    <li class="nav-item hidden-sm-down"><a href="#" class="nav-link nav-link-expand"><i class="ficon icon-expand2"></i></a></li>
                </ul>
                <ul class="nav navbar-nav float-xs-right">
                    <li class="dropdown dropdown-user nav-item"><a href="#" data-toggle="dropdown" class="dropdown-toggle nav-link dropdown-user-link"><span class="avatar avatar-online"><i></i></span><span  id="profile_user"> </span></a>
                        <%--class="user-name"--%>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a href="${pageContext.request.contextPath}/changepassword" class="dropdown-item"><i class="icon-head"></i> تغییر كلمه عبور</a>
                            <%--<a href="#" class="dropdown-item"><i class="icon-mail6"></i> My Inbox</a>--%>
                            <%--<a href="#" class="dropdown-item"><i class="icon-clipboard2"></i> Task</a>--%>
                            <%--<a href="#" class="dropdown-item"><i class="icon-calendar5"></i> Calender</a>--%>
                            <div class="dropdown-divider"></div><a href="<secu:url value="/logout" />" class="dropdown-item"><i class="icon-power3"></i> خروج</a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>

