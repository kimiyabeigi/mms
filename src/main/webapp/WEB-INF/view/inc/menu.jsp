<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<div>

</div>
<div class="main-menu-content">
    <ul id="main-menu-navigation" data-menu="menu-navigation" class="navigation navigation-main">
        <li class=" nav-item">
            <a href="">
                <i class="icon-home3"></i><span data-i18n="nav.dash.main" class="menu-title">صفحه اصلی</span>
            </a>
        </li>

        <sec:authorize access="@permissionService.hasPermission('api/base/company', 'GET') ||
                               @permissionService.hasPermission('api/sys/service', 'GET') ||
                               @permissionService.hasPermission('api/base/index', 'GET') ||
                               @permissionService.hasPermission('api/base/terminalDetail', 'GET') ||
                               @permissionService.hasPermission('api/base/city', 'GET') ||
                               @permissionService.hasPermission('api/base/guild', 'GET')">
            <li class=" nav-item"><a href="#"><i class="icon-document"></i><span data-i18n="nav.dash.main"
                                                                                 class="menu-title">اطلاعات پایه</span></a>
                <ul class="menu-content">

                    <sec:authorize access="@permissionService.hasPermission('api/base/company', 'GET')">
                        <li><a href="${pageContext.request.contextPath}/listcompany" data-i18n="nav.dash.ecommerce"
                               class="menu-item">تعریف شرکت</a>
                        </li>
                    </sec:authorize>
                    <sec:authorize access="@permissionService.hasPermission('api/sys/service', 'GET')">
                        <li><a href="${pageContext.request.contextPath}/listservice" data-i18n="nav.dash.project"
                               class="menu-item">تعریف خدمات</a>
                        </li>
                    </sec:authorize>
                    <sec:authorize access="@permissionService.hasPermission('api/base/index', 'GET')">
                        <li><a href="${pageContext.request.contextPath}/listfactor" data-i18n="nav.dash.analytics"
                               class="menu-item">تعریف شاخص</a>
                        </li>
                    </sec:authorize>
                    <sec:authorize access="@permissionService.hasPermission('api/base/terminalDetail', 'GET')">
                        <li><a href="${pageContext.request.contextPath}/listterminal" data-i18n="nav.dash.crm"
                               class="menu-item">تعریف انواع پایانه فروش</a>
                        </li>
                    </sec:authorize>
                    <sec:authorize access="@permissionService.hasPermission('api/base/city', 'GET')">
                        <li><a href="${pageContext.request.contextPath}/liststate" data-i18n="nav.dash.fitness"
                               class="menu-item">تعریف کد استان و شهر</a>
                        </li>
                    </sec:authorize>
                    <sec:authorize access="@permissionService.hasPermission('api/base/guild', 'GET')">
                        <li><a href="${pageContext.request.contextPath}/listguild" data-i18n="nav.dash.fitness"
                               class="menu-item">تعریف کد اصناف</a>
                        </li>
                    </sec:authorize>
                </ul>
            </li>
        </sec:authorize>

        <sec:authorize access="@permissionService.hasPermission('api/company/setting/view', 'GET')">
            <li class=" nav-item"><a href="#"><i class="icon-office"></i><span data-i18n="nav.dash.main"
                                                                               class="menu-title">تنظیمات </span></a>
                <ul class="menu-content">
                    <sec:authorize access="@permissionService.hasPermission('api/company/setting/view', 'GET')">
                        <li><a href="${pageContext.request.contextPath}/managecompany" data-i18n="nav.dash.ecommerce"
                               class="menu-item">تنظیمات شركت</a></li>
                    </sec:authorize>
                </ul>
            </li>
        </sec:authorize>

        <sec:authorize access="@permissionService.hasPermission('api/base/company/active', 'GET')">
            <li class=" nav-item">
                <a href="${pageContext.request.contextPath}/registerterminal">
                    <i class="icon-paper"></i>
                    <span data-i18n="nav.dash.main" class="menu-title">ثبت درخواست</span>
                </a>
            </li>
        </sec:authorize>

        <sec:authorize access="@permissionService.hasPermission('api/psp/trace/case/unknown', 'PUT') ||
                               @permissionService.hasPermission('api/psp/trace/batch/unknown', 'GET') ||
                               @permissionService.hasPermission('api/psp/trace/batch/accept', 'GET') ||
                               @permissionService.hasPermission('api/psp/trace/batch/register', 'GET')">
            <li class=" nav-item"><a href="#"><i class="icon-search"></i><span data-i18n="nav.dash.main"
                                                                               class="menu-title">پیگیری</span></a>
                <ul class="menu-content">
                    <sec:authorize access="@permissionService.hasPermission('api/psp/trace/case/unknown', 'PUT')">
                        <li><a href="${pageContext.request.contextPath}/moredipeygiri" data-i18n="nav.dash.ecommerce"
                               class="menu-item">پیگیری وضعیت</a>
                        </li>
                    </sec:authorize>
                    <li><a href="" data-i18n="nav.dash.ecommerce" class="menu-item">پیگیری دسته ای</a>
                        <ul class="menu-content">
                            <sec:authorize
                                    access="@permissionService.hasPermission('api/psp/trace/batch/unknown', 'GET')">
                                <li><a href="${pageContext.request.contextPath}/searchtrace"
                                       data-i18n="nav.dash.ecommerce"
                                       class="menu-item">درخواست های نامعلوم</a>
                                </li>
                            </sec:authorize>
                            <sec:authorize
                                    access="@permissionService.hasPermission('api/psp/trace/batch/accept', 'GET')">
                                <li><a href="${pageContext.request.contextPath}/traceaccept"
                                       data-i18n="nav.dash.ecommerce"
                                       class="menu-item">پیگیری وضعیت درخواست</a>
                                </li>
                            </sec:authorize>
                            <sec:authorize
                                    access="@permissionService.hasPermission('api/psp/trace/batch/register', 'GET')">
                                <li><a href="${pageContext.request.contextPath}/traceregister"
                                       data-i18n="nav.dash.ecommerce" class="menu-item">دریافت شماره پذیرنده و
                                    پایانه</a>
                                </li>
                            </sec:authorize>
                        </ul>
                    </li>
                </ul>
            </li>
        </sec:authorize>

        <sec:authorize access="@permissionService.hasPermission('api/psp/register/report/master', 'POST')">
            <li class=" nav-item"><a href="${pageContext.request.contextPath}/report"><i class="icon-list"></i><span
                    data-i18n="nav.dash.main" class="menu-title">گزراش پایانه فروش</span></a>
            </li>
        </sec:authorize>

        <sec:authorize access="@permissionService.hasPermission('api/user', 'GET')">
            <li class=" nav-item"><a href="${pageContext.request.contextPath}/usermanager"><i
                    class="icon-user"></i><span data-i18n="nav.dash.main" class="menu-title">مدیریت كاربران</span></a>
            </li>
        </sec:authorize>

        <sec:authorize access="@permissionService.hasPermission('api/role', 'GET')">
            <li class=" nav-item"><a href="${pageContext.request.contextPath}/rolemanage"><i
                    class="icon-users"></i><span data-i18n="nav.dash.main" class="menu-title">مدیریت نقش</span></a></li>
        </sec:authorize>
        <%--<li class=" nav-item"><a href="/userrole"><i class="icon-list"></i><span data-i18n="nav.dash.main" class="menu-title">دسترسی كاربران</span></a></li>--%>
    </ul>
</div>
