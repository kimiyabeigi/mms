<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8" %>
<html lang="en" data-textdirection="rtl" class="loading">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>مدیریت پایانه های فروش</title>

  <link rel="shortcut icon" type="image/x-icon" href="app-assets/images/ico/favicon.ico">
  <link rel="shortcut icon" type="image/png" href="app-assets/images/ico/favicon-32.png">

  <!-- BEGIN VENDOR CSS-->
  <link rel="stylesheet" type="text/css" href="app-assets/css-rtl/bootstrap.css">
    <!-- font icons-->
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/extensions/sweetalert.css">
    <link rel="stylesheet" type="text/css" href="app-assets/fonts/icomoon.css">
    <link rel="stylesheet" type="text/css" href="app-assets/fonts/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/tools.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/extensions/pace.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/jsgrid/jsgrid-theme.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/jsgrid/jsgrid.min.css">
    <!-- END VENDOR CSS-->
    <!-- BEGIN ROBUST CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css-rtl/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css-rtl/app.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css-rtl/colors.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css-rtl/custom-rtl.css">
    <link rel="stylesheet" type="text/css" href="app-assets/ui-jquery.css">

    <link rel="stylesheet" type="text/css" href="app-assets/vendors/js/datepicker/bootstrap-datepicker.min.css">



    <!-- END ROBUST CSS-->
  <!-- BEGIN Page Level CSS-->
  <link rel="stylesheet" type="text/css" href="app-assets/css-rtl/core/menu/menu-types/vertical-menu.css">
  <link rel="stylesheet" type="text/css" href="app-assets/css-rtl/core/menu/menu-types/vertical-overlay-menu.css">
  <!-- END Page Level CSS-->
  <!-- BEGIN Custom CSS-->
  <link rel="stylesheet" type="text/css" href="assets/css/style-rtl.css">

  <!-- END Custom CSS-->
  <style>
    fieldset {
      padding-top:10px;
      border:1px solid #666;
      border-radius:8px;
    }
    legend {
      float:left;
      margin-top:-20px;
    }
    legend + * {
      clear:both;
    }

    .spinner {
        width: 40px;
        height: 40px;
        margin: 100px auto;
        background-color: #333;

        border-radius: 100%;
        -webkit-animation: sk-scaleout 1.0s infinite ease-in-out;
        animation: sk-scaleout 1.0s infinite ease-in-out;
        margin-right: 50%;
        z-index: 99;
    }

    @-webkit-keyframes sk-scaleout {
        0% { -webkit-transform: scale(0) }
        100% {
            -webkit-transform: scale(1.0);
            opacity: 0;
        }
    }

    @keyframes sk-scaleout {
        0% {
            -webkit-transform: scale(0);
            transform: scale(0);
        } 100% {
              -webkit-transform: scale(1.0);
              transform: scale(1.0);
              opacity: 0;
          }
    }
    .container_blur {
        width: 100%;
        height: 100%;
        overflow: hidden;
        -webkit-filter: blur(15px);
        -moz-filter: blur(15px);
        -o-filter: blur(15px);
        -ms-filter: blur(15px);
        filter: blur(15px);

    }
    .custom-combobox {
        position: relative;
        display: inline-block;

    }
    .custom-combobox-toggle {
        position: absolute;
        top: 0;
        bottom: 0;
        margin-left: -1px;
        padding: 0;
    }
    .custom-combobox-input {
        margin: 0;
        padding: 5px 10px;
    }
    .ui-draggable, .ui-droppable {
        background-position: top;
    }
      td{
          font-size: 13px;!important;
      }
    .ui-autocomplete-list {
        max-height: 300px;
        overflow-y: auto;
        overflow-x: hidden;
        z-index:1000 !important;
    }
    .ui-autocomplete {
        max-height: 300px;

        overflow-y: auto;
        overflow-x: hidden;
        z-index:1000 !important;
    }
    .ui-autocomplete.ui-front {
        max-height: 200px;

        overflow-y: auto;
        overflow-x: hidden;
        z-index:1000 !important;
    }
  </style>
    <script src="app-assets/js/scripts/jquery-1.11.1.min.js"></script>

    <%--<script src="app-assets/vendors/js/datepicker/bootstrap-datepicker.js" ></script>--%>
    <%--<script src="app-assets/vendors/js/datepicker/bootstrap-datepicker.fa.js" ></script>--%>

    <script>


        $(document).ready(function() {
            $("#LicenseIssuanceDate").datepicker({
                      isRTL: true,
                        dateFormat: "yy/mm/dd"
             });  $("#LicenseValidityDate").datepicker({
                      isRTL: true,
                        dateFormat: "yy/mm/dd"
             });  $("#ContractExpiryDate").datepicker({
                      isRTL: true,
                        dateFormat: "yy/mm/dd"
             });  $("#eIssuanceDate").datepicker({
                      isRTL: true,
                        dateFormat: "yy/mm/dd"
             });  $("#eExpiryDate").datepicker({
                      isRTL: true,
                        dateFormat: "yy/mm/dd"
             });

        });


    </script>


</head>
<body  data-open="click" data-menu="vertical-menu" data-col="2-columns" class="vertical-layout vertical-menu 2-columns  fixed-navbar">
<!-- navbar-fixed-top-->
<div class="spinner" style="position: absolute">
    <div class="double-bounce1"></div>
    <div class="double-bounce2"></div>
</div>
<div id="sppiner" class="">
<jsp:directive.include file="inc/topmenu.jsp"/>
<div class="fullscreen-search-overlay"></div>

<!-- ////////////////////////////////////////////////////////////////////////////-->


<!-- main menu-->
<div data-scroll-to-active="true" class="main-menu menu-fixed menu-dark menu-accordion menu-shadow">
  <!-- main menu header-->
  <!-- / main menu header-->
  <!-- main menu content-->

  <!--Menu -->
  <jsp:directive.include file="inc/menu.jsp"/>
  <!--Menu -->

  <!-- /main menu content-->
</div>
<!-- / main menu-->

<div class="app-content content container-fluid">

  <div class="content-wrapper">
    <div class="content-header row">
    </div>
    <div class="content-body">
      <!-- Sales stats -->


      <section id="register_request_1">
        <div class="row">
          <div class="col-xs-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">ثبت درخواست</h4>
                <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                <div class="heading-elements">
                  <ul class="list-inline mb-0">
                    <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                  </ul>
                </div>
              </div>
              <div class="card-body collapse in">
                <div class="card-block card-dashboard ">
                  <div class="row">
                      <div class="col-md-6">


                          <div class="form-group">
                          <label >
                            شركت ارائه دهنده :
                            <span class="danger">*</span>
                          </label>
                          <select  class="custom-select form-control" id="provider_company" name="itemoption_company">
                            <option value="-1">-- شركت را انتخاب نمایید --</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-6" >
                        <div class="form-group" id="option_request_type">
                          <label >
                            نوع درخواست :
                            <span class="danger">*</span>
                          </label>
                          <select  class="custom-select form-control" id="request_type"  name="itemoption_company" >
                              <option value="-1">-- درخواست را انتخاب نمایید --</option>

                          </select>
                        </div>
                      </div>

                    </div>
                  <div class="row">

                      <div class="col-md-6">
                        <div class="form-group">
                          <label >
                            شماره مشتری :
                            <span class="danger">*</span>
                          </label>
                          <input type="text"  class="form-control maleki" value=""  name="itemoption_company" id="customer_no" placeholder="شماره مشتری را درج نمایید" >


                        </div>
                      </div>
                    <div class="col-md-6" style="margin-top: 26px;">
                      <button class="btn btn-success" onclick="acquire_info2()">اطلاعات پذیرنده</button>
                    </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section id="real_acquire_specification_2">
        <div class="row">
          <div class="col-xs-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">مشخصات پذیرنده</h4>
                <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                <div class="heading-elements">
                  <ul class="list-inline mb-0">
                    <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                  </ul>
                </div>
              </div>
              <div class="card-body collapse in">
                <div class="card-block card-dashboard ">
                  <div class="row">
                      <div class="col-md-3">
                        <div class="form-group">
                          <label >
                           نوع مشتری :
                              <span class="danger">*</span>

                          </label>
                          <input  class="form-control" id="clientTypeDesc" name="" disabled>

                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label >
                           جنسیت :
                              <span class="danger">*</span>

                          </label>
                          <input  class="form-control" id="sex" name="" disabled>

                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label >
                          وضعیت حیات :
                              <span class="danger">*</span>

                          </label>
                          <input  class="form-control" id="lifeStatus" name="" disabled>

                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label >
                           كد ملی :
                              <span class="danger">*</span>

                          </label>
                          <input  class="form-control" id="nationalCode" name="" disabled>

                        </div>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-md-3">
                        <div class="form-group">
                          <label >
                           نام :
                              <span class="danger">*</span>

                          </label>
                          <input  class="form-control" id="firstName" name="" disabled>

                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label >
                           نام خانوادگی :
                              <span class="danger">*</span>

                          </label>
                          <input  class="form-control" id="lastName" name="" disabled>

                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label >
                          نام پدر :
                              <span class="danger">*</span>

                          </label>
                          <input  class="form-control" id="fatherName" name="" disabled>

                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label >
                           شماره شناسنامه :
                              <span class="danger">*</span>

                          </label>
                          <input  class="form-control" id="regNumber" name="" disabled>

                        </div>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-md-3">
                        <div class="form-group">
                          <label >
                           نام انگلیسی :
                              <span class="danger">*</span>

                          </label>
                          <input  class="form-control" id="firstNameEn" name="" disabled>

                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label >
                           نام خانوادگی انگلیسی :
                              <span class="danger">*</span>

                          </label>
                          <input  class="form-control" id="lastNameEn" name="" disabled>

                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label >
                          نام پدر انگلیسی :
                              <span class="danger">*</span>

                          </label>
                          <input  class="form-control" id="fatherNameEn" name="" disabled>

                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label >
                          تاریخ تولد :
                              <span class="danger">*</span>

                          </label>
                          <input  class="form-control" id="birthDate" name="" disabled>
<%----%>
                        </div>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-md-1">
                          <div class="form-group">
                              <label >
                                  كشور :
                                  <span class="danger">*</span>

                              </label>
                              <input  class="form-control" id="countryNameAbbr" name="" disabled>

                          </div>
                      </div>
                      <div class="col-md-2">
                        <div class="form-group">
                          <label >
                            &nbsp
                          </label>
                          <input  class="form-control" id="countryDesc" name="" disabled>

                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label >
                           تاریخ صدور :
                              <span class="danger">*</span>

                          </label>
                          <input  class="form-control" id="issuanceDate" name="" disabled>

                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label >
                          محل صدور :
                              <span class="danger">*</span>

                          </label>
                          <input  class="form-control" id="issuancePlaceDesc" name="" disabled>

                        </div>
                      </div>
                      <div class="col-md-3">
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-md-3">
                        <div class="form-group">
                          <label >
                           شماره تلفن ثابت(پیش شماره) :
                            <span class="danger">*</span>

                          </label>
                          <input  class="form-control" id="phone" placeholder="02142710000" name="" style="direction: ltr" >

                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label >
                           شماره تلفن همراه :
                            <span class="danger">*</span>

                          </label>
                          <input  class="form-control" id="mobilePhone" value="" name="" style="direction: ltr">

                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label >
                          نمابر(پیش شماره) :
                              <span class="danger">*</span>
                          </label>
                          <input  class="form-control" id="fax" value="02126214971" name="" style="direction: ltr">

                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label >
                            كد پستی :
                            <span class="danger">*</span>

                          </label>
                          <input  class="form-control" id="postalCode" value="1486787799" name="" style="direction: ltr" >

                        </div>
                      </div>
                  </div>
                  <div class="row">

                      <div class="col-md-6">
                        <div class="form-group">
                          <label >
                           آدرس :
                            <span class="danger">*</span>
                          </label>
                          <textarea type="text"  class="form-control" value=""  name="itemoption_company" id="address">خ ولیعصر</textarea>

                        </div>
                      </div>
                    <div class="col-md-6" style="margin-top: 40px;text-align: left">
                        <button class="btn btn-success" id="account_info_1" onclick="real_account_info()">اطلاعات حساب</button>
                        <button class="btn btn-warning" onclick="register_request_back()">بازگشت</button>
                    </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section id="citizen_real_acquire_specification_2">
        <div class="row">
          <div class="col-xs-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title"> مشخصات پذیرنده حقیقی اتباع</h4>
                <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                <div class="heading-elements">
                  <ul class="list-inline mb-0">
                    <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                  </ul>
                </div>
              </div>
              <div class="card-body collapse in">
                <div class="card-block card-dashboard ">
                  <div class="row">
                      <div class="col-md-3">
                        <div class="form-group">
                          <label >
                           نوع مشتری :
                              <span class="danger">*</span>

                          </label>
                          <input  class="form-control" id="c_clientType" name="" disabled>

                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label >
                           جنسیت :
                              <span class="danger">*</span>

                          </label>
                          <input  class="form-control" id="c_sex" name="" disabled>

                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label >
                          وضعیت حیات :
                              <span class="danger">*</span>

                          </label>
                          <input  class="form-control" id="c_lifeStatus" name="" disabled>

                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label >
                           كد فراگیر :
                              <span class="danger">*</span>

                          </label>
                          <input  class="form-control" id="c_foreignCode" name="" disabled>

                        </div>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-md-3">
                        <div class="form-group">
                          <label >
                           نام :
                              <span class="danger">*</span>

                          </label>
                          <input  class="form-control" id="c_firstName" name="" disabled>

                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label >
                           نام خانوادگی :
                              <span class="danger">*</span>

                          </label>
                          <input  class="form-control" id="c_lastName" name="" disabled>

                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label >
                          نام پدر :
                              <span class="danger">*</span>

                          </label>
                          <input  class="form-control" id="c_fatherName" name="" disabled>

                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label >
                           شماره گذرنامه :
                              <span class="danger">*</span>

                          </label>
                          <input  class="form-control" id="c_passportnumber" name="" disabled>

                        </div>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-md-3">
                        <div class="form-group">
                          <label >
                           نام انگلیسی :
                              <span class="danger">*</span>

                          </label>
                          <input  class="form-control" id="c_firstnameEN" name="" disabled>

                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label >
                           نام خانوادگی انگلیسی :
                              <span class="danger">*</span>

                          </label>
                          <input  class="form-control" id="c_lastnameEN" name="" disabled>

                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label >
                          نام پدر انگلیسی :
                              <span class="danger">*</span>

                          </label>
                          <input  class="form-control" id="c_fatherNameEn" name="" disabled>

                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label >
                          تاریخ صدور :
                              <span class="danger">*</span>

                          </label>
                          <input  class="form-control" id="c_issuanceDate" name="" disabled>

                        </div>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-md-1">
                        <div class="form-group">
                          <label >
                           كشور :
                              <span class="danger">*</span>

                          </label>
                          <input  class="form-control" id="c_countryNameAbbr" name="" disabled>

                        </div>
                      </div>
                      <div class="col-md-2">
                          <div class="form-group">
                              <label >
                                &nbsp
                              </label>
                              <input  class="form-control" id="c_countryDesc" name="" disabled>

                          </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label >
                           تاریخ تولد :
                              <span class="danger">*</span>

                          </label>
                          <input  class="form-control" id="c_birthDate" name="" disabled>

                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label >
                          محل صدور :
                              <span class="danger">*</span>

                          </label>
                          <input  class="form-control" id="c_issuancePlaceCode" name="" disabled>

                        </div>
                      </div>
                      <div class="col-md-3">
                          <div class="form-group">
                              <label >
                                  تاریخ اعتبار گذرنامه:
                                  <span class="danger">*</span>

                              </label>
                              <input  class="form-control" id="c_passportExpiryDate" name="" disabled>

                          </div>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-md-3">
                        <div class="form-group">
                          <label >
                              شماره تلفن ثابت(پیش شماره) :
                            <span class="danger">*</span>

                          </label>
                          <input  class="form-control number" placeholder="02142710000" id="c_phone" name="" >

                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group number">
                          <label >
                           شماره تلفن همراه :
                            <span class="danger">*</span>

                          </label>
                          <input  class="form-control" id="c_mobilePhone" name="" >

                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label >
                          نمابر(پیش شماره) :
                              <span class="danger">*</span>

                          </label>
                          <input  class="form-control number" placeholder="02142710000" id="c_fax" name=""  >

                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label >
                            كد پستی :
                            <span class="danger">*</span>

                          </label>
                          <input  class="form-control number" id="c_postalCode" name="" >

                        </div>
                      </div>
                  </div>
                  <div class="row">

                      <div class="col-md-6">
                          <div class="form-group">
                              <label >
                                  آدرس :
                                  <span class="danger">*</span>
                              </label>
                              <textarea type="text"  class="form-control" value=""  name="itemoption_company" id="c_address"></textarea>

                          </div>
                      </div>
                    <div class="col-md-6" style="margin-top: 40px;text-align: left">
                        <button class="btn btn-success" id="citizen_account_info" onclick="citizen_account_info()">اطلاعات صاحب حساب</button>
                        <button class="btn btn-warning" onclick="register_request_back()">بازگشت</button>
                    </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section id="legal_acquire_specification_2">
        <div class="row">
          <div class="col-xs-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">مشخصات پذیرنده حقوقی</h4>
                <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                <div class="heading-elements">
                  <ul class="list-inline mb-0">
                    <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                  </ul>
                </div>
              </div>
              <div class="card-body collapse in">
                <div class="card-block card-dashboard ">
                  <div class="row">
                      <div class="col-md-3">
                        <div class="form-group">
                          <label >
                           نوع مشتری :
                              <span class="danger">*</span>

                          </label>
                          <input  class="form-control" id="l_clientType" name="" disabled>

                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label >
                           نام :
                              <span class="danger">*</span>

                          </label>
                          <input  class="form-control" id="l_firstName" name="" disabled>

                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label >
                           كد اقتصادی :
                              <span class="danger">*</span>

                          </label>
                          <input  class="form-control" id="l_commerceCode" name="" disabled>

                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label >
                           شناسه ملی :
                              <span class="danger">*</span>

                          </label>
                          <input  class="form-control" id="l_nationalCode" name="" disabled>

                        </div>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-md-3">
                        <div class="form-group">
                          <label >
                           نام انگلیسی :
                              <span class="danger">*</span>

                          </label>
                          <input  class="form-control" id="l_firstNameEn" name="" disabled>

                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label >
                           شماره ثبت :
                              <span class="danger">*</span>

                          </label>
                          <input  class="form-control" id="l_regNumber" name="" disabled>

                        </div>
                      </div>
                      <div class="col-md-3">
                          <div class="form-group">
                              <label >
                                  محل صدور :
                                  <span class="danger">*</span>

                              </label>
                              <input  class="form-control" id="l_issuancePlaceDesc" name="" disabled>

                          </div>

                      </div>
                      <div class="col-md-1">
                        <div class="form-group">
                          <label >
                           كشور :
                              <span class="danger">*</span>

                          </label>
                          <input  class="form-control" id="l_countryNameAbbr" name="" disabled>

                        </div>
                      </div>
                          <div class="col-md-2">
                              <div class="form-group">
                                  <label >
                                    &nbsp
                                  </label>
                                  <input  class="form-control" id="l_countryDesc" name="" disabled>

                              </div>
                          </div>

                  </div>
                  <div class="row">
                      <div class="col-md-3">
                        <div class="form-group">
                          <label >
                              تاریخ ثبت :
                              <span class="danger">*</span>

                          </label>
                          <input  class="form-control" id="l_birthDate" name="" disabled>

                        </div>
                      </div>
                      <div class="col-md-3">
                        <%--<div class="form-group">--%>
                          <%--<label >--%>
                           <%--تاریخ ثبت :--%>
                              <%--<span class="danger">*</span>--%>

                          <%--</label>--%>
                          <%--<input  class="form-control" id="l_birthDate" name="" disabled>--%>

                        <%--</div>--%>
                      </div>
                      <div class="col-md-3">
                      </div>
                      <div class="col-md-3">
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-md-3">
                        <div class="form-group">
                          <label >
                              شماره تلفن ثابت(پیش شماره) :
                            <span class="danger">*</span>

                          </label>
                          <input  class="form-control number" placeholder="02142710000" id="l_phone" name="" >

                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label >
                           شماره تلفن همراه :
                            <span class="danger">*</span>

                          </label>
                          <input  class="form-control number" id="l_mobilePhone" name="" >

                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label >
                          نمابر(پیش شماره) :
                              <span class="danger">*</span>

                          </label>
                          <input  class="form-control number" placeholder="02142710000" id="l_fax" name="" >

                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label >
                            كد پستی :
                            <span class="danger">*</span>

                          </label>
                          <input  class="form-control number" id="l_postalCode" name="" >

                        </div>
                      </div>
                  </div>
                  <div class="row">

                      <div class="col-md-6">
                          <div class="form-group">
                              <label >
                                  آدرس :
                                  <span class="danger">*</span>
                              </label>
                              <textarea type="text"  class="form-control"   name="itemoption_company" id="l_address"></textarea>

                          </div>
                      </div>
                    <div class="col-md-6" style="margin-top: 40px;text-align: left">
                        <button class="btn btn-success" id="legal_account_info_btn" onclick="legal_account_info()">اطلاعات حساب</button>
                        <button class="btn btn-warning" onclick="register_request_back()">بازگشت</button>
                    </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section id="account_info_3">
        <div class="row">
          <div class="col-xs-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">اطلاعات حساب </h4>
                <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                <div class="heading-elements">
                  <ul class="list-inline mb-0">
                    <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                  </ul>
                </div>
              </div>
              <div class="card-body collapse in">
                <div class="card-block card-dashboard ">
                  <div class="row">
                      <div class="col-md-12">
                        <table class="table table-bordered">
                          <thead>
                          <tr style="font-size: 13px;text-align: center;background-color: darkseagreen">
                            <td>انتخاب</td>
                            <td>شماره حساب</td>
                            <td>شرح حساب</td>
                            <td>نوع حساب</td>
                            <td>شماره شبا</td>
                            <td>كد شعبه دارنده حساب</td>
                          </tr>
                          </thead>
                          <tbody id="account_info_table">

                          </tbody>
                        </table>
                      </div>
                  </div>

                  <div class="row">
                      <div class="col-md-6">
                      </div>
                    <div class="col-md-6" style="margin-top: 40px;text-align: left">
                      <button class="btn btn-success" id="account_owners_info" onclick="account_owners_info()">اطلاعات صاحبان حساب</button>
                      <button class="btn btn-warning" onclick="acquire_info_back2()">بازگشت</button>
                    </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section id="account_owners_info_4">
        <div class="row">
          <div class="col-xs-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">صاحبان حساب</h4>
                <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                <div class="heading-elements">
                  <ul class="list-inline mb-0">
                    <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                  </ul>
                </div>
              </div>
              <div class="card-body collapse in">
                <div class="card-block card-dashboard ">
                  <div class="row">
                      <div class="col-md-12">
                        <table class="table table-bordered">
                          <thead>
                          <tr style="font-size: 13px;text-align: center;background-color: darkseagreen">
                            <td>انتخاب</td>
                            <td>شماره مشتری</td>
                            <td>نام مشتری</td>
                            <td>وضعیت</td>
                          </tr>
                          </thead>
                          <tbody id="account_info_table_2">

                          </tbody>
                        </table>
                      </div>
                  </div>

                  <div class="row">

                      <div class="col-md-6">
                      </div>
                    <div class="col-md-6" style="margin-top: 40px;text-align: left">
                      <button class="btn btn-success" onclick="acquire_info5()">اطلاعات صاحبان حساب</button>
                      <button class="btn btn-warning" onclick="account_info_back()">بازگشت</button>
                    </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <div class="real_acquire_specification_5" id="real_acquire_specification_5">



      </div>


      <%--<section id="citizen_real_acquire_specification_5">--%>
        <%--<div class="row">--%>
          <%--<div class="col-xs-12">--%>
            <%--<div class="card">--%>
              <%--<div class="card-header">--%>
                <%--<h4 class="card-title">اطلاعات حساب</h4>--%>
                <%--<a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>--%>
                <%--<div class="heading-elements">--%>
                  <%--<ul class="list-inline mb-0">--%>
                    <%--<li><a data-action="expand"><i class="icon-expand2"></i></a></li>--%>
                  <%--</ul>--%>
                <%--</div>--%>
              <%--</div>--%>
              <%--<div class="card-body collapse in">--%>
                <%--<div class="card-block card-dashboard ">--%>
                  <%--<div class="row">--%>
                    <%--<div class="col-md-3">--%>
                      <%--<div class="form-group">--%>
                        <%--<label >--%>
                          <%--نوع مشتری :--%>
                        <%--</label>--%>
                        <%--<input  class="form-control" id="clientTypeDesc" name="" disabled>--%>

                      <%--</div>--%>
                    <%--</div>--%>
                    <%--<div class="col-md-3">--%>
                      <%--<div class="form-group">--%>
                        <%--<label >--%>
                          <%--جنسیت :--%>
                        <%--</label>--%>
                        <%--<input  class="form-control" id="sex" name="" disabled>--%>

                      <%--</div>--%>
                    <%--</div>--%>
                    <%--<div class="col-md-3">--%>
                      <%--<div class="form-group">--%>
                        <%--<label >--%>
                          <%--وضعیت حیات :--%>
                        <%--</label>--%>
                        <%--<input  class="form-control" id="lifeStatus" name="" disabled>--%>

                      <%--</div>--%>
                    <%--</div>--%>
                    <%--<div class="col-md-3">--%>
                      <%--<div class="form-group">--%>
                        <%--<label >--%>
                          <%--كد فراگیر :--%>
                        <%--</label>--%>
                        <%--<input  class="form-control" id="foreignCode" name="" disabled>--%>

                      <%--</div>--%>
                    <%--</div>--%>
                  <%--</div>--%>
                  <%--<div class="row">--%>
                    <%--<div class="col-md-3">--%>
                      <%--<div class="form-group">--%>
                        <%--<label >--%>
                          <%--نام :--%>
                        <%--</label>--%>
                        <%--<input  class="form-control" id="givenName" name="" disabled>--%>

                      <%--</div>--%>
                    <%--</div>--%>
                    <%--<div class="col-md-3">--%>
                      <%--<div class="form-group">--%>
                        <%--<label >--%>
                          <%--نام خانوادگی :--%>
                        <%--</label>--%>
                        <%--<input  class="form-control" id="enGivenName" name="" disabled>--%>

                      <%--</div>--%>
                    <%--</div>--%>
                    <%--<div class="col-md-3">--%>
                      <%--<div class="form-group">--%>
                        <%--<label >--%>
                          <%--نام پدر :--%>
                        <%--</label>--%>
                        <%--<input  class="form-control" id="fatherName" name="" disabled>--%>

                      <%--</div>--%>
                    <%--</div>--%>
                    <%--<div class="col-md-3">--%>
                      <%--<div class="form-group">--%>
                        <%--<label >--%>
                          <%--شماره گذرنامه :--%>
                        <%--</label>--%>
                        <%--<input  class="form-control" id="passportNumber" name="" disabled>--%>

                      <%--</div>--%>
                    <%--</div>--%>
                  <%--</div>--%>
                  <%--<div class="row">--%>
                    <%--<div class="col-md-3">--%>
                      <%--<div class="form-group">--%>
                        <%--<label >--%>
                          <%--نام انگلیسی :--%>
                        <%--</label>--%>
                        <%--<input  class="form-control" id="enGivenName" name="" disabled>--%>

                      <%--</div>--%>
                    <%--</div>--%>
                    <%--<div class="col-md-3">--%>
                      <%--<div class="form-group">--%>
                        <%--<label >--%>
                          <%--نام خانوادگی انگلیسی :--%>
                        <%--</label>--%>
                        <%--<input  class="form-control" id="enSurName" name="" disabled>--%>

                      <%--</div>--%>
                    <%--</div>--%>
                    <%--<div class="col-md-3">--%>
                      <%--<div class="form-group">--%>
                        <%--<label >--%>
                          <%--نام پدر انگلیسی :--%>
                        <%--</label>--%>
                        <%--<input  class="form-control" id="enfatherName" name="" disabled>--%>

                      <%--</div>--%>
                    <%--</div>--%>
                    <%--<div class="col-md-3">--%>
                      <%--<div class="form-group">--%>
                        <%--<label >--%>
                          <%--تاریخ اعتبار گذرنامه :--%>
                        <%--</label>--%>
                        <%--<input  class="form-control" id="passportExpiryDate" name="" disabled>--%>

                      <%--</div>--%>
                    <%--</div>--%>
                  <%--</div>--%>
                  <%--<div class="row">--%>
                      <%--<div class="col-md-1">--%>
                          <%--<div class="form-group">--%>
                              <%--<label >--%>
                                  <%--كشور :--%>
                              <%--</label>--%>
                              <%--<input  class="form-control" id="countryNameAbbr" name="" disabled>--%>

                          <%--</div>--%>
                      <%--</div>--%>
                    <%--<div class="col-md-2">--%>
                      <%--<div class="form-group">--%>
                        <%--<label >--%>
                            <%--&nbsp--%>
                        <%--</label>--%>
                        <%--<input  class="form-control" id="countryDesc" name="" disabled>--%>

                      <%--</div>--%>
                    <%--</div>--%>
                    <%--<div class="col-md-3">--%>
                      <%--<div class="form-group">--%>
                        <%--<label >--%>
                          <%--تاریخ تولد :--%>
                        <%--</label>--%>
                        <%--<input  class="form-control" id="birthDate" name="" disabled>--%>

                      <%--</div>--%>
                    <%--</div>--%>
                    <%--<div class="col-md-3">--%>
                      <%--<div class="form-group">--%>
                        <%--<label >--%>
                          <%--محل صدور :--%>
                        <%--</label>--%>
                        <%--<input  class="form-control" id="issuPlaceDesc" name="" disabled>--%>

                      <%--</div>--%>
                    <%--</div>--%>
                      <%--<div class="col-md-3">--%>
                          <%--<div class="form-group">--%>
                              <%--<label >--%>
                                  <%--تاریخ صدور :--%>
                              <%--</label>--%>
                              <%--<input  class="form-control" id="dateOfIssuance" name="" disabled>--%>

                          <%--</div>--%>
                      <%--</div>--%>
                  <%--</div>--%>
                  <%--<div class="row">--%>
                    <%--<div class="col-md-3">--%>
                      <%--<div class="form-group">--%>
                        <%--<label >--%>
                          <%--شماره تلفن ثابت :--%>
                          <%--<span class="danger">*</span>--%>

                        <%--</label>--%>
                        <%--<input  class="form-control" id="phone" name="" >--%>

                      <%--</div>--%>
                    <%--</div>--%>
                    <%--<div class="col-md-3">--%>
                      <%--<div class="form-group">--%>
                        <%--<label >--%>
                          <%--شماره تلفن همراه :--%>
                          <%--<span class="danger">*</span>--%>

                        <%--</label>--%>
                        <%--<input  class="form-control" id="mobilePhone" name="" >--%>

                      <%--</div>--%>
                    <%--</div>--%>
                    <%--<div class="col-md-3">--%>
                      <%--<div class="form-group">--%>
                        <%--<label >--%>
                          <%--نمابر :--%>

                        <%--</label>--%>
                        <%--<input  class="form-control" id="fax" name="" >--%>

                      <%--</div>--%>
                    <%--</div>--%>
                    <%--<div class="col-md-3">--%>
                      <%--<div class="form-group">--%>
                        <%--<label >--%>
                          <%--كد پستی :--%>
                          <%--<span class="danger">*</span>--%>

                        <%--</label>--%>
                        <%--<input  class="form-control" id="postalCode" name="" >--%>

                      <%--</div>--%>
                    <%--</div>--%>
                  <%--</div>--%>
                  <%--<div class="row">--%>

                    <%--<div class="col-md-6">--%>
                      <%--<div class="form-group">--%>
                        <%--<label >--%>
                          <%--آدرس :--%>
                          <%--<span class="danger">*</span>--%>
                        <%--</label>--%>
                        <%--<textarea type="text" id="address" class="form-control"  name="itemoption_company">--%>
                          <%--</textarea>--%>

                      <%--</div>--%>
                    <%--</div>--%>
                    <%--<div class="col-md-6">--%>

                    <%--</div>--%>
                  <%--</div>--%>

                  <%--<div class="row">--%>
                    <%--<div class="col-md-3">--%>
                      <%--<label >--%>
                        <%--سمت :--%>
                        <%--<span class="danger">*</span>--%>
                      <%--</label>--%>
                      <%--<select class="custom-select form-control required" id="" name="eventType">--%>
                        <%--<option value="IPG">IPG</option>--%>
                        <%--<option value="POS">POS</option>--%>
                      <%--</select>--%>
                    <%--</div>--%>
                    <%--<div class="col-md-9">--%>
                      <%--<div class="form-group">--%>
                        <%--<label >--%>
                          <%--كنترل اطلاعات مشتری :--%>
                          <%--<span class="danger">*</span>--%>
                        <%--</label>--%>
                        <%--<textarea type="text"  class="form-control"  name="itemoption_company">--%>
                          <%--</textarea>--%>

                      <%--</div>--%>
                    <%--</div>--%>

                  <%--</div>--%>
                  <%--<div class="row">--%>
                    <%--<div class="col-md-6">--%>
                    <%--</div>--%>
                    <%--<div class="col-md-6" style="margin-top: 40px;text-align: left">--%>
                        <%--<button class="btn btn-success" onclick="store_info()">اطلاعات حساب</button>--%>
                        <%--<button class="btn btn-warning" onclick="account_owners_info_back()">بازگشت</button>--%>
                    <%--</div>--%>
                  <%--</div>--%>
                <%--</div>--%>
              <%--</div>--%>
            <%--</div>--%>
          <%--</div>--%>
        <%--</div>--%>
      <%--</section>--%>
      <%--<section id="legal_acquire_specification_5">--%>
        <%--<div class="row">--%>
          <%--<div class="col-xs-12">--%>
            <%--<div class="card">--%>
              <%--<div class="card-header">--%>
                <%--<h4 class="card-title">مشخصات پذیرنده حقوقی</h4>--%>
                <%--<a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>--%>
                <%--<div class="heading-elements">--%>
                  <%--<ul class="list-inline mb-0">--%>
                    <%--<li><a data-action="expand"><i class="icon-expand2"></i></a></li>--%>
                  <%--</ul>--%>
                <%--</div>--%>
              <%--</div>--%>
              <%--<div class="card-body collapse in">--%>
                <%--<div class="card-block card-dashboard ">--%>
                  <%--<div class="row">--%>
                    <%--<div class="col-md-3">--%>
                      <%--<div class="form-group">--%>
                        <%--<label >--%>
                          <%--نوع مشتری :--%>
                        <%--</label>--%>
                        <%--<input  class="form-control" id="clientTypeDesc" name="" disabled>--%>

                      <%--</div>--%>
                    <%--</div>--%>
                    <%--<div class="col-md-3">--%>
                      <%--<div class="form-group">--%>
                        <%--<label >--%>
                          <%--نام :--%>
                        <%--</label>--%>
                        <%--<input  class="form-control" id="clientName" name="" disabled>--%>

                      <%--</div>--%>
                    <%--</div>--%>
                    <%--<div class="col-md-3">--%>
                      <%--<div class="form-group">--%>
                        <%--<label >--%>
                          <%--كد اقتصادی :--%>
                        <%--</label>--%>
                        <%--<input  class="form-control" id="commerceCode" name="" disabled>--%>

                      <%--</div>--%>
                    <%--</div>--%>
                    <%--<div class="col-md-3">--%>
                      <%--<div class="form-group">--%>
                        <%--<label >--%>
                          <%--شناسه ملی :--%>
                        <%--</label>--%>
                        <%--<input  class="form-control" id="nationalCode" name="" disabled>--%>

                      <%--</div>--%>
                    <%--</div>--%>
                  <%--</div>--%>
                  <%--<div class="row">--%>
                    <%--<div class="col-md-3">--%>
                      <%--<div class="form-group">--%>
                        <%--<label >--%>
                          <%--نام انگلیسی :--%>
                        <%--</label>--%>
                        <%--<input  class="form-control" id="enCompanyName" name="" disabled>--%>

                      <%--</div>--%>
                    <%--</div>--%>
                    <%--<div class="col-md-3">--%>
                      <%--<div class="form-group">--%>
                        <%--<label >--%>
                          <%--شماره ثبت :--%>
                        <%--</label>--%>
                        <%--<input  class="form-control" id="regNumber" name="" disabled>--%>

                      <%--</div>--%>
                    <%--</div>--%>
                      <%--<div class="col-md-1">--%>
                          <%--<div class="form-group">--%>
                              <%--<label >--%>
                                  <%--كشور :--%>
                              <%--</label>--%>
                              <%--<input  class="form-control" id="countryNameAbbr" name="" disabled>--%>

                          <%--</div>--%>
                      <%--</div>--%>
                    <%--<div class="col-md-2">--%>
                      <%--<div class="form-group">--%>
                        <%--<label >--%>
                         <%--&nbsp--%>
                        <%--</label>--%>
                        <%--<input  class="form-control" id="countryDesc" name="" disabled>--%>

                      <%--</div>--%>
                    <%--</div>--%>
                      <%--<div class="col-md-3">--%>
                          <%--<div class="form-group">--%>
                              <%--<label >--%>
                                  <%--تاریخ صدور :--%>
                              <%--</label>--%>
                              <%--<input  class="form-control" id="dateOfIssuance" name="" disabled>--%>

                          <%--</div>--%>
                      <%--</div>--%>
                  <%--</div>--%>
                  <%--<div class="row">--%>

                    <%--<div class="col-md-3">--%>
                      <%--<div class="form-group">--%>
                        <%--<label >--%>
                          <%--تاریخ ثبت :--%>
                        <%--</label>--%>
                        <%--<input  class="form-control" id="birthDate" name="" disabled>--%>

                      <%--</div>--%>
                    <%--</div>--%>
                    <%--<div class="col-md-3">--%>
                      <%--<div class="form-group">--%>
                        <%--<label >--%>
                          <%--محل صدور :--%>
                        <%--</label>--%>
                        <%--<input  class="form-control" id="issuPlaceDesc" name="" disabled>--%>

                      <%--</div>--%>
                    <%--</div>--%>
                      <%--<div class="col-md-3">--%>
                      <%--</div>--%>
                  <%--</div>--%>
                  <%--<div class="row">--%>
                    <%--<div class="col-md-3">--%>
                      <%--<div class="form-group">--%>
                        <%--<label >--%>
                          <%--شماره تلفن ثابت :--%>
                          <%--<span class="danger">*</span>--%>

                        <%--</label>--%>
                        <%--<input  class="form-control" id="phone" name="" >--%>

                      <%--</div>--%>
                    <%--</div>--%>
                    <%--<div class="col-md-3">--%>
                      <%--<div class="form-group">--%>
                        <%--<label >--%>
                          <%--شماره تلفن همراه :--%>
                          <%--<span class="danger">*</span>--%>

                        <%--</label>--%>
                        <%--<input  class="form-control" id="mobilePhone" name="" >--%>

                      <%--</div>--%>
                    <%--</div>--%>
                    <%--<div class="col-md-3">--%>
                      <%--<div class="form-group">--%>
                        <%--<label >--%>
                          <%--نمابر :--%>

                        <%--</label>--%>
                        <%--<input  class="form-control" id="fax" name="" >--%>

                      <%--</div>--%>
                    <%--</div>--%>
                    <%--<div class="col-md-3">--%>
                      <%--<div class="form-group">--%>
                        <%--<label >--%>
                          <%--كد پستی :--%>
                          <%--<span class="danger">*</span>--%>

                        <%--</label>--%>
                        <%--<input  class="form-control" id="postalCode" name="" >--%>

                      <%--</div>--%>
                    <%--</div>--%>
                  <%--</div>--%>
                  <%--<div class="row">--%>

                    <%--<div class="col-md-6">--%>
                      <%--<div class="form-group">--%>
                        <%--<label >--%>
                          <%--آدرس :--%>
                          <%--<span class="danger">*</span>--%>
                        <%--</label>--%>
                        <%--<textarea type="text" id="address" class="form-control"  name="itemoption_company">--%>
                          <%--</textarea>--%>

                      <%--</div>--%>
                    <%--</div>--%>
                    <%--<div class="col-md-6">--%>

                    <%--</div>--%>
                  <%--</div>--%>

                  <%--<div class="row">--%>
                    <%--<div class="col-md-3">--%>
                      <%--<label >--%>
                        <%--سمت :--%>
                        <%--<span class="danger">*</span>--%>
                      <%--</label>--%>
                      <%--<select class="custom-select form-control required" id="" name="eventType">--%>
                        <%--<option value="IPG">IPG</option>--%>
                        <%--<option value="POS">POS</option>--%>
                      <%--</select>--%>
                    <%--</div>--%>
                    <%--<div class="col-md-9">--%>
                      <%--<div class="form-group">--%>
                        <%--<label >--%>
                          <%--كنترل اطلاعات مشتری :--%>
                          <%--<span class="danger">*</span>--%>
                        <%--</label>--%>
                        <%--<textarea type="text"  class="form-control"  name="itemoption_company">--%>
                          <%--</textarea>--%>

                      <%--</div>--%>
                    <%--</div>--%>

                  <%--</div>--%>
                  <%--<div class="row">--%>
                    <%--<div class="col-md-6">--%>
                    <%--</div>--%>
                    <%--<div class="col-md-6" style="margin-top: 40px;text-align: left">--%>
                        <%--<button class="btn btn-success" onclick="store_info()">اطلاعات حساب</button>--%>
                        <%--<button class="btn btn-warning" onclick="account_owners_info_back()">بازگشت</button>--%>
                    <%--</div>--%>
                  <%--</div>--%>
                <%--</div>--%>
              <%--</div>--%>
            <%--</div>--%>
          <%--</div>--%>
        <%--</div>--%>
      <%--</section>--%>
      <section id="store_info_6">
            <div class="row">
                <div class="col-xs-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">اطلاعات فروشگاه</h4>
                            <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-body collapse in">
                            <div class="card-block card-dashboard ">

                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label >
                                                نام فروشگاه :
                                                <span class="danger">*</span>

                                            </label>
                                            <input  class="form-control" id="ShopName" name="" >

                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label >
                                                نام انگلیسی :
                                                <span class="danger">*</span>

                                            </label>
                                            <input  class="form-control" id="ShopNameEn" name="" >

                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label >
                                                كد تكمیلی صنف :
                                                <span class="danger">*</span>

                                            </label>
                                            <select  class="form-control" id="GuildCode" name="" >
                                                <option value="-1">--انتخاب كد تكمیلی صنف--</option>

                                            </select>

                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label >
                                                نوع مالكیت :
                                                <span class="danger">*</span>

                                            </label>
                                            <select  class="custom-select form-control" id="OwnershipType" onchange="OwnershipType_change()" name="" >
                                                <option value="-1">--انتخاب نوع مالكیت--</option>
                                                <option value="1">مالك</option>
                                                <option value="2">استیجاری</option>
                                            </select>

                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label >
                                                شماره قرارداد اجاره :
                                                <span class="danger" id="ContractNo_m" >*</span>

                                            </label>
                                            <input  class="form-control" id="ContractNo" name=""  >

                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label >
                                                تاریخ اتمام قرارداد اجاره :
                                                <span class="danger" id="ContractExpiryDate_m" >*</span>

                                            </label>
                                            <input  class="form-control date-inputmask" placeholder="yyyy/mm/dd" id="ContractExpiryDate"  name=""  >

                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label >

                                                شماره جواز كسب :
                                                <span class="danger">*</span>
                                            </label>
                                            <input  class="form-control" id="LicenseNo" name="" >

                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label >
                                                تاریخ صدور جواز كسب :
                                                <span class="danger">*</span>

                                            </label>
                                            <input  class="form-control date-inputmask" placeholder="yyyy/mm/dd" id="LicenseIssuanceDate" name="" >

                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label >
                                                تاریخ اعتبار جواز كسب :
                                                <span class="danger">*</span>

                                            </label>
                                            <input  class="form-control date-inputmask" placeholder="yyyy/mm/dd" id="LicenseValidityDate" name="" >

                                        </div>
                                    </div>
                                    <div class="col-md-9">
                                    </div>


                                </div>
                                آدرس نصب
                                <div class="dropdown-divider">

                                </div>

                                <div class="row">
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <label >

                                                <span class="danger"> &nbsp</span>

                                            </label>
                                            <input  class="form-control" id="country_symbol" name="" value="IR" style="text-align: center" disabled>

                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label >
                                                كشور :
                                                <span class="danger">*</span>

                                            </label>
                                            <input  class="form-control" id="CountryName" name="IRAN" value="ایران" style="text-align: center" disabled >

                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label >
                                                نام استان :
                                                <span class="danger">*</span>

                                            </label>
                                            <select  class="custom-select form-control" id="StateId" name="" >
                                                <option value="-1">--انتخاب استان--</option>
                                            </select>

                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label >
                                                نام شهر :
                                                <span class="danger">*</span>

                                            </label>
                                            <select  class="form-control" id="CityId" name="" >
                                                <option value="-1">--انتخاب شهر--</option>
                                            </select>

                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label >
                                                منطقه شهرداری :
                                                <span class="danger">*</span>

                                            </label>
                                            <input  class="form-control" id="MunicipalityNo" name="" >

                                        </div>
                                    </div>
                                    </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label >
                                                شماره تلفن ثابت(پیش شماره) :
                                                <span class="danger">*</span>

                                            </label>
                                            <input  class="form-control" placeholder="02142710000" id="Store_Phone" name="" >

                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label >
                                                نمابر(پیش شماره) :
                                                <span class="danger">*</span>

                                            </label>
                                            <input  class="form-control" placeholder="02142710000" id="Store_Fax" name="" >

                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label >
                                                كد پستی :
                                                <span class="danger">*</span>

                                            </label>
                                            <input  class="form-control" id="Store_PostalCode" name="" >

                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label >
                                                        آدرس :
                                                <span class="danger">*</span>

                                            </label>
                                            <textarea class="form-control" id="Store_address"></textarea>

                                        </div>
                                    </div>

                                    <div class="col-md-8">


                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                    </div>
                                    <div class="col-md-6" style="margin-top: 40px;text-align: left">
                                        <button class="btn btn-success" onclick="terminal_type_info()">اطلاعات درخواست</button>
                                        <button class="btn btn-warning" onclick="acquire_info_back5()">بازگشت</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            </div>
      </section>
      <section id="store_info_ipg_6">
            <div class="row">
                <div class="col-xs-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">اطلاعات درخواست-نماد الكترونیكی</h4>
                            <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-body collapse in">
                            <div class="card-block card-dashboard ">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label >
                                            نوع نماد الكترونیك :
                                            <span class="danger">*</span>
                                        </label>
                                        <select class="custom-select form-control required" id="eNamadId" name="eventType">
                                            <option value="-1">نماد را انتخاب نمایید</option>

                                            <option value="1">یك ستاره</option>
                                            <option value="2">دوستاره</option>
                                        </select>
                                    </div>
                                    <div class="col-md-9">

                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label >
                                                تاریخ صدور نماد:
                                                <span class="danger">*</span>

                                            </label>
                                            <input  class="form-control date-inputmask" id="eIssuanceDate" name="" >

                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label >
                                                تاریخ اتمام نماد :
                                                <span class="danger">*</span>

                                            </label>
                                            <input  class="form-control date-inputmask" id="eExpiryDate" name="" >

                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label >
                                                آدرس سایت :
                                                <span class="danger">*</span>

                                            </label>
                                            <input  class="form-control" id="eSiteAddress" placeholder="http://www.karafarinbank.com" name="" >

                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label >
                                                آدرس پست الكترونیك :
                                                <span class="danger">*</span>

                                            </label>
                                            <input  class="form-control" id="email" name="" >

                                        </div>
                                    </div>
                                </div>

                                <div style="width: 100%; height: 20px; border-bottom: 1px solid #aaa0a080; text-align: right">
                                  <span style="font-size: 18px; background-color: #fff; padding: 0 10px;">
                                 اطلاعات رابط فنی
                                  </span>
                                </div>
                                <div class="row" style="margin-top: 14px;">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label >
                                                نام :
                                                <span class="danger">*</span>

                                            </label>
                                            <input  class="form-control" id="firstName_enamad" name="" >

                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label >
                                                نام خانوادگی :
                                                <span class="danger">*</span>

                                            </label>
                                            <input  class="form-control" id="lastName_enamad" name="" >

                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label >

                                                شماره تلفن ثابت(پیش شماره) :
                                                <span class="danger">*</span>
                                            </label>
                                            <input  class="form-control" placeholder="02142710000" id="phone_enamad" name="" >

                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label >
                                                شماره تلفن همراه :
                                                <span class="danger">*</span>

                                            </label>
                                            <input  class="form-control" id="mobile_enamad" name="" >

                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                    </div>
                                    <div class="col-md-6" style="margin-top: 40px;text-align: left">
                                        <button class="btn btn-success" onclick="ipg_submit()">ذخیره</button>
                                        <button class="btn btn-warning" onclick="ipg_submit_back()">بازگشت</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
      <section id="terminal_type_info_7">
        <div class="row">
          <div class="col-xs-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">اطلاعات در خواست - نوع پایانه</h4>
                <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                <div class="heading-elements">
                  <ul class="list-inline mb-0">
                    <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                  </ul>
                </div>
              </div>
              <div class="card-body collapse in">
                <div class="card-block card-dashboard ">
                  <div class="row">
                    <div class="col-md-3">
                      <label >
                        نوع پایانه :
                        <span class="danger">*</span>
                      </label>
                      <select class="custom-select form-control required" id="terminalDetail" name="eventType">
<option value="-1">---لطفا نوع پایانه مشخص نمایید-</option>
                      </select>
                    </div>
                    <div class="col-md-9">

                    </div>

                  </div>



                  <div class="row">
                    <div class="col-md-6">
                    </div>
                    <div class="col-md-6" style="margin-top: 40px;text-align: left">
                      <button class="btn btn-success" onclick="terminal_type_info_7()"> ذخیره </button>
                      <button class="btn btn-warning" onclick="store_info_back()">بازگشت</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
        <div class="modal fade text-xs-left" id="suc_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <%--<button type="button" class="close" data-dismiss="modal" aria-label="Close">--%>
                        <%--<span aria-hidden="true">&times;</span>--%>
                        <%--</button>--%>
                        <h4 class="modal-title" id="myModalLabel1">هشدار سیستمی</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-5">وضعیت درخواست :</div>
                            <div class="col-md-7" id="req_status"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-5">شماره درخواست : </div>
                            <div class="col-md-7" id="req_no"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-5">كدپیگیری شركت :</div>
                            <div class="col-md-7" id="req_trace"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-5">توضیحات :</div>
                            <div class="col-md-7" id="req_desc"></div>
                        </div>
                        <%--<span class="tag tag-success">اطلاعات پرسنلی</span>--%>

                        <%--<div class="dropdown-divider" style="background-color: #1b7e5a"> </div>--%>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn grey btn-outline-secondary" id="close_modal" data-dismiss="modal">تایید</button>
                        <button type="button" class="btn btn-outline-secondary" onclick="retry_tran()" id="req_retry"><i class="icon-refresh"></i> تلاش مجدد</button>
                        <button type="button" class="btn grey btn-outline-secondary"  id="req_retry_cencel" data-dismiss="modal">انصراف</button>
                        <%--<button type="button" class="btn btn-outline-primary">Save changes</button>--%>
                    </div>
                </div>
            </div>
        </div>


      <!--/ most selling products-->

    </div>
  </div>

</div>
<!-- ////////////////////////////////////////////////////////////////////////////-->

</div>

<!-- BEGIN VENDOR JS-->

<%--<script src="app-assets/js/core/libraries/jquery.js" type="text/javascript"></script>--%>
<script src="app-assets/vendors/js/ui/tether.min.js" type="text/javascript"></script>
<script src="app-assets/js/core/libraries/bootstrap.min.js" type="text/javascript"></script>
<script src="app-assets/vendors/js/ui/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
<script src="app-assets/vendors/js/ui/unison.min.js" type="text/javascript"></script>
<script src="app-assets/vendors/js/ui/blockUI.min.js" type="text/javascript"></script>
<script src="app-assets/vendors/js/ui/jquery.matchHeight-min.js" type="text/javascript"></script>
<script src="app-assets/vendors/js/ui/jquery-sliding-menu.js" type="text/javascript"></script>
<script src="app-assets/vendors/js/sliders/slick/slick.min.js" type="text/javascript"></script>
<script src="app-assets/vendors/js/ui/screenfull.min.js" type="text/javascript"></script>
<script src="app-assets/vendors/js/extensions/pace.min.js" type="text/javascript"></script>
<script src="app-assets/vendors/js/forms/extended/inputmask/jquery.inputmask.bundle.min.js" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->

<script src="app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="app-assets/js/core/libraries/jquery_ui/jquery-ui-jalali-datepicker.js" type="text/javascript"></script>
<script src="app-assets/vendors/js/tables/jsgrid/jquery.validate.min.js" type="text/javascript"></script>
<!-- END PAGE VENDOR JS-->
<!-- BEGIN ROBUST JS-->
<script src="app-assets/js/core/app-menu.js" type="text/javascript"></script>
<script src="app-assets/js/core/app.js" type="text/javascript"></script>
<script src="app-assets/js/scripts/forms/extended/form-inputmask.js" type="text/javascript"></script>
<script src="app-assets/vendors/js/extensions/sweetalert.min.js" type="text/javascript"></script>
<script src="app-assets/vendors/js/modal/sweetalert.min.js" type="text/javascript"></script>
<!-- END ROBUST JS-->
<%--<script type="text/javascript" src="app-assets/js/scripts/ui/breadcrumbs-with-stats.js"></script>--%>
<script src="app-assets/vendors/js/tables/jsgrid/RegisterTerminal.js" type="text/javascript"></script>
<script src="app-assets/js/scripts/sessions/jquery.session.js" type="text/javascript"></script>
<script src="app-assets/js/scripts/tools.js" type="text/javascript"></script>

<script type="text/javascript">
    $('#suc_modal').modal('hide');
    $('#req_retry').hide();
    $('#close_modal').hide();
    $('#req_retry_cencel').hide();
    $('.spinner').hide();
    function OwnershipType_change(){

        if ($('#OwnershipType').val() == 1){

            $('#ContractNo').attr("disabled",true)
            $('#ContractExpiryDate').attr("disabled",true)
            $('#ContractNo_m').hide()
            $('#ContractExpiryDate_m').hide()
        }
        else{

            $('#ContractNo').attr("disabled",false)
            $('#ContractExpiryDate').attr("disabled",false)
            $('#ContractNo_m').show()
            $('#ContractExpiryDate_m').show()
        }

    };




</script>

</body>
</html>
