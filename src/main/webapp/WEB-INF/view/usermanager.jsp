<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8" %>
<html lang="en" data-textdirection="rtl" class="loading">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>مدیریت پایانه های فروش</title>

  <link rel="shortcut icon" type="image/x-icon" href="app-assets/images/ico/favicon.ico">
  <link rel="shortcut icon" type="image/png" href="app-assets/images/ico/favicon-32.png">

  <!-- BEGIN VENDOR CSS-->
  <link rel="stylesheet" type="text/css" href="app-assets/css-rtl/bootstrap.css">
  <!-- font icons-->
  <link rel="stylesheet" type="text/css" href="app-assets/fonts/icomoon.css">
  <link rel="stylesheet" type="text/css" href="app-assets/fonts/flag-icon-css/css/flag-icon.min.css">
  <link rel="stylesheet" type="text/css" href="app-assets/css/tools.css">
  <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/extensions/pace.css">
  <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/jsgrid/jsgrid-theme.min.css">
  <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/jsgrid/jsgrid.min.css">
  <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/extensions/sweetalert.css">
  <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/extensions/toastr.css">

  <!-- END VENDOR CSS-->
  <!-- BEGIN ROBUST CSS-->
  <link rel="stylesheet" type="text/css" href="app-assets/css-rtl/bootstrap-extended.css">
  <link rel="stylesheet" type="text/css" href="app-assets/css-rtl/app.css">
  <link rel="stylesheet" type="text/css" href="app-assets/css-rtl/colors.css">
  <link rel="stylesheet" type="text/css" href="app-assets/css-rtl/custom-rtl.css">
  <script src="app-assets/js/scripts/tools.js" type="text/javascript"></script>

  <!-- END ROBUST CSS-->
  <!-- BEGIN Page Level CSS-->
  <link rel="stylesheet" type="text/css" href="app-assets/css-rtl/core/menu/menu-types/vertical-menu.css">
  <link rel="stylesheet" type="text/css" href="app-assets/css-rtl/core/menu/menu-types/vertical-overlay-menu.css">
  <!-- END Page Level CSS-->
  <!-- BEGIN Custom CSS-->
  <link rel="stylesheet" type="text/css" href="assets/css/style-rtl.css">

  <!-- END Custom CSS-->
</head>
<body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="vertical-layout vertical-menu 2-columns  fixed-navbar">

<!-- navbar-fixed-top-->
<jsp:directive.include file="inc/topmenu.jsp"/>

<div class="fullscreen-search-overlay"></div>

<!-- ////////////////////////////////////////////////////////////////////////////-->


<!-- main menu-->
<div data-scroll-to-active="true" class="main-menu menu-fixed menu-dark menu-accordion menu-shadow">
  <!-- main menu header-->
  <!-- / main menu header-->
  <!-- main menu content-->

  <!--Menu -->
  <jsp:directive.include file="inc/menu.jsp"/>
  <!--Menu -->

  <!-- /main menu content-->
</div>
<!-- / main menu-->

<div class="app-content content container-fluid">
  <div class="content-wrapper">
    <div class="content-header row">
    </div>
    <div class="content-body">
      <!-- Sales stats -->


      <section id="basic">

        <div class="row">
          <div class="col-xs-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">لیست كاربران</h4>
                <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>

                <div class="heading-elements">
                  <ul class="list-inline mb-0">
                    <li>

                    </li>
                    <li><a data-action="expand"><i class="icon-expand2"></i></a></li>

                  </ul>
                </div>
              </div>
              <div class="card-body collapse in">
                <div class="card-block card-dashboard ">
                  <%--<p>جدول با فیلتر با تمام قابلیت ها به جز صفحه بندی</p>--%>
                    <div class="modal fade text-xs-left" id="insert_user_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <%--<button type="button" class="close" data-dismiss="modal" aria-label="Close">--%>
                            <%--<span aria-hidden="true">&times;</span>--%>
                            <%--</button>--%>
                            <h4 class="modal-title" id="myModalLabel1">ورود اطلاعات</h4>
                          </div>
                          <div class="modal-body">
                            <div class="row">
                              <div class="col-md-3">
                                <div class="form-group">
                                  <label for="org_code">نام كاربری :<span class="danger">*</span>
                                  </label>
                                  <input type="text" class="form-control " id="userName" name="code_req" >
                                </div>
                              </div>
                              <div class="col-md-2">
                                <div class="form-group">
                                  <label>كد كاربر :<span class="danger">*</span>
                                  </label>
                                  <input type="text" class="form-control " id="codeuser" name="code_req" >

                                </div>
                              </div>
                              <div class="col-md-2">
                                <%--<div class="form-group">--%>
                                  <%--<label for="org_code">نوع كاربر انسانی :<span class="danger">*</span>--%>
                                  <%--</label>--%>
                                  <%--<select type="" class="custom-select form-control " id="org_code" name="code_req" >--%>
                                    <%--<option>فردی</option>--%>
                                    <%--<option>سازمانی</option>--%>
                                  <%--</select>--%>
                                <%--</div>--%>
                              </div>
                              <div class="col-md-2">
                                <%--<div class="form-group">--%>
                                  <%--<label for="org_code">واحد سازمانی :<span class="danger">*</span>--%>
                                  <%--</label>--%>
                                  <%--<select type="radio" class="custom-select form-control" id="org_code" name="code_req" >--%>
                                    <%--<option>فردی</option>--%>
                                    <%--<option>سازمانی</option>--%>
                                  <%--</select>--%>
                                <%--</div>--%>
                              </div>
                              <div class="col-md-3"></div>

                            </div>
                           <span class="tag tag-success">اطلاعات پرسنلی</span>

                            <div class="dropdown-divider" style="background-color: #1b7e5a"> </div>
                            <div class="row">
                              <div class="col-md-3">
                                <div class="form-group">
                                  <label for="org_code">نام :<span class="danger">*</span>
                                  </label>
                                  <input type="text" class="form-control " id="firstName" name="code_req" >
                                </div>
                              </div>
                              <div class="col-md-3">
                                <div class="form-group">
                                  <label for="org_code">نام خانوادگی :<span class="danger">*</span>
                                  </label>
                                  <input type="text" class="form-control " id="lastName" name="code_req" >
                                </div>
                              </div>
                              <div class="col-md-3">
                                <div class="form-group">

                                  <div class="form-group">
                                    <label for="org_code">شعبه :<span class="danger">*</span>
                                    </label>
                                    <select type="" class="custom-select form-control " id="branchId" name="code_req" >

                                    </select>
                                  </div>
                                </div>
                              </div>
                              <div class="col-md-3">
                                <%--<div class="form-group">--%>
                                  <%--<label for="org_code">كد پرسنلی :<span class="danger">*</span>--%>
                                  <%--</label>--%>
                                  <%--<input type="text" class="form-control " id="org_code" name="code_req" >--%>
                                <%--</div>--%>
                                  <div class="form-group">

                                    <div class="form-group">
                                      <label for="org_code">نقش كاربری :<span class="danger">*</span>
                                      </label>
                                      <select type="" class="custom-select form-control " id="roleId" name="code_req" >

                                      </select>
                                    </div>
                                  </div>
                              </div>

                            </div>
                            <span class="tag tag-success"> روش احراز هویت</span>


                            <div class="dropdown-divider" style="background-color: #1b7e5a"> </div>
                            <div class="row">
                              <div class="col-md-3">
                                <div class="form-group">
                                  <label for="org_code">رمز عبور :<span class="danger">*</span>
                                  </label>
                                  <input type="password" class="form-control " id="password" name="code_req" >
                                </div>
                              </div>
                              <div class="col-md-3">
                                <div class="form-group">
                                  <label for="org_code">تكرار رمز عبور :<span class="danger">*</span>
                                  </label>
                                  <input type="password" class="form-control " id="password_t" name="code_req" >
                                </div>
                              </div>
                              <div class="col-md-3">
                                <%--<div class="form-group">--%>
                                  <%--<label for="org_code">از ip :<span class="danger">*</span>--%>
                                  <%--</label>--%>
                                  <%--<input type="text" class="form-control " id="org_code" name="code_req" >--%>
                                <%--</div>--%>
                              </div>
                              <div class="col-md-3">
                                <%--<div class="form-group">--%>
                                  <%--<label for="org_code">تا ip:<span class="danger">*</span>--%>
                                  <%--</label>--%>
                                  <%--<input type="text" class="form-control " id="org_code" name="code_req" >--%>
                                <%--</div>--%>
                              </div>
                            </div>
                            <span class="tag tag-success">اعتبار كاربر</span>
                            <div class="dropdown-divider" style="background-color: #1b7e5a"> </div>
                            <div class="row">
                              <div class="col-md-2">
                                <div class="form-group">
                                  <label for="active">وضعیت كاربر :<span class="danger"></span>
                                  </label>
                                  <select class="form-control custom-select" id="active" name="code_req" >
                                    <option value="0" >فعال</option>
                                    <option value="1">غیر فعال</option>
                                  </select>
                                </div>
                              </div>

                            </div>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn  btn-success" id="insert_user" >ذخیره</button>
                            <button type="button" class="btn grey btn-outline-secondary" id="close_modal" data-dismiss="modal">بستن</button>
                            <%--<button type="button" class="btn btn-outline-primary">Save changes</button>--%>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-12">
                        <button class="btn btn-success" id="insert_user_btn"><i class="icon-plus" style="color: white"></i> ثبت كاربر</button>
                      </div>
                    </div>
                    <br>
                      <div  id="usermanager"></div>


                </div>
              </div>
            </div>
          </div>


        </div>
      </section>

      <!--/ most selling products-->

    </div>
  </div>
</div>
<!-- ////////////////////////////////////////////////////////////////////////////-->



<!-- BEGIN VENDOR JS-->
<script src="app-assets/js/core/libraries/jquery.js" type="text/javascript"></script>
<script src="app-assets/vendors/js/ui/tether.min.js" type="text/javascript"></script>
<script src="app-assets/js/core/libraries/bootstrap.min.js" type="text/javascript"></script>
<script src="app-assets/vendors/js/ui/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
<script src="app-assets/vendors/js/ui/unison.min.js" type="text/javascript"></script>
<script src="app-assets/vendors/js/ui/blockUI.min.js" type="text/javascript"></script>
<script src="app-assets/vendors/js/ui/jquery.matchHeight-min.js" type="text/javascript"></script>
<script src="app-assets/vendors/js/ui/jquery-sliding-menu.js" type="text/javascript"></script>
<script src="app-assets/vendors/js/sliders/slick/slick.min.js" type="text/javascript"></script>
<script src="app-assets/vendors/js/ui/screenfull.min.js" type="text/javascript"></script>
<script src="app-assets/vendors/js/extensions/pace.min.js" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
<script src="app-assets/vendors/js/modal/sweetalert.min.js" type="text/javascript"></script>
<script src="app-assets/vendors/js/extensions/sweetalert.min.js" type="text/javascript"></script>
<script src="app-assets/js/scripts/extensions/sweet-alerts.js" type="text/javascript"></script>
<script src="app-assets/js/scripts/extensions/toastr.min.js" type="text/javascript"></script>
<script src="app-assets/vendors/js/extensions/toastr.min.js" type="text/javascript"></script>
<script src="app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="app-assets/vendors/js/tables/jsgrid/jsgrid.min.js" type="text/javascript"></script>
<script src="app-assets/vendors/js/tables/jsgrid/jquery.validate.min.js" type="text/javascript"></script>
<!-- END PAGE VENDOR JS-->
<!-- BEGIN ROBUST JS-->
<script src="app-assets/js/core/app-menu.js" type="text/javascript"></script>
<script src="app-assets/js/core/app.js" type="text/javascript"></script>

<!-- END ROBUST JS-->


<script type="text/javascript" src="app-assets/js/scripts/ui/breadcrumbs-with-stats.js"></script>
<script src="app-assets/js/scripts/tables/jsgrid/jsgrid.js" type="text/javascript"></script>
<script src="app-assets/vendors/js/tables/jsgrid/ListUser.js" type="text/javascript"></script>
<script src="app-assets/js/scripts/sessions/jquery.session.js" type="text/javascript"></script>
<script src="app-assets/js/scripts/tools.js" type="text/javascript"></script>


<script type="text/javascript">

$('#insert_user_btn').click(function () {
    $('.form-control').val('');

    $('#insert_user_modal').modal({
              show:true,
              backdrop:"static",
              keyboard:true,
                                  })
    $('#active').val('0')

})


  $('#insert_user').click(function () {
      if( $('#active').val() == 0){
          status_user = true
      }
      else {
          status_user = false
      }
      row = {
          userId:null,
          firstName:$('#firstName').val(),
          userCode:$('#codeuser').val(),
          lastName:$('#lastName').val(),
          userName:$('#userName').val(),
          password:$('#password').val(),
          roleId:$('#roleId').val(),
          branchId:$('#branchId').val(),
          active:status_user
      }

      var pass = !(/(?=^.{8,}$)(?=.*\d)(?=.*[!@#$%^&*]+)(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/.test(row.password));
      if(pass){
          // alert("غلط")

          swal({
              title: "",
              text: "كلمه عبور(حداقل 8 كاراكتر, حداقل يك حرف بزرگ, حداقل یک حرف کوچک, حداقل يك عدد, حداقل يك كاراكتر خاص ('_@#$%*~!'))",
              confirmButtonText: "تایید",
              confirmButtonColor: "#DA4453",
              closeOnConfirm: true,
              closeOnCancel: false
          });
      }else {
          // alert("درست")

      if(!row.userName){
          sweet_ALert("نام كاربری الزامی می باشد")


      }else if(!row.userCode){
          sweet_ALert("كد كاربری الزامی می باشد")

      }else if(!row.firstName){
          sweet_ALert("نام الزامی می باشد")

      }else if(!row.lastName){
          sweet_ALert("نام خانوادگی الزامی می باشد")

      }else if(!row.password){
          sweet_ALert("كلمه عبور الزامی می باشد")

      }else if(!$('#password').val()){
          sweet_ALert("تكرار كلمه عبور الزامی می باشد")

      }else if($('#password').val() != $('#password_t').val() ){
          sweet_ALert("كلمه عبور یكسان نمی باشد")

      }else{

          $.ajax({
                     type: "PUT",
                     dataType: 'json',
                     contentType : "application/json",
                     data:JSON.stringify(row),
                    url: "api/user",
                     success: function (result) {
                         if(result.status=="SUCCESS"){

                             swal({
                                      title: "",
                                      text: result.description,
                                      type: "info",
                                      // showCancelButton: true,
                                      confirmButtonText: "تایید",
                                      // cancelButtonText: "No, cancel plx!",
                                      closeOnConfirm: false,
                                      closeOnCancel: false
                                  }, function(isConfirm) {
                                 if (isConfirm) {
                                     // swal("Changed!", "Confirm button text was changed!!", "success");
                                     location.reload()
                                 } else {
                                     // swal("Cancelled", "It's safe.", "error");
                                 }
                             });
                         }
                         else {
                             sweet_ALert(result.description)
                         }
                     },
                     error:function (result, status,xhr) {
                         swal({
                                  title:'<small>'+result.responseJSON.description+'</small>',
                                  confirmButtonColor: "#DA4453",
                                  text: result.responseJSON.data[0].message,
                                  confirmButtonText: "تایید",
                                  html: true
                              });
                     },
                     async: false

                 });

      }

      }

  })

</script>
</body>
</html>
