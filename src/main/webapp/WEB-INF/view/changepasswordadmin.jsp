<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8" %>
<html lang="en" data-textdirection="rtl" class="loading">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>مدیریت پایانه های فروش</title>

  <link rel="shortcut icon" type="image/x-icon" href="app-assets/images/ico/favicon.ico">
  <link rel="shortcut icon" type="image/png" href="app-assets/images/ico/favicon-32.png">

  <!-- BEGIN VENDOR CSS-->
  <link rel="stylesheet" type="text/css" href="app-assets/css-rtl/bootstrap.css">
  <!-- font icons-->
  <link rel="stylesheet" type="text/css" href="app-assets/fonts/icomoon.css">
  <link rel="stylesheet" type="text/css" href="app-assets/fonts/flag-icon-css/css/flag-icon.min.css">
  <link rel="stylesheet" type="text/css" href="app-assets/css/tools.css">
  <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/extensions/pace.css">
  <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/jsgrid/jsgrid-theme.min.css">
  <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/jsgrid/jsgrid.min.css">
  <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/extensions/sweetalert.css">

  <!-- END VENDOR CSS-->
  <!-- BEGIN ROBUST CSS-->
  <link rel="stylesheet" type="text/css" href="app-assets/css-rtl/bootstrap-extended.css">
  <link rel="stylesheet" type="text/css" href="app-assets/css-rtl/app.css">
  <link rel="stylesheet" type="text/css" href="app-assets/css-rtl/colors.css">
  <link rel="stylesheet" type="text/css" href="app-assets/css-rtl/custom-rtl.css">
  <script src="app-assets/js/scripts/tools.js" type="text/javascript"></script>

  <!-- END ROBUST CSS-->
  <!-- BEGIN Page Level CSS-->
  <link rel="stylesheet" type="text/css" href="app-assets/css-rtl/core/menu/menu-types/vertical-menu.css">
  <link rel="stylesheet" type="text/css" href="app-assets/css-rtl/core/menu/menu-types/vertical-overlay-menu.css">
  <!-- END Page Level CSS-->
  <!-- BEGIN Custom CSS-->
  <link rel="stylesheet" type="text/css" href="assets/css/style-rtl.css">

  <!-- END Custom CSS-->

</head>
<body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="vertical-layout vertical-menu 2-columns  fixed-navbar">

<!-- navbar-fixed-top-->
<jsp:directive.include file="inc/topmenu.jsp"/>

<div class="fullscreen-search-overlay"></div>

<!-- ////////////////////////////////////////////////////////////////////////////-->


<!-- main menu-->
<div data-scroll-to-active="true" class="main-menu menu-fixed menu-dark menu-accordion menu-shadow">
  <!-- main menu header-->
  <!-- / main menu header-->
  <!-- main menu content-->

  <!--Menu -->
  <jsp:directive.include file="inc/menu.jsp"/>
  <!--Menu -->

  <!-- /main menu content-->
</div>
<!-- / main menu-->

<div class="app-content content container-fluid">
  <div class="content-wrapper">
    <div class="content-header row">
    </div>
    <div class="content-body">
      <!-- Sales stats -->


      <section id="basic">

        <div class="row">
          <div class="col-xs-3"></div>
          <div class="col-xs-6">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">تغییر كلمه عبور</h4>
                <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                <div class="heading-elements">
                  <ul class="list-inline mb-0">
                    <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                  </ul>
                </div>
              </div>
              <div class="card-body collapse in">
                <div class="card-block card-dashboard ">
                  <%--<p>جدول با فیلتر با تمام قابلیت ها به جز صفحه بندی</p>--%>
                    <div class="row">
                      <div class="col-md-12">
                      <div class="form-group">
                      <label >
                     كلمه عبور جدید:
                      </label>
                      <input type="password" class="form-control" id="np" name="" >

                      </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                      <div class="form-group">
                      <label >
                      تكرار كلمه عبور جدید :
                      </label>
                        <input type="password" class="form-control" id="rnp" name="" >

                      </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <button class="btn btn-success" id="changepassword">ذخیره</button>
                      </div>
                    </div>

                </div>
              </div>
            </div>
          </div>
          <div class="col-xs-3"></div>
        </div>
      </section>

      <!--/ most selling products-->

    </div>
  </div>
</div>
<!-- ////////////////////////////////////////////////////////////////////////////-->

<script type="text/javascript">
    userid = localStorage.getItem("userId");


</script>

<!-- BEGIN VENDOR JS-->
<script src="app-assets/js/core/libraries/jquery.js" type="text/javascript"></script>
<script src="app-assets/vendors/js/ui/tether.min.js" type="text/javascript"></script>
<script src="app-assets/js/core/libraries/bootstrap.min.js" type="text/javascript"></script>
<script src="app-assets/vendors/js/ui/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
<script src="app-assets/vendors/js/ui/unison.min.js" type="text/javascript"></script>
<script src="app-assets/vendors/js/ui/blockUI.min.js" type="text/javascript"></script>
<script src="app-assets/vendors/js/ui/jquery.matchHeight-min.js" type="text/javascript"></script>
<script src="app-assets/vendors/js/ui/jquery-sliding-menu.js" type="text/javascript"></script>
<script src="app-assets/vendors/js/sliders/slick/slick.min.js" type="text/javascript"></script>
<script src="app-assets/vendors/js/ui/screenfull.min.js" type="text/javascript"></script>
<script src="app-assets/vendors/js/extensions/pace.min.js" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
<script src="app-assets/vendors/js/modal/sweetalert.min.js" type="text/javascript"></script>
<script src="app-assets/vendors/js/extensions/sweetalert.min.js" type="text/javascript"></script>
<script src="app-assets/js/scripts/extensions/sweet-alerts.js" type="text/javascript"></script>
<script src="app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="app-assets/vendors/js/tables/jsgrid/jsgrid.min.js" type="text/javascript"></script>
<script src="app-assets/vendors/js/tables/jsgrid/jquery.validate.min.js" type="text/javascript"></script>
<!-- END PAGE VENDOR JS-->
<!-- BEGIN ROBUST JS-->
<script src="app-assets/js/core/app-menu.js" type="text/javascript"></script>
<script src="app-assets/js/core/app.js" type="text/javascript"></script>

<!-- END ROBUST JS-->


<script type="text/javascript" src="app-assets/js/scripts/ui/breadcrumbs-with-stats.js"></script>
<script src="app-assets/js/scripts/tables/jsgrid/jsgrid.js" type="text/javascript"></script>
<script src="app-assets/js/scripts/sessions/jquery.session.js" type="text/javascript"></script>
<script src="app-assets/js/scripts/tools.js" type="text/javascript"></script>

<script type="text/javascript">

var url = new URL(window.location);
    var id = url.searchParams.get("id");
    $('#changepassword').click(function () {

  userdata = {
    userId:id,
    oldPassword :null,
      newPassword:$('#np').val(),
    rPassword:$('#rnp').val(),
    }
    if(userdata.rPassword != userdata.newPassword){
        swal({
            title: "",
            text: "تكرار كلمه عبور نادرست می باشد",
            confirmButtonText: "تایید",
            confirmButtonColor: "#DA4453",
            closeOnConfirm: true,
            closeOnCancel: false
        });
    }
    else
    {
        var pass = !(/(?=^.{8,}$)(?=.*\d)(?=.*[!@#$%^&*]+)(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/.test(userdata.newPassword));

        if(pass){
            // alert("غلط")

            swal({
                title: "",
                text: "كلمه عبور(حداقل 8 كاراكتر, حداقل يك حرف بزرگ, حداقل یک حرف کوچک, حداقل يك عدد, حداقل يك كاراكتر خاص ('_@#$%*~!'))",
                confirmButtonText: "تایید",
                confirmButtonColor: "#DA4453",
                closeOnConfirm: true,
                closeOnCancel: false
            });
        }else {
            // alert("درست")


            $.ajax({
            type: "POST",
            data: JSON.stringify(userdata),
            dataType: 'json',
            contentType: "application/json",
           url: "api/user/change/password/admin",
            success: function (result, status, xhr)
            {
                if(result.status=="SUCCESS"){

                    swal({
                             title: "",
                             text: result.description,
                             type: "info",
                             // showCancelButton: true,
                             confirmButtonText: "تایید",
                             // cancelButtonText: "No, cancel plx!",
                             closeOnConfirm: false,
                             closeOnCancel: false
                         }, function(isConfirm) {
                        if (isConfirm) {
                            // swal("Changed!", "Confirm button text was changed!!", "success");
                            window.location = "usermanager" ;
                        } else {
                            // swal("Cancelled", "It's safe.", "error");
                        }
                    });
                    }

            },
            error:function (result, status) {

                swal({
                    title:'<small>'+result.responseJSON.description+'</small>',
                    confirmButtonColor: "#DA4453",
                    text: result.responseJSON.data[0].message,
                    confirmButtonText: "تایید",
                    html: true
                });



            },
        });
    }
    }
    })

</script>


</body>
</html>
