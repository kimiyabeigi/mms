<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html lang="en" data-textdirection="rtl" class="loading">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>بانك كار آفرین</title>

    <link rel="shortcut icon" type="image/x-icon" href="app-assets/images/ico/favicon.ico">
    <link rel="shortcut icon" type="image/png" href="app-assets/images/ico/favicon-32.png">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <!-- BEGIN VENDOR CSS-->
    <jsp:directive.include file="css.jsp"/>
    <!-- END Custom CSS-->
</head>
<body data-open="click" data-menu="vertical-menu" data-col="1-column" class="vertical-layout vertical-menu 1-column  blank-page blank-page" >
<!-- ////////////////////////////////////////////////////////////////////////////-->
<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <sec:if test="${param.error ne null}">

            <div class="alert alert-danger no-border" style="text-align: center">
                <sec:set var = "errorMessage" value = "${SPRING_SECURITY_LAST_EXCEPTION.message}"/>
                <sec:if test = "${fn:containsIgnoreCase(errorMessage, 'Bad credentials')}">
                    <div>نام كاربری یا كلمه عبور صحیح نمی باشد</div>
                </sec:if>

                <sec:if test = "${fn:containsIgnoreCase(errorMessage, 'UserDetailsService returned null')}">
                    <div>نام كاربری یا كلمه عبور صحیح نمی باشد</div>
                </sec:if>

                 <sec:if test = "${fn:containsIgnoreCase(errorMessage, 'User is disabled')}">
                     <div>امكان ورود به سیستم برای این نام كاربری از این شعبه مقدور نمی باشد
                     </div>
                 </sec:if>
            </div>
        </sec:if>
        <div class="content-body">
            <section class="flexbox-container">
            <div class="col-md-4 offset-md-4 col-xs-10 offset-xs-1  box-shadow-2 p-0">
                <div class="card border-grey border-lighten-3 m-0">
                    <div class="card-header no-border">
                        <div class="card-title text-xs-center">
                            <div class="p-1"><img src="app-assets/images/logo/eliteadmin-logo-dark.png" width="200px"></div>
                        </div>
                        <h6 class="card-subtitle line-on-side text-muted text-xs-center font-small-3 pt-2"><span>سامانه مدیریت پایانه های فروش</span></h6>
                    </div>
                    <form action="${pageContext.request.contextPath}/login" method="post">
                        <div class="card-body collapse in">
                            <div class="card-block">
                                <fieldset class="form-group position-relative has-icon-left mb-0" style="padding-bottom: 10px;">
                                    <input type="text" class="form-control form-control-lg input-lg" style="font-family: Vazir" name="username" oninvalid="this.setCustomValidity('نام كاربری خالی می باشد')" id="username" placeholder="نام كاربری"  required="">
                                    <div class="form-control-position">
                                        <i class="icon-head"></i>
                                    </div>
                                </fieldset>
                                <fieldset class="form-group position-relative has-icon-left">
                                    <input type="password" class="form-control form-control-lg input-lg" style="font-family: Vazir" name="password" oninvalid="this.setCustomValidity('كلمه عبور خالی می باشد')" id="password" placeholder="كلمه عبور" required="">
                                    <div class="form-control-position">
                                        <i class="icon-key3"></i>
                                    </div>
                                </fieldset>
                                <fieldset class="form-group row">
                                    <div class="col-md-6 col-xs-12 text-xs-center text-md-left">
                                        <%--<fieldset>--%>
                                        <%--<input type="checkbox" id="remember-me" class="chk-remember">--%>
                                        <%--<label for="remember-me"> Remember Me</label>--%>
                                        <%--</fieldset>--%>
                                    </div>
                                    <%--<div class="col-md-6 col-xs-12 text-xs-center text-md-right"><a href="recover-password.html" class="card-link">Forgot Password?</a></div>--%>
                                </fieldset>



                                <sec:if test="${param.logout ne null}">
                                    <div>you have benn logged out</div>
                                </sec:if>

                                <button id="submit_ok" type="submit" class="btn btn-primary btn-lg btn-block" style="background-color: #006614;border-color: #006614" id="html-alert"><i class="icon-unlock2"></i> ورود به سیستم</button>
                            </div>
                        </div>
                        <%--<input type="hidden" name="${_csf.parameterName}" value="${_csf.token}" />--%>
                    </form>
                    <div class="card-footer">
                        <div class="center"  style="text-align: center;">
                            <p class="float-sm-center text-xs-center m-0" >نسخه : 1.2.2</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        </div>
    </div>
</div>
<!-- ////////////////////////////////////////////////////////////////////////////-->

<!-- BEGIN VENDOR JS-->
<script src="app-assets/js/core/libraries/jquery.min.js" type="text/javascript"></script>
<script src="app-assets/vendors/js/ui/tether.min.js" type="text/javascript"></script>
<script src="app-assets/js/core/libraries/bootstrap.min.js" type="text/javascript"></script>
<script src="app-assets/vendors/js/ui/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
<script src="app-assets/vendors/js/ui/unison.min.js" type="text/javascript"></script>
<script src="app-assets/vendors/js/ui/blockUI.min.js" type="text/javascript"></script>
<script src="app-assets/vendors/js/ui/jquery.matchHeight-min.js" type="text/javascript"></script>
<script src="app-assets/vendors/js/ui/jquery-sliding-menu.js" type="text/javascript"></script>
<script src="app-assets/vendors/js/sliders/slick/slick.min.js" type="text/javascript"></script>
<script src="app-assets/vendors/js/ui/screenfull.min.js" type="text/javascript"></script>
<script src="app-assets/vendors/js/extensions/pace.min.js" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
<script src="app-assets/vendors/js/forms/icheck/icheck.min.js" type="text/javascript"></script>
<script src="app-assets/vendors/js/forms/validation/jqBootstrapValidation.js" type="text/javascript"></script>
<!-- END PAGE VENDOR JS-->
<!-- BEGIN ROBUST JS-->
<script src="app-assets/js/core/app-menu.js" type="text/javascript"></script>
<script src="app-assets/js/core/app.js" type="text/javascript"></script>
<script src="app-assets/js/scripts/ui/fullscreenSearch.js" type="text/javascript"></script>
<script src="app-assets/vendors/js/extensions/sweetalert.min.js" type="text/javascript"></script>
<script src="app-assets/js/scripts/extensions/sweet-alerts.js" type="text/javascript"></script>

<!-- END ROBUST JS-->
<!-- BEGIN PAGE LEVEL JS-->
<script src="app-assets/js/scripts/forms/form-login-register.js" type="text/javascript"></script>
<!-- END PAGE LEVEL JS-->
</body>

<%--
<script>
    $('#submit_ok').click(function () {
        row = {
            username : $('#username').val(),
            password : $('#password').val()
        }
        $.ajax({
            type: "POST",
            dataType: 'json',
            contentType : "application/json",
            data:JSON.stringify(row),
           url: "/login",

            success:function(response) {

                window.location("/ListCompany")

            },
        });
    })

</script>
--%>

</html>
