<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8" %>
<html lang="en" data-textdirection="rtl" class="loading">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <title>مدیریت پایانه های فروش</title>
  <link rel="shortcut icon" type="image/x-icon" href="app-assets/images/ico/favicon.ico">
  <link rel="shortcut icon" type="image/png" href="app-assets/images/ico/favicon-32.png">

  <!-- BEGIN VENDOR CSS-->

  <!-- END Custom CSS-->  <link rel="stylesheet" type="text/css" href="app-assets/css-rtl/bootstrap.css">
  <!-- font icons-->
  <link rel="stylesheet" type="text/css" href="app-assets/fonts/icomoon.css">
  <link rel="stylesheet" type="text/css" href="app-assets/fonts/flag-icon-css/css/flag-icon.min.css">
  <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/sliders/slick/slick.css">
  <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/extensions/pace.css">
  <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/jsgrid/jsgrid-theme.min.css">
  <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/jsgrid/jsgrid.min.css">
  <link rel="stylesheet" type="text/css" href="app-assets/css/tools.css">
  <!-- END VENDOR CSS-->
  <!-- BEGIN ROBUST CSS-->
  <link rel="stylesheet" type="text/css" href="app-assets/css-rtl/bootstrap-extended.css">
  <link rel="stylesheet" type="text/css" href="app-assets/css-rtl/app.css">
  <link rel="stylesheet" type="text/css" href="app-assets/css-rtl/colors.css">
  <link rel="stylesheet" type="text/css" href="app-assets/css-rtl/custom-rtl.css">
  <!-- END ROBUST CSS-->
  <!-- BEGIN Page Level CSS-->
  <link rel="stylesheet" type="text/css" href="app-assets/css-rtl/core/menu/menu-types/vertical-menu.css">
  <link rel="stylesheet" type="text/css" href="app-assets/css-rtl/core/menu/menu-types/vertical-overlay-menu.css">
  <link rel="stylesheet" type="text/css" href="app-assets/css-rtl/plugins/forms/wizard.css">
  <!-- END Page Level CSS-->
  <!-- BEGIN Custom CSS-->
  <link rel="stylesheet" type="text/css" href="assets/css/style-rtl.css">

</head>
<body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="vertical-layout vertical-menu 2-columns  fixed-navbar">

<!-- navbar-fixed-top-->
<jsp:directive.include file="inc/topmenu.jsp"/>

<div class="fullscreen-search-overlay"></div>

<!-- ////////////////////////////////////////////////////////////////////////////-->

<!-- main menu-->
<div data-scroll-to-active="true" class="main-menu menu-fixed menu-dark menu-accordion menu-shadow">
  <!-- main menu header-->
  <!-- / main menu header-->
  <!-- main menu content-->

  <!--Menu -->
  <jsp:directive.include file="inc/menu.jsp"/>
  <!--Menu -->

  <!-- /main menu content-->
</div>
<!-- / main menu-->
<!-- main menu content-->
<!-- /main menu content-->
<!-- main menu footer-->

<!-- / main menu-->





<div class="app-content content container-fluid">
  <div class="content-wrapper">

    <div class="content-body">


      <!-- Modal -->
      <div class="modal fade text-xs-left" id="modal_default" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" id="close_modal" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <h4 class="modal-title" id="myModalLabel1">تنظیمات شركت</h4>
            </div>
            <div class="modal-body">
              <section id="validation">
                <div class="row" style="margin-top: 3px;">
                  <div class="col-xs-12">
                    <div class="card">
                      <div class="card-body collapse in">
                        <div class="card-block">
                          <form  class="steps-validation wizard-circle">
                            <!-- Step 1 -->
                            <h6>تنظیمات كلی</h6>
                            <fieldset>

                              <div class="row">
                                <div class="col-md-4">
                                  <div class="form-group">
                                  <label >
           نام شركت :
                                    <span class="danger">*</span>
                                  </label>
                                  <select  class="custom-select form-control" id="itemoption_company" name="itemoption_company">
                                    <option value="-1">--شركت را انتخاب نمایید--</option>
                                  </select>
                                </div>
                                </div>



                                <div class="col-md-4">
                                  <div class="form-group">
                                    <label for="org_code">
                                      كد سازمان درخواست دهنده :                              <span class="danger"></span>
                                    </label>
                                    <input type="text" class="form-control " id="org_code" name="code_req" >
                                  </div>
                                </div>
                                <div class="col-md-4">
                                  <div class="form-group">
                                    <label for="checkbox1" style="margin-top: 31px;">
                                      فعال :
                                      <span class="danger"></span>
                                    </label>
                                    <label class="inline custom-control custom-checkbox block">
                                    <input type="checkbox" class="custom-control-input" id="checkbox1" name="companyname" checked="checked" style="margin-right: -91px;margin-top: 11px;" >
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description ml-0"></span>
                                    </label>
                                  </div>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <label>انواع درخواست :</label>
                                    <div class=" c-inputs-stacked" id="itemcheckbox_company" style="direction: ltr">
                                    </div>
                                  </div>
                                  <button class="btn btn-success" id="save_config" >ذخیره</button>

                                </div>
                              </div>
                            </fieldset>
                            <!-- Step 2 -->
                            <h6>تنظیمات خدمت</h6>
                            <fieldset>
                              <div class="row" style="margin-top: 3px">
                                <div id="ConfigService" style="padding-bottom: 15px"></div>

                                <%--<div class="col-md-2">--%>
                                <%--كد خدمت :--%>
                                <%--</div>--%>
                                <%--<div class="col-md-2">--%>
                                <%--<div class="form-group">--%>
                                <%--<select class="custom-select form-control required" id="" name="eventType">--%>
                                <%--<option value="PSPIPG">PSPIPG</option>--%>
                                <%--<option value="POS">POS</option>--%>
                                <%--</select>--%>
                                <%--</div>--%>
                                <%--</div>--%>
                                <%--<div class="col-md-2">--%>
                                <%--نام خدمت:--%>
                                <%--</div>--%>
                                <%--<div class="col-md-2">--%>
                                <%--<div class="form-group">--%>
                                <%--<input type="text" class="form-control required" id="jobTitle5" name="jobTitle" >--%>
                                <%--</div>--%>
                                <%--</div>--%>
                                <%--<div class="col-md-2">--%>
                                <%--<div class="form-group">--%>

                                <%--فعال&nbsp;&nbsp;--%>
                                <%--<label class="inline custom-control custom-checkbox block">--%>
                                <%--<input type="checkbox" class="custom-control-input" value="PSPIPG">--%>
                                <%--<span class="custom-control-indicator"></span>--%>
                                <%--<span class="custom-control-description ml-0"></span>--%>
                                <%--</label>--%>
                                <%--</div>--%>
                                <%--</div>--%>
                                <%--<div class="col-md-2">--%>
                                <%--<button class="btn btn-success">ذخیره</button>--%>
                                <%--</div>--%>
                              </div>
                            </fieldset>


                            <!-- Step 3 -->
                            <h6>تنظیمات پایانه</h6>
                            <fieldset>
                              <div class="row">
                                <div class="col-md-12">

                                  <div id="ConfigTerminal" style="padding-bottom: 15px"></div>
                                  <div class="alert alert-info"  id="ConfigTerminal_text" style="padding: 5px;text-align: center;margin: 20px" >تنظیمات پایانه برای شركت هایی فعال می شوند كه خدمات POS ارائه دهند.</div>


                                </div>
                              </div>
                            </fieldset>

                            <!-- Step 4 -->
                            <h6>تنظیمات كد وضعیت</h6>
                            <fieldset>
                              <div class="row">
                                <div class="col-md-12">
                                  <div id="ConfigStatus" style="padding-bottom: 15px"></div>
                                </div>
                              </div>
                            </fieldset>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>


            </div>
            <%--<div class="modal-footer">--%>
              <%--<button type="button" class="btn grey btn-outline-secondary" id="close_modal" data-dismiss="modal">بستن</button>--%>
              <%--<button type="button" class="btn btn-outline-primary">Save changes</button>--%>
            <%--</div>--%>
          </div>
        </div>
      </div>
      <!-- Form wizard with step validation section start -->
      <!-- Form wizard with step validation section end -->

      <!-- Form wizard with vertical tabs section start -->
      <!-- Form wizard with vertical tabs section end -->
      <div class="row">
        <div class="col-xs-12">
          <div class="card">
            <div class="card-header">
              <h4 class="card-title">تنظیمات شركت</h4>
              <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
              <div class="heading-elements">
                <ul class="list-inline mb-0">
                  <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                  <li><a data-action="reload"><i class="icon-reload"></i></a></li>
                  <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                  <li><a data-action="close"><i class="icon-cross2"></i></a></li>
                </ul>
              </div>
            </div>
            <div class="card-body collapse in">
              <div class="card-block card-dashboard">
                <div class="row">
                  <div class="col-md-2"><button type="button" class="btn btn-flickr"  id="modal_show">
                    <i class="icon-plus"></i>اضافه كردن
                  </button>
                  </div>
                  <div class="col-md-10"></div>
                </div>

              </div>
              <div class="table-responsive" style="padding: 10px;">
                <table class="table table-bordered">
                  <thead class="thead-inverse" >
                  <tr >

                    <th style="text-align: center">كد شركت</th>
                    <th style="text-align: center">نام شركت</th>
                    <th style="text-align: center">وضعیت</th>

                    <th style="text-align: center">عملیات</th>
                  </tr>
                  </thead>
                  <tbody id="companysetting">


                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>


    </div>
  </div>
</div>
<div id="checkbox-value" style="display: none"></div>
<!-- ////////////////////////////////////////////////////////////////////////////-->




<!-- BEGIN VENDOR JS-->
<script src="app-assets/js/core/libraries/jquery.min.js" type="text/javascript"></script>
<script src="app-assets/vendors/js/ui/tether.min.js" type="text/javascript"></script>
<script src="app-assets/js/core/libraries/bootstrap.min.js" type="text/javascript"></script>
<script src="app-assets/vendors/js/ui/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
<script src="app-assets/vendors/js/ui/unison.min.js" type="text/javascript"></script>
<script src="app-assets/vendors/js/ui/blockUI.min.js" type="text/javascript"></script>
<script src="app-assets/vendors/js/ui/jquery.matchHeight-min.js" type="text/javascript"></script>
<script src="app-assets/vendors/js/ui/jquery-sliding-menu.js" type="text/javascript"></script>
<script src="app-assets/vendors/js/sliders/slick/slick.min.js" type="text/javascript"></script>
<script src="app-assets/vendors/js/ui/screenfull.min.js" type="text/javascript"></script>
<script src="app-assets/vendors/js/extensions/pace.min.js" type="text/javascript"></script>
<script src="app-assets/vendors/js/tables/jsgrid/jsgrid.min.js" type="text/javascript"></script>
<script src="app-assets/vendors/js/forms/validation/jquery.validate.min.js" type="text/javascript"></script>
<script src="app-assets/js/scripts/sessions/jquery.session.js" type="text/javascript"></script>

<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
<script src="app-assets/vendors/js/extensions/jquery.steps.min.js" type="text/javascript"></script>
<!-- BEGIN ROBUST JS-->
<script src="app-assets/js/core/app-menu.js" type="text/javascript"></script>
<script src="app-assets/js/core/app.js" type="text/javascript"></script>
<script src="app-assets/js/scripts/ui/fullscreenSearch.js" type="text/javascript"></script>
<script src="app-assets/vendors/js/tables/jsgrid/ConfigStatus.js" type="text/javascript"></script>
<script src="app-assets/vendors/js/tables/jsgrid/ConfigTerminal.js" type="text/javascript"></script>
<script src="app-assets/vendors/js/tables/jsgrid/ConfigService.js" type="text/javascript"></script>
<script src="app-assets/vendors/js/tables/jsgrid/ManageCompany.js" type="text/javascript"></script>
<script src="app-assets/js/scripts/tables/jsgrid/jsgrid.js" type="text/javascript"></script>
<script src="app-assets/js/scripts/sessions/jquery.session.js" type="text/javascript"></script>
<script src="app-assets/js/scripts/tools.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="app-assets/vendors/css/extensions/sweetalert.css">

<!-- END ROBUST JS-->
<!-- BEGIN PAGE LEVEL JS-->
<script src="app-assets/js/scripts/forms/wizard-steps.js" type="text/javascript"></script>
<script src="app-assets/vendors/js/modal/sweetalert.min.js" type="text/javascript"></script>
<script src="app-assets/vendors/js/extensions/sweetalert.min.js" type="text/javascript"></script>
<script src="app-assets/js/scripts/extensions/sweet-alerts.js" type="text/javascript"></script>


<!-- END PAGE LEVEL JS-->
<script type="text/javascript">
//    $('select').on('change', function()
//    {
//        alert( this.value );
//    });
//
$('.actions').hide();

$('#modal_show').click(function () {
    $('#modal_default').modal('show');
    $.session.set("id_company_modal","N");
    $.session.set("status_form",'');

    $.session.set("modal_status","show");
    $('input').val('')
    $('input[type="checkbox"]').attr('checked', false);
    location.reload();


});

function formid(id) {

    $('input[type=checkbox]').attr('checked', false);
    $.session.set("id_company",id);
    $.session.set("modal_status","show");
    $.session.set("id_company_modal","T");
    $.ajax({
        type: "GET",
        dataType: 'json',
        contentType : "application/json",
       url: "api/company/setting/"+id,
        success: function (result,status, xhr){
            localStorage.setItem("rahiminia", JSON.stringify(result));
//            $.session.set("rahiminia",JSON.stringify(result));
//            $.session.set("rahiminia1",result.data.companyId);
//            $.session.set("rahiminia2",result.data.active);
//            alert(JSON.stringify(result))
//            alert(result.data.companySettingDetails[0].companySettingDetailId)
            $.session.set("companyCode",$('#companyCode'+id).text());
//            $('#itemoption_company').val(result.data.companyId);
//            $('#checkbox1').attr('checked', result.data.active);
//            $('#org_code').val($('#companyCode'+id).text());

            location.reload();
        },
    });
  }


function r12() {
//    $('#itemoption_company').val($.session.get("rahiminia1"));
//    $('#checkbox1').attr('checked', $.session.get("rahiminia2"));

    jsonstorage = JSON.parse(localStorage.getItem("rahiminia"))
    $('#org_code').val(jsonstorage.data.organizationCode);
      $('#itemoption_company').val(jsonstorage.data.companyId);
      $('#checkbox1').attr('checked', jsonstorage.data.active);
    i=0;

    while(i<jsonstorage.data.companySettingDetails.length){

        $('#terminal_id'+jsonstorage.data.companySettingDetails[i].terminalId).attr('checked', jsonstorage.data.companySettingDetails[i].selected);
        $('#terminal_id'+jsonstorage.data.companySettingDetails[i].terminalId).attr('companySettingDetailId', jsonstorage.data.companySettingDetails[i].companySettingDetailId);
        $('#terminal_id'+jsonstorage.data.companySettingDetails[i].terminalId).attr('companySettingId', jsonstorage.data.companySettingDetails[i].companySettingId);
        if( jsonstorage.data.companySettingDetails[i].terminalId == 1){

            if(jsonstorage.data.companySettingDetails[i].selected == true){

                $('#ConfigTerminal').show()
                $('#ConfigTerminal_text').hide()


            }else{
                $('#ConfigTerminal').hide()
                $('#ConfigTerminal_text').show()
            }

        }
        else{


        }
        i++;
    }


}
//
if($.session.get("id_company_modal") == "T" && $.session.get("modal_status") == "show"){
    $('#modal_default').modal('show');
    $('.actions').show();


    r12();

}else if($.session.get("modal_status") == "show"){
$('.wizard_req').hide();
    $('#modal_default').modal('show');
    $.session.set("modal_status", '');




}

$('#checkbox-value').text('false')
$('#checkbox-value').text($('#checkbox1').val());

$("#checkbox1").on('change', function() {
    if ($(this).is(':checked')) {
        $(this).attr('value', true);
    }
     else {
       $(this).attr('value', false);
    }

    $('#checkbox-value').text($('#checkbox1').val());
});


$('#close_modal').click(function () {
    $.session.set("modal_status", '');
    $.session.set("status_form",'');
    location.reload();
});
//$(".modal").on("click",function(event){
//    $.session.set("id_company_modal", '');
//});
</script>
</body>
</html>
