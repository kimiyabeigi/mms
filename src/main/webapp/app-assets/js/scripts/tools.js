/**
 * Created by r.rahiminia on 01/08/2018.
 */

function checkpassword(){

    $.ajax({
        type: "GET",
        dataType: 'json',
        contentType : "application/json",

       url: "api/user/current",
        success: function (result,status, xhr){

            $('#profile_user').html(result.data.firstName+'&nbsp'+result.data.lastName);
            userId = result.data.userId;
            localStorage.setItem("userId",userId);


            if(result.data.firstLogin ==  true ){

                window.location = "changepassword" ;

            }
            else{

                window.location ="index";

            }

        },
    });

}



$.ajax({
    type: "GET",
    dataType: 'json',
    contentType : "application/json",

   url: "api/user/current",
    success: function (result,status, xhr){

        $('#profile_user').html(result.data.firstName+'&nbsp'+result.data.lastName);
        userId = result.data.userId;
        localStorage.setItem("userId",userId);

/*
        if(result.data.firstLogin ==  true ){

                checkpassword();
        }
*/



    },
});

function insert_confirm(result) {
if(result.status == "SUCCESS"){
    $.session.set('inserterror', result.status);

    swal({
        title: "",
        text: "ثبت با موفقیت انجام شد",
        confirmButtonText: "تایید",
        closeOnConfirm: false,
        closeOnCancel: false
    }, function(isConfirm) {
        if (isConfirm) {

            location.reload();

        } else {

        }
    });
}else{

    $.session.set('inserterror', result.status);
    swal({
        title: "",
        text: result.description,
        confirmButtonText: "تایید",
        confirmButtonColor: "#DA4453",
        closeOnConfirm: true,
        closeOnCancel: false
    });

}


}

function delete_confirm(response){
    location.reload();
    // swal({
    //     title: "",
    //     text: response,
    //     confirmButtonText: "تایید",
    //     closeOnConfirm: true,
    //     closeOnCancel: false
    // });
    $.session.set("del_status","SUCCESS")


}

function delete_error(response){
    swal({
        title: "",
        text: response,
        confirmButtonText: "تایید",
             confirmButtonColor: "#DA4453",
             closeOnConfirm: true,
        closeOnCancel: false
    });
    $.session.set("del_status","ERROR")
}

function insert_error(response) {
    $.session.set('inserterror', "ERROR");
    $.session.set("modal_status", '');

    i=0;
    var res='';
    while (i<response.responseJSON.data.length)
    {
        var str2 = '<p>'+response.responseJSON.data[i].message+'</p>';
        res = res + str2;
        i++;
    }
    swal({
        title:'<small>'+response.responseJSON.description+'</small>',
        confirmButtonColor: "#DA4453",
        text: res,
        confirmButtonText: "تایید",
        html: true
    });

}

function update_error(response) {
    statuserror=response.responseJSON.status;
    $.session.set('statuserror', statuserror);
    $.session.set("modal_status", '');

    i=0;
    var res='';
    while (i<response.responseJSON.data.length)
    {
        var str2 = '<p>'+response.responseJSON.data[i].message+'</p>';
        res = res + str2;
        i++;
    }

    swal({
        title:'<small>'+response.responseJSON.description+'</small>',
        confirmButtonColor: "#DA4453",
        text: res,
        confirmButtonText: "تایید",
        html: true
    });
    $('#account_owners_info').hide();
}

function  update_success(response) {

    statuserror=response.status;
    $.session.set('statuserror', statuserror);

    if(response.status == "SUCCESS"){

        $.session.set("modal_status", '');

        swal({
            title: "",
            text: "ثبت با موفقیت انجام شد",
            confirmButtonText: "تایید",
            closeOnConfirm: true,
            closeOnCancel: false
        });
    }else{
        $.session.set("modal_status", '');

        swal({
            title: "",
            text: response.description,
            confirmButtonText: "تایید",
            confirmButtonColor: "#DA4453",
            closeOnConfirm: true,
            closeOnCancel: false
        });

    }

}




function checkCodeMeli(code)
{

    var L=code.length;

    if(L<8 || parseInt(code,10)==0) return false;
    code=('0000'+code).substr(L+4-10);
    if(parseInt(code.substr(3,6),10)==0) return false;
    var c=parseInt(code.substr(9,1),10);
    var s=0;
    for(var i=0;i<9;i++)
        s+=parseInt(code.substr(i,1),10)*(10-i);
    s=s%11;
    return (s<2 && c==s) || (s>=2 && c==(11-s));
    return true;
}

$('').keyup(function(e) {
    if(this.value!='-')
        while(isNaN(this.value))
            this.value = this.value.split('').reverse().join('').replace(/[\D]/i,'')
                .split('').reverse().join('');
})
    .on("cut copy paste",function(e){
        e.preventDefault();
    });

function insert_confirm_company_setting(result) {
    if(result.status == "SUCCESS"){
        $.session.set('inserterror', result.status);
        $.session.set("modal_status", '');

        swal({
            title: "",
            text: "ثبت با موفقیت انجام شد",
            confirmButtonText: "تایید",
            closeOnConfirm: true,
            closeOnCancel: false
        });
        $('input').val('');
    }else{
        $.session.set("modal_status", '');
        $.session.set('inserterror', result.status);
        swal({
            title: "",
            text: result.description,
            confirmButtonText: "تایید",
            confirmButtonColor: "#DA4453",
            closeOnConfirm: true,
                 allowOutsideClick: false,
            closeOnCancel: false
        });

    }


}
function sweet_ALert(text){
    swal({

        title: "",
        text: text,
        confirmButtonText: "تایید",
             allowOutsideClick: true,
        confirmButtonColor: "#DA4453",
        closeOnConfirm: true,
        closeOnCancel: false,

    });
}
// date pattern
function  date_pattern(text) {

        var result = !((/[0-9]{4}\/(0[1-9]|1[0-2])\/(0[1-9]|[1-2][0-9]|3[0-1])/).test(text))

    return result

}

function checkShamsi(str) {
    var patt = /(13|14)([0-9][0-9])\/(((0?[1-6])\/((0?[1-9])|([12][0-9])|(3[0-1])))|(((0?[7-9])|(1[0-2]))\/((0?[1-9])|([12][0-9])|(30))))/g;
    var result = patt.test(str);
    // if (result) {
    //     var pos = str.indexOf('/');
    //     var year = str.substring(0, pos);
    //     var nextPos = str.indexOf('/', pos + 1);
    //     var month = str.substring(pos + 1, nextPos);
    //     var day = str.substring(nextPos + 1);
    //     if (month == 12 && (year + 1) % 4 != 0 && day == 30) { // kabise = 1379, 1383, 1387,... (year +1) divides on 4 remains 0
    //         result = false;
    //
    //     }
    //     return result;
    //
    //
    // }
    return result;
}

$("#mobilePhone , #phone , #postalCode , #fax , .number").keypress(function (e) {
    //if the letter is not digit then display error and don't type anything
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
        $("#errmsg").html("Digits Only").show().fadeOut("slow");
        return false;
    }
});