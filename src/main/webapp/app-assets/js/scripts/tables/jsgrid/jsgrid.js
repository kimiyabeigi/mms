var ctx = "${pageContext.request.contextPath}"

$(document).ready(function() {


    $("#rolemanage").jsGrid({
        height: "70%",
        width: "100%",
        editing: true,
        sorting: true,
        inserting:true,
        paging: true,
        filtering:true,
        autoload: true,
        pageSize: 10,
        pageButtonCount: 5,
        deleteConfirm: "آیا از حذف این ركورد مطمین هستید؟",
        controller: db,
        fields: [
            {
                name: "name",
                type: "text",
                width: "25%",
                attr: { placeholder: "mm/dd/yyyy" },
                headerTemplate: function() {
                    return "نام نقش"
                },
            },
            { name: "nameEN",
                type: "text",
                width: "25%",
                headerTemplate: function() {
                    return "نام نقش انگلیسی"
                }
            },
            { name: "description",
                type: "text",
                width: "25%",
                headerTemplate: function() {
                    return "توضیحات"
                }
            },
            { type: "control",
                width: "25%",
                modeSwitchButton: false,
                editButton: true,
                itemTemplate: function(value, item) {
                    var $result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
                    var $customButton = $("<button class='btn btn-accent-1'>")
                        .text("دسترسی")
                        .click(function(e) {
                            // window.location = url;
                            // alert("جزییات " + item.name);
                            window.location = "userrole?id="+item.roleId;
                            e.stopPropagation();
                        });
                    // return "عملیات"
                    return $result.add($customButton);
                }
            }
        ]
    });
    $("#usermanager").jsGrid({
        height: "70%",
        width: "100%",
        editing: true,
        sorting: true,
        paging: true,
        filtering:true,
        autoload: true,
        pageSize: 10,
        pageButtonCount: 5,
        deleteConfirm: "آیا از حذف این ركورد مطمین هستید؟",
        controller: db,
        fields: [
            {
                name: "userName",
                type: "text",
                width: "10%",
                editing:false,

                headerTemplate: function() {
                    return "نام كاربری"
                },
            },
            {
                name: "userCode",
                type: "text",
                width: "10  %",
                // editing:false,

                headerTemplate: function() {
                    return "كد كاربر"
                },
            },
            { name: "firstName",
                type: "text",
                width: "10%",
                headerTemplate: function() {
                    return "نام"
                }
            },
            { name: "lastName",
                type: "text",
                width: "10%",
                headerTemplate: function() {
                    return "نام خانوادگی"
                }
            },
            // { name: "",
            //     type: "text",
            //     width: 80,
            //     headerTemplate: function() {
            //         return "عنوان"
            //     }
            // },
            { name: "roleId",
                type: "select",
                items:db.user_role,
                valueField:"roleId",
                textField:"name",
                width: "10%",
                headerTemplate: function() {
                    return "گروه كاربری"
                },filterTemplate: function() {
                    var $select = jsGrid.fields.select.prototype.filterTemplate.call(this);
                    $select.prepend($("<option selected>").prop("value", "0").text("(همه)"));
                    return $select;

                 },
            },
            { name: "branchId",
                type: "select",
                items:db.user_branch,
                valueField:"branchId",
                textField:"name",
                width: "15%",
                headerTemplate: function() {
                    return "زیر واحد سازمانی"
                },filterTemplate: function() {
                    var $select = jsGrid.fields.select.prototype.filterTemplate.call(this);
                    $select.prepend($("<option selected>").prop("value", "0").text("(همه)"));
                    return $select;

                },
            },
            // { name: "",
            //     type: "text",
            //     width: 80,
            //     headerTemplate: function() {
            //         return "كد پرسنلی"
            //     }
            // },
            // { name: "",
            //     type: "text",
            //     width: 80,
            //     headerTemplate: function() {
            //         return "از ip"
            //     }
            // },
            // { name: "",
            //     type: "text",
            //     width: 80,
            //     headerTemplate: function() {
            //         return "تا ip"
            //     }
            // },
            { name: "active",
                type: "checkbox",
                width: "10%",
                headerTemplate: function() {
                    return "وضعیت"
                },
            },


            {
                type: "control",
                modeSwitchButton: false,
                width: "10%",
                editButton: true,
                headerTemplate: function() {
                    // return $("<button>").attr("type", "button").text("Add")
                    //     .on("click", function () {
                    //         alert("asd")
                    //         showDetailsDialog("Add", {});
                    //     });

                    return "عملیات"
                }
            },
            { type: "button",
                width: "15%",
                modeSwitchButton: false,
                editButton: false,
                deleteButton:false,
                itemTemplate: function(value, item) {

                    var $result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
                    var $customButton = $("<button class='btn btn-accent-1'>")
                        .text("تغییر كلمه عبور")
                        .click(function(e) {
                            window.location ="changepasswordadmin?id="+item.userId;
                            e.stopPropagation();
                        });
                    // return "عملیات"
                    return $result.add($customButton);
                },
                headerTemplate: function() {
                    return "كلمه عبور"
                }
            }
        ]
    });

    $("#ListCompany").jsGrid({
        height: "70%",
        width: "100%",
        editing: true,
        inserting: true,
        paging: true,
        filtering:true,
        autoload: true,
        pageSize: 10,
        pageButtonCount: 5,
        deleteConfirm: "آیا از حذف این ركورد مطمین هستید؟",
        controller: db,

        fields: [
            {
                name: "companyCode",
                type: "text",
                align: "center",
                width: "10%",
                editing:false,
            //     validate: ["required",
            //     {
            //         validator: "rangeLength",
            //         param: [2, 2] ,
            //         message: " كد شركت حداكثر 2 كاراكتر می باشد."
            //     },
            // ] ,
                headerTemplate: function() {
                    return "كد شركت"
                },
            },



            { name: "name",
                type: "text",
                align: "center",
                width: "80%",
                // validate: ["required",
                //     {
                //         validator: "rangeLength",
                //         param: [1, 50] ,
                //         message: " نام شركت حداكثر 50 كاراكتر می باشد.",
                //     }
                // ],
                headerTemplate: function() {
                return "نام شركت"
            },
                // _createTextBox: function() {
                //     var $result = jsGrid.fields.text.prototype._createTextBox.call(this);
                //     $result.attr("placeholder", "شركت");
                //     return $result;
                // },
            },


        {
            type: "control",
            width: "10%",
            align: "center",
            modeSwitchButton: false,
            editButton: true,
            headerTemplate: function() {
                // return $("<button>").attr("type", "button").text("Add")
                //     .on("click", function () {
                //         alert("asd")
                //         showDetailsDialog("Add", {});
                //     });

                return "عملیات"
            }
        }
    ]
});
    $("#ListService").jsGrid({
        height: "70%",
        width: "100%",
        paging: true,
        filtering:true,
        autoload: true,
        pageSize: 10,
        pageButtonCount: 5,
        controller: db,
        fields: [
            {
                name: "serviceCode",
                type: "text",
                width: 30,
                align: "center",
                headerTemplate: function() {
                    return "كد خدمت"
                },
            },



            { name: "name",
                type: "text",
                width: 150,
                align: "center",
                // validate: "required",
                headerTemplate: function() {
                    return "نام خدمت"
                }
            },

        ]
    });
    $("#ListFactor").jsGrid({
        height: "70%",
        width: "100%",
        editing: true,
        inserting: true,
        paging: true,
        filtering:true,
        autoload: true,
        pageSize: 10,
        pageButtonCount: 5,
        deleteConfirm: "آیا از حذف این ركورد مطمین هستید؟",
        controller: db,
        fields: [
            {
                name: "indexCode",
                type: "text",
                width: "10%",
                align: "center",
                editing:false,

                // validate: ["required",
                //     {
                //         validator: "rangeLength",
                //         param: [1, 15] ,
                //         message: " كد شاخص : طول كاراكتر می بایست 15 باشد"
                //     },
                // ] ,
                headerTemplate: function() {
                    return "كد شاخص"
                },
            },



            { name: "name",
                type: "text",
                width: "80%",
                align: "center",
                // validate: ["required",
                //     {
                //         validator: "rangeLength",
                //         param: [1, 30] ,
                //         message: " نام شاخص : طول كاراكتر می بایست 30 باشد",
                //     }
                // ],
                headerTemplate: function() {
                    return "نام شاخص"
                }
            },


            {
                type: "control",
                width : "10%" ,
                align: "center",
                modeSwitchButton: false,
                editButton: true,
                headerTemplate: function() {
                    // return $("<button>").attr("type", "button").text("Add")
                    //     .on("click", function () {
                    //         alert("asd")
                    //         showDetailsDialog("Add", {});
                    //     });

                    return "عملیات"
                }
            }
        ]
    });
    $("#ListTerminal").jsGrid({
        height: "70%",
        width: "100%",
        paging: true,
        editing: true,
        inserting: true,
        filtering:true,
        autoload: true,
        selecting: true,


        pageSize: 10,
        pageButtonCount: 5,
        deleteConfirm: "آیا از حذف این ركورد مطمین هستید؟",
        controller: db,
        fields: [

            {
                name: "terminalId",
                type: "select",
                items:db.terminal,
                valueField:"terminalId",
                textField:"code",
                width:"20%",
                align: "center",



                headerTemplate: function() {
                    return "نوع درخواست"
                }
            ,filterTemplate: function() {
        var $select = jsGrid.fields.select.prototype.filterTemplate.call(this);
        $select.prepend($("<option selected>").prop("value", "0").text("(همه)"));
        return $select;

    },
            },
            {

                name: "terminalDetailCode",
                type: "text",
                width: "35%",
                align: "center",
                editing:false,

                // validate: ["required",
                //     {
                //         validator: "rangeLength",
                //         param: [1, 15] ,
                //         message: " كد پایانه : طول كاراكتر می بایست 15 باشد"
                //     },
                //     function(value, item) {
                //         return item.IsRetired ? value > 55 : true;
                //     }
                //
                // ] ,
                headerTemplate: function() {
                    return "كد پایانه"
                },
            },

            { name: "name",
                type: "text",
                width: "35%",
                align: "center",
                // validate: ["required",
                //     {
                //         validator: "rangeLength",
                //         param: [1, 30] ,
                //         message: " نام پایانه : طول كاراكتر می بایست 30 باشد"
                //     },
                //     function(value, item) {
                //         return item.IsRetired ? value > 55 : true;
                //     }
                //
                // ] ,
                headerTemplate: function() {
                    return "نام پایانه"
                },
            },

            {
                type: "control",
                modeSwitchButton: false,
                editButton: true,
                align: "center",
                width: "10%",
                headerTemplate: function() {
                    // return $("<button>").attr("type", "button").text("Add")
                    //     .on("click", function () {
                    //         alert("asd")
                    //         showDetailsDialog("Add", {});
                    //     });

                    return "عملیات"
                }
            }

        ]
    });
    $("#ListGuild").jsGrid({
        height: "70%",
        width: "100%",
        editing: true,
        inserting: true,
        sorting: true,
        paging: true,
        filtering:true,
        autoload: true,
        pageSize: 10,
        pageButtonCount: 5,
        deleteConfirm: "آیا از حذف این ركورد مطمین هستید؟",
        controller: db,
        fields: [
            {
                name: "guildCode",
                type: "text",
                width: "20%",
                editing:false,

                // validate: ["required",
                //     {
                //         validator: "rangeLength",
                //         param: [1, 25] ,
                //         message: " كد شركت حداكثر 25 كاراكتر می باشد."
                //     },
                // ] ,
                headerTemplate: function() {
                    return "كد تكمیلی صنف"
                },
            },



            { name: "description",
                type: "text",
                width: "60%",
                // validate: ["required",
                //     {
                //         validator: "rangeLength",
                //         param: [1, 30] ,
                //         message: " نام شركت حداكثر 30 كاراكتر می باشد.",
                //     }
                // ],
                headerTemplate: function() {
                    return "شرح كد تكمیلی صنف"
                }
            },

            { name: "active",
                type: "checkbox",
                width: "10%",
                insertTemplate: function() {
                    var $result = jsGrid.fields.checkbox.prototype.insertTemplate.call(this);
                    $result.prop("checked", true);
                    return $result;
                },
                headerTemplate: function() {
                    return "فعال"
                },
            },


            {
                type: "control",
                modeSwitchButton: false,
                width: "10%",
                editButton: true,
                headerTemplate: function() {
                    // return $("<button>").attr("type", "button").text("Add")
                    //     .on("click", function () {
                    //         alert("asd")
                    //         showDetailsDialog("Add", {});
                    //     });

                    return "عملیات"
                }
            }
        ]
    });
    $("#ListGuild2").jsGrid({
        height: "70%",
        width: "100%",
        sorting: true,
        paging: true,
        filtering:true,
        autoload: true,
        pageSize: 10,
        pageButtonCount: 5,
        deleteConfirm: "آیا از حذف این ركورد مطمین هستید؟",
        controller: db,
                                // rowClick: function(item) {
                                //    alert(item)
                                // },
        fields: [
            {

                // headerTemplate: function() {
                //     return $("<button>").attr("type", "Delete").text("پیگیری")
                //         .on("click", function () {
                //             deleteSelectedItems();
                //         });
                // },
                itemTemplate: function(_, item) {
                    return $("<input>").attr("type", "checkbox")
                        .prop("checked", $.inArray(item, selectedItems) > -1)
                        .on("change", function () {
                            $(this).is(":checked") ? selectItem(item) : unselectItem(item);
                        });
                },
                align: "center",
                width: 100
            },

            { name: "createDate",
                type: "text",
                width: 100,
                // validate: ["required",
                //     {
                //         validator: "rangeLength",
                //         param: [1, 30] ,
                //         message: " نام شركت حداكثر 30 كاراكتر می باشد.",
                //     }
                // ],
                headerTemplate: function() {
                    return "تاریخ"
                }
            },
            { name: "accountNo",
                        type: "text",
                        width: 150,
                        headerTemplate: function() {
                            return "شماره حساب"
                        }
                    },
            { name: "clientNumber",
                        type: "text",
                        width: 150,
                        headerTemplate: function() {
                            return "شماره مشتری"
                        }
                    },
            { name: "branchName",
                        type: "text",
                        width: 130,
                        // validate: ["required",
                        //     {
                        //         validator: "rangeLength",
                        //         param: [1, 30] ,
                        //         message: " نام شركت حداكثر 30 كاراكتر می باشد.",
                        //     }
                        // ],
                        headerTemplate: function() {
                            return "شعبه"
                        }
                    },{ name: "requestNumber",
                        type: "number",
                        width: 100,
                        // validate: ["required",
                        //     {
                        //         validator: "rangeLength",
                        //         param: [1, 30] ,
                        //         message: " نام شركت حداكثر 30 كاراكتر می باشد.",
                        //     }
                        // ],
                        headerTemplate: function() {
                            return "شماره درخواست"
                        }
                    },{ name: "pspTrackingCode",
                        type: "text",
                        width: 120,
                        // validate: ["required",
                        //     {
                        //         validator: "rangeLength",
                        //         param: [1, 30] ,
                        //         message: " نام شركت حداكثر 30 كاراكتر می باشد.",
                        //     }
                        // ],
                        headerTemplate: function() {
                            return "كد پیگیری شركت"
                        }
                    },{ name: "status",
                        type: "text",
                        width: 150,
                        // validate: ["required",
                        //     {
                        //         validator: "rangeLength",
                        //         param: [1, 30] ,
                        //         message: " نام شركت حداكثر 30 كاراكتر می باشد.",
                        //     }
                        // ],
                        headerTemplate: function() {
                            return "توضیحات"
                        }
                    },






            // {
            //     type: "control",
            //     modeSwitchButton: false,
            //     width: 15,
            //     editButton: true,
            //     headerTemplate: function() {
            //         // return $("<button>").attr("type", "button").text("Add")
            //         //     .on("click", function () {
            //         //         alert("asd")
            //         //         showDetailsDialog("Add", {});
            //         //     });
            //
            //         return "عملیات"
            //     }
            // }
        ]
    });
    $("#TraceAccept").jsGrid({
        height: "70%",
        width: "100%",
        sorting: true,
        paging: true,
        filtering:true,
        autoload: true,
        pageSize: 10,
        pageButtonCount: 5,
        deleteConfirm: "آیا از حذف این ركورد مطمین هستید؟",
        controller: db,
                                // rowClick: function(item) {
                                //    alert(item)
                                // },
        fields: [
            {

                // headerTemplate: function() {
                //     return $("<button>").attr("type", "Delete").text("پیگیری")
                //         .on("click", function () {
                //             deleteSelectedItems();
                //         });
                // },
                itemTemplate: function(_, item) {
                    return $("<input>").attr("type", "checkbox")
                        .prop("checked", $.inArray(item, selectedItems) > -1)
                        .on("change", function () {
                            $(this).is(":checked") ? selectItem(item) : unselectItem(item);
                        });
                },
                align: "center",
                width: 40
            },

            { name: "createDate",
                type: "text",
                width: 100,
                // validate: ["required",
                //     {
                //         validator: "rangeLength",
                //         param: [1, 30] ,
                //         message: " نام شركت حداكثر 30 كاراكتر می باشد.",
                //     }
                // ],
                headerTemplate: function() {
                    return "تاریخ"
                }
            },
            { name: "accountNo",
                        type: "text",
                        width: 150,
                        headerTemplate: function() {
                            return "شماره حساب"
                        }
                    },
            { name: "clientNumber",
                        type: "text",
                        width: 110,
                        headerTemplate: function() {
                            return "شماره مشتری"
                        }
                    },
            { name: "branchName",
                        type: "text",
                        width: 130,
                        // validate: ["required",
                        //     {
                        //         validator: "rangeLength",
                        //         param: [1, 30] ,
                        //         message: " نام شركت حداكثر 30 كاراكتر می باشد.",
                        //     }
                        // ],
                        headerTemplate: function() {
                            return "شعبه"
                        }
                    },{ name: "requestNumber",
                        type: "number",
                        width: 100,
                        // validate: ["required",
                        //     {
                        //         validator: "rangeLength",
                        //         param: [1, 30] ,
                        //         message: " نام شركت حداكثر 30 كاراكتر می باشد.",
                        //     }
                        // ],
                        headerTemplate: function() {
                            return "شماره درخواست"
                        }
                    },{ name: "pspTrackingCode",
                        type: "text",
                        width: 120,
                        // validate: ["required",
                        //     {
                        //         validator: "rangeLength",
                        //         param: [1, 30] ,
                        //         message: " نام شركت حداكثر 30 كاراكتر می باشد.",
                        //     }
                        // ],
                        headerTemplate: function() {
                            return "كد پیگیری شركت"
                        }
                    },{ name: "status",
                        type: "text",
                        width: 150,
                        // validate: ["required",
                        //     {
                        //         validator: "rangeLength",
                        //         param: [1, 30] ,
                        //         message: " نام شركت حداكثر 30 كاراكتر می باشد.",
                        //     }
                        // ],
                        headerTemplate: function() {
                            return "توضیحات"
                        }

                    },{ name: "pspAcceptanceCode",
                        type: "text",
                        width: 150,
                        // validate: ["required",
                        //     {
                        //         validator: "rangeLength",
                        //         param: [1, 30] ,
                        //         message: " نام شركت حداكثر 30 كاراكتر می باشد.",
                        //     }
                        // ],
                        headerTemplate: function() {
                            return "كد پذیرنده"
                        }
                    },{ name: "pspTerminalCode",
                        type: "text",
                        width: 150,
                        // validate: ["required",
                        //     {
                        //         validator: "rangeLength",
                        //         param: [1, 30] ,
                        //         message: " نام شركت حداكثر 30 كاراكتر می باشد.",
                        //     }
                        // ],
                        headerTemplate: function() {
                            return "كد پایانه"
                        }
                    },






            // {
            //     type: "control",
            //     modeSwitchButton: false,
            //     width: 15,
            //     editButton: true,
            //     headerTemplate: function() {
            //         // return $("<button>").attr("type", "button").text("Add")
            //         //     .on("click", function () {
            //         //         alert("asd")
            //         //         showDetailsDialog("Add", {});
            //         //     });
            //
            //         return "عملیات"
            //     }
            // }
        ]
    });

    $("#report12").jsGrid({
        height: "70%",
        width: "100%",
        sorting: true,
        paging: true,
        filtering:true,
        autoload: true,
        pageSize: 10,
        pageButtonCount: 5,
        deleteConfirm: "آیا از حذف این ركورد مطمین هستید؟",
        controller: db,
                                // rowClick: function(item) {
                                //    alert(item)
                                // },
        fields: [


            { name: "requestDate",
                type: "text",
                width: 100,
                // validate: ["required",
                //     {
                //         validator: "rangeLength",
                //         param: [1, 30] ,
                //         message: " نام شركت حداكثر 30 كاراكتر می باشد.",
                //     }
                // ],
                headerTemplate: function() {
                    return "تاریخ"
                }
            },
            { name: "accountNo",
                        type: "text",
                        width: 150,
                        headerTemplate: function() {
                            return "شماره حساب"
                        }
                    },
            { name: "clientNumber",
                        type: "text",
                        width: 110,
                        headerTemplate: function() {
                            return "شماره مشتری"
                        }
                    },
            { name: "branch",
                        type: "text",
                        width: 130,
                        // validate: ["required",
                        //     {
                        //         validator: "rangeLength",
                        //         param: [1, 30] ,
                        //         message: " نام شركت حداكثر 30 كاراكتر می باشد.",
                        //     }
                        // ],
                        headerTemplate: function() {
                            return "شعبه"
                        }
                    },{ name: "companyName",
                        type: "text",
                        width: 100,
                        // validate: ["required",
                        //     {
                        //         validator: "rangeLength",
                        //         param: [1, 30] ,
                        //         message: " نام شركت حداكثر 30 كاراكتر می باشد.",
                        //     }
                        // ],
                        headerTemplate: function() {
                            return "نام شركت"
                        }
                    },{ name: "requestStatus",
                        type: "text",
                        width: 100,
                        // validate: ["required",
                        //     {
                        //         validator: "rangeLength",
                        //         param: [1, 30] ,
                        //         message: " نام شركت حداكثر 30 كاراكتر می باشد.",
                        //     }
                        // ],
                        headerTemplate: function() {
                            return "وضعیت درخواست"
                        }
                    },{ name: "requestNumber",
                        type: "text",
                        width: 100,
                        // validate: ["required",
                        //     {
                        //         validator: "rangeLength",
                        //         param: [1, 30] ,
                        //         message: " نام شركت حداكثر 30 كاراكتر می باشد.",
                        //     }
                        // ],
                        headerTemplate: function() {
                            return "شماره درخواست"
                        }
                    },{ name: "pspTrackingCode",
                        type: "text",
                        width: 120,
                        // validate: ["required",
                        //     {
                        //         validator: "rangeLength",
                        //         param: [1, 30] ,
                        //         message: " نام شركت حداكثر 30 كاراكتر می باشد.",
                        //     }
                        // ],
                        headerTemplate: function() {
                            return "كد پیگیری شركت"
                        }
                    },{ name: "description",
                        type: "text",
                        width: 150,
                        // validate: ["required",
                        //     {
                        //         validator: "rangeLength",
                        //         param: [1, 30] ,
                        //         message: " نام شركت حداكثر 30 كاراكتر می باشد.",
                        //     }
                        // ],
                        headerTemplate: function() {
                            return "توضیحات"
                        }

                    },{ name: "pspAcceptanceCode",
                        type: "text",
                        width: 150,
                        // validate: ["required",
                        //     {
                        //         validator: "rangeLength",
                        //         param: [1, 30] ,
                        //         message: " نام شركت حداكثر 30 كاراكتر می باشد.",
                        //     }
                        // ],
                        headerTemplate: function() {
                            return "كد پذیرنده"
                        }
                    },{ name: "pspTerminalCode",
                        type: "text",
                        width: 150,
                        // validate: ["required",
                        //     {
                        //         validator: "rangeLength",
                        //         param: [1, 30] ,
                        //         message: " نام شركت حداكثر 30 كاراكتر می باشد.",
                        //     }
                        // ],
                        headerTemplate: function() {
                            return "كد پایانه"
                        }
                    },

            { type: "button",
                width: 150,
                modeSwitchButton: false,
                editButton: false,
                deleteButton:false,
                itemTemplate: function(value, item) {

                    var $result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
                    var $customButton = $("<button class='btn btn-accent-1'>")
                        .text("جزییات")
                        .click(function(e) {
                            // window.location ="report_de?id="+item.pspRequestId;
                            e.stopPropagation();


                            window.open(
                                'report_de?id='+item.pspRequestId,
                                '_blank' // <- This is what makes it open in a new window.
                            );


                        });
                    // return "عملیات"
                    return $result.add($customButton);
                },
                headerTemplate: function() {
                    return "عملیات"
                }
            }






        ]
    });

    var selectedItems = [];

    var selectItem = function(item) {
        selectedItems.push(item);
        item.selected=true;
        localStorage.setItem("listtrackingcode",JSON.stringify(selectedItems))
        // alert(JSON.stringify(selectedItems))

    };

    var unselectItem = function(item)
    {
        item.selected=false;
        selectedItems = $.grep(selectedItems, function(i) {
            return i !== item;
        });
        // alert(JSON.stringify(item))
        localStorage.setItem("listtrackingcode",JSON.stringify(selectedItems))

        // alert(JSON.stringify(selectedItems))

    };

    var deleteSelectedItems = function() {
        if(!selectedItems.length || !confirm("Are you sure?"))
            return;
        deleteClientsFromDb(selectedItems);
        var $grid = $("#ListGuild2");
        $grid.jsGrid("option", "pageIndex", 1);
        $grid.jsGrid("loadData");
        selectedItems = [];
    };

    var deleteClientsFromDb = function(deletingClients) {
        db.clients = $.map(db.clients, function(client) {
            return ($.inArray(client, deletingClients) > -1) ? null : client;
        });
    };



    $("#ListState").jsGrid({
        height: "70%",
        width: "100%",
        editing: true,
        inserting: true,
        sorting: true,
        paging: true,
        filtering:true,
        autoload: true,
        pageSize: 15,
        pageButtonCount: 5,
        deleteConfirm: "آیا از حذف این ركورد مطمین هستید؟",
        controller: db,
        fields: [


            {
                name: "stateCode",
                type: "text",
                width: "10%",
                editing:false,

                // validate: ["required",
                //     {
                //         validator: "rangeLength",
                //         param: [1, 10] ,
                //         message: " كد استان حداكثر 25 كاراكتر می باشد."
                //     },
                // ] ,
                headerTemplate: function() {
                    return "كد استان"
                },
            },



            { name: "name",
                type: "text",
                width: "30%",
                // validate: ["required",
                //     {
                //         validator: "rangeLength",
                //         param: [1, 30] ,
                //         message: " نام استان حداكثر 30 كاراكتر می باشد.",
                //     }
                // ],
                headerTemplate: function() {
                    return "نام استان"
                }
            },

            { name: "nameEN",
                type: "text",
                width:"30%",

                // validate: [
                //     {
                //         validator: "rangeLength",
                //         param: [0, 30] ,
                //         message: " نام انگلیسی استان حداكثر 30 كاراكتر می باشد.",
                //     }
                // ],
                headerTemplate: function() {
                    return "نام انگلیسی استان"
                }
            },

            { name: "active",
                type: "checkbox",
                width: "10%",
                insertTemplate: function() {
                    var $result = jsGrid.fields.checkbox.prototype.insertTemplate.call(this);
                    $result.prop("checked", true);
                    return $result;
                },
                headerTemplate: function() {
                    return "فعال"
                },
            },




            // {
            //     type: "control",
            //     modeSwitchButton: false,
            //     width: 20,
            //     editButton: true,
            //     headerTemplate: function() {
            //         // return $("<button>").attr("type", "button").text("Add")
            //         //     .on("click", function () {
            //         //         alert("asd")
            //         //         showDetailsDialog("Add", {});
            //         //     });
            //
            //         return "عملیات"
            //     }
            // }

            { type: "control",
                width: "20%",
                modeSwitchButton: false,
                editButton: true,
                itemTemplate: function(value, item) {

                    var $result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
                    var $customButton = $("<button class='btn btn-accent-1'>")
                        .text("جزییات شهر")
                        .click(function(e) {
                            // window.location = url;
                            // alert("جزییات " + item.name);
                            window.location = "listcity?id="+item.stateId;
                            e.stopPropagation();
                        });
                    // return "عملیات"
                    return $result.add($customButton);
                }
            }
        ]
    });

    $("#ListCity").jsGrid({
        height: "70%",
        width: "100%",
        editing: true,
        inserting: true,
        sorting: true,
        paging: true,
        filtering:true,
        autoload: true,
        pageSize: 15,
        pageButtonCount: 5,
        deleteConfirm: "آیا از حذف این ركورد مطمین هستید؟",
        controller: db,
        fields: [
            {
                name: "cityCode",
                type: "text",
                width: "20%",
                editing:false,

                // validate: ["required",
                //     {
                //         validator: "rangeLength",
                //         param: [1, 10] ,
                //         message: " كد شهر حداكثر 10 كاراكتر می باشد."
                //     },
                // ] ,
                headerTemplate: function() {
                    return "كد شهر"
                },
            },



            { name: "name",
                type: "text",
                width: "30%",
                // validate: ["required",
                //     {
                //         validator: "rangeLength",
                //         param: [1, 30] ,
                //         message: " نام شهر حداكثر 30 كاراكتر می باشد.",
                //     }
                // ],
                headerTemplate: function() {
                    return "نام شهر"
                }
            },

            { name: "nameEN",
                type: "text",
                width:"30%",
                // validate: [
                //     {
                //         validator: "rangeLength",
                //         param: [0, 30] ,
                //         message: " نام انگلیسی شهر حداكثر 30 كاراكتر می باشد.",
                //     }
                // ],
                headerTemplate: function() {
                    return "نام انگلیسی شهر"
                }
            },

            { name: "active",
                type: "checkbox",
                width: "10%",
                insertTemplate: function() {
                    var $result = jsGrid.fields.checkbox.prototype.insertTemplate.call(this);
                    $result.prop("checked", true);
                    return $result;
                },
                headerTemplate: function() {
                    return "فعال"
                },
            },




            {
                type: "control",
                modeSwitchButton: false,
                width: "10%",
                editButton: true,
                headerTemplate: function() {
                    // return $("<button>").attr("type", "button").text("Add")
                    //     .on("click", function () {
                    //         alert("asd")
                    //         showDetailsDialog("Add", {});
                    //     });

                    return "عملیات"
                }
            }
        ]
    });

    $("#ConfigService").jsGrid({
        height: "100%",
        width: "100%",
        sorting: true,
        paging: true,
        editing: true,
        inserting: true,
        autoload: true,
        selecting: true,
        pageSize: 15,
        pageButtonCount: 5,
        // deleteConfirm: "آیا از حذف این ركورد مطمین هستید؟",
        controller: db,
        fields: [

            {
                name: "serviceId",
                type: "select",
                items:db.service,
                valueField:"serviceId",
                textField:"name",
                width:"80%",



                headerTemplate: function() {
                    return "كد خدمت"
                },
            },


            { name: "active",
                type: "checkbox",
                width: "10%",

                headerTemplate: function() {
                    return "فعال"
                },
            },

            {
                type: "control",
                modeSwitchButton: false,
                editButton: true,
                deleteButton:false,
                width: "10%",
                headerTemplate: function() {
                    // return $("<button>").attr("type", "button").text("Add")
                    //     .on("click", function () {
                    //         alert("asd")
                    //         showDetailsDialog("Add", {});
                    //     });

                    return "عملیات"
                }
            }

        ]
    });

    $("#ConfigTerminal").jsGrid({
        height: "100%",
        width: "100%",
        sorting: true,
        paging: true,
        editing: true,
        inserting: true,
        autoload: true,
        selecting: true,
        pageSize: 15,
        pageButtonCount: 5,
        deleteConfirm: "آیا از حذف این ركورد مطمین هستید؟",
        controller: db1,
        fields: [
            {
                name: "terminalDetailId",
                type: "select",
                items:db1.terminalDetail,
                valueField:"terminalDetailId",
                textField:"name",
                width:"40%",
                headerTemplate: function() {
                    return "كد پایانه"
                },
            },
            {
                name: "connectionCode",
                type: "text",
                width:"40%",
                placeholder : "كد اتصال را درج نمایید",
                // validate: ["required",
                //     {
                //         validator: "rangeLength",
                //         param: [1, 10] ,
                //         message: " طول فیلد كد اتصال حداكثر 10 كاراكتر می باشد"
                //     },
                //     function(value, item) {
                //         return item.IsRetired ? value > 55 : true;
                //     }

                // ] ,
                headerTemplate: function() {
                    return "كد اتصال"
                },
            },

            { name: "active",
                type: "checkbox",
                width:"10%",

                headerTemplate: function() {
                    return "فعال"
                },
            },

            {
                type: "control",
                modeSwitchButton: false,
                editButton: true,
                deleteButton:false,
                width:"10%",
                headerTemplate: function() {
                    // return $("<button>").attr("type", "button").text("Add")
                    //     .on("click", function () {
                    //         alert("asd")
                    //         showDetailsDialog("Add", {});
                    //     });

                    return "عملیات"
                }
            }

        ]
    });

    $("#ConfigStatus").jsGrid({
        height: "100%",
        width: "100%",
        sorting: true,
        paging: true,
        editing: true,
        inserting: true,
        autoload: true,
        selecting: true,
        pageSize: 15,
        pageButtonCount: 5,
        deleteConfirm: "آیا از حذف این ركورد مطمین هستید؟",
        controller: db2,
        fields: [

            {
                name: "serviceId",
                type: "select",
                items:db.service,
                valueField:"serviceId",
                textField:"name",
                width:"30%",



                headerTemplate: function() {
                    return "كد خدمت"
                },
            },

            {
                name: "indexId",
                type: "select",
                items:db2.index,
                valueField:"indexId",
                textField:"name",
                width:"20%",



                headerTemplate: function() {
                    return "كد شاخص"
                },
            },

            {
                name: "statusCode",
                type: "text",
                width:"20%",
                // validate: ["required",
                //     {
                //         validator: "rangeLength",
                //         param: [1, 10] ,
                //         message: " كد استان حداكثر 25 كاراكتر می باشد."
                //     },
                // ] ,
                headerTemplate: function() {
                    return "كد وضعیت"
                },
            },
            {
                name: "statusTypeId",
                type: "select",
                items:db2.statusType,
                valueField:"statusTypeId",
                textField:"code",
                width:"20%",

                headerTemplate: function() {
                    return "نوع وضعیت"
                },
            },
// یبلیب
   {
                type: "control",
                modeSwitchButton: false,
                editButton: true,
                deleteButton:false,
                  width:"10%",
                headerTemplate: function() {
                    // return $("<button>").attr("type", "button").text("Add")
                    //     .on("click", function () {
                    //         alert("asd")
                    //         showDetailsDialog("Add", {});
                    //     });

                    return "عملیات"
                }
            }

        ]
    });

    // $("#SearchTrace").jsGrid({
    //     height: "70%",
    //     width: "100%",
    //     editing: true,
    //     inserting: true,
    //     sorting: true,
    //     paging: true,
    //     filtering:true,
    //     autoload: true,
    //     pageSize: 15,
    //     pageButtonCount: 5,
    //     deleteConfirm: "آیا از حذف این ركورد مطمین هستید؟",
    //     controller: db5,
    //     fields: [
    //         {
    //             name: "guildCode",
    //             type: "text",
    //             width: 100,
    //             // validate: ["required",
    //             //     {
    //             //         validator: "rangeLength",
    //             //         param: [1, 25] ,
    //             //         message: " كد شركت حداكثر 25 كاراكتر می باشد."
    //             //     },
    //             // ] ,
    //             headerTemplate: function() {
    //                 return "انتخاب"
    //             },
    //         },
    //
    //
    //
    //         { name: "description",
    //             type: "text",
    //             width: 150,
    //             // validate: ["required",
    //             //     {
    //             //         validator: "rangeLength",
    //             //         param: [1, 30] ,
    //             //         message: " نام شركت حداكثر 30 كاراكتر می باشد.",
    //             //     }
    //             // ],
    //             headerTemplate: function() {
    //                 return "تاریخ"
    //             }
    //         },
    //
    //         { name: "description",
    //             type: "text",
    //             width: 150,
    //             // validate: ["required",
    //             //     {
    //             //         validator: "rangeLength",
    //             //         param: [1, 30] ,
    //             //         message: " نام شركت حداكثر 30 كاراكتر می باشد.",
    //             //     }
    //             // ],
    //             headerTemplate: function() {
    //                 return "شماره حساب"
    //             }
    //         }, { name: "description",
    //             type: "text",
    //             width: 150,
    //             // validate: ["required",
    //             //     {
    //             //         validator: "rangeLength",
    //             //         param: [1, 30] ,
    //             //         message: " نام شركت حداكثر 30 كاراكتر می باشد.",
    //             //     }
    //             // ],
    //             headerTemplate: function() {
    //                 return "شعبه"
    //             }
    //         },{ name: "description",
    //             type: "text",
    //             width: 150,
    //             // validate: ["required",
    //             //     {
    //             //         validator: "rangeLength",
    //             //         param: [1, 30] ,
    //             //         message: " نام شركت حداكثر 30 كاراكتر می باشد.",
    //             //     }
    //             // ],
    //             headerTemplate: function() {
    //                 return "شماره درخواست"
    //             }
    //         },{ name: "description",
    //             type: "text",
    //             width: 150,
    //             // validate: ["required",
    //             //     {
    //             //         validator: "rangeLength",
    //             //         param: [1, 30] ,
    //             //         message: " نام شركت حداكثر 30 كاراكتر می باشد.",
    //             //     }
    //             // ],
    //             headerTemplate: function() {
    //                 return "كد پیگیری شركت"
    //             }
    //         },{ name: "description",
    //             type: "text",
    //             width: 150,
    //             // validate: ["required",
    //             //     {
    //             //         validator: "rangeLength",
    //             //         param: [1, 30] ,
    //             //         message: " نام شركت حداكثر 30 كاراكتر می باشد.",
    //             //     }
    //             // ],
    //             headerTemplate: function() {
    //                 return "توضیحات"
    //             }
    //         },
    //         { name: "description",
    //             type: "text",
    //             width: 150,
    //             // validate: ["required",
    //             //     {
    //             //         validator: "rangeLength",
    //             //         param: [1, 30] ,
    //             //         message: " نام شركت حداكثر 30 كاراكتر می باشد.",
    //             //     }
    //             // ],
    //             headerTemplate: function() {
    //                 return "شماره مشتری"
    //             }
    //         },
    //
    //         { name: "active",
    //             type: "checkbox",
    //             width: 8,
    //
    //             headerTemplate: function() {
    //                 return "فعال"
    //             },
    //         },
    //
    //
    //         {
    //             type: "control",
    //             modeSwitchButton: false,
    //             width: 15,
    //             editButton: true,
    //             headerTemplate: function() {
    //                 // return $("<button>").attr("type", "button").text("Add")
    //                 //     .on("click", function () {
    //                 //         alert("asd")
    //                 //         showDetailsDialog("Add", {});
    //                 //     });
    //
    //                 return "عملیات"
    //             }
    //         }
    //     ]
    // });





    /***********************************
*      Data Service Scenario       *
***********************************/

 $("#serviceScenario").jsGrid({
        height: "auto",
        width: "100%",

        sorting: true,
        paging: false,
        autoload: true,

        controller: {
            loadData: function() {
                var d = $.Deferred();

                $.ajax({
                   url: "http://services.odata.org/V3/(S(3mnweai3qldmghnzfshavfok))/OData/OData.svc/Products",
                    dataType: "json"
                }).done(function(response) {
                    d.resolve(response.value);
                });

                return d.promise();
            }
        },

        fields: [
            { name: "Name", type: "text" },
            { name: "Description", type: "textarea", width: 150 },
            { name: "Rating", type: "number", width: 50, align: "center",
                itemTemplate: function(value) {
                    return $("<div>").addClass("rating").append(Array(value + 1).join("&#9733;"));
                }
            },
            { name: "Price", type: "number", width: 50,
                itemTemplate: function(value) {
                    return value.toFixed(2) + "$"; }
            }
        ]
    });

/******************************
*      Sorting Scenario       *
******************************/

$("#sorting-table").jsGrid({
    height:"400px",
    width: "100%",

    autoload: true,
    selecting: false,

    controller: db,

    fields: [
        { name: "Name", type: "text", width: 150 },
        { name: "Age", type: "number", width: 50 },
        { name: "Address", type: "text", width: 200 },
        { name: "Country", type: "select", items: db.countries, valueField: "Id", textField: "asdas" },
        { name: "Married", type: "checkbox", title: "Is Married" }
    ]
});


$("#sort").on('click', function() {
    var field = $("#sortingField").val();
    $("#sorting-table").jsGrid("sort", field);
});

/************************
*      Validation       *
************************/

$("#validation").jsGrid({
    width: "100%",
    filtering: true,
    editing: true,
    inserting: true,
    sorting: true,
    paging: true,
    autoload: true,
    pageSize: 15,
    pageButtonCount: 5,
    deleteConfirm: "Do you really want to delete the client?",
    controller: db,
    fields: [
        { name: "Name", type: "text", width: 150, validate: "required" },
        { name: "Age", type: "number", width: 50, validate: { validator: "range", param: [18, 80] } },
        { name: "Address", type: "text", width: 200, validate: { validator: "rangeLength", param: [10, 250] } },
        { name: "Country", type: "select", items: db.countries, valueField: "Id", textField: "Name",
            validate: { message: "Country should be specified", validator: function(value) { return value > 0; } } },
        { name: "Married", type: "checkbox", title: "Is Married", sorting: false },
        { type: "control" }
    ]
});


/*****************************
*      Loading by Page       *
*****************************/

$("#loading").jsGrid({
    width: "100%",

    autoload: true,
    paging: true,
    pageLoading: true,
    pageSize: 15,
    pageIndex: 2,

    controller: {
        loadData: function(filter) {
            var startIndex = (filter.pageIndex - 1) * filter.pageSize;
            return {
                data: db.clients.slice(startIndex, startIndex + filter.pageSize),
                itemsCount: db.clients.length
            };
        }
    },

    fields: [
        { name: "Name", type: "text", width: 150 },
        { name: "Age", type: "number", width: 50 },
        { name: "Address", type: "text", width: 200 },
        { name: "Country", type: "select", items: db.countries, valueField: "Id", textField: "Name" },
        { name: "Married", type: "checkbox", title: "Is Married" }
    ]
});


$("#pager").on("change", function() {
    var page = parseInt($(this).val(), 10);
    $("#loading").jsGrid("openPage", page);
});

/**********************************
*      Custom View Scenario       *
**********************************/

$("#customView").jsGrid({
    width: "100%",
    filtering: true,
    editing: true,
    sorting: true,
    paging: true,
    autoload: true,

    pageSize: 15,
    pageButtonCount: 5,

    controller: db,

    fields: [
        { name: "Name", type: "text", width: 150 },
        { name: "Age", type: "number", width: 50 },
        { name: "Address", type: "text", width: 200 },
        { name: "Country", type: "select", items: db.countries, valueField: "Id", textField: "Name" },
        { name: "Married", type: "checkbox", title: "Is Married", sorting: false },
        { type: "control", modeSwitchButton: false, editButton: false }
    ]
});


$(".config-panel input[type=checkbox]").on("click", function() {
    var $cb = $(this);
    $("#customView").jsGrid("option", $cb.attr("id"), $cb.is(":checked"));
});

/**************************
*      Batch Delete       *
**************************/

$("#batchDelete").jsGrid({
    width: "100%",
    autoload: true,
    confirmDeleting: false,
    paging: true,
    controller: {
        loadData: function() {
            return db.clients;
        }
    },
    fields: [
        {
            headerTemplate: function() {
                return $("<button>").attr("type", "button").text("Delete") .addClass("btn btn-primary mr-1")
                        .on("click", function () {
                            deleteSelectedItems();
                        });
            },
            itemTemplate: function(_, item) {
                return $("<input>").attr("type", "checkbox")
                        .prop("checked", $.inArray(item, selectedItems) > -1)
                        .on("change", function () {
                            $(this).is(":checked") ? selectItem(item) : unselectItem(item);
                        });
            },
            align: "center",
            width: 50
        },
        { name: "Name", type: "text", width: 150 },
        { name: "Age", type: "number", width: 50 },
        { name: "Address", type: "text", width: 200 }
    ]
});


// var selectedItems = [];
//
// var selectItem = function(item) {
//     selectedItems.push(item);
// };
//
// var unselectItem = function(item) {
//     selectedItems = $.grep(selectedItems, function(i) {
//         return i !== item;
//     });
// };
//
// var deleteSelectedItems = function() {
//     if(!selectedItems.length || !confirm("Are you sure?"))
//         return;
//
//     deleteClientsFromDb(selectedItems);
//
//     var $grid = $("#batchDelete");
//     $grid.jsGrid("option", "pageIndex", 1);
//
//     $grid.jsGrid("loadData");
//
//     selectedItems = [];
// };
//
// var deleteClientsFromDb = function(deletingClients) {
//     db.clients = $.map(db.clients, function(client) {
//         return ($.inArray(client, deletingClients) > -1) ? null : client;
//     });
// };

/*************************************
*      External Pager Scenario       *
*************************************/

$("#external").jsGrid({
    width: "100%",
    paging: true,
    pageSize: 15,
    pageButtonCount: 5,
    pagerContainer: "#externalPager",
    pagerFormat: "current page: {pageIndex} &nbsp;&nbsp; {first} {prev} {pages} {next} {last} &nbsp;&nbsp; total pages: {pageCount}",
    pagePrevText: "<",
    pageNextText: ">",
    pageFirstText: "<<",
    pageLastText: ">>",
    pageNavigatorNextText: "&#8230;",
    pageNavigatorPrevText: "&#8230;",

    data: db.clients,

    fields: [
        { name: "Name", type: "text", width: 150 },
        { name: "Age", type: "number", width: 50 },
        { name: "Address", type: "text", width: 200 },
        { name: "Country", type: "select", items: db.countries, valueField: "Id", textField: "Name" },
        { name: "Married", type: "checkbox", title: "Is Married" }
    ]
});

/************************
*      Reordering       *
************************/

$("#reordering").jsGrid({
    width: "100%",
    autoload: true,

    rowClass: function(item, itemIndex) {
        return "client-" + itemIndex;
    },

    controller: {
        loadData: function() {
            return db.clients.slice(0, 15);
        }
    },

    fields: [
        { name: "Name", type: "text", width: 150 },
        { name: "Age", type: "number", width: 50 },
        { name: "Address", type: "text", width: 200 },
        { name: "Country", type: "select", items: db.countries, valueField: "Id", textField: "Name" },
        { name: "Married", type: "checkbox", title: "Is Married", sorting: false }
    ],

    onRefreshed: function() {
        var $gridData = $("#reordering .jsgrid-grid-body tbody");

        $gridData.sortable({
            update: function(e, ui) {
                // array of indexes
                var clientIndexRegExp = /\s*client-(\d+)\s*/;
                var indexes = $.map($gridData.sortable("toArray", { attribute: "class" }), function(classes) {
                    console.log(clientIndexRegExp.exec(classes));
                    return clientIndexRegExp.exec(classes)[1];
                });

                // arrays of items
                var items = $.map($gridData.find("tr"), function(row) {
                    return $(row).data("JSGridItem");
                });
                console && console.log("Reordered items", items);
            }
        });
    }
});



});
