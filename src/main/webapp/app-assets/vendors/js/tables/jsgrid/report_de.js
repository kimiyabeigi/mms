(function() {
    var url = new URL(window.location);
    var id = url.searchParams.get("id");
    $.ajax({
        type: "GET",
        dataType: 'json',
        contentType : "application/json",
       url: "api/psp/register/report/detail?pspRequestId="+id,
        success: function (response,status, xhr){
            if(response.status == "SUCCESS"){
window.dataexport=response.data;
                $('.tr_remove_account_info').remove();
                i = 0;
                while (i < response.data.length) {
                    var trHTML_account_info_table = '<tr class="tr_remove_account_info">' +
                        '<td style="font-size: 11px;padding: 0;text-align: center;vertical-align: middle">' + response.data[i].createDate + '</td>' +
                        '<td style="font-size: 11px;padding: 0;text-align: center;vertical-align: middle">' + response.data[i].createTime + '</td>' +
                        '<td style="font-size: 11px;padding: 0;text-align: center;vertical-align: middle">' + response.data[i].branch + '</td>' +
                        '<td style="font-size: 11px;padding: 0;text-align: center;vertical-align: middle">' + response.data[i].userCode + '</td>' +
                        '<td style="font-size: 11px;padding: 0;text-align: center;vertical-align: middle">' + response.data[i].requestStatus + '</td>' +
                        '<td style="font-size: 11px;padding: 0;text-align: center;vertical-align: middle">' + response.data[i].requestNumber + '</td>' +
                        '<td style="font-size: 11px;padding: 0;text-align: center;vertical-align: middle">' + response.data[i].pspTrackingCode + '</td>' +
                        '<td style="font-size: 11px;padding: 0;text-align: center;vertical-align: middle">' + response.data[i].service + '</td>' +
                        '<td style="font-size: 11px;padding: 0;text-align: center;vertical-align: middle">' + response.data[i].statusType + '</td>' +
                        '<td style="font-size: 11px;padding: 0;text-align: center;vertical-align: middle">' + response.data[i].description + '</td>' +
                        '<tr>';
                    i++;
                    $('#account_info_table').append(trHTML_account_info_table);
                }

            }else {
                    swal("اطلاعاتی برای نمایش وجود ندارد")
            }
        },

        async: false

    });

    $('#export_ex').click(function () {
        window.location = "api/psp/export/detail.xls?id="+id

    })

}());
// $('#export_ex').click(function () {
//     $.ajax({
//         type: "GET",
//         dataType: 'json',
//         contentType : "application/json",
//         // data:dataexport,
//        url: "api/psp/download.xls",
//         success: function (response,status, xhr){
//
//         },
//
//         async: false
//
//     });
//
// })

