(function() {
$('#btn_casetraceunknown').click(function () {
    requestNumber = $('#requestNumber').val();

    $('#req_tracedesc').val('')
    $('#req_tracedesc').val('')
    $.ajax({
        type: "PUT",
        dataType: 'json',
        contentType : "application/json",

        url: "api/psp/trace/case/unknown?requestNumber="+requestNumber,
        success: function (result,status, xhr){
            if(result.status=="SUCCESS"){

                $('#req_tracedesc').val(result.data[0].errorDesc);
                $('#req_tracecode').val(result.data[0].pspTrackingCode);
            }
            else{
                sweet_ALert(result.description);
            }


           ;

        },

        async: false

    });
});

$('#btn_pspTrackingCode').click(function () {

    pspTrackingCode = $('#pspTrackingCode').val();
    $('#req_tracestatus').val('');
    $('#clientTypeDesc1').val('');

    $.ajax({
        type: "PUT",
        dataType: 'json',
        contentType : "application/json",

        url: "api/psp/trace/case/accept?pspTrackingCode="+pspTrackingCode,
        success: function (result,status, xhr){
            if(result.status=="SUCCESS"){

                $('#req_tracedesc1').val(result.data[0].errorDesc);
                $('#req_tracestatus').val(result.data[0].status);
            }
            else{
                sweet_ALert(result.description);
            }


            ;

        },

        async: false

    });

});

$('#btn_pspTrackingCode2').click(function () {

    pspTrackingCode2 = $('#pspTrackingCode2').val();
    $('#clientTypeDesc2').val('')
    $('#req_trace').val('')
    $('#req_terminal').val('')
    $.ajax({
        type: "PUT",
        dataType: 'json',
        contentType : "application/json",

        url: "api/psp/trace/case/register?pspTrackingCode="+pspTrackingCode2,
        success: function (result,status, xhr){
            if(result.status=="SUCCESS"){

                $('#clientTypeDesc2').val(result.data[0].errorDesc);
                $('#req_trace').val(result.data[0].PSPAcceptanceCode);
                $('#req_terminal').val(result.data[0].PSPTerminalCode);



            }
            else{
                sweet_ALert(result.description);
            }


            ;

        },

        async: false

    });

});

}());