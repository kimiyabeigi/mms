(function() {

    var db = {

        loadData: function(filter) {
            return $.grep(this.clients, function(client) {
                return (!filter.name || client.name.toLowerCase().indexOf(filter.name) > -1 || client.name.toUpperCase().indexOf(filter.name) > -1)
                // && (!filter.indexId || client.indexId === filter.indexId)
                && (!filter.indexCode || client.indexCode.toLowerCase().indexOf(filter.indexCode) > -1 || client.indexCode.toUpperCase().indexOf(filter.indexCode) > -1)
                // && (!filter.Country || client.Country === filter.Country)
                // && (filter.Married === undefined || client.Married === filter.Married);
            });

        },

        insertItem: function(insertingClient) {
            this.clients.push(insertingClient);
            baseIndex = {
                indexId:null,
                indexCode:insertingClient.indexCode,
                name:insertingClient.name,
                deleted:false
            }

            $.ajax({
                type: "PUT",
                dataType: 'json',
                contentType : "application/json",
                data:JSON.stringify(baseIndex),
                url: "api/base/index",
                success: function (result) {

                    insert_confirm(result);

                },
                error:function (response, status) {

                    insert_error(response);
                },
                async: false

            });
        },

        updateItem: function(updatingItem) {

            baseIndex = {
                indexId:updatingItem.indexId,
                indexCode:updatingItem.indexCode,
                name:updatingItem.name,
                deleted:false
            }

            $.ajax({
                type: "POST",
                dataType: 'json',
                contentType : "application/json",
                 data:JSON.stringify(baseIndex),
                 url: "api/base/index",

                success: function (result) {
                    update_success(result)
                },
                error:function (response, status,xhr) {
                    update_error(response)
                },
                async: false

            });

        },


        deleteItem: function(deletingClient) {
            var clientIndex = $.inArray(deletingClient, this.clients);
            $.ajax({
                type: "DELETE",
                dataType: 'json',
                contentType : "application/json",

                url: "api/base/index/"+deletingClient.indexId+"",
                success: function (result) {
                    if(result.status=="SUCCESS"){
                        delete_confirm(result.description)
                    }
                    else{

                        delete_error(result.description)
                    }
                },
                 async:false

             });

        }

    };

    window.db = db;
    $.ajax({
        type: "GET",
        dataType: 'json',
        contentType : "application/json",

        url: "api/base/index",
        success: function (result,status, xhr){

            if(result.status == "SUCCESS"){
                listFactor =  result.data;

                db.clients =listFactor;
            }else {
                listFactor=[];
                db.clients =listFactor;
            }
        },

        async: false

    });



}());