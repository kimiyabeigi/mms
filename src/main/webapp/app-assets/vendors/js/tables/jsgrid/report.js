(function() {
    localStorage.setItem('listtrackingcode','')

    var db = {

        loadData: function(filter) {
            return $.grep(db.clients, function(client) {
                return (!filter.requestDate || client.requestDate.indexOf(filter.requestDate) > -1)
                    // && (!filter.indexId || client.indexId === filter.indexId)
                    && (!filter.accountNo || client.accountNo.indexOf(filter.accountNo) > -1)
                    && (!filter.clientNumber || client.clientNumber.indexOf(filter.clientNumber) > -1)
                    && (!filter.branch || client.branch.indexOf(filter.branch) > -1)
                    && (!filter.companyName || client.companyName.indexOf(filter.companyName) > -1)
                    && (!filter.requestStatus || client.requestStatus.indexOf(filter.requestStatus) > -1)
                    && (!filter.pspTrackingCode || client.pspTrackingCode.indexOf(filter.pspTrackingCode) > -1)
                    && (!filter.pspAcceptanceCode || client.pspAcceptanceCode.indexOf(filter.pspAcceptanceCode) > -1)
                    && (!filter.pspTerminalCode || client.pspTerminalCode.indexOf(filter.pspTerminalCode) > -1)
                    && (!filter.description || client.description.indexOf(filter.description) > -1)
                    && (!filter.requestNumber.toString() || client.requestNumber.toString().indexOf(filter.requestNumber.toString()) > -1);
                    // && (!filter.requestNumber || client.requestNumber === filter.requestNumber);
            });

            // return db.clients;
        },
    };

    window.db = db;
    // if( $.session.get('status') == '0'){


    var obj = localStorage.getItem("tablereport")


    if (obj == 100 || !obj || obj == "undefined") {

        listGuild=[];
        db.clients =listGuild;


    }else{

        var obj = JSON.parse(localStorage.getItem("tablereport"))

                window.listGuild =  obj;
                db.clients =listGuild;
            }

    localStorage.setItem("tablereport",100)


$('#submit_excel').hide();

    $.ajax({
        type: "GET",
        dataType: 'json',
        contentType : "application/json",
        url: "api/sys/branch",
        success: function (result,status, xhr){
            if(result.status == "SUCCESS"){
                j = 0;
                while (j < result.data.length) {
                    var optinHTML = '<option class="option_remove" value="' + result.data[j].branchId + '">' + result.data[j].name + '</option>';
                    j++
                    $('#branchId').append(optinHTML);
                }
            }else {
                    alert("لود شعبه ناموفق")
            }
        },

        async: false

    });
    $.ajax({
        type: "GET",
        dataType: 'json',
        contentType : "application/json",
        url: "api/base/company",
        success: function (result,status, xhr){
            if(result.status == "SUCCESS"){
                j = 0;
                while (j < result.data.length) {
                    var optinHTML = '<option class="option_remove" value="' + result.data[j].companyId + '">' + result.data[j].name + '</option>';
                    j++
                    $('#listcompany_select').append(optinHTML);
                }
            }else {
                    alert("لود شعبه ناموفق")
            }
        },

        async: false

    });


    $('#submit_report').click(function () {
        report = {
            requestDateFrom:$('#requestDateFrom').val() ,
            requestDateTo:$('#requestDateTo').val(),
            clientNumber:$('#clientNumber').val(),
            pspTrackingCode:$('#pspTrackingCode').val(),
            accountNo:$('#accountNo').val(),
            pspAcceptanceCode:$('#pspAcceptanceCode').val(),
            companyId:$('#listcompany_select').val(),
            requestNumber:$('#requestNumber').val(),
            pspTerminalCode:$('#pspTerminalCode').val(),
            branchId:$('#branchId').val(),
            requestStatusId:$('#requestStatusId').val()
        }
        if(report.requestDateFrom > report.requestDateTo){
            sweet_ALert("فیلد تاریخ شروع درخواست از  فیلد تاریخ پایان درخواست بزرگتر می باشد ")

        }
        else {

            $.ajax({
                       type: "POST",
                       dataType: 'json',
                       contentType : "application/json",
                       data:JSON.stringify(report),
                       url: "api/psp/register/report/master",
                       success: function (response,status, xhr){
                           if(response.status == "SUCCESS"){

                               localStorage.setItem("tablereport",JSON.stringify(response.data));
                               location.reload();
            //
            //                    $('#submit_excel').show();

                               // $('.tr_remove_account_info').remove();

                               // i = 0;
                               // while (i < response.data.length) {
                               //     var trHTML_account_info_table = '<tr class="tr_remove_account_info">' +
                               //                                     '<td style="font-size: 11px;padding: 0;text-align: center;vertical-align: middle">' + response.data[i].requestDate + '</td>' +
                               //                                     '<td style="font-size: 11px;padding: 0;text-align: center;vertical-align: middle">' + response.data[i].accountNo + '</td>' +
                               //                                     '<td style="font-size: 11px;padding: 0;text-align: center;vertical-align: middle">' + response.data[i].clientNumber + '</td>' +
                               //                                     '<td style="font-size: 11px;padding: 0;text-align: center;vertical-align: middle">' + response.data[i].branch + '</td>' +
                               //                                     '<td style="font-size: 11px;padding: 0;text-align: center;vertical-align: middle">' + response.data[i].companyName + '</td>' +
                               //                                     '<td style="font-size: 11px;padding: 0;text-align: center;vertical-align: middle">' + response.data[i].requestStatus + '</td>' +
                               //                                     '<td style="font-size: 11px;padding: 0;text-align: center;vertical-align: middle">' + response.data[i].requestNumber + '</td>' +
                               //                                     '<td style="font-size: 11px;padding: 0;text-align: center;vertical-align: middle">' + response.data[i].pspTrackingCode + '</td>' +
                               //                                     '<td style="font-size: 11px;padding: 0;text-align: center;vertical-align: middle">' + response.data[i].pspAcceptanceCode + '</td>' +
                               //                                     '<td style="font-size: 11px;padding: 0;text-align: center;vertical-align: middle">' + response.data[i].pspTerminalCode + '</td>' +
                               //                                     '<td><button class="btn btn-brown" onclick="report_detail('+response.data[i].pspRequestId+')">  جزییات</button> </td>' +
                               //                                     '<tr>';
                               //     i++;
                               //     $('#account_info_table').append(trHTML_account_info_table);
                               //
                               // }

                           }else {
                               swal(response.description)
                           }
                       },
                   //
                       async: false
                   //
                   });



        }

    })


    $('#submit_excel').click(function () {
        window.location = "api/psp/export/master.xls?" +
            "requestDateFrom="+$('#requestDateFrom').val()+
            "&requestDateTo="+$('#requestDateTo').val()+
            "&clientNumber="+$('#clientNumber').val()+
            "&pspTrackingCode="+$('#pspTrackingCode').val()+
            "&accountNo="+$('#accountNo').val()+
            "&pspAcceptanceCode="+$('#pspAcceptanceCode').val()+
            "&companyId="+$('#listcompany_select').val()+
            "&requestNumber="+$('#requestNumber').val()+
            "&pspTerminalCode="+$('#pspTerminalCode').val()+
            "&branchId="+$('#branchId').val()+
            "&requestStatusId="+$('#requestStatusId').val();
    })


}())
function report_detail(id) {
    window.location = ctx + "/report_de?id="+id;
}
$('#claerform_report').click(function () {

    $('input[type=text]').val('');
    $('#requestStatusId').val('')
    $('#branchId').val('')
    $('#listcompany_select').val('')

})
