(function() {

    var db = {

        loadData: function(filter) {
            return $.grep(this.clients, function(client) {
                return (!filter.userName || client.userName.indexOf(filter.userName) > -1)
                    && (!filter.userCode || client.userCode === filter.userCode)
                    && (!filter.firstName || client.firstName.indexOf(filter.firstName) > -1)
                    && (!filter.lastName || client.lastName.indexOf(filter.lastName) > -1)
                && (!filter.roleId  || client.roleId === filter.roleId)
                && (!filter.branchId || client.branchId === filter.branchId);
            });

        },

        insertItem: function(insertingClient) {

        },

        updateItem: function(updatingItem) {

        // testkmayar = {
        //     userId: updatingItem.userId,
        //     userCode: updatingItem.userCode,
        //     firstName: updatingItem.firstName,
        //     lastName: updatingItem.lastName,
        //     userName: updatingItem.userName,
        //     password: updatingItem.password,
        //     roleId: updatingItem.roleId,
        //     branchId: updatingItem.branchId,
        //     firstLogin: updatingItem.firstLogin,
        //     isActive: updatingItem.active
        // }



            $.ajax({
                type: "POST",
                dataType: 'json',
                contentType : "application/json",
                data:JSON.stringify(updatingItem),
               url: "api/user",

                success: function (result) {
                    update_success(result)
                },
                error:function (response, status,xhr) {
                    update_error(response)
                },
                async: false

            });

        },


        deleteItem: function(deletingClient) {
            var clientIndex = $.inArray(deletingClient, this.clients);
            $.ajax({
                type: "DELETE",
                dataType: 'json',
                contentType : "application/json",

               url: "api/user/"+deletingClient.userId+"",
                success: function (result) {
                    if(result.status=="SUCCESS"){
                        delete_confirm(result.description)
                    }
                    else{

                        delete_error(result.description)
                    }
                },
            });

        }

    };

    window.db = db;
    $.ajax({
        type: "GET",
        dataType: 'json',
        contentType : "application/json",

       url: "api/user",
        success: function (result,status, xhr){

            if(result.status == "SUCCESS"){
                listGuild =  result.data;

                db.clients =listGuild;
            }else {
                listGuild=[];
                db.clients =listGuild;
            }
        },

        async: false

    });

    $.ajax({
        type: "GET",
        dataType: 'json',
        contentType : "application/json",
       url: "api/sys/branch",

        success: function (result) {
            j = 0;
            while (j < result.data.length) {
                var optinHTML = '<option class="option_remove" value="' + result.data[j].branchId + '">' + result.data[j].name + '</option>';
                j++
                $('#branchId').append(optinHTML);
            }
            if(result.status == "SUCCESS")
            {
                user_branch =  result.data;
                db.user_branch =user_branch;
            }
            else
            {
                user_branch=[];
                db.user_branch =user_branch;
            }
        },
        error:function (response, status,xhr) {
            alert(response)
        },
        async: false

    });


    $.ajax({
        type: "GET",
        dataType: 'json',
        contentType : "application/json",
       url: "api/role",
        success: function (result) {
            j = 0;
            while (j < result.data.length) {
                var optinHTML = '<option class="option_remove" value="' + result.data[j].roleId + '">' + result.data[j].name + '</option>';
                j++
                $('#roleId').append(optinHTML);
            }
            if(result.status == "SUCCESS")
            {
                user_role =  result.data;
                db.user_role =user_role;
            }
            else
            {
                user_role=[];
                db.user_role =user_role;
            }
        },
        error:function (response, status,xhr) {
            alert(response)
        },
        async: false

    });

}());