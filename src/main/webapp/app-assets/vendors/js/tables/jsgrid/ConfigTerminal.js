(function() {

    var db1 = {

        loadData: function(filter) {
            return $.grep(db1.clients, function(client) {
                return (!filter.name || client.name.indexOf(filter.name) > -1)
                    && (!filter.terminalId || client.terminalId === filter.terminalId)
                    // && (!filter.Address || client.Address.indexOf(filter.Address) > -1)
                    && (!filter.terminal || client.terminal === filter.terminal)
                // && (filter.Married === undefined || client.Married === filter.Married);
            });
        },

        insertItem: function(insertingClient) {
            if($.session.get("id_company_modal") == "N"){
                this.clients.push(insertingClient);
                baseTerminalDetail = {
                    companySettingTerminalId : null,
                    companySettingId : $.session.get('companySettingId'),
                    terminalDetailId:insertingClient.terminalDetailId,
                    active:insertingClient.active,
                    connectionCode:insertingClient.connectionCode,
                    deleted:false
                }
            }else{
                this.clients.push(insertingClient);
                baseTerminalDetail = {
                    companySettingTerminalId : insertingClient.companySettingTerminalId,
                    companySettingId : $.session.get("id_company") ,
                    terminalDetailId:insertingClient.terminalDetailId,
                    active:insertingClient.active,
                    connectionCode:insertingClient.connectionCode,
                    deleted:false
                }
            }

            $.ajax({
                type: "PUT",
                dataType: 'json',
                contentType : "application/json",
                data:JSON.stringify(baseTerminalDetail),
               url: "api/company/setting/terminal",
                // success: function (result) {
                //     alert(result.description)
                //     db1.clients =  result.status;
                //     $.session.set('inserterror', result.status);
                //
                // },
                success: function (response) {
                    db1.clients =  response.status;
                    insert_confirm_company_setting(response);


                },
                error:function (response, status) {
                    insert_error(response);
                },
                async: false

            });
            setTimeout(function(){ reloadtableconfigterminal();    }, 3000);

        },

        updateItem: function(updatingItem) {
            baseTerminalDetail = {
                companySettingTerminalId:updatingItem.companySettingTerminalId,
                companySettingId : $.session.get("id_company"),
                terminalDetailId:updatingItem.terminalDetailId,
                active:updatingItem.active,
                connectionCode:updatingItem.connectionCode,
                deleted:false
            }

            $.ajax({
                type: "POST",
                dataType: 'json',
                contentType : "application/json",
                data:JSON.stringify(baseTerminalDetail),
               url: "api/company/setting/terminal",

                // success:function(response) {
                //
                //     statuserror=response.status;
                //     $.session.set('statuserror', statuserror);
                //     alert(response.description)
                // },
                success: function (result) {
                    update_success(result)
                },
                error:function (response, status,xhr) {
                    update_error(response)
                },
                async: false

            });
        },


        deleteItem: function(deletingClient) {

        }

    };

    window.db1 = db1;

    var id=$.session.get("id_company");

    if($.session.get("id_company_modal") == "N"){
        listService=[];
        db1.clients =listService;
    }
    else {

        $.ajax({
            type: "GET",
            dataType: 'json',
            contentType : "application/json",

           url: "api/company/setting/terminal/master/"+id,
            success: function (result,status, xhr){
                if(result.status == "SUCCESS"){

                    listService =  result.data;
                    db1.clients =listService;

                }else {

                    listService=[];
                    db1.clients =listService;
                }
            },
        });

    }
    // $.ajax({
    //     type: "GET",
    //     dataType: 'json',
    //     contentType : "application/json",
    //
    //    url: "api/company/setting/terminal",
    //     success: function (result,status, xhr){
    //
    //         if(result.status == "SUCCESS"){
    //
    //             listTerminal_de =  result.data;
    //             db1.clients =listTerminal_de;
    //         }else {
    //             listTerminal_de=[];
    //             db1.clients =listTerminal_de;
    //         }
    //     },
    //
    //     async: false
    //
    // });

    $.ajax({
        type: "GET",
        dataType: 'json',
        contentType : "application/json",

        url: "api/base/terminalDetail",
        success: function (result,status, xhr){

            if(result.status == "SUCCESS"){

                listTerminal_de =  result.data;
                db1.terminalDetail =listTerminal_de;
            }else {
                listTerminal_de=[];
                db1.terminalDetail =listTerminal_de;
            }
        },

        async: false

    });

    function reloadtableconfigterminal() {
        $.ajax({
                   type: "GET",
                   dataType: 'json',
                   contentType : "application/json",
                   url: "api/company/setting/terminal/master/"+id,
                   success: function (result,status, xhr){
                       if(result.status == "SUCCESS"){
                           listService =  result.data;
                           db1.clients =listService;
                       }else {
                           listService=[];
                           db1.clients =listService;
                       }
                   },
                   async: false

               });
        $("#ConfigTerminal").jsGrid("search");
    }
}());