(function() {

    var db = {

        loadData: function(filter) {
            return $.grep(this.clients, function(client) {
                return (!filter.name || client.name.toLowerCase().indexOf(filter.name) > -1 || client.name.toUpperCase().indexOf(filter.name) > -1)
                && (!filter.terminalDetailCode || client.terminalDetailCode === filter.terminalDetailCode)
                // && (!filter.name || client.name.indexOf(filter.name) > -1)
                && (!filter.terminalId || client.terminalId === filter.terminalId)
                // && (filter.Married === undefined || client.Married === filter.Married);
            });

        },

        insertItem: function(insertingClient) {
            this.clients.push(insertingClient);
            baseTerminalDetail = {
                terminalDetailId:null,
                terminalDetailCode:insertingClient.terminalDetailCode,
                terminalId:insertingClient.terminalId,
                name:insertingClient.name,
                deleted:false
            }

            $.ajax({
                type: "PUT",
                dataType: 'json',
                contentType : "application/json",
                data:JSON.stringify(baseTerminalDetail),
               url: "api/base/terminalDetail",
                success: function (result) {

                    insert_confirm(result);

                },
                error:function (response, status) {

                    insert_error(response);
                },
                async: false

            });
        },

        updateItem: function(updatingItem) {

            baseTerminalDetail = {
                terminalDetailId:updatingItem.terminalDetailId,
                terminalDetailCode:updatingItem.terminalDetailCode,
                terminalId:updatingItem.terminalId,
                name:updatingItem.name,
                deleted:false
            }

            $.ajax({
                type: "POST",
                dataType: 'json',
                contentType : "application/json",
                data:JSON.stringify(baseTerminalDetail),
               url: "api/base/terminalDetail",

                success: function (result) {
                    update_success(result)
                },
                error:function (response, status,xhr) {
                    update_error(response)
                },
                async: false

            });
        },


        deleteItem: function(deletingClient) {

            var clientIndex = $.inArray(deletingClient, this.clients);
            $.ajax({
                type: "DELETE",
                dataType: 'json',
                contentType : "application/json",

               url: "api/base/terminalDetail/"+deletingClient.terminalDetailId+"",
                success: function (result) {
                    if(result.status=="SUCCESS"){
                    delete_confirm(result.description)
                    }
                    else{

                        delete_error(result.description)
                    }
                },
                async:false
            });
        }

    };

    window.db = db;



    // db.terminal =[
    //     {Name : "POS",Id:1},
    //     {Name : "MOB",Id:2},
    //         {Name : "IPG",Id:3}
    // ];




    $.ajax({
        type: "GET",
        dataType: 'json',
        contentType : "application/json",

       url: "api/sys/terminal/filter",
        success: function (result,status, xhr){

            if(result.status == "SUCCESS"){

                listTerminal =  result.data;
                db.terminal =listTerminal;
            }else {
                listTerminal=[];
                db.terminal =listTerminal;
            }
        },

        async: false

    });


    $.ajax({
        type: "GET",
        dataType: 'json',
        contentType : "application/json",

       url: "api/base/terminalDetail",
        success: function (result,status, xhr){

            if(result.status == "SUCCESS"){

                listTerminal_de =  result.data;
                db.clients =listTerminal_de;
            }else {
                listTerminal_de=[];
                db.clients =listTerminal_de;
            }
        },

        async: false

    });

    $.ajax({
        type: "GET",
        dataType: 'json',
        contentType : "application/json",
       url: "api/base/company",

        success:function(response) {
            i=0;
            while (i<response.data.length){
                var itemoption = '<option value="'+response.data[i].companyId+'">'+response.data[i].name+'</option>' ;
                i++
                $('#itemoption_company').append(itemoption);

            }

        },
        async: false
    });

    $.ajax({
        type: "GET",
        dataType: 'json',
        contentType : "application/json",
       url: "api/sys/terminal",

        success:function(response) {
            j=0;
            while (j<response.data.length){
            var trHTML =
                '<label class="inline custom-control custom-checkbox block">'+
                '<input type="checkbox" data="'+response.data[j].terminalId+'" class="custom-control-input checkbox12" name="terminal[]" value="'+response.data[j].terminalId+'">'+
                '<span class="custom-control-indicator"></span>'+
                '<span class="custom-control-description ml-0"></span>'+
                '</label>'+response.data[j].name+'';
            $('#itemcheckbox_company').append(trHTML);
                j++;
            }
        },
        async: false
    });


    $('form').submit(function(event) {
        // var terminal = new Array();
        // $("input:checked").each(function() {
        //     terminal.push($(this).val());
        // });

        var ar=[]
        $('.checkbox12').each(function () {
            if($(this).is(':checked')){
                ar.push({terminalId:$(this).attr('data'),companySettingDetailId:null,companySettingId:null,selected:true});
            }
        });
        // alert(JSON.stringify(ar));
        // myJson = {obj: {}};
        // i=0;
        // while(i < terminal.length){
        //     var companySettingDetailId = null;
        //     var companySettingId = null;
        //     var terminalId = terminal[i];
        //     var selected = true;
        //     myJson.obj["companySettingDetailId"] = companySettingDetailId;
        //     myJson.obj["companySettingId"] = companySettingId;
        //     myJson.obj["terminalId"] = terminalId;
        //     myJson.obj["selected"] = selected;
        //     i++;
        // }
        // alert(JSON.stringify(myJson));
        // get the form data

        // there are many ways to get this data using jQuery (you can use the class or id also)
        var formData = {
            'companyId'              : $('input[name=companyname]').val(),
            'companySettingId'              : null,
            'organizationCode'                   : $('input[name=code_req]').val(),
            'companySettingDetails'          : JSON.stringify(ar),
            'active'            :true
        };

        // process the form
        $.ajax({
            type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
            url         : 'api/sys/terminal', // the url where we want to POST
            data        : JSON.stringify(formData), // our data object
            dataType    : 'json', // what type of data do we expect back from the server
            encode          : true
        })
        // using the done promise callback
            .done(function(data) {

                // log data to the console so we can see
                console.log(data);

                // here we will handle errors and validation messages
            });

        // stop the form from submitting the normal way and refreshing the page
        event.preventDefault();
    });

}());

