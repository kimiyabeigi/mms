(function() {
    localStorage.setItem('listtrackingcode','')

    var db = {

        loadData: function(filter) {
            return $.grep(db.clients, function(client) {
                return (!filter.createDate || client.createDate.indexOf(filter.createDate) > -1)
                    // && (!filter.indexId || client.indexId === filter.indexId)
                    && (!filter.pspTrackingCode || client.pspTrackingCode.indexOf(filter.pspTrackingCode) > -1)

                    && (!filter.accountNo || client.accountNo.indexOf(filter.accountNo) > -1)
                    && (!filter.clientNumber || client.clientNumber.indexOf(filter.clientNumber) > -1)
                    && (!filter.branchName || client.branchName.indexOf(filter.branchName) > -1)
                    && (!filter.pspAcceptanceCode || client.pspAcceptanceCode.indexOf(filter.pspAcceptanceCode) > -1)
                    && (!filter.requestNumber || client.requestNumber === filter.requestNumber);
            });

            // return db.clients;
        },
    };

    window.db = db;
    // if( $.session.get('status') == '0'){

        $.ajax({
                   type: "GET",
                   dataType: 'json',
                   contentType : "application/json",
                  url: "api/psp/trace/batch/unknown",
                   success: function (result,status, xhr){
                       $.session.set('unknown',JSON.stringify(result.data))
                       $.session.set('status','1')
                       if(result.status == "SUCCESS"){
                           // alert(JSON.stringify(result.data))
                           window.listGuild =  result.data;
                           db.clients =listGuild;
                       }else {
                           listGuild=[];
                           db.clients =listGuild;
                       }
                   },
                   async: false
               });

    // }
    // else{
    //     $.session.set('status','0')
    //     result = JSON.parse($.session.get('unknown'))
    //     if(result.status == "SUCCESS"){
    //         window.listGuild =  result.data;
    //         db.clients =listGuild;
    //     }else {
    //         listGuild=[];
    //         db.clients =listGuild;
    //     }

    // }


    // var array2 = [{"pspRequestId":572,"pspRequestStatusId":null,"requestStatusId":4,"createDate":"1397/03/13","accountNo":"0200435684603","clientNumber":"0040000321","branchName":"مير اصفهان","requestNumber":2572,"pspTrackingCode":null,"companyCode":"IK","selected":false,"status":"نامعلوم"},{"pspRequestId":573,"pspRequestStatusId":null,"requestStatusId":4,"createDate":"1397/03/13","accountNo":"0200435684603","clientNumber":"0040000321","branchName":"مير اصفهان","requestNumber":2573,"pspTrackingCode":null,"companyCode":"IK","selected":false,"status":"نامعلوم"},{"pspRequestId":576,"pspRequestStatusId":null,"requestStatusId":4,"createDate":"1397/03/13","accountNo":"0200435684603","clientNumber":"0040000321","branchName":"مير اصفهان","requestNumber":2576,"pspTrackingCode":null,"companyCode":"IK","selected":false,"status":"نامعلوم"}];
    function findindex_trace(requestNumber,pspTrackingCode,status)
    {
        var array3 = JSON.parse($.session.get('unknown'));
        // alert(array3)
        // var list2 = JSON.stringify(array3.data)
        var list2 = array3
        // alert(list2)
        var val = requestNumber
        var filteredObj = list2.find(function(item, i){
            if(item.requestNumber === val){
                // alert(i);

                list2[i].pspTrackingCode = pspTrackingCode
                list2[i].status = status
                // alert(JSON.stringify(list2))
                db.clients =list2;
                // alert(JSON.stringify(list2))
                $.session.set('unknown',JSON.stringify(list2))
                $("#ListGuild2").jsGrid("search");
                return i;
            }

        });
    }


    $('#gettrackingcode').click(function () {

        // data=[{
        //     pspRequestId:572,
        //     pspRequestStatusId:null,
        //     requestStatusId:4,
        //     createDate:1397/03/13,
        //     accountNo:"0200435684603",
        //     clientNumber:"0040000321",
        //     branchName:	"مير اصفهان",
        //     requestNumber:	2572,
        //     pspTrackingCode:null,
        //     companyCode:"IK",
        //     selected:false,
        //     status:"نامعلوم",
        //
        // }]
        // db.clients =data;
        // $("#ListGuild2").jsGrid("search");
        if(localStorage.getItem('listtrackingcode').length <= 2){

            sweet_ALert("حداقل یك گزینه را باید انتخاب كنید")

        }else
        {

            $.ajax({
                       type: "PUT",
                       dataType: 'json',
                       contentType: "application/json",
                       data: localStorage.getItem('listtrackingcode'),
                      url: "api/psp/trace/batch/unknown",
                       beforeSend: function () {
                           $('#sppiner').addClass('container_blur');
                           $('.spinner').show();

                       },
                       success: function (result, status, xhr) {
                           $('#sppiner').removeClass('container_blur');
                           $('.spinner').hide();
                           i = 0;
                           flag_error = 0;
                           localStorage.setItem('listtrackingcode','')

                           if (result.status == "ERROR")
                           {
                               sweet_ALert(result.description)

                           }
                           else
                           {
                               while (i < result.data.length)
                               {

                                   // $("#ListGuild2").jsGrid("updateItem", item, { pspTrackingCode: result.data[i].pspTrackingCode }).done(function() {
                                   //     console.log("update completed");
                                   //     $("#ListGuild2").jsGrid("refresh");
                                   // });
                                   if (result.data[i].status == "ERROR")
                                   {
                                       flag_error = 1
                                       descerror = result.data[i].errorDesc
                                   }
                                   else
                                   {
                                       findindex_trace(result.data[i].requestNumber, result.data[i].pspTrackingCode,result.data[i].errorDesc)

                                   }
                                   i++;

                               }
                               if (flag_error == 1)
                               {
                                   sweet_ALert(descerror)
                               }
                               else
                               {

                               }
                           }
                           // if(result.status == "SUCCESS"){
                           //     listGuild =  result.data;
                           //     localStorage.setItem("listtrackingcode",'')
                           //
                           //
                           //     db.clients =listGuild;
                           // }else {
                           //     listGuild=[];
                           //     db.clients =listGuild;
                           //     // swalert("",result.description)
                           // }
                       },

                       async: false
                   });
        }
    })



}());