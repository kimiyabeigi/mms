
$('#real_acquire_specification_2').hide();
$('#citizen_real_acquire_specification_2').hide();
$('#legal_acquire_specification_2').hide();
$('#account_info_3').hide();
$('#account_owners_info_4').hide();
$('#real_acquire_specification_5').hide();
$('#citizen_real_acquire_specification_5').hide();
$('#legal_acquire_specification_5').hide();
$('#store_info_ipg_6').hide();
$('#terminal_type_info_7').hide();


$.ajax({
    type: "GET",
    dataType: 'json',
    contentType : "application/json",
    url: "api/base/company/active",

    success:function(response) {
        i=0;
        while (i<response.data.length){
            var optinHTML = '<option value="'+response.data[i].companyId+'">'+response.data[i].name+'</option>';
            i++
            $('#provider_company').append(optinHTML);
        }

    },
    async: false
});

$('#provider_company').on('change', function() {
    $('#option_request_type').show();
    $.ajax({
        type: "GET",
        dataType: 'json',
        contentType : "application/json",
        url: "api/sys/terminal/reqister/"+this.value,

        success:function(response) {
            i=0;
            while (i<response.data.length){
                var optinHTML = '<option value="'+response.data[i].terminalId+'">'+response.data[i].name+'</option>';
                i++
                $('#request_type').append(optinHTML);
            }

        },
        async: false
    });
})

function register_request_back() {

    $('#register_request_1').show();
    $('#real_acquire_specification_2').hide();
}
function acquire_info2() {

    // privider_company =$('#provider_company').val();
    // request_type = $('#request_type').val();
    // customer_no = $('#customer_no').val();

    // if(customer_no.length <= 13 && customer_no != '' && request_type != null && privider_company != null ){
    // if(true ){
    //     $('#register_request_1').hide();
    //     $('#real_acquire_specification_2').show();
    // }
    // else{
    //     if(customer_no.length > 12)
    //         alert("شماره مشتری حداكثر 12 كاراكتر می باشد.")
    //
    //     else {
    //         alert("فیلدهای اجباری را پر نمایید")
    //     }
    // }
    var clientNumber = $('#customer_no').val()

    $.ajax({
        type: "GET",
        dataType: 'json',
        contentType : "application/json",

        url: "api/core/client/info?clientNumber="+clientNumber,

        success:function(response) {

            if(response.status == "SUCCESS"){
                //legal
                if(response.data.clientType == "L"){
                    alert("L")
                    $('#register_request_1').hide();
                    $('#legal_acquire_specification_2').show();


                //iranian individual
                }else if(response.data.countryNameAbbr == "IR"){

                    alert("IR")
                    $('#register_request_1').hide();
                    $('#real_acquire_specification_2').show();
                    if(!checkCodeMeli(response.data.nationalCode)){

                   alert("كد ملی صحیح نمی باشد")
                    $('#account_info_1').hide();
                    }
                    $('#clientId').val(response.data.clientId)
                    $('#clientRequestId').val(response.data.clientRequestId)
                    $('#positionId').val(response.data.positionId)
                    $('#clientNumber').val(response.data.clientNumber)
                    if(response.data.sex == "M"){
                        sex = "مرد"
                    }else {
                        sex = "زن"
                    }
                    $('#sex').val(sex)
                    if(response.data.lifeStatus == "Y"){
                        lifeStatus = "فوت شده"
                        $('#account_info_1').hide();
                        alert("مشتری در قید حیات نبوده و امكان ثبت در خواست وجود ندارد")

                    }else {
                        lifeStatus = "در قید حیات"
                    }
                    $('#lifeStatus').val(lifeStatus)
                    $('#firstName').val(response.data.firstName)
                    $('#firstNameEn').val(response.data.firstNameEn)
                    $('#lastName').val(response.data.lastName)
                    $('#lastNameEn').val(response.data.lastNameEn)
                    $('#fatherName').val(response.data.fatherName)
                    $('#fatherNameEn').val(response.data.fatherNameEn)


                    $('#nationalCode').val(response.data.nationalCode)
                    $('#regNumber').val(response.data.regNumber)
                    $('#foreignCode').val(response.data.foreignCode)
                    $('#passportNumber').val(response.data.passportNumber)
                    $('#birthDay').val(response.data.birthDay)
                    $('#passportExpiryDate').val(response.data.passportExpiryDate)
                    $('#countryNameAbbr').val(response.data.countryDesc)
                    $('#countryCode').val(response.data.countryDesc)
                    $('#issuanceDate').val(response.data.issuanceDate)
                    $('#issuancePlaceCode').val(response.data.issuancePlaceCode)
                    $('#issuancePlaceDesc').val(response.data.issuancePlaceDesc)
                    $('#mobilePhone').val(response.data.mobilePhone)
                    $('#phone').val(response.data.phone)
                    $('#fax').val(response.data.fax)
                    $('#postalCode').val(response.data.postalCode)
                    $('#address').val(response.data.address)
                    $('#clientType').val(response.data.clientType)
                    $('#clientKind').val(response.data.clientKind)
                    $('#companyName').val(response.data.companyName)
                    $('#companyNameEn').val(response.data.companyNameEn)
                    $('#commerceCode').val(response.data.commerceCode)
                    $('#clientTypeDesc').val("حقیقی")//response.data.clientType


                }
                //other country indivadual
                else{

                alert("khareji")
                    $('#register_request_1').hide();
                    $('#citizen_real_acquire_specification_2').show();

                    $('#clientId').val(response.data.clientId)
                    $('#clientRequestId').val(response.data.clientRequestId)
                    $('#positionId').val(response.data.positionId)
                    $('#c_clientNumber').val(response.data.clientNumber)
                    $('#c_sex').val(response.data.sex)
                    $('#c_lifeStatus').val(response.data.lifeStatus)
                    $('#c_firstName').val(response.data.firstName)
                    $('#c_firstNameEn').val(response.data.firstNameEn)
                    $('#c_lastName').val(response.data.lastName)
                    $('#c_lastNameEn').val(response.data.lastNameEn)
                    $('#c_fatherName').val(response.data.fatherName)
                    $('#c_fatherNameEn').val(response.data.fatherNameEn)
                    $('#c_nationalCode').val(response.data.nationalCode)
                    $('#c_regNumber').val(response.data.regNumber)
                    $('#c_foreignCode').val(response.data.foreignCode)
                    $('#c_passportnumber').val(response.data.passportNumber)
                    $('#c_birthDay').val(response.data.birthDay)
                    $('#c_passportExpiryDate').val(response.data.passportExpiryDate)
                    $('#c_countryNameAbbr').val(response.data.countryNameAbbr)
                    $('#c_countryCode').val(response.data.countryDesc)
                    $('#c_countryDesc').val(response.data.countryDesc)
                    $('#c_issuanceDate').val(response.data.issuanceDate)
                    $('#c_issuancePlaceCode').val(response.data.issuancePlaceDesc)
                    $('#c_issuancePlaceDesc').val(response.data.issuancePlaceDesc)
                    $('#c_mobilePhone').val(response.data.mobilePhone)
                    $('#c_phone').val(response.data.phone)
                    $('#c_fax').val(response.data.fax)
                    $('#c_postalCode').val(response.data.postalCode)
                    $('#c_address').val(response.data.address)
                    $('#c_clientType').val(response.data.clientType)
                    $('#c_clientKind').val(response.data.clientKind)
                    $('#c_companyName').val(response.data.companyName)
                    $('#c_companyNameEn').val(response.data.companyNameEn)
                    $('#c_commerceCode').val(response.data.commerceCode)

                }
            }
            else{
                alert(response.description)
            }



        },
        async: false
    });


}
function acquire_info_back2() {

    $('#real_acquire_specification_2').show();
    $('#account_info_3').hide();
}


function account_info() {

    // if ($('#phone').val().length != 11 ){
    //     alert("شماره تماس حداكثر 11 رقمی باشد")
    // }else if ($('#mobilePhone').val().length != 11 ){
    //     alert("شماره تماس حداكثر 11 رقمی باشد")
    // }else if ($('#postalCode').val().length != 10){
    //     alert("شماره تماس حداكثر 11 رقمی باشد")
    // }else if ($('#phone').val().length != 11 ){
    //     alert("شماره تماس حداكثر 11 رقمی باشد")
    // }else if(!$('#address').val()){
    //     alert("آدرس خالی می باشد")
    // }
    // else {


    // }


    $.ajax({
        type: "GET",
        dataType: 'json',
        contentType: "application/json",
        url: "api/core/account/info?clientNumber=" + $('#customer_no').val(),

        success: function (response) {
            //
            if (response.status == "SUCCESS") {
                alert(response.data.length)
                i=0;
                while (i < response.data.length) {

                    var trHTML_account_info_table = '<tr>' +
                        '<td><input type="radio" name="accountNo" value="' + response.data[i].accountNo + '"></td>' +
                        '<td>' + response.data[i].accountNo + '</td>' +
                        '<td>' + response.data[i].accountType + '</td>' +
                        '<td>' + response.data[i].branchId + '</td>' +
                        '<td>' + response.data[i].ibanNo + '</td>' +
                        '<td>' + response.data[i].branchCode + '</td>' +
                        '<tr>';
                    i++;
                    $('#account_info_table').append(trHTML_account_info_table);

                }
                $('#real_acquire_specification_2').hide();
                $('#account_info_3').show();
            }
            else {
                alert(response.description)
            }


        },
        async: false
    });

    // privider_company =$('#provider_company').val();
    // request_type = $('#request_type').val();
    // customer_no = $('#customer_no').val();
    // if(customer_no.length <= 13 && customer_no != '' && request_type != null && privider_company != null ){

    // $('#clientId').val(response.data[i].clientId)
    // $('#clientRequestId').val(response.data[i].clientRequestId)
    // $('#positionId').val(response.data[i].positionId)
    // $('#clientNumber').val(response.data[i].clientNumber)
    // $('#sex').val(response.data[i].sex)
    // $('#lifeStatus').val(response.data[i].lifeStatus)
    // $('#firstName').val(response.data[i].firstName)
    // $('#firstNameEn').val(response.data[i].firstNameEn)
    // $('#lastName').val(response.data[i].lastName)
    // $('#lastNameEn').val(response.data[i].lastNameEn)
    // $('#fatherName').val(response.data[i].fatherName)
    // $('#fatherNameEn').val(response.data[i].fatherNameEn)
    // $('#nationalCode').val(response.data[i].nationalCode)
    // $('#regNumber').val(response.data[i].regNumber)
    // $('#foreignCode').val(response.data[i].foreignCode)
    // $('#passportNumber').val(response.data[i].passportNumber)
    // $('#birthDay').val(response.data[i].birthDay)
    // $('#passportExpiryDate').val(response.data[i].passportExpiryDate)
    // $('#countryNameAbbr').val(response.data[i].countryNameAbbr)
    // $('#countryCode').val(response.data[i].countryCode)
    // $('#countryDesc').val(response.data[i].countryDesc)
    // $('#issuanceDate').val(response.data[i].issuanceDate)
    // $('#issuancePlaceCode').val(response.data[i].issuancePlaceCode)
    // $('#issuancePlaceDesc').val(response.data[i].issuancePlaceDesc)
    // $('#mobilePhone').val(response.data[i].mobilePhone)
    // $('#phone').val(response.data[i].phone)
    // $('#fax').val(response.data[i].fax)
    // $('#postalCode').val(response.data[i].postalCode)
    // $('#address').val(response.data[i].address)
    // $('#clientType').val(response.data[i].clientType)
    // $('#clientKind').val(response.data[i].clientKind)
    // $('#companyName').val(response.data[i].companyName)
    // $('#companyNameEn').val(response.data[i].companyNameEn)
    // $('#commerceCode').val(response.data[i].commerceCode)





}
function account_info_back() {

    $('#account_owners_info_4').hide();
    $('#account_info_3').show();




}


function account_owners_info() {

    var radio1=$('input[type="radio"]:checked').val();
    //
    // privider_company =$('#provider_company').val();
    // request_type = $('#request_type').val();
    // customer_no = $('#customer_no').val();
    // if(customer_no.length <= 13 && customer_no != '' && request_type != null && privider_company != null ){


    $.ajax({
        type: "GET",
        dataType: 'json',
        contentType: "application/json",
        url: "api/core/accountOwner/info?accountNumber=" +radio1,

        success: function (response) {
            //
            if (response.status == "SUCCESS") {

                if(response.data.length = 1 && response.data[0].clientNumber == $('#customer_no').val()){
                    $('#account_info_3').hide();
                    $('#store_info_6').show();
                }else
               {
                    i = 0;
                    while (i < response.data.length) {


                        var trHTML_account_info_table_2 = '<tr>' +
                            '<td><input type="checkbox" name="accountNo" value="' + response.data[i].accountNo + '"></td>' +
                            '<td>' + response.data[i].clientNumber + '</td>' +
                            '<td>' + response.data[i].clientName + '</td>' +
                            '<td>' + response.data[i].clientType + '</td>' +
                            '<tr>';
                        i++;
                        $('#account_info_table_2').append(trHTML_account_info_table_2);

                    }
                    $('#account_info_3').hide();
                    $('#account_owners_info_4').show();

               }
            }
            else {
                alert(response.description)
            }


        },
        async: false
    });


}
function account_owners_info_back() {

    $('#real_acquire_specification_5').hide();
    $('#account_owners_info_4').show();
}

function acquire_info5() {

    // privider_company =$('#provider_company').val();
    // request_type = $('#request_type').val();
    // customer_no = $('#customer_no').val();

    // if(customer_no.length <= 13 && customer_no != '' && request_type != null && privider_company != null ){
    if(true ){
        $('#account_owners_info_4').hide();
        $('#real_acquire_specification_5').show();
    }
    else{
        if(customer_no.length > 12)
            alert("شماره مشتری حداكثر 12 كاراكتر می باشد.")

        else {
            alert("فیلدهای اجباری را پر نمایید")
        }
    }



}
function acquire_info_back5() {

    $('#real_acquire_specification_5').show();
    $('#store_info_6').hide();
}


function store_info() {

    privider_company =$('#provider_company').val();
    request_type = $('#request_type').val();
    customer_no = $('#customer_no').val();

    // if(customer_no.length <= 13 && customer_no != '' && request_type != null && privider_company != null ){
    if(true ){
        $('#real_acquire_specification_5').hide();
        $('#store_info_6').show();
    }
    else{
        if(customer_no.length > 12)
            alert("شماره مشتری حداكثر 12 كاراكتر می باشد.")

        else {
            alert("فیلدهای اجباری را پر نمایید")
        }
    }



}
function store_info_back() {

    $('#store_info_6').show();
    $('#terminal_type_info_7').hide();
}


function terminal_type_info() {

    privider_company =$('#provider_company').val();
    request_type = $('#request_type').val();
    customer_no = $('#customer_no').val();

    // if(customer_no.length <= 13 && customer_no != '' && request_type != null && privider_company != null ){
    if(true ){
        $('#store_info_6').hide();
        $('#terminal_type_info_7').show();
    }
    else{
        if(customer_no.length > 12)
            alert("شماره مشتری حداكثر 12 كاراكتر می باشد.")

        else {
            alert("فیلدهای اجباری را پر نمایید")
        }
    }



}


