(function() {
    localStorage.setItem('listtrackingcode','')

    var db = {

        loadData: function(filter) {
            return $.grep(db.clients, function(client) {
                return (!filter.createDate || client.createDate.indexOf(filter.createDate) > -1)
                    // && (!filter.indexId || client.indexId === filter.indexId)
                    && (!filter.accountNo || client.accountNo.indexOf(filter.accountNo) > -1)
                    && (!filter.clientNumber || client.clientNumber.indexOf(filter.clientNumber) > -1)
                    && (!filter.branchName || client.branchName.indexOf(filter.branchName) > -1)
                    && (!filter.pspTrackingCode || client.pspTrackingCode.indexOf(filter.pspTrackingCode) > -1)
                    && (!filter.pspAcceptanceCode || client.pspAcceptanceCode.indexOf(filter.pspAcceptanceCode) > -1)
                    && (!filter.pspTerminalCode || client.pspTerminalCode.indexOf(filter.pspTerminalCode) > -1)
                    && (!filter.requestNumber || client.requestNumber === filter.requestNumber);
            });

            // return db.clients;
        },
    };

    window.db = db;
    // if( $.session.get('status') == '0'){

    $.ajax({
               type: "GET",
               dataType: 'json',
               contentType : "application/json",
               url: "api/psp/trace/batch/register",
               success: function (result,status, xhr){
                   $.session.set('traceaccept',JSON.stringify(result.data))
                   $.session.set('status','1')
                   if(result.status == "SUCCESS"){
                       window.listGuild =  result.data;
                       db.clients =listGuild;
                   }else {
                       listGuild=[];
                       db.clients =listGuild;
                   }
               },
               async: false
           });

    // }
    // else{
    //     $.session.set('status','0')
    //     result = JSON.parse($.session.get('unknown'))
    //     if(result.status == "SUCCESS"){
    //         window.listGuild =  result.data;
    //         db.clients =listGuild;
    //     }else {
    //         listGuild=[];
    //         db.clients =listGuild;
    //     }

    // }


    // var array2 = [{"pspRequestId":572,"pspRequestStatusId":null,"requestStatusId":4,"createDate":"1397/03/13","accountNo":"0200435684603","clientNumber":"0040000321","branchName":"مير اصفهان","requestNumber":2572,"pspTrackingCode":null,"companyCode":"IK","selected":false,"status":"نامعلوم"},{"pspRequestId":573,"pspRequestStatusId":null,"requestStatusId":4,"createDate":"1397/03/13","accountNo":"0200435684603","clientNumber":"0040000321","branchName":"مير اصفهان","requestNumber":2573,"pspTrackingCode":null,"companyCode":"IK","selected":false,"status":"نامعلوم"},{"pspRequestId":576,"pspRequestStatusId":null,"requestStatusId":4,"createDate":"1397/03/13","accountNo":"0200435684603","clientNumber":"0040000321","branchName":"مير اصفهان","requestNumber":2576,"pspTrackingCode":null,"companyCode":"IK","selected":false,"status":"نامعلوم"}];
    function findindex_trace(requestNumber,status,pspterminalCode,pspacceptanceCode)
    {
        var array3 = JSON.parse($.session.get('traceaccept'));
        // alert(array3)
        // var list2 = JSON.stringify(array3.data)
        var list2 = array3
        // alert(list2)
        var val = requestNumber
        var filteredObj = list2.find(function(item, i){
            if(item.requestNumber === val){
                // alert(i);
                list2[i].status = status
                list2[i].pspacceptanceCode = pspacceptanceCode
                list2[i].pspterminalCode = pspterminalCode
                // alert(JSON.stringify(list2))
                db.clients =list2;
                // alert(JSON.stringify(list2))
                $.session.set('traceaccept',JSON.stringify(list2))
                $("#TraceAccept").jsGrid("search");
                return i;
            }

        });
    }


    $('#getstatusterminal').click(function () {

        // data=[{
        //     pspRequestId:572,
        //     pspRequestStatusId:null,
        //     requestStatusId:4,
        //     createDate:1397/03/13,
        //     accountNo:"0200435684603",
        //     clientNumber:"0040000321",
        //     branchName:	"مير اصفهان",
        //     requestNumber:	2572,
        //     pspTrackingCode:null,
        //     companyCode:"IK",
        //     selected:false,
        //     status:"نامعلوم",
        //
        // }]
        // db.clients =data;
        // $("#ListGuild2").jsGrid("search");



        if(localStorage.getItem('listtrackingcode').length <= 2){

            sweet_ALert("حداقل یك گزینه را باید انتخاب كنید")

        }else{

            $.ajax({
                       type: "PUT",
                       dataType: 'json',
                       contentType : "application/json",
                       data:localStorage.getItem('listtrackingcode'),
                       url: "api/psp/trace/batch/register",
                       beforeSend:function () {
                           $('#sppiner').addClass('container_blur');
                           $('.spinner').show();

                       },
                       success: function (result,status, xhr){
                           $('#sppiner').removeClass('container_blur');
                           $('.spinner').hide();
                           i=0;
                           flag_error=0;
                           localStorage.setItem('listtrackingcode','')
                           if(result.status == "ERROR"){
                               sweet_ALert(result.description)

                           }else{

                               while(i < result.data.length){

                                   // $("#ListGuild2").jsGrid("updateItem", item, { pspTrackingCode: result.data[i].pspTrackingCode }).done(function() {
                                   //     console.log("update completed");
                                   //     $("#ListGuild2").jsGrid("refresh");
                                   // });
                                   if(result.data[i].status == "ERROR"){
                                       flag_error=1
                                       descerror=result.data[i].errorDesc
                                   }
                                   else
                                   {
                                       findindex_trace(result.data[i].requestNumber,result.data[i].errorDesc,result.data[i].pspterminalCode,result.data[i].pspacceptanceCode)

                                   }

                                   i++;
                               }
                               if(flag_error==1){
                                   sweet_ALert(descerror)
                               }else {

                               }
                           }


                           // if(result.status == "SUCCESS"){
                           //     listGuild =  result.data;
                           //     localStorage.setItem("listtrackingcode",'')
                           //
                           //
                           //     db.clients =listGuild;
                           // }else {
                           //     listGuild=[];
                           //     db.clients =listGuild;
                           //     // swalert("",result.description)
                           // }
                       },

                       async: false
                   });

        }

    })



}());