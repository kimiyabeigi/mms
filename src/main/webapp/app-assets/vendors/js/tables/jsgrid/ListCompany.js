
(function() {




    var db = {

        loadData: function(filter) {
            return $.grep(db.clients, function(client) {
                return (!filter.name || client.name.toLowerCase().indexOf(filter.name) > -1 || client.name.toUpperCase().indexOf(filter.name) > -1 )
                && (!filter.companyCode || client.companyCode.toLowerCase().indexOf(filter.companyCode) > -1  || client.companyCode.toUpperCase().indexOf(filter.companyCode) > -1)
                // && (!filter.Address || client.Address.indexOf(filter.Address) > -1)
                // && (!filter.Country || client.Country === filter.Country)
                // && (filter.Married === undefined || client.Married === filter.Married);

            });

        },

        // loadData: function() {
        //     return db.clients;
        // },

        insertItem: function(insertingClient) {
            this.clients.push(insertingClient);
            baseCompany = {
                companyId:null,
                companyCode:insertingClient.companyCode,
                name:insertingClient.name,
                deleted:false
            }
// ;
            $.ajax({
                type: "PUT",
                dataType: 'json',
                contentType : "application/json",
                data:JSON.stringify(baseCompany),
                url: "api/base/company",
                success: function (result) {
                    // insert_confirm(result);
                    listCompnay =  result.data;
                    db.clients =listCompnay;



                },
                error:function (response, status) {

                        insert_error(response);
                },
                async: false

            });
            setTimeout(function(){ reloadtable();    }, 100);

        },

        updateItem: function(updatingItem) {

            baseCompany = {
                companyId:updatingItem.companyId,
                companyCode:updatingItem.companyCode,
                name:updatingItem.name,
                deleted:false
            }

            $.ajax({
                type: "POST",
                dataType: 'json',
                contentType : "application/json",
                 data:JSON.stringify(baseCompany),
                 url: "api/base/company",
                success: function (result) {
                    update_success(result)
                },
                error:function (response, status,xhr) {
                    update_error(response)
                },

                async: false

            });

        },


        deleteItem: function(deletingClient) {
            var clientIndex = $.inArray(deletingClient, this.clients);
            $.ajax({
                type: "DELETE",
                dataType: 'json',
                contentType : "application/json",

                url: "api/base/company/"+deletingClient.companyId+"",
                // success: function (result) {
                //
                //     if(result.status =="ERROR"){
                //
                //
                //         delete_confirm(result.description)
                //
                //     }else{
                //         delete_confirm(result.description)
                //
                //         $.session.set("del_status","SUCCESS");
                //     }
                // }
                success: function (result) {
                    if(result.status=="SUCCESS"){
                        delete_confirm(result.description)
                    }
                    else{

                        delete_error(result.description)
                    }
                },
                       async:false

                   });

        }



    };
    window.db = db;

    $.ajax({
               type: "GET",
               dataType: 'json',
               contentType : "application/json",

               url: "api/base/company",
               success: function (result,status, xhr){

                   if(result.status == "SUCCESS"){
                       listCompnay =  result.data;
                       db.clients =listCompnay;
                   }else {
                       listCompnay=[];
                       db.clients =listCompnay;
                   }
               },

               async: false

           });





    function reloadtable() {
        $.ajax({
                   type: "GET",
                   dataType: 'json',
                   contentType : "application/json",

                   url: "api/base/company",
                   success: function (result,status, xhr){

                       if(result.status == "SUCCESS"){
                           listCompnay =  result.data;
                           db.clients =listCompnay;
                       }else {
                           listCompnay=[];
                           db.clients =listCompnay;
                       }
                   },

                   async: false

               });
        $("#ListCompany").jsGrid("search");
    }


}());