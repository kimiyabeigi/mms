(function() {

    var db = {

        loadData: function(filter) {
            return $.grep(this.clients, function(client) {
                return (!filter.description || client.description.indexOf(filter.description) > -1)
                    // && (!filter.indexId || client.indexId === filter.indexId)
                    && (!filter.name || client.name.indexOf(filter.name) > -1)
                    && (!filter.description || client.description.indexOf(filter.description) > -1)
                    && (!filter.nameEN || client.nameEN.indexOf(filter.nameEN) > -1)
                // && (!filter.nameEN || client.nameEN === filter.nameEN)

                // && (filter.Married === undefined || client.Married === filter.Married);
            });

        },

        insertItem: function(insertingClient) {
            this.clients.push(insertingClient);
            rolelist = {
                roleId:null,
                name:insertingClient.name,
                nameEN:insertingClient.nameEN,
                description : insertingClient.description,
            }
            $.ajax({
                type: "PUT",
                dataType: 'json',
                contentType : "application/json",
                data:JSON.stringify(rolelist),
               url: "api/role/",
                success: function (result) {

                    insert_confirm(result);
                    swal({
                             title:'<small>'+response.responseJSON.description+'</small>',
                             confirmButtonColor: "#DA4453",
                             text: res,
                             confirmButtonText: "تایید",
                             html: true
                         });


                },
                error:function (response, status) {

                    insert_error(response);
                },
                async: false

            });
        },

        updateItem: function(updatingItem) {



            $.ajax({
                type: "POST",
                dataType: 'json',
                contentType : "application/json",
                data:JSON.stringify(updatingItem),
               url: "api/role/",

                success: function (result) {
                    update_success(result)
                },
                error:function (response, status,xhr) {
                    update_error(response)
                },
                async: false

            });

        },


        deleteItem: function(deletingClient) {
            var clientIndex = $.inArray(deletingClient, this.clients);
            $.ajax({
                type: "DELETE",
                dataType: 'json',
                contentType : "application/json",

               url: "api/role/"+deletingClient.roleId+"",
                success: function (result) {
                    if(result.status=="SUCCESS"){
                        delete_confirm(result.description)
                    }
                    else{

                        delete_error(result.description)
                    }
                },
            });

        }

    };

    window.db = db;
    $.ajax({
        type: "GET",
        dataType: 'json',
        contentType : "application/json",

       url: "api/role",
        success: function (result,status, xhr){

            if(result.status == "SUCCESS"){
                listGuild =  result.data;

                db.clients =listGuild;
            }else {
                listGuild=[];
                db.clients =listGuild;
            }
        },

        async: false

    });



}());