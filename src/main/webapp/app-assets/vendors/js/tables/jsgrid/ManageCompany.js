(function() {


    $.ajax({
        type: "GET",
        dataType: 'json',
        contentType : "application/json",
       url: "api/base/company",

        success:function(response) {
            i=0;
            while (i<response.data.length){
                var itemoption = '<option value="'+response.data[i].companyId+'">'+response.data[i].name+'</option>' ;
                i++
                $('#itemoption_company').append(itemoption);

            }

        },
        async: false
    });

    $.ajax({
        type: "GET",
        dataType: 'json',
        contentType : "application/json",
       url: "api/company/setting/view",

        success:function(response) {
            i=0;
            while (i<response.data.length){
                if(response.data[i].active == true){
                    status_cs="فعال";
                    color="tag-success"

                }else{
                    status_cs="غیر فعال"
                    color="tag-danger"

                }//

                var trHTML = '<tr>' +
                    '<td id="companyCode'+response.data[i].companySettingId+'">'+response.data[i].companyCode+'</td>'+
                    '<td>'+response.data[i].name+'</td>'+
                    '<td style="text-align: center"><div class="tag '+color+'">'+status_cs+'</div></td>'+
                    '<td style="text-align: center"><button class="btn btn-amber" onclick="formid('+response.data[i].companySettingId+')"><span class="icon-edit2" style="padding-left:5px "></span>ویرایش اطلاعات</button> </td>'+
                    '<tr>';
                i++
                $('#companysetting').append(trHTML);

            }

        },
        async: false
    });

    $.ajax({
        type: "GET",
        dataType: 'json',
        contentType : "application/json",
       url: "api/sys/terminal",

        success:function(response) {
            j=0;
            while (j<response.data.length){
                var trHTML =
                    '<label class="inline custom-control custom-checkbox block">'+
                    '<input type="checkbox" data="'+response.data[j].terminalId+'" companySettingDetailId="" id="terminal_id'+response.data[j].terminalId+'" class="custom-control-input checkbox12" name="terminal[]" value="'+response.data[j].terminalId+'">'+
                    '<span class="custom-control-indicator"></span>'+
                    '<span class="custom-control-description ml-0"></span>'+
                    '</label>'+response.data[j].code+'';
                $('#itemcheckbox_company').append(trHTML);
                j++;
            }
        },
        async: false
    });



    $('form').submit(function(event) {

            if($("#itemoption_company").val() == -1){
                event.preventDefault();

                swal({
                    title:'<small>وجود خطا در اعتبار سنجی </small>',
                    confirmButtonColor: "#DA4453",
                    text: "فیلد نام شركت اجباری می باشد",
                    confirmButtonText: "تایید",
                    html: true
                });

            }
            else{

                    // var terminal = new Array();

                    // $("input:checked").each(function() {
                    //     terminal.push($(this).val());
                    // });
                    event.preventDefault();

                            var ar=[]
                            $('.checkbox12').each(function ()
                            {
                                if ($(this).is(':checked')) {
                                    ar.push({
                                                companySettingDetailId: $(this).attr('companySettingDetailId'),
                                                companySettingId: $.session.get("id_company"),
                                                terminalId: $(this).attr('data'),
                                                selected: true
                                            });
                                } else {
                                    ar.push({
                                                companySettingDetailId: $(this).attr('companySettingDetailId'),
                                                companySettingId: $.session.get("id_company"),
                                                terminalId: $(this).attr('data'),
                                                selected: false
                                            });
                                }
                            });
                    // alert(JSON.stringify(ar));
                    // myJson = {obj: {}};
                    // i=0;
                    // while(i < terminal.length){
                    //     var companySettingDetailId = null;
                    //     var companySettingId = null;
                    //     var terminalId = terminal[i];
                    //     var selected = true;
                    //     myJson.obj["companySettingDetailId"] = companySettingDetailId;
                    //     myJson.obj["companySettingId"] = companySettingId;
                    //     myJson.obj["terminalId"] = terminalId;
                    //     myJson.obj["selected"] = selected;
                    //     i++;
                    // }
                    // alert(JSON.stringify(myJson));
                i=0;
                 while(i<ar.length){

                    if(ar[i].terminalId == 1){
                        if(ar[i].selected == true){
                            $('#ConfigTerminal').show()
                            $('#ConfigTerminal_text').hide()

                            break

                        }

                    }else{
                        $('#ConfigTerminal').hide()
                        $('#ConfigTerminal_text').show()

                    }


                    i++;
                }
            if(ar.length == 0 ){
                $('#ConfigTerminal').hide()
                $('#ConfigTerminal_text').show()
            }


                    if($('#checkbox-value').text() =="true" || $('#checkbox-value').text() =="on"){
                        active =  true;
                    }
                    else{
                        active =  false;

                    }
                    if($.session.get("id_company_modal") == "N"){
                    var formData = {
                        'companySettingId'         : null,
                        'companyId'                : $('#itemoption_company :selected').val(),
                        'organizationCode'         : $('input[name=code_req]').val(),
                        'active'                   : active,
                        'companySettingDetails'    : ar

                    };

                        $.ajax({
                            type        : 'PUT',
                            url         : 'api/company/setting',
                            contentType : "application/json",
                            data        : JSON.stringify(formData), // our data object
                            dataType    : 'json',



                        success: function (response) {


                            if(response.status == "ERROR"){
                            swal({
                                title:'<small></small>',
                                confirmButtonColor: "#DA4453",
                                text: response.description,
                                confirmButtonText: "تایید",
                                html: true
                            });

                            }else{
                                $('.wizard_req').show();
                                $("#save_config").attr("disabled", 'disabled');
                                $('.actions').show();
                                $.session.set('companySettingId',response.data.companySettingId);
                                swal({
                                    title:'<small></small>',
                                    // confirmButtonColor: "#DA4453",
                                    text: response.description,
                                    confirmButtonText: "تایید",
                                    html: true
                                });
                            }
                        },

                        error:function (response, status) {

                            swal({
                                title:'<small>'+response.responseJSON.description+'</small>',
                                confirmButtonColor: "#DA4453",
                                text: response.responseJSON.data[0].message,
                                confirmButtonText: "تایید",
                                html: true
                            });



                        },
                        async: false

                    });




                    }else {
                        // alert("companySettingId==="+$.session.get("id_company"))

                        var ar = []

                        $('.checkbox12').each(function () {
                            if ($(this).is(':checked')) {
                                ar.push({
                                    companySettingDetailId: $(this).attr('companySettingDetailId'),
                                    companySettingId: $.session.get("id_company"),
                                    terminalId: $(this).attr('data'),
                                    selected: true
                                });
                            } else {
                                ar.push({
                                    companySettingDetailId: $(this).attr('companySettingDetailId'),
                                    companySettingId: $.session.get("id_company"),
                                    terminalId: $(this).attr('data'),
                                    selected: false
                                });
                            }
                        });
                        var formData = {
                            'companySettingId': $.session.get("id_company"),
                            'companyId': $('#itemoption_company :selected').val(),
                            'organizationCode': $('input[name=code_req]').val(),
                            'companySettingDetails': ar,
                            'active': active
                        };
                        // alert(JSON.stringify(formData))
                        $.ajax({
                            type: 'POST',
                            url: 'api/company/setting',
                            contentType: "application/json",
                            data: JSON.stringify(formData), // our data object
                            dataType: 'json',


                            success: function (response) {

                                $.session.set("modal_status", '');

                                $.session.set('companySettingId', response.data.companySettingId);
                                swal({
                                    title: '<small></small>',
                                    // confirmButtonColor: "#DA4453",
                                    text: response.description,
                                    confirmButtonText: "تایید",
                                    html: true
                                });

                            },
                            error: function (response, status) {

                                swal({
                                    title: '<small>' + response.responseJSON.description + '</small>',
                                    confirmButtonColor: "#DA4453",
                                    text: response.responseJSON.data[0].message,
                                    confirmButtonText: "تایید",
                                    html: true
                                });


                            },
                            async: false

                        });

                    }


                    }});

    }());

