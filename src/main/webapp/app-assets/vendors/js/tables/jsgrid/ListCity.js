(function() {
    var url = new URL(window.location);
    var id = url.searchParams.get("id");
    var db = {

        loadData: function(filter) {
            return $.grep(this.clients, function(client) {
                return (!filter.name || client.name.indexOf(filter.name) > -1)
                // && (!filter.cityCode || client.cityCode === filter.cityCode)
                && (!filter.nameEN || client.nameEN.toLowerCase().indexOf(filter.nameEN) > -1|| client.nameEN.toUpperCase().indexOf(filter.nameEN) > -1)
                    && (!filter.cityCode || client.cityCode.indexOf(filter.cityCode) > -1)
                // && (!filter.cityCode || client.cityCode === filter.cityCode)
                       && (filter.active === undefined || client.active === filter.active);
            });

        },


        insertItem: function(insertingClient) {
            this.clients.push(insertingClient);
            baseCity = {
                cityId:null,
                cityCode:insertingClient.cityCode,
                stateId:id,
                name:insertingClient.name,
                nameEN:insertingClient.nameEN,
                active : insertingClient.active,
                deleted:false
            }

            $.ajax({
                type: "PUT",
                dataType: 'json',
                contentType : "application/json",
                data:JSON.stringify(baseCity),
               url: "api/base/city/",
                success: function (result) {

                    insert_confirm(result);

                },
                error:function (response, status) {

                    insert_error(response);
                },
                async: false

            });
        },


        updateItem: function(updatingItem) {
            baseCity = {
                cityId:updatingItem.cityId,
                stateId :id,
                cityCode:updatingItem.cityCode,
                name:updatingItem.name,
                nameEN:updatingItem.nameEN,
                active : updatingItem.active,
                deleted:false
            }

            $.ajax({
                type: "POST",
                dataType: 'json',
                contentType : "application/json",
                data:JSON.stringify(baseCity),
               url: "api/base/city",

                success: function (result) {
                    update_success(result)
                },
                error:function (response, status,xhr) {
                    update_error(response)
                },
                async: false

            });

        },


        deleteItem: function(deletingClient) {
            var clientIndex = $.inArray(deletingClient, this.clients);
            $.ajax({
                type: "DELETE",
                dataType: 'json',
                contentType : "application/json",

               url: "api/base/city/"+deletingClient.cityId+"",
                success: function (result) {
                    if(result.status=="SUCCESS"){
                        delete_confirm(result.description)
                    }
                    else{

                        delete_error(result.description)
                    }
                },
            async:false

            });

        }

    };

    window.db = db;
    $.ajax({
        type: "GET",
        dataType: 'json',
        contentType : "application/json",

       url: "api/base/city/stateId/"+id,
        success: function (result,status, xhr){

            if(result.status == "SUCCESS"){

                listCity =  result.data;
                db.clients =listCity;
            }else {
                listCity=[];
                db.clients =listCity;
            }
        },

        async: false

    });



}());