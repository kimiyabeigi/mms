
$('#register_request_1').show();
$('#real_acquire_specification_2').hide();
$('#citizen_real_acquire_specification_2').hide();
$('#legal_acquire_specification_2').hide();
$('#account_info_3').hide();
$('#account_owners_info_4').hide();
$('#real_acquire_specification_5').hide();
$('#citizen_real_acquire_specification_5').hide();
$('#legal_acquire_specification_5').hide();
$('#store_info_ipg_6').hide();
$('#store_info_6').hide();
$('#terminal_type_info_7').hide();

var obj1=[]
window.ar1=[];

$('.option_remove_g').remove();
$.ajax({
    type: "GET",
    dataType: 'json',
    contentType: "application/json",
    url: "api/base/guild",
    success: function (response, status, xhr) {
        if (response.status == "SUCCESS") {
            i = 0;
            while (i < response.data.length) {
                var optinHTML1 = '<option class="option_remove_g" value="' + response.data[i].guildId + '">' + response.data[i].guildCode + '-'+response.data[i].description+'</option>';
                i++
                $('#GuildCode').append(optinHTML1);
            }
        } else {

            // listGuild=[];
            // $('#GuildCode').append('');
        }
    },

    async: false

});
//state
$.ajax({
    type: "GET",
    dataType: 'json',
    contentType: "application/json",
    url: "api/base/state/active",

    success: function (response) {
        i = 0;
        while (i < response.data.length) {
            var optinHTML = '<option class="option_remove_g" value="' + response.data[i].stateId + '">' + response.data[i].name + '</option>';
            i++
            $('#StateId').append(optinHTML);
        }

    },
    async: false
});
//city
$('#StateId').on('change', function () {
    $('.test17').val('')

    $('.option_remove').remove();
    $.ajax({
        type: "GET",
        dataType: 'json',
        contentType: "application/json",
        url: "api/base/city/stateId/" + this.value,
        success: function (response) {
            j = 0;
            while (j < response.data.length) {
                var optinHTML = '<option class="option_remove" value="' + response.data[j].cityId + '">' + response.data[j].name + '</option>';
                j++
                $('#CityId').append(optinHTML);
            }
        },
        async: false
    });
})


var object = {
    pspRequestId: null,
    companyId:null,
    terminalId: null,
    pspAcceptanceCode:null,
    pspTerminalCode:null,
    pspTrackingCode:null,
    ipAddress:null,
    computerName:null,
    requestDate:null,
    userId:null,
    branchId:null,
    requestNumber:null,
    pspClients:null,
    pspAccount: null,
    pspAccountOwners: null,
    pspShop: null,
    pspipg: null
};
$.ajax({
    type: "GET",
    dataType: 'json',
    contentType : "application/json",
   url: "api/base/company/active",

    success:function(response) {
        i=0;
        while (i<response.data.length){
            var optinHTML = '<option value="'+response.data[i].companyId+'">'+response.data[i].name+'</option>';
            i++
            $('#provider_company').append(optinHTML);
        }

    },
    async: false
});

$('#provider_company').on('change', function() {
    $('.option_remove').remove();
    $.ajax({
        type: "GET",
        dataType: 'json',
        contentType : "application/json",
       url: "api/sys/terminal/request/"+this.value,

        success:function(response) {
            i=0;
            while (i<response.data.length){
                var optinHTML = '<option class="option_remove" value="'+response.data[i].terminalId+'">'+response.data[i].code+'</option>';
                i++
                $('#request_type').append(optinHTML);
            }

        },
        async: false
    });
})

function register_request_back() {

    if(page_status_2 == "LEGAL"){

        $('#register_request_1').show();
        $('#legal_acquire_specification_2').hide();

    }else if(page_status_2 == "REAL"){

        $('#register_request_1').show();
        $('#real_acquire_specification_2').hide();

    }else{

        $('#register_request_1').show();
        $('#citizen_real_acquire_specification_2').hide();
    }

}
function acquire_info2() {

   window.request_type = $('#request_type').val();
    window.privider_company = $('#provider_company').val();
    window.customer_no = $('#customer_no').val();
    // object.push({"companyId":privider_company});


    if (privider_company == -1) {

       sweet_ALert("فیلد شركت ارائه دهنده اجباری می باشد.")

    }else if (request_type == -1) {

       sweet_ALert("انتخاب نوع درخواست اجباری می باشد.")

    } else if (customer_no == '' || customer_no.length > 12 ) {

        var msg = ""

        if (customer_no == '')

            msg = "فیلد شماره مشتری اجباری می باشد."

        else

            msg = "فیلد شماره مشتری حداكثر 12 حرف می باشد."

        sweet_ALert(msg)

    }else if(request_type == null){

        // alert(request_type)
        sweet_ALert("فیلد نوع درخواست اجباری می باشد.")

    } else {

        window.request_type_storage= $('#request_type').val();
        //
    var clientNumber = $('#customer_no').val()

    $.ajax({
        type: "GET",
        dataType: 'json',
        contentType: "application/json",
       url: "api/core/client/info?clientNumber=" + clientNumber,
        success: function (response) {
            // alert(Object.keys(response.data).length)
            // for (member in response.data) {
            //     if (response.data[member] != null){
            //         alert("ok")
            //     }
            //     else {
            //         alert("no")
            //     }
            //     // Do work here
            //             }

            if (response.status == "SUCCESS")

            {

                $('#account_info_1').show()
                $('#legal_account_info').show()
                $('#citizen_account_info').show()
                $('#legal_account_info_btn').show()

                        // alert(JSON.stringify(response.data))
                        window.jsonobject = response.data;

                        // alert(JSON.stringify(jsonobject));
                window.clienttype_service = response.data.clientType;

                        //legal
                        if (response.data.clientType == "L")
                        {
                            window.page_status_2 = "LEGAL";
                            window.clienttype_service = response.data.clientType;
                            if (response.data.sex == "M")
                            {
                                sex = "مرد"
                            }
                            else if(response.data.sex == "F")
                            {
                                sex = "زن"
                            }
                            else{
                                sex = ""
                            }
                            $('#sex').val(sex)
                            if (response.data.lifeStatus == "Y")
                            {
                                lifeStatus = "فوت شده"
                                $('#account_info_1').hide();

                            }
                            else
                            {
                                lifeStatus = "در قید حیات"
                            }
                            // alert("L")
                            $('#register_request_1').hide();
                            $('#legal_acquire_specification_2').show();
                            $('#l_clientType').val("حقوقی")
                            $('#l_firstName').val(response.data.companyName)
                            $('#l_commerceCode').val(response.data.commerceCode)
                            $('#l_nationalCode').val(response.data.nationalCode)
                            $('#l_firstNameEn').val(response.data.companyNameEn)
                            $('#l_regNumber').val(response.data.issuancePlaceDesc)
                            $('#l_issuancePlaceDesc').val(response.data.issuancePlaceDesc)
                            $('#l_birthDate').val(response.data.issuanceDate)
                            // $('#l_birthDate').val(response.data.birthDate)
                            $('#l_countryDesc').val(response.data.countryDesc)
                            $('#l_countryNameAbbr').val(response.data.countryNameAbbr)
                            $('#l_mobilePhone').val(response.data.mobilePhone)
                            $('#l_phone').val(response.data.phone)
                            $('#l_fax').val(response.data.fax)
                            $('#l_postalCode').val(response.data.postalCode)
                            $('#l_address').val(response.data.address)


                            //iranian individual
                        }
                        else if (response.data.countryNameAbbr == "IR")
                        {
                            window.page_status_2 = "REAL";
                            window.clienttype_service = response.data.clientType;

                            // alert("IR")
                            $('#register_request_1').hide();
                            $('#real_acquire_specification_2').show();
                            $('#clientId').val(response.data.clientId)
                            $('#clientRequestId').val(response.data.clientRequestId)
                            $('#positionId').val(response.data.positionId)
                            $('#clientNumber').val(response.data.clientNumber)
                            if (response.data.sex == "M")
                            {
                                sex = "مرد"
                            }
                            else if(response.data.sex == "F")
                            {
                                sex = "زن"
                            }
                            else{
                                sex = ""
                            }
                            $('#sex').val(sex)
                            if (response.data.lifeStatus == "Y")
                            {
                                lifeStatus = "فوت شده"
                                $('#account_info_1').hide();

                            }
                            else
                            {
                                lifeStatus = "در قید حیات"
                            }
                            if (response.data.clientType == "I")
                            {
                                ctype_text = "حقیقی"

                            }
                            else
                            {
                                ctype_text = "حقوقی"
                            }
                            $('#lifeStatus').val(lifeStatus)
                            $('#firstName').val(response.data.firstName)
                            $('#firstNameEn').val(response.data.firstNameEn)
                            $('#lastName').val(response.data.lastName)
                            $('#lastNameEn').val(response.data.lastNameEn)
                            $('#fatherName').val(response.data.fatherName)
                            $('#fatherNameEn').val(response.data.fatherNameEn)
                            $('#nationalCode').val(response.data.nationalCode)
                            $('#regNumber').val(response.data.regNumber)
                            $('#foreignCode').val(response.data.foreignCode)
                            $('#passportNumber').val(response.data.passportNumber)
                            $('#birthDate').val(response.data.birthDate)
                            $('#passportExpiryDate').val(response.data.passportExpiryDate)
                            $('#countryNameAbbr').val(response.data.countryNameAbbr)
                            $('#countryDesc').val(response.data.countryDesc)
                            $('#issuanceDate').val(response.data.issuanceDate)
                            $('#issuancePlaceCode').val(response.data.issuancePlaceCode)
                            $('#issuancePlaceDesc').val(response.data.issuancePlaceDesc)
                            $('#mobilePhone').val(response.data.mobilePhone)
                            $('#phone').val(response.data.phone)
                            $('#fax').val(response.data.fax)
                            $('#postalCode').val(response.data.postalCode)
                            $('#address').val(response.data.address)
                            $('#clientType').val(ctype_text)
                            $('#clientKind').val(response.data.clientKind)
                            $('#companyName').val(response.data.companyName)
                            $('#companyNameEn').val(response.data.companyNameEn)
                            $('#commerceCode').val(response.data.commerceCode)
                            $('#clientTypeDesc').val("حقیقی")//response.data.clientType


                        }
                        //other country indivadual
                        else
                        {
                            window.page_status_2 = "KHAREJI";
                            // alert("khareji")
                            if (response.data.clientType == "I")
                            {
                                ctype_text = "حقیقی"

                            }
                            else
                            {
                                ctype_text = "حقوقی"
                            }
                            if (response.data.sex == "M")
                            {
                                sex = "مرد"
                            }
                            else if(response.data.sex == "F")
                            {
                                sex = "زن"
                            }
                            else{
                                sex = ""
                            }
                            $('#sex').val(sex)
                            if (response.data.lifeStatus == "Y")
                            {
                                lifeStatus = "فوت شده"
                                $('#account_info_1').hide();

                            }
                            else
                            {
                                lifeStatus = "در قید حیات"
                            }

                            $('#register_request_1').hide();
                            $('#citizen_real_acquire_specification_2').show();
                            $('#clientId').val(response.data.clientId)
                            $('#clientRequestId').val(response.data.clientRequestId)
                            $('#positionId').val(response.data.positionId)
                            $('#c_clientNumber').val(response.data.clientNumber)
                            $('#c_sex').val(sex)
                            $('#c_lifeStatus').val(lifeStatus)
                            $('#c_firstName').val(response.data.firstName)
                            $('#c_firstnameEN').val(response.data.firstNameEn)
                            $('#c_lastName').val(response.data.lastName)
                            $('#c_lastnameEN').val(response.data.lastNameEn)
                            $('#c_fatherName').val(response.data.fatherName)
                            $('#c_fatherNameEn').val(response.data.fatherNameEn)
                            $('#c_nationalCode').val(response.data.nationalCode)
                            $('#c_regNumber').val(response.data.regNumber)
                            $('#c_foreignCode').val(response.data.foreignCode)
                            $('#c_passportnumber').val(response.data.passportNumber)
                            $('#c_birthDate').val(response.data.birthDate)
                            $('#c_passportExpiryDate').val(response.data.passportExpiryDate)
                            $('#c_countryNameAbbr').val(response.data.countryNameAbbr)
                            $('#c_countryDesc').val(response.data.countryDesc)
                            $('#c_issuanceDate').val(response.data.issuanceDate)
                            $('#c_issuancePlaceCode').val(response.data.issuancePlaceDesc)
                            $('#c_issuancePlaceDesc').val(response.data.issuancePlaceDesc)
                            $('#c_mobilePhone').val(response.data.mobilePhone)
                            $('#c_phone').val(response.data.phone)
                            $('#c_fax').val(response.data.fax)
                            $('#c_postalCode').val(response.data.postalCode)
                            $('#c_address').val(response.data.address)
                            $('#c_clientType').val(ctype_text)
                            $('#c_clientKind').val(response.data.clientKind)
                            $('#c_companyName').val(response.data.companyName)
                            $('#c_companyNameEn').val(response.data.companyNameEn)
                            $('#c_commerceCode').val(response.data.commerceCode)

                        }

                    if (response.data.errorCode == "000000")
                    {


                    }
                    else {
                        sweet_ALert(response.data.errorDesc)
                        $('#account_info_1').hide()
                        $('#legal_account_info').hide()
                        $('#citizen_account_info').hide()
                        $('#legal_account_info_btn').hide()


                    }
                }

            else
            {
                sweet_ALert(response.description)
            }


        },
        async: false
    });

    }
}

function acquire_info_back2() {
    if(page_status_2 == "LEGAL"){
        $('#account_info_3').hide();
        $('#legal_acquire_specification_2').show();
        $('.tr_remove_account_info').remove();

        //iranian individual
    }else if(page_status_2 == "REAL"){
        $('#account_info_3').hide();
        $('#real_acquire_specification_2').show();
        $('.tr_remove_account_info').remove();

    }else{
        $('#account_info_3').hide();
        $('#citizen_real_acquire_specification_2').show();
        $('.tr_remove_account_info').remove();

    }
}


function real_account_info() {
    jsonobject.clientKind='O';
    jsonobject.phone=$('#phone').val();
    jsonobject.fax=$('#fax').val();
    jsonobject.postalCode=$('#postalCode').val();
    jsonobject.address=$('#address').val();
    jsonobject.mobilePhone=$('#mobilePhone').val();

    ar1.push(jsonobject);
    object.pspClients=  ar1;
    $('.tr_remove_account_info').remove();
    phone = $('#phone').val();
    mobilePhone = $('#mobilePhone').val();
    fax = $('#fax').val();
    postalCode = $('#postalCode').val();
    address = $('#address').val();

    var postalCode_mask = !(/^([0-9]+)$/.test(postalCode));
    var phone_mask = !(/^([0-9]+)$/.test(phone));
    var fax_mask = !(/^([0-9]+)$/.test(fax));
    var mobilePhone_mask = !(/^09([0-9]+)$/.test(mobilePhone));


   if(phone == '' ){

        sweet_ALert("فیلد  شماره تلفن ثابت اجباری می باشد.")

    }else if(phone.length > 11){

        sweet_ALert("فیلد شماره تلفن ثابت حداكثر 11 عدد می باشد.")

    } else if(phone_mask){

        sweet_ALert("برای فیلد شماره تلفن ثابت فقط مجاز به استفاده از اعداد می باشید")

    }else if(mobilePhone == '' ){

       sweet_ALert("فیلد  شماره تلفن همراه اجباری می باشد.")

   }else if(mobilePhone.length != 11){

       sweet_ALert("شماره موبایل حداكثر 11 رقمی درج شود.")

   } else if(mobilePhone_mask){

       sweet_ALert("فیلد شماره تلفن همراه حداكثر 11 رقم و با 09 شروع میشود.")

   } else if(fax == '' ){

       sweet_ALert("فیلد  نمابر اجباری می باشد.")

   }else if(fax.length > 11){

       sweet_ALert("فیلد نمابر حداكثر 11 عدد می باشد.")

   } else if(fax_mask){

       sweet_ALert("برای فیلد نمابر فقط مجاز به استفاده از اعداد می باشید")

   }
   else if(postalCode == '' ){

        sweet_ALert("فیلد  كد پستی اجباری می باشد.")

    }else if(postalCode.length != 10){

        sweet_ALert("كد پستی 10 رقمی درج شود.")

    } else if(postalCode_mask){

        sweet_ALert("برای فیلد كد پستی فقط مجاز به استفاده از اعداد می باشید")

    }else if(address == '' ){

        sweet_ALert("فیلد  آدرس اجباری می باشد.")

    }else if(address.length > 255){

        sweet_ALert("فیلد آدرس حداكثر 255 حرف می باشد.")

    }
   else
    {
        $.ajax({
            type: "GET",
            dataType: 'json',
            contentType: "application/json",
           url: "api/core/account/info?clientNumber=" + $('#customer_no').val()+"&clientType="+clienttype_service,

            success: function (response) {
                //
                if (response.status == "SUCCESS") {

                    window.radio_aco = response.data;
                    // alert("dasd"+JSON.stringify(radio_aco))
                    i=0;

                    while (i < response.data.length) {
                        if(response.data[i].accountType.toLowerCase()=="s"){
                            type="پس انداز"
                        }
                        else {
                            type="جاری"

                        }

                        var trHTML_account_info_table = '<tr class="tr_remove_account_info">' +
                            '<td style="text-align: center"><input type="radio" onchange=change_text_btn("'+response.data[i].ownershipType+'") name="accountNo" value="' + i + '"></td>' +
                            '<td style="text-align: center">' + response.data[i].accountNo + '</td>' +
                            '<td style="text-align: center">' + response.data[i].accountDesc + '</td>' +
                            '<td style="text-align: center">' + type + '</td>' +
                            '<td style="text-align: center">' + response.data[i].ibanNo + '</td>' +
                            '<td style="text-align: center">' + response.data[i].branchCode + '</td>' +
                            '<tr>';
                        i++;
                        $('#account_info_table').append(trHTML_account_info_table);

                    }
                    $('#citizen_real_acquire_specification_2').hide();
                    $('#legal_acquire_specification_2').hide();
                    $('#real_acquire_specification_2').hide();
                    $('#account_info_3').show();
                }
                else {
                    sweet_ALert(response.description)
                }


            },
            async: false
        });
    }
    // }


    // privider_company =$('#provider_company').val();
    // request_type = $('#request_type').val();
    // customer_no = $('#customer_no').val();
    // if(customer_no.length <= 13 && customer_no != '' && request_type != null && privider_company != null ){
    // $('#clientId').val(response.data[i].clientId)
    // $('#clientRequestId').val(response.data[i].clientRequestId)
    // $('#positionId').val(response.data[i].positionId)
    // $('#clientNumber').val(response.data[i].clientNumber)
    // $('#sex').val(response.data[i].sex)
    // $('#lifeStatus').val(response.data[i].lifeStatus)
    // $('#firstName').val(response.data[i].firstName)
    // $('#firstNameEn').val(response.data[i].firstNameEn)
    // $('#lastName').val(response.data[i].lastName)
    // $('#lastNameEn').val(response.data[i].lastNameEn)
    // $('#fatherName').val(response.data[i].fatherName)
    // $('#fatherNameEn').val(response.data[i].fatherNameEn)
    // $('#nationalCode').val(response.data[i].nationalCode)
    // $('#regNumber').val(response.data[i].regNumber)
    // $('#foreignCode').val(response.data[i].foreignCode)
    // $('#passportNumber').val(response.data[i].passportNumber)
    // $('#birthDay').val(response.data[i].birthDay)
    // $('#passportExpiryDate').val(response.data[i].passportExpiryDate)
    // $('#countryNameAbbr').val(response.data[i].countryNameAbbr)
    // $('#countryCode').val(response.data[i].countryCode)
    // $('#countryDesc').val(response.data[i].countryDesc)
    // $('#issuanceDate').val(response.data[i].issuanceDate)
    // $('#issuancePlaceCode').val(response.data[i].issuancePlaceCode)
    // $('#issuancePlaceDesc').val(response.data[i].issuancePlaceDesc)
    // $('#mobilePhone').val(response.data[i].mobilePhone)
    // $('#phone').val(response.data[i].phone)
    // $('#fax').val(response.data[i].fax)
    // $('#postalCode').val(response.data[i].postalCode)
    // $('#address').val(response.data[i].address)
    // $('#clientType').val(response.data[i].clientType)
    // $('#clientKind').val(response.data[i].clientKind)
    // $('#companyName').val(response.data[i].companyName)
    // $('#companyNameEn').val(response.data[i].companyNameEn)
    // $('#commerceCode').val(response.data[i].commerceCode)





}
function change_text_btn(type)
{
    if (type == "SG")
    {
        $('#account_owners_info').text("اطلاعات فروشگاه")
    }
    else
    {
        $('#account_owners_info').text("اطلاعات صاحبان حساب")

    }
}
function citizen_account_info() {

    c_phone = $('#c_phone').val();
    c_mobilePhone = $('#c_mobilePhone').val();
    c_fax = $('#c_fax').val();
    c_postalCode = $('#c_postalCode').val();
    c_address = $('#c_address').val();

    var c_phone_mask = !(/^([0-9]+)$/.test(c_phone));
    var c_mobilePhone_mask = !(/^09([0-9]+)$/.test(c_mobilePhone));
    var c_fax_mask = !(/^([0-9]+)$/.test(c_fax));
    var c_postalCode_mask = !(/^([0-9]+)$/.test(c_postalCode));




    if(c_phone == '' ){

        sweet_ALert("فیلد  شماره تلفن ثابت اجباری می باشد.")

    }else if(c_phone.length > 11){

        sweet_ALert("شماره تماس حداكثر 11 رقمی درج شود.")

    } else if(c_phone_mask){

        sweet_ALert("برای فیلد شماره تلفن ثابت فقط مجاز به استفاده از اعداد می باشید")

    }else if(c_mobilePhone == '' ){

        sweet_ALert("فیلد  شماره تلفن همراه اجباری می باشد.")

    }else if(c_mobilePhone.length != 11){

        sweet_ALert("فیلد شماره تلفن همراه حداكثر 11 رقم می باشد.")

    } else if(c_mobilePhone_mask){

        sweet_ALert("فیلد شماره تلفن همراه حداكثر 11 رقم و با 09 شروع میشود.")

    } else if(c_fax == '' ){

        sweet_ALert("فیلد  نمابر اجباری می باشد.")

    }else if(c_fax.length > 11){

        sweet_ALert("فیلد نمابر حداكثر 11 عدد می باشد.")

    } else if(c_fax_mask){

        sweet_ALert("برای فیلد نمابر فقط مجاز به استفاده از اعداد می باشید")

    }
    else if(c_postalCode == '' ){

        sweet_ALert("فیلد  كد پستی اجباری می باشد.")

    }else if(c_postalCode.length != 10){

        sweet_ALert("كد پستی 10 رقمی درج شود.")

    } else if(c_postalCode_mask){

        sweet_ALert("برای فیلد كد پستی فقط مجاز به استفاده از اعداد می باشید")

    }else if(c_address == '' ){

        sweet_ALert("فیلد  آدرس اجباری می باشد.")

    }else if(c_address.length > 255){

        sweet_ALert("فیلد آدرس حداكثر 255 حرف می باشد.")

    }
    else
    {
        jsonobject.clientKind='O';
        jsonobject.phone=$('#c_phone').val();
        jsonobject.fax=$('#c_fax').val();
        jsonobject.postalCode=$('#c_postalCode').val();
        jsonobject.address=$('#c_address').val();
        jsonobject.mobilePhone=$('#c_mobilePhone').val();

        ar1.push(jsonobject);
        object.pspClients=  ar1;
        $.ajax({
            type: "GET",
            dataType: 'json',
            contentType: "application/json",
           url: "api/core/account/info?clientNumber=" + $('#customer_no').val()+"&clientType="+clienttype_service,

            success: function (response) {
                //
                if (response.status == "SUCCESS") {
                    // alert(response.data.length)
                    // alert(JSON.stringify(response.data))
                    window.radio_aco = response.data;

                    i = 0;
                    while (i < response.data.length) {
                        if(response.data[i].accountType=="s"){
                            type="پس انداز"
                        }
                        else {
                            type="جاری"

                        }

                        var trHTML_account_info_table = '<tr class="tr_remove_account_info">' +

                        // var trHTML_account_info_table = '<tr class="tr_remove_client_info">' +
                            '<td><input type="radio" name="accountNo" value="' + i + '"></td>' +
                            '<td>' + response.data[i].accountNo + '</td>' +
                            '<td>' + response.data[i].accountDesc + '</td>' +
                            '<td>' + type + '</td>' +
                            '<td>' + response.data[i].ibanNo + '</td>' +
                            '<td>' + response.data[i].branchCode + '</td>' +
                            '<tr>';
                        i++;
                        $('#account_info_table').append(trHTML_account_info_table);

                    }
                    $('#citizen_real_acquire_specification_2').hide();
                    $('#legal_acquire_specification_2').hide();
                    $('#real_acquire_specification_2').hide();
                    $('#account_info_3').show();
                }
                else {
                    sweet_ALert(response.description)
                }


            },
            async: false
        });
    }
    // }


    // privider_company =$('#provider_company').val();
    // request_type = $('#request_type').val();
    // customer_no = $('#customer_no').val();
    // if(customer_no.length <= 13 && customer_no != '' && request_type != null && privider_company != null ){

    // $('#clientId').val(response.data[i].clientId)
    // $('#clientRequestId').val(response.data[i].clientRequestId)
    // $('#positionId').val(response.data[i].positionId)
    // $('#clientNumber').val(response.data[i].clientNumber)
    // $('#sex').val(response.data[i].sex)
    // $('#lifeStatus').val(response.data[i].lifeStatus)
    // $('#firstName').val(response.data[i].firstName)
    // $('#firstNameEn').val(response.data[i].firstNameEn)
    // $('#lastName').val(response.data[i].lastName)
    // $('#lastNameEn').val(response.data[i].lastNameEn)
    // $('#fatherName').val(response.data[i].fatherName)
    // $('#fatherNameEn').val(response.data[i].fatherNameEn)
    // $('#nationalCode').val(response.data[i].nationalCode)
    // $('#regNumber').val(response.data[i].regNumber)
    // $('#foreignCode').val(response.data[i].foreignCode)
    // $('#passportNumber').val(response.data[i].passportNumber)
    // $('#birthDay').val(response.data[i].birthDay)
    // $('#passportExpiryDate').val(response.data[i].passportExpiryDate)
    // $('#countryNameAbbr').val(response.data[i].countryNameAbbr)
    // $('#countryCode').val(response.data[i].countryCode)
    // $('#countryDesc').val(response.data[i].countryDesc)
    // $('#issuanceDate').val(response.data[i].issuanceDate)
    // $('#issuancePlaceCode').val(response.data[i].issuancePlaceCode)
    // $('#issuancePlaceDesc').val(response.data[i].issuancePlaceDesc)
    // $('#mobilePhone').val(response.data[i].mobilePhone)
    // $('#phone').val(response.data[i].phone)
    // $('#fax').val(response.data[i].fax)
    // $('#postalCode').val(response.data[i].postalCode)
    // $('#address').val(response.data[i].address)
    // $('#clientType').val(response.data[i].clientType)
    // $('#clientKind').val(response.data[i].clientKind)
    // $('#companyName').val(response.data[i].companyName)
    // $('#companyNameEn').val(response.data[i].companyNameEn)
    // $('#commerceCode').val(response.data[i].commerceCode)





}
function legal_account_info() {

    l_phone = $('#l_phone').val();
    l_mobilePhone = $('#l_mobilePhone').val();
    l_fax = $('#l_fax').val();
    l_postalCode = $('#l_postalCode').val();
    l_address = $('#l_address').val();
    // alert(l_address)

    jsonobject.clientKind='O';
    jsonobject.phone=$('#l_phone').val();
    jsonobject.fax=$('#l_fax').val();
    jsonobject.postalCode=$('#l_postalCode').val();
    jsonobject.address=$('#l_address').val();
    jsonobject.mobilePhone=$('#l_mobilePhone').val();
    jsonobject.sex=null;
    jsonobject.lifeStatus=null;

    ar1.push(jsonobject);
    object.pspClients=  ar1;


    var l_postalCode_mask = !(/^([0-9]+)$/.test(l_postalCode));
    var l_phone_mask = !(/^([0-9]+)$/.test(l_phone));
    var l_fax_mask = !(/^([0-9]+)$/.test(l_fax));
    var l_mobilePhone_mask = !(/^09([0-9]+)$/.test(l_mobilePhone));


    if(l_phone == '' ){

        sweet_ALert("فیلد  شماره تلفن ثابت اجباری می باشد.")

    }else if(l_phone.length > 11){

        sweet_ALert("شماره تماس حداكثر 11 رقمی درج شود.")

    } else if(l_phone_mask){

        sweet_ALert("برای فیلد شماره تلفن ثابت فقط مجاز به استفاده از اعداد می باشید")

    }else if(l_mobilePhone == '' ){

        sweet_ALert("فیلد  شماره تلفن همراه اجباری می باشد.")

    }else if(l_mobilePhone.length != 11){

        sweet_ALert("فیلد شماره تلفن همراه حداكثر 11 رقم می باشد.")

    } else if(l_mobilePhone_mask){

        sweet_ALert(" شماره تلفن همراه حداكثر 11 رقمی درج شده و با 09 شروع شود.")

    } else if(l_fax == '' ){

        sweet_ALert("فیلد  نمابر اجباری می باشد.")

    }else if(l_fax.length > 11){

        sweet_ALert("فیلد نمابر حداكثر 11 عدد می باشد.")

    } else if(l_fax_mask){

        sweet_ALert("برای فیلد نمابر فقط مجاز به استفاده از اعداد می باشید")

    }
    else if(l_postalCode == '' ){

        sweet_ALert("فیلد  كد پستی اجباری می باشد.")

    }else if(l_postalCode.length != 10){

        sweet_ALert("فیلد كد پستی 10 رقمی درج شود.")

    } else if(l_postalCode_mask){

        sweet_ALert("برای فیلد كد پستی فقط مجاز به استفاده از اعداد می باشید")

    }else if(l_address == '' ){

        sweet_ALert("فیلد  آدرس اجباری می باشد.")

    }else if(l_address.length > 255){

        sweet_ALert("فیلد آدرس حداكثر 255 حرف می باشد.")

    }
    else
    {
        $.ajax({
            type: "GET",
            dataType: 'json',
            contentType: "application/json",
           url: "api/core/account/info?clientNumber=" + $('#customer_no').val() +"&clientType="+clienttype_service,

            success: function (response) {
                //
                if (response.status == "SUCCESS") {
                    // alert(response.data.length)
                    // alert(JSON.stringify(response.data))

                    window.radio_aco = response.data;


                    i = 0;
                    while (i < response.data.length) {
                        if(response.data[i].accountType=="s"){
                            type="پس انداز"
                        }
                        else {
                            type="جاری"

                        }



                        var trHTML_account_info_table = '<tr class="tr_remove_account_info">' +
                            '<td><input type="radio" name="accountNo" value="' + i + '"></td>' +
                            '<td>' + response.data[i].accountNo + '</td>' +
                            '<td>' + response.data[i].accountDesc + '</td>' +
                            '<td>' + type + '</td>' +
                            '<td>' + response.data[i].ibanNo + '</td>' +
                            '<td>' + response.data[i].branchCode + '</td>' +
                            '<tr>';
                        i++;
                        $('#account_info_table').append(trHTML_account_info_table);

                    }
                    $('#citizen_real_acquire_specification_2').hide();
                    $('#legal_acquire_specification_2').hide();
                    $('#real_acquire_specification_2').hide();
                    $('#account_info_3').show();
                }
                else {
                    sweet_ALert(response.description)
                }


            },
            async: false
        });
    }
    // }


    // privider_company =$('#provider_company').val();
    // request_type = $('#request_type').val();
    // customer_no = $('#customer_no').val();
    // if(customer_no.length <= 13 && customer_no != '' && request_type != null && privider_company != null ){

    // $('#clientId').val(response.data[i].clientId)
    // $('#clientRequestId').val(response.data[i].clientRequestId)
    // $('#positionId').val(response.data[i].positionId)
    // $('#clientNumber').val(response.data[i].clientNumber)
    // $('#sex').val(response.data[i].sex)
    // $('#lifeStatus').val(response.data[i].lifeStatus)
    // $('#firstName').val(response.data[i].firstName)
    // $('#firstNameEn').val(response.data[i].firstNameEn)
    // $('#lastName').val(response.data[i].lastName)
    // $('#lastNameEn').val(response.data[i].lastNameEn)
    // $('#fatherName').val(response.data[i].fatherName)
    // $('#fatherNameEn').val(response.data[i].fatherNameEn)
    // $('#nationalCode').val(response.data[i].nationalCode)
    // $('#regNumber').val(response.data[i].regNumber)
    // $('#foreignCode').val(response.data[i].foreignCode)
    // $('#passportNumber').val(response.data[i].passportNumber)
    // $('#birthDay').val(response.data[i].birthDay)
    // $('#passportExpiryDate').val(response.data[i].passportExpiryDate)
    // $('#countryNameAbbr').val(response.data[i].countryNameAbbr)
    // $('#countryCode').val(response.data[i].countryCode)
    // $('#countryDesc').val(response.data[i].countryDesc)
    // $('#issuanceDate').val(response.data[i].issuanceDate)
    // $('#issuancePlaceCode').val(response.data[i].issuancePlaceCode)
    // $('#issuancePlaceDesc').val(response.data[i].issuancePlaceDesc)
    // $('#mobilePhone').val(response.data[i].mobilePhone)
    // $('#phone').val(response.data[i].phone)
    // $('#fax').val(response.data[i].fax)
    // $('#postalCode').val(response.data[i].postalCode)
    // $('#address').val(response.data[i].address)
    // $('#clientType').val(response.data[i].clientType)
    // $('#clientKind').val(response.data[i].clientKind)
    // $('#companyName').val(response.data[i].companyName)
    // $('#companyNameEn').val(response.data[i].companyNameEn)
    // $('#commerceCode').val(response.data[i].commerceCode)





}


function account_info_back() {

    $('#account_owners_info_4').hide();
    $('#account_info_3').show();

}
function account_owners_info() {
    $('.tr_remove_client_info').remove();

    if(!$('input[type="radio"]:checked').val()){

        sweet_ALert("انتخاب یك شماره حساب اجباری می باشد")

    }else {

        for (var i = 0; i < radio_aco.length; i++) {
            if (radio_aco[i].clientNumber == $('#clientNumber').val()) {
                obj = radio_aco[i];
                window.hiddenrow = i;
                break;
            }
        }

        var row_ac = $('input[type="radio"]:checked').val();
        // alert(JSON.stringify(radio_aco));
        // alert(radio_aco);
        accountNumber = radio_aco[row_ac].accountNo;
        object.pspAccount = radio_aco[row_ac];

        if(radio_aco[row_ac].ownershipType == "SG" && clienttype_service!="L"){

            $('#account_info_3').hide();
            $('#store_info_6').show();
            window.page = "account_info_3";


            //guild


        }else{

            var radio1 = $('input[type="radio"]:checked').val();
            // alert(radio1);

            //
            // privider_company =$('#provider_company').val();
            // request_type = $('#request_type').val();
            // customer_no = $('#customer_no').val();
            // if(customer_no.length <= 13 && customer_no != '' && request_type != null && privider_company != null ){

            $.ajax({
                       type: "GET",
                       dataType: 'json',
                       contentType: "application/json",
                      url: "api/core/accountOwner/info?accountNumber=" + accountNumber,
                       success: function (response) {
                           //
                           if (response.status == "SUCCESS") {
                               // alert(response.data.length);
                                   window.page = "real_acquire_specification_5";
                                   i = 0;
                                   while (i < response.data.length) {
                                       if (response.data[i].active){
                                           active_text="فعال"
                                       }else{
                                           active_text="غیر فعال"
                                           // $('#accountNoo'+i).attr('disabled', 'disabled')
                                       }
                                       var trHTML_account_info_table_2 = '<tr class="row' + i + ' tr_remove_client_info">' +
                                                                         '<td style="text-align: center"><label class="inline custom-control custom-checkbox block"><input ' +
                                                                         'type="checkbox" ' +
                                                                         'class="check_accno custom-control-input" ' +
                                                                         'accountOwnerId="' + response.data[i].accountOwnerId + '" ' +
                                                                         'clientRequestId="' + response.data[i].clientRequestId + '" ' +
                                                                         'clientNumber="' + response.data[i].clientNumber + '" ' +
                                                                         'clientName="' + response.data[i].clientName + '" ' +
                                                                         'clientType="' + response.data[i].clientType + '" ' +
                                                                         'active="' + response.data[i].active + '" ' +
                                                                         'name="accountNo"' +
                                                                         'id="accountNoo'+i+'"' +
                                                                         'value="' + response.data[i].clientNumber + '">' +
                                                                         '                                    <span class="custom-control-indicator"></span>' +
                                                                         '                                    <span class="custom-control-description ml-0"></span></label>' +
                                                                         '</td>' +
                                                                         '<td style="text-align: center">' + response.data[i].clientNumber + '</td>' +
                                                                         '<td style="text-align: center">' + response.data[i].clientName + '</td>' +
                                                                         '<td style="text-align: center">' + active_text + '</td>' +
                                                                         '<tr>';



                                       $('#account_info_table_2').append(trHTML_account_info_table_2);

                                       if (response.data[i].active){

                                       }else{

                                           $('#accountNoo'+i).attr('disabled', 'disabled')

                                       }
                                       i++;
                                   }
                                   // $('.row' + hiddenrow).hide();
                                   $('#account_info_3').hide();
                                   $('#account_owners_info_4').show();

                           }
                           else {
                               sweet_ALert(response.description)
                           }


                       },
                       async: false
                   });
        }


    }
}
function terminal_type_info() {

    ShopName = $('#ShopName').val();
    ShopNameEn = $('#ShopNameEn').val();
    OwnershipType = $('#OwnershipType').val();
    ContractNo = $('#ContractNo').val();
    GuildCode = $('#GuildCode').val();
    ContractNo = $('#ContractNo').val();
    LicenseNo = $('#LicenseNo').val();
    LicenseIssuanceDate = $('#LicenseIssuanceDate').val();
    ContractExpiryDate = $('#ContractExpiryDate').val();
    LicenseValidityDate = $('#LicenseValidityDate').val();
    StateId = $('#StateId').val();
    CityId = $('#CityId').val();
    MunicipalityNo = $('#MunicipalityNo').val();
    Store_PostalCode = $('#Store_PostalCode').val();
    Store_address = $('#Store_address').val();
    Store_Phone = $('#Store_Phone').val();
    Store_Fax = $('#Store_Fax').val();
    var ShopName_pattern =(/[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/.test(ShopName));
    var shopNameEn_pattern =(/^([a-zA-Z]+)$/.test(ShopNameEn));
    var municipalityNo_mask = !(/^([0-9]+)$/.test(MunicipalityNo));
    var store_PostalCode_mask = !(/^([0-9]+)$/.test(Store_PostalCode));
    var store_Phone_mask = !(/^([0-9]+)$/.test(Store_Phone));
    var store_Fax_mask = !(/^([0-9]+)$/.test(Store_Fax));
    // alert(ShopName_pattern)
    $('.tr_remove_client_terminalId').remove()

    if(ShopName == '' ){

        sweet_ALert("فیلد نام فروشگاه اجباری می باشد.")

    } else if(ShopName.length > 50){

        sweet_ALert("فیلد نام فروشگاه حداكثر 50 حرف می باشد.")

    }else if(ShopName_pattern){

        sweet_ALert("نام فروشگاه نباید شامل كاراكتر خاص باشد")

    }else if(ShopNameEn == '' ){

        sweet_ALert("فیلد نام انگلیسی اجباری می باشد.")

    } else if(ShopNameEn.length > 50){

        sweet_ALert("فیلد نام انگلیسی حداكثر 50 حرف می باشد.")


    } else if(!shopNameEn_pattern){

        sweet_ALert("متن مدنظر باید بصورت انگلیسی درج شود")

    }else if(ContractNo.length > 50){

        sweet_ALert("فیلد شماره قرارداد اجاره حداكثر 50 حرف می باشد.")

    } else if(GuildCode == -1){


        sweet_ALert("فیلد كد تكمیلی صنف اجباری می باشد.")


    }else if(OwnershipType == -1 ){

        sweet_ALert("فیلد نوع مالكیت اجباری می باشد.")

    }else if(LicenseNo == '' ){

        sweet_ALert("فیلد شماره جواز كسب اجباری می باشد.")

    } else if(LicenseNo.length > 50){

        sweet_ALert("فیلد شماره جواز كسب حداكثر 50 حرف می باشد.")

    }else if(LicenseIssuanceDate == '' ){

        sweet_ALert("فیلد  تاریخ صدور جواز كسب اجباری می باشد.")

    }else if(!checkShamsi(LicenseIssuanceDate)){

        sweet_ALert("فیلد تاریخ صدور جواز كسب را با فرمت yyyy/mm/dd وارد نمایید.")

    }else if(!checkShamsi(ContractExpiryDate) && ContractExpiryDate != '' ){

        sweet_ALert("فیلد تاریخ اتمام قرارداد اجاره را با فرمت yyyy/mm/dd وارد نمایید.")

    }else if(LicenseValidityDate == '' ){

        sweet_ALert("فیلد  تاریخ اعتبار جواز كسب اجباری می باشد.")

    }else if(!checkShamsi(LicenseValidityDate)){

        sweet_ALert("فیلد تاریخ اعتبار جواز كسب را با فرمت yyyy/mm/dd وارد نمایید.")

    }else if (LicenseValidityDate < LicenseIssuanceDate ){

        sweet_ALert("تاریخ اعتبار جواز كسب نمی تواند از تاریخ صدور كوچكتر باشد")

    } else if(StateId == -1){

            sweet_ALert("فیلد نام استان اجباری می باشد.")

    }else if(CityId == -1){

        sweet_ALert("فیلد نام شهر اجباری می باشد.")

    } else if(municipalityNo_mask){

        sweet_ALert("برای فیلد منطقه شهرداری فقط مجاز به استفاده از اعداد می باشید")

    }else if(MunicipalityNo == '' ){

        sweet_ALert("فیلد  منطقه شهرداری اجباری می باشد.")
    }
    else if(MunicipalityNo.length > 25){

            sweet_ALert("فیلد منطقه شهرداری حداكثر 25 عدد می باشد.")

        }else if(Store_PostalCode == '' ){

        sweet_ALert("فیلد  كد پستی اجباری می باشد.")

    }else if(Store_PostalCode.length > 10){

        sweet_ALert("فیلد كد پستی 10 رقمی درج شود.")

    } else if(store_PostalCode_mask){

        sweet_ALert("برای فیلد كد پستی فقط مجاز به استفاده از اعداد می باشید")

    }else if(Store_address == '' ){

        sweet_ALert("فیلد  آدرس اجباری می باشد.")

    }else if(Store_address.length > 255){

        sweet_ALert("فیلد آدرس حداكثر 255 حرف می باشد.")

    }else if(Store_Phone == '' ){

        sweet_ALert("فیلد  شماره تلفن ثابت اجباری می باشد.")

    }else if(Store_Phone.length > 11){

        sweet_ALert("فیلد شماره تلفن ثابت حداكثر 11 عدد می باشد.")

    } else if(store_Phone_mask){

        sweet_ALert("برای فیلد شماره تلفن ثابت فقط مجاز به استفاده از اعداد می باشید")

    }else if(Store_Fax == '' ){

        sweet_ALert("فیلد  نمابر اجباری می باشد.")

    }else if(Store_Fax.length > 11){

        sweet_ALert("فیلد نمابر حداكثر 11 عدد می باشد.")

    } else if(store_Fax_mask){

        sweet_ALert("برای فیلد نمابر فقط مجاز به استفاده از اعداد می باشید")

    }

    // if(true){
    //     // $('#store_info_6').hide();
    //     // $('#terminal_type_info_7').show();
    // }
    // else{
    //     if(customer_no.length > 12)
    //         alert("شماره مشتری حداكثر 12 كاراكتر می باشد.")
    //
    //     else {
    //         alert("فیلدهای اجباری را پر نمایید")
    //     }
    // }
    // alert(request_type)
    else{

     if(OwnershipType == 2){


            if(ContractNo == '' ){

                sweet_ALert("فیلد شماره قرارداد اجباری می باشد.")

            } else if(ContractNo.length > 50){

                sweet_ALert("فیلد شماره قرارداد اجاره حداكثر 50 حرف می باشد.")

            }else if (ContractExpiryDate == ''){
                sweet_ALert("فیلد تاریخ قرارداد اجباری می باشد.")

            }

            else {
                if(request_type == "1"){
                    $('#tr_remove_client_terminalId').remove();
                    // alert("2")
                    $.ajax({
                               type: "GET",
                               dataType: 'json',
                               contentType: "application/json",
                              url: "api/base/terminalDetail/companyId/"+privider_company,
                               success: function (response) {
                                   i = 0;
                                   while (i < response.data.length) {

                                       var terminalDetail = '<option class="tr_remove_client_terminalId" value="'+response.data[i].terminalDetailId +'">'+ response.data[i].name +'</option>';
                                       i++;
                                       $('#terminalDetail').append(terminalDetail);
                                   }




                               },
                               async: false
                           });

                    $('#store_info_6').hide();
                    $('#terminal_type_info_7').show();

                }else{
                    // alert("4")

                    $('#store_info_6').hide();
                    $('#store_info_ipg_6').show();


                }

            }


        }else{

         if(request_type == "1" || request_type == "3" ){
             $('#tr_remove_client_terminalId').remove();
             // alert("2")
             $.ajax({
                        type: "GET",
                        dataType: 'json',
                        contentType: "application/json",
                       url: "api/base/terminalDetail/companyId/"+privider_company,
                        success: function (response) {
                            i = 0;
                            while (i < response.data.length) {

                                var terminalDetail = '<option class="tr_remove_client_terminalId" value="'+response.data[i].terminalDetailId +'">'+ response.data[i].name +'</option>';
                                i++;
                                $('#terminalDetail').append(terminalDetail);
                            }




                        },
                        async: false
                    });

             $('#store_info_6').hide();
             $('#terminal_type_info_7').show();

         }else{
             // alert("4")

             $('#store_info_6').hide();
             $('#store_info_ipg_6').show();


         }

     }




    }



}
function acquire_info5() {
    var ar=[]
    $('.check_accno').each(function ()
    {
        if($(this).is(':checked')){
            ar.push({
                accountOwnerId:$(this).attr('accountOwnerId'),
                clientRequestId:$(this).attr('clientRequestId'),
                clientNumber:$(this).attr('clientNumber'),
                clientName:$(this).attr('clientName'),
                clientType:$(this).attr('clientType'),
                active:$(this).attr('active')});
        }
    });

    // alert(JSON.stringify(ar));
    object.pspAccountOwners= ar;
    z=0;
    while(z < ar.length){

        if(ar[z].clientType == "L"){
           flag_ar = "NO"
        }
        else{
            flag_ar = "OK"
        }
        break;
        z++;

    }

    if(ar == ''){
        sweet_ALert("انتخاب یك شماره مشتری اجباری می باشد")
    }else if(flag_ar =="OK") {

        i = 0;
        while (i < ar.length) {

            // alert(JSON.stringify(ar));

            // privider_company =$('#provider_company').val();
            // request_type = $('#request_type').val();
            // customer_no = $('#customer_no').val();

            $.ajax({
                       type: "GET",
                       dataType: 'json',
                       contentType: "application/json",
                      url: "api/core/client/info?clientNumber=" + ar[i].clientNumber,
                       success: function (response) {
                           if(response.data.errorCode == "000000"){
                               btn_infostore123="Success"
                           }
                           else{
                               btn_infostore123="ERROR"
                           }
                           window.ownerobjectjson = response.data;
                           $('#account_owners_info_4').hide();
                           $('#real_acquire_specification_5').show();

                           if (response.data.countryNameAbbr == "IR" || response.data.lifeStatus == ''){
                               window.page_status_5 = "REAL"
                               if (response.data.sex == "M") {
                                   sex = "مرد"
                               } else if(response.data.sex == "F") {
                                   sex = "زن"
                               }
                               else {
                                   sex = ""
                               }

                               $('#sex').val(sex)

                               if (response.data.lifeStatus == "Y"){
                                   lifeStatus = "فوت شده"
                                   $('#account_info_1').hide();
                                   sweet_ALert("مشتری در قید حیات نبوده و امكان ثبت در خواست وجود ندارد")
                               }else if(response.data.lifeStatus == "N"){
                                   lifeStatus = "در قید حیات"
                               }else{
                                   lifeStatus = ""

                               }
                               j=i+1;
                               h1 = ' <section id=""><div class="row"><div class="col-xs-12"> <div class="card"> <div class="card-header">' +
                                    '<h5 class="card-title" style="float: right;margin-top: 2px">  اطلاعات صاحب حساب:</h5><h4>' + response.data.firstName +'&nbsp;'+response.data.lastName+'</h4> ' +
                                    '<a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a> <div class="heading-elements"> <ul class="list-inline mb-0"><li><a onclick="toogle_account('+i+')"><i style="color: red" class="icon-arrow-circle-o-down"></i></a></li></ul> <\/div><\/div>' +
                                    '<div class="card-body collapse in body_toogle" id="body_toogle'+i+'"> <div class="card-block card-dashboard "> <div class="row"><div class="col-md-3"> <div class="form-group"> ' +
                                    '<label > نوع مشتری : </label><input class="form-control" value="حقیقی" id="o_clientTypeDesc' + i + '" name="" disabled><\/div><\/div><div class="col-md-3"> <div class="form-group"> ' +
                                    '<label > جنسیت : </label><input class="form-control" value="' + sex + '" id="o_sex' + i + '" name="" disabled><\/div><\/div><div class="col-md-3"> <div class="form-group"> ' +
                                    '<label > وضعیت حیات : </label><input class="form-control" value = "' + lifeStatus + '"  id="o_lifeStatus' + i + '" name="" disabled><\/div><\/div><div class="col-md-3"> <div class="form-group">' +
                                    '<label > كد ملی : </label><input class="form-control" value = "' + response.data.nationalCode + '" id="o_nationalCode' + i + '" name="" disabled> <\/div><\/div><\/div><div class="row"><div class="col-md-3"> <div class="form-group">' +
                                    '<label > نام : </label><input class="form-control" value = "' + response.data.firstName + '" id="o_firstName' + i + '" name="" disabled><\/div><\/div><div class="col-md-3"> <div class="form-group"> ' +
                                    '<label > نام خانوادگی : </label><input class="form-control" value = "' + response.data.lastName + '" id="o_lastName' + i + '" name="" disabled><\/div><\/div><div class="col-md-3"> <div class="form-group">' +
                                    '<label > نام پدر : </label><input class="form-control" value = "' + response.data.fatherName + '" id="o_fatherName' + i + '" name="" disabled><\/div><\/div><div class="col-md-3"> <div class="form-group"> ' +
                                    '<label > شماره شناسنامه : </label><input class="form-control" value="' + response.data.regNumber + '" id="o_regNumber' + i + '" name="" disabled><\/div><\/div><\/div><div class="row"> <div class="col-md-3"><div class="form-group"> ' +
                                    '<label > نام انگلیسی : </label><input class="form-control" value="' + response.data.firstNameEn + '" id="o_firstNameEn' + i + '" name="" disabled><\/div><\/div><div class="col-md-3"> <div class="form-group"> ' +
                                    '<label > نام خانوادگی انگلیسی : </label><input class="form-control" value="' + response.data.lastNameEn + '" id="o_lastNameEn' + i + '" name="" disabled><\/div><\/div><div class="col-md-3"> <div class="form-group"> ' +
                                    '<label > نام پدر انگلیسی : </label><input class="form-control" value="' + response.data.fatherNameEn + '" id="o_fatherNameEn' + i + '" name="" disabled><\/div><\/div><div class="col-md-3"> <div class="form-group"> ' +
                                    '<label > تاریخ تولد : </label><input class="form-control" value="' + response.data.birthDate + '" id="o_birthDate' + i + '" name="" disabled><\/div><\/div><\/div><div class="row"> <div class="col-md-3"> <div class="form-group">' +
                                    '<label > كشور : </label><input class="form-control" value="' + response.data.countryDesc + '" id="o_countryDesc' + i + '" name="" disabled><\/div><\/div><div class="col-md-3"> <div class="form-group">' +
                                    '<label > تاریخ صدور : </label><input class="form-control" value="' + response.data.issuanceDate + '" id="o_issuanceDate' + i + '" name="" disabled><\/div><\/div><div class="col-md-3"> <div class="form-group">' +
                                    '<label > محل صدور : </label><input class="form-control" value="' + response.data.issuancePlaceDesc + '" id="o_issuancePlaceDesc' + i + '" name="" disabled><\/div><\/div><div class="col-md-3"> <\/div><\/div><div class="row"><div class="col-md-3"> <div class="form-group">' +
                                    '<label > شماره تلفن ثابت(پیش شماره) : <span class="danger">*</span> </label><input class="form-control" id="o_phone' + i + '" value="' + response.data.phone + '" name="" style="direction: ltr" ><\/div><\/div><div class="col-md-3"> <div class="form-group"> ' +
                                    '<label > شماره تلفن همراه : <span class="danger">*</span> </label><input class="form-control" id="o_mobilePhone' + i + '" value="' + response.data.mobilePhone + '" name="" style="direction: ltr"><\/div><\/div><div class="col-md-3"> <div class="form-group">' +
                                    '<label >نمابر(پیش شماره) : </label><input class="form-control" placeholder="02142710000" id="o_fax' + i + '" value="' + response.data.fax +'" name="" style="direction: ltr"><\/div><\/div><div class="col-md-3"> <div class="form-group">' +
                                    '<label > كد پستی : <span class="danger">*</span> </label><input class="form-control" id="o_postalCode' + i + '" value="' + response.data.postalCode + '" name="" style="direction: ltr" ><\/div><\/div><\/div>' +
                                    '<div class="row">' +
                                    ' <div class="col-md-6"> ' +
                                    '<div class="form-group">' +
                                    '<label > آدرس : <span class="danger">*</span> </label>' +
                                    '<textarea type="text" class="form-control"  id="o_address' + i + '"  name="itemoption_company">' + response.data.address + ' <\/textarea>' +
                                    '<\/div>' +
                                    '<\/div>' +
                                    '<div class="col-md-6" style="margin-top: 40px;text-align: left"> ' +
                                    '<\/div>' +
                                    '<\/div>' +
                                    '<div class="row">' +
                                    ' <div class="col-md-3"> ' +
                                    '<div class="form-group">' +
                                    '<label > سمت : <span class="danger">*</span> </label>' +
                                    '<select type="text" class="form-control"  id="o_position' + i + '" value="" name="itemoption_company">' +
                                    '<option value = "2" selected = "selected">سایر اعضاء<\/option>'+
                                    '<option value = "1">مدیرعامل<\/option>'+
                                    ' <\/select>' +
                                    '<\/div>' +
                                    '<\/div>' +
                                    '<div class="col-md-9"> ' +
                                    '<label > كنترل اطلاعات مشتری : <span class="danger"></span> </label>' +
                                    '<input class="form-control" id="o_control" value="'+response.data.errorDesc+'" name="" style="direction: rtl" disabled>'+
                                    '<\/div>' +
                                    '<\/div>' +

                                    '<\/div><\/div><\/div></section>';



                               $('#real_acquire_specification_5').append(h1);
                               $('.body_toogle').hide();
                           }
                           else {

                               // window.page_status_5 = "LEGAL"
                               // اتباع خارجی
                               window.page_status_5 = "REAL_khareji"
                               if (response.data.sex == "M") {
                                   sex = "مرد"
                               } else if(response.data.sex == "F") {
                                   sex = "زن"
                               }
                               else {
                                   sex = ""
                               }

                               $('#sex').val(sex)

                               if (response.data.lifeStatus == "Y"){
                                   lifeStatus = "فوت شده"
                                   $('#account_info_1').hide();
                                   sweet_ALert("مشتری در قید حیات نبوده و امكان ثبت در خواست وجود ندارد")
                               }else if(response.data.lifeStatus == "N"){
                                   lifeStatus = "در قید حیات"
                               }else{
                                   lifeStatus = ""

                               }
                               j=i+1;
                               h1 = ' <section id=""><div class="row"><div class="col-xs-12"> <div class="card"> <div class="card-header">' +
                                    '<h5 class="card-title" style="float: right;margin-top: 2px">  اطلاعات صاحب حساب:</h5><h4>' + response.data.firstName +'&nbsp;'+response.data.lastName+'</h4> ' +
                                    '<a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a> <div class="heading-elements"> <ul class="list-inline mb-0"><li><a onclick="toogle_account('+i+')"><i style="color: red" class="icon-arrow-circle-o-down"></i></a></li></ul> <\/div><\/div>' +
                                    '<div class="card-body collapse in body_toogle" id="body_toogle'+i+'"> <div class="card-block card-dashboard "> <div class="row"><div class="col-md-3"> <div class="form-group"> ' +
                                    '<label > نوع مشتری : </label><input class="form-control" value="حقیقی" id="c_clientTypeDesc' + i + '" name="" disabled><\/div><\/div><div class="col-md-3"> <div class="form-group"> ' +
                                    '<label > جنسیت : </label><input class="form-control" value="' + sex + '" id="c_sex' + i + '" name="" disabled><\/div><\/div><div class="col-md-3"> <div class="form-group"> ' +
                                    '<label > وضعیت حیات : </label><input class="form-control" value = "' + lifeStatus + '"  id="c_lifeStatus' + i + '" name="" disabled><\/div><\/div><div class="col-md-3"> <div class="form-group">' +
                                    '<label > كد فراگیر : </label><input class="form-control" value = "' + response.data.foreignCode + '" id="c_nationalCode' + i + '" name="" disabled> <\/div><\/div><\/div><div class="row"><div class="col-md-3"> <div class="form-group">' +
                                    '<label > نام : </label><input class="form-control" value = "' + response.data.firstName + '" id="c_firstName' + i + '" name="" disabled><\/div><\/div><div class="col-md-3"> <div class="form-group"> ' +
                                    '<label > نام خانوادگی : </label><input class="form-control" value = "' + response.data.lastName + '" id="c_lastName' + i + '" name="" disabled><\/div><\/div><div class="col-md-3"> <div class="form-group">' +
                                    '<label > نام پدر : </label><input class="form-control" value = "' + response.data.fatherName + '" id="c_fatherName' + i + '" name="" disabled><\/div><\/div><div class="col-md-3"> <div class="form-group"> ' +
                                    '<label > شماره گذرنامه : </label><input class="form-control" value="' + response.data.passportNumber + '" id="c_regNumber' + i + '" name="" disabled><\/div><\/div><\/div><div class="row"> <div class="col-md-3"><div class="form-group"> ' +
                                    '<label > نام انگلیسی : </label><input class="form-control" value="' + response.data.firstNameEn + '" id="c_firstNameEn' + i + '" name="" disabled><\/div><\/div><div class="col-md-3"> <div class="form-group"> ' +
                                    '<label > نام خانوادگی انگلیسی : </label><input class="form-control" value="' + response.data.lastNameEn + '" id="c_lastNameEn' + i + '" name="" disabled><\/div><\/div><div class="col-md-3"> <div class="form-group"> ' +
                                    '<label > نام پدر انگلیسی : </label><input class="form-control" value="' + response.data.fatherNameEn + '" id="c_fatherNameEn' + i + '" name="" disabled><\/div><\/div><div class="col-md-3"> <div class="form-group"> ' +
                                    '<label > تاریخ تولد : </label><input class="form-control" value="' + response.data.birthDate + '" id="c_birthDate' + i + '" name="" disabled><\/div><\/div><\/div><div class="row">  <div class="col-md-1">' +
                                    '                        <div class="form-group">' +
                                    '                          <label >' +
                                    'كشور :' +
                                    '                              <span class="danger">*</span>\n' +
                                    '                          </label>' +
                                    '                          <input  class="form-control" id="c_countryNameAbbr'+ i +'" value="' + response.data.countryNameAbbr + '" name="" disabled>\n' +
                                    '                        </div>' +
                                    '                      </div> <div class="col-md-2"> <div class="form-group">' +
                                    '<label > &nbsp; </label><input class="form-control" value="' + response.data.countryDesc + '" id="c_countryDesc' + i + '" name="" disabled><\/div><\/div><div class="col-md-3"> <div class="form-group">' +
                                    '<label > تاریخ صدور : </label><input class="form-control" value="' + response.data.issuanceDate + '" id="c_issuanceDate' + i + '" name="" disabled><\/div><\/div><div class="col-md-3"> <div class="form-group">' +
                                    '<label > محل صدور : </label><input class="form-control" value="' + response.data.issuancePlaceDesc + '" id="c_issuancePlaceDesc' + i + '" name="" disabled><\/div><\/div>' +
                                    '<div class="col-md-3"><div class="form-group">' +
                                    '<label > تاریخ اعتبار گذرنامه : </label><input class="form-control" value="' + response.data.passportExpiryDate + '" id="c_issuancePlaceDesc' + i + '" name="" disabled></div> <\/div><\/div>' +
                                    '<div class="row">' +
                                    '<div class="col-md-3">' +
                                    ' <div class="form-group">' +
                                    '<label > (پیش شماره)شماره تلفن ثابت : <span class="danger">*</span> </label><input class="form-control" id="c_phone' + i + '" value="' + response.data.phone + '" name="" style="direction: ltr" ><\/div><\/div><div class="col-md-3"> <div class="form-group"> ' +
                                    '<label > شماره تلفن همراه : <span class="danger">*</span> </label><input class="form-control" id="c_mobilePhone' + i + '" value="' + response.data.mobilePhone + '" name="" style="direction: ltr"><\/div><\/div><div class="col-md-3"> <div class="form-group">' +
                                    '<label > نمابر(پیش شماره) : </label><input class="form-control" placeholder="02142710000" id="c_fax' + i + '" value="' + response.data.fax +'" name="" style="direction: ltr"><\/div><\/div><div class="col-md-3"> <div class="form-group">' +
                                    '<label > كد پستی : <span class="danger">*</span> </label><input class="form-control" id="c_postalCode' + i + '" value="' + response.data.postalCode + '" name="" style="direction: ltr" ><\/div><\/div><\/div>' +
                                    '<div class="row">' +
                                    ' <div class="col-md-6"> ' +
                                    '<div class="form-group">' +
                                    '<label > آدرس : <span class="danger">*</span> </label>' +
                                    '<textarea type="text" class="form-control"  id="c_address' + i + '"  name="itemoption_company"> ' + response.data.address + '<\/textarea>' +
                                    '<\/div>' +
                                    '<\/div>' +
                                    '<div class="col-md-6" style="margin-top: 40px;text-align: left"> ' +
                                    '<\/div>' +
                                    '<\/div>' +
                                    '<div class="row">' +
                                    ' <div class="col-md-3"> ' +
                                    '<div class="form-group">' +
                                    '<label > سمت : <span class="danger">*</span> </label>' +
                                    '<select type="text" class="form-control"  id="c_position' + i + '" value="" name="itemoption_company">' +
                                    '<option value = "2" selected = "selected">سایر اعضاء<\/option>'+
                                    '<option value = "1">مدیرعامل<\/option>'+

                                    ' <\/select>' +
                                    '<\/div>' +
                                    '<\/div>' +
                                    '<div class="col-md-9"> ' +
                                    '<label > كنترل اطلاعات مشتری : <span class="danger"></span> </label>' +
                                    '<input class="form-control" id="c_control" value="'+response.data.errorDesc+'" name="" style="direction: rtl" disabled>'+
                                    '<\/div>' +
                                    '<\/div>' +

                                    '<\/div><\/div><\/div></section>';



                               $('#real_acquire_specification_5').append(h1);
                               $('.body_toogle').hide();

                           }



                           //         $.session.set("modal_status", '');
                           // $.session.set('companySettingId', response.data.companySettingId);
                           // swal({
                           //     title: '<small></small>',
                           //     // confirmButtonColor: "#DA4453",
                           //     text: response.description,
                           //     confirmButtonText: "تایید",
                           //     html: true
                           // });

                       },
                       error: function (response, status) {
                           alert("no")
                           // swal({
                           //     title: '<small>' + response.responseJSON.description + '</small>',
                           //     confirmButtonColor: "#DA4453",
                           //     text: response.responseJSON.data[0].message,
                           //     confirmButtonText: "تایید",
                           //     html: true
                           // });

                           //
                       },
                       async: false

                   });


            i++;
            //
            if(page_status_5 == "REAL_khareji"){

                btn_country = "khareji"
            }
            window.owner_counter = i
        }
        h2='<div class="row">'+
           '<div class="col-md-6"></div>'+
           '<div class="col-md-6" style="text-align: left">'+
           '<button class="btn btn-success testbtn" id="btn_infostore123" onclick="store_info()" style="margin-left: 10;">اطلاعات فروشگاه</button>'+
           '<button class="btn btn-warning" onclick="account_owners_info_back()">بازگشت</button>'+
           '<\/div><\/div>';


        $('#real_acquire_specification_5').append(h2);

        if(btn_infostore123 == "ERROR" || btn_country == "khareji") {

            $('#btn_infostore123').hide()


        }
        else{

            $('#btn_infostore123').show()

        }

        if(btn_country == "khareji"){
            sweet_ALert("امكان انتخاب مشتری حقیقی اتباع بعنوان صاحب حساب وجود ندارد.")
            btn_country = ""

        }






    }
    else {
        sweet_ALert("امكان انتخاب مشتری حقوقی بعنوان صاحب حساب وجود ندارد")

    }
}
function acquire_info_back5() {

    $('#'+page).show();
    $('#store_info_6').hide();
}
function store_info() {
    if (request_type == "3") {

       mob_submit();

    }
    else
    {



        //guild
        $('.option_remove_g').remove();
        $.ajax({
                   type: "GET",
                   dataType: 'json',
                   contentType: "application/json",
                  url: "api/base/guild",
                   success: function (response, status, xhr) {
                       if (response.status == "SUCCESS")
                       {
                           i = 0;
                           while (i < response.data.length)
                           {
                               var optinHTML1 = '<option class="option_remove_g" value="' + response.data[i].guildId +
                                                '">' + response.data[i].guildCode + '-' + response.data[i].description +
                                                '</option>';
                               i++
                               $('#GuildCode').append(optinHTML1);
                           }
                       }
                       else
                       {

                           // listGuild=[];
                           // $('#GuildCode').append('');
                       }
                   },

                   async: false

               });
        //state
        $.ajax({
                   type: "GET",
                   dataType: 'json',
                   contentType: "application/json",
                  url: "api/base/state/active",

                   success: function (response) {
                       i = 0;
                       while (i < response.data.length)
                       {
                           var optinHTML = '<option class="option_remove_g" value="' + response.data[i].stateId + '">' +
                                           response.data[i].name + '</option>';
                           i++
                           $('#StateId').append(optinHTML);
                       }

                   },
                   async: false
               });
        //city
        $('#StateId').on('change', function () {

            $('.option_remove').remove();
            $('.test18').val()
            $.ajax({
                       type: "GET",
                       dataType: 'json',
                       contentType: "application/json",
                      url: "api/base/city/stateId/" + this.value,

                       success: function (response) {


                           j = 0;
                           while (j < response.data.length)
                           {
                               var optinHTML = '<option class="option_remove test18" value="' + response.data[j].cityId +
                                               '">' + response.data[j].name + '</option>';
                               j++
                               $('#CityId').append(optinHTML);
                           }

                       },
                       async: false
                   });
        })
        var i = 0;
        var o_positioncount = 0;
        while (i < owner_counter)
        {
            o_phone = $('#o_phone' + i).val();
            o_mobilePhone = $('#o_mobilePhone' + i).val();
            o_fax = $('#o_fax' + i).val();
            o_postalCode = $('#o_postalCode' + i).val();
            o_address = $('#o_address' + i).val();
            o_position = $('#o_position' + i).val();

            var o_postalCode_mask = !(/^([0-9]+)$/.test(o_postalCode));
            var o_phone_mask = !(/^([0-9]+)$/.test(o_phone));
            var o_fax_mask = !(/^([0-9]+)$/.test(o_fax));
            var o_mobilePhone_mask = !(/^09([0-9]+)$/.test(o_mobilePhone));
            if (o_position == 1)
            {

                o_positioncount++;

            }


            if (o_phone == '')
            {

                sweet_ALert("فیلد  شماره تلفن ثابت اجباری می باشد.")
                action_account = "NO"

                break

            }
            else if (o_phone.length > 11)
            {

                sweet_ALert("فیلد شماره تلفن ثابت حداكثر 11 عدد می باشد.")
                action_account = "NO"

                break

            }
            else if (o_phone_mask)
            {

                sweet_ALert("برای فیلد شماره تلفن ثابت فقط مجاز به استفاده از اعداد می باشید")
                action_account = "NO"

                break

            }
            else if (o_mobilePhone == '')
            {

                sweet_ALert("فیلد  شماره تلفن همراه اجباری می باشد.")
                action_account = "NO"

                break

            }
            else if (o_mobilePhone.length != 11)
            {

                sweet_ALert("فیلد شماره تلفن همراه حداكثر 11 رقم می باشد.")
                action_account = "NO"

                break

            }
            else if (o_mobilePhone_mask)
            {

                sweet_ALert("شماره تلفن همراه حداكثر 11 رقمی درج شده و با 09 شروع شود.")
                action_account = "NO"

                break

            }
            else if (o_fax == '')
            {

                sweet_ALert("فیلد  نمابر اجباری می باشد.")
                action_account = "NO"

                break

            }
            else if (o_fax.length > 11)
            {

                sweet_ALert("فیلد نمابر حداكثر 11 عدد می باشد.")
                action_account = "NO"

                break

            }
            else if (o_fax_mask)
            {

                sweet_ALert("برای فیلد نمابر فقط مجاز به استفاده از اعداد می باشید")
                action_account = "NO"

                break

            }
            else if (o_postalCode == '')
            {

                sweet_ALert("فیلد  كد پستی اجباری می باشد.")
                break

            }
            else if (o_postalCode.length > 10)
            {

                sweet_ALert("كد پستی 10 رقمی درج شود.")
                action_account = "NO"

                break

            }
            else if (o_postalCode_mask)
            {

                sweet_ALert("برای فیلد كد پستی فقط مجاز به استفاده از اعداد می باشید")
                action_account = "NO"

                break

            }
            else if (o_address == '')
            {

                sweet_ALert("فیلد  آدرس اجباری می باشد.")
                action_account = "NO"

                break

            }
            else if (o_address.length > 255)
            {

                sweet_ALert("فیلد آدرس حداكثر 255 حرف می باشد.")
                action_account = "NO"

                break

            }
            else
            {


                ownerobjectjson.clientKind = 'S';
                ownerobjectjson.phone = $('#o_phone' + i).val();
                ownerobjectjson.fax = $('#o_fax' + i).val();
                ownerobjectjson.postalCode = $('#o_postalCode' + i).val();
                ownerobjectjson.address = $('#o_address' + i).val();
                ownerobjectjson.mobilePhone = $('#o_mobilePhone' + i).val();
                ownerobjectjson.positionId = $('#o_position' + i).val();

                ar1.push(ownerobjectjson);
                object.pspClients = ar1;


                action_account = "OK"
            }
            i++;
        }

        if (o_positioncount == 0)
        {

            sweet_ALert("تعیین یك مدیر عامل بعنوان صاحب حساب الزامی است.")
            action_account = "NO"

        }
        else if (o_positioncount > 1)
        {

            sweet_ALert("بیش از دو مدیر عامل انتخاب شده است.");
            action_account = "NO"

        }
        else if (action_account == "OK")
        {
            $('#real_acquire_specification_5').hide();
            $('#store_info_6').show();
        }
        else
        {

        }

    }

}
function store_info_back() {

    $('#store_info_6').show();
    $('#terminal_type_info_7').hide();
}


// test12= privider_company;

    // obj1.push({"companyId":test12})
// var object = {
//     clientRequestId: null,
//     companyId:null,
//     terminalId: null,
//     pspAcceptanceCode:null,
//     pspTerminalCode:null,
//     ipAddress:null,
//     computerName:null,
//     requestDate:null,
//     branchId:null,
//     requestNumber:null,
//     clients:null,
//     account: null,
//     accountOwners: null,
//     shop: null,
//     ipg: null
// };


function terminal_type_info_7() {
    object.companyId = privider_company;
    object.terminalId = request_type;
    //
    //
    // alert(JSON.stringify(object));

// alert("gggg"+$('#GuildCode').val())
    pspsgo = {
        shopId: null,
        pspRequestId: null,
        shopName: $('#ShopName').val(),
        shopNameEn: $('#ShopNameEn').val(),
        guildId: $('#GuildCode').val(),
        ownershipType: $('#OwnershipType').val(),
        contractNo: $('#ContractNo').val(),
        contractExpiryDate: $('#ContractExpiryDate').val(),
        licenseNo: $('#LicenseNo').val(),
        licenseIssuanceDate: $('#LicenseIssuanceDate').val(),
        licenseValidityDate: $('#LicenseValidityDate').val(),
        countryNameAbbr: $('#country_symbol').val(),
        countryName: $('#CountryName').val(),
        stateId: $('#StateId').val(),
        cityId: $('#CityId').val(),
        municipalityNo: $('#MunicipalityNo').val(),
        phone: $('#Store_Phone').val(),
        fax: $('#Store_Fax').val(),
        postalCode: $('#Store_PostalCode').val(),
        address: $('#Store_address').val(),
    }


    if(request_type == "1") {
        ipg = {
            IPGId: null,
            pspRequestId: null,
            eNamadId: null,
            siteAddress: null,
            issuanceDate: null,
            email: null,
            expiryDate: null,
            firstName: null,
            lastName: null,
            phone: null,
            mobilePhone: null,
            terminalDetailId: $('#terminalDetail').val()
        }
    }
    else {

        ipg = {
            IPGId: null,
            pspRequestId: null,
            eNamadId: $('#eNamadId').val(),
            siteAddress: $('#siteAddress').val(),
            issuanceDate: $('#issuanceDate').val(),
            email: $('#email').val(),
            expiryDate: $('#expiryDate').val(),
            firstName: $('#firstName').val(),
            lastName: $('#lastName').val(),
            phone: $('#phone').val(),
            mobilePhone: $('#mobilePhone').val(),
            terminalDetailId:null,
        }
    }

object.pspipg=ipg;
object.pspShop =pspsgo;
    terminalDetail = $('#terminalDetail').val();
if(terminalDetail == "-1" ){
    sweet_ALert("پایانه مشخص نمایید")

    }
    else {


    $.ajax({
        type: "PUT",
        dataType: 'json',
        contentType: "application/json",
        data:JSON.stringify(object),
       url: "api/psp/register",
        beforeSend:function () {
            $('#sppiner').addClass('container_blur');
            $('.spinner').show();

        },
        success: function (response, status, xhr) {
            $('#sppiner').removeClass('container_blur');
            $('.spinner').hide();
            window.finallobj= object;
            window.finallobj.pspRequestId = response.data.data.pspRequestId
            window.finallobj.requestNumber = response.data.data.requestNumber

            // alert(response.status);

            $('#req_status').text(response.data.status);
            $('#req_desc').text(response.data.errorDesc);
            $('#req_trace').text(response.data.pspTrackingCode);
            $('#req_no').text(response.data.requestNumber);
            window.requestNumber_txt = response.data.requestNumber

            $('#suc_modal').modal('show');
            if(response.data.statusEn == "EXCEPTION"){

                $('#req_retry').show();
                $('#req_retry_cencel').show();

            }else{

                $('#close_modal').show();

            }

        },

        async: false

    });


        // alert(JSON.stringify(object))


    }

}

function ipg_submit() {

    siteAddress = $('#eSiteAddress').val();
    issuanceDate = $('#eIssuanceDate').val();
    email = $('#email').val();
    expiryDate = $('#eExpiryDate').val();
    firstName = $('#firstName_enamad').val();
    lastName = $('#lastName_enamad').val();
    phone = $('#phone_enamad').val();
    mobilePhone = $('#mobile_enamad').val();

    var email_pattern =!(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email));
    // var siteAddress_mask = !(/^(https?:\/\/)?((([a-zA-Z\d]([a-zA-Z\d-]*[a-zA-Z\d])*)\.)+[a-zA-Z]{2,}|((\d{1,3}\.){3}\d{1,3}))(\:\d+)?(\/[-a-zA-Z\d%_.~+]*)*(\?[;&a-zA-Z\d%_.~+=-]*)?(\#[-a-zA-Z\d_]*)?$/.test(siteAddress));
    var mobilePhone_mask = !(/^09([0-9]+)$/.test(mobilePhone));
    var phone_mask = !(/^([0-9]+)$/.test(phone));
    var siteAddress_mask = !(/^(http)?:\/\/[a-zA-Z0-9-\.]+\.[a-z]{2,4}/.test(siteAddress));
    if ($('#eNamadId').val() == -1){
        sweet_ALert("نوع نماد را مشخص نمایید")
    }
    else if(issuanceDate == '' ){

        sweet_ALert("فیلد  تاریخ صدور نماد اجباری می باشد.")

    }else if(!checkShamsi(issuanceDate) && issuanceDate != ''){

        sweet_ALert("فیلد تاریخ  صدور نماد را با فرمت yyyy/mm/dd وارد نمایید.")

    }else if(expiryDate == '' ){

        sweet_ALert("فیلد  تاریخ اتمام نماد اجباری می باشد.")

    }else if(!checkShamsi(expiryDate) && expiryDate != ''){

        sweet_ALert("فیلد تاریخ اتمام نماد را با فرمت yyyy/mm/dd وارد نمایید.")

    }else if (expiryDate <= issuanceDate ){

        sweet_ALert("تاریخ اتمام نماد نمی تواند از تاریخ صدور نماد كوچكتر باشد")

    } else if(siteAddress == '' ){

        sweet_ALert("فیلد آدرس سایت اجباری می باشد.")

    }
    // else if(siteAddress_mask){
    //
    //     sweet_ALert(" فیلد آدرس سایت در قالب استاندارد نمیباشد. ")
    //
    //
    // }
    else if(email == '' ){

        sweet_ALert("فیلد ایمیل اجباری می باشد.")

    } else if(email_pattern){

        sweet_ALert(" فیلد ایمیل در قالب استاندارد نمیباشد. ")


    } else if(firstName == ''){

        sweet_ALert("فیلد نام اجباری می باشد.")

    }else if(firstName.length > 20){

        sweet_ALert("فیلد نام حداكثر 20 حرف می باشد.")

    }else if(lastName == '' ){

        sweet_ALert("فیلد نام خانوادگی اجباری می باشد.")

    } else if(lastName.length > 20){

        sweet_ALert("فیلد نام خانوادگی حداكثر 20 حرف می باشد.")

    }else if(phone == '' ){

        sweet_ALert("فیلد  شماره تلفن ثابت اجباری می باشد.")

    }else if(phone.length > 11){
        // alert(phone.length)

        sweet_ALert("فیلد شماره تلفن ثابت حداكثر 11 عدد می باشد.")

    } else if(phone_mask){

        sweet_ALert("برای فیلد شماره تلفن ثابت فقط مجاز به استفاده از اعداد می باشید")

    }else if(mobilePhone == '' ){

        sweet_ALert("فیلد  شماره تلفن همراه اجباری می باشد.")

    }else if(mobilePhone.length != 11){

        sweet_ALert("فیلد شماره تلفن همراه حداكثر 11 رقم می باشد.")

    } else if(mobilePhone_mask){

        sweet_ALert("شماره تلفن همراه حداكثر 11 رقمی درج شده و با 09 شروع شود.")

    }else if(siteAddress_mask){

        sweet_ALert("آدرس وب سایت را به شكل صحیح وارد نمایید")

    }else {

        object.companyId = privider_company;
        object.terminalId = request_type;


        // alert(JSON.stringify(object));


        pspsgo = {
            shopId: null,
            pspRequestId: null,
            shopName: $('#ShopName').val(),
            shopNameEn: $('#ShopNameEn').val(),
            guildId: $('#GuildCode').val(),
            ownershipType: $('#OwnershipType').val(),
            contractNo: $('#ContractNo').val(),
            contractExpiryDate: $('#ContractExpiryDate').val(),
            licenseNo: $('#LicenseNo').val(),
            licenseIssuanceDate: $('#LicenseIssuanceDate').val(),
            licenseValidityDate: $('#LicenseValidityDate').val(),
            countryNameAbbr: $('#country_symbol').val(),
            countryName: $('#CountryName').val(),
            stateId: $('#StateId').val(),
            cityId: $('#CityId').val(),
            municipalityNo: $('#MunicipalityNo').val(),
            phone: $('#Store_Phone').val(),
            fax: $('#Store_Fax').val(),
            postalCode: $('#Store_PostalCode').val(),
            address: $('#Store_address').val(),
        }


        if (request_type == "1") {
            ipg = {
                IPGId: null,
                pspRequestId: null,
                eNamadId: null,
                siteAddress: null,
                issuanceDate: null,
                email: null,
                expiryDate: null,
                firstName: null,
                lastName: null,
                phone: null,
                mobilePhone: null,
                terminalDetailId: $('#terminalDetail').val()
            }
        }
        else {

            ipg = {
                IPGId: null,
                pspRequestId: null,
                eNamadId: $('#eNamadId').val(),
                siteAddress: $('#eSiteAddress').val(),
                issuanceDate: $('#eIssuanceDate').val(),
                email: $('#email').val(),
                expiryDate: $('#expiryDate').val(),
                firstName: $('#firstName_enamad').val(),
                lastName: $('#lastName_enamad').val(),
                phone: $('#phone_enamad').val(),
                mobilePhone: $('#mobile_enamad').val(),
                terminalDetailId: null,
            }
        }

        object.pspipg = ipg;
        object.pspShop = pspsgo;


            $.ajax({
                type: "PUT",
                dataType: 'json',
                contentType: "application/json",
                data: JSON.stringify(object),
               url: "api/psp/register",
                beforeSend:function () {
                    $('#sppiner').addClass('container_blur');
                    $('.spinner').show();

                },
                success: function (response, status, xhr) {
                    // alert(response.status);
                    $('#sppiner').removeClass('container_blur');
                    $('.spinner').hide();

                    // alert(response.status);
                    window.finallobj= object;
                    window.finallobj.pspRequestId = response.data.data.pspRequestId
                    window.finallobj.requestNumber = response.data.data.requestNumber
                    $('#req_status').text(response.data.status);
                    $('#req_desc').text(response.data.errorDesc);
                    $('#req_trace').text(response.data.pspTrackingCode);
                    $('#req_no').text(response.data.requestNumber);
                    window.requestNumber_txt = response.data.requestNumber
                    $('#suc_modal').modal('show');
                    if(response.data.statusEn == "EXCEPTION"){

                        $('#req_retry').show();
                        $('#req_retry_cencel').show();
                    }else{

                        $('#close_modal').show();

                    }

                },

                async: false

            });


            // alert(JSON.stringify(object))



    }
}

function mob_submit()
    {
        object.companyId = privider_company;
        object.terminalId = request_type;
        pspsgo = {
            shopId: null,
            pspRequestId: null,
            shopName: $('#ShopName').val(),
            shopNameEn: $('#ShopNameEn').val(),
            guildId: $('#GuildCode').val(),
            ownershipType: $('#OwnershipType').val(),
            contractNo: $('#ContractNo').val(),
            contractExpiryDate: $('#ContractExpiryDate').val(),
            licenseNo: $('#LicenseNo').val(),
            licenseIssuanceDate: $('#LicenseIssuanceDate').val(),
            licenseValidityDate: $('#LicenseValidityDate').val(),
            countryNameAbbr: $('#country_symbol').val(),
            countryName: $('#CountryName').val(),
            stateId: $('#StateId').val(),
            cityId: $('#CityId').val(),
            municipalityNo: $('#MunicipalityNo').val(),
            phone: $('#Store_Phone').val(),
            fax: $('#Store_Fax').val(),
            postalCode: $('#Store_PostalCode').val(),
            address: $('#Store_address').val(),
        }
        if (request_type == "1") {
            ipg = {
                IPGId: null,
                pspRequestId: null,
                eNamadId: null,
                siteAddress: null,
                issuanceDate: null,
                email: null,
                expiryDate: null,
                firstName: null,
                lastName: null,
                phone: null,
                mobilePhone: null,
                terminalDetailId: $('#terminalDetail').val()
            }
        }
        else {

            ipg = {
                IPGId: null,
                pspRequestId: null,
                eNamadId: $('#eNamadId').val(),
                siteAddress: $('#eSiteAddress').val(),
                issuanceDate: $('#eIssuanceDate').val(),
                email: $('#email').val(),
                expiryDate: $('#expiryDate').val(),
                firstName: $('#firstName_enamad').val(),
                lastName: $('#lastName_enamad').val(),
                phone: $('#phone_enamad').val(),
                mobilePhone: $('#mobile_enamad').val(),
                terminalDetailId: null,
            }
        }
        object.pspipg = ipg;
        object.pspShop = pspsgo;
        $.ajax({
                   type: "PUT",
                   dataType: 'json',
                   contentType: "application/json",
                   data: JSON.stringify(object),
                  url: "api/psp/register",
                   success: function (response, status, xhr) {
                       // alert(response.status);
                       $('#sppiner').removeClass('container_blur');
                       $('.spinner').hide();

                       // alert(response.status);
                       window.finallobj= object;
                       window.finallobj.pspRequestId = response.data.data.pspRequestId
                       window.finallobj.requestNumber = response.data.data.requestNumber
                       $('#req_status').text(response.data.status);
                       $('#req_desc').text(response.data.errorDesc);
                       $('#req_trace').text(response.data.pspTrackingCode);
                       $('#req_no').text(response.data.requestNumber);
                       window.requestNumber_txt = response.data.requestNumber
                       $('#suc_modal').modal('show');
                       if(response.data.statusEn == "EXCEPTION"){

                           $('#req_retry').show();
                           $('#req_retry_cencel').show();
                       }else{

                           $('#close_modal').show();

                       }

                   },

                   async: false

         });

    }

function  ipg_submit_back() {
    $('#store_info_ipg_6').hide();
    $('#store_info_6').show();

}
function account_owners_info_back() {

    $('.real_acquire_specification_5').empty()
    $('#real_acquire_specification_5').hide();
    $('#account_owners_info_4').show();

}

function retry_tran() {
    $.ajax({
        type: "PUT",
        dataType: 'json',
        contentType: "application/json",
        data:JSON.stringify(window.finallobj),
       url: "api/psp/register",
        beforeSend:function () {
            $('#sppiner').addClass('container_blur');
            $('.spinner').show();

        },
        success: function (response, status, xhr) {
            $('#sppiner').removeClass('container_blur');
            $('.spinner').hide();
            // alert(response.status);

            $('#req_status').text(response.data.status);
            $('#req_desc').text(response.data.errorDesc);
            $('#req_trace').text(response.data.pspTrackingCode);
            $('#req_no').text(requestNumber_txt);

            $('#suc_modal').modal('show');
            if(response.data.statusEn == "EXCEPTION"){

                $('#req_retry').show();
                $('#req_retry_cencel').show();

            }else{

                $('#close_modal').show();

            }

        },

        async: false

    });

}

function toogle_account(i)
{
    $('#body_toogle'+i).toggle();

}


 $( function() {
     $.widget( "custom.combobox", {
         _create: function() {
             this.wrapper = $( "<span>" )
                     .addClass( "custom-combobox" )
                     .insertAfter( this.element );

             this.element.hide();
             this._createAutocomplete();
             this._createShowAllButton();
         },

         _createAutocomplete: function() {
             var selected = this.element.children( ":selected" ),
                     value = selected.val() ? selected.text() : "";

             this.input = $( "<input>" )
                     .appendTo( this.wrapper )
                     .val( value )
                     .attr( "title", "" )
                     .addClass( " form-control margin_rah test12" )
                     .autocomplete({
                                       delay: 0,
                                       minLength: 0,
                                       source: $.proxy( this, "_source" )
                                   })
                     .tooltip({
                                  classes: {
                                      "ui-tooltip": "ui-state-highlight"
                                  }
                              });

             this._on( this.input, {
                 autocompleteselect: function( event, ui ) {
                     ui.item.option.selected = true;
                     this._trigger( "select", event, {
                         item: ui.item.option
                     });
                 },

                 autocompletechange: "_removeIfInvalid"
             });
         },

         _createShowAllButton: function() {
             var input = this.input,
                     wasOpen = false;

             $( "<a>" )
                     .attr( "tabIndex", -1 )
                     .attr( "title", "نمایش همه" )
                     .tooltip()
                     .appendTo( this.wrapper )
                     .button({
                                 icons: {
                                     primary: "ui-icon-triangle-1-s"
                                 },
                                 text: false
                             })
                     .removeClass( "ui-corner-all" )
                     .addClass( "custom-combobox-toggle ui-corner-right " )
                     .on( "mousedown", function() {
                         wasOpen = input.autocomplete( "widget" ).is( ":visible" );
                     })
                     .on( "click", function() {
                         input.trigger( "focus" );

                         // Close if already visible
                         if ( wasOpen ) {
                             return;
                         }

                         // Pass empty string as value to search for, displaying all results
                         input.autocomplete( "search", "" );
                     });
         },

         _source: function( request, response ) {
             var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" );
             response( this.element.children( "option" ).map(function() {
                 var text = $( this ).text();
                 if ( this.value && ( !request.term || matcher.test(text) ) )
                     return {
                         label: text,
                         value: text,
                         option: this
                     };
             }) );
         },

         _removeIfInvalid: function( event, ui ) {

             // Selected an item, nothing to do
             if ( ui.item ) {
                 return;
             }

             // Search for a match (case-insensitive)
             var value = this.input.val(),
                     valueLowerCase = value.toLowerCase(),
                     valid = false;
             this.element.children( "option" ).each(function() {
                 if ( $( this ).text().toLowerCase() === valueLowerCase ) {
                     this.selected = valid = true;
                     return false;
                 }
             });

             // Found a match, nothing to do
             if ( valid ) {
                 return;
             }

             // Remove invalid value
             this.input
                     .val( "" )
                     .attr( "title", value + "یافت نشد" )
                     .tooltip( "open" );
             this.element.val( "" );
             this._delay(function() {
                 this.input.tooltip( "close" ).attr( "title", "" );
             }, 2500 );
             this.input.autocomplete( "instance" ).term = "";
         },

         _destroy: function() {
             this.wrapper.remove();
             this.element.show();
         }
     });

     test123 = $("#GuildCode" ).combobox();

     // $( "#toggle" ).on( "click", function() {
     //     $( "#GuildCode" ).toggle();
     // });

 });
$( function() {
    $.widget( "custom.combobox", {
        _create: function() {
            this.wrapper = $( "<span>" )
                    .addClass( "custom-combobox" )
                    .insertAfter( this.element );

            this.element.hide();
            this._createAutocomplete();
            this._createShowAllButton();
        },

        _createAutocomplete: function() {
            var selected = this.element.children( ":selected" ),
                    value = selected.val() ? selected.text() : "";

            this.input = $( "<input>" )
                    .appendTo( this.wrapper )
                    .val( value )
                    .attr( "title", "" )
                    .addClass( " form-control margin_rah test12 test17" )
                    .autocomplete({
                                      delay: 0,
                                      minLength: 0,
                                      source: $.proxy( this, "_source" )
                                  })
                    .tooltip({
                                 classes: {
                                     "ui-tooltip": "ui-state-highlight"
                                 }
                             });

            this._on( this.input, {
                autocompleteselect: function( event, ui ) {
                    ui.item.option.selected = true;
                    this._trigger( "select", event, {
                        item: ui.item.option
                    });
                },

                autocompletechange: "_removeIfInvalid"
            });
        },

        _createShowAllButton: function() {
            var input = this.input,
                    wasOpen = false;

            $( "<a>" )
                    .attr( "tabIndex", -1 )
                    .attr( "title", "نمایش همه" )
                    .tooltip()
                    .appendTo( this.wrapper )
                    .button({
                                icons: {
                                    primary: "ui-icon-triangle-1-s"
                                },
                                text: false
                            })
                    .removeClass( "ui-corner-all" )
                    .addClass( "custom-combobox-toggle ui-corner-right" )
                    .on( "mousedown", function() {
                        wasOpen = input.autocomplete( "widget" ).is( ":visible" );
                    })
                    .on( "click", function() {
                        input.trigger( "focus" );

                        // Close if already visible
                        if ( wasOpen ) {
                            return;
                        }

                        // Pass empty string as value to search for, displaying all results
                        input.autocomplete( "search", "" );
                    });
        },

        _source: function( request, response ) {
            var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" );
            response( this.element.children( "option" ).map(function() {
                var text = $( this ).text();
                if ( this.value && ( !request.term || matcher.test(text) ) )
                    return {
                        label: text,
                        value: text,
                        option: this
                    };
            }) );
        },

        _removeIfInvalid: function( event, ui ) {

            // Selected an item, nothing to do
            if ( ui.item ) {
                return;
            }

            // Search for a match (case-insensitive)
            var value = this.input.val(),
                    valueLowerCase = value.toLowerCase(),
                    valid = false;
            this.element.children( "option" ).each(function() {
                if ( $( this ).text().toLowerCase() === valueLowerCase ) {
                    this.selected = valid = true;
                    return false;
                }
            });

            // Found a match, nothing to do
            if ( valid ) {
                return;
            }

            // Remove invalid value
            this.input
                    .val( "" )
                    .attr( "title", value + "یافت نشد" )
                    .tooltip( "open" );
            this.element.val( "" );
            this._delay(function() {
                this.input.tooltip( "close" ).attr( "title", "" );
            }, 2500 );
            this.input.autocomplete( "instance" ).term = "";
        },

        _destroy: function() {
            this.wrapper.remove();
            this.element.show();
        }
    });

    test1234 = $("#CityId" ).combobox();

    // $( "#toggle" ).on( "click", function() {
    //     $( "#GuildCode" ).toggle();
    // });

});



