(function() {

    var db = {

        loadData: function(filter) {
            return $.grep(db.clients, function(client) {
                return (!filter.name || client.name.indexOf(filter.name) > -1)
                    // && (!filter.terminalId || client.terminalId === filter.terminalId)
                    // && (!filter.Address || client.Address.indexOf(filter.Address) > -1)
                    // && (!filter.terminal || client.terminal === filter.terminal)
                // && (filter.Married === undefined || client.Married === filter.Married);
            });



        },

        insertItem: function(insertingClient) {


            if($.session.get("id_company_modal") == "N"){
                this.clients.push(insertingClient);
                baseServiceSetting = {
                    companySettingServiceId : null,
                    companySettingId: $.session.get('companySettingId'),
                    serviceId:insertingClient.serviceId,
                    active : insertingClient.active,
                    deleted:false
                }
            }else{

                this.clients.push(insertingClient);
                baseServiceSetting = {
                    companySettingServiceId : insertingClient.companySettingServiceId,
                    companySettingId:  $.session.get("id_company"),
                    serviceId:insertingClient.serviceId,
                    active : insertingClient.active,
                    deleted:false
                }
            }
            $.ajax({
                type: "PUT",
                dataType: 'json',
                contentType : "application/json",
                data:JSON.stringify(baseServiceSetting),
               url: "api/company/setting/service",
                success: function (response) {
                    // $("#ConfigService").jsGrid("insertItem", response.data).done(function() {
                    //     console.log("insertion completed");
                    // });
                    alert(JSON.stringify(response.data))
                    db.clients =  response.data;
                    insert_confirm_company_setting(response);

// alert(response.data)
//                     $("#ConfigService").jsGrid("insertItem",JSON.stringify(response.data));
                    // $("#ConfigService").jsGrid("loadData");
                    // $("#ConfigService").jsGrid("refresh");
                    // $("#ConfigService").jsGrid("search");


                    // db.clients =response;



                },
                error:function (response, status) {
                    insert_error(response);
                },
                // success: function (result) {
                //     alert(result.description)
                //     db.clients =  result.status;
                //     $.session.set('inserterror', result.status);
                //     $.session.set("id_company_modal", '');
                //
                // },
                async: false

            });
            setTimeout(function(){ reloadtableconfigservice();    }, 3000);

        },

        updateItem: function(updatingItem) {
            baseServiceSetting = {
                companySettingServiceId : updatingItem.companySettingServiceId,
                companySettingId:$.session.get("id_company"),
                serviceId:updatingItem.serviceId,
                active : updatingItem.active,
                deleted:false
            }

            $.ajax({
                type: "POST",
                dataType: 'json',
                contentType : "application/json",
                data:JSON.stringify(baseServiceSetting),
               url: "api/company/setting/service",

                // success:function(response) {
                //     $.session.set("id_company_modal", '');
                //     statuserror=response.status;
                //     $.session.set('statuserror', statuserror);
                //     alert(response.description)
                // },
                success: function (result) {
                    update_success(result)
                },
                error:function (response, status,xhr) {
                    update_error(response)
                },
                async: false

            });
        },

        deleteItem: function(deletingClient) {

        }

    };

    window.db = db;



    // db.terminal =[
    //     {Name : "POS",Id:1},
    //     {Name : "MOB",Id:2},
    //         {Name : "IPG",Id:3}
    // ];
var id=$.session.get("id_company");

if($.session.get("id_company_modal") == "N"){
    listService=[];
    db.clients =listService;
}
else {

    $.ajax({
        type: "GET",
        dataType: 'json',
        contentType : "application/json",

       url: "api/company/setting/service/master/"+id,
        success: function (result,status, xhr){
            if(result.status == "SUCCESS"){

                listService =  result.data;
                db.clients =listService;

            }else {

                listService=[];
                db.clients =listService;
            }
        },
    });

    }


        // $.ajax({
        //     type: "GET",
        //     dataType: 'json',
        //     contentType : "application/json",
        //
        //    url: "api/company/setting/service",
        //     success: function (result,status, xhr){
        //
        //         if(result.status == "SUCCESS"){
        //
        //             listService =  result.data;
        //             db.service =listService;
        //
        //         }else {
        //
        //             listService=[];
        //             db.service =listService;
        //         }
        //     },
        //
        //     async: false
        //
        // });


////////////////
//List service//
////////////////
    $.ajax({
        type: "GET",
        dataType: 'json',
        contentType : "application/json",

       url: "api/sys/service",
        success: function (result,status, xhr){

            if(result.status == "SUCCESS"){

                listService =  result.data;
                db.service =listService;
            }else {
                listService=[];
                db.service =listService;
            }
        },

        async: false

    });

    function reloadtableconfigservice() {
        if($.session.get("id_company_modal") == "N"){
            $.ajax({
                       type: "GET",
                       dataType: 'json',
                       contentType : "application/json",

                      url: "api/company/setting/service/master/"+$.session.get('companySettingId'),
                       success: function (result,status, xhr){
                           if(result.status == "SUCCESS"){

                               listService =  result.data;
                               db.clients =listService;

                           }else {

                               listService=[];
                               db.clients =listService;
                           }
                       },
                       async: false

                   });
        }
        else {

            $.ajax({
                       type: "GET",
                       dataType: 'json',
                       contentType : "application/json",

                      url: "api/company/setting/service/master/"+id,
                       success: function (result,status, xhr){
                           if(result.status == "SUCCESS"){

                               listService =  result.data;
                               db.clients =listService;

                           }else {

                               listService=[];
                               db.clients =listService;
                           }
                       },
                       async: false

                   });


        }
        $("#ConfigService").jsGrid("search");

    }

}());