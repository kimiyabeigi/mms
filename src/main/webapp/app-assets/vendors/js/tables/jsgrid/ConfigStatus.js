(function() {

    var db2 = {

        loadData: function(filter) {
            return $.grep(db2.clients, function(client) {
                return (!filter.name || client.name.indexOf(filter.name) > -1)
                // && (!filter.terminalId || client.terminalId === filter.terminalId)
                // && (!filter.Address || client.Address.indexOf(filter.Address) > -1)
                // && (!filter.terminal || client.terminal === filter.terminal)
                // && (filter.Married === undefined || client.Married === filter.Married);
            });

        },
        insertItem: function(insertingClient) {
            if($.session.get("id_company_modal") == "N"){
                this.clients.push(insertingClient);
                baseStatusSetting = {
                    companySettingStatusId : null,
                    companySettingId: $.session.get('companySettingId'),
                    serviceId:insertingClient.serviceId,
                    indexId : insertingClient.indexId,
                    statusCode: insertingClient.statusCode,
                    statusTypeId : insertingClient.statusTypeId,
                    deleted:false
                }
            }else{
                this.clients.push(insertingClient);
                baseStatusSetting = {
                    companySettingStatusId : insertingClient.companySettingStatusId,
                    companySettingId: $.session.get("id_company"),
                    serviceId:insertingClient.serviceId,
                    indexId : insertingClient.indexId,
                    statusCode: insertingClient.statusCode,
                    statusTypeId : insertingClient.statusTypeId,
                    deleted:false
                }
            }



            $.ajax({
                type: "PUT",
                dataType: 'json',
                contentType : "application/json",
                data:JSON.stringify(baseStatusSetting),
               url: "api/company/setting/status",
                // success: function (result) {
                //     alert(result.description)
                //     db2.clients =  result.status;
                //     $.session.set('inserterror', result.status);
                // },
                success: function (response) {
                    db2.clients =  response.status;
                    insert_confirm_company_setting(response);


                },
                error:function (response, status) {
                    insert_error(response);
                },
                async: false

            });
            setTimeout(function(){ reloadtable();    }, 3000);

        },

        updateItem: function(updatingItem) {

            baseStatusSetting = {
                companySettingStatusId : updatingItem.companySettingStatusId ,
                companySettingId: $.session.get("id_company"),
                serviceId:updatingItem.serviceId,
                indexId : updatingItem.indexId,
                statusCode: updatingItem.statusCode,
                statusTypeId : updatingItem.statusTypeId,
                deleted:false
            }

            $.ajax({
                type: "POST",
                dataType: 'json',
                contentType : "application/json",
                data:JSON.stringify(baseStatusSetting),
               url: "api/company/setting/status",

                success: function (result) {
                    update_success(result)
                },
                error:function (response, status,xhr) {
                    update_error(response)
                },
                async: false

            });
        },


        deleteItem: function(deletingClient) {

        }

    };

    window.db2 = db2;
    var id=$.session.get("id_company");

    if($.session.get("id_company_modal") == "N"){
        listStatus =  [];
        db2.clients =listStatus;
    }
    else {
        $.ajax({
            type: "GET",
            dataType: 'json',
            contentType : "application/json",

           url: "api/company/setting/status/master/"+id,
            success: function (result,status, xhr){

                if(result.status == "SUCCESS"){

                    listStatus =  result.data;
                    db2.clients =listStatus;

                }else {

                    listStatus=[];
                    db2.clients =listStatus;
                }
            },

            async: false

        });
    }

    // db.terminal =[
    //     {Name : "POS",Id:1},
    //     {Name : "MOB",Id:2},
    //         {Name : "IPG",Id:3}
    // ];





    $.ajax({
        type: "GET",
        dataType: 'json',
        contentType : "application/json",

       url: "api/sys/statusType",
        success: function (result,status, xhr){

            if(result.status == "SUCCESS"){

                liststatusType =  result.data;
                db2.statusType =liststatusType;

            }else {

                liststatusType=[];
                db2.statusType =liststatusType;
            }
        },

        async: false

    });


    $.ajax({
        type: "GET",
        dataType: 'json',
        contentType : "application/json",

       url: "api/base/index",
        success: function (result,status, xhr){

            if(result.status == "SUCCESS"){

                listIndex =  result.data;
                db2.index =listIndex;
            }else {
                listIndex=[];
                db2.index =listIndex;
            }
        },

        async: false

    });

    function reloadtable() {
    $.ajax({
               type: "GET",
               dataType: 'json',
               contentType : "application/json",

              url: "api/company/setting/status/master/"+id,
               success: function (result,status, xhr){

                   if(result.status == "SUCCESS"){

                       listStatus =  result.data;
                       db2.clients =listStatus;

                   }else {

                       listStatus=[];
                       db2.clients =listStatus;
                   }
               },

               async: false

           });

        $("#ConfigStatus").jsGrid("search");
    }
}());