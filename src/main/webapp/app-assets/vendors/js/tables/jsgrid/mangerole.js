(function() {
    var url = new URL(window.location);
    var id = url.searchParams.get("id");
    $.ajax({
               type: "GET",
               dataType: 'json',
               contentType : "application/json",

              url: "api/form",

               success: function (result) {
                   j = 0;
                   while (j < result.data.length) {


                       var optinHTML =
                               '<div class="col-md-3">' +
                               '<div >' +

                               '<label class="inline custom-control custom-checkbox block">'+
                               '<input type="checkbox"' +
                               'formId="' + result.data[j].formId + '"' +
                               'id="form'+result.data[j].formId+'"' +
                               'class="option_remove check_accno custom-control-input checkbox12" >' +
                               '<span class="custom-control-indicator"></span>'+
                               '<span class="custom-control-description ml-0" style="padding-right: 10"></span>'+
                               '</label>' +
                               '<span style="padding-right: 20px;">' +
                               result.data[j].name
                               '</span>';




                       j++
                       $('#listform').append(optinHTML);
                   }

               },
               error:function (response, status,xhr) {

               },
               async: false

           });


    $.ajax({
        type: "GET",
        dataType: 'json',
        contentType : "application/json",
       url: "api/roleForm/"+id,
        success: function (result) {
            j = 0;
            while (j < result.data.length)
            {
                    $('#form'+result.data[j].formId).attr('checked',result.data[j].selected);
                    $('#form'+result.data[j].formId).attr('roleFormId',result.data[j].roleFormId);
                j++
            }
        },
        error:function (response, status,xhr) {

        },
        async: false

    });


$('#insert_user_btn').click(function () {


    var ar=[]
    $('.check_accno').each(function ()
    {
        if($(this).is(':checked')){
            ar.push({
                roleFormId:$(this).attr('roleFormId'),
                formId:$(this).attr('formId'),
                roleId:id,
                selected:true
            });
        }else{
            ar.push({
                roleFormId:$(this).attr('roleFormId'),
                formId:$(this).attr('formId'),
                roleId:id,
                selected:false
            });
        }
    });

    $.ajax({
        type: "POST",
        dataType: 'json',
        contentType : "application/json",
        data:JSON.stringify(ar),
       url: "api/roleForm/",
        success: function (result) {
            swal({
                     title: '',
                     confirmButtonColor: "#1ac3da",
                     text: result.description,
                     confirmButtonText: "تایید",
                     html: true
                 });

        },
        error:function (response, status,xhr) {
            swal({
                     title: '',
                     confirmButtonColor: "#da410c",
                     text: response.description,
                     confirmButtonText: "تایید",
                     html: true
                 });
        },
        async: false

    });



    // alert(JSON.stringify(ar));

})

}());