$('#sarch_company').click(function () {

    baseCompany = {
        companyCode:$('#companyCode').val(),
        name:$('#name').val()
    }
    if(baseCompany.companyCode == ''){
        baseCompany.companyCode =null;
    }else if(baseCompany.name==''){
        baseCompany.name=null;
    }else{}


    if((baseCompany.companyCode == '') && (baseCompany.name=='')){
        alert("كد شركت یا نام شركت خالی می باشد")

    }
    else{
    $.ajax({
        type: "GET",
        dataType: 'json',
        contentType : "application/json",
        data:baseCompany,
       url: "api/base/company/search",

        success:function(response) {

            statuserror=response.status;
            $.session.set('statuserror', statuserror);
            alert(response.description)
        },
        async: false

    });
    }
});

(function() {

    var db = {

        loadData: function(filter) {
            return $.grep(this.clients, function(client) {
                return (!filter.name || client.name.indexOf(filter.name) > -1)
                // && (!filter.Age || client.Age === filter.Age)
                // && (!filter.Address || client.Address.indexOf(filter.Address) > -1)
                // && (!filter.Country || client.Country === filter.Country)
                // && (filter.Married === undefined || client.Married === filter.Married);
            });

        },

        insertItem: function(insertingClient) {
            this.clients.push(insertingClient);
            baseCompany = {
                companyId:null,
                companyCode:insertingClient.companyCode,
                name:insertingClient.name,
                deleted:false
            }

            $.ajax({
                type: "PUT",
                dataType: 'json',
                contentType : "application/json",
                data:JSON.stringify(baseCompany),
               url: "api/base/company",
                success: function (result) {
                    alert(result.description)
                    db.clients =  result.status;
                    $.session.set('inserterror', result.status);

                },
                async: false

            });
        },

        updateItem: function(updatingItem) {

            baseCompany = {
                companyId:updatingItem.companyId,
                companyCode:updatingItem.companyCode,
                name:updatingItem.name,
                deleted:false
            }

            $.ajax({
                type: "POST",
                dataType: 'json',
                contentType : "application/json",
                 data:JSON.stringify(baseCompany),
                url: "api/base/company",

            success:function(response) {

                statuserror=response.status;
                $.session.set('statuserror', statuserror);
                alert(response.description)
                     },
                async: false

            });

        },


        deleteItem: function(deletingClient) {
            var clientIndex = $.inArray(deletingClient, this.clients);
            $.ajax({
                type: "DELETE",
                dataType: 'json',
                contentType : "application/json",

               url: "api/base/company/"+deletingClient.companyId+"",
                success: function (result) {
                    alert(result.description)

                }
            });

        }

    };

    window.db = db;
    $.ajax({
        type: "GET",
        dataType: 'json',
        contentType : "application/json",
//
       url: "api/base/service/",
        success: function (result,status, xhr){

            if(result.status == "SUCCESS"){

                listCompnay =  result.data;
                db.clients =listCompnay;
            }else {
                listCompnay=[];
                db.clients =listCompnay;
            }
        },

        async: false

    });



}());