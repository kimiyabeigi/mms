(function() {

    var db = {

        loadData: function(filter) {
            return $.grep(this.clients, function(client) {
                return (!filter.description || client.description.indexOf(filter.description) > -1)
                    // && (!filter.indexId || client.indexId === filter.indexId)
                    && (!filter.guildCode || client.guildCode.indexOf(filter.guildCode) > -1)
                // && (!filter.Country || client.Country === filter.Country)
                       && (filter.active === undefined || client.active === filter.active);
            });

        },

        insertItem: function(insertingClient) {
            this.clients.push(insertingClient);
            baseGuild = {
                guildId:null,
                guildCode:insertingClient.guildCode,
                description:insertingClient.description,
                active : insertingClient.active,
                deleted:false
            }

            $.ajax({
                type: "PUT",
                dataType: 'json',
                contentType : "application/json",
                data:JSON.stringify(baseGuild),
               url: "api/base/guild",
                success: function (result) {

                    insert_confirm(result);

                },
                error:function (response, status) {
                    insert_error(response);
                },
                async: false

            });
        },

        updateItem: function(updatingItem) {

            baseGuild = {
                guildId:updatingItem.guildId,
                guildCode:updatingItem.guildCode,
                description:updatingItem.description,
                active : updatingItem.active,
                deleted:false
            }

            $.ajax({
                type: "POST",
                dataType: 'json',
                contentType : "application/json",
                data:JSON.stringify(baseGuild),
               url: "api/base/guild",

                success: function (result) {
                    update_success(result)
                },
                error:function (response, status,xhr) {
                    update_error(response)
                },
                async: false

            });

        },


        deleteItem: function(deletingClient) {
            var clientIndex = $.inArray(deletingClient, this.clients);
            $.ajax({
                type: "DELETE",
                dataType: 'json',
                contentType : "application/json",

               url: "api/base/guild/"+deletingClient.guildId+"",
                success: function (result) {
                    if(result.status=="SUCCESS"){
                        delete_confirm(result.description)
                    }
                    else{

                        delete_error(result.description)
                    }
                },
                       async:false

            });

        }

    };

    window.db = db;
    $.ajax({
        type: "GET",
        dataType: 'json',
        contentType : "application/json",

       url: "api/base/guild",
        success: function (result,status, xhr){

            if(result.status == "SUCCESS"){
                listGuild =  result.data;

                db.clients =listGuild;
            }else {
                listGuild=[];
                db.clients =listGuild;
            }
        },

        async: false

    });



}());