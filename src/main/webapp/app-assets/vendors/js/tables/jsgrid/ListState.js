(function() {

    var db = {

        loadData: function(filter) {
            return $.grep(this.clients, function(client) {
                return (!filter.name || client.name.indexOf(filter.name) > -1)
                // && (!filter.active || client.active === filter.active)
                && (!filter.nameEN || client.nameEN.toLowerCase().indexOf(filter.nameEN) > -1|| client.nameEN.toUpperCase().indexOf(filter.nameEN) > -1)
                    && (!filter.stateCode || client.stateCode.indexOf(filter.stateCode) > -1)
                // && (!filter.Country || client.Country === filter.Country)
                && (filter.active === undefined || client.active === filter.active);
            });

        },
//تست
        insertItem: function(insertingClient) {
            this.clients.push(insertingClient);
            baseState = {
                stateId:null,
                stateCode:insertingClient.stateCode,
                name:insertingClient.name,
                nameEN:insertingClient.nameEN,
                active : insertingClient.active,
                deleted:false
            }

            $.ajax({
                type: "PUT",
                dataType: 'json',
                contentType : "application/json",
                data:JSON.stringify(baseState),
               url: "api/base/state",
                success: function (response) {
                    statuserror=response.status;

                    $.session.set('statuserror', statuserror);


                    insert_confirm(response);

                },
                error:function (response, status) {

                    insert_error(response);
                },
                async: false

            });
        },


        updateItem: function(updatingItem) {

            baseState = {
                stateId:updatingItem.stateId,
                stateCode:updatingItem.stateCode,
                name:updatingItem.name,
                nameEN:updatingItem.nameEN,
                active : updatingItem.active,
                deleted:false
            }

            $.ajax({
                type: "POST",
                dataType: 'json',
                contentType : "application/json",
                 data:JSON.stringify(baseState),
                url: "api/base/state",

                success: function (result) {
                    update_success(result)
                },
                error:function (response, status,xhr) {
                    update_error(response)
                },
                async: false

            });

        },


        deleteItem: function(deletingClient) {
            var clientIndex = $.inArray(deletingClient, this.clients);
            $.ajax({
                type: "DELETE",
                dataType: 'json',
                contentType : "application/json",

               url: "api/base/state/"+deletingClient.stateId+"",
                success: function (result) {
                    if(result.status=="SUCCESS"){
                        delete_confirm(result.description)
                    }
                    else{

                        delete_error(result.description)
                    }
                },
                       async:false

                 });

        }

    };

    window.db = db;
    $.ajax({
        type: "GET",
        dataType: 'json',
        contentType : "application/json",

       url: "api/base/state",
        success: function (result,status, xhr){

            if(result.status == "SUCCESS"){

                listState =  result.data;
                db.clients =listState;
            }else {
                listState=[];
                db.clients =listState;
            }
        },

        async: false

    });



}());