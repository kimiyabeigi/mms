package com.karafarin.terminalmanagement.dto.sys;

public class SysBranch
{
    private Long branchId;
    private String branchCode;
    private String fromIP;
    private String name;
    private String toIP;
    private String departmentCode;
    private boolean defaultBranch;

    public boolean isDefaultBranch()
    {
        return defaultBranch;
    }

    public void setDefaultBranch(boolean defaultBranch)
    {
        this.defaultBranch = defaultBranch;
    }

    public Long getBranchId()
    {
        return branchId;
    }

    public void setBranchId(Long branchId)
    {
        this.branchId = branchId;
    }

    public String getBranchCode()
    {
        return branchCode;
    }

    public void setBranchCode(String branchCode)
    {
        this.branchCode = branchCode;
    }

    public String getFromIP()
    {
        return fromIP;
    }

    public void setFromIP(String fromIP)
    {
        this.fromIP = fromIP;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getToIP()
    {
        return toIP;
    }

    public void setToIP(String toIP)
    {
        this.toIP = toIP;
    }

    public String getDepartmentCode()
    {
        return departmentCode;
    }

    public void setDepartmentCode(String departmentCode)
    {
        this.departmentCode = departmentCode;
    }
}
