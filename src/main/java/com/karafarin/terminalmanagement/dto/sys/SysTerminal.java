package com.karafarin.terminalmanagement.dto.sys;

public class SysTerminal
{

    private Long terminalId;
    private String code;

    public Long getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(Long terminalId) {
        this.terminalId = terminalId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

}
