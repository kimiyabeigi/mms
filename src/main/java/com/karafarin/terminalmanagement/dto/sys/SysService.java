package com.karafarin.terminalmanagement.dto.sys;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

public class SysService
{
    private Long serviceId;

    @Length(max = 15, message = "{validate.service.code.length}")
    @NotEmpty(message = "{validate.service.code.mandatory}")
    private String serviceCode;

    @Length(max = 30, message = "{validate.service.name.length}")
    @NotEmpty(message = "{validate.service.name.mandatory}")
    private String name;

    public String getServiceCode()
    {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode)
    {
        this.serviceCode = serviceCode;
    }

    public Long getServiceId()
    {
        return serviceId;
    }

    public void setServiceId(Long serviceId)
    {
        this.serviceId = serviceId;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

}
