package com.karafarin.terminalmanagement.dto.sys;

public class SysStatusType
{
    private Long statusTypeId;
    private String code;
    private String description;

    public Long getStatusTypeId()
    {
        return statusTypeId;
    }

    public void setStatusTypeId(Long statusTypeId)
    {
        this.statusTypeId = statusTypeId;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }
}
