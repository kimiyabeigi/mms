package com.karafarin.terminalmanagement.dto.sys;

public class SysRequestStatus
{
    private Long requestStatusId;
	private String requestStatusCode;
	private String description;

    public Long getRequestStatusId()
    {
        return requestStatusId;
    }

    public void setRequestStatusId(Long requestStatusId)
    {
        this.requestStatusId = requestStatusId;
    }

    public String getRequestStatusCode()
    {
        return requestStatusCode;
    }

    public void setRequestStatusCode(String requestStatusCode)
    {
        this.requestStatusCode = requestStatusCode;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }
}
