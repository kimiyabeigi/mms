package com.karafarin.terminalmanagement.dto.sys;

public class SysEnamad
{
    private Long eNamadId;
    private String enamadCode;
    private String name;

    public Long geteNamadId()
    {
        return eNamadId;
    }

    public void seteNamadId(Long eNamadId)
    {
        this.eNamadId = eNamadId;
    }

    public String getEnamadCode()
    {
        return enamadCode;
    }

    public void setEnamadCode(String enamadCode)
    {
        this.enamadCode = enamadCode;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }
}
