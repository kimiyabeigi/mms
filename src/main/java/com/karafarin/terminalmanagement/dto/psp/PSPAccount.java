package com.karafarin.terminalmanagement.dto.psp;


public class PSPAccount
{
    private Long accountId;
    private Long pspRequestId;
    private String accountNo;
    private String accountType;
    private String accountDesc;
    private Long branchId;
    private String branchCode;
    private String ibanNo;
    private String accountStatus;
    private String ccy;
    private String invalidAccount;
    private String ownershipType;
    private String mainClient;

    public String getBranchCode()
    {
        return branchCode;
    }

    public void setBranchCode(String branchCode)
    {
        this.branchCode = branchCode;
    }

    public String getMainClient()
    {
        return mainClient;
    }

    public void setMainClient(String mainClient)
    {
        this.mainClient = mainClient;
    }

    public String getOwnershipType()
    {
        return ownershipType;
    }

    public void setOwnershipType(String ownershipType)
    {
        this.ownershipType = ownershipType;
    }

    public String getInvalidAccount()
    {
        return invalidAccount;
    }

    public void setInvalidAccount(String invalidAccount)
    {
        this.invalidAccount = invalidAccount;
    }

    public String getAccountStatus()
    {
        return accountStatus;
    }

    public void setAccountStatus(String accountStatus)
    {
        this.accountStatus = accountStatus;
    }

    public String getCcy()
    {
        return ccy;
    }

    public void setCcy(String ccy)
    {
        this.ccy = ccy;
    }

    public Long getAccountId()
    {
        return accountId;
    }

    public void setAccountId(Long accountId)
    {
        this.accountId = accountId;
    }

    public Long getPspRequestId()
    {
        return pspRequestId;
    }

    public void setPspRequestId(Long pspRequestId)
    {
        this.pspRequestId = pspRequestId;
    }

    public String getAccountNo()
    {
        return accountNo;
    }

    public void setAccountNo(String accountNo)
    {
        this.accountNo = accountNo;
    }

    public String getAccountType()
    {
        return accountType;
    }

    public void setAccountType(String accountType)
    {
        this.accountType = accountType;
    }

    public String getAccountDesc()
    {
        return accountDesc;
    }

    public void setAccountDesc(String accountDesc)
    {
        this.accountDesc = accountDesc;
    }

    public Long getBranchId()
    {
        return branchId;
    }

    public void setBranchId(Long branchId)
    {
        this.branchId = branchId;
    }

    public String getIbanNo()
    {
        return ibanNo;
    }

    public void setIbanNo(String ibanNo)
    {
        this.ibanNo = ibanNo;
    }
}
