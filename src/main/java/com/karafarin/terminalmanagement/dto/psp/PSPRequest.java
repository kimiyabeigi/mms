package com.karafarin.terminalmanagement.dto.psp;

public class PSPRequest
{
    private Long pspRequestId;
    private Long companyId;
    private Long terminalId;
    private Long requestStatusId;
    private String pspAcceptanceCode;
    private String pspTerminalCode;
    private String pspTrackingCode;
    private String ipAddress;
    private String computerName;
    private String requestDate;
    private Long userId;
    private Long branchId;
    private Long requestNumber;

    public String getRequestDate()
    {
        return requestDate;
    }

    public void setRequestDate(String requestDate)
    {
        this.requestDate = requestDate;
    }

    public Long getPspRequestId()
    {
        return pspRequestId;
    }

    public void setPspRequestId(Long pspRequestId)
    {
        this.pspRequestId = pspRequestId;
    }

    public Long getCompanyId()
    {
        return companyId;
    }

    public void setCompanyId(Long companyId)
    {
        this.companyId = companyId;
    }

    public Long getTerminalId()
    {
        return terminalId;
    }

    public void setTerminalId(Long terminalId)
    {
        this.terminalId = terminalId;
    }

    public String getPspAcceptanceCode()
    {
        return pspAcceptanceCode;
    }

    public void setPspAcceptanceCode(String pspAcceptanceCode)
    {
        this.pspAcceptanceCode = pspAcceptanceCode;
    }

    public String getPspTerminalCode()
    {
        return pspTerminalCode;
    }

    public void setPspTerminalCode(String pspTerminalCode)
    {
        this.pspTerminalCode = pspTerminalCode;
    }

    public String getPspTrackingCode()
    {
        return pspTrackingCode;
    }

    public void setPspTrackingCode(String pspTrackingCode)
    {
        this.pspTrackingCode = pspTrackingCode;
    }

    public String getIpAddress()
    {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress)
    {
        this.ipAddress = ipAddress;
    }

    public String getComputerName()
    {
        return computerName;
    }

    public void setComputerName(String computerName)
    {
        this.computerName = computerName;
    }

    public Long getUserId()
    {
        return userId;
    }

    public void setUserId(Long userId)
    {
        this.userId = userId;
    }

    public Long getBranchId()
    {
        return branchId;
    }

    public void setBranchId(Long branchId)
    {
        this.branchId = branchId;
    }

    public Long getRequestNumber()
    {
        return requestNumber;
    }

    public void setRequestNumber(Long requestNumber)
    {
        this.requestNumber = requestNumber;
    }

    public Long getRequestStatusId()
    {
        return requestStatusId;
    }

    public void setRequestStatusId(Long requestStatusId)
    {
        this.requestStatusId = requestStatusId;
    }
}
