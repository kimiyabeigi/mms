package com.karafarin.terminalmanagement.dto.psp;

public class PSPIPG
{
    private Long IPGId;
    private Long pspRequestId;
    private Long eNamadId;
    private String siteAddress;
    private String issuanceDate;
    private String email;
    private String expiryDate;
    private String firstName;
    private String lastName;
    private String phone;
    private String mobilePhone;
    private Long terminalDetailId;

    public String getIssuanceDate()
    {
        return issuanceDate;
    }

    public void setIssuanceDate(String issuanceDate)
    {
        this.issuanceDate = issuanceDate;
    }

    public String getExpiryDate()
    {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate)
    {
        this.expiryDate = expiryDate;
    }

    public Long getPspRequestId()
    {
        return pspRequestId;
    }

    public void setPspRequestId(Long pspRequestId)
    {
        this.pspRequestId = pspRequestId;
    }

    public Long getIPGId()
    {
        return IPGId;
    }

    public void setIPGId(Long IPGId)
    {
        this.IPGId = IPGId;
    }

    public Long geteNamadId()
    {
        return eNamadId;
    }

    public void seteNamadId(Long eNamadId)
    {
        this.eNamadId = eNamadId;
    }

    public String getSiteAddress()
    {
        return siteAddress;
    }

    public void setSiteAddress(String siteAddress)
    {
        this.siteAddress = siteAddress;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getFirstName()
    {
        return firstName;
    }

    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }

    public String getLastName()
    {
        return lastName;
    }

    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }

    public String getPhone()
    {
        return phone;
    }

    public void setPhone(String phone)
    {
        this.phone = phone;
    }

    public String getMobilePhone()
    {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone)
    {
        this.mobilePhone = mobilePhone;
    }

    public Long getTerminalDetailId()
    {
        return terminalDetailId;
    }

    public void setTerminalDetailId(Long terminalDetailId)
    {
        this.terminalDetailId = terminalDetailId;
    }
}
