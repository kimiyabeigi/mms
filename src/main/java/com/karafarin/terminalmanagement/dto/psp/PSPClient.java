package com.karafarin.terminalmanagement.dto.psp;

public class PSPClient
{
    private Long clientId;
    private Long pspRequestId;
    private Long positionId;
    private String clientNumber;
    private String sex;
    private String lifeStatus;
    private String firstName;
    private String firstNameEn;
    private String lastName;
    private String lastNameEn;
    private String fatherName;
    private String fatherNameEn;
    private String nationalCode;
    private String regNumber;
    private String foreignCode;
    private String passportNumber;
    private String birthDate;
    private String passportExpiryDate;
    private String countryNameAbbr;
    private String countryCode;
    private String countryDesc;
    private String issuanceDate;
    private String issuancePlaceCode;
    private String issuancePlaceDesc;
    private String mobilePhone;
    private String phone;
    private String fax;
    private String postalCode;
    private String address;
    private String clientType;
    private String clientKind;
    private String companyName;
    private String companyNameEn;
    private String commerceCode;
    private String errorCode;
    private String errorDesc;
    private String foreignLicenseNo;
    private String foreignLicenseIssuer;

    public String getForeignLicenseNo()
    {
        return foreignLicenseNo;
    }

    public void setForeignLicenseNo(String foreignLicenseNo)
    {
        this.foreignLicenseNo = foreignLicenseNo;
    }

    public String getForeignLicenseIssuer()
    {
        return foreignLicenseIssuer;
    }

    public void setForeignLicenseIssuer(String foreignLicenseIssuer)
    {
        this.foreignLicenseIssuer = foreignLicenseIssuer;
    }

    public String getErrorCode()
    {
        return errorCode;
    }

    public void setErrorCode(String errorCode)
    {
        this.errorCode = errorCode;
    }

    public String getErrorDesc()
    {
        return errorDesc;
    }

    public void setErrorDesc(String errorDesc)
    {
        this.errorDesc = errorDesc;
    }

    public String getBirthDate()
    {
        return birthDate;
    }

    public void setBirthDate(String birthDate)
    {
        this.birthDate = birthDate;
    }

    public String getPassportExpiryDate()
    {
        return passportExpiryDate;
    }

    public void setPassportExpiryDate(String passportExpiryDate)
    {
        this.passportExpiryDate = passportExpiryDate;
    }

    public String getIssuanceDate()
    {
        return issuanceDate;
    }

    public void setIssuanceDate(String issuanceDate)
    {
        this.issuanceDate = issuanceDate;
    }

    public String getClientKind()
    {
        return clientKind;
    }

    public void setClientKind(String clientKind)
    {
        this.clientKind = clientKind;
    }

    public String getCompanyName()
    {
        return companyName;
    }

    public void setCompanyName(String companyName)
    {
        this.companyName = companyName;
    }

    public String getCompanyNameEn()
    {
        return companyNameEn;
    }

    public void setCompanyNameEn(String companyNameEn)
    {
        this.companyNameEn = companyNameEn;
    }

    public String getCommerceCode()
    {
        return commerceCode;
    }

    public void setCommerceCode(String commerceCode)
    {
        this.commerceCode = commerceCode;
    }

    public Long getPspRequestId()
    {
        return pspRequestId;
    }

    public void setPspRequestId(Long pspRequestId)
    {
        this.pspRequestId = pspRequestId;
    }

    public Long getPositionId()
    {
        return positionId;
    }

    public void setPositionId(Long positionId)
    {
        this.positionId = positionId;
    }

    public String getClientType()
    {
        return clientType;
    }

    public void setClientType(String clientType)
    {
        this.clientType = clientType;
    }

    public String getForeignCode()
    {
        return foreignCode;
    }

    public void setForeignCode(String foreignCode)
    {
        this.foreignCode = foreignCode;
    }

    public String getPassportNumber()
    {
        return passportNumber;
    }

    public void setPassportNumber(String passportNumber)
    {
        this.passportNumber = passportNumber;
    }

    public Long getClientId()
    {
        return clientId;
    }

    public void setClientId(Long clientId)
    {
        this.clientId = clientId;
    }

    public String getClientNumber()
    {
        return clientNumber;
    }

    public void setClientNumber(String clientNumber)
    {
        this.clientNumber = clientNumber;
    }

    public String getSex()
    {
        return sex;
    }

    public void setSex(String sex)
    {
        this.sex = sex;
    }

    public String getLifeStatus()
    {
        return lifeStatus;
    }

    public void setLifeStatus(String lifeStatus)
    {
        this.lifeStatus = lifeStatus;
    }

    public String getFirstName()
    {
        return firstName;
    }

    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }

    public String getFirstNameEn()
    {
        return firstNameEn;
    }

    public void setFirstNameEn(String firstNameEn)
    {
        this.firstNameEn = firstNameEn;
    }

    public String getLastName()
    {
        return lastName;
    }

    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }

    public String getLastNameEn()
    {
        return lastNameEn;
    }

    public void setLastNameEn(String lastNameEn)
    {
        this.lastNameEn = lastNameEn;
    }

    public String getFatherName()
    {
        return fatherName;
    }

    public void setFatherName(String fatherName)
    {
        this.fatherName = fatherName;
    }

    public String getFatherNameEn()
    {
        return fatherNameEn;
    }

    public void setFatherNameEn(String fatherNameEn)
    {
        this.fatherNameEn = fatherNameEn;
    }

    public String getNationalCode()
    {
        return nationalCode;
    }

    public void setNationalCode(String nationalCode)
    {
        this.nationalCode = nationalCode;
    }

    public String getRegNumber()
    {
        return regNumber;
    }

    public void setRegNumber(String regNumber)
    {
        this.regNumber = regNumber;
    }

    public String getCountryNameAbbr()
    {
        return countryNameAbbr;
    }

    public void setCountryNameAbbr(String countryNameAbbr)
    {
        this.countryNameAbbr = countryNameAbbr;
    }

    public String getCountryCode()
    {
        return countryCode;
    }

    public void setCountryCode(String countryCode)
    {
        this.countryCode = countryCode;
    }

    public String getCountryDesc()
    {
        return countryDesc;
    }

    public void setCountryDesc(String countryDesc)
    {
        this.countryDesc = countryDesc;
    }

    public String getIssuancePlaceCode()
    {
        return issuancePlaceCode;
    }

    public void setIssuancePlaceCode(String issuancePlaceCode)
    {
        this.issuancePlaceCode = issuancePlaceCode;
    }

    public String getIssuancePlaceDesc()
    {
        return issuancePlaceDesc;
    }

    public void setIssuancePlaceDesc(String issuancePlaceDesc)
    {
        this.issuancePlaceDesc = issuancePlaceDesc;
    }

    public String getMobilePhone()
    {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone)
    {
        this.mobilePhone = mobilePhone;
    }

    public String getPhone()
    {
        return phone;
    }

    public void setPhone(String phone)
    {
        this.phone = phone;
    }

    public String getFax()
    {
        return fax;
    }

    public void setFax(String fax)
    {
        this.fax = fax;
    }

    public String getPostalCode()
    {
        return postalCode;
    }

    public void setPostalCode(String postalCode)
    {
        this.postalCode = postalCode;
    }

    public String getAddress()
    {
        return address;
    }

    public void setAddress(String address)
    {
        this.address = address;
    }
}
