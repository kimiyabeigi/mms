package com.karafarin.terminalmanagement.dto.psp;

public class PSPAccountOwner
{
    private Long accountOwnerId;
    private Long pspRequestId;
    private String clientNumber;
    private String clientName;
    private boolean isActive;
    private String clientType;

    public Long getAccountOwnerId()
    {
        return accountOwnerId;
    }

    public void setAccountOwnerId(Long accountOwnerId)
    {
        this.accountOwnerId = accountOwnerId;
    }

    public Long getPspRequestId()
    {
        return pspRequestId;
    }

    public void setPspRequestId(Long pspRequestId)
    {
        this.pspRequestId = pspRequestId;
    }

    public String getClientNumber()
    {
        return clientNumber;
    }

    public void setClientNumber(String clientNumber)
    {
        this.clientNumber = clientNumber;
    }

    public String getClientName()
    {
        return clientName;
    }

    public void setClientName(String clientName)
    {
        this.clientName = clientName;
    }

    public boolean isActive()
    {
        return isActive;
    }

    public void setActive(boolean active)
    {
        isActive = active;
    }

    public String getClientType()
    {
        return clientType;
    }

    public void setClientType(String clientType)
    {
        this.clientType = clientType;
    }
}
