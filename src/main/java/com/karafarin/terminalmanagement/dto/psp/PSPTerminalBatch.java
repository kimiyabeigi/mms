package com.karafarin.terminalmanagement.dto.psp;

import com.karafarin.terminalmanagement.model.psp.PSPFollowUp;

public class PSPTerminalBatch extends PSPFollowUp
{
    private String pspAcceptanceCode;
    private String pspTerminalCode;

    public String getPspAcceptanceCode()
    {
        return pspAcceptanceCode;
    }

    public void setPspAcceptanceCode(String pspAcceptanceCode)
    {
        this.pspAcceptanceCode = pspAcceptanceCode;
    }

    public String getPspTerminalCode()
    {
        return pspTerminalCode;
    }

    public void setPspTerminalCode(String pspTerminalCode)
    {
        this.pspTerminalCode = pspTerminalCode;
    }
}
