package com.karafarin.terminalmanagement.dto.psp;

public class PSPReportRegisterMaster
{
    private Long pspRequestId;
    private String requestDate;
    private String accountNo;
    private String clientNumber;
    private String branch;
    private String companyName;
    private String requestStatus;
    private Long requestNumber;
    private String pspTrackingCode;
    private String description;
    private String pspAcceptanceCode;
    private String pspTerminalCode;

    public String getRequestDate()
    {
        return requestDate;
    }

    public void setRequestDate(String requestDate)
    {
        this.requestDate = requestDate;
    }

    public Long getPspRequestId()
    {
        return pspRequestId;
    }

    public void setPspRequestId(Long pspRequestId)
    {
        this.pspRequestId = pspRequestId;
    }

    public String getAccountNo()
    {
        return accountNo;
    }

    public void setAccountNo(String accountNo)
    {
        this.accountNo = accountNo;
    }

    public String getClientNumber()
    {
        return clientNumber;
    }

    public void setClientNumber(String clientNumber)
    {
        this.clientNumber = clientNumber;
    }

    public String getBranch()
    {
        return branch;
    }

    public void setBranch(String branch)
    {
        this.branch = branch;
    }

    public String getCompanyName()
    {
        return companyName;
    }

    public void setCompanyName(String companyName)
    {
        this.companyName = companyName;
    }

    public String getRequestStatus()
    {
        return requestStatus;
    }

    public void setRequestStatus(String requestStatus)
    {
        this.requestStatus = requestStatus;
    }

    public Long getRequestNumber()
    {
        return requestNumber;
    }

    public void setRequestNumber(Long requestNumber)
    {
        this.requestNumber = requestNumber;
    }

    public String getPspTrackingCode()
    {
        return pspTrackingCode;
    }

    public void setPspTrackingCode(String pspTrackingCode)
    {
        this.pspTrackingCode = pspTrackingCode;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getPspAcceptanceCode()
    {
        return pspAcceptanceCode;
    }

    public void setPspAcceptanceCode(String pspAcceptanceCode)
    {
        this.pspAcceptanceCode = pspAcceptanceCode;
    }

    public String getPspTerminalCode()
    {
        return pspTerminalCode;
    }

    public void setPspTerminalCode(String pspTerminalCode)
    {
        this.pspTerminalCode = pspTerminalCode;
    }
}
