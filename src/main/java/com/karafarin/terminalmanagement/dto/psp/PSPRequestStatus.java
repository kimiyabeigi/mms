package com.karafarin.terminalmanagement.dto.psp;

public class PSPRequestStatus
{
    private Long pspRequestStatusId;
    private Long pspRequestId;
    private Long requestStatusId;
    private Long statusTypeId;
    private Long serviceId;
    private String pspErrorCode;
    private String pspErrorDesc;
    private String trackingCode;
    private String createDate;
    private String status;

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public Long getRequestStatusId()
    {
        return requestStatusId;
    }

    public void setRequestStatusId(Long requestStatusId)
    {
        this.requestStatusId = requestStatusId;
    }

    public String getCreateDate()
    {
        return createDate;
    }

    public void setCreateDate(String createDate)
    {
        this.createDate = createDate;
    }

    public Long getPspRequestStatusId()
    {
        return pspRequestStatusId;
    }

    public void setPspRequestStatusId(Long pspRequestStatusId)
    {
        this.pspRequestStatusId = pspRequestStatusId;
    }

    public Long getPspRequestId()
    {
        return pspRequestId;
    }

    public void setPspRequestId(Long pspRequestId)
    {
        this.pspRequestId = pspRequestId;
    }

    public Long getServiceId()
    {
        return serviceId;
    }

    public void setServiceId(Long serviceId)
    {
        this.serviceId = serviceId;
    }

    public String getPspErrorCode()
    {
        return pspErrorCode;
    }

    public void setPspErrorCode(String pspErrorCode)
    {
        this.pspErrorCode = pspErrorCode;
    }

    public String getPspErrorDesc()
    {
        return pspErrorDesc;
    }

    public void setPspErrorDesc(String pspErrorDesc)
    {
        this.pspErrorDesc = pspErrorDesc;
    }

    public String getTrackingCode()
    {
        return trackingCode;
    }

    public void setTrackingCode(String trackingCode)
    {
        this.trackingCode = trackingCode;
    }

    public Long getStatusTypeId()
    {
        return statusTypeId;
    }

    public void setStatusTypeId(Long statusTypeId)
    {
        this.statusTypeId = statusTypeId;
    }
}
