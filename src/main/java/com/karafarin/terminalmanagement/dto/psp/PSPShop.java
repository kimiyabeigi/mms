package com.karafarin.terminalmanagement.dto.psp;

public class PSPShop
{
    private Long shopId;
    private Long pspRequestId;
    private String shopName;
    private String shopNameEn;
    private String ownershipType;
    private Long guildId;
    private String contractNo;
    private String contractExpiryDate;
    private String licenseNo;
    private String licenseIssuanceDate;
    private String licenseValidityDate;
    private String countryNameAbbr;
    private String countryName;
    private Long stateId;
    private Long cityId;
    private String municipalityNo;
    private String postalCode;
    private String address;
    private String phone;
    private String fax;

    public String getContractExpiryDate()
    {
        return contractExpiryDate;
    }

    public void setContractExpiryDate(String contractExpiryDate)
    {
        this.contractExpiryDate = contractExpiryDate;
    }

    public String getLicenseIssuanceDate()
    {
        return licenseIssuanceDate;
    }

    public void setLicenseIssuanceDate(String licenseIssuanceDate)
    {
        this.licenseIssuanceDate = licenseIssuanceDate;
    }

    public String getLicenseValidityDate()
    {
        return licenseValidityDate;
    }

    public void setLicenseValidityDate(String licenseValidityDate)
    {
        this.licenseValidityDate = licenseValidityDate;
    }

    public Long getShopId()
    {
        return shopId;
    }

    public void setShopId(Long shopId)
    {
        this.shopId = shopId;
    }

    public Long getPspRequestId()
    {
        return pspRequestId;
    }

    public void setPspRequestId(Long pspRequestId)
    {
        this.pspRequestId = pspRequestId;
    }

    public String getShopName()
    {
        return shopName;
    }

    public void setShopName(String shopName)
    {
        this.shopName = shopName;
    }

    public String getShopNameEn()
    {
        return shopNameEn;
    }

    public void setShopNameEn(String shopNameEn)
    {
        this.shopNameEn = shopNameEn;
    }

    public String getOwnershipType()
    {
        return ownershipType;
    }

    public void setOwnershipType(String ownershipType)
    {
        this.ownershipType = ownershipType;
    }

    public Long getGuildId()
    {
        return guildId;
    }

    public void setGuildId(Long guildId)
    {
        this.guildId = guildId;
    }

    public String getContractNo()
    {
        return contractNo;
    }

    public void setContractNo(String contractNo)
    {
        this.contractNo = contractNo;
    }

    public String getLicenseNo()
    {
        return licenseNo;
    }

    public void setLicenseNo(String licenseNo)
    {
        this.licenseNo = licenseNo;
    }

    public String getCountryNameAbbr()
    {
        return countryNameAbbr;
    }

    public void setCountryNameAbbr(String countryNameAbbr)
    {
        this.countryNameAbbr = countryNameAbbr;
    }

    public String getCountryName()
    {
        return countryName;
    }

    public void setCountryName(String countryName)
    {
        this.countryName = countryName;
    }

    public Long getStateId()
    {
        return stateId;
    }

    public void setStateId(Long stateId)
    {
        this.stateId = stateId;
    }

    public Long getCityId()
    {
        return cityId;
    }

    public void setCityId(Long cityId)
    {
        this.cityId = cityId;
    }

    public String getMunicipalityNo()
    {
        return municipalityNo;
    }

    public void setMunicipalityNo(String municipalityNo)
    {
        this.municipalityNo = municipalityNo;
    }

    public String getPostalCode()
    {
        return postalCode;
    }

    public void setPostalCode(String postalCode)
    {
        this.postalCode = postalCode;
    }

    public String getAddress()
    {
        return address;
    }

    public void setAddress(String address)
    {
        this.address = address;
    }

    public String getPhone()
    {
        return phone;
    }

    public void setPhone(String phone)
    {
        this.phone = phone;
    }

    public String getFax()
    {
        return fax;
    }

    public void setFax(String fax)
    {
        this.fax = fax;
    }
}
