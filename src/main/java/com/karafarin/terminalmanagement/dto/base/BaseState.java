package com.karafarin.terminalmanagement.dto.base;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Pattern;

public class BaseState
{
    private Long stateId;

    @Length(max = 10, message = "{validate.state.code.length}")
    @Pattern(regexp = "^([ 0-9]+)|(?)$", message = "{validate.state.code.pattern}")
    @NotEmpty(message = "{validate.state.code.mandatory}")
    private String stateCode;

    @Length(max = 30, message = "{validate.state.name.length}")
    @NotEmpty(message = "{validate.state.name.mandatory}")
    private String name;

    @Length(max = 30, message = "{validate.state.enName.length}")
    @Pattern(regexp = "^([ A-Za-z]+)|(?)$", message = "{validate.state.enName.pattern}")
    private String nameEN;

    private boolean active;

    public Long getStateId()
    {
        return stateId;
    }

    public void setStateId(Long stateId)
    {
        this.stateId = stateId;
    }

    public String getStateCode() {
        return stateCode;
    }

    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getNameEN()
    {
        return nameEN;
    }

    public void setNameEN(String nameEN)
    {
        this.nameEN = nameEN;
    }

    public boolean isActive()
    {
        return active;
    }

    public void setActive(boolean active)
    {
        this.active = active;
    }

}
