package com.karafarin.terminalmanagement.dto.base;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Pattern;

public class BaseTerminalDetail {

    private Long terminalDetailId;

    @Length(max = 15, message = "{validate.terminal.code.length}")
    @Pattern(regexp = "^([ 0-9A-Za-z×!@#$%^&*)(+-/|{}\":?><,.`~_]+)|(?)$", message = "{validate.terminal.code.pattern}")
    @NotEmpty(message = "{validate.terminal.code.mandatory}")
    private String terminalDetailCode;

    private Long terminalId;

    @Length(max = 30, message = "{validate.terminal.name.length}")
    @NotEmpty(message = "{validate.terminal.name.mandatory}")
    private String name;

    public Long getTerminalDetailId() {
        return terminalDetailId;
    }

    public void setTerminalDetailId(Long terminalDetailId) {
        this.terminalDetailId = terminalDetailId;
    }

    public String getTerminalDetailCode() {
        return terminalDetailCode;
    }

    public void setTerminalDetailCode(String terminalDetailCode) {
        this.terminalDetailCode = terminalDetailCode;
    }

    public Long getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(Long terminalId) {
        this.terminalId = terminalId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
