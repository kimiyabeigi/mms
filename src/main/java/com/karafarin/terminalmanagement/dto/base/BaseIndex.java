package com.karafarin.terminalmanagement.dto.base;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Pattern;

public class BaseIndex
{
    private Long indexId;

    @Length(max = 15, message = "{validate.index.code.length}")
    @Pattern(regexp = "^([ A-Za-z]+)|(?)$", message = "{validate.index.code.pattern}")
    @NotEmpty(message = "{validate.index.code.mandatory}")
    private String indexCode;

    @Length(max = 30, message = "{validate.index.name.length}")
    @Pattern(regexp = "^([ a-z0-9A-Z\u0600-\u06FF]+)|(?)$", message = "{validate.index.name.pattern}")
    @NotEmpty(message = "{validate.index.name.mandatory}")
    private String name;

    public void setIndexId(Long indexId) {
        this.indexId = indexId;
    }

    public String getIndexCode() {
        return indexCode;
    }

    public void setIndexCode(String indexCode) {
        this.indexCode = indexCode;
    }

    public Long getIndexId() {
        return indexId;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

}
