package com.karafarin.terminalmanagement.dto.base;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

public class BaseCompany
{
    private Long companyId;

    @Length(max = 2, message = "{validate.company.code.length}")
    @Pattern(regexp = "^([A-Za-z]+)|(?)$", message = "{validate.company.code.pattern}")
    @NotEmpty(message = "{validate.company.code.mandatory}")
    private String companyCode;

    @Length(max = 50, message = "{validate.company.name.length}")
    @NotEmpty(message = "{validate.company.name.mandatory}")
    @Pattern(regexp = "^([ 0-9a-zA-Z\u0600-\u06FF]+)|(?)$" , message = "{validate.company.name.pattern}")
    private String name;

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

}
