package com.karafarin.terminalmanagement.dto.base;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Pattern;

public class BaseCity
{
    private Long cityId;

    @Length(max = 10, message = "{validate.city.code.length}")
    @Pattern(regexp = "^([0-9]+)|(?)$", message = "{validate.city.code.pattern}")
    @NotEmpty(message = "{validate.city.code.mandatory}")
    private String cityCode;

    private Long stateId;

    @Length(max = 30, message = "{validate.city.name.length}")
    @NotEmpty(message = "{validate.city.name.mandatory}")
    private String name;

    @Length(max = 30, message = "{validate.city.enName.length}")
    @Pattern(regexp = "^([ A-Za-z]+)|(?)$", message = "{validate.city.enName.pattern}")
    private String nameEN;

    private boolean active;

    public boolean isActive()
    {
        return active;
    }

    public void setActive(boolean active)
    {
        this.active = active;
    }

    public Long getCityId()
    {
        return cityId;
    }

    public void setCityId(Long cityId)
    {
        this.cityId = cityId;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public Long getStateId()
    {
        return stateId;
    }

    public void setStateId(Long stateId)
    {
        this.stateId = stateId;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getNameEN()
    {
        return nameEN;
    }

    public void setNameEN(String nameEN)
    {
        this.nameEN = nameEN;
    }

}
