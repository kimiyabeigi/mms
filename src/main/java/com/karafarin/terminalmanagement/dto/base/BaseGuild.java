package com.karafarin.terminalmanagement.dto.base;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Pattern;

public class BaseGuild {

    private Long guildId;

    @Length(max = 25, message = "{validate.guild.code.length}")
    @NotEmpty(message = "{validate.guild.code.mandatory}")
    private String guildCode;

    @Length(max = 30, message = "{validate.guild.description.length}")
    @NotEmpty(message = "{validate.guild.description.mandatory}")
    private String description;

    private boolean isActive;

    public Long getGuildId() {
        return guildId;
    }

    public void setGuildId(Long guildId) {
        this.guildId = guildId;
    }

    public String getGuildCode() {
        return guildCode;
    }

    public void setGuildCode(String guildCode) {
        this.guildCode = guildCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

}
