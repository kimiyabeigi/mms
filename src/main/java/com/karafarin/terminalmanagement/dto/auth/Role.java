package com.karafarin.terminalmanagement.dto.auth;

import org.hibernate.validator.constraints.NotEmpty;

public class Role
{
    private Long roleId;

    @NotEmpty(message = "{validate.role.name}")
    private String name;

    @NotEmpty(message = "{validate.role.name.english}")
    private String nameEN;

    private String description;

    public Long getRoleId()
    {
        return roleId;
    }

    public void setRoleId(Long roleId)
    {
        this.roleId = roleId;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getNameEN()
    {
        return nameEN;
    }

    public void setNameEN(String nameEN)
    {
        this.nameEN = nameEN;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }
}
