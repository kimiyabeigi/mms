package com.karafarin.terminalmanagement.dto.auth;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Pattern;

public class User
{
    private Long userId;
    @NotEmpty(message = "{validate.user.userCode.mandatory}")
    private String userCode;

    @NotEmpty(message = "{validate.user.firstName.mandatory}")
    private String firstName;

    @NotEmpty(message = "{validate.user.lastName.mandatory}")
    private String lastName;

    @NotEmpty(message = "{validate.user.userName.mandatory}")
    private String userName;

/*
    @Pattern(regexp = "(?=^.{8,}$)(?=.*\\d)(?=.*[!@#$%^&*]+)(?![.\\n])(?=.*[A-Z])(?=.*[a-z]).*$",
             message = "{validate.password.pattern}")
*/
    private String password;

    private Long roleId;

    private Long branchId;

    private boolean isActive;

    private boolean firstLogin;

    public String getUserCode()
    {
        return userCode;
    }

    public void setUserCode(String userCode)
    {
        this.userCode = userCode;
    }

    public boolean isFirstLogin()
    {
        return firstLogin;
    }

    public void setFirstLogin(boolean firstLogin)
    {
        this.firstLogin = firstLogin;
    }

    public boolean isActive()
    {
        return isActive;
    }

    public void setActive(boolean active)
    {
        isActive = active;
    }

    public Long getUserId()
    {
        return userId;
    }

    public void setUserId(Long userId)
    {
        this.userId = userId;
    }

    public String getFirstName()
    {
        return firstName;
    }

    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }

    public String getLastName()
    {
        return lastName;
    }

    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }

    public String getUserName()
    {
        return userName;
    }

    public void setUserName(String userName)
    {
        this.userName = userName;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public Long getRoleId()
    {
        return roleId;
    }

    public void setRoleId(Long roleId)
    {
        this.roleId = roleId;
    }

    public Long getBranchId()
    {
        return branchId;
    }

    public void setBranchId(Long branchId)
    {
        this.branchId = branchId;
    }
}
