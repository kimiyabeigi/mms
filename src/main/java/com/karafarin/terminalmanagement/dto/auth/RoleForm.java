package com.karafarin.terminalmanagement.dto.auth;

public class RoleForm
{
    private Long roleFormId;
    private Long roleId;
    private Long formId;
    private boolean selected;

    public boolean isSelected()
    {
        return selected;
    }

    public void setSelected(boolean selected)
    {
        this.selected = selected;
    }

    public Long getRoleFormId()
    {
        return roleFormId;
    }

    public void setRoleFormId(Long roleFormId)
    {
        this.roleFormId = roleFormId;
    }

    public Long getRoleId()
    {
        return roleId;
    }

    public void setRoleId(Long roleId)
    {
        this.roleId = roleId;
    }

    public Long getFormId()
    {
        return formId;
    }

    public void setFormId(Long formId)
    {
        this.formId = formId;
    }
}
