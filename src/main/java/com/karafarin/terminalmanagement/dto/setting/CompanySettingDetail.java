package com.karafarin.terminalmanagement.dto.setting;

public class CompanySettingDetail
{
    private Long companySettingDetailId;
    private Long companySettingId;
    private Long terminalId;
    private boolean selected;

    public boolean isSelected()
    {
        return selected;
    }

    public void setSelected(boolean selected)
    {
        this.selected = selected;
    }

    public Long getTerminalId()
    {
        return terminalId;
    }

    public void setTerminalId(Long terminalId)
    {
        this.terminalId = terminalId;
    }

    public Long getCompanySettingDetailId()
    {
        return companySettingDetailId;
    }

    public void setCompanySettingDetailId(Long companySettingDetailId)
    {
        this.companySettingDetailId = companySettingDetailId;
    }

    public Long getCompanySettingId()
    {
        return companySettingId;
    }

    public void setCompanySettingId(Long companySettingId)
    {
        this.companySettingId = companySettingId;
    }

}
