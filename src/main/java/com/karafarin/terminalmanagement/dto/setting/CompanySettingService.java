package com.karafarin.terminalmanagement.dto.setting;

public class CompanySettingService
{
    private Long companySettingServiceId;
    private Long companySettingId;
    private Long serviceId;
    private boolean isActive;

    public Long getServiceId()
    {
        return serviceId;
    }

    public void setServiceId(Long serviceId)
    {
        this.serviceId = serviceId;
    }

    public Long getCompanySettingServiceId()
    {
        return companySettingServiceId;
    }

    public void setCompanySettingServiceId(Long companySettingServiceId)
    {
        this.companySettingServiceId = companySettingServiceId;
    }

    public Long getCompanySettingId()
    {
        return companySettingId;
    }

    public void setCompanySettingId(Long companySettingId)
    {
        this.companySettingId = companySettingId;
    }

    public boolean isActive()
    {
        return isActive;
    }

    public void setActive(boolean active)
    {
        isActive = active;
    }

}
