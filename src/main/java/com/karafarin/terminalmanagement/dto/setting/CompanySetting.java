package com.karafarin.terminalmanagement.dto.setting;

import org.hibernate.validator.constraints.NotEmpty;

import java.util.List;

public class CompanySetting
{

    private Long companySettingId;

    private Long companyId;

    private String organizationCode;

    private boolean isActive;
    private List<CompanySettingDetail> companySettingDetails;

    public Long getCompanyId()
    {
        return companyId;
    }

    public void setCompanyId(Long companyId)
    {
        this.companyId = companyId;
    }

    public String getOrganizationCode()
    {
        return organizationCode;
    }

    public void setOrganizationCode(String organizationCode)
    {
        this.organizationCode = organizationCode;
    }

    public Long getCompanySettingId()
    {
        return companySettingId;
    }

    public void setCompanySettingId(Long companySettingId)
    {
        this.companySettingId = companySettingId;
    }

    public boolean isActive()
    {
        return isActive;
    }

    public void setActive(boolean active)
    {
        isActive = active;
    }

    public List<CompanySettingDetail> getCompanySettingDetails()
    {
        return companySettingDetails;
    }

    public void setCompanySettingDetails(List<CompanySettingDetail> companySettingDetails)
    {
        this.companySettingDetails = companySettingDetails;
    }
}
