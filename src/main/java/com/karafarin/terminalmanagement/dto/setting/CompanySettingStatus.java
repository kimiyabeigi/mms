package com.karafarin.terminalmanagement.dto.setting;

import org.hibernate.validator.constraints.NotEmpty;

public class CompanySettingStatus
{
    private Long companySettingStatusId;
    private Long companySettingId;
    private Long serviceId;
    private Long indexId;

    @NotEmpty(message = "{validate.companySettingStatus.statusCode.mandatory}")
    private String statusCode;

    private Long statusTypeId;

    public Long getCompanySettingStatusId()
    {
        return companySettingStatusId;
    }

    public void setCompanySettingStatusId(Long companySettingStatusId)
    {
        this.companySettingStatusId = companySettingStatusId;
    }

    public Long getCompanySettingId()
    {
        return companySettingId;
    }

    public void setCompanySettingId(Long companySettingId)
    {
        this.companySettingId = companySettingId;
    }

    public Long getServiceId()
    {
        return serviceId;
    }

    public void setServiceId(Long serviceId)
    {
        this.serviceId = serviceId;
    }

    public Long getIndexId()
    {
        return indexId;
    }

    public void setIndexId(Long indexId)
    {
        this.indexId = indexId;
    }

    public String getStatusCode()
    {
        return statusCode;
    }

    public void setStatusCode(String statusCode)
    {
        this.statusCode = statusCode;
    }

    public Long getStatusTypeId()
    {
        return statusTypeId;
    }

    public void setStatusTypeId(Long statusTypeId)
    {
        this.statusTypeId = statusTypeId;
    }

}
