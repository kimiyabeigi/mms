package com.karafarin.terminalmanagement.dto.setting;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

public class CompanySettingTerminal
{
    private Long companySettingTerminalId;
    private Long companySettingId;
    private Long terminalDetailId;

    @NotEmpty(message = "{validate.companySettingTerminal.connectionCode.mandatory}")
    @Length(max = 10, message = "{validate.companySettingTerminal.connectionCode.length}")
    private String ConnectionCode;

    private boolean isActive;

    public Long getTerminalDetailId()
    {
        return terminalDetailId;
    }

    public void setTerminalDetailId(Long terminalDetailId)
    {
        this.terminalDetailId = terminalDetailId;
    }

    public Long getCompanySettingTerminalId()
    {
        return companySettingTerminalId;
    }

    public void setCompanySettingTerminalId(Long companySettingTerminalId)
    {
        this.companySettingTerminalId = companySettingTerminalId;
    }

    public Long getCompanySettingId()
    {
        return companySettingId;
    }

    public void setCompanySettingId(Long companySettingId)
    {
        this.companySettingId = companySettingId;
    }

    public String getConnectionCode()
    {
        return ConnectionCode;
    }

    public void setConnectionCode(String connectionCode)
    {
        ConnectionCode = connectionCode;
    }

    public boolean isActive()
    {
        return isActive;
    }

    public void setActive(boolean active)
    {
        isActive = active;
    }
}
