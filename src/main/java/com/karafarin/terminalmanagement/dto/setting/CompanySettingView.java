package com.karafarin.terminalmanagement.dto.setting;

public class CompanySettingView
{
    private Long companySettingId;
    private String companyCode;
    private String name;
    private boolean isActive;

    public Long getCompanySettingId()
    {
        return companySettingId;
    }

    public void setCompanySettingId(Long companySettingId)
    {
        this.companySettingId = companySettingId;
    }

    public String getCompanyCode()
    {
        return companyCode;
    }

    public void setCompanyCode(String companyCode)
    {
        this.companyCode = companyCode;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public boolean isActive()
    {
        return isActive;
    }

    public void setActive(boolean active)
    {
        isActive = active;
    }
}
