package com.karafarin.terminalmanagement.controller.auth;

import com.karafarin.terminalmanagement.dto.auth.Role;
import com.karafarin.terminalmanagement.message.Response;
import com.karafarin.terminalmanagement.service.impl.auth.ServiceRoleImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(path = "/api/role")
public class RestRoleController
{
    @Autowired
    private ServiceRoleImpl serviceRole;

    @RequestMapping(method = RequestMethod.GET)
    @PreAuthorize("@permissionService.hasPermission('api/role', 'GET')")
    public Response getRoles() {
        return serviceRole.findAll();
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public Response getRole(@PathVariable Long id) {
        return serviceRole.findById(id);
    }

    @RequestMapping(method = RequestMethod.PUT)
    public Response putRole(@RequestBody @Valid Role role) {
        return serviceRole.add(role);
    }

    @RequestMapping(method = RequestMethod.POST)
    public Response postRole(@RequestBody @Valid Role role)
    {
        return serviceRole.update(role);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    public Response delRole(@PathVariable Long id){
        return serviceRole.delete(id);
    }
}
