package com.karafarin.terminalmanagement.controller.auth;

import com.karafarin.terminalmanagement.message.Response;
import com.karafarin.terminalmanagement.service.impl.auth.ServiceFormImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/api/form")
public class RestFormController
{
    @Autowired
    private ServiceFormImpl serviceForm;

    @RequestMapping(method = RequestMethod.GET)
    public Response getForms() {
        return serviceForm.findAll();
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public Response getForm(@PathVariable Long id) {
        return serviceForm.findById(id);
    }

}
