package com.karafarin.terminalmanagement.controller.auth;

import com.karafarin.terminalmanagement.dto.auth.User;
import com.karafarin.terminalmanagement.message.Response;
import com.karafarin.terminalmanagement.model.ChangePassword;
import com.karafarin.terminalmanagement.service.impl.auth.ServiceUserImpl;
import com.karafarin.terminalmanagement.util.UserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(path = "/api/user")
public class RestUserController
{
    @Autowired
    private ServiceUserImpl serviceUser;

    @RequestMapping(method = RequestMethod.GET)
    @PreAuthorize("@permissionService.hasPermission('api/user', 'GET')")
    public Response getUsers() {
        return serviceUser.findAll();
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public Response getUser(@PathVariable Long id) {
        return serviceUser.findById(id);
    }

    @RequestMapping(path = "/current", method = RequestMethod.GET)
    public Response getCurrentUser() {
        return serviceUser.findUserByUserNameAndPassword(UserUtil.getCurrentUser().getUserName());
    }

    @RequestMapping(method = RequestMethod.PUT)
    public Response putUser(@RequestBody @Valid User user) {
        return serviceUser.add(user);
    }

    @RequestMapping(method = RequestMethod.POST)
    public Response postUser(@RequestBody @Valid User user)
    {
        return serviceUser.update(user);
    }

    @RequestMapping(path = "/change/password", method = RequestMethod.POST)
    public Response postPassword(@RequestBody @Valid ChangePassword changePassword)
    {
        return serviceUser.changePassword(changePassword, false);
    }

    @RequestMapping(path = "/change/password/admin", method = RequestMethod.POST)
    public Response postPasswordByAdmin(@RequestBody @Valid ChangePassword changePassword)
    {
        return serviceUser.changePassword(changePassword, true);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    public Response delUser(@PathVariable Long id){
        return serviceUser.delete(id);
    }
}
