package com.karafarin.terminalmanagement.controller.auth;

import com.karafarin.terminalmanagement.dto.auth.RoleForm;
import com.karafarin.terminalmanagement.message.Response;
import com.karafarin.terminalmanagement.service.impl.auth.ServiceRoleFormImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/api/roleForm")
public class RestRoleFormController
{
    @Autowired
    private ServiceRoleFormImpl serviceRoleForm;

    @RequestMapping(method = RequestMethod.GET)
    public Response getRoleForms() {
        return serviceRoleForm.findAll();
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public Response getRoleForm(@PathVariable Long id) {
        return serviceRoleForm.findByRoleId(id);
    }

    @RequestMapping(method = RequestMethod.PUT)
    public Response putRoleForm(@RequestBody List<RoleForm> roleForms) {
        return serviceRoleForm.addList(roleForms);
    }

    @RequestMapping(method = RequestMethod.POST)
    public Response postRoleForm(@RequestBody List<RoleForm> roleForms)
    {
        return serviceRoleForm.updateList(roleForms);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    public Response delRoleForm(@PathVariable Long id){
        return serviceRoleForm.delete(id);
    }

}
