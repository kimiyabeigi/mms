package com.karafarin.terminalmanagement.controller.setting;

import com.karafarin.terminalmanagement.dto.setting.CompanySetting;
import com.karafarin.terminalmanagement.message.Response;
import com.karafarin.terminalmanagement.service.impl.setting.ServiceCompanySettingImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(path = "/api/company/setting")
public class RestCompanySettingController
{
    @Autowired
    private ServiceCompanySettingImpl serviceCompanySetting;

    @RequestMapping(method = RequestMethod.GET)
    public Response getCompanySettings() {
        return serviceCompanySetting.findAll();
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public Response getCompanySetting(@PathVariable Long id) {
        return serviceCompanySetting.findById(id);
    }

    @RequestMapping(path = "/view", method = RequestMethod.GET)
    @PreAuthorize("@permissionService.hasPermission('/api/company/setting/view', 'GET')")
    public Response getCompanySettingViews() {
        return serviceCompanySetting.findViewAll();
    }

    @RequestMapping(path = "view/{id}", method = RequestMethod.GET)
    public Response getCompanySettingView(@PathVariable Long id) {
        return serviceCompanySetting.findViewById(id);
    }

    @RequestMapping(method = RequestMethod.PUT)
    public Response putCompanySetting(@RequestBody CompanySetting companySetting) {
        return serviceCompanySetting.add(companySetting);
    }

    @RequestMapping(method = RequestMethod.POST)
    public Response postCompanySetting(@RequestBody CompanySetting companySetting)
    {
        return serviceCompanySetting.update(companySetting);
    }
}
