package com.karafarin.terminalmanagement.controller.setting;

import com.karafarin.terminalmanagement.dto.setting.CompanySettingService;
import com.karafarin.terminalmanagement.message.Response;
import com.karafarin.terminalmanagement.service.impl.setting.ServiceCompanySettingServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/api/company/setting/service")
public class RestCompanySettingServiceController
{
    @Autowired
    private ServiceCompanySettingServiceImpl serviceCompanySettingService;

    @RequestMapping(method = RequestMethod.GET)
    public Response getCompanySettingServices() {
        return serviceCompanySettingService.findAll();
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public Response getCompanySettingService(@PathVariable Long id) {
        return serviceCompanySettingService.findById(id);
    }

    @RequestMapping(path = "master/{id}", method = RequestMethod.GET)
    public Response getCompanySettingServiceByCompanySettingId(@PathVariable Long id) {
        return serviceCompanySettingService.findByCompanySettingId(id);
    }

    @RequestMapping(method = RequestMethod.PUT)
    public Response putCompanySettingService(@RequestBody CompanySettingService companySettingService) {
        return serviceCompanySettingService.add(companySettingService);
    }

    @RequestMapping(method = RequestMethod.POST)
    public Response postCompanySetting(@RequestBody CompanySettingService companySettingService)
    {
        return serviceCompanySettingService.update(companySettingService);
    }

}
