package com.karafarin.terminalmanagement.controller.setting;

import com.karafarin.terminalmanagement.dto.setting.CompanySettingTerminal;
import com.karafarin.terminalmanagement.message.Response;
import com.karafarin.terminalmanagement.service.impl.setting.ServiceCompanySettingTerminalImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(path = "/api/company/setting/terminal")
public class RestCompanySettingTerminalController
{
    @Autowired
    private ServiceCompanySettingTerminalImpl serviceCompanySettingTerminal;

    @RequestMapping(method = RequestMethod.GET)
    public Response getCompanySettingTerminals() {
        return serviceCompanySettingTerminal.findAll();
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public Response getCompanySettingTerminal(@PathVariable Long id) {
        return serviceCompanySettingTerminal.findById(id);
    }

    @RequestMapping(path = "master/{id}", method = RequestMethod.GET)
    public Response getCompanySettingTerminalByCompanySettingId(@PathVariable Long id) {
        return serviceCompanySettingTerminal.findByCompanySettingId(id);
    }

    @RequestMapping(method = RequestMethod.PUT)
    public Response putCompanySettingTerminal(@RequestBody @Valid CompanySettingTerminal companySettingTerminal) {
        return serviceCompanySettingTerminal.add(companySettingTerminal);
    }

    @RequestMapping(method = RequestMethod.POST)
    public Response postCompanySettingTerminal(@RequestBody @Valid CompanySettingTerminal companySettingTerminal)
    {
        return serviceCompanySettingTerminal.update(companySettingTerminal);
    }
}
