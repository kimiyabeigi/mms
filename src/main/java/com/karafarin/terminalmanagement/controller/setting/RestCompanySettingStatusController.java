package com.karafarin.terminalmanagement.controller.setting;

import com.karafarin.terminalmanagement.dto.setting.CompanySettingStatus;
import com.karafarin.terminalmanagement.message.Response;
import com.karafarin.terminalmanagement.service.impl.setting.ServiceCompanySettingStatusImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(path = "/api/company/setting/status")
public class RestCompanySettingStatusController
{
    @Autowired
    private ServiceCompanySettingStatusImpl sserviceCompanySettingStatus;

    @RequestMapping(method = RequestMethod.GET)
    public Response getAllCompanySettingStatus() {
        return sserviceCompanySettingStatus.findAll();
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public Response getCompanySettingStatus(@PathVariable Long id) {
        return sserviceCompanySettingStatus.findById(id);
    }

    @RequestMapping(path = "master/{id}", method = RequestMethod.GET)
    public Response getCompanySettingStatusByCompanySettingId(@PathVariable Long id) {
        return sserviceCompanySettingStatus.findByCompanySettingId(id);
    }

    @RequestMapping(method = RequestMethod.PUT)
    public Response putCompanySettingStatus(@RequestBody @Valid CompanySettingStatus companySettingStatus) {
        return sserviceCompanySettingStatus.add(companySettingStatus);
    }

    @RequestMapping(method = RequestMethod.POST)
    public Response postCompanySettingStatus(@RequestBody @Valid CompanySettingStatus companySettingTerminal)
    {
        return sserviceCompanySettingStatus.update(companySettingTerminal);
    }
}
