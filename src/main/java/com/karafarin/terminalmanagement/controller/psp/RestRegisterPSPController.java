package com.karafarin.terminalmanagement.controller.psp;

import com.karafarin.terminalmanagement.message.Response;
import com.karafarin.terminalmanagement.message.enums.ResponseStatusEnum;
import com.karafarin.terminalmanagement.model.Messages;
import com.karafarin.terminalmanagement.model.psp.RegisterRequest;
import com.karafarin.terminalmanagement.service.impl.psp.ServiceRegisterPSPImpl;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/api/psp/register")
public class RestRegisterPSPController
{
    @Autowired
    private ServiceRegisterPSPImpl serviceRegisterPSP;
    @Autowired
    private Messages messages;
    private static final Logger logger = LogManager.getLogger(RestRegisterPSPController.class);

    @RequestMapping(method = RequestMethod.PUT)
    public Response putCompanySetting(@RequestBody RegisterRequest registerRequest)
    {
        Response response = new Response();
        try
        {
            response = serviceRegisterPSP.registerRequest(registerRequest);
        } catch (Exception e)
        {
            logger.error("There is an error in method [putCompanySetting] for [RegisterRequest]", e);
            response.setStatus(ResponseStatusEnum.ERROR);
            response.setDescription(messages.get("system.error"));
            response.setData(null);
        }
        return response;
    }

}
