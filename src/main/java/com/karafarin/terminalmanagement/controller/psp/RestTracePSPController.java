package com.karafarin.terminalmanagement.controller.psp;

import com.karafarin.terminalmanagement.dto.psp.PSPStatusBatch;
import com.karafarin.terminalmanagement.dto.psp.PSPTerminalBatch;
import com.karafarin.terminalmanagement.dto.psp.PSPTraceBatch;
import com.karafarin.terminalmanagement.message.Response;
import com.karafarin.terminalmanagement.service.impl.psp.ServiceTracePSPImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/api/psp/trace")
public class RestTracePSPController
{
    @Autowired
    private ServiceTracePSPImpl serviceTracePSP;

    @RequestMapping(path = "batch/unknown", method = RequestMethod.GET)
    @PreAuthorize("@permissionService.hasPermission('api/psp/trace/batch/unknown', 'GET')")
    public Response getPSPTraceUnknowns()
    {
        return serviceTracePSP.findAllUnknownRequests();
    }

    @RequestMapping(path = "batch/unknown", method = RequestMethod.PUT)
    public Response putPSPTraceUnknown(@RequestBody List<PSPTraceBatch> pspTraceBatches)
    {
        return serviceTracePSP.traceUnknownRequest(pspTraceBatches);
    }

    @RequestMapping(path = "batch/accept", method = RequestMethod.GET)
    @PreAuthorize("@permissionService.hasPermission('api/psp/trace/batch/accept', 'GET')")
    public Response getPSPStatusAccepts()
    {
        return serviceTracePSP.findAllAcceptedRequests();
    }

    @RequestMapping(path = "batch/accept", method = RequestMethod.PUT)
    public Response putPSPStatusAccept(@RequestBody List<PSPStatusBatch> pspStatusBatches)
    {
        return serviceTracePSP.statusAcceptedRequest(pspStatusBatches);
    }

    @RequestMapping(path = "batch/register", method = RequestMethod.GET)
    @PreAuthorize("@permissionService.hasPermission('api/psp/trace/batch/register', 'GET')")
    public Response getPSPTerminalRegisters()
    {
        return serviceTracePSP.findAllRegisterRequests();
    }

    @RequestMapping(path = "batch/register", method = RequestMethod.PUT)
    public Response putPSPTerminalRegister(@RequestBody List<PSPTerminalBatch> pspTerminalBatches)
    {
        return serviceTracePSP.terminalRegisteredRequest(pspTerminalBatches);
    }

    @RequestMapping(path = "case/unknown", method = RequestMethod.PUT)
    @PreAuthorize("@permissionService.hasPermission('api/psp/trace/case/unknown', 'PUT')")
    public Response putCasePSPTerminalRegister(
            @RequestParam(value = "requestNumber", required = true) Long requestNumber)
    {
        return serviceTracePSP.traceRequestByRequestNumber(requestNumber);
    }

    @RequestMapping(path = "case/accept", method = RequestMethod.PUT)
    public Response putCasePSPStatusAccept(
            @RequestParam(value = "pspTrackingCode", required = true) String pspTrackingCode)
    {
        return serviceTracePSP.statusRequestByPSPTrackingCode(pspTrackingCode);
    }

    @RequestMapping(path = "case/register", method = RequestMethod.PUT)
    public Response putCasePSPTerminalRegister(
            @RequestParam(value = "pspTrackingCode", required = true) String pspTrackingCode)
    {
        return serviceTracePSP.terminalRequestByPSPTrackingCode(pspTrackingCode);
    }

}
