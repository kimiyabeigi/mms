package com.karafarin.terminalmanagement.controller.psp;

import com.karafarin.terminalmanagement.model.Messages;
import com.karafarin.terminalmanagement.model.psp.ParamsReportMaster;
import com.karafarin.terminalmanagement.service.impl.psp.ServiceReportRegisterImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping(path = "/api/psp/export")
public class RestExportController
{
    @Autowired
    private ServiceReportRegisterImpl serviceReportRegisterMaster;
    @Autowired
    private Messages messages;

    @RequestMapping(path = "detail", method = RequestMethod.GET)
    public ModelAndView exportDetail(@RequestParam(value = "id", required = false) Long id) {
        Map<String, String> message = new HashMap<>();
        message.put("excel.create.date", messages.get("excel.create.date"));
        message.put("excel.detail.date", messages.get("excel.detail.date"));
        message.put("excel.detail.time", messages.get("excel.detail.time"));
        message.put("excel.detail.branch", messages.get("excel.detail.branch"));
        message.put("excel.detail.user" , messages.get("excel.detail.user"));
        message.put("excel.detail.request.status" , messages.get("excel.detail.request.status"));
        message.put("excel.detail.request.number" , messages.get("excel.detail.request.number"));
        message.put("excel.detail.tracking.code", messages.get("excel.detail.tracking.code"));
        message.put("excel.detail.service", messages.get("excel.detail.service"));
        message.put("excel.detail.status.type", messages.get("excel.detail.status.type"));
        message.put("excel.detail.description", messages.get("excel.detail.description"));

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("details", serviceReportRegisterMaster.detailFindAll(id).getData());
        modelAndView.addObject("message", message);
        return modelAndView;
    }

    @RequestMapping(path = "master", method = RequestMethod.GET)
    public ModelAndView exportMaster(
            @RequestParam(value = "requestDateFrom", required = false) String requestDateFrom,
            @RequestParam(value = "requestDateTo", required = false) String requestDateTo,
            @RequestParam(value = "clientNumber", required = false) String clientNumber,
            @RequestParam(value = "pspTrackingCode", required = false) String pspTrackingCode,
            @RequestParam(value = "accountNo", required = false) String accountNo,
            @RequestParam(value = "pspAcceptanceCode", required = false) String pspAcceptanceCode,
            @RequestParam(value = "companyId", required = false) String companyId,
            @RequestParam(value = "requestNumber", required = false) String requestNumber,
            @RequestParam(value = "pspTerminalCode", required = false) String pspTerminalCode,
            @RequestParam(value = "branchId", required = false) String branchId,
            @RequestParam(value = "requestStatusId", required = false) String requestStatusId) {

        ParamsReportMaster paramsReportMaster = new ParamsReportMaster();
        paramsReportMaster.setRequestDateFrom(requestDateFrom);
        paramsReportMaster.setRequestDateTo(requestDateTo);
        paramsReportMaster.setClientNumber(clientNumber);
        paramsReportMaster.setPspTrackingCode(pspTrackingCode);
        paramsReportMaster.setAccountNo(accountNo);
        paramsReportMaster.setPspAcceptanceCode(pspAcceptanceCode);
        paramsReportMaster.setCompanyId(companyId);
        paramsReportMaster.setRequestNumber(requestNumber);
        paramsReportMaster.setPspTerminalCode(pspTerminalCode);
        paramsReportMaster.setBranchId(branchId);
        paramsReportMaster.setRequestStatusId(requestStatusId);

        Map<String, String> message = new HashMap<>();
        message.put("excel.create.date", messages.get("excel.create.date"));
        message.put("excel.master.date", messages.get("excel.master.date"));
        message.put("excel.master.account.no", messages.get("excel.master.account.no"));
        message.put("excel.master.client.no", messages.get("excel.master.client.no"));
        message.put("excel.master.branch", messages.get("excel.master.branch"));
        message.put("excel.master.company.name", messages.get("excel.master.company.name"));
        message.put("excel.master.request.status", messages.get("excel.master.request.status"));
        message.put("excel.master.request.no", messages.get("excel.master.request.no"));
        message.put("excel.master.psp.tracking.code", messages.get("excel.master.psp.tracking.code"));
        message.put("excel.master.psp.acceptance.code", messages.get("excel.master.psp.acceptance.code"));
        message.put("excel.master.psp.terminal.code", messages.get("excel.master.psp.terminal.code"));

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("masters", serviceReportRegisterMaster.masterFindAll(paramsReportMaster).getData());
        modelAndView.addObject("message", message);
        return modelAndView;
    }
}
