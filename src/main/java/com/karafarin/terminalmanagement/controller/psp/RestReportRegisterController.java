package com.karafarin.terminalmanagement.controller.psp;

import com.karafarin.terminalmanagement.message.Response;
import com.karafarin.terminalmanagement.model.psp.ParamsReportMaster;
import com.karafarin.terminalmanagement.service.impl.psp.ServiceReportRegisterImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/api/psp/register/report")
public class RestReportRegisterController
{
    @Autowired
    private ServiceReportRegisterImpl serviceReportRegisterMaster;

    @RequestMapping(path = "/master", method = RequestMethod.POST)
    @PreAuthorize("@permissionService.hasPermission('/api/psp/register/report/master', 'POST')")
    public Response getMasters(@RequestBody ParamsReportMaster paramsReportMaster) {
        return serviceReportRegisterMaster.masterFindAll(paramsReportMaster);
    }

    @RequestMapping(path = "/detail", method = RequestMethod.GET)
    public Response getDetails(
            @RequestParam(value = "pspRequestId", required = true) Long pspRequestId) {
        return serviceReportRegisterMaster.detailFindAll(pspRequestId);
    }

}
