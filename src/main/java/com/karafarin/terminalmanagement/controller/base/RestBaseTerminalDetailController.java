package com.karafarin.terminalmanagement.controller.base;

import com.karafarin.terminalmanagement.dto.base.BaseTerminalDetail;
import com.karafarin.terminalmanagement.message.Response;
import com.karafarin.terminalmanagement.service.impl.base.ServiceBaseTerminalDetailImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(path = "/api/base/terminalDetail")
public class RestBaseTerminalDetailController {

    @Autowired
    private ServiceBaseTerminalDetailImpl baseTerminalDetailService;

    @RequestMapping(method = RequestMethod.GET)
    @PreAuthorize("@permissionService.hasPermission('/api/base/terminalDetail', 'GET')")
    public Response getBaseTerminalDetails() {
        return baseTerminalDetailService.findAll();
    }

    @RequestMapping(path = "selected/{id}", method = RequestMethod.GET)
    public Response getSelectedTerminalDetails(@PathVariable Long id) {
        return baseTerminalDetailService.findSelectedTerminalDetails(id);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public Response getBaseTerminalDetail(@PathVariable Long id) {
        return baseTerminalDetailService.findById(id);
    }

    @RequestMapping(path = "companyId/{id}", method = RequestMethod.GET)
    public Response getBaseTerminalDetailByCompanyId(@PathVariable Long id) {
        return baseTerminalDetailService.findByCompanyId(id);
    }

    @RequestMapping(method = RequestMethod.PUT)
    public Response putBaseTerminalDetail(@RequestBody @Valid BaseTerminalDetail baseIndex) {
        return baseTerminalDetailService.add(baseIndex);
    }

    @RequestMapping(method = RequestMethod.POST)
    public Response postBaseTerminalDetail(@RequestBody @Valid BaseTerminalDetail baseIndex)
    {
        return baseTerminalDetailService.update(baseIndex);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    public Response delBaseTerminalDetail(@PathVariable Long id){
        return baseTerminalDetailService.delete(id);
    }
}
