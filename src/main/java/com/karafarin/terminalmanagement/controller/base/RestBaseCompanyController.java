package com.karafarin.terminalmanagement.controller.base;

import com.karafarin.terminalmanagement.dto.base.BaseCompany;
import com.karafarin.terminalmanagement.message.Response;
import com.karafarin.terminalmanagement.service.impl.base.ServiceBaseCompanyImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(path = "/api/base/company")
public class RestBaseCompanyController
{
    @Autowired
    private ServiceBaseCompanyImpl baseCompanyService;

    @RequestMapping(method = RequestMethod.GET)
    @PreAuthorize("@permissionService.hasPermission('/api/base/company', 'GET')")
    public Response getBaseCompanies() {
        return baseCompanyService.findAll();
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public Response getBaseCompany(@PathVariable Long id) {
        return baseCompanyService.findById(id);
    }

    @RequestMapping(path = "/search", method = RequestMethod.GET)
    public Response getBaseCompanies(
            @RequestParam(value = "companyCode", required = false) String companyCode,
            @RequestParam(value = "name", required = false) String name) {
        return baseCompanyService.search(companyCode, name);
    }

    @RequestMapping(path = "/active", method = RequestMethod.GET)
    @PreAuthorize("@permissionService.hasPermission('/api/base/company/active', 'GET')")
    public Response getActiveCompaniesForRequest() {
        return baseCompanyService.findActiveCompaniesForRequest();
    }

    @RequestMapping(method = RequestMethod.PUT)
    public Response putBaseCompany(@RequestBody @Valid BaseCompany baseCompany) {
        return baseCompanyService.add(baseCompany);
    }

    @RequestMapping(method = RequestMethod.POST)
    public Response postBaseCompany(@RequestBody @Valid BaseCompany baseCompany)
    {
        return baseCompanyService.update(baseCompany);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    public Response delBaseCompany(@PathVariable Long id){
        return baseCompanyService.delete(id);
    }
}
