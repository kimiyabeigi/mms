package com.karafarin.terminalmanagement.controller.base;

import com.karafarin.terminalmanagement.dto.base.BaseState;
import com.karafarin.terminalmanagement.message.Response;
import com.karafarin.terminalmanagement.service.impl.base.ServiceBaseStateImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(path = "/api/base/state")
public class RestBaseStateController
{
    @Autowired
    private ServiceBaseStateImpl baseStateService;

    @RequestMapping(method = RequestMethod.GET)
    @PreAuthorize("@permissionService.hasPermission('/api/base/state', 'GET')")
    public Response getBaseStates() {
        return baseStateService.findAll(true);
    }

    @RequestMapping(path = "active", method = RequestMethod.GET)
    public Response getActiveBaseStates() {
        return baseStateService.findAll(false);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public Response getBaseState(@PathVariable Long id) {
        return baseStateService.findById(id);
    }

    @RequestMapping(method = RequestMethod.PUT)
    public Response putBaseState(@RequestBody @Valid BaseState baseState) {
        return baseStateService.add(baseState);
    }

    @RequestMapping(method = RequestMethod.POST)
    public Response postBaseState(@RequestBody @Valid BaseState baseState)
    {
        return baseStateService.update(baseState);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    public Response delBaseStaet(@PathVariable Long id){
        return baseStateService.delete(id);
    }
}
