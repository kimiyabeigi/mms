package com.karafarin.terminalmanagement.controller.base;

import com.karafarin.terminalmanagement.dto.base.BaseCity;
import com.karafarin.terminalmanagement.message.Response;
import com.karafarin.terminalmanagement.service.impl.base.ServiceBaseCityImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(path = "/api/base/city")
public class RestBaseCityController
{
    @Autowired
    private ServiceBaseCityImpl baseCityService;

    @RequestMapping(method = RequestMethod.GET)
    @PreAuthorize("@permissionService.hasPermission('/api/base/city', 'GET')")
    public Response getBaseCities() {
        return baseCityService.findAll(true);
    }

    @RequestMapping(path = "active", method = RequestMethod.GET)
    public Response getActiveBaseCities() {
        return baseCityService.findAll(false);
    }

    @RequestMapping(path = "cityId/{id}", method = RequestMethod.GET)
    public Response getBaseCity(@PathVariable Long id) {
        return baseCityService.findById(id);
    }

    @RequestMapping(path = "stateId/{id}", method = RequestMethod.GET)
    public Response getBaseCitiesByStateId(@PathVariable Long id) {
        return baseCityService.findByStateId(id);
    }

    @RequestMapping(method = RequestMethod.PUT)
    public Response putBaseCity(@RequestBody @Valid BaseCity baseCity) {
        return baseCityService.add(baseCity);
    }

    @RequestMapping(method = RequestMethod.POST)
    public Response postBaseCity(@RequestBody @Valid BaseCity baseCity)
    {
        return baseCityService.update(baseCity);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    public Response delBaseCity(@PathVariable Long id){
        return baseCityService.delete(id);
    }
}
