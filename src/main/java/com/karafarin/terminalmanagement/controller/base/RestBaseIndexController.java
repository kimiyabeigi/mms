package com.karafarin.terminalmanagement.controller.base;

import com.karafarin.terminalmanagement.dto.base.BaseIndex;
import com.karafarin.terminalmanagement.message.Response;
import com.karafarin.terminalmanagement.service.impl.base.ServiceBaseIndexImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(path = "/api/base/index")
public class RestBaseIndexController
{
    @Autowired
    private ServiceBaseIndexImpl baseIndexService;

    @RequestMapping(method = RequestMethod.GET)
    @PreAuthorize("@permissionService.hasPermission('/api/base/index', 'GET')")
    public Response getBaseIndexes() {
        return baseIndexService.findAll();
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public Response getBaseIndex(@PathVariable Long id) {
        return baseIndexService.findById(id);
    }

    @RequestMapping(method = RequestMethod.PUT)
    public Response putBaseIndex(@RequestBody @Valid BaseIndex baseIndex) {
        return baseIndexService.add(baseIndex);
    }

    @RequestMapping(method = RequestMethod.POST)
    public Response postBaseIndex(@RequestBody @Valid BaseIndex baseIndex)
    {
        return baseIndexService.update(baseIndex);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    public Response delBaseIndex(@PathVariable Long id){
        return baseIndexService.delete(id);
    }
}
