package com.karafarin.terminalmanagement.controller.base;

import com.karafarin.terminalmanagement.dto.base.BaseGuild;
import com.karafarin.terminalmanagement.message.Response;
import com.karafarin.terminalmanagement.service.impl.base.ServiceBaseGuildImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(path = "/api/base/guild")
public class RestBaseGuildController {

    @Autowired
    private ServiceBaseGuildImpl baseGuildService;

    @RequestMapping(method = RequestMethod.GET)
    @PreAuthorize("@permissionService.hasPermission('/api/base/guild', 'GET')")
    public Response getBaseGuilds() {
        return baseGuildService.findAll(true);
    }

    @RequestMapping(path = "active", method = RequestMethod.GET)
    public Response getActiveBaseGuilds() {
        return baseGuildService.findAll(false);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public Response getBaseGuild(@PathVariable Long id) {
        return baseGuildService.findById(id);
    }

    @RequestMapping(method = RequestMethod.PUT)
    public Response putBaseCity(@RequestBody @Valid BaseGuild baseGuild) {
        return baseGuildService.add(baseGuild);
    }

    @RequestMapping(method = RequestMethod.POST)
    public Response postBaseCity(@RequestBody @Valid BaseGuild baseGuild)
    {
        return baseGuildService.update(baseGuild);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    public Response delBaseCity(@PathVariable Long id){
        return baseGuildService.delete(id);
    }
}
