package com.karafarin.terminalmanagement.controller.sys;

import com.karafarin.terminalmanagement.message.Response;
import com.karafarin.terminalmanagement.service.intf.sys.ServiceSysBranch;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/api/sys/branch")
public class RestSysBranch
{
    @Autowired
    private ServiceSysBranch serviceSysBranch;

    @RequestMapping(method = RequestMethod.GET)
    public Response getBranches() {
        return serviceSysBranch.findAll();
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public Response getBranch(@PathVariable Long id) {
        return serviceSysBranch.findById(id);
    }

}
