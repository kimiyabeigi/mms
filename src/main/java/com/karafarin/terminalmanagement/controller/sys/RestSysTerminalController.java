package com.karafarin.terminalmanagement.controller.sys;

import com.karafarin.terminalmanagement.message.Response;
import com.karafarin.terminalmanagement.message.enums.TerminalEnum;
import com.karafarin.terminalmanagement.service.impl.sys.ServiceSysTerminalImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/api/sys/terminal")
public class RestSysTerminalController
{

    @Autowired
    ServiceSysTerminalImpl sysTerminalService;

    @RequestMapping(method = RequestMethod.GET)
    public Response getBaseTerminals() {
        return sysTerminalService.findAll();
    }

    @RequestMapping(path = "filter", method = RequestMethod.GET)
    public Response getBaseTerminalsByFilter() {
        return sysTerminalService.findTerminalsByFilter(TerminalEnum.IPG.getValue());
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public Response getBaseTerminal(@PathVariable Long id) {
        return sysTerminalService.findById(id);
    }

    @RequestMapping(path = "request/{id}", method = RequestMethod.GET)
    public Response getTerminalsRequest(@PathVariable Long id) {
        return sysTerminalService.findTerminalsRequest(id);
    }
}
