package com.karafarin.terminalmanagement.controller.sys;

import com.karafarin.terminalmanagement.message.Response;
import com.karafarin.terminalmanagement.service.impl.sys.ServiceSysStatusTypeImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/api/sys/statusType")
public class RestSysStatusTypeController
{
    @Autowired
    private ServiceSysStatusTypeImpl sysStatusTypeService;

    @RequestMapping(method = RequestMethod.GET)
    public Response getBaseCompanies() {
        return sysStatusTypeService.findAll();
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public Response getBaseCompany(@PathVariable Long id) {
        return sysStatusTypeService.findById(id);
    }
}
