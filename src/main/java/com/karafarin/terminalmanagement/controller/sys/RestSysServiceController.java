package com.karafarin.terminalmanagement.controller.sys;

import com.karafarin.terminalmanagement.message.Response;
import com.karafarin.terminalmanagement.service.impl.sys.ServiceSysServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/api/sys/service")
public class RestSysServiceController
{
    @Autowired
    private ServiceSysServiceImpl sysServiceService;

    @RequestMapping(method = RequestMethod.GET)
    @PreAuthorize("@permissionService.hasPermission('/api/sys/service', 'GET')")
    public Response getServices() {
        return sysServiceService.findAll();
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public Response getService(@PathVariable Long id) {
        return sysServiceService.findById(id);
    }


}
