package com.karafarin.terminalmanagement.controller.core;

import com.karafarin.terminalmanagement.message.Response;
import com.karafarin.terminalmanagement.service.impl.core.ServiceCoreImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/api/core")
public class RestCoreController
{
    @Autowired
    private ServiceCoreImpl serviceCore;

    @RequestMapping(path = "/client/info", method = RequestMethod.GET)
    public Response getClientInfo(
            @RequestParam(value = "clientNumber", required = true) String clientNumber) {
        return serviceCore.getClientInfo(clientNumber);
    }

    @RequestMapping(path = "/account/info", method = RequestMethod.GET)
    public Response getAccountInfo(
            @RequestParam(value = "clientNumber", required = true) String clientNumber,
            @RequestParam(value = "clientType", required = true) String clientType) {
        return serviceCore.getAccountInfo(clientNumber, clientType);
    }

    @RequestMapping(path = "/accountOwner/info", method = RequestMethod.GET)
    public Response getAccountOwnersInfo(
            @RequestParam(value = "accountNumber", required = true) String accountNumber) {
        return serviceCore.getAccountOwnersInfo(accountNumber);
    }
}
