package com.karafarin.terminalmanagement.message.enums;

public enum ResponseOwnerEnum
{
    SYSTEM,
    CORE
}
