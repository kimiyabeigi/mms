package com.karafarin.terminalmanagement.message.enums;

public enum TerminalEnum
{
    POS(1L),
    IPG(2L),
    MOB(3L);

    private final Long id;

    TerminalEnum(Long id)
    {
        this.id = id;
    }

    public Long getValue()
    {
        return id;
    }
}
