package com.karafarin.terminalmanagement.message.enums;

public enum PositionEnum
{
    CEO(1L),
    OTHER_MEMBER(2L);

    private final Long id;

    PositionEnum(Long id)
    {
        this.id = id;
    }

    public Long getValue()
    {
        return id;
    }
}
