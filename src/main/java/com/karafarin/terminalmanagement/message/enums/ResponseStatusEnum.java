package com.karafarin.terminalmanagement.message.enums;

public enum ResponseStatusEnum
{
    SUCCESS,
    ERROR,
    WARNING,
    INFO
}
