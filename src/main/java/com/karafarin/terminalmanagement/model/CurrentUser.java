package com.karafarin.terminalmanagement.model;

import com.karafarin.terminalmanagement.dto.auth.User;

public class CurrentUser extends User
{
    private Long branchId;
    private String ip;
    private String computerName;
    private String roleName;
    private String roleNameEN;

    public String getRoleNameEN()
    {
        return roleNameEN;
    }

    public void setRoleNameEN(String roleNameEN)
    {
        this.roleNameEN = roleNameEN;
    }

    public String getRoleName()
    {
        return roleName;
    }

    public void setRoleName(String roleName)
    {
        this.roleName = roleName;
    }

    public String getComputerName()
    {
        return computerName;
    }

    public void setComputerName(String computerName)
    {
        this.computerName = computerName;
    }

    public String getIp()
    {
        return ip;
    }

    public void setIp(String ip)
    {
        this.ip = ip;
    }

    public Long getBranchId()
    {
        return branchId;
    }

    public void setBranchId(Long branchId)
    {
        this.branchId = branchId;
    }
}
