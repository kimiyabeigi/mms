package com.karafarin.terminalmanagement.model.core;


import org.w3c.dom.Node;

public class ChannelResponse
{
    private String cmd;
    private String jobId;
    private String msgId;
    private Integer errorNo;
    private String result;
    private String desc;
    private String fDesc;
    private String errorMessage;
    private Node content;

    public String getErrorMessage()
    {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage)
    {
        this.errorMessage = errorMessage;
    }

    public Node getContent()
    {
        return content;
    }

    public void setContent(Node content)
    {
        this.content = content;
    }

    public String getCmd()
    {
        return cmd;
    }

    public void setCmd(String cmd)
    {
        this.cmd = cmd;
    }

    public String getJobId()
    {
        return jobId;
    }

    public void setJobId(String jobId)
    {
        this.jobId = jobId;
    }

    public String getMsgId()
    {
        return msgId;
    }

    public void setMsgId(String msgId)
    {
        this.msgId = msgId;
    }

    public Integer getErrorNo()
    {
        return errorNo;
    }

    public void setErrorNo(Integer errorNo)
    {
        this.errorNo = errorNo;
    }

    public String getResult()
    {
        return result;
    }

    public void setResult(String result)
    {
        this.result = result;
    }

    public String getDesc()
    {
        return desc;
    }

    public void setDesc(String desc)
    {
        this.desc = desc;
    }

    public String getfDesc()
    {
        return fDesc;
    }

    public void setfDesc(String fDesc)
    {
        this.fDesc = fDesc;
    }

}
