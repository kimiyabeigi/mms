package com.karafarin.terminalmanagement.model;

import com.karafarin.terminalmanagement.message.enums.ResponseOwnerEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.sql.SQLException;
import java.util.Locale;

@Component
public class Messages
{
    @Autowired
    private MessageSource messageSource;

    private MessageSourceAccessor accessor;

    @PostConstruct
    private void init() {
        accessor = new MessageSourceAccessor(messageSource, new Locale("fa","IR"));
    }

    public String get(String code)
    {
        return accessor.getMessage(code);
    }

    public String getExceptionDescription(Exception e, ResponseOwnerEnum className){
        String result = null;

        if (e instanceof DuplicateKeyException)
        {
            result = get("baseEntity.duplicate.error");
        } else if (e instanceof DataIntegrityViolationException)
        {
            result = get("psp.error.1");
        } else if (e instanceof SQLException ||
                   e instanceof DataAccessException)
        {
            result = get("system.db.error");
        } else
        {
            if (className.equals(ResponseOwnerEnum.SYSTEM))
                result = get("system.error");
            else if (className.equals(ResponseOwnerEnum.CORE))
                result = get("core.error");
        }

        return result;
    };
}
