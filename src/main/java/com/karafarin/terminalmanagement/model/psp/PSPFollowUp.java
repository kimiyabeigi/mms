package com.karafarin.terminalmanagement.model.psp;

public class PSPFollowUp
{
    private Long pspRequestId;
    private Long pspRequestStatusId;
    private Long requestStatusId;
    private String createDate;
    private String accountNo;
    private String clientNumber;
    private String branchName;
    private Long requestNumber;
    private String pspTrackingCode;
    private String Status;
    private String companyCode;
    private boolean selected;

    public Long getRequestStatusId()
    {
        return requestStatusId;
    }

    public void setRequestStatusId(Long requestStatusId)
    {
        this.requestStatusId = requestStatusId;
    }

    public Long getPspRequestId()
    {
        return pspRequestId;
    }

    public void setPspRequestId(Long pspRequestId)
    {
        this.pspRequestId = pspRequestId;
    }

    public Long getPspRequestStatusId()
    {
        return pspRequestStatusId;
    }

    public void setPspRequestStatusId(Long pspRequestStatusId)
    {
        this.pspRequestStatusId = pspRequestStatusId;
    }

    public String getCreateDate()
    {
        return createDate;
    }

    public void setCreateDate(String createDate)
    {
        this.createDate = createDate;
    }

    public String getAccountNo()
    {
        return accountNo;
    }

    public void setAccountNo(String accountNo)
    {
        this.accountNo = accountNo;
    }

    public String getClientNumber()
    {
        return clientNumber;
    }

    public void setClientNumber(String clientNumber)
    {
        this.clientNumber = clientNumber;
    }

    public String getBranchName()
    {
        return branchName;
    }

    public void setBranchName(String branchName)
    {
        this.branchName = branchName;
    }

    public Long getRequestNumber()
    {
        return requestNumber;
    }

    public void setRequestNumber(Long requestNumber)
    {
        this.requestNumber = requestNumber;
    }

    public String getPspTrackingCode()
    {
        return pspTrackingCode;
    }

    public void setPspTrackingCode(String pspTrackingCode)
    {
        this.pspTrackingCode = pspTrackingCode;
    }

    public String getStatus()
    {
        return Status;
    }

    public void setStatus(String status)
    {
        Status = status;
    }

    public String getCompanyCode()
    {
        return companyCode;
    }

    public void setCompanyCode(String companyCode)
    {
        this.companyCode = companyCode;
    }

    public boolean isSelected()
    {
        return selected;
    }

    public void setSelected(boolean selected)
    {
        this.selected = selected;
    }
}
