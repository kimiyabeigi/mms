package com.karafarin.terminalmanagement.model.psp;

import java.util.Date;

public class ParamsReportMaster
{
    private String requestDateFrom;
	private String requestDateTo;
	private String clientNumber;
	private String pspTrackingCode;
	private String accountNo;
	private String pspAcceptanceCode;
	private String companyId;
	private String requestNumber;
	private String pspTerminalCode;
	private String branchId;
	private String requestStatusId;

    public String getRequestDateFrom() {
        return requestDateFrom;
    }

    public void setRequestDateFrom(String requestDateFrom) {
        this.requestDateFrom = requestDateFrom;
    }

    public String getRequestDateTo() {
        return requestDateTo;
    }

    public void setRequestDateTo(String requestDateTo) {
        this.requestDateTo = requestDateTo;
    }

    public String getClientNumber() {
        return clientNumber;
    }

    public void setClientNumber(String clientNumber) {
        this.clientNumber = clientNumber;
    }

    public String getPspTrackingCode() {
        return pspTrackingCode;
    }

    public void setPspTrackingCode(String pspTrackingCode) {
        this.pspTrackingCode = pspTrackingCode;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getPspAcceptanceCode() {
        return pspAcceptanceCode;
    }

    public void setPspAcceptanceCode(String pspAcceptanceCode) {
        this.pspAcceptanceCode = pspAcceptanceCode;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getRequestNumber() {
        return requestNumber;
    }

    public void setRequestNumber(String requestNumber) {
        this.requestNumber = requestNumber;
    }

    public String getPspTerminalCode() {
        return pspTerminalCode;
    }

    public void setPspTerminalCode(String pspTerminalCode) {
        this.pspTerminalCode = pspTerminalCode;
    }

    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    public String getRequestStatusId() {
        return requestStatusId;
    }

    public void setRequestStatusId(String requestStatusId) {
        this.requestStatusId = requestStatusId;
    }
}
