package com.karafarin.terminalmanagement.model.psp;

import com.karafarin.terminalmanagement.dto.psp.*;

import java.util.List;

public class RegisterRequest
{
    private Long pspRequestId;
    private Long companyId;
    private Long terminalId;
    private String pspAcceptanceCode;
    private String pspTerminalCode;
    private String pspTrackingCode;
    private String ipAddress;
    private String computerName;
    private String requestDate;
    private Long userId;
    private Long branchId;
    private Long requestNumber;
    private List<PSPClient> pspClients;
    private PSPAccount pspAccount;
    private List<PSPAccountOwner> pspAccountOwners;
    private PSPShop pspShop;
    private PSPIPG pspipg;
    private Long pspRequestStatusId;

    public Long getRequestNumber()
    {
        return requestNumber;
    }

    public void setRequestNumber(Long requestNumber)
    {
        this.requestNumber = requestNumber;
    }

    public Long getPspRequestStatusId()
    {
        return pspRequestStatusId;
    }

    public void setPspRequestStatusId(Long pspRequestStatusId)
    {
        this.pspRequestStatusId = pspRequestStatusId;
    }

    public Long getPspRequestId()
    {
        return pspRequestId;
    }

    public void setPspRequestId(Long pspRequestId)
    {
        this.pspRequestId = pspRequestId;
    }

    public Long getCompanyId()
    {
        return companyId;
    }

    public void setCompanyId(Long companyId)
    {
        this.companyId = companyId;
    }

    public Long getTerminalId()
    {
        return terminalId;
    }

    public void setTerminalId(Long terminalId)
    {
        this.terminalId = terminalId;
    }

    public String getPspAcceptanceCode()
    {
        return pspAcceptanceCode;
    }

    public void setPspAcceptanceCode(String pspAcceptanceCode)
    {
        this.pspAcceptanceCode = pspAcceptanceCode;
    }

    public String getPspTerminalCode()
    {
        return pspTerminalCode;
    }

    public void setPspTerminalCode(String pspTerminalCode)
    {
        this.pspTerminalCode = pspTerminalCode;
    }

    public String getPspTrackingCode()
    {
        return pspTrackingCode;
    }

    public void setPspTrackingCode(String pspTrackingCode)
    {
        this.pspTrackingCode = pspTrackingCode;
    }

    public String getIpAddress()
    {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress)
    {
        this.ipAddress = ipAddress;
    }

    public String getComputerName()
    {
        return computerName;
    }

    public void setComputerName(String computerName)
    {
        this.computerName = computerName;
    }

    public String getRequestDate()
    {
        return requestDate;
    }

    public void setRequestDate(String requestDate)
    {
        this.requestDate = requestDate;
    }

    public Long getUserId()
    {
        return userId;
    }

    public void setUserId(Long userId)
    {
        this.userId = userId;
    }

    public Long getBranchId()
    {
        return branchId;
    }

    public void setBranchId(Long branchId)
    {
        this.branchId = branchId;
    }

    public List<PSPClient> getPspClients()
    {
        return pspClients;
    }

    public void setPspClients(List<PSPClient> pspClients)
    {
        this.pspClients = pspClients;
    }

    public PSPAccount getPspAccount()
    {
        return pspAccount;
    }

    public void setPspAccount(PSPAccount pspAccount)
    {
        this.pspAccount = pspAccount;
    }

    public List<PSPAccountOwner> getPspAccountOwners()
    {
        return pspAccountOwners;
    }

    public void setPspAccountOwners(List<PSPAccountOwner> pspAccountOwners)
    {
        this.pspAccountOwners = pspAccountOwners;
    }

    public PSPShop getPspShop()
    {
        return pspShop;
    }

    public void setPspShop(PSPShop pspShop)
    {
        this.pspShop = pspShop;
    }

    public PSPIPG getPspipg()
    {
        return pspipg;
    }

    public void setPspipg(PSPIPG pspipg)
    {
        this.pspipg = pspipg;
    }
}
