package com.karafarin.terminalmanagement.model.psp;

public class WrapperResponse
{
    private Object data;
    private String status;
    private String statusEn;
    private String errorDesc;
    private String pspTrackingCode;
    private String PSPAcceptanceCode;
    private String PSPTerminalCode;
    private Long requestNumber;

    public String getPSPAcceptanceCode() {
        return PSPAcceptanceCode;
    }

    public void setPSPAcceptanceCode(String PSPAcceptanceCode) {
        this.PSPAcceptanceCode = PSPAcceptanceCode;
    }

    public String getPSPTerminalCode() {
        return PSPTerminalCode;
    }

    public void setPSPTerminalCode(String PSPTerminalCode) {
        this.PSPTerminalCode = PSPTerminalCode;
    }

    public String getStatusEn()
    {
        return statusEn;
    }

    public void setStatusEn(String statusEn)
    {
        this.statusEn = statusEn;
    }

    public String getPspTrackingCode()
    {
        return pspTrackingCode;
    }

    public void setPspTrackingCode(String pspTrackingCode)
    {
        this.pspTrackingCode = pspTrackingCode;
    }

    public Long getRequestNumber()
    {
        return requestNumber;
    }

    public void setRequestNumber(Long requestNumber)
    {
        this.requestNumber = requestNumber;
    }

    public Object getData()
    {
        return data;
    }

    public void setData(Object data)
    {
        this.data = data;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getErrorDesc()
    {
        return errorDesc;
    }

    public void setErrorDesc(String errorDesc)
    {
        this.errorDesc = errorDesc;
    }
}
