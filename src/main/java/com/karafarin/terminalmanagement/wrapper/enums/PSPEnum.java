package com.karafarin.terminalmanagement.wrapper.enums;

public enum PSPEnum
{
    IRAN_KISH("IK"),
    TEJARAT_PARSIAN("TP");

    private final String code;

    PSPEnum(String code)
    {
        this.code = code;
    }

    public String getValue()
    {
        return code;
    }

}
