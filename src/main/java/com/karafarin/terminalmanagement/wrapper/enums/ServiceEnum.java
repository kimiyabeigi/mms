package com.karafarin.terminalmanagement.wrapper.enums;

public enum ServiceEnum
{
    REGISTER(1L),
    TRACE(2L),
    STATUS(3L),
    TERMINAL(4L);

    private final Long id;

    ServiceEnum(Long id)
    {
        this.id = id;
    }

    public Long getValue()
    {
        return id;
    }

}
