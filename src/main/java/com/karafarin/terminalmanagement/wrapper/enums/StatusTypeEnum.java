package com.karafarin.terminalmanagement.wrapper.enums;

public enum StatusTypeEnum
{
    SUCCESS(1L),
    FAIL(2L),
    UNKNOWN(3L),
    EXCEPTION(4L),
    CHANNEL_ERROR(5L);

    private final Long id;

    StatusTypeEnum(Long id)
    {
        this.id = id;
    }

    public Long getValue()
    {
        return id;
    }
}
