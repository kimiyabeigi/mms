package com.karafarin.terminalmanagement.wrapper.service.impl;

import com.karafarin.terminalmanagement.dao.impl.psp.PSPRequestDaoImpl;
import com.karafarin.terminalmanagement.dao.impl.psp.PSPRequestStatusDaoImpl;
import com.karafarin.terminalmanagement.dao.impl.setting.CompanySettingDaoImpl;
import com.karafarin.terminalmanagement.dto.psp.PSPRequest;
import com.karafarin.terminalmanagement.dto.setting.CompanySetting;
import com.karafarin.terminalmanagement.model.Messages;
import com.karafarin.terminalmanagement.model.core.ChannelRequest;
import com.karafarin.terminalmanagement.model.core.ChannelResponse;
import com.karafarin.terminalmanagement.util.ChannelUtil;
import com.karafarin.terminalmanagement.util.UserUtil;
import com.karafarin.terminalmanagement.util.date.DateConverterUtil;
import com.karafarin.terminalmanagement.wrapper.enums.PSPEnum;
import com.karafarin.terminalmanagement.wrapper.enums.StatusTypeEnum;
import com.karafarin.terminalmanagement.wrapper.model.req.PosTraceRequest;
import com.karafarin.terminalmanagement.wrapper.model.resp.PosTraceResponse;
import com.karafarin.terminalmanagement.wrapper.service.intf.ServicePosTraceReq;
import com.karafarin.terminalmanagement.wrapper.util.WrapperUtil;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.w3c.dom.Node;

import java.sql.SQLException;
import java.util.Date;

@Service
public class ServicePosTraceReqImpl implements ServicePosTraceReq
{

    private static final Logger logger = LogManager.getLogger(ServicePosTraceReqImpl.class);

    @Autowired
    private ChannelUtil channelUtil;
    @Autowired
    private Messages messages;
    @Autowired
    private CompanySettingDaoImpl companySettingDao;
    @Autowired
    private PSPRequestDaoImpl pspRequestDao;

    @Override
    public PosTraceResponse posTraceReq(String companyCode, PosTraceRequest posTraceRequest) throws SQLException
    {
        try
        {
            logger.info("Start of method [posTraceReq] for companyCode [" + posTraceRequest.getCompanyCode() + "]");

            PosTraceResponse result = new PosTraceResponse();

            ChannelRequest channelRequest = new ChannelRequest();

            channelRequest.setCmd(companyCode + "posTraceReq");
            channelRequest.setAppName("RYMMS");
            channelRequest.setUserId(String.valueOf(UserUtil.getCurrentUser().getUserId()));
            channelRequest.setMsgId("mms_msg_cgi" + UserUtil.getCurrentUser().getUserId() + "_" +
                                    DateConverterUtil.getAsStringWithTime(new Date(), "fa")
                                                     .replaceAll("[^a-zA-Z0-9]", ""));
            channelRequest.setJobId("GCIJOB");
            channelRequest.setBranchCode(String.valueOf(UserUtil.getCurrentUser().getBranchId()));
            channelRequest.setContent(makeContentByCompanyCode(posTraceRequest));

            ChannelResponse channelResponse = channelUtil.sendRequest(channelRequest);

            if (channelResponse.getErrorNo() == 0)
            {
                result = makePosTraceResponse(posTraceRequest.getCompanyCode(), channelResponse.getContent());
            }
            else if (channelResponse.getErrorNo() < 0)
            {
                result.setStatusTypeId(StatusTypeEnum.CHANNEL_ERROR.getValue());
                result.setErrorCode(String.valueOf(channelResponse.getErrorNo()));
                result.setErrorDesc(channelResponse.getErrorMessage());
                result.setTrackingCode(null);
            }
            else
            {
                result.setStatusTypeId(StatusTypeEnum.EXCEPTION.getValue());
                result.setErrorCode(String.valueOf(channelResponse.getErrorNo()));
                result.setErrorDesc(channelResponse.getErrorMessage());
                result.setTrackingCode(null);
            }

            logger.info("End of method [posTraceReq] for companyCode [" + posTraceRequest.getCompanyCode() + "]");

            return result;
        } catch (Exception e)
        {
            logger.error("There is an error in method [posTraceReq] ", e);
        }
        return null;
    }

    private CompanySetting findCompanySettingByCompanyId(Long id) throws SQLException
    {
        CompanySetting result = new CompanySetting();

        logger.info("Start method [findCompanySettingByCompanyId] via companyId [" + id + "]");

        result = companySettingDao.findByCompanyId(id);

        logger.info("End successfully method [findCompanySettingByCompanyId] via companyId [" + id + "]");

        return result;
    }

    private PSPRequest findPSPRequestByRequestNumber(Long requestNumber) throws SQLException
    {
        PSPRequest result = new PSPRequest();

        logger.info("Start method [findPSPRequestByRequestNumber] via requestNumber [" + requestNumber + "]");

        result = pspRequestDao.findByRequestNumber(requestNumber);

        logger.info("End successfully method [findPSPRequestByRequestNumber] via requestNumber [" + requestNumber + "]");

        return result;
    }

    private String makeContentByCompanyCode(PosTraceRequest posTraceRequest) throws SQLException
    {
        StringBuilder result = new StringBuilder();

        if (posTraceRequest.getCompanyCode().equalsIgnoreCase(PSPEnum.IRAN_KISH.getValue()))
        {
            result.append("<AcceptorNo></AcceptorNo>")
                  .append("<ClientTrackingCode>").append(posTraceRequest.getRequestNumber())
                  .append("</ClientTrackingCode>")
                  .append("<TrackingCode>").append(posTraceRequest.getTrackingCode())
                  .append("</TrackingCode>");
        } else if (posTraceRequest.getCompanyCode().equalsIgnoreCase(PSPEnum.TEJARAT_PARSIAN.getValue()))
        {
            PSPRequest pspRequest = findPSPRequestByRequestNumber(posTraceRequest.getRequestNumber());
            CompanySetting companySetting = findCompanySettingByCompanyId(pspRequest.getCompanyId());

            result.append("<requestcode>").append(posTraceRequest.getRequestNumber()).append("</requestcode>")
                  .append("<organizationId>").append(companySetting.getOrganizationCode()).append("</organizationId>");
        }

        return result.toString();
    }

    private PosTraceResponse makePosTraceResponse(String companyCode, Node content)
    {
        logger.info("Start of method [makePosTraceResponse] for companyCode [" + companyCode + "]");

        PosTraceResponse result = new PosTraceResponse();
        if (companyCode.equalsIgnoreCase(PSPEnum.IRAN_KISH.getValue()))
        {
            result = makeResponseFromIK(content);

        } else if (companyCode.equalsIgnoreCase(PSPEnum.TEJARAT_PARSIAN.getValue()))
        {
            result = makeResponseFromTP(content);
        }

        logger.info("End of method [makePosTraceResponse] for companyCode [" + companyCode + "]");

        return result;
    }

    private PosTraceResponse makeResponseFromTP(Node content)
    {
        PosTraceResponse result = new PosTraceResponse();
        logger.info("Start of method [makeResponseFromTP]");

        String channelState = null;
        String actionState = null;
        String description = null;

        for (int i = 0; i < content.getChildNodes().getLength(); i++)
        {
            String nodeValue = content.getChildNodes().item(i).getTextContent();
            switch (content.getChildNodes().item(i).getNodeName())
            {
                case "PosTrackIds":
                    result.setTrackingCode(nodeValue);
                    break;
                case "ChannelState":
                    channelState = nodeValue;
                    break;
                case "ActionState":
                    actionState = nodeValue;
                    break;
                case "Description":
                    description = nodeValue;
                    break;
            }
        }

        if (channelState.equalsIgnoreCase("0"))
        {
            result.setStatusTypeId(StatusTypeEnum.EXCEPTION.getValue());
            result.setErrorCode(actionState);
            result.setErrorDesc(messages.get("psp.request.exception"));
        } else if (channelState.equalsIgnoreCase("1"))
        {
            if (actionState.equalsIgnoreCase("0"))
            {
                result.setStatusTypeId(StatusTypeEnum.SUCCESS.getValue());
                result.setErrorCode("0");
                result.setErrorDesc(messages.get("psp.request.success"));
            } else
            {
                result.setStatusTypeId(StatusTypeEnum.FAIL.getValue());
                result.setErrorCode(actionState);
                result.setErrorDesc(description);
            }
        }

        logger.info("End of method [makeResponseFromTP]");

        return result;
    }

    private PosTraceResponse makeResponseFromIK(Node content)
    {
        logger.info("Start of method [makeResponseFromIK]");
        PosTraceResponse result = new PosTraceResponse();

        String value = null;
        String exceptionCode = null;
        String exception = null;

        for (int i = 0; i < content.getChildNodes().getLength(); i++)
        {
            String nodeValue = content.getChildNodes().item(i).getTextContent();
            switch (content.getChildNodes().item(i).getNodeName())
            {
                case "Value":
                    value = nodeValue;
                    break;
                case "ExceptionDesc":
                    exception = nodeValue;
                    break;
                case "ExceptionCode":
                    exceptionCode = nodeValue;
                    break;
                case "TrackingCode":
                    result.setTrackingCode(nodeValue);
                    break;
                case "ClientTrackingCode":
                    break;
                case "Status":
                    break;
                case "TerminalCode":
                    break;
                case "AcceptorNo":
                    break;
            }
        }

        if (value.equalsIgnoreCase("true") && exceptionCode.equalsIgnoreCase("0"))
        {
            result.setStatusTypeId(StatusTypeEnum.SUCCESS.getValue());
            result.setErrorCode("0");
            result.setErrorDesc(messages.get("psp.request.success"));
        } else if (value.equalsIgnoreCase("false") || value == null)
        {
            if (exceptionCode.equalsIgnoreCase("-1"))
            {
                result.setStatusTypeId(StatusTypeEnum.EXCEPTION.getValue());
                result.setErrorCode(exceptionCode);
                result.setErrorDesc(messages.get("psp.request.exception"));
            } else if (exceptionCode.equalsIgnoreCase("0"))
            {
                result.setStatusTypeId(StatusTypeEnum.FAIL.getValue());
                result.setErrorCode(exceptionCode);
                result.setErrorDesc(exception);
            }
        }

        logger.info("End of method [makeResponseFromIK]");

        return result;
    }
}
