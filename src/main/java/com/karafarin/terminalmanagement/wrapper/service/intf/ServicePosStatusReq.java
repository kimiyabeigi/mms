package com.karafarin.terminalmanagement.wrapper.service.intf;

import com.karafarin.terminalmanagement.wrapper.model.req.PosStatusRequest;
import com.karafarin.terminalmanagement.wrapper.model.resp.PosStatusResponse;

public interface ServicePosStatusReq
{
    PosStatusResponse posStatusReq(String companyCode, PosStatusRequest posStatusRequest);
}
