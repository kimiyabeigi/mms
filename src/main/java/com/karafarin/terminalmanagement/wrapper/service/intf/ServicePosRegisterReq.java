package com.karafarin.terminalmanagement.wrapper.service.intf;

import com.karafarin.terminalmanagement.wrapper.model.req.PosRegisterRequest;
import com.karafarin.terminalmanagement.wrapper.model.resp.PosRegisterResponse;

public interface ServicePosRegisterReq
{
    PosRegisterResponse posRegisterReq(String companyCode, PosRegisterRequest posRegisterRequest);
}
