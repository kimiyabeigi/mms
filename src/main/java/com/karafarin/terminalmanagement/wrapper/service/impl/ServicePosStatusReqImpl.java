package com.karafarin.terminalmanagement.wrapper.service.impl;

import com.karafarin.terminalmanagement.dao.impl.setting.CompanySettingStatusDaoImpl;
import com.karafarin.terminalmanagement.model.Messages;
import com.karafarin.terminalmanagement.model.core.ChannelRequest;
import com.karafarin.terminalmanagement.model.core.ChannelResponse;
import com.karafarin.terminalmanagement.util.ChannelUtil;
import com.karafarin.terminalmanagement.util.UserUtil;
import com.karafarin.terminalmanagement.util.date.DateConverterUtil;
import com.karafarin.terminalmanagement.wrapper.enums.PSPEnum;
import com.karafarin.terminalmanagement.wrapper.enums.ServiceEnum;
import com.karafarin.terminalmanagement.wrapper.enums.StatusTypeEnum;
import com.karafarin.terminalmanagement.wrapper.model.req.PosStatusRequest;
import com.karafarin.terminalmanagement.wrapper.model.resp.PosStatusResponse;
import com.karafarin.terminalmanagement.wrapper.service.intf.ServicePosStatusReq;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.w3c.dom.Node;

import java.sql.SQLException;
import java.util.Date;

@Service
public class ServicePosStatusReqImpl implements ServicePosStatusReq
{
    private static final Logger logger = LogManager.getLogger(ServicePosStatusReqImpl.class);

    @Autowired
    private ChannelUtil channelUtil;
    @Autowired
    private Messages messages;
    @Autowired
    private CompanySettingStatusDaoImpl companySettingStatusDao;

    @Override
    public PosStatusResponse posStatusReq(String companyCode, PosStatusRequest posStatusRequest)
    {
        try
        {
            logger.info("Start of method [posStatusReq] for companyCode [" + posStatusRequest.getCompanyCode() + "]");

            PosStatusResponse result = new PosStatusResponse();

            ChannelRequest channelRequest = new ChannelRequest();

            channelRequest.setCmd(companyCode + "posStatusReq");
            channelRequest.setAppName("RYMMS");
            channelRequest.setUserId(String.valueOf(UserUtil.getCurrentUser().getUserId()));
            channelRequest.setMsgId("mms_msg_cgi" + UserUtil.getCurrentUser().getUserId() + "_" +
                                    DateConverterUtil.getAsStringWithTime(new Date(), "fa")
                                                     .replaceAll("[^a-zA-Z0-9]", ""));
            channelRequest.setJobId("GCIJOB");
            channelRequest.setBranchCode(String.valueOf(UserUtil.getCurrentUser().getBranchId()));
            channelRequest.setContent(makeContentByCompanyCode(posStatusRequest));

            ChannelResponse channelResponse = channelUtil.sendRequest(channelRequest);

            if (channelResponse.getErrorNo() == 0)
            {
                result = makePosTraceResponse(posStatusRequest.getCompanyCode(), channelResponse.getContent());
            }
            else if (channelResponse.getErrorNo() < 0)
            {
                result.setStatusTypeId(StatusTypeEnum.CHANNEL_ERROR.getValue());
                result.setErrorCode(String.valueOf(channelResponse.getErrorNo()));
                result.setErrorDesc(channelResponse.getErrorMessage());
            }
            else
            {
                result.setStatusTypeId(StatusTypeEnum.EXCEPTION.getValue());
                result.setErrorCode(String.valueOf(channelResponse.getErrorNo()));
                result.setErrorDesc(channelResponse.getErrorMessage());
            }

            logger.info("End of method [posStatusReq] for companyCode [" + posStatusRequest.getCompanyCode() + "]");

            return result;
        } catch (Exception e)
        {
            logger.error("There is an error in method [posStatusReq] ", e);
        }
        return null;
    }

    private PosStatusResponse makePosTraceResponse(String companyCode, Node content) throws SQLException
    {
        logger.info("Start of method [makePosTraceResponse] for companyCode [" + companyCode + "]");

        PosStatusResponse result = new PosStatusResponse();
        if (companyCode.equalsIgnoreCase(PSPEnum.IRAN_KISH.getValue()))
        {
            result = makeResponseFromIK(content);

        } else if (companyCode.equalsIgnoreCase(PSPEnum.TEJARAT_PARSIAN.getValue()))
        {
            result = makeResponseFromTP(content);
        }

        logger.info("End of method [makePosTraceResponse] for companyCode [" + companyCode + "]");

        return result;
    }

    private PosStatusResponse makeResponseFromTP(Node content) throws SQLException
    {
        PosStatusResponse result = new PosStatusResponse();
        logger.info("Start of method [makeResponseFromTP]");

        String checkPosStatusResult = null;
        for (int i = 0; i < content.getChildNodes().getLength(); i++)
        {
            String nodeValue = content.getChildNodes().item(i).getTextContent();
            switch (content.getChildNodes().item(i).getNodeName())
            {
                case "PosStatus":
                    checkPosStatusResult = nodeValue;
                    break;
            }
        }

        Long statusId = companySettingStatusDao
                .findStatusId(checkPosStatusResult, ServiceEnum.STATUS.getValue(), PSPEnum.IRAN_KISH.getValue());
        result.setStatusTypeId(statusId);
        result.setStatus(messages.get("psp.tp.status." + checkPosStatusResult));
        result.setErrorCode(String.valueOf(result.getStatusTypeId()));
        result.setErrorDesc(result.getStatus());

        logger.info("End of method [makeResponseFromTP]");
        return result;
    }

    private PosStatusResponse makeResponseFromIK(Node content) throws SQLException
    {
        logger.info("Start of method [makeResponseFromIK]");
        PosStatusResponse result = new PosStatusResponse();

        String value = null;
        String exceptionCode = null;
        String exception = null;
        String status = null;

        for (int i = 0; i < content.getChildNodes().getLength(); i++)
        {
            String nodeValue = content.getChildNodes().item(i).getTextContent();
            switch (content.getChildNodes().item(i).getNodeName())
            {
                case "Value":
                    value = nodeValue;
                    break;
                case "ExceptionDesc":
                    exception = nodeValue;
                    break;
                case "ExceptionCode":
                    exceptionCode = nodeValue;
                    break;
                case "TrackingCode":
                    break;
                case "ClientTrackingCode":
                    break;
                case "Status":
                    status = nodeValue;
                    break;
                case "TerminalCode":
                    break;
                case "AcceptorNo":
                    break;
            }
        }

        if (value.equalsIgnoreCase("true") && exceptionCode.equalsIgnoreCase("0"))
        {
            Long statusId = companySettingStatusDao
                    .findStatusId(status, ServiceEnum.STATUS.getValue(), PSPEnum.IRAN_KISH.getValue());
            result.setStatusTypeId(statusId);
            result.setStatus(messages.get("psp.ik.status." + status));
            result.setErrorCode("0");
            result.setErrorDesc(result.getStatus());
        } else if (value.equalsIgnoreCase("false") || value == null)
        {
            if (exceptionCode.equalsIgnoreCase("-1"))
            {
                result.setStatusTypeId(StatusTypeEnum.EXCEPTION.getValue());
                result.setErrorDesc(messages.get("psp.request.exception"));
                result.setErrorCode(exceptionCode);
            } else if (exceptionCode.equalsIgnoreCase("0"))
            {
                result.setStatusTypeId(StatusTypeEnum.FAIL.getValue());
                result.setErrorCode(exceptionCode);
                result.setErrorDesc(exception);
            }
        }
        logger.info("End of method [makeResponseFromIK]");

        return result;
    }

    private String makeContentByCompanyCode(PosStatusRequest posStatusRequest)
    {
        StringBuilder result = new StringBuilder();

        if (posStatusRequest.getCompanyCode().equalsIgnoreCase(PSPEnum.IRAN_KISH.getValue()))
        {
            result.append("<AcceptorNo></AcceptorNo>")
                  .append("<ClientTrackingCode>").append(posStatusRequest.getRequestNumber())
                  .append("</ClientTrackingCode>")
                  .append("<TrackingCode>").append(posStatusRequest.getTrackingCode())
                  .append("</TrackingCode>");
        } else if (posStatusRequest.getCompanyCode().equalsIgnoreCase(PSPEnum.TEJARAT_PARSIAN.getValue()))
        {
            result.append("<PosId>").append(posStatusRequest.getTrackingCode()).append("</PosId>");
        }

        return result.toString();
    }
}
