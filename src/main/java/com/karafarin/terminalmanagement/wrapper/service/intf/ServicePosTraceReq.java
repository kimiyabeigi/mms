package com.karafarin.terminalmanagement.wrapper.service.intf;

import com.karafarin.terminalmanagement.wrapper.model.req.PosTraceRequest;
import com.karafarin.terminalmanagement.wrapper.model.resp.PosTraceResponse;

import java.sql.SQLException;

public interface ServicePosTraceReq
{
    PosTraceResponse posTraceReq(String companyCode, PosTraceRequest posTraceRequest) throws SQLException;
}
