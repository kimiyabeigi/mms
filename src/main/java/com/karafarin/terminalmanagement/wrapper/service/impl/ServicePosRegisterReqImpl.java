package com.karafarin.terminalmanagement.wrapper.service.impl;

import com.karafarin.terminalmanagement.message.enums.RequestStatusEnum;
import com.karafarin.terminalmanagement.model.Messages;
import com.karafarin.terminalmanagement.model.core.ChannelRequest;
import com.karafarin.terminalmanagement.model.core.ChannelResponse;
import com.karafarin.terminalmanagement.util.ChannelUtil;
import com.karafarin.terminalmanagement.util.UserUtil;
import com.karafarin.terminalmanagement.util.date.DateConverterUtil;
import com.karafarin.terminalmanagement.wrapper.enums.PSPEnum;
import com.karafarin.terminalmanagement.wrapper.enums.StatusTypeEnum;
import com.karafarin.terminalmanagement.wrapper.model.req.PosRegisterRequest;
import com.karafarin.terminalmanagement.wrapper.model.resp.PosRegisterResponse;
import com.karafarin.terminalmanagement.wrapper.service.intf.ServicePosRegisterReq;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.w3c.dom.Node;

import java.util.Date;

@Service
public class ServicePosRegisterReqImpl implements ServicePosRegisterReq
{
    private static final Logger logger = LogManager.getLogger(ServicePosRegisterReqImpl.class);

    @Autowired
    private ChannelUtil channelUtil;
    @Autowired
    private Messages messages;

    @Override
    public PosRegisterResponse posRegisterReq(String companyCode, PosRegisterRequest posRegisterRequest)
    {
        try
        {
            logger.info("Start of method [posRegisterReq] for companyCode [" + posRegisterRequest.getCompanyCode() + "]");

            PosRegisterResponse result = new PosRegisterResponse();

            ChannelRequest channelRequest = new ChannelRequest();

            channelRequest.setCmd(companyCode + "PosRegisterReq");
            channelRequest.setAppName("RYMMS");
            channelRequest.setUserId(String.valueOf(UserUtil.getCurrentUser().getUserId()));
            channelRequest.setMsgId("mms_msg_cgi" + UserUtil.getCurrentUser().getUserId() + "_" +
                                    DateConverterUtil.getAsStringWithTime(new Date(), "fa")
                                                     .replaceAll("[^a-zA-Z0-9]", ""));
            channelRequest.setJobId("GCIJOB");
            channelRequest.setBranchCode(String.valueOf(UserUtil.getCurrentUser().getBranchId()));
            channelRequest.setContent(posRegisterRequest.getRegisterFields());

            ChannelResponse channelResponse = channelUtil.sendRequest(channelRequest);

            if (channelResponse.getErrorNo() == 0)
            {
                result = makePosRegisterResponse(posRegisterRequest.getCompanyCode(), channelResponse.getContent());
            }
            else if (channelResponse.getErrorNo() < 0)
            {
                result.setStatusTypeId(StatusTypeEnum.CHANNEL_ERROR.getValue());
                result.setErrorCode(String.valueOf(channelResponse.getErrorNo()));
                result.setErrorDesc(channelResponse.getErrorMessage());
                result.setTrackingCode(null);
            }
            else
            {
                result.setStatusTypeId(StatusTypeEnum.EXCEPTION.getValue());
                result.setErrorCode(String.valueOf(channelResponse.getErrorNo()));
                result.setErrorDesc(channelResponse.getErrorMessage());
                result.setTrackingCode(null);
            }

            logger.info("Start of method [posRegisterReq] for companyCode [" + posRegisterRequest.getCompanyCode() + "]");
            return result;

        } catch (Exception e)
        {
            logger.error("There is an error in method [posRegisterReq] ", e);
        }

        return null;
    }

    private PosRegisterResponse makePosRegisterResponse(String companyCode, Node content)
    {
        logger.info("Start of method [makePosRegisterResponse] for companyCode [" + companyCode + "]");

        PosRegisterResponse result = new PosRegisterResponse();
        if (companyCode.equalsIgnoreCase(PSPEnum.IRAN_KISH.getValue()))
        {
            result = makeResponseFromIK(content);

        } else if (companyCode.equalsIgnoreCase(PSPEnum.TEJARAT_PARSIAN.getValue()))
        {
            result = makeResponseFromTP(content);
        }

        logger.info("End of method [makePosRegisterResponse] for companyCode [" + companyCode + "]");

        return result;
    }

    private PosRegisterResponse makeResponseFromTP(Node content)
    {
        logger.info("Start of method [makeResponseFromTP]");
        PosRegisterResponse result = new PosRegisterResponse();
        String channelState = null;
        String actionState = null;
        String description = null;

        for (int i = 0; i < content.getChildNodes().getLength(); i++)
        {
            String nodeValue = content.getChildNodes().item(i).getTextContent();
            switch (content.getChildNodes().item(i).getNodeName())
            {
                case "PosTrackIds":
                    result.setTrackingCode(nodeValue);
                    break;
                case "ChannelState":
                    channelState = nodeValue;
                    break;
                case "ActionState":
                    actionState = nodeValue;
                    break;
                case "Description":
                    description = nodeValue;
                    break;
            }
        }

        if (channelState.equalsIgnoreCase("0"))
        {
            result.setStatusTypeId(StatusTypeEnum.EXCEPTION.getValue());
            result.setErrorCode(actionState);
            result.setErrorDesc(messages.get("psp.request.exception"));
        } else if (channelState.equalsIgnoreCase("1"))
        {
            if (actionState.equalsIgnoreCase("0"))
            {
                result.setStatusTypeId(StatusTypeEnum.SUCCESS.getValue());
                result.setErrorCode("0");
                result.setErrorDesc(messages.get("psp.request.success"));
            } else
            {
                result.setStatusTypeId(StatusTypeEnum.FAIL.getValue());
                result.setErrorCode(actionState);
                result.setErrorDesc(StringUtils.isNotEmpty(description) ? description :
                                   messages.get("psp.tp.register.error." + actionState));
            }
        }

        logger.info("End of method [makeResponseFromTP]");
        return result;
    }

    private PosRegisterResponse makeResponseFromIK(Node content)
    {
        logger.info("Start of method [makeResponseFromIK]");

        PosRegisterResponse result = new PosRegisterResponse();
        String exceptionDesc = null;
        String exceptionCode = null;

        for (int i = 0; i < content.getChildNodes().getLength(); i++)
        {
            String nodeValue = content.getChildNodes().item(i).getTextContent();
            switch (content.getChildNodes().item(i).getNodeName())
            {
                case "ExceptionCode":
                    exceptionCode = nodeValue;
                    break;
                case "TrackingCode":
                    result.setTrackingCode(nodeValue);
                    break;
            }
        }

        if (exceptionCode.equalsIgnoreCase("0"))
        {
            result.setStatusTypeId(StatusTypeEnum.SUCCESS.getValue());
            result.setErrorCode("0");
            result.setErrorDesc(messages.get("psp.request.success"));
        } else if (exceptionCode.equalsIgnoreCase("-1"))
        {
            result.setStatusTypeId(StatusTypeEnum.EXCEPTION.getValue());
            result.setErrorCode(exceptionCode);
            result.setErrorDesc(messages.get("psp.request.exception"));
        } else
        {
            result.setStatusTypeId(StatusTypeEnum.FAIL.getValue());
            result.setErrorDesc(messages.get("psp.ik.register.error." + exceptionCode));
            result.setErrorCode(exceptionCode);
        }

        logger.info("End of method [makeResponseFromIK]");

        return result;
    }

}
