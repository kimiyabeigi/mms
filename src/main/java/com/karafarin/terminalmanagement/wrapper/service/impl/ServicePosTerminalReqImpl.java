package com.karafarin.terminalmanagement.wrapper.service.impl;

import com.karafarin.terminalmanagement.dao.impl.setting.CompanySettingStatusDaoImpl;
import com.karafarin.terminalmanagement.model.Messages;
import com.karafarin.terminalmanagement.model.core.ChannelRequest;
import com.karafarin.terminalmanagement.model.core.ChannelResponse;
import com.karafarin.terminalmanagement.util.ChannelUtil;
import com.karafarin.terminalmanagement.util.UserUtil;
import com.karafarin.terminalmanagement.util.date.DateConverterUtil;
import com.karafarin.terminalmanagement.wrapper.enums.PSPEnum;
import com.karafarin.terminalmanagement.wrapper.enums.ServiceEnum;
import com.karafarin.terminalmanagement.wrapper.enums.StatusTypeEnum;
import com.karafarin.terminalmanagement.wrapper.model.req.PosTerminalRequest;
import com.karafarin.terminalmanagement.wrapper.model.resp.PosTerminalResponse;
import com.karafarin.terminalmanagement.wrapper.service.intf.ServicePosTerminalReq;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.w3c.dom.Node;

import java.sql.SQLException;
import java.util.Date;

@Service
public class ServicePosTerminalReqImpl implements ServicePosTerminalReq
{
    private static final Logger logger = LogManager.getLogger(ServicePosTerminalReqImpl.class);

    @Autowired
    private ChannelUtil channelUtil;
    @Autowired
    private Messages messages;
    @Autowired
    private CompanySettingStatusDaoImpl companySettingStatusDao;

    @Override
    public PosTerminalResponse posTerminalReq(String companyCode, PosTerminalRequest posTerminalRequest)
    {
        try
        {
            logger.info(
                    "Start of method [posTerminalReq] for companyCode [" + posTerminalRequest.getCompanyCode() + "]");

            PosTerminalResponse result = new PosTerminalResponse();

            ChannelRequest channelRequest = new ChannelRequest();

            channelRequest.setCmd(companyCode + "posTerminalReq");
            channelRequest.setAppName("RYMMS");
            channelRequest.setUserId(String.valueOf(UserUtil.getCurrentUser().getUserId()));
            channelRequest.setMsgId("mms_msg_cgi" + UserUtil.getCurrentUser().getUserId() + "_" +
                                    DateConverterUtil.getAsStringWithTime(new Date(), "fa")
                                                     .replaceAll("[^a-zA-Z0-9]", ""));
            channelRequest.setJobId("GCIJOB");
            channelRequest.setBranchCode(String.valueOf(UserUtil.getCurrentUser().getBranchId()));
            channelRequest.setContent(makeContentByCompanyCode(posTerminalRequest));

            ChannelResponse channelResponse = channelUtil.sendRequest(channelRequest);

            if (channelResponse.getErrorNo() == 0)
            {
                result = makePosTraceResponse(posTerminalRequest.getCompanyCode(), channelResponse.getContent());
            }
            else if (channelResponse.getErrorNo() < 0)
            {
                result.setStatusTypeId(StatusTypeEnum.CHANNEL_ERROR.getValue());
                result.setErrorCode(String.valueOf(channelResponse.getErrorNo()));
                result.setErrorDesc(channelResponse.getErrorMessage());
            }
            else
            {
                result.setStatusTypeId(StatusTypeEnum.EXCEPTION.getValue());
                result.setErrorCode(String.valueOf(channelResponse.getErrorNo()));
                result.setErrorDesc(channelResponse.getErrorMessage());
            }

            logger.info("End of method [posTerminalReq] for companyCode [" + posTerminalRequest.getCompanyCode() + "]");

            return result;
        } catch (Exception e)
        {
            logger.error("There is an error in method [posTerminalReq] ", e);
        }
        return null;

    }

    private PosTerminalResponse makePosTraceResponse(String companyCode, Node content) throws SQLException
    {
        logger.info("Start of method [makePosTraceResponse] for companyCode [" + companyCode + "]");

        PosTerminalResponse result = new PosTerminalResponse();
        if (companyCode.equalsIgnoreCase(PSPEnum.IRAN_KISH.getValue()))
        {
            result = makeResponseFromIK(content);

        } else if (companyCode.equalsIgnoreCase(PSPEnum.TEJARAT_PARSIAN.getValue()))
        {
            result = makeResponseFromTP(content);
        }

        logger.info("End of method [makePosTraceResponse] for companyCode [" + companyCode + "]");

        return result;
    }

    private PosTerminalResponse makeResponseFromTP(Node content)
    {
        PosTerminalResponse result = new PosTerminalResponse();
        logger.info("Start of method [makeResponseFromTP]");

        String posTermNo = null;
        String customerId = null;
        String actionState = null;
        String channelState = null;
        for (int i = 0; i < content.getChildNodes().getLength(); i++)
        {
            String nodeValue = content.getChildNodes().item(i).getTextContent();
            switch (content.getChildNodes().item(i).getNodeName())
            {
                case "PosTermNo":
                    posTermNo = nodeValue;
                    break;
                case "CustomerId":
                    customerId = nodeValue;
                    break;
                case "UnInstallDate":
                    break;
                case "InstallDate":
                    break;
                case "ChannelState":
                    channelState = nodeValue;
                    break;
                case "ActionState":
                    actionState = nodeValue;
                    break;
                case "Description":
                    break;
            }
        }

        if (channelState.equalsIgnoreCase("0"))
        {
            result.setStatusTypeId(StatusTypeEnum.EXCEPTION.getValue());
            result.setErrorCode(actionState);
            result.setErrorDesc(messages.get("psp.request.exception"));
        } else if (channelState.equalsIgnoreCase("1"))
        {
            if (actionState.equalsIgnoreCase("0"))
            {
                result.setStatusTypeId(StatusTypeEnum.SUCCESS.getValue());
                result.setErrorCode("0");
                result.setErrorDesc(messages.get("psp.request.terminal.success"));
                result.setTerminalCode(StringUtils.isNotEmpty(posTermNo) ? posTermNo : null);
                result.setAcceptanceCode(StringUtils.isNotEmpty(customerId) ? customerId : null);
            } else
            {
                result.setStatusTypeId(StatusTypeEnum.UNKNOWN.getValue());
                result.setErrorCode(actionState);
                result.setErrorDesc(messages.get("psp.tp.terminal." + actionState));
            }
        }

        logger.info("End of method [makeResponseFromTP]");
        return result;
    }

    private PosTerminalResponse makeResponseFromIK(Node content) throws SQLException
    {
        logger.info("Start of method [makeResponseFromIK]");
        PosTerminalResponse result = new PosTerminalResponse();

        String value = null;
        String exceptionCode = null;
        String exception = null;
        String status = null;
        String terminalCode = null;
        String acceptorNo = null;

        for (int i = 0; i < content.getChildNodes().getLength(); i++)
        {
            String nodeValue = content.getChildNodes().item(i).getTextContent();
            switch (content.getChildNodes().item(i).getNodeName())
            {
                case "Value":
                    value = nodeValue;
                    break;
                case "ExceptionDesc":
                    exception = nodeValue;
                    break;
                case "ExceptionCode":
                    exceptionCode = nodeValue;
                    break;
                case "TrackingCode":
                    break;
                case "ClientTrackingCode":
                    break;
                case "Status":
                    status = nodeValue;
                    break;
                case "TerminalCode":
                    terminalCode = nodeValue;
                    break;
                case "AcceptorNo":
                    acceptorNo = nodeValue;
                    break;
            }
        }

        if (value.equalsIgnoreCase("true") && exceptionCode.equalsIgnoreCase("0"))
        {
            Long statusId = companySettingStatusDao
                    .findStatusId(status, ServiceEnum.TERMINAL.getValue(), PSPEnum.IRAN_KISH.getValue());
            result.setStatusTypeId(statusId);
            result.setErrorCode(String.valueOf(statusId));
            if (statusId.equals(StatusTypeEnum.SUCCESS.getValue()))
            {
                result.setErrorDesc(messages.get("psp.request.terminal.success"));
                result.setTerminalCode(StringUtils.isNotEmpty(terminalCode) ? terminalCode : null);
                result.setAcceptanceCode(StringUtils.isNotEmpty(acceptorNo) ? acceptorNo : null);
            }
            else
            {
                result.setErrorDesc(messages.get("psp.ik.status." + status));
            }
        } else if (value.equalsIgnoreCase("false") || value == null)
        {
            if (exceptionCode.equalsIgnoreCase("-1"))
            {
                result.setStatusTypeId(StatusTypeEnum.EXCEPTION.getValue());
                result.setErrorCode(exceptionCode);
                result.setErrorDesc(messages.get("psp.request.exception"));
            } else if (exceptionCode.equalsIgnoreCase("0"))
            {
                result.setStatusTypeId(StatusTypeEnum.FAIL.getValue());
                result.setErrorCode(exceptionCode);
                result.setErrorDesc(exception);
            }
        }
        logger.info("End of method [makeResponseFromIK]");

        return result;
    }

    private String makeContentByCompanyCode(PosTerminalRequest posTerminalRequest)
    {
        StringBuilder result = new StringBuilder();

        if (posTerminalRequest.getCompanyCode().equalsIgnoreCase(PSPEnum.IRAN_KISH.getValue()))
        {
            result.append("<AcceptorNo></AcceptorNo>")
                  .append("<ClientTrackingCode>").append(posTerminalRequest.getRequestNumber())
                  .append("</ClientTrackingCode>")
                  .append("<TrackingCode>").append(posTerminalRequest.getTrackingCode())
                  .append("</TrackingCode>");
        } else if (posTerminalRequest.getCompanyCode().equalsIgnoreCase(PSPEnum.TEJARAT_PARSIAN.getValue()))
        {
            result.append("<PosId>").append(posTerminalRequest.getTrackingCode()).append("</PosId>");
        }

        return result.toString();
    }
}
