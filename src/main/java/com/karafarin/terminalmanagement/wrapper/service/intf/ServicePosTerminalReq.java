package com.karafarin.terminalmanagement.wrapper.service.intf;

import com.karafarin.terminalmanagement.wrapper.model.req.PosTerminalRequest;
import com.karafarin.terminalmanagement.wrapper.model.resp.PosTerminalResponse;

public interface ServicePosTerminalReq
{
    PosTerminalResponse posTerminalReq(String companyCode, PosTerminalRequest posTerminalRequest);
}
