package com.karafarin.terminalmanagement.wrapper.model.resp;

public class PosResponse
{
    private Long statusTypeId;
    private String errorCode;
    private String errorDesc;

    public Long getStatusTypeId()
    {
        return statusTypeId;
    }

    public void setStatusTypeId(Long statusTypeId)
    {
        this.statusTypeId = statusTypeId;
    }

    public String getErrorCode()
    {
        return errorCode;
    }

    public void setErrorCode(String errorCode)
    {
        this.errorCode = errorCode;
    }

    public String getErrorDesc()
    {
        return errorDesc;
    }

    public void setErrorDesc(String errorDesc)
    {
        this.errorDesc = errorDesc;
    }
}
