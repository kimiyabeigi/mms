package com.karafarin.terminalmanagement.wrapper.model.resp;

public class PosTraceResponse extends PosResponse
{
    private String trackingCode;

    public String getTrackingCode()
    {
        return trackingCode;
    }

    public void setTrackingCode(String trackingCode)
    {
        this.trackingCode = trackingCode;
    }
}
