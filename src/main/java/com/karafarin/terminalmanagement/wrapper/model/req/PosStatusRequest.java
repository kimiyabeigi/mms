package com.karafarin.terminalmanagement.wrapper.model.req;

public class PosStatusRequest extends PosRequest
{
    private Long requestNumber;

    public Long getRequestNumber()
    {
        return requestNumber;
    }

    public void setRequestNumber(Long requestNumber)
    {
        this.requestNumber = requestNumber;
    }
}
