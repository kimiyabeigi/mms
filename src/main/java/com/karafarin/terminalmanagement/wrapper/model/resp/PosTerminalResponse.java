package com.karafarin.terminalmanagement.wrapper.model.resp;

public class PosTerminalResponse extends PosResponse
{
    private String acceptanceCode;
    private String terminalCode;

    public String getAcceptanceCode()
    {
        return acceptanceCode;
    }

    public void setAcceptanceCode(String acceptanceCode)
    {
        this.acceptanceCode = acceptanceCode;
    }

    public String getTerminalCode()
    {
        return terminalCode;
    }

    public void setTerminalCode(String terminalCode)
    {
        this.terminalCode = terminalCode;
    }
}
