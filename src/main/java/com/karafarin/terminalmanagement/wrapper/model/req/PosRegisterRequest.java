package com.karafarin.terminalmanagement.wrapper.model.req;

public class PosRegisterRequest
{
    private String companyCode;
    private String registerFields;

    public String getCompanyCode()
    {
        return companyCode;
    }

    public void setCompanyCode(String companyCode)
    {
        this.companyCode = companyCode;
    }

    public String getRegisterFields()
    {
        return registerFields;
    }

    public void setRegisterFields(String registerFields)
    {
        this.registerFields = registerFields;
    }
}
