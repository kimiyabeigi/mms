package com.karafarin.terminalmanagement.wrapper.model.req;

import com.karafarin.terminalmanagement.wrapper.model.resp.PosRegisterResponse;

public class PosTerminalRequest extends PosRequest
{
    private Long requestNumber;

    public Long getRequestNumber()
    {
        return requestNumber;
    }

    public void setRequestNumber(Long requestNumber)
    {
        this.requestNumber = requestNumber;
    }
}
