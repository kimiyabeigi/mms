package com.karafarin.terminalmanagement.wrapper.model.resp;

public class PosStatusResponse extends PosResponse
{
    private String status;

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }
}
