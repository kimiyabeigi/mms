package com.karafarin.terminalmanagement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
public class TerminalManagementApplication {

	public static void main(String[] args) {
		SpringApplication.run(TerminalManagementApplication.class, args);
		System.out.println("###########################");
		System.out.println("### System initialized! ###");
		System.out.println("###########################");
	}
}
