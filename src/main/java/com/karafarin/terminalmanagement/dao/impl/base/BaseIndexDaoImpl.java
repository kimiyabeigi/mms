package com.karafarin.terminalmanagement.dao.impl.base;

import com.karafarin.terminalmanagement.config.jdbc.SqlConnectionConfiguration;
import com.karafarin.terminalmanagement.dao.intf.base.BaseIndexDao;
import com.karafarin.terminalmanagement.dao.mapper.base.BaseIndexMapper;
import com.karafarin.terminalmanagement.dto.base.BaseIndex;
import com.karafarin.terminalmanagement.util.UserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository
public class BaseIndexDaoImpl implements BaseIndexDao
{
    @Autowired
    private SqlConnectionConfiguration sqlConnectionConfiguration;

    private JdbcTemplate jdbcTemplate = null;
    private SimpleJdbcCall simpleJdbcCall;

    @PostConstruct
    public void postConstruct()
    {
        if (jdbcTemplate == null)
            jdbcTemplate = sqlConnectionConfiguration.getJdbcTemplate();
    }

    @Override
    public Long add(BaseIndex entity) throws SQLException, DataAccessException
    {
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_BaseIndexIns")
                .declareParameters(
                        new SqlParameter("UserCallerId", Types.BIGINT),
                        new SqlParameter("IndexId", Types.BIGINT),
                        new SqlParameter("IndexCode", Types.NVARCHAR),
                        new SqlParameter("Name", Types.NVARCHAR),
                        new SqlParameter("Ip", Types.NVARCHAR),
                        new SqlParameter("ComputerName", Types.NVARCHAR));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("UserCallerId", UserUtil.getCurrentUser().getUserId());
        sqlParameterSource.addValue("IndexId", entity.getIndexId());
        sqlParameterSource.addValue("IndexCode", entity.getIndexCode());
        sqlParameterSource.addValue("Name", entity.getName());
        sqlParameterSource.addValue("Ip", UserUtil.getCurrentUser().getIp());
        sqlParameterSource.addValue("ComputerName", UserUtil.getCurrentUser().getComputerName());

        Map<String, Object> resultExecuted = simpleJdbcCall.execute(sqlParameterSource);
        return (Long) resultExecuted.get("RETURN_VALUE");
    }

    @Override
    public BaseIndex findById(Long id) throws SQLException, DataAccessException
    {
        BaseIndex result = new BaseIndex();
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_BaseIndexSel")
                .declareParameters(
                        new SqlParameter("IndexId", Types.BIGINT));
        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("IndexId", id);

        Map<String, Object> resultExecuted = simpleJdbcCall.
                returningResultSet("records", new BaseIndexMapper()).
                execute(sqlParameterSource);
        if (((List) resultExecuted.get("records")).size() > 0)
            result = ((List<BaseIndex>) resultExecuted.get("records")).get(0);

         return result;
    }

    @Override
    public List<BaseIndex> findAll() throws SQLException, DataAccessException
    {
        List<BaseIndex> result = new ArrayList<>();
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_BaseIndexSel")
                .declareParameters(
                        new SqlParameter("IndexId", Types.BIGINT));
        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("IndexId", -1);

        Map<String, Object> resultExecuted = simpleJdbcCall.
                returningResultSet("records", new BaseIndexMapper()).
                execute(sqlParameterSource);
        if (((List) resultExecuted.get("records")).size() > 0)
            result = (List<BaseIndex>) resultExecuted.get("records");

        return result;
    }

    @Override
    public boolean update(BaseIndex entity) throws SQLException, DataAccessException
    {
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_BaseIndexUpd")
                .declareParameters(
                        new SqlParameter("UserCallerId", Types.BIGINT),
                        new SqlParameter("IndexId", Types.BIGINT),
                        new SqlParameter("IndexCode", Types.NVARCHAR),
                        new SqlParameter("Name", Types.NVARCHAR),
                        new SqlParameter("Ip", Types.NVARCHAR),
                        new SqlParameter("ComputerName", Types.NVARCHAR));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("UserCallerId", UserUtil.getCurrentUser().getUserId());
        sqlParameterSource.addValue("IndexId", entity.getIndexId());
        sqlParameterSource.addValue("IndexCode", entity.getIndexCode());
        sqlParameterSource.addValue("Name", entity.getName());
        sqlParameterSource.addValue("Ip", UserUtil.getCurrentUser().getIp());
        sqlParameterSource.addValue("ComputerName", UserUtil.getCurrentUser().getComputerName());

        Map<String, Object> resultExecuted = simpleJdbcCall.execute(sqlParameterSource);
        return (Integer) resultExecuted.get("#update-count-1") == 1;
    }

    @Override
    public List<BaseIndex> search(BaseIndex entity) throws SQLException, DataAccessException
    {
        return null;
    }

    @Override
    public boolean delete(Long id) throws SQLException, DataAccessException
    {
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_BaseIndexDel")
                .declareParameters(
                        new SqlParameter("UserCallerId", Types.BIGINT),
                        new SqlParameter("IndexId", Types.BIGINT),
                        new SqlParameter("Ip", Types.NVARCHAR),
                        new SqlParameter("ComputerName", Types.NVARCHAR));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("UserCallerId", UserUtil.getCurrentUser().getUserId());
        sqlParameterSource.addValue("IndexId", id);
        sqlParameterSource.addValue("Ip", UserUtil.getCurrentUser().getIp());
        sqlParameterSource.addValue("ComputerName", UserUtil.getCurrentUser().getComputerName());

        Map<String, Object> resultExecuted = simpleJdbcCall.execute(sqlParameterSource);
        return (Integer) resultExecuted.get("#update-count-1") == 1;
    }
}
