package com.karafarin.terminalmanagement.dao.impl.auth;

import com.karafarin.terminalmanagement.config.jdbc.SqlConnectionConfiguration;
import com.karafarin.terminalmanagement.dao.intf.auth.RoleFormDao;
import com.karafarin.terminalmanagement.dao.mapper.auth.RoleFormMapper;
import com.karafarin.terminalmanagement.dto.auth.RoleForm;
import com.karafarin.terminalmanagement.util.UserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository
public class RoleFormDaoImpl implements RoleFormDao
{
    @Autowired
    private SqlConnectionConfiguration sqlConnectionConfiguration;

    private JdbcTemplate jdbcTemplate = null;
    private SimpleJdbcCall simpleJdbcCall;

    @PostConstruct
    public void postConstruct()
    {
        if (jdbcTemplate == null) jdbcTemplate = sqlConnectionConfiguration.getJdbcTemplate();
    }

    @Override
    public Long add(RoleForm entity) throws SQLException, DataAccessException
    {
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_RoleFormIns")
                      .declareParameters(new SqlParameter("UserCallerId", Types.BIGINT),
                                         new SqlParameter("RoleId", Types.BIGINT),
                                         new SqlParameter("FormId", Types.BIGINT),
                                         new SqlParameter("Ip", Types.NVARCHAR),
                                         new SqlParameter("ComputerName", Types.NVARCHAR));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("UserCallerId", UserUtil.getCurrentUser().getUserId());
        sqlParameterSource.addValue("RoleId", entity.getRoleId());
        sqlParameterSource.addValue("FormId", entity.getFormId());
        sqlParameterSource.addValue("Ip", UserUtil.getCurrentUser().getIp());
        sqlParameterSource.addValue("ComputerName", UserUtil.getCurrentUser().getComputerName());

        Map<String, Object> resultExecuted = simpleJdbcCall.execute(sqlParameterSource);
        return (Long) resultExecuted.get("RETURN_VALUE");
    }

    @Override
    public RoleForm findById(Long id) throws SQLException, DataAccessException
    {
        return null;
    }

    @Override
    public List<RoleForm> findAll() throws SQLException, DataAccessException
    {
        return null;
    }

    @Override
    public boolean delete(Long id) throws SQLException, DataAccessException
    {
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_RoleFormDel")
                      .declareParameters(new SqlParameter("UserCallerId", Types.BIGINT),
                                         new SqlParameter("RoleFormId", Types.BIGINT),
                                         new SqlParameter("Ip", Types.NVARCHAR),
                                         new SqlParameter("ComputerName", Types.NVARCHAR));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("UserCallerId", UserUtil.getCurrentUser().getUserId());
        sqlParameterSource.addValue("RoleFormId", id);
        sqlParameterSource.addValue("Ip", UserUtil.getCurrentUser().getIp());
        sqlParameterSource.addValue("ComputerName", UserUtil.getCurrentUser().getComputerName());

        Map<String, Object> resultExecuted = simpleJdbcCall.execute(sqlParameterSource);
        return (Integer) resultExecuted.get("#update-count-1") == 1;
    }

    @Override
    public boolean update(RoleForm entity) throws SQLException, DataAccessException
    {
        return false;
    }

    @Override
    public List<RoleForm> search(RoleForm entity) throws SQLException, DataAccessException
    {
        return null;
    }

    @Override
    public List<RoleForm> findByRoleId(Long id) throws SQLException, DataAccessException
    {
        List<RoleForm> result = new ArrayList<>();
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_RoleFormSel")
                      .declareParameters(new SqlParameter("RoleId", Types.BIGINT));
        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("RoleId", id);

        Map<String, Object> resultExecuted =
                simpleJdbcCall.returningResultSet("records", new RoleFormMapper()).execute(sqlParameterSource);
        if (((List) resultExecuted.get("records")).size() > 0)
            result = (List<RoleForm>) resultExecuted.get("records");

        return result;
    }
}
