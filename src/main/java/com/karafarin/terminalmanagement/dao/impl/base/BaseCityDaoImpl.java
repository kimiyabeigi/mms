package com.karafarin.terminalmanagement.dao.impl.base;

import com.karafarin.terminalmanagement.config.jdbc.SqlConnectionConfiguration;
import com.karafarin.terminalmanagement.dao.intf.base.BaseCityDao;
import com.karafarin.terminalmanagement.dao.mapper.base.BaseCityMapper;
import com.karafarin.terminalmanagement.dao.mapper.base.BaseCompanyMapper;
import com.karafarin.terminalmanagement.dto.base.BaseCity;
import com.karafarin.terminalmanagement.util.UserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository
public class BaseCityDaoImpl implements BaseCityDao
{
    @Autowired
    private SqlConnectionConfiguration sqlConnectionConfiguration;

    private JdbcTemplate jdbcTemplate = null;
    private SimpleJdbcCall simpleJdbcCall;

    @PostConstruct
    public void postConstruct()
    {
        if (jdbcTemplate == null)
            jdbcTemplate = sqlConnectionConfiguration.getJdbcTemplate();
    }

    @Override
    public Long add(BaseCity entity) throws SQLException, DataAccessException
    {
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_BaseCityIns")
                .declareParameters(
                        new SqlParameter("UserCallerId", Types.BIGINT),
                        new SqlParameter("CityId", Types.BIGINT),
                        new SqlParameter("CityCode", Types.NVARCHAR),
                        new SqlParameter("StateId", Types.BIGINT),
                        new SqlParameter("Name", Types.NVARCHAR),
                        new SqlParameter("NameEN", Types.NVARCHAR),
                        new SqlParameter("IsActive", Types.BIT),
                        new SqlParameter("Ip", Types.NVARCHAR),
                        new SqlParameter("ComputerName", Types.NVARCHAR));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("UserCallerId", UserUtil.getCurrentUser().getUserId());
        sqlParameterSource.addValue("CityId", entity.getCityId());
        sqlParameterSource.addValue("CityCode", entity.getCityCode());
        sqlParameterSource.addValue("StateId", entity.getStateId());
        sqlParameterSource.addValue("Name", entity.getName());
        sqlParameterSource.addValue("NameEN", entity.getNameEN());
        sqlParameterSource.addValue("IsActive", entity.isActive());
        sqlParameterSource.addValue("Ip", UserUtil.getCurrentUser().getIp());
        sqlParameterSource.addValue("ComputerName", UserUtil.getCurrentUser().getComputerName());

        Map<String, Object> resultExecuted = simpleJdbcCall.execute(sqlParameterSource);
        return (Long) resultExecuted.get("RETURN_VALUE");
    }

    @Override
    public BaseCity findById(Long id) throws SQLException, DataAccessException
    {
        BaseCity result = new BaseCity();
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_BaseCitySel")
                .declareParameters(
                        new SqlParameter("CityId", Types.BIGINT),
                        new SqlParameter("All", Types.BIT));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("CityId", id);
        sqlParameterSource.addValue("All", 1);

        Map<String, Object> resultExecuted = simpleJdbcCall.
                returningResultSet("records", new BaseCityMapper()).
                execute(sqlParameterSource);
        if (((List) resultExecuted.get("records")).size() > 0)
            result = ((List<BaseCity>) resultExecuted.get("records")).get(0);

        return result;
    }

    @Override
    public List<BaseCity> findAll() throws SQLException, DataAccessException
    {
        return null;
    }

    @Override
    public List<BaseCity> findAll(boolean all) throws SQLException, DataAccessException
    {
        List<BaseCity> result = new ArrayList<>();
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_BaseCitySel")
                .declareParameters(
                        new SqlParameter("CityId", Types.BIGINT),
                        new SqlParameter("All", Types.BIT));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("CityId", -1);
        sqlParameterSource.addValue("All", all);

        Map<String, Object> resultExecuted = simpleJdbcCall.
                returningResultSet("records", new BaseCompanyMapper()).
                execute(sqlParameterSource);
        if (((List) resultExecuted.get("records")).size() > 0)
            result = (List<BaseCity>) resultExecuted.get("records");

        return result;
    }

    @Override
    public boolean delete(Long id) throws SQLException, DataAccessException
    {
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_BaseCityDel")
                .declareParameters(
                        new SqlParameter("UserCallerId", Types.BIGINT),
                        new SqlParameter("CityId", Types.BIGINT),
                        new SqlParameter("Ip", Types.NVARCHAR),
                        new SqlParameter("ComputerName", Types.NVARCHAR));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("UserCallerId", UserUtil.getCurrentUser().getUserId());
        sqlParameterSource.addValue("CityId", id);
        sqlParameterSource.addValue("Ip", UserUtil.getCurrentUser().getIp());
        sqlParameterSource.addValue("ComputerName", UserUtil.getCurrentUser().getComputerName());

        Map<String, Object> resultExecuted = simpleJdbcCall.execute(sqlParameterSource);
        return (Integer) resultExecuted.get("#update-count-1") == 1;
    }

    @Override
    public boolean update(BaseCity entity) throws SQLException, DataAccessException
    {
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_BaseCityUpd")
                .declareParameters(
                        new SqlParameter("UserCallerId", Types.BIGINT),
                        new SqlParameter("CityId", Types.BIGINT),
                        new SqlParameter("CityCode", Types.NVARCHAR),
                        new SqlParameter("StateId", Types.BIGINT),
                        new SqlParameter("Name", Types.NVARCHAR),
                        new SqlParameter("NameEN", Types.NVARCHAR),
                        new SqlParameter("IsActive", Types.BIT),
                        new SqlParameter("Ip", Types.NVARCHAR),
                        new SqlParameter("ComputerName", Types.NVARCHAR));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("UserCallerId", UserUtil.getCurrentUser().getUserId());
        sqlParameterSource.addValue("CityId",entity.getCityId());
        sqlParameterSource.addValue("CityCode", entity.getCityCode());
        sqlParameterSource.addValue("StateId", entity.getStateId());
        sqlParameterSource.addValue("Name", entity.getName());
        sqlParameterSource.addValue("NameEN", entity.getNameEN());
        sqlParameterSource.addValue("IsActive", entity.isActive());
        sqlParameterSource.addValue("Ip", UserUtil.getCurrentUser().getIp());
        sqlParameterSource.addValue("ComputerName", UserUtil.getCurrentUser().getComputerName());

        Map<String, Object> resultExecuted = simpleJdbcCall.execute(sqlParameterSource);
        return (Integer) resultExecuted.get("#update-count-1") == 1;
    }

    @Override
    public List<BaseCity> search(BaseCity entity) throws SQLException, DataAccessException
    {
        return null;
    }

    @Override
    public List<BaseCity> findByStateId(Long id) throws SQLException, DataAccessException
    {
        List<BaseCity> result = new ArrayList<>();
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_BaseCitySelByStateId")
                .declareParameters(
                        new SqlParameter("StateId", Types.BIGINT));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("StateId", id);

        Map<String, Object> resultExecuted = simpleJdbcCall.
                returningResultSet("records", new BaseCityMapper()).
                execute(sqlParameterSource);
        if (((List) resultExecuted.get("records")).size() > 0)
            result = (List<BaseCity>) resultExecuted.get("records");

        return result;
    }
}
