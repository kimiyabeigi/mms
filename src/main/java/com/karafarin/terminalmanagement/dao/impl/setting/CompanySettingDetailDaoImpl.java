package com.karafarin.terminalmanagement.dao.impl.setting;

import com.karafarin.terminalmanagement.config.jdbc.SqlConnectionConfiguration;
import com.karafarin.terminalmanagement.dao.intf.setting.CompanySettingDetailDao;
import com.karafarin.terminalmanagement.dto.setting.CompanySettingDetail;
import com.karafarin.terminalmanagement.util.UserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.sql.SQLException;
import java.sql.Types;
import java.util.List;
import java.util.Map;

@Repository
public class CompanySettingDetailDaoImpl implements CompanySettingDetailDao
{
    @Autowired
    private SqlConnectionConfiguration sqlConnectionConfiguration;

    private JdbcTemplate jdbcTemplate = null;
    private SimpleJdbcCall simpleJdbcCall;

    @PostConstruct
    public void postConstruct()
    {
        if (jdbcTemplate == null)
            jdbcTemplate = sqlConnectionConfiguration.getJdbcTemplate();
    }

    @Override
    public Long add(CompanySettingDetail entity) throws SQLException, DataAccessException
    {
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_CompanySettingDetailIns")
                .declareParameters(
                        new SqlParameter("UserCallerId", Types.BIGINT),
                        new SqlParameter("CompanySettingId", Types.BIGINT),
                        new SqlParameter("TerminalId", Types.BIGINT),
                        new SqlParameter("Selected", Types.BOOLEAN),
                        new SqlParameter("Ip", Types.NVARCHAR),
                        new SqlParameter("ComputerName", Types.NVARCHAR));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("UserCallerId", UserUtil.getCurrentUser().getUserId());
        sqlParameterSource.addValue("CompanySettingId", entity.getCompanySettingId());
        sqlParameterSource.addValue("TerminalId", entity.getTerminalId());
        sqlParameterSource.addValue("Selected", entity.isSelected());
        sqlParameterSource.addValue("Ip", UserUtil.getCurrentUser().getIp());
        sqlParameterSource.addValue("ComputerName", UserUtil.getCurrentUser().getComputerName());

        Map<String, Object> resultExecuted = simpleJdbcCall.execute(sqlParameterSource);
        return (Long) resultExecuted.get("RETURN_VALUE");
    }

    @Override
    public CompanySettingDetail findById(Long id) throws SQLException, DataAccessException
    {
        return null;
    }

    @Override
    public List<CompanySettingDetail> findAll() throws SQLException, DataAccessException
    {
        return null;
    }

    @Override
    public boolean delete(Long id) throws SQLException, DataAccessException
    {
        return false;
    }

    @Override
    public boolean update(CompanySettingDetail entity) throws SQLException, DataAccessException
    {
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_CompanySettingDetailUpd")
                .declareParameters(
                        new SqlParameter("UserCallerId", Types.BIGINT),
                        new SqlParameter("CompanySettingDetailId", Types.BIGINT),
                        new SqlParameter("CompanySettingId", Types.BIGINT),
                        new SqlParameter("TerminalId", Types.BIGINT),
                        new SqlParameter("Selected", Types.BOOLEAN),
                        new SqlParameter("Ip", Types.NVARCHAR),
                        new SqlParameter("ComputerName", Types.NVARCHAR));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("UserCallerId", UserUtil.getCurrentUser().getUserId());
        sqlParameterSource.addValue("CompanySettingDetailId", entity.getCompanySettingDetailId());
        sqlParameterSource.addValue("CompanySettingId", entity.getCompanySettingId());
        sqlParameterSource.addValue("TerminalId", entity.getTerminalId());
        sqlParameterSource.addValue("Selected", entity.isSelected());
        sqlParameterSource.addValue("Ip", UserUtil.getCurrentUser().getIp());
        sqlParameterSource.addValue("ComputerName", UserUtil.getCurrentUser().getComputerName());

        Map<String, Object> resultExecuted = simpleJdbcCall.execute(sqlParameterSource);
        return (Integer) resultExecuted.get("#update-count-1") == 1;
    }

    @Override
    public List<CompanySettingDetail> search(CompanySettingDetail entity) throws SQLException, DataAccessException
    {
        return null;
    }

}
