package com.karafarin.terminalmanagement.dao.impl.psp;

import com.karafarin.terminalmanagement.config.jdbc.SqlConnectionConfiguration;
import com.karafarin.terminalmanagement.dao.intf.psp.PSPReportRegisterMasterDao;
import com.karafarin.terminalmanagement.dao.mapper.psp.PSPReportRegisterMasterMapper;
import com.karafarin.terminalmanagement.dto.psp.PSPReportRegisterMaster;
import com.karafarin.terminalmanagement.model.psp.ParamsReportMaster;
import com.karafarin.terminalmanagement.util.date.DateConverterUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository
public class PSPReportRegisterMasterImpl implements PSPReportRegisterMasterDao
{
    @Autowired
    private SqlConnectionConfiguration sqlConnectionConfiguration;

    private JdbcTemplate jdbcTemplate = null;
    private SimpleJdbcCall simpleJdbcCall;

    @PostConstruct
    public void postConstruct()
    {
        if (jdbcTemplate == null)
            jdbcTemplate = sqlConnectionConfiguration.getJdbcTemplate();
    }

    @Override
    public List<PSPReportRegisterMaster> findAll(ParamsReportMaster paramsReportMaster)
            throws SQLException, DataAccessException
    {
        List<PSPReportRegisterMaster> result = new ArrayList<>();
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_ReportRegisterMasterSel")
                      .declareParameters(
                              new SqlParameter("RequestDateFrom", Types.DATE),
                              new SqlParameter("RequestDateTo", Types.DATE),
                              new SqlParameter("ClientNumber", Types.NVARCHAR),
                              new SqlParameter("PSPTrackingCode", Types.NVARCHAR),
                              new SqlParameter("AccountNo", Types.NVARCHAR),
                              new SqlParameter("PSPAcceptanceCode", Types.NVARCHAR),
                              new SqlParameter("CompanyId", Types.BIGINT),
                              new SqlParameter("RequestNumber", Types.BIGINT),
                              new SqlParameter("PSPTerminalCode", Types.NVARCHAR),
                              new SqlParameter("BranchId", Types.BIGINT),
                              new SqlParameter("RequestStatusId", Types.BIGINT));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("RequestDateFrom",
                StringUtils.isNotEmpty(paramsReportMaster.getRequestDateFrom()) ?
                        new java.sql.Date(DateConverterUtil
                                .convertStringWithAndWithoutDelimiterToDate(paramsReportMaster.getRequestDateFrom()).getTime()) : null);
        sqlParameterSource.addValue("RequestDateTo",
                StringUtils.isNotEmpty(paramsReportMaster.getRequestDateTo()) ?
                        new java.sql.Date(DateConverterUtil
                                .convertStringWithAndWithoutDelimiterToDate(paramsReportMaster.getRequestDateTo()).getTime()) : null);
        sqlParameterSource.addValue("ClientNumber",
                StringUtils.isNotEmpty(paramsReportMaster.getClientNumber()) ? paramsReportMaster.getClientNumber() : null);
        sqlParameterSource.addValue("PSPTrackingCode",
                StringUtils.isNotEmpty(paramsReportMaster.getPspTrackingCode()) ? paramsReportMaster.getPspTrackingCode() : null);
        sqlParameterSource.addValue("AccountNo",
                StringUtils.isNotEmpty(paramsReportMaster.getAccountNo()) ? paramsReportMaster.getAccountNo() : null);
        sqlParameterSource.addValue("PSPAcceptanceCode",
                StringUtils.isNotEmpty(paramsReportMaster.getPspAcceptanceCode()) ? paramsReportMaster.getPspAcceptanceCode() : null);
        sqlParameterSource.addValue("CompanyId",
                StringUtils.isNotEmpty(paramsReportMaster.getCompanyId()) ? paramsReportMaster.getCompanyId() : null);
        sqlParameterSource.addValue("RequestNumber",
                StringUtils.isNotEmpty(paramsReportMaster.getRequestNumber()) ? paramsReportMaster.getRequestNumber() : null);
        sqlParameterSource.addValue("PSPTerminalCode",
                StringUtils.isNotEmpty(paramsReportMaster.getPspTerminalCode()) ? paramsReportMaster.getPspTerminalCode() : null);
        sqlParameterSource.addValue("BranchId",
                StringUtils.isNotEmpty(paramsReportMaster.getBranchId()) ? paramsReportMaster.getBranchId() : null);
        sqlParameterSource.addValue("RequestStatusId",
                StringUtils.isNotEmpty(paramsReportMaster.getRequestStatusId()) ? paramsReportMaster.getRequestStatusId() : null);

        Map<String, Object> resultExecuted =
                simpleJdbcCall.returningResultSet("records", new PSPReportRegisterMasterMapper()).
                                                                   execute(sqlParameterSource);
        if (((List) resultExecuted.get("records")).size() > 0)
            result = (List<PSPReportRegisterMaster>) resultExecuted.get("records");

        return result;
    }

}
