package com.karafarin.terminalmanagement.dao.impl.psp;

import com.karafarin.terminalmanagement.config.jdbc.SqlConnectionConfiguration;
import com.karafarin.terminalmanagement.dao.intf.psp.PSPAccountDao;
import com.karafarin.terminalmanagement.dao.mapper.psp.PSPAccountMapper;
import com.karafarin.terminalmanagement.dto.psp.PSPAccount;
import com.karafarin.terminalmanagement.util.UserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository
public class PSPAccountDaoImpl implements PSPAccountDao
{
    @Autowired
    private SqlConnectionConfiguration sqlConnectionConfiguration;

    private JdbcTemplate jdbcTemplate = null;
    private SimpleJdbcCall simpleJdbcCall;

    @PostConstruct
    public void postConstruct()
    {
        if (jdbcTemplate == null)
            jdbcTemplate = sqlConnectionConfiguration.getJdbcTemplate();
    }

    @Override
    public Long add(PSPAccount entity) throws SQLException, DataAccessException
    {
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_pspAccountIns")
                .declareParameters(
                        new SqlParameter("UserCallerId", Types.BIGINT),
                        new SqlParameter("PSPRequestId", Types.BIGINT),
                        new SqlParameter("AccountNo", Types.NVARCHAR),
                        new SqlParameter("AccountType", Types.NVARCHAR),
                        new SqlParameter("AccountDesc", Types.NVARCHAR),
                        new SqlParameter("BranchId", Types.BIGINT),
                        new SqlParameter("IbanNo", Types.NVARCHAR),
                        new SqlParameter("Ip", Types.NVARCHAR),
                        new SqlParameter("ComputerName", Types.NVARCHAR));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("UserCallerId", UserUtil.getCurrentUser().getUserId());
        sqlParameterSource.addValue("PSPRequestId", entity.getPspRequestId());
        sqlParameterSource.addValue("AccountNo", entity.getAccountNo());
        sqlParameterSource.addValue("AccountType", entity.getAccountType());
        sqlParameterSource.addValue("AccountDesc", entity.getAccountDesc());
        sqlParameterSource.addValue("BranchId", entity.getBranchId());
        sqlParameterSource.addValue("IbanNo", entity.getIbanNo());
        sqlParameterSource.addValue("Ip", UserUtil.getCurrentUser().getIp());
        sqlParameterSource.addValue("ComputerName", UserUtil.getCurrentUser().getComputerName());

        Map<String, Object> resultExecuted = simpleJdbcCall.execute(sqlParameterSource);
        return (Long) resultExecuted.get("RETURN_VALUE");
    }

    @Override
    public PSPAccount findById(Long id) throws SQLException, DataAccessException
    {
        PSPAccount result = new PSPAccount();
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_pspAccountSel")
                .declareParameters(
                        new SqlParameter("AccountId", Types.BIGINT));
        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("AccountId", id);

        Map<String, Object> resultExecuted = simpleJdbcCall.
                returningResultSet("records", new PSPAccountMapper()).
                execute(sqlParameterSource);
        if (((List) resultExecuted.get("records")).size() > 0)
            result = ((List<PSPAccount>) resultExecuted.get("records")).get(0);

        return result;
    }

    @Override
    public List<PSPAccount> findAll() throws SQLException, DataAccessException
    {
        List<PSPAccount> result = new ArrayList<>();
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_pspAccountSel")
                .declareParameters(
                        new SqlParameter("AccountId", Types.BIGINT));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("AccountId", -1);

        Map<String, Object> resultExecuted = simpleJdbcCall.
                returningResultSet("records", new PSPAccountMapper()).
                execute(sqlParameterSource);
        if (((List) resultExecuted.get("records")).size() > 0)
            result = (List<PSPAccount>) resultExecuted.get("records");

        return result;
    }

    @Override
    public boolean delete(Long id) throws SQLException, DataAccessException
    {
        return false;
    }

    @Override
    public boolean update(PSPAccount entity) throws SQLException, DataAccessException
    {
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_pspAccountUpd")
                .declareParameters(
                        new SqlParameter("UserCallerId", Types.BIGINT),
                        new SqlParameter("AccountId", Types.BIGINT),
                        new SqlParameter("PSPRequestId", Types.BIGINT),
                        new SqlParameter("AccountNo", Types.NVARCHAR),
                        new SqlParameter("AccountType", Types.NVARCHAR),
                        new SqlParameter("AccountDesc", Types.NVARCHAR),
                        new SqlParameter("BranchId", Types.BIGINT),
                        new SqlParameter("IbanNo", Types.NVARCHAR),
                        new SqlParameter("Ip", Types.NVARCHAR),
                        new SqlParameter("ComputerName", Types.NVARCHAR));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("UserCallerId", UserUtil.getCurrentUser().getUserId());
        sqlParameterSource.addValue("AccountId", entity.getAccountId());
        sqlParameterSource.addValue("PSPRequestId", entity.getPspRequestId());
        sqlParameterSource.addValue("AccountNo", entity.getAccountNo());
        sqlParameterSource.addValue("AccountType", entity.getAccountType());
        sqlParameterSource.addValue("AccountDesc", entity.getAccountDesc());
        sqlParameterSource.addValue("BranchId", entity.getBranchId());
        sqlParameterSource.addValue("IbanNo", entity.getIbanNo());
        sqlParameterSource.addValue("Ip", UserUtil.getCurrentUser().getIp());
        sqlParameterSource.addValue("ComputerName", UserUtil.getCurrentUser().getComputerName());

        Map<String, Object> resultExecuted = simpleJdbcCall.execute(sqlParameterSource);
        return (Integer) resultExecuted.get("#update-count-1") == 1;
    }

    @Override
    public List<PSPAccount> search(PSPAccount entity) throws SQLException, DataAccessException
    {
        return null;
    }
}
