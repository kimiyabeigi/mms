package com.karafarin.terminalmanagement.dao.impl.sys;

import com.karafarin.terminalmanagement.config.jdbc.SqlConnectionConfiguration;
import com.karafarin.terminalmanagement.dao.intf.sys.SysENamadDao;
import com.karafarin.terminalmanagement.dao.mapper.sys.SysENamadMapper;
import com.karafarin.terminalmanagement.dto.sys.SysEnamad;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository
public class SysENamadDaoImpl implements SysENamadDao
{
    @Autowired
    private SqlConnectionConfiguration sqlConnectionConfiguration;

    private JdbcTemplate jdbcTemplate = null;
    private SimpleJdbcCall simpleJdbcCall;

    @PostConstruct
    public void postConstruct()
    {
        if (jdbcTemplate == null) jdbcTemplate = sqlConnectionConfiguration.getJdbcTemplate();
    }

    @Override
    public Long add(SysEnamad entity) throws SQLException, DataAccessException
    {
        return null;
    }

    @Override
    public SysEnamad findById(Long id) throws SQLException, DataAccessException
    {
        SysEnamad result = new SysEnamad();
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_sysENamadSel")
                .declareParameters(
                        new SqlParameter("ENamadId", Types.BIGINT));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("ENamadId", id);

        Map<String, Object> resultExecuted = simpleJdbcCall.
                returningResultSet("records", new SysENamadMapper()).
                execute(sqlParameterSource);
        if (((List) resultExecuted.get("records")).size() > 0)
            result = ((List<SysEnamad>) resultExecuted.get("records")).get(0);

        return result;
    }

    @Override
    public List<SysEnamad> findAll() throws SQLException, DataAccessException
    {
        List<SysEnamad> result = new ArrayList<>();
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_sysENamadSel")
                .declareParameters(
                        new SqlParameter("ENamadId", Types.BIGINT));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("ENamadId", -1);

        Map<String, Object> resultExecuted = simpleJdbcCall.
                returningResultSet("records", new SysENamadMapper()).
                execute(sqlParameterSource);
        if (((List) resultExecuted.get("records")).size() > 0)
            result = (List<SysEnamad>) resultExecuted.get("records");

        return result;
    }

    @Override
    public boolean delete(Long id) throws SQLException, DataAccessException
    {
        return false;
    }

    @Override
    public boolean update(SysEnamad entity) throws SQLException, DataAccessException
    {
        return false;
    }

    @Override
    public List<SysEnamad> search(SysEnamad entity) throws SQLException, DataAccessException
    {
        return null;
    }
}
