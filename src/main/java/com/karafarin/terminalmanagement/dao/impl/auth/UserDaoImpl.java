package com.karafarin.terminalmanagement.dao.impl.auth;

import com.karafarin.terminalmanagement.config.jdbc.SqlConnectionConfiguration;
import com.karafarin.terminalmanagement.dao.intf.auth.UserDao;
import com.karafarin.terminalmanagement.dao.mapper.auth.UserMapper;
import com.karafarin.terminalmanagement.dto.auth.User;
import com.karafarin.terminalmanagement.model.ChangePassword;
import com.karafarin.terminalmanagement.util.UserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository
public class UserDaoImpl implements UserDao
{
    @Autowired
    private SqlConnectionConfiguration sqlConnectionConfiguration;

    private JdbcTemplate jdbcTemplate = null;
    private SimpleJdbcCall simpleJdbcCall;

    @PostConstruct
    public void postConstruct()
    {
        if (jdbcTemplate == null) jdbcTemplate = sqlConnectionConfiguration.getJdbcTemplate();
    }

    @Override
    public Long add(User entity) throws SQLException, DataAccessException
    {
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_UserIns")
                      .declareParameters(new SqlParameter("UserCallerId", Types.BIGINT),
                                         new SqlParameter("UserCode", Types.NVARCHAR),
                                         new SqlParameter("FirstName", Types.NVARCHAR),
                                         new SqlParameter("LastName", Types.NVARCHAR),
                                         new SqlParameter("UserName", Types.NVARCHAR),
                                         new SqlParameter("Password", Types.NVARCHAR),
                                         new SqlParameter("RoleId", Types.BIGINT),
                                         new SqlParameter("BranchId", Types.BIGINT),
                                         new SqlParameter("IsActive", Types.BIT),
                                         new SqlParameter("Ip", Types.NVARCHAR),
                                         new SqlParameter("ComputerName", Types.NVARCHAR));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("UserCallerId", UserUtil.getCurrentUser().getUserId());
        sqlParameterSource.addValue("UserCode", entity.getUserCode());
        sqlParameterSource.addValue("FirstName", entity.getFirstName());
        sqlParameterSource.addValue("LastName", entity.getLastName());
        sqlParameterSource.addValue("UserName", entity.getUserName());
        sqlParameterSource.addValue("Password", UserUtil.getPasswordEncoded(entity.getPassword()));
        sqlParameterSource.addValue("RoleId", entity.getRoleId());
        sqlParameterSource.addValue("BranchId", entity.getBranchId());
        sqlParameterSource.addValue("IsActive", entity.isActive());
        sqlParameterSource.addValue("Ip", UserUtil.getCurrentUser().getIp());
        sqlParameterSource.addValue("ComputerName", UserUtil.getCurrentUser().getComputerName());

        Map<String, Object> resultExecuted = simpleJdbcCall.execute(sqlParameterSource);
        return (Long) resultExecuted.get("RETURN_VALUE");
    }

    @Override
    public User findById(Long id) throws SQLException, DataAccessException
    {
        User result = new User();
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_UserSel").declareParameters(new SqlParameter("UserId", Types.BIGINT));
        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("UserId", id);

        Map<String, Object> resultExecuted = simpleJdbcCall.
                                                                   returningResultSet("records", new UserMapper()).
                                                                   execute(sqlParameterSource);
        if (((List) resultExecuted.get("records")).size() > 0)
            result = ((List<User>) resultExecuted.get("records")).get(0);

        return result;
    }

    @Override
    public List<User> findAll() throws SQLException, DataAccessException
    {
        List<User> result = new ArrayList<>();
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_UserSel").declareParameters(new SqlParameter("UserId", Types.BIGINT));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("UserId", -1);

        Map<String, Object> resultExecuted = simpleJdbcCall.returningResultSet("records", new UserMapper()).
                execute(sqlParameterSource);
        if (((List) resultExecuted.get("records")).size() > 0) result = (List<User>) resultExecuted.get("records");

        return result;
    }

    @Override
    public List<User> findAllUsers() throws SQLException, DataAccessException
    {
        List<User> result = new ArrayList<>();
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_UserSel").declareParameters(new SqlParameter("UserId", Types.BIGINT));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("UserId", 0);

        Map<String, Object> resultExecuted = simpleJdbcCall.returningResultSet("records", new UserMapper()).
                execute(sqlParameterSource);
        if (((List) resultExecuted.get("records")).size() > 0) result = (List<User>) resultExecuted.get("records");

        return result;
    }

    @Override
    public boolean delete(Long id) throws SQLException, DataAccessException
    {
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_UserDel")
                      .declareParameters(new SqlParameter("UserCallerId", Types.BIGINT),
                                         new SqlParameter("UserId", Types.BIGINT),
                                         new SqlParameter("Ip", Types.NVARCHAR),
                                         new SqlParameter("ComputerName", Types.NVARCHAR));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("UserCallerId", UserUtil.getCurrentUser().getUserId());
        sqlParameterSource.addValue("UserId", id);
        sqlParameterSource.addValue("Ip", UserUtil.getCurrentUser().getIp());
        sqlParameterSource.addValue("ComputerName", UserUtil.getCurrentUser().getComputerName());

        Map<String, Object> resultExecuted = simpleJdbcCall.execute(sqlParameterSource);
        return (Integer) resultExecuted.get("#update-count-1") == 1;
    }

    @Override
    public boolean update(User entity) throws SQLException, DataAccessException
    {
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_UserUpd")
                      .declareParameters(new SqlParameter("UserCallerId", Types.BIGINT),
                                         new SqlParameter("UserId", Types.BIGINT),
                                         new SqlParameter("UserCode", Types.NVARCHAR),
                                         new SqlParameter("FirstName", Types.NVARCHAR),
                                         new SqlParameter("LastName", Types.NVARCHAR),
                                         new SqlParameter("UserName", Types.NVARCHAR),
                                         new SqlParameter("RoleId", Types.BIGINT),
                                         new SqlParameter("BranchId", Types.BIGINT),
                                         new SqlParameter("IsActive", Types.BIT),
                                         new SqlParameter("Ip", Types.NVARCHAR),
                                         new SqlParameter("ComputerName", Types.NVARCHAR));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("UserCallerId", UserUtil.getCurrentUser().getUserId());
        sqlParameterSource.addValue("UserId", entity.getUserId());
        sqlParameterSource.addValue("UserCode", entity.getUserCode());
        sqlParameterSource.addValue("FirstName", entity.getFirstName());
        sqlParameterSource.addValue("LastName", entity.getLastName());
        sqlParameterSource.addValue("UserName", entity.getUserName());
        sqlParameterSource.addValue("RoleId", entity.getRoleId());
        sqlParameterSource.addValue("BranchId", entity.getBranchId());
        sqlParameterSource.addValue("IsActive", entity.isActive());
        sqlParameterSource.addValue("Ip", UserUtil.getCurrentUser().getIp());
        sqlParameterSource.addValue("ComputerName", UserUtil.getCurrentUser().getComputerName());

        Map<String, Object> resultExecuted = simpleJdbcCall.execute(sqlParameterSource);
        return (Integer) resultExecuted.get("#update-count-1") == 1;
    }

    @Override
    public List<User> search(User entity) throws SQLException, DataAccessException
    {
        return null;
    }

    @Override
    public User findUserByUserName(String userName) throws SQLException, DataAccessException
    {
        User result;
        try
        {
            result = jdbcTemplate.queryForObject("SELECT UserId, UserCode, FirstName, LastName, UserName, Password, RoleId," +
                                                 "            BranchId, IsActive, FirstLogin " + " FROM [dbo].[tbl_User] " +
                                                 " WHERE [UserName] = '" + userName + "'", new UserMapper());
        } catch (EmptyResultDataAccessException e)
        {
            result = null;
        }

        return result;
    }

    @Override
    public boolean changePassword(ChangePassword changePassword, boolean isAdmin) throws SQLException,
                                                                                                  DataAccessException
    {
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_UserChangePasswordUpd")
                      .declareParameters(new SqlParameter("UserCallerId", Types.BIGINT),
                                         new SqlParameter("UserId", Types.BIGINT),
                                         new SqlParameter("NewPassword", Types.NVARCHAR),
                                         new SqlParameter("OldPassword", Types.NVARCHAR),
                                         new SqlParameter("FirstLogin", Types.BIT),
                                         new SqlParameter("IsAdmin", Types.BIT),
                                         new SqlParameter("Ip", Types.NVARCHAR),
                                         new SqlParameter("ComputerName", Types.NVARCHAR));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("UserCallerId", UserUtil.getCurrentUser().getUserId());
        sqlParameterSource.addValue("UserId", changePassword.getUserId());
        sqlParameterSource.addValue("NewPassword", UserUtil.getPasswordEncoded(changePassword.getNewPassword()));
        sqlParameterSource.addValue("OldPassword", UserUtil.getPasswordEncoded(changePassword.getOldPassword()));
        sqlParameterSource.addValue("FirstLogin", 0);
        sqlParameterSource.addValue("IsAdmin", isAdmin ? 1 : 0);
        sqlParameterSource.addValue("Ip", UserUtil.getCurrentUser().getIp());
        sqlParameterSource.addValue("ComputerName", UserUtil.getCurrentUser().getComputerName());

        Map<String, Object> resultExecuted = simpleJdbcCall.execute(sqlParameterSource);
        return (Integer) resultExecuted.get("#update-count-1") == 1;
    }
}
