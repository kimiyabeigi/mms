package com.karafarin.terminalmanagement.dao.impl.psp;

import com.karafarin.terminalmanagement.config.jdbc.SqlConnectionConfiguration;
import com.karafarin.terminalmanagement.dao.intf.psp.PSPRequestStatusDao;
import com.karafarin.terminalmanagement.dao.mapper.psp.PSPRequestStatusMapper;
import com.karafarin.terminalmanagement.dto.psp.PSPRequestStatus;
import com.karafarin.terminalmanagement.model.CurrentUser;
import com.karafarin.terminalmanagement.util.UserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository
public class PSPRequestStatusDaoImpl implements PSPRequestStatusDao
{
    @Autowired
    private SqlConnectionConfiguration sqlConnectionConfiguration;

    private JdbcTemplate jdbcTemplate = null;
    private SimpleJdbcCall simpleJdbcCall;

    @PostConstruct
    public void postConstruct()
    {
        if (jdbcTemplate == null) jdbcTemplate = sqlConnectionConfiguration.getJdbcTemplate();
    }

    @Override
    public Long add(PSPRequestStatus entity) throws SQLException, DataAccessException
    {
        CurrentUser currentUser = UserUtil.getCurrentUser();
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_pspRequestStatusIns")
                      .declareParameters(new SqlParameter("UserCallerId", Types.BIGINT),
                                         new SqlParameter("PSPRequestId", Types.BIGINT),
                                         new SqlParameter("RequestStatusId", Types.BIGINT),
                                         new SqlParameter("StatusTypeId", Types.BIGINT),
                                         new SqlParameter("ServiceId", Types.BIGINT),
                                         new SqlParameter("UserId", Types.BIGINT),
                                         new SqlParameter("BranchId", Types.BIGINT),
                                         new SqlParameter("PSPErrorCode", Types.NVARCHAR),
                                         new SqlParameter("PSPErrorDesc", Types.NVARCHAR),
                                         new SqlParameter("Status", Types.NVARCHAR),
                                         new SqlParameter("Ip", Types.NVARCHAR),
                                         new SqlParameter("ComputerName", Types.NVARCHAR));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("UserCallerId", currentUser.getUserId());
        sqlParameterSource.addValue("PSPRequestId", entity.getPspRequestId());
        sqlParameterSource.addValue("RequestStatusId", entity.getRequestStatusId());
        sqlParameterSource.addValue("StatusTypeId", entity.getStatusTypeId());
        sqlParameterSource.addValue("ServiceId", entity.getServiceId());
        sqlParameterSource.addValue("UserId", currentUser.getUserId());
        sqlParameterSource.addValue("BranchId", currentUser.getBranchId());
        sqlParameterSource.addValue("PSPErrorCode", entity.getPspErrorCode());
        sqlParameterSource.addValue("PSPErrorDesc", entity.getPspErrorDesc());
        sqlParameterSource.addValue("Status", entity.getStatus());
        sqlParameterSource.addValue("Ip", currentUser.getIp());
        sqlParameterSource.addValue("ComputerName", currentUser.getComputerName());

        Map<String, Object> resultExecuted = simpleJdbcCall.execute(sqlParameterSource);
        return (Long) resultExecuted.get("RETURN_VALUE");
    }

    @Override
    public PSPRequestStatus findById(Long id) throws SQLException, DataAccessException
    {
        PSPRequestStatus result = new PSPRequestStatus();
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_pspRequestStatusSel")
                      .declareParameters(new SqlParameter("PSPRequestStatusId", Types.BIGINT));
        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("PSPRequestStatusId", id);

        Map<String, Object> resultExecuted = simpleJdbcCall.
                                                                   returningResultSet("records",
                                                                                      new PSPRequestStatusMapper()).
                                                                   execute(sqlParameterSource);
        if (((List) resultExecuted.get("records")).size() > 0)
            result = ((List<PSPRequestStatus>) resultExecuted.get("records")).get(0);

        return result;
    }

    @Override
    public List<PSPRequestStatus> findAll() throws SQLException, DataAccessException
    {
        List<PSPRequestStatus> result = new ArrayList<>();
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_pspRequestStatusSel")
                      .declareParameters(new SqlParameter("PSPRequestStatusId", Types.BIGINT));
        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("PSPRequestStatusId", "");

        Map<String, Object> resultExecuted = simpleJdbcCall.
                                                                   returningResultSet("records",
                                                                                      new PSPRequestStatusMapper()).
                                                                   execute(sqlParameterSource);
        if (((List) resultExecuted.get("records")).size() > 0)
            result = (List<PSPRequestStatus>) resultExecuted.get("records");

        return result;
    }

    @Override
    public boolean delete(Long id) throws SQLException, DataAccessException
    {
        return false;
    }

    @Override
    public boolean update(PSPRequestStatus entity) throws SQLException, DataAccessException
    {
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_pspRequestStatusUpd")
                      .declareParameters(
                              new SqlParameter("UserCallerId", Types.BIGINT),
                              new SqlParameter("PSPRequestStatusId", Types.BIGINT),
                              new SqlParameter("PSPRequestId", Types.BIGINT),
                              new SqlParameter("RequestStatusId", Types.BIGINT),
                              new SqlParameter("StatusTypeId", Types.BIGINT),
                              new SqlParameter("ServiceId", Types.BIGINT),
                              new SqlParameter("PSPErrorCode", Types.NVARCHAR),
                              new SqlParameter("PSPErrorDesc", Types.NVARCHAR),
                              new SqlParameter("Status", Types.NVARCHAR),
                              new SqlParameter("Ip", Types.NVARCHAR),
                              new SqlParameter("ComputerName", Types.NVARCHAR));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("UserCallerId", UserUtil.getCurrentUser().getUserId());
        sqlParameterSource.addValue("PSPRequestStatusId", entity.getPspRequestStatusId());
        sqlParameterSource.addValue("PSPRequestId", entity.getPspRequestId());
        sqlParameterSource.addValue("RequestStatusId", entity.getRequestStatusId());
        sqlParameterSource.addValue("StatusTypeId", entity.getStatusTypeId());
        sqlParameterSource.addValue("ServiceId", entity.getServiceId());
        sqlParameterSource.addValue("PSPErrorCode", entity.getPspErrorCode());
        sqlParameterSource.addValue("PSPErrorDesc", entity.getPspErrorDesc());
        sqlParameterSource.addValue("Status", entity.getStatus());
        sqlParameterSource.addValue("Ip", UserUtil.getCurrentUser().getIp());
        sqlParameterSource.addValue("ComputerName", UserUtil.getCurrentUser().getComputerName());

        Map<String, Object> resultExecuted = simpleJdbcCall.execute(sqlParameterSource);
        return (Integer) resultExecuted.get("#update-count-1") == 1;
    }

    @Override
    public List<PSPRequestStatus> search(PSPRequestStatus entity) throws SQLException, DataAccessException
    {
        return null;
    }

    @Override
    public List<PSPRequestStatus> findByPSPRequestId(Long id) throws SQLException, DataAccessException
    {
        List<PSPRequestStatus> result = new ArrayList<>();
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_pspRequestStatusByPSPRequestIDSel")
                      .declareParameters(new SqlParameter("PSPRequestId", Types.BIGINT));
        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("PSPRequestId", "");

        Map<String, Object> resultExecuted = simpleJdbcCall.
                                                                   returningResultSet("records",
                                                                                      new PSPRequestStatusMapper()).
                                                                   execute(sqlParameterSource);
        if (((List) resultExecuted.get("records")).size() > 0)
            result = (List<PSPRequestStatus>) resultExecuted.get("records");

        return result;
    }

    @Override
    public boolean updateRequestStatusIdById(Long id, Long requestStatusId) throws SQLException, DataAccessException
    {
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_pspRequestStatusIdUpd")
                      .declareParameters(
                              new SqlParameter("UserCallerId", Types.BIGINT),
                              new SqlParameter("PSPRequestStatusId", Types.BIGINT),
                              new SqlParameter("RequestStatusId", Types.BIGINT),
                              new SqlParameter("Ip", Types.NVARCHAR),
                              new SqlParameter("ComputerName", Types.NVARCHAR));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("UserCallerId", UserUtil.getCurrentUser().getUserId());
        sqlParameterSource.addValue("PSPRequestStatusId", id);
        sqlParameterSource.addValue("RequestStatusId", requestStatusId);
        sqlParameterSource.addValue("Ip", UserUtil.getCurrentUser().getIp());
        sqlParameterSource.addValue("ComputerName", UserUtil.getCurrentUser().getComputerName());

        Map<String, Object> resultExecuted = simpleJdbcCall.execute(sqlParameterSource);
        return (Integer) resultExecuted.get("#update-count-1") == 1;
    }

    @Override
    public boolean updateStatusById(Long id, String status) throws SQLException, DataAccessException
    {
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_pspStatusUpd")
                      .declareParameters(
                              new SqlParameter("UserCallerId", Types.BIGINT),
                              new SqlParameter("PSPRequestStatusId", Types.BIGINT),
                              new SqlParameter("Status", Types.NVARCHAR),
                              new SqlParameter("Ip", Types.NVARCHAR),
                              new SqlParameter("ComputerName", Types.NVARCHAR));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("UserCallerId", UserUtil.getCurrentUser().getUserId());
        sqlParameterSource.addValue("PSPRequestStatusId", id);
        sqlParameterSource.addValue("Status", status);
        sqlParameterSource.addValue("Ip", UserUtil.getCurrentUser().getIp());
        sqlParameterSource.addValue("ComputerName", UserUtil.getCurrentUser().getComputerName());

        Map<String, Object> resultExecuted = simpleJdbcCall.execute(sqlParameterSource);
        return (Integer) resultExecuted.get("#update-count-1") == 1;
    }
}
