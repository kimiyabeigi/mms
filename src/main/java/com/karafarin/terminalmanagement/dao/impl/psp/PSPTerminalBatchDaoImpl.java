package com.karafarin.terminalmanagement.dao.impl.psp;

import com.karafarin.terminalmanagement.config.jdbc.SqlConnectionConfiguration;
import com.karafarin.terminalmanagement.dao.intf.psp.PSPTerminalBatchDao;
import com.karafarin.terminalmanagement.dao.mapper.psp.PSPTerminalBatchMapper;
import com.karafarin.terminalmanagement.dto.psp.PSPTerminalBatch;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository
public class PSPTerminalBatchDaoImpl implements PSPTerminalBatchDao
{
    @Autowired
    private SqlConnectionConfiguration sqlConnectionConfiguration;

    private JdbcTemplate jdbcTemplate = null;
    private SimpleJdbcCall simpleJdbcCall;

    @PostConstruct
    public void postConstruct()
    {
        if (jdbcTemplate == null) jdbcTemplate = sqlConnectionConfiguration.getJdbcTemplate();
    }

    @Override
    public Long add(PSPTerminalBatch entity) throws SQLException, DataAccessException
    {
        return null;
    }

    @Override
    public PSPTerminalBatch findById(Long id) throws SQLException, DataAccessException
    {
        return null;
    }

    @Override
    public List<PSPTerminalBatch> findAll() throws SQLException, DataAccessException
    {
        List<PSPTerminalBatch> result = new ArrayList<>();
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_pspTerminalRegisterSel");

        Map<String, Object> resultExecuted =
                simpleJdbcCall.returningResultSet("records", new PSPTerminalBatchMapper()).execute();
        if (((List) resultExecuted.get("records")).size() > 0)
            result = (List<PSPTerminalBatch>) resultExecuted.get("records");

        return result;
    }

    @Override
    public boolean delete(Long id) throws SQLException, DataAccessException
    {
        return false;
    }

    @Override
    public boolean update(PSPTerminalBatch entity) throws SQLException, DataAccessException
    {
        return false;
    }

    @Override
    public List<PSPTerminalBatch> search(PSPTerminalBatch entity) throws SQLException, DataAccessException
    {
        return null;
    }

    @Override
    public PSPTerminalBatch findByPSPTrackingCode(String pspTrackingCode) throws SQLException, DataAccessException
    {
        PSPTerminalBatch result = new PSPTerminalBatch();
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_pspRequestByPSPTrackingCodeSel")
                      .declareParameters(
                              new SqlParameter("PSPTrackingCode", Types.NVARCHAR));
        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("PSPTrackingCode", pspTrackingCode);

        Map<String, Object> resultExecuted =
                simpleJdbcCall.returningResultSet("records", new PSPTerminalBatchMapper()).
                        execute(sqlParameterSource);
        if (((List) resultExecuted.get("records")).size() > 0)
            result = ((List<PSPTerminalBatch>) resultExecuted.get("records")).get(0);

        return result;
    }

    @Override
    public PSPTerminalBatch findRegisterByPSPTrackingCode(String pspTrackingCode) throws SQLException,
                                                                                         DataAccessException
    {
        PSPTerminalBatch result = new PSPTerminalBatch();
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_pspRegisterByPSPTrackingCodeSel")
                      .declareParameters(
                              new SqlParameter("PSPTrackingCode", Types.NVARCHAR));
        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("PSPTrackingCode", pspTrackingCode);

        Map<String, Object> resultExecuted =
                simpleJdbcCall.returningResultSet("records", new PSPTerminalBatchMapper()).
                        execute(sqlParameterSource);
        if (((List) resultExecuted.get("records")).size() > 0)
            result = ((List<PSPTerminalBatch>) resultExecuted.get("records")).get(0);

        return result;
    }
}
