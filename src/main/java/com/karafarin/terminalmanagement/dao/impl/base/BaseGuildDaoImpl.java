package com.karafarin.terminalmanagement.dao.impl.base;

import com.karafarin.terminalmanagement.config.jdbc.SqlConnectionConfiguration;
import com.karafarin.terminalmanagement.dao.intf.base.BaseGuildDao;
import com.karafarin.terminalmanagement.dao.mapper.base.BaseGuildMapper;
import com.karafarin.terminalmanagement.dto.base.BaseGuild;
import com.karafarin.terminalmanagement.util.UserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository
public class BaseGuildDaoImpl implements BaseGuildDao
{
    @Autowired
    private SqlConnectionConfiguration sqlConnectionConfiguration;

    private JdbcTemplate jdbcTemplate = null;
    private SimpleJdbcCall simpleJdbcCall;

    @PostConstruct
    public void postConstruct()
    {
        if(jdbcTemplate == null)
            jdbcTemplate = sqlConnectionConfiguration.jdbcTemplate;
    }


    @Override
    public Long add(BaseGuild entity) throws SQLException, DataAccessException {

        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_BaseGuildIns")
                .declareParameters(
                        new SqlParameter("UserCallerId", Types.BIGINT),
                        new SqlParameter("GuildId", Types.BIGINT),
                        new SqlParameter("GuildCode", Types.NVARCHAR),
                        new SqlParameter("Description", Types.NVARCHAR),
                        new SqlParameter("IsActive",Types.BIT),
                        new SqlParameter("Ip", Types.NVARCHAR),
                        new SqlParameter("ComputerName", Types.NVARCHAR));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("UserCallerId", UserUtil.getCurrentUser().getUserId());
        sqlParameterSource.addValue("GuildId",entity.getGuildId());
        sqlParameterSource.addValue("GuildCode",entity.getGuildCode());
        sqlParameterSource.addValue("Description",entity.getDescription());
        sqlParameterSource.addValue("IsActive",entity.isActive());
        sqlParameterSource.addValue("Ip", UserUtil.getCurrentUser().getIp());
        sqlParameterSource.addValue("ComputerName", UserUtil.getCurrentUser().getComputerName());

        Map<String,Object> resulExecuted = simpleJdbcCall.execute(sqlParameterSource);
        return (Long) resulExecuted.get("RETURN_VALUE");

    }

    @Override
    public BaseGuild findById(Long id) throws SQLException, DataAccessException {

        BaseGuild result = new BaseGuild();
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_BaseGuildSel")
                .declareParameters(
                        new SqlParameter("GuildId", Types.BIGINT),
                        new SqlParameter("All", Types.BIT));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("GuildId", id);
        sqlParameterSource.addValue("All", 1);

        Map<String, Object> resultExecuted = simpleJdbcCall.
                returningResultSet("records", new BaseGuildMapper()).
                execute(sqlParameterSource);
        if (((List) resultExecuted.get("records")).size() > 0)
            result = ((List<BaseGuild>) resultExecuted.get("records")).get(0);

        return result;

    }

    @Override
    public List<BaseGuild> findAll() throws SQLException, DataAccessException
    {
        return null;
    }

    @Override
    public List<BaseGuild> findAll(boolean all) throws SQLException, DataAccessException {

        List<BaseGuild> result = new ArrayList<>();
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_BaseGuildSel")
                .declareParameters(
                        new SqlParameter("GuildId", Types.BIGINT),
                        new SqlParameter("All", Types.BIT));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("GuildId", -1);
        sqlParameterSource.addValue("All", all);

        Map<String, Object> resultExecuted = simpleJdbcCall.
                returningResultSet("records", new BaseGuildMapper()).
                execute(sqlParameterSource);
        if (((List) resultExecuted.get("records")).size() > 0)
            result = (List<BaseGuild>) resultExecuted.get("records");

        return result;
    }

    @Override
    public boolean delete(Long id) throws SQLException, DataAccessException {

        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_BaseGuildDel")
                .declareParameters(
                        new SqlParameter("UserCallerId", Types.BIGINT),
                        new SqlParameter("GuildId", Types.BIGINT),
                        new SqlParameter("Ip", Types.NVARCHAR),
                        new SqlParameter("ComputerName", Types.NVARCHAR));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("UserCallerId", UserUtil.getCurrentUser().getUserId());
        sqlParameterSource.addValue("GuildId",id);
        sqlParameterSource.addValue("Ip", UserUtil.getCurrentUser().getIp());
        sqlParameterSource.addValue("ComputerName", UserUtil.getCurrentUser().getComputerName());

        Map<String, Object> resultExecuted = simpleJdbcCall.execute(sqlParameterSource);
        return (Integer) resultExecuted.get("#update-count-1") == 1;
    }

    @Override
    public boolean update(BaseGuild entity) throws SQLException, DataAccessException
    {

        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_BaseGuildUpd")
                .declareParameters(
                        new SqlParameter("UserCallerId", Types.BIGINT),
                        new SqlParameter("GuildId", Types.BIGINT),
                        new SqlParameter("GuildCode", Types.NVARCHAR),
                        new SqlParameter("Description", Types.NVARCHAR),
                        new SqlParameter("IsActive", Types.BIT),
                        new SqlParameter("Ip", Types.NVARCHAR),
                        new SqlParameter("ComputerName", Types.NVARCHAR));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("UserCallerId", UserUtil.getCurrentUser().getUserId());
        sqlParameterSource.addValue("GuildId", entity.getGuildId());
        sqlParameterSource.addValue("GuildCode", entity.getGuildCode());
        sqlParameterSource.addValue("Description", entity.getDescription());
        sqlParameterSource.addValue("IsActive", entity.isActive());
        sqlParameterSource.addValue("Ip", UserUtil.getCurrentUser().getIp());
        sqlParameterSource.addValue("ComputerName", UserUtil.getCurrentUser().getComputerName());

        Map<String, Object> resultExecuted = simpleJdbcCall.execute(sqlParameterSource);
        return (Integer) resultExecuted.get("#update-count-1") == 1;

    }

    @Override
    public List<BaseGuild> search(BaseGuild entity) throws SQLException, DataAccessException
    {
        return null;
    }

}
