package com.karafarin.terminalmanagement.dao.impl.psp;

import com.karafarin.terminalmanagement.config.jdbc.SqlConnectionConfiguration;
import com.karafarin.terminalmanagement.dao.intf.psp.PSPRequestDao;
import com.karafarin.terminalmanagement.dao.mapper.psp.PSPRequestMapper;
import com.karafarin.terminalmanagement.dto.psp.PSPRequest;
import com.karafarin.terminalmanagement.util.UserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository
public class PSPRequestDaoImpl implements PSPRequestDao
{
    @Autowired
    private SqlConnectionConfiguration sqlConnectionConfiguration;

    private JdbcTemplate jdbcTemplate = null;
    private SimpleJdbcCall simpleJdbcCall;

    @PostConstruct
    public void postConstruct()
    {
        if (jdbcTemplate == null)
            jdbcTemplate = sqlConnectionConfiguration.getJdbcTemplate();
    }

    @Override
    public Long add(PSPRequest entity) throws SQLException, DataAccessException
    {
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_pspRequestIns")
                .declareParameters(
                        new SqlParameter("UserCallerId", Types.BIGINT),
                        new SqlParameter("CompanyId", Types.BIGINT),
                        new SqlParameter("TerminalId", Types.BIGINT),
                        new SqlParameter("RequestStatusId", Types.BIGINT),
                        new SqlParameter("PSPAcceptanceCode", Types.NVARCHAR),
                        new SqlParameter("PSPTerminalCode", Types.NVARCHAR),
                        new SqlParameter("PSPTrackingCode", Types.NVARCHAR),
                        new SqlParameter("IPAddress", Types.NVARCHAR),
                        new SqlParameter("ComputerName", Types.NVARCHAR),
                        new SqlParameter("UserId", Types.BIGINT),
                        new SqlParameter("BranchId", Types.BIGINT),
                        new SqlParameter("Ip", Types.NVARCHAR));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("UserCallerId", UserUtil.getCurrentUser().getUserId());
        sqlParameterSource.addValue("CompanyId", entity.getCompanyId());
        sqlParameterSource.addValue("TerminalId", entity.getTerminalId());
        sqlParameterSource.addValue("RequestStatusId", entity.getRequestStatusId());
        sqlParameterSource.addValue("PSPAcceptanceCode", entity.getPspAcceptanceCode());
        sqlParameterSource.addValue("PSPTerminalCode", entity.getPspTerminalCode());
        sqlParameterSource.addValue("PSPTrackingCode", entity.getPspTrackingCode());
        sqlParameterSource.addValue("IPAddress", UserUtil.getCurrentUser().getIp());
        sqlParameterSource.addValue("ComputerName", UserUtil.getCurrentUser().getComputerName());
        sqlParameterSource.addValue("UserId", entity.getUserId());
        sqlParameterSource.addValue("BranchId", UserUtil.getCurrentUser().getBranchId());
        sqlParameterSource.addValue("Ip", UserUtil.getCurrentUser().getIp());

        Map<String, Object> resultExecuted = simpleJdbcCall.execute(sqlParameterSource);
        return (Long) resultExecuted.get("RETURN_VALUE");
    }

    @Override
    public PSPRequest findById(Long id) throws SQLException, DataAccessException
    {
        PSPRequest result = new PSPRequest();
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_pspRequestSel")
                .declareParameters(
                        new SqlParameter("PSPRequestId", Types.BIGINT));
        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("PSPRequestId", id);

        Map<String, Object> resultExecuted = simpleJdbcCall.
                returningResultSet("records", new PSPRequestMapper()).
                execute(sqlParameterSource);
        if (((List) resultExecuted.get("records")).size() > 0)
            result = ((List<PSPRequest>) resultExecuted.get("records")).get(0);

        return result;
    }

    @Override
    public List<PSPRequest> findAll() throws SQLException, DataAccessException
    {
        List<PSPRequest> result = new ArrayList<>();
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_pspRequestSel")
                .declareParameters(
                        new SqlParameter("PSPRequestId", Types.BIGINT));
        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("PSPRequestId", "");

        Map<String, Object> resultExecuted = simpleJdbcCall.
                returningResultSet("records", new PSPRequestMapper()).
                execute(sqlParameterSource);
        if (((List) resultExecuted.get("records")).size() > 0)
            result = (List<PSPRequest>) resultExecuted.get("records");

        return result;
    }

    @Override
    public boolean delete(Long id) throws SQLException, DataAccessException
    {
        return false;
    }

    @Override
    public boolean update(PSPRequest entity) throws SQLException, DataAccessException
    {
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_pspRequestUpd")
                .declareParameters(
                        new SqlParameter("UserCallerId", Types.BIGINT),
                        new SqlParameter("PSPRequestId", Types.BIGINT),
                        new SqlParameter("CompanyId", Types.BIGINT),
                        new SqlParameter("TerminalId", Types.BIGINT),
                        new SqlParameter("RequestStatusId", Types.BIGINT),
                        new SqlParameter("PSPAcceptanceCode", Types.NVARCHAR),
                        new SqlParameter("PSPTerminalCode", Types.NVARCHAR),
                        new SqlParameter("PSPTrackingCode", Types.NVARCHAR),
                        new SqlParameter("IPAddress", Types.NVARCHAR),
                        new SqlParameter("ComputerName", Types.NVARCHAR),
                        new SqlParameter("UserId", Types.NVARCHAR),
                        new SqlParameter("BranchId", Types.NVARCHAR),
                        new SqlParameter("Ip", Types.NVARCHAR));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("UserCallerId", UserUtil.getCurrentUser().getUserId());
        sqlParameterSource.addValue("PSPRequestId", entity.getPspRequestId());
        sqlParameterSource.addValue("CompanyId", entity.getCompanyId());
        sqlParameterSource.addValue("TerminalId", entity.getTerminalId());
        sqlParameterSource.addValue("RequestStatusId", entity.getRequestStatusId());
        sqlParameterSource.addValue("PSPAcceptanceCode", entity.getPspAcceptanceCode());
        sqlParameterSource.addValue("PSPTerminalCode", entity.getPspTerminalCode());
        sqlParameterSource.addValue("PSPTrackingCode", entity.getPspTrackingCode());
        sqlParameterSource.addValue("IPAddress", entity.getIpAddress());
        sqlParameterSource.addValue("ComputerName", entity.getComputerName());
        sqlParameterSource.addValue("UserId", UserUtil.getCurrentUser().getIp());
        sqlParameterSource.addValue("BranchId", UserUtil.getCurrentUser().getBranchId());
        sqlParameterSource.addValue("Ip", UserUtil.getCurrentUser().getIp());

        Map<String, Object> resultExecuted = simpleJdbcCall.execute(sqlParameterSource);
        return (Integer) resultExecuted.get("#update-count-1") == 1;
    }

    @Override
    public List<PSPRequest> search(PSPRequest entity) throws SQLException, DataAccessException
    {
        return null;
    }

    @Override
    public boolean updateTrackingCode(Long id, String trackingCode) throws SQLException, DataAccessException
    {
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_pspRequestTrackingCodeUpd")
                      .declareParameters(
                              new SqlParameter("UserCallerId", Types.BIGINT),
                              new SqlParameter("PSPRequestId", Types.BIGINT),
                              new SqlParameter("PSPTrackingCode", Types.NVARCHAR),
                              new SqlParameter("ComputerName", Types.NVARCHAR),
                              new SqlParameter("UserId", Types.NVARCHAR),
                              new SqlParameter("Ip", Types.NVARCHAR));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("UserCallerId", UserUtil.getCurrentUser().getUserId());
        sqlParameterSource.addValue("PSPRequestId", id);
        sqlParameterSource.addValue("PSPTrackingCode", trackingCode);
        sqlParameterSource.addValue("ComputerName", UserUtil.getCurrentUser().getComputerName());
        sqlParameterSource.addValue("UserId", UserUtil.getCurrentUser().getUserId());
        sqlParameterSource.addValue("Ip", UserUtil.getCurrentUser().getIp());

        Map<String, Object> resultExecuted = simpleJdbcCall.execute(sqlParameterSource);
        return (Integer) resultExecuted.get("#update-count-1") == 1;
    }

    @Override
    public boolean updateRequestStatusId(Long id, Long requestStatusId) throws SQLException, DataAccessException
    {
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_pspRequestRequestStatusIdUpd")
                      .declareParameters(
                              new SqlParameter("UserCallerId", Types.BIGINT),
                              new SqlParameter("PSPRequestId", Types.BIGINT),
                              new SqlParameter("RequestStatusId", Types.NVARCHAR),
                              new SqlParameter("ComputerName", Types.NVARCHAR),
                              new SqlParameter("UserId", Types.NVARCHAR),
                              new SqlParameter("Ip", Types.NVARCHAR));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("UserCallerId", UserUtil.getCurrentUser().getUserId());
        sqlParameterSource.addValue("PSPRequestId", id);
        sqlParameterSource.addValue("RequestStatusId", requestStatusId);
        sqlParameterSource.addValue("ComputerName", UserUtil.getCurrentUser().getComputerName());
        sqlParameterSource.addValue("UserId", UserUtil.getCurrentUser().getUserId());
        sqlParameterSource.addValue("Ip", UserUtil.getCurrentUser().getIp());

        Map<String, Object> resultExecuted = simpleJdbcCall.execute(sqlParameterSource);
        return (Integer) resultExecuted.get("#update-count-1") == 1;
    }

    @Override
    public boolean updateTerminalById(Long id, String pspAcceptanceCode, String pspTerminalCode) throws SQLException,
                                                                                                        DataAccessException
    {
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_pspRequestTerminalUpd")
                      .declareParameters(
                              new SqlParameter("UserCallerId", Types.BIGINT),
                              new SqlParameter("PSPRequestId", Types.BIGINT),
                              new SqlParameter("PSPAcceptanceCode", Types.NVARCHAR),
                              new SqlParameter("PSPTerminalCode", Types.NVARCHAR),
                              new SqlParameter("ComputerName", Types.NVARCHAR),
                              new SqlParameter("UserId", Types.NVARCHAR),
                              new SqlParameter("Ip", Types.NVARCHAR));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("UserCallerId", UserUtil.getCurrentUser().getUserId());
        sqlParameterSource.addValue("PSPRequestId", id);
        sqlParameterSource.addValue("PSPAcceptanceCode", pspAcceptanceCode);
        sqlParameterSource.addValue("PSPTerminalCode", pspTerminalCode);
        sqlParameterSource.addValue("ComputerName", UserUtil.getCurrentUser().getComputerName());
        sqlParameterSource.addValue("UserId", UserUtil.getCurrentUser().getUserId());
        sqlParameterSource.addValue("Ip", UserUtil.getCurrentUser().getIp());

        Map<String, Object> resultExecuted = simpleJdbcCall.execute(sqlParameterSource);
        return (Integer) resultExecuted.get("#update-count-1") == 1;
    }

    @Override
    public PSPRequest findByRequestNumber(Long requestNumber) throws SQLException, DataAccessException
    {
        PSPRequest result = new PSPRequest();
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_pspRequestSelByRequestNumber")
                      .declareParameters(
                              new SqlParameter("RequestNumber", Types.BIGINT));
        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("RequestNumber", requestNumber);

        Map<String, Object> resultExecuted = simpleJdbcCall.
                                                                   returningResultSet("records", new PSPRequestMapper()).
                                                                   execute(sqlParameterSource);
        if (((List) resultExecuted.get("records")).size() > 0)
            result = ((List<PSPRequest>) resultExecuted.get("records")).get(0);

        return result;
    }
}
