package com.karafarin.terminalmanagement.dao.impl.sys;

import com.karafarin.terminalmanagement.config.jdbc.SqlConnectionConfiguration;
import com.karafarin.terminalmanagement.dao.intf.sys.SysRequestStatusDao;
import com.karafarin.terminalmanagement.dao.mapper.sys.SysRequestStatusMapper;
import com.karafarin.terminalmanagement.dto.sys.SysEnamad;
import com.karafarin.terminalmanagement.dto.sys.SysRequestStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.sql.SQLException;
import java.sql.Types;
import java.util.List;
import java.util.Map;

@Repository
public class SysRequestStatusDaoImpl implements SysRequestStatusDao
{
    @Autowired
    private SqlConnectionConfiguration sqlConnectionConfiguration;

    private JdbcTemplate jdbcTemplate = null;
    private SimpleJdbcCall simpleJdbcCall;

    @PostConstruct
    public void postConstruct()
    {
        if (jdbcTemplate == null) jdbcTemplate = sqlConnectionConfiguration.getJdbcTemplate();
    }

    @Override
    public Long add(SysRequestStatus entity) throws SQLException, DataAccessException
    {
        return null;
    }

    @Override
    public SysRequestStatus findById(Long id) throws SQLException, DataAccessException
    {
        SysRequestStatus result = new SysRequestStatus();
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_sysRequestStatusSel")
                      .declareParameters(new SqlParameter("RequestStatusId", Types.BIGINT));
        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("RequestStatusId", id);

        Map<String, Object> resultExecuted = simpleJdbcCall.
                                                                   returningResultSet("records",
                                                                                      new SysRequestStatusMapper()).
                                                                   execute(sqlParameterSource);
        if (((List) resultExecuted.get("records")).size() > 0)
            result = ((List<SysRequestStatus>) resultExecuted.get("records")).get(0);

        return result;
    }

    @Override
    public List<SysRequestStatus> findAll() throws SQLException, DataAccessException
    {
        return null;
    }

    @Override
    public boolean delete(Long id) throws SQLException, DataAccessException
    {
        return false;
    }

    @Override
    public boolean update(SysRequestStatus entity) throws SQLException, DataAccessException
    {
        return false;
    }

    @Override
    public List<SysRequestStatus> search(SysRequestStatus entity) throws SQLException, DataAccessException
    {
        return null;
    }
}
