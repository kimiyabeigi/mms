package com.karafarin.terminalmanagement.dao.impl.psp;

import com.karafarin.terminalmanagement.config.jdbc.SqlConnectionConfiguration;
import com.karafarin.terminalmanagement.dao.intf.psp.PSPTraceBatchDao;
import com.karafarin.terminalmanagement.dao.mapper.psp.PSPTraceBatchMapper;
import com.karafarin.terminalmanagement.dto.psp.PSPTraceBatch;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository
public class PSPTraceBatchDaoImpl implements PSPTraceBatchDao
{
    @Autowired
    private SqlConnectionConfiguration sqlConnectionConfiguration;

    private JdbcTemplate jdbcTemplate = null;
    private SimpleJdbcCall simpleJdbcCall;

    @PostConstruct
    public void postConstruct()
    {
        if (jdbcTemplate == null) jdbcTemplate = sqlConnectionConfiguration.getJdbcTemplate();
    }

    @Override
    public Long add(PSPTraceBatch entity) throws SQLException, DataAccessException
    {
        return null;
    }

    @Override
    public PSPTraceBatch findById(Long id) throws SQLException, DataAccessException
    {
        return null;
    }

    @Override
    public List<PSPTraceBatch> findAll() throws SQLException, DataAccessException
    {
        List<PSPTraceBatch> result = new ArrayList<>();
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_pspTraceUnknownSel");

        Map<String, Object> resultExecuted =
                simpleJdbcCall.returningResultSet("records", new PSPTraceBatchMapper()).execute();
        if (((List) resultExecuted.get("records")).size() > 0)
            result = (List<PSPTraceBatch>) resultExecuted.get("records");

        return result;
    }

    @Override
    public boolean delete(Long id) throws SQLException, DataAccessException
    {
        return false;
    }

    @Override
    public boolean update(PSPTraceBatch entity) throws SQLException, DataAccessException
    {
        return false;
    }

    @Override
    public List<PSPTraceBatch> search(PSPTraceBatch entity) throws SQLException, DataAccessException
    {
        return null;
    }

    @Override
    public PSPTraceBatch findByRequestNumber(Long requestNumber) throws SQLException, DataAccessException
    {
        PSPTraceBatch result = new PSPTraceBatch();
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_pspRequestByRequestNumberSel")
                      .declareParameters(new SqlParameter("RequestNumber", Types.BIGINT));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("RequestNumber", requestNumber);

        Map<String, Object> resultExecuted = simpleJdbcCall.returningResultSet("records", new PSPTraceBatchMapper()).
                execute(sqlParameterSource);
        if (((List) resultExecuted.get("records")).size() > 0)
            result = ((List<PSPTraceBatch>) resultExecuted.get("records")).get(0);

        return result;
    }
}
