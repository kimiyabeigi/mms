package com.karafarin.terminalmanagement.dao.impl.setting;

import com.karafarin.terminalmanagement.config.jdbc.SqlConnectionConfiguration;
import com.karafarin.terminalmanagement.dao.intf.setting.CompanySettingDao;
import com.karafarin.terminalmanagement.dao.mapper.setting.CompanySettingMapper;
import com.karafarin.terminalmanagement.dao.mapper.setting.CompanySettingViewMapper;
import com.karafarin.terminalmanagement.dto.setting.CompanySetting;
import com.karafarin.terminalmanagement.dto.setting.CompanySettingView;
import com.karafarin.terminalmanagement.util.UserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.SqlReturnResultSet;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository
public class CompanySettingDaoImpl implements CompanySettingDao
{
    @Autowired
    private SqlConnectionConfiguration sqlConnectionConfiguration;

    private JdbcTemplate jdbcTemplate = null;
    private SimpleJdbcCall simpleJdbcCall;

    @PostConstruct
    public void postConstruct()
    {
        if (jdbcTemplate == null)
            jdbcTemplate = sqlConnectionConfiguration.getJdbcTemplate();
    }

    @Override
    public Long add(CompanySetting entity) throws SQLException, DataAccessException
    {
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_CompanySettingIns")
                .declareParameters(
                        new SqlParameter("UserCallerId", Types.BIGINT),
                        new SqlParameter("CompanyId", Types.BIGINT),
                        new SqlParameter("OrganizationCode", Types.NVARCHAR),
                        new SqlParameter("IsActive", Types.BOOLEAN),
                        new SqlParameter("Ip", Types.NVARCHAR),
                        new SqlParameter("ComputerName", Types.NVARCHAR));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("UserCallerId", UserUtil.getCurrentUser().getUserId());
        sqlParameterSource.addValue("CompanyId", entity.getCompanyId());
        sqlParameterSource.addValue("OrganizationCode", entity.getOrganizationCode());
        sqlParameterSource.addValue("IsActive", entity.isActive());
        sqlParameterSource.addValue("Ip", UserUtil.getCurrentUser().getIp());
        sqlParameterSource.addValue("ComputerName", UserUtil.getCurrentUser().getComputerName());

        Map<String, Object> resultExecuted = simpleJdbcCall.execute(sqlParameterSource);
        return (Long) resultExecuted.get("RETURN_VALUE");
    }

    @Override
    public CompanySetting findById(Long id) throws SQLException, DataAccessException
    {
        CompanySetting result = new CompanySetting();
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_CompanySettingSel")
                .declareParameters(
                        new SqlParameter("CompanySettingId", Types.BIGINT),
                        new SqlReturnResultSet("records", new CompanySettingMapper()));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("CompanySettingId", id);

        Map<String, Object> resultExecuted = simpleJdbcCall.
                execute(sqlParameterSource);
        if (((List) resultExecuted.get("records")).size() > 0)
            result = ((List<CompanySetting>) resultExecuted.get("records")).get(0);

        return result;
    }

    @Override
    public List<CompanySetting> findAll() throws SQLException, DataAccessException
    {
        List<CompanySetting> result = new ArrayList<>();
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_CompanySettingSel")
                .declareParameters(
                        new SqlParameter("CompanySettingId", Types.BIGINT),
                        new SqlReturnResultSet("records", new CompanySettingMapper()));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("CompanySettingId", -1);

        Map<String, Object> resultExecuted = simpleJdbcCall.
                execute(sqlParameterSource);
        if (((List) resultExecuted.get("records")).size() > 0)
            result = (List<CompanySetting>) resultExecuted.get("records");

        return result;
    }

    @Override
    public boolean delete(Long id) throws SQLException, DataAccessException
    {
        return false;
    }

    @Override
    public boolean update(CompanySetting entity) throws SQLException, DataAccessException
    {
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_CompanySettingUpd")
                .declareParameters(
                        new SqlParameter("UserCallerId", Types.BIGINT),
                        new SqlParameter("CompanySettingId", Types.BIGINT),
                        new SqlParameter("OrganizationCode", Types.NVARCHAR),
                        new SqlParameter("CompanyId", Types.BIGINT),
                        new SqlParameter("IsActive", Types.BOOLEAN),
                        new SqlParameter("Ip", Types.NVARCHAR),
                        new SqlParameter("ComputerName", Types.NVARCHAR));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("UserCallerId", UserUtil.getCurrentUser().getUserId());
        sqlParameterSource.addValue("CompanySettingId", entity.getCompanySettingId());
        sqlParameterSource.addValue("OrganizationCode", entity.getOrganizationCode());
        sqlParameterSource.addValue("CompanyId", entity.getCompanyId());
        sqlParameterSource.addValue("IsActive", entity.isActive());
        sqlParameterSource.addValue("Ip", UserUtil.getCurrentUser().getIp());
        sqlParameterSource.addValue("ComputerName", UserUtil.getCurrentUser().getComputerName());

        Map<String, Object> resultExecuted = simpleJdbcCall.execute(sqlParameterSource);
        return (Integer) resultExecuted.get("#update-count-1") == 1;
    }

    @Override
    public List<CompanySetting> search(CompanySetting entity) throws SQLException, DataAccessException
    {
        return null;
    }

    @Override
    public CompanySettingView findViewById(Long id) throws SQLException, DataAccessException
    {
        CompanySettingView result = new CompanySettingView();
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_CompanySettingViewSel")
                .declareParameters(
                        new SqlParameter("CompanySettingId", Types.BIGINT),
                        new SqlReturnResultSet("records", new CompanySettingViewMapper()));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("CompanySettingId", id);

        Map<String, Object> resultExecuted = simpleJdbcCall.
                execute(sqlParameterSource);
        if (((List) resultExecuted.get("records")).size() > 0)
            result = ((List<CompanySettingView>) resultExecuted.get("records")).get(0);

        return result;
    }

    @Override
    public List<CompanySettingView> findViewAll() throws SQLException, DataAccessException
    {
        List<CompanySettingView> result = new ArrayList<>();
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_CompanySettingViewSel")
                .declareParameters(
                        new SqlParameter("CompanySettingId", Types.BIGINT),
                        new SqlReturnResultSet("records", new CompanySettingViewMapper()));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("CompanySettingId", -1);

        Map<String, Object> resultExecuted = simpleJdbcCall.
                execute(sqlParameterSource);
        if (((List) resultExecuted.get("records")).size() > 0)
            result = (List<CompanySettingView>) resultExecuted.get("records");

        return result;
    }

    @Override
    public CompanySetting findByCompanyId(Long id) throws SQLException, DataAccessException
    {
        CompanySetting result = new CompanySetting();
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_CompanySettingSelByCompanyId")
                      .declareParameters(
                              new SqlParameter("CompanyId", Types.BIGINT),
                              new SqlReturnResultSet("records", new CompanySettingMapper()));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("CompanyId", id);

        Map<String, Object> resultExecuted = simpleJdbcCall.
                                                                   execute(sqlParameterSource);
        if (((List) resultExecuted.get("records")).size() > 0)
            result = ((List<CompanySetting>) resultExecuted.get("records")).get(0);

        return result;
    }
}
