package com.karafarin.terminalmanagement.dao.impl.psp;

import com.karafarin.terminalmanagement.config.jdbc.SqlConnectionConfiguration;
import com.karafarin.terminalmanagement.dao.intf.psp.PSPIPGDao;
import com.karafarin.terminalmanagement.dao.mapper.psp.PSPIPGMapper;
import com.karafarin.terminalmanagement.dto.psp.PSPIPG;
import com.karafarin.terminalmanagement.util.UserUtil;
import com.karafarin.terminalmanagement.util.date.DateConverterUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository
public class PSPIPGDaoImpl implements PSPIPGDao
{
    @Autowired
    private SqlConnectionConfiguration sqlConnectionConfiguration;

    private JdbcTemplate jdbcTemplate = null;
    private SimpleJdbcCall simpleJdbcCall;

    @PostConstruct
    public void postConstruct()
    {
        if (jdbcTemplate == null)
            jdbcTemplate = sqlConnectionConfiguration.getJdbcTemplate();
    }

    @Override
    public Long add(PSPIPG entity) throws SQLException, DataAccessException
    {
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_pspIPGIns")
                .declareParameters(
                        new SqlParameter("UserCallerId", Types.BIGINT),
                        new SqlParameter("PSPRequestId", Types.BIGINT),
                        new SqlParameter("ENamadId", Types.BIGINT),
                        new SqlParameter("SiteAddress", Types.NVARCHAR),
                        new SqlParameter("IssuanceDate", Types.DATE),
                        new SqlParameter("Email", Types.NVARCHAR),
                        new SqlParameter("ExpiryDate", Types.DATE),
                        new SqlParameter("FirstName", Types.NVARCHAR),
                        new SqlParameter("LastName", Types.NVARCHAR),
                        new SqlParameter("Phone", Types.NVARCHAR),
                        new SqlParameter("MobilePhone", Types.NVARCHAR),
                        new SqlParameter("TerminalDetailId", Types.BIGINT),
                        new SqlParameter("Ip", Types.NVARCHAR),
                        new SqlParameter("ComputerName", Types.NVARCHAR));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("UserCallerId", UserUtil.getCurrentUser().getUserId());
        sqlParameterSource.addValue("PSPRequestId", entity.getPspRequestId());
        sqlParameterSource.addValue("ENamadId", entity.geteNamadId());
        sqlParameterSource.addValue("SiteAddress", entity.getSiteAddress());
        sqlParameterSource.addValue("IssuanceDate", DateConverterUtil
                .convertStringWithAndWithoutDelimiterToDate(entity.getIssuanceDate()));
        sqlParameterSource.addValue("Email", entity.getEmail());
        sqlParameterSource.addValue("ExpiryDate", DateConverterUtil
                .convertStringWithAndWithoutDelimiterToDate(entity.getExpiryDate()));
        sqlParameterSource.addValue("FirstName", entity.getFirstName());
        sqlParameterSource.addValue("LastName", entity.getLastName());
        sqlParameterSource.addValue("Phone", entity.getPhone());
        sqlParameterSource.addValue("MobilePhone", entity.getMobilePhone());
        sqlParameterSource.addValue("TerminalDetailId", entity.getTerminalDetailId());
        sqlParameterSource.addValue("Ip", UserUtil.getCurrentUser().getIp());
        sqlParameterSource.addValue("ComputerName", UserUtil.getCurrentUser().getComputerName());

        Map<String, Object> resultExecuted = simpleJdbcCall.execute(sqlParameterSource);
        return (Long) resultExecuted.get("RETURN_VALUE");
    }

    @Override
    public PSPIPG findById(Long id) throws SQLException, DataAccessException
    {
        PSPIPG result = new PSPIPG();
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_pspIPGSel")
                .declareParameters(
                        new SqlParameter("IPGId", Types.BIGINT));
        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("IPGId", id);

        Map<String, Object> resultExecuted = simpleJdbcCall.
                returningResultSet("records", new PSPIPGMapper()).
                execute(sqlParameterSource);
        if (((List) resultExecuted.get("records")).size() > 0)
            result = ((List<PSPIPG>) resultExecuted.get("records")).get(0);

        return result;

    }

    @Override
    public List<PSPIPG> findAll() throws SQLException, DataAccessException
    {
        List<PSPIPG> result = new ArrayList<>();
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_pspIPGSel")
                .declareParameters(
                        new SqlParameter("IPGId", Types.BIGINT));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("IPGId", -1);

        Map<String, Object> resultExecuted = simpleJdbcCall.
                returningResultSet("records", new PSPIPGMapper()).
                execute(sqlParameterSource);
        if (((List) resultExecuted.get("records")).size() > 0)
            result = (List<PSPIPG>) resultExecuted.get("records");

        return result;
    }

    @Override
    public boolean delete(Long id) throws SQLException, DataAccessException
    {
        return false;
    }

    @Override
    public boolean update(PSPIPG entity) throws SQLException, DataAccessException
    {
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_pspIPGUpd")
                .declareParameters(
                        new SqlParameter("UserCallerId", Types.BIGINT),
                        new SqlParameter("IPGId", Types.BIGINT),
                        new SqlParameter("PSPRequestId", Types.BIGINT),
                        new SqlParameter("ENamadId", Types.BIGINT),
                        new SqlParameter("SiteAddress", Types.NVARCHAR),
                        new SqlParameter("IssuanceDate", Types.DATE),
                        new SqlParameter("Email", Types.NVARCHAR),
                        new SqlParameter("ExpiryDate", Types.DATE),
                        new SqlParameter("FirstName", Types.NVARCHAR),
                        new SqlParameter("LastName", Types.NVARCHAR),
                        new SqlParameter("Phone", Types.NVARCHAR),
                        new SqlParameter("MobilePhone", Types.NVARCHAR),
                        new SqlParameter("TerminalDetailId", Types.BIGINT),
                        new SqlParameter("Ip", Types.NVARCHAR),
                        new SqlParameter("ComputerName", Types.NVARCHAR));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("UserCallerId", UserUtil.getCurrentUser().getUserId());
        sqlParameterSource.addValue("IPGId", entity.getIPGId());
        sqlParameterSource.addValue("PSPRequestId", entity.getPspRequestId());
        sqlParameterSource.addValue("ENamadId", entity.geteNamadId());
        sqlParameterSource.addValue("SiteAddress", entity.getSiteAddress());
        sqlParameterSource.addValue("IssuanceDate", DateConverterUtil
                .convertStringWithAndWithoutDelimiterToDate(entity.getIssuanceDate()));
        sqlParameterSource.addValue("Email", entity.getEmail());
        sqlParameterSource.addValue("ExpiryDate", DateConverterUtil
                .convertStringWithAndWithoutDelimiterToDate(entity.getExpiryDate()));
        sqlParameterSource.addValue("FirstName", entity.getFirstName());
        sqlParameterSource.addValue("LastName", entity.getLastName());
        sqlParameterSource.addValue("Phone", entity.getPhone());
        sqlParameterSource.addValue("MobilePhone", entity.getMobilePhone());
        sqlParameterSource.addValue("TerminalDetailId", entity.getTerminalDetailId());
        sqlParameterSource.addValue("Ip", UserUtil.getCurrentUser().getIp());
        sqlParameterSource.addValue("ComputerName", UserUtil.getCurrentUser().getComputerName());

        Map<String, Object> resultExecuted = simpleJdbcCall.execute(sqlParameterSource);
        return (Integer) resultExecuted.get("#update-count-1") == 1;
    }

    @Override
    public List<PSPIPG> search(PSPIPG entity) throws SQLException, DataAccessException
    {
        return null;
    }
}
