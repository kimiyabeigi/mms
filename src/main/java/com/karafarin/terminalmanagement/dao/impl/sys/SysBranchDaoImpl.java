package com.karafarin.terminalmanagement.dao.impl.sys;

import com.karafarin.terminalmanagement.config.jdbc.SqlConnectionConfiguration;
import com.karafarin.terminalmanagement.dao.intf.sys.SysBranchDao;
import com.karafarin.terminalmanagement.dao.mapper.sys.SysBranchMapper;
import com.karafarin.terminalmanagement.dto.sys.SysBranch;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository
public class SysBranchDaoImpl implements SysBranchDao
{
    @Autowired
    private SqlConnectionConfiguration sqlConnectionConfiguration;

    private JdbcTemplate jdbcTemplate = null;
    private SimpleJdbcCall simpleJdbcCall;

    @PostConstruct
    public void postConstruct()
    {
        if (jdbcTemplate == null) jdbcTemplate = sqlConnectionConfiguration.getJdbcTemplate();
    }

    @Override
    public Long add(SysBranch entity) throws SQLException, DataAccessException
    {
        return null;
    }

    @Override
    public SysBranch findById(Long id) throws SQLException, DataAccessException
    {
        SysBranch result = new SysBranch();
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_sysBranchSel")
                .declareParameters(
                        new SqlParameter("BranchId", Types.BIGINT));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("BranchId", id);

        Map<String, Object> resultExecuted = simpleJdbcCall.
                returningResultSet("records", new SysBranchMapper()).
                execute(sqlParameterSource);
        if (((List) resultExecuted.get("records")).size() > 0)
            result = ((List<SysBranch>) resultExecuted.get("records")).get(0);

        return result;
    }

    @Override
    public List<SysBranch> findAll() throws SQLException, DataAccessException
    {
        List<SysBranch> result = new ArrayList<>();
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_sysBranchSel")
                .declareParameters(
                        new SqlParameter("BranchId", Types.BIGINT));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("BranchId", -1);

        Map<String, Object> resultExecuted = simpleJdbcCall.
                returningResultSet("records", new SysBranchMapper()).
                execute(sqlParameterSource);
        if (((List) resultExecuted.get("records")).size() > 0)
            result = (List<SysBranch>) resultExecuted.get("records");

        return result;
    }

    @Override
    public boolean delete(Long id) throws SQLException, DataAccessException
    {
        return false;
    }

    @Override
    public boolean update(SysBranch entity) throws SQLException, DataAccessException
    {
        return false;
    }

    @Override
    public List<SysBranch> search(SysBranch entity) throws SQLException, DataAccessException
    {
        return null;
    }

    @Override
    public SysBranch findByBranchCode(String branchCode) throws SQLException, DataAccessException
    {
        SysBranch result = new SysBranch();
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_sysBranchByCodeSel")
                .declareParameters(
                        new SqlParameter("BranchCode", Types.NVARCHAR));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("BranchCode", branchCode);

        Map<String, Object> resultExecuted = simpleJdbcCall.
                returningResultSet("records", new SysBranchMapper()).
                execute(sqlParameterSource);
        if (((List) resultExecuted.get("records")).size() > 0)
            result = ((List<SysBranch>) resultExecuted.get("records")).get(0);

        return result;
    }

    @Override
    public SysBranch findSupervisorBranch() throws SQLException, DataAccessException
    {
        SysBranch result = new SysBranch();
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_sysBranchSupervisorSel");

        Map<String, Object> resultExecuted = simpleJdbcCall.
                returningResultSet("records", new SysBranchMapper()).
                execute();
        if (((List) resultExecuted.get("records")).size() > 0)
            result = ((List<SysBranch>) resultExecuted.get("records")).get(0);

        return result;
    }
}
