package com.karafarin.terminalmanagement.dao.impl.base;

import com.karafarin.terminalmanagement.config.jdbc.SqlConnectionConfiguration;
import com.karafarin.terminalmanagement.dao.intf.base.BaseCompanyDao;
import com.karafarin.terminalmanagement.dao.mapper.base.BaseCompanyMapper;
import com.karafarin.terminalmanagement.dto.base.BaseCompany;
import com.karafarin.terminalmanagement.util.UserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository
public class BaseCompanyDaoImpl implements BaseCompanyDao
{
    @Autowired
    private SqlConnectionConfiguration sqlConnectionConfiguration;

    private JdbcTemplate jdbcTemplate = null;
    private SimpleJdbcCall simpleJdbcCall;

    @PostConstruct
    public void postConstruct()
    {
        if (jdbcTemplate == null)
            jdbcTemplate = sqlConnectionConfiguration.getJdbcTemplate();

    }

    @Override
    public Long add(BaseCompany entity) throws SQLException, DataAccessException
    {
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_BaseCompanyIns")
                .declareParameters(
                        new SqlParameter("UserCallerId", Types.BIGINT),
                        new SqlParameter("CompanyCode", Types.VARCHAR),
                        new SqlParameter("Name", Types.NVARCHAR),
                        new SqlParameter("Ip", Types.NVARCHAR),
                        new SqlParameter("ComputerName", Types.NVARCHAR));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("UserCallerId", UserUtil.getCurrentUser().getUserId());
        sqlParameterSource.addValue("CompanyCode", entity.getCompanyCode());
        sqlParameterSource.addValue("Name", entity.getName());
        sqlParameterSource.addValue("Ip", UserUtil.getCurrentUser().getIp());
        sqlParameterSource.addValue("ComputerName", UserUtil.getCurrentUser().getComputerName());

        Map<String, Object> resultExecuted = simpleJdbcCall.execute(sqlParameterSource);
        return (Long) resultExecuted.get("RETURN_VALUE");
    }

    @Override
    public BaseCompany findById(Long id) throws SQLException, DataAccessException
    {
        BaseCompany result = new BaseCompany();
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_BaseCompanySel")
                .declareParameters(
                        new SqlParameter("CompanyId", Types.BIGINT));
        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("CompanyId", id);

        Map<String, Object> resultExecuted = simpleJdbcCall.
                returningResultSet("records", new BaseCompanyMapper()).
                execute(sqlParameterSource);
        if (((List) resultExecuted.get("records")).size() > 0)
            result = ((List<BaseCompany>) resultExecuted.get("records")).get(0);

        return result;
    }

    @Override
    public List<BaseCompany> findAll() throws SQLException, DataAccessException
    {
        List<BaseCompany> result = new ArrayList<>();
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_BaseCompanySel")
                .declareParameters(
                        new SqlParameter("CompanyId", Types.BIGINT));
        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("CompanyId", -1);

        Map<String, Object> resultExecuted = simpleJdbcCall.
                returningResultSet("records", new BaseCompanyMapper()).
                execute(sqlParameterSource);
        if (((List) resultExecuted.get("records")).size() > 0)
            result = (List<BaseCompany>) resultExecuted.get("records");

        return result;
    }

    @Override
    public boolean update(BaseCompany entity) throws SQLException, DataAccessException
    {
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_BaseCompanyUpd")
                .declareParameters(
                        new SqlParameter("UserCallerId", Types.BIGINT),
                        new SqlParameter("CompanyId", Types.BIGINT),
                        new SqlParameter("CompanyCode", Types.VARCHAR),
                        new SqlParameter("Name", Types.NVARCHAR),
                        new SqlParameter("Ip", Types.NVARCHAR),
                        new SqlParameter("ComputerName", Types.NVARCHAR));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("UserCallerId", UserUtil.getCurrentUser().getUserId());
        sqlParameterSource.addValue("CompanyId", entity.getCompanyId());
        sqlParameterSource.addValue("CompanyCode", entity.getCompanyCode());
        sqlParameterSource.addValue("Name", entity.getName());
        sqlParameterSource.addValue("Ip", UserUtil.getCurrentUser().getIp());
        sqlParameterSource.addValue("ComputerName", UserUtil.getCurrentUser().getComputerName());

        Map<String, Object> resultExecuted = simpleJdbcCall.execute(sqlParameterSource);
        return (Integer) resultExecuted.get("#update-count-1") == 1;
    }

    @Override
    public List<BaseCompany> search(BaseCompany entity) throws SQLException, DataAccessException
    {
        List<BaseCompany> result = new ArrayList<>();
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_BaseCompanySrh")
                .declareParameters(
                        new SqlParameter("CompanyCode", Types.VARCHAR),
                        new SqlParameter("Name", Types.NVARCHAR));
        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("CompanyCode", entity.getCompanyCode());
        sqlParameterSource.addValue("Name", entity.getName());

        Map<String, Object> resultExecuted = simpleJdbcCall.
                returningResultSet("records", new BaseCompanyMapper()).
                execute(sqlParameterSource);
        if (((List) resultExecuted.get("records")).size() > 0)
            result = (List<BaseCompany>) resultExecuted.get("records");

        return result;
    }

    @Override
    public boolean delete(Long id) throws SQLException, DataAccessException
    {
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_BaseCompanyDel")
                .declareParameters(
                        new SqlParameter("UserCallerId", Types.BIGINT),
                        new SqlParameter("CompanyId", Types.BIGINT),
                        new SqlParameter("Ip", Types.NVARCHAR),
                        new SqlParameter("ComputerName", Types.NVARCHAR));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("UserCallerId", UserUtil.getCurrentUser().getUserId());
        sqlParameterSource.addValue("CompanyId", id);
        sqlParameterSource.addValue("Ip", UserUtil.getCurrentUser().getIp());
        sqlParameterSource.addValue("ComputerName", UserUtil.getCurrentUser().getComputerName());

        Map<String, Object> resultExecuted = simpleJdbcCall.execute(sqlParameterSource);
        return (Integer) resultExecuted.get("#update-count-1") == 1;
    }

    @Override
    public List<BaseCompany> findActiveCompaniesForRequest() throws SQLException, DataAccessException
    {
        List<BaseCompany> result = new ArrayList<>();
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_CompanyRequestSel");

        Map<String, Object> resultExecuted = simpleJdbcCall.
                returningResultSet("records", new BaseCompanyMapper()).
                execute();
        if (((List) resultExecuted.get("records")).size() > 0)
            result = (List<BaseCompany>) resultExecuted.get("records");

        return result;
    }
}
