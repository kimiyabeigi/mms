package com.karafarin.terminalmanagement.dao.impl.psp;

import com.karafarin.terminalmanagement.config.jdbc.SqlConnectionConfiguration;
import com.karafarin.terminalmanagement.dao.intf.psp.PSPReportRegisterDetailDao;
import com.karafarin.terminalmanagement.dao.mapper.psp.PSPReportRegisterDetailMapper;
import com.karafarin.terminalmanagement.dto.psp.PSPReportRegisterDetail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository
public class PSPReportRegisterDetailImpl implements PSPReportRegisterDetailDao
{
    @Autowired
    private SqlConnectionConfiguration sqlConnectionConfiguration;

    private JdbcTemplate jdbcTemplate = null;
    private SimpleJdbcCall simpleJdbcCall;

    @PostConstruct
    public void postConstruct()
    {
        if (jdbcTemplate == null)
            jdbcTemplate = sqlConnectionConfiguration.getJdbcTemplate();
    }

    @Override
    public List<PSPReportRegisterDetail> findAll(Long pspRequestId) throws SQLException, DataAccessException
    {
        List<PSPReportRegisterDetail> result = new ArrayList<>();
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_ReportRegisterDetailSel")
                      .declareParameters(
                              new SqlParameter("PSPRequestId", Types.BIGINT));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("PSPRequestId", pspRequestId);

        Map<String, Object> resultExecuted =
                simpleJdbcCall.returningResultSet("records", new PSPReportRegisterDetailMapper()).
                        execute(sqlParameterSource);
        if (((List) resultExecuted.get("records")).size() > 0)
            result = (List<PSPReportRegisterDetail>) resultExecuted.get("records");

        return result;

    }
}
