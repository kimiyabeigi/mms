package com.karafarin.terminalmanagement.dao.impl.psp;

import com.karafarin.terminalmanagement.config.jdbc.SqlConnectionConfiguration;
import com.karafarin.terminalmanagement.dao.intf.psp.PSPClientDao;
import com.karafarin.terminalmanagement.dao.mapper.psp.PSPClientMapper;
import com.karafarin.terminalmanagement.dto.psp.PSPClient;
import com.karafarin.terminalmanagement.util.UserUtil;
import com.karafarin.terminalmanagement.util.date.DateConverterUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository
public class PSPClientDaoImpl implements PSPClientDao
{
    @Autowired
    private SqlConnectionConfiguration sqlConnectionConfiguration;

    private JdbcTemplate jdbcTemplate = null;
    private SimpleJdbcCall simpleJdbcCall;

    @PostConstruct
    public void postConstruct()
    {
        if (jdbcTemplate == null)
            jdbcTemplate = sqlConnectionConfiguration.getJdbcTemplate();
    }

    @Override
    public Long add(PSPClient entity) throws SQLException, DataAccessException
    {
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_pspClientIns")
                .declareParameters(
                        new SqlParameter("UserCallerId", Types.BIGINT),
                        new SqlParameter("PSPRequestId", Types.BIGINT),
                        new SqlParameter("PositionId", Types.BIGINT),
                        new SqlParameter("ClientNumber", Types.NVARCHAR),
                        new SqlParameter("Sex", Types.NVARCHAR),
                        new SqlParameter("LifeStatus", Types.NVARCHAR),
                        new SqlParameter("FirstName", Types.NVARCHAR),
                        new SqlParameter("FirstNameEn", Types.NVARCHAR),
                        new SqlParameter("LastName", Types.NVARCHAR),
                        new SqlParameter("LastNameEn", Types.NVARCHAR),
                        new SqlParameter("FatherName", Types.NVARCHAR),
                        new SqlParameter("FatherNameEn", Types.NVARCHAR),
                        new SqlParameter("NationalCode", Types.NVARCHAR),
                        new SqlParameter("RegNumber", Types.NVARCHAR),
                        new SqlParameter("ForeignCode", Types.NVARCHAR),
                        new SqlParameter("PassportNumber", Types.NVARCHAR),
                        new SqlParameter("BirthDay", Types.DATE),
                        new SqlParameter("PassportExpiryDate", Types.DATE),
                        new SqlParameter("CountryNameAbbr", Types.NVARCHAR),
                        new SqlParameter("CountryCode", Types.NVARCHAR),
                        new SqlParameter("CountryDesc", Types.NVARCHAR),
                        new SqlParameter("IssuanceDate", Types.DATE),
                        new SqlParameter("IssuancePlaceCode", Types.NVARCHAR),
                        new SqlParameter("IssuancePlaceDesc", Types.NVARCHAR),
                        new SqlParameter("MobilePhone", Types.NVARCHAR),
                        new SqlParameter("Phone", Types.NVARCHAR),
                        new SqlParameter("Fax", Types.NVARCHAR),
                        new SqlParameter("PostalCode", Types.NVARCHAR),
                        new SqlParameter("Address", Types.NVARCHAR),
                        new SqlParameter("ClientType", Types.NVARCHAR),
                        new SqlParameter("ClientKind", Types.NVARCHAR),
                        new SqlParameter("CompanyName", Types.NVARCHAR),
                        new SqlParameter("CompanyNameEn", Types.NVARCHAR),
                        new SqlParameter("CommerceCode", Types.NVARCHAR),
                        new SqlParameter("Ip", Types.NVARCHAR),
                        new SqlParameter("ComputerName", Types.NVARCHAR));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("UserCallerId", UserUtil.getCurrentUser().getUserId());
        sqlParameterSource.addValue("PSPRequestId", entity.getPspRequestId());
        sqlParameterSource.addValue("PositionId", entity.getPositionId());
        sqlParameterSource.addValue("ClientNumber", entity.getClientNumber());
        sqlParameterSource.addValue("Sex", entity.getSex());
        sqlParameterSource.addValue("LifeStatus", entity.getLifeStatus());
        sqlParameterSource.addValue("FirstName", entity.getFirstName());
        sqlParameterSource.addValue("FirstNameEn", entity.getFirstNameEn());
        sqlParameterSource.addValue("LastName", entity.getLastName());
        sqlParameterSource.addValue("LastNameEn", entity.getLastNameEn());
        sqlParameterSource.addValue("FatherName", entity.getFatherName());
        sqlParameterSource.addValue("FatherNameEn", entity.getFirstNameEn());
        sqlParameterSource.addValue("NationalCode", entity.getNationalCode());
        sqlParameterSource.addValue("RegNumber", entity.getRegNumber());
        sqlParameterSource.addValue("ForeignCode", entity.getForeignCode());
        sqlParameterSource.addValue("PassportNumber", entity.getPassportNumber());
        sqlParameterSource.addValue("BirthDay", DateConverterUtil
                .convertStringWithAndWithoutDelimiterToDate(entity.getBirthDate()));
        sqlParameterSource.addValue("PassportExpiryDate", DateConverterUtil
                .convertStringWithAndWithoutDelimiterToDate(entity.getPassportExpiryDate()));
        sqlParameterSource.addValue("CountryNameAbbr", entity.getCountryNameAbbr());
        sqlParameterSource.addValue("CountryCode", entity.getCountryCode());
        sqlParameterSource.addValue("CountryDesc", entity.getCountryDesc());
        sqlParameterSource.addValue("IssuanceDate", DateConverterUtil
                .convertStringWithAndWithoutDelimiterToDate(entity.getIssuanceDate()));
        sqlParameterSource.addValue("IssuancePlaceCode", entity.getIssuancePlaceCode());
        sqlParameterSource.addValue("IssuancePlaceDesc", entity.getIssuancePlaceDesc());
        sqlParameterSource.addValue("MobilePhone", entity.getMobilePhone());
        sqlParameterSource.addValue("Phone", entity.getPhone());
        sqlParameterSource.addValue("Fax", entity.getFax());
        sqlParameterSource.addValue("PostalCode", entity.getPostalCode());
        sqlParameterSource.addValue("Address", entity.getAddress());
        sqlParameterSource.addValue("ClientType", entity.getClientType());
        sqlParameterSource.addValue("ClientKind", entity.getClientKind());
        sqlParameterSource.addValue("CompanyName", entity.getCompanyName());
        sqlParameterSource.addValue("CompanyNameEn", entity.getCompanyNameEn());
        sqlParameterSource.addValue("CommerceCode", entity.getCommerceCode());
        sqlParameterSource.addValue("Ip", UserUtil.getCurrentUser().getIp());
        sqlParameterSource.addValue("ComputerName", UserUtil.getCurrentUser().getComputerName());

        Map<String, Object> resultExecuted = simpleJdbcCall.execute(sqlParameterSource);
        return (Long) resultExecuted.get("RETURN_VALUE");
    }

    @Override
    public PSPClient findById(Long id) throws SQLException, DataAccessException
    {
        PSPClient result = new PSPClient();
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_pspClientSel")
                .declareParameters(
                        new SqlParameter("ClientId", Types.BIGINT));
        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("ClientId", id);

        Map<String, Object> resultExecuted = simpleJdbcCall.
                returningResultSet("records", new PSPClientMapper()).
                execute(sqlParameterSource);
        if (((List) resultExecuted.get("records")).size() > 0)
            result = ((List<PSPClient>) resultExecuted.get("records")).get(0);

        return result;
    }

    @Override
    public List<PSPClient> findAll() throws SQLException, DataAccessException
    {
        List<PSPClient> result = new ArrayList<>();
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_pspClientSel")
                .declareParameters(
                        new SqlParameter("ClientId", Types.BIGINT));
        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("ClientId", "");

        Map<String, Object> resultExecuted = simpleJdbcCall.
                returningResultSet("records", new PSPClientMapper()).
                execute(sqlParameterSource);
        if (((List) resultExecuted.get("records")).size() > 0)
            result = (List<PSPClient>) resultExecuted.get("records");

        return result;
    }

    @Override
    public boolean delete(Long id) throws SQLException, DataAccessException
    {
        return false;
    }

    @Override
    public boolean update(PSPClient entity) throws SQLException, DataAccessException
    {
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_pspClientUpd")
                .declareParameters(
                        new SqlParameter("UserCallerId", Types.BIGINT),
                        new SqlParameter("ClientId", Types.BIGINT),
                        new SqlParameter("PSPRequestId", Types.BIGINT),
                        new SqlParameter("PositionId", Types.BIGINT),
                        new SqlParameter("ClientNumber", Types.NVARCHAR),
                        new SqlParameter("ClientNumber", Types.NVARCHAR),
                        new SqlParameter("Sex", Types.NVARCHAR),
                        new SqlParameter("LifeStatus", Types.NVARCHAR),
                        new SqlParameter("FirstName", Types.NVARCHAR),
                        new SqlParameter("FirstNameEn", Types.NVARCHAR),
                        new SqlParameter("LastName", Types.NVARCHAR),
                        new SqlParameter("LastNameEn", Types.NVARCHAR),
                        new SqlParameter("FatherName", Types.NVARCHAR),
                        new SqlParameter("FatherNameEn", Types.NVARCHAR),
                        new SqlParameter("NationalCode", Types.NVARCHAR),
                        new SqlParameter("RegNumber", Types.NVARCHAR),
                        new SqlParameter("ForeignCode", Types.NVARCHAR),
                        new SqlParameter("PassportNumber", Types.NVARCHAR),
                        new SqlParameter("BirthDay", Types.DATE),
                        new SqlParameter("PassportExpiryDate", Types.DATE),
                        new SqlParameter("CountryNameAbbr", Types.NVARCHAR),
                        new SqlParameter("CountryCode", Types.NVARCHAR),
                        new SqlParameter("CountryDesc", Types.NVARCHAR),
                        new SqlParameter("IssuanceDate", Types.DATE),
                        new SqlParameter("IssuancePlaceCode", Types.NVARCHAR),
                        new SqlParameter("IssuancePlaceDesc", Types.NVARCHAR),
                        new SqlParameter("MobilePhone", Types.NVARCHAR),
                        new SqlParameter("Phone", Types.NVARCHAR),
                        new SqlParameter("Fax", Types.NVARCHAR),
                        new SqlParameter("PostalCode", Types.NVARCHAR),
                        new SqlParameter("Address", Types.NVARCHAR),
                        new SqlParameter("ClientType", Types.NVARCHAR),
                        new SqlParameter("ClientKind", Types.NVARCHAR),
                        new SqlParameter("CompanyName", Types.NVARCHAR),
                        new SqlParameter("CompanyNameEn", Types.NVARCHAR),
                        new SqlParameter("CommerceCode", Types.NVARCHAR),
                        new SqlParameter("Ip", Types.NVARCHAR),
                        new SqlParameter("ComputerName", Types.NVARCHAR));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("UserCallerId", UserUtil.getCurrentUser().getUserId());
        sqlParameterSource.addValue("ClientId", entity.getClientId());
        sqlParameterSource.addValue("PSPRequestId", entity.getPspRequestId());
        sqlParameterSource.addValue("PositionId", entity.getPositionId());
        sqlParameterSource.addValue("ClientNumber", entity.getClientNumber());
        sqlParameterSource.addValue("Sex", entity.getSex());
        sqlParameterSource.addValue("LifeStatus", entity.getLifeStatus());
        sqlParameterSource.addValue("FirstName", entity.getFirstName());
        sqlParameterSource.addValue("FirstNameEn", entity.getFirstNameEn());
        sqlParameterSource.addValue("LastName", entity.getLastName());
        sqlParameterSource.addValue("LastNameEn", entity.getLastNameEn());
        sqlParameterSource.addValue("FatherName", entity.getFatherName());
        sqlParameterSource.addValue("FatherNameEn", entity.getFirstNameEn());
        sqlParameterSource.addValue("NationalCode", entity.getNationalCode());
        sqlParameterSource.addValue("RegNumber", entity.getRegNumber());
        sqlParameterSource.addValue("ForeignCode", entity.getForeignCode());
        sqlParameterSource.addValue("PassportNumber", entity.getPassportNumber());
        sqlParameterSource.addValue("BirthDay", DateConverterUtil
                .convertStringWithAndWithoutDelimiterToDate(entity.getBirthDate()));
        sqlParameterSource.addValue("PassportExpiryDate", DateConverterUtil
                .convertStringWithAndWithoutDelimiterToDate(entity.getPassportExpiryDate()));
        sqlParameterSource.addValue("CountryNameAbbr", entity.getCountryNameAbbr());
        sqlParameterSource.addValue("CountryCode", entity.getCountryCode());
        sqlParameterSource.addValue("CountryDesc", entity.getCountryDesc());
        sqlParameterSource.addValue("IssuanceDate", DateConverterUtil
                .convertStringWithAndWithoutDelimiterToDate(entity.getIssuanceDate()));
        sqlParameterSource.addValue("IssuancePlaceCode", entity.getIssuancePlaceCode());
        sqlParameterSource.addValue("IssuancePlaceDesc", entity.getIssuancePlaceDesc());
        sqlParameterSource.addValue("MobilePhone", entity.getMobilePhone());
        sqlParameterSource.addValue("Phone", entity.getPhone());
        sqlParameterSource.addValue("Fax", entity.getFax());
        sqlParameterSource.addValue("PostalCode", entity.getPostalCode());
        sqlParameterSource.addValue("Address", entity.getAddress());
        sqlParameterSource.addValue("ClientType", entity.getAddress());
        sqlParameterSource.addValue("ClientKind", entity.getClientKind());
        sqlParameterSource.addValue("CompanyName", entity.getCompanyName());
        sqlParameterSource.addValue("CompanyNameEn", entity.getCompanyNameEn());
        sqlParameterSource.addValue("CommerceCode", entity.getCommerceCode());
        sqlParameterSource.addValue("Ip", UserUtil.getCurrentUser().getIp());
        sqlParameterSource.addValue("ComputerName", UserUtil.getCurrentUser().getComputerName());

        Map<String, Object> resultExecuted = simpleJdbcCall.execute(sqlParameterSource);
        return (Integer) resultExecuted.get("#update-count-1") == 1;
    }

    @Override
    public List<PSPClient> search(PSPClient entity) throws SQLException, DataAccessException
    {
        return null;
    }
}
