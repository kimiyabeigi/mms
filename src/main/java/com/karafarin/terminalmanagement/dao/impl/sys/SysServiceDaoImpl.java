package com.karafarin.terminalmanagement.dao.impl.sys;

import com.karafarin.terminalmanagement.config.jdbc.SqlConnectionConfiguration;
import com.karafarin.terminalmanagement.dao.intf.sys.SysServiceDao;
import com.karafarin.terminalmanagement.dao.mapper.sys.SysServiceMapper;
import com.karafarin.terminalmanagement.dto.sys.SysService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository
public class SysServiceDaoImpl implements SysServiceDao
{
    @Autowired
    private SqlConnectionConfiguration sqlConnectionConfiguration;

    private JdbcTemplate jdbcTemplate = null;
    private SimpleJdbcCall simpleJdbcCall;

    @PostConstruct
    public void postConstruct()
    {
        if (jdbcTemplate == null)
            jdbcTemplate = sqlConnectionConfiguration.getJdbcTemplate();
    }

    @Override
    public Long add(SysService entity) throws SQLException
    {
        return null;
    }

    @Override
    public SysService findById(Long id) throws SQLException, DataAccessException
    {
        SysService result = new SysService();
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_sysServiceSel")
                .declareParameters(
                        new SqlParameter("ServiceId", Types.BIGINT));
        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("ServiceId", id);

        Map<String, Object> resultExecuted = simpleJdbcCall.
                returningResultSet("records", new SysServiceMapper()).
                execute(sqlParameterSource);
        if (((List) resultExecuted.get("records")).size() > 0)
            result = ((List<SysService>) resultExecuted.get("records")).get(0);

        return result;
    }

    @Override
    public List<SysService> findAll() throws SQLException, DataAccessException
    {
        List<SysService> result = new ArrayList<>();
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_sysServiceSel")
                .declareParameters(
                        new SqlParameter("ServiceId", Types.BIGINT));
        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("ServiceId", -1);

        Map<String, Object> resultExecuted = simpleJdbcCall.
                returningResultSet("records", new SysServiceMapper()).
                execute(sqlParameterSource);
        if (((List) resultExecuted.get("records")).size() > 0)
            result = (List<SysService>) resultExecuted.get("records");

        return result;
    }

    @Override
    public boolean update(SysService entity) throws SQLException, DataAccessException
    {
        return false;
    }

    @Override
    public List<SysService> search(SysService entity) throws SQLException, DataAccessException
    {
        return null;
    }

    @Override
    public boolean delete(Long id) throws SQLException, DataAccessException
    {
        return false;
    }
}
