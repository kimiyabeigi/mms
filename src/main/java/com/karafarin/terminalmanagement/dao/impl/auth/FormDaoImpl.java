package com.karafarin.terminalmanagement.dao.impl.auth;

import com.karafarin.terminalmanagement.config.jdbc.SqlConnectionConfiguration;
import com.karafarin.terminalmanagement.dao.intf.auth.FormDao;
import com.karafarin.terminalmanagement.dao.mapper.auth.FormMapper;
import com.karafarin.terminalmanagement.dto.auth.Form;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository
public class FormDaoImpl implements FormDao
{
    @Autowired
    private SqlConnectionConfiguration sqlConnectionConfiguration;

    private JdbcTemplate jdbcTemplate = null;
    private SimpleJdbcCall simpleJdbcCall;

    @PostConstruct
    public void postConstruct()
    {
        if (jdbcTemplate == null) jdbcTemplate = sqlConnectionConfiguration.getJdbcTemplate();
    }

    @Override
    public Long add(Form entity) throws SQLException, DataAccessException
    {
        return null;
    }

    @Override
    public Form findById(Long id) throws SQLException, DataAccessException
    {
        Form result = new Form();
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_FormSel").declareParameters(new SqlParameter("FormId", Types.BIGINT));
        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("FormId", id);

        Map<String, Object> resultExecuted =
                simpleJdbcCall.returningResultSet("records", new FormMapper()).execute(sqlParameterSource);
        if (((List) resultExecuted.get("records")).size() > 0)
            result = ((List<Form>) resultExecuted.get("records")).get(0);

        return result;
    }

    @Override
    public List<Form> findAll() throws SQLException, DataAccessException
    {
        List<Form> result = new ArrayList<>();
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_FormSel").declareParameters(new SqlParameter("FormId", Types.BIGINT));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("FormId", -1);

        Map<String, Object> resultExecuted =
                simpleJdbcCall.returningResultSet("records", new FormMapper()).execute(sqlParameterSource);
        if (((List) resultExecuted.get("records")).size() > 0) result = (List<Form>) resultExecuted.get("records");

        return result;
    }

    @Override
    public boolean delete(Long id) throws SQLException, DataAccessException
    {
        return false;
    }

    @Override
    public boolean update(Form entity) throws SQLException, DataAccessException
    {
        return false;
    }

    @Override
    public List<Form> search(Form entity) throws SQLException, DataAccessException
    {
        return null;
    }
}
