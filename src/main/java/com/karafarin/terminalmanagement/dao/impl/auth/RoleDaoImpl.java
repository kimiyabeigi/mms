package com.karafarin.terminalmanagement.dao.impl.auth;

import com.karafarin.terminalmanagement.config.jdbc.SqlConnectionConfiguration;
import com.karafarin.terminalmanagement.dao.intf.auth.RoleDao;
import com.karafarin.terminalmanagement.dao.mapper.auth.RoleMapper;
import com.karafarin.terminalmanagement.dto.auth.Role;
import com.karafarin.terminalmanagement.util.UserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository
public class RoleDaoImpl implements RoleDao
{
    @Autowired
    private SqlConnectionConfiguration sqlConnectionConfiguration;

    private JdbcTemplate jdbcTemplate = null;
    private SimpleJdbcCall simpleJdbcCall;

    @PostConstruct
    public void postConstruct()
    {
        if (jdbcTemplate == null) jdbcTemplate = sqlConnectionConfiguration.getJdbcTemplate();
    }

    @Override
    public Long add(Role entity) throws SQLException, DataAccessException
    {
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_RoleIns")
                      .declareParameters(new SqlParameter("UserCallerId", Types.BIGINT),
                                         new SqlParameter("Name", Types.NVARCHAR),
                                         new SqlParameter("NameEN", Types.NVARCHAR),
                                         new SqlParameter("Description", Types.NVARCHAR),
                                         new SqlParameter("Ip", Types.NVARCHAR),
                                         new SqlParameter("ComputerName", Types.NVARCHAR));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("UserCallerId", UserUtil.getCurrentUser().getUserId());
        sqlParameterSource.addValue("Name", entity.getName());
        sqlParameterSource.addValue("NameEN", entity.getNameEN());
        sqlParameterSource.addValue("Description", entity.getDescription());
        sqlParameterSource.addValue("Ip", UserUtil.getCurrentUser().getIp());
        sqlParameterSource.addValue("ComputerName", UserUtil.getCurrentUser().getComputerName());

        Map<String, Object> resultExecuted = simpleJdbcCall.execute(sqlParameterSource);
        return (Long) resultExecuted.get("RETURN_VALUE");
    }

    @Override
    public Role findById(Long id) throws SQLException, DataAccessException
    {
        Role result = new Role();
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_RoleSel").declareParameters(new SqlParameter("RoleId", Types.BIGINT));
        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("RoleId", id);

        Map<String, Object> resultExecuted =
                simpleJdbcCall.returningResultSet("records", new RoleMapper()).execute(sqlParameterSource);
        if (((List) resultExecuted.get("records")).size() > 0)
            result = ((List<Role>) resultExecuted.get("records")).get(0);

        return result;
    }

    @Override
    public List<Role> findAll() throws SQLException, DataAccessException
    {
        List<Role> result = new ArrayList<>();
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_RoleSel").declareParameters(new SqlParameter("RoleId", Types.BIGINT));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("RoleId", -1);

        Map<String, Object> resultExecuted =
                simpleJdbcCall.returningResultSet("records", new RoleMapper()).execute(sqlParameterSource);
        if (((List) resultExecuted.get("records")).size() > 0)
            result = (List<Role>) resultExecuted.get("records");

        return result;
    }

    @Override
    public List<Role> findAllRoles() throws SQLException, DataAccessException
    {
        List<Role> result = new ArrayList<>();
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_RoleSel").declareParameters(new SqlParameter("RoleId", Types.BIGINT));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("RoleId", 0);

        Map<String, Object> resultExecuted =
                simpleJdbcCall.returningResultSet("records", new RoleMapper()).execute(sqlParameterSource);
        if (((List) resultExecuted.get("records")).size() > 0)
            result = (List<Role>) resultExecuted.get("records");

        return result;
    }

    @Override
    public boolean delete(Long id) throws SQLException, DataAccessException
    {
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_RoleDel")
                      .declareParameters(new SqlParameter("UserCallerId", Types.BIGINT),
                                         new SqlParameter("RoleId", Types.BIGINT),
                                         new SqlParameter("Ip", Types.NVARCHAR),
                                         new SqlParameter("ComputerName", Types.NVARCHAR));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("UserCallerId", UserUtil.getCurrentUser().getUserId());
        sqlParameterSource.addValue("RoleId", id);
        sqlParameterSource.addValue("Ip", UserUtil.getCurrentUser().getIp());
        sqlParameterSource.addValue("ComputerName", UserUtil.getCurrentUser().getComputerName());

        Map<String, Object> resultExecuted = simpleJdbcCall.execute(sqlParameterSource);
        return (Integer) resultExecuted.get("#update-count-1") == 1;
    }

    @Override
    public boolean update(Role entity) throws SQLException, DataAccessException
    {
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_RoleUpd")
                      .declareParameters(new SqlParameter("UserCallerId", Types.BIGINT),
                                         new SqlParameter("RoleId", Types.BIGINT),
                                         new SqlParameter("Name", Types.NVARCHAR),
                                         new SqlParameter("NameEN", Types.NVARCHAR),
                                         new SqlParameter("Description", Types.NVARCHAR),
                                         new SqlParameter("Ip", Types.NVARCHAR),
                                         new SqlParameter("ComputerName", Types.NVARCHAR));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("UserCallerId", UserUtil.getCurrentUser().getUserId());
        sqlParameterSource.addValue("RoleId", entity.getRoleId());
        sqlParameterSource.addValue("Name", entity.getName());
        sqlParameterSource.addValue("NameEN", entity.getNameEN());
        sqlParameterSource.addValue("Description", entity.getDescription());
        sqlParameterSource.addValue("Ip", UserUtil.getCurrentUser().getIp());
        sqlParameterSource.addValue("ComputerName", UserUtil.getCurrentUser().getComputerName());

        Map<String, Object> resultExecuted = simpleJdbcCall.execute(sqlParameterSource);
        return (Integer) resultExecuted.get("#update-count-1") == 1;
    }

    @Override
    public List<Role> search(Role entity) throws SQLException, DataAccessException
    {
        return null;
    }
}
