package com.karafarin.terminalmanagement.dao.impl.psp;

import com.karafarin.terminalmanagement.config.jdbc.SqlConnectionConfiguration;
import com.karafarin.terminalmanagement.dao.intf.psp.PSPAccountOwnerDao;
import com.karafarin.terminalmanagement.dao.mapper.psp.PSPAccountOwnerMapper;
import com.karafarin.terminalmanagement.dto.psp.PSPAccountOwner;
import com.karafarin.terminalmanagement.util.UserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository
public class PSPAccountOwnerDaoImpl implements PSPAccountOwnerDao
{
    @Autowired
    private SqlConnectionConfiguration sqlConnectionConfiguration;

    private JdbcTemplate jdbcTemplate = null;
    private SimpleJdbcCall simpleJdbcCall;

    @PostConstruct
    public void postConstruct()
    {
        if (jdbcTemplate == null)
            jdbcTemplate = sqlConnectionConfiguration.getJdbcTemplate();
    }

    @Override
    public Long add(PSPAccountOwner entity) throws SQLException, DataAccessException
    {
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_pspAccountOwnerIns")
                .declareParameters(
                        new SqlParameter("UserCallerId", Types.BIGINT),
                        new SqlParameter("PSPRequestId", Types.BIGINT),
                        new SqlParameter("ClientNumber", Types.NVARCHAR),
                        new SqlParameter("ClientName", Types.NVARCHAR),
                        new SqlParameter("IsActive", Types.BIT),
                        new SqlParameter("ClientType", Types.NVARCHAR),
                        new SqlParameter("Ip", Types.NVARCHAR),
                        new SqlParameter("ComputerName", Types.NVARCHAR));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("UserCallerId", UserUtil.getCurrentUser().getUserId());
        sqlParameterSource.addValue("PSPRequestId", entity.getPspRequestId());
        sqlParameterSource.addValue("ClientNumber", entity.getClientNumber());
        sqlParameterSource.addValue("ClientName", entity.getClientName());
        sqlParameterSource.addValue("IsActive", entity.isActive());
        sqlParameterSource.addValue("ClientType", entity.getClientType());
        sqlParameterSource.addValue("Ip", UserUtil.getCurrentUser().getIp());
        sqlParameterSource.addValue("ComputerName", UserUtil.getCurrentUser().getComputerName());

        Map<String, Object> resultExecuted = simpleJdbcCall.execute(sqlParameterSource);
        return (Long) resultExecuted.get("RETURN_VALUE");
    }

    @Override
    public PSPAccountOwner findById(Long id) throws SQLException, DataAccessException
    {
        PSPAccountOwner result = new PSPAccountOwner();
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_pspAccountOwnerSel")
                .declareParameters(
                        new SqlParameter("AccountOwnerId", Types.BIGINT));
        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("AccountOwnerId", id);

        Map<String, Object> resultExecuted = simpleJdbcCall.
                returningResultSet("records", new PSPAccountOwnerMapper()).
                execute(sqlParameterSource);
        if (((List) resultExecuted.get("records")).size() > 0)
            result = ((List<PSPAccountOwner>) resultExecuted.get("records")).get(0);

        return result;
    }

    @Override
    public List<PSPAccountOwner> findAll() throws SQLException, DataAccessException
    {
        List<PSPAccountOwner> result = new ArrayList<>();
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_pspAccountOwnerSel")
                .declareParameters(
                        new SqlParameter("AccountOwnerId", Types.BIGINT));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("AccountOwnerId", -1);

        Map<String, Object> resultExecuted = simpleJdbcCall.
                returningResultSet("records", new PSPAccountOwnerMapper()).
                execute(sqlParameterSource);
        if (((List) resultExecuted.get("records")).size() > 0)
            result = (List<PSPAccountOwner>) resultExecuted.get("records");

        return result;
    }

    @Override
    public boolean delete(Long id) throws SQLException, DataAccessException
    {
        return false;
    }

    @Override
    public boolean update(PSPAccountOwner entity) throws SQLException, DataAccessException
    {
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_pspAccountOwnerUpd")
                .declareParameters(
                        new SqlParameter("UserCallerId", Types.BIGINT),
                        new SqlParameter("AccountOwnerId", Types.BIGINT),
                        new SqlParameter("PSPRequestId", Types.BIGINT),
                        new SqlParameter("ClientNumber", Types.NVARCHAR),
                        new SqlParameter("ClientName", Types.NVARCHAR),
                        new SqlParameter("IsActive", Types.BIT),
                        new SqlParameter("ClientType", Types.NVARCHAR),
                        new SqlParameter("Ip", Types.NVARCHAR),
                        new SqlParameter("ComputerName", Types.NVARCHAR));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("UserCallerId", UserUtil.getCurrentUser().getUserId());
        sqlParameterSource.addValue("AccountOwnerId", entity.getAccountOwnerId());
        sqlParameterSource.addValue("PSPRequestId", entity.getPspRequestId());
        sqlParameterSource.addValue("ClientNumber", entity.getClientNumber());
        sqlParameterSource.addValue("ClientName", entity.getClientName());
        sqlParameterSource.addValue("IsActive", entity.isActive());
        sqlParameterSource.addValue("ClientType", entity.getClientType());
        sqlParameterSource.addValue("Ip", UserUtil.getCurrentUser().getIp());
        sqlParameterSource.addValue("ComputerName", UserUtil.getCurrentUser().getComputerName());

        Map<String, Object> resultExecuted = simpleJdbcCall.execute(sqlParameterSource);
        return (Integer) resultExecuted.get("#update-count-1") == 1;
    }

    @Override
    public List<PSPAccountOwner> search(PSPAccountOwner entity) throws SQLException, DataAccessException
    {
        return null;
    }
}
