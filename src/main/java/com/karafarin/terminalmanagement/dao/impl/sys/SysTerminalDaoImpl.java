package com.karafarin.terminalmanagement.dao.impl.sys;

import com.karafarin.terminalmanagement.config.jdbc.SqlConnectionConfiguration;
import com.karafarin.terminalmanagement.dao.intf.sys.SysTerminalDao;
import com.karafarin.terminalmanagement.dao.mapper.sys.SysTerminalMapper;
import com.karafarin.terminalmanagement.dto.sys.SysTerminal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository
public class SysTerminalDaoImpl implements SysTerminalDao
{

    @Autowired
    SqlConnectionConfiguration sqlConnectionConfiguration;
    private JdbcTemplate jdbcTemplate = null;
    private SimpleJdbcCall simpleJdbcCall;

    @PostConstruct
    public void postConstruct()
    {
        if (jdbcTemplate == null) jdbcTemplate = sqlConnectionConfiguration.getJdbcTemplate();
    }

    @Override
    public Long add(SysTerminal entity) throws SQLException, DataAccessException
    {
        return null;
    }

    @Override
    public SysTerminal findById(Long id) throws SQLException, DataAccessException
    {
        SysTerminal result = new SysTerminal();
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_sysTerminalSel")
                .declareParameters(
                        new SqlParameter("TerminalId", Types.BIGINT));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("TerminalId", id);

        Map<String, Object> resultExecuted = simpleJdbcCall.
                returningResultSet("records", new SysTerminalMapper()).
                execute(sqlParameterSource);
        if (((List) resultExecuted.get("records")).size() > 0)
            result = ((List<SysTerminal>) resultExecuted.get("records")).get(0);

        return result;
    }

    @Override
    public List<SysTerminal> findAll() throws SQLException, DataAccessException
    {
        List<SysTerminal> result = new ArrayList<>();
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_sysTerminalSel")
                .declareParameters(
                        new SqlParameter("TerminalId", Types.BIGINT));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("TerminalId", -1);

        Map<String, Object> resultExecuted = simpleJdbcCall.
                returningResultSet("records", new SysTerminalMapper()).
                execute(sqlParameterSource);
        if (((List) resultExecuted.get("records")).size() > 0)
            result = (List<SysTerminal>) resultExecuted.get("records");

        return result;
    }

    @Override
    public List<SysTerminal> findTerminalsByFilter(Long hideId) throws SQLException, DataAccessException
    {
        List<SysTerminal> result = new ArrayList<>();
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_sysTerminalSelByFilter")
                      .declareParameters(
                              new SqlParameter("TerminalId", Types.BIGINT));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("TerminalId", hideId);

        Map<String, Object> resultExecuted = simpleJdbcCall.
                                                                   returningResultSet("records", new SysTerminalMapper()).
                                                                   execute(sqlParameterSource);
        if (((List) resultExecuted.get("records")).size() > 0)
            result = (List<SysTerminal>) resultExecuted.get("records");

        return result;
    }

    @Override
    public boolean update(SysTerminal entity) throws SQLException, DataAccessException
    {
        return false;
    }

    @Override
    public List<SysTerminal> search(SysTerminal entity) throws SQLException, DataAccessException
    {
        return null;
    }

    @Override
    public boolean delete(Long id) throws SQLException, DataAccessException
    {
        return false;
    }

    @Override
    public List<SysTerminal> findTerminalsRequest(Long companyId) throws SQLException, DataAccessException
    {
        List<SysTerminal> result = new ArrayList<>();
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_TerminalRequestSel")
                .declareParameters(
                        new SqlParameter("CompanyId", Types.BIGINT));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("CompanyId", companyId);

        Map<String, Object> resultExecuted = simpleJdbcCall.
                returningResultSet("records", new SysTerminalMapper()).
                execute(sqlParameterSource);
        if (((List) resultExecuted.get("records")).size() > 0)
            result = (List<SysTerminal>) resultExecuted.get("records");

        return result;
    }
}
