package com.karafarin.terminalmanagement.dao.impl.psp;

import com.karafarin.terminalmanagement.config.jdbc.SqlConnectionConfiguration;
import com.karafarin.terminalmanagement.dao.intf.psp.PSPShopDao;
import com.karafarin.terminalmanagement.dao.mapper.psp.PSPShopMapper;
import com.karafarin.terminalmanagement.dto.psp.PSPShop;
import com.karafarin.terminalmanagement.util.UserUtil;
import com.karafarin.terminalmanagement.util.date.DateConverterUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository
public class PSPShopDaoImpl implements PSPShopDao
{
    @Autowired
    private SqlConnectionConfiguration sqlConnectionConfiguration;

    private JdbcTemplate jdbcTemplate = null;
    private SimpleJdbcCall simpleJdbcCall;

    @PostConstruct
    public void postConstruct()
    {
        if (jdbcTemplate == null)
            jdbcTemplate = sqlConnectionConfiguration.getJdbcTemplate();
    }

    @Override
    public Long add(PSPShop entity) throws SQLException, DataAccessException
    {
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_pspShopIns")
                .declareParameters(
                        new SqlParameter("UserCallerId", Types.BIGINT),
                        new SqlParameter("PSPRequestId", Types.BIGINT),
                        new SqlParameter("ShopName", Types.NVARCHAR),
                        new SqlParameter("ShopNameEn", Types.NVARCHAR),
                        new SqlParameter("OwnershipType", Types.NVARCHAR),
                        new SqlParameter("GuildId", Types.BIGINT),
                        new SqlParameter("ContractNo", Types.NVARCHAR),
                        new SqlParameter("ContractExpiryDate", Types.DATE),
                        new SqlParameter("LicenseNo", Types.NVARCHAR),
                        new SqlParameter("LicenseIssuanceDate", Types.DATE),
                        new SqlParameter("LicenseValidityDate", Types.DATE),
                        new SqlParameter("CountryNameAbbr", Types.NVARCHAR),
                        new SqlParameter("CountryName", Types.NVARCHAR),
                        new SqlParameter("StateId", Types.BIGINT),
                        new SqlParameter("CityId", Types.BIGINT),
                        new SqlParameter("MunicipalityNo", Types.NVARCHAR),
                        new SqlParameter("PostalCode", Types.NVARCHAR),
                        new SqlParameter("Address", Types.NVARCHAR),
                        new SqlParameter("Phone", Types.NVARCHAR),
                        new SqlParameter("Fax", Types.NVARCHAR),
                        new SqlParameter("Ip", Types.NVARCHAR),
                        new SqlParameter("ComputerName", Types.NVARCHAR));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("UserCallerId", UserUtil.getCurrentUser().getUserId());
        sqlParameterSource.addValue("PSPRequestId", entity.getPspRequestId());
        sqlParameterSource.addValue("ShopName", entity.getShopName());
        sqlParameterSource.addValue("ShopNameEn", entity.getShopNameEn());
        sqlParameterSource.addValue("OwnershipType", entity.getOwnershipType());
        sqlParameterSource.addValue("GuildId", entity.getGuildId());
        sqlParameterSource.addValue("ContractNo", entity.getContractNo());
        sqlParameterSource.addValue("ContractExpiryDate", DateConverterUtil
                .convertStringWithAndWithoutDelimiterToDate(entity.getContractExpiryDate()));
        sqlParameterSource.addValue("LicenseNo", entity.getLicenseNo());
        sqlParameterSource.addValue("LicenseIssuanceDate", DateConverterUtil
                .convertStringWithAndWithoutDelimiterToDate(entity.getLicenseIssuanceDate()));
        sqlParameterSource.addValue("LicenseValidityDate", DateConverterUtil
                .convertStringWithAndWithoutDelimiterToDate(entity.getLicenseValidityDate()));
        sqlParameterSource.addValue("CountryNameAbbr", entity.getCountryNameAbbr());
        sqlParameterSource.addValue("CountryName", entity.getCountryName());
        sqlParameterSource.addValue("StateId", entity.getStateId());
        sqlParameterSource.addValue("CityId", entity.getCityId());
        sqlParameterSource.addValue("MunicipalityNo", entity.getMunicipalityNo());
        sqlParameterSource.addValue("PostalCode", entity.getPostalCode());
        sqlParameterSource.addValue("Address", entity.getAddress());
        sqlParameterSource.addValue("Phone", entity.getPhone());
        sqlParameterSource.addValue("Fax", entity.getFax());
        sqlParameterSource.addValue("Ip", UserUtil.getCurrentUser().getIp());
        sqlParameterSource.addValue("ComputerName", UserUtil.getCurrentUser().getComputerName());

        Map<String, Object> resultExecuted = simpleJdbcCall.execute(sqlParameterSource);
        return (Long) resultExecuted.get("RETURN_VALUE");
    }

    @Override
    public PSPShop findById(Long id) throws SQLException, DataAccessException
    {
        PSPShop result = new PSPShop();
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_pspShopSel")
                .declareParameters(
                        new SqlParameter("ShopId", Types.BIGINT));
        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("ShopId", id);

        Map<String, Object> resultExecuted = simpleJdbcCall.
                returningResultSet("records", new PSPShopMapper()).
                execute(sqlParameterSource);
        if (((List) resultExecuted.get("records")).size() > 0)
            result = ((List<PSPShop>) resultExecuted.get("records")).get(0);

        return result;
    }

    @Override
    public List<PSPShop> findAll() throws SQLException, DataAccessException
    {
        List<PSPShop> result = new ArrayList<>();
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_pspShopSel")
                .declareParameters(
                        new SqlParameter("ShopId", Types.BIGINT));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("ShopId", -1);

        Map<String, Object> resultExecuted = simpleJdbcCall.
                returningResultSet("records", new PSPShopMapper()).
                execute(sqlParameterSource);
        if (((List) resultExecuted.get("records")).size() > 0)
            result = (List<PSPShop>) resultExecuted.get("records");

        return result;
    }

    @Override
    public boolean delete(Long id) throws SQLException, DataAccessException
    {
        return false;
    }

    @Override
    public boolean update(PSPShop entity) throws SQLException, DataAccessException
    {
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_pspShopUpd")
                .declareParameters(
                        new SqlParameter("UserCallerId", Types.BIGINT),
                        new SqlParameter("ShopId", Types.BIGINT),
                        new SqlParameter("PSPRequestId", Types.BIGINT),
                        new SqlParameter("ShopName", Types.NVARCHAR),
                        new SqlParameter("ShopNameEn", Types.NVARCHAR),
                        new SqlParameter("OwnershipType", Types.NVARCHAR),
                        new SqlParameter("GuildId", Types.BIGINT),
                        new SqlParameter("ContractNo", Types.NVARCHAR),
                        new SqlParameter("ContractExpiryDate", Types.DATE),
                        new SqlParameter("LicenseNo", Types.NVARCHAR),
                        new SqlParameter("LicenseIssuanceDate", Types.DATE),
                        new SqlParameter("LicenseValidityDate", Types.DATE),
                        new SqlParameter("CountryNameAbbr", Types.NVARCHAR),
                        new SqlParameter("CountryName", Types.NVARCHAR),
                        new SqlParameter("StateId", Types.BIGINT),
                        new SqlParameter("CityId", Types.BIGINT),
                        new SqlParameter("MunicipalityNo", Types.NVARCHAR),
                        new SqlParameter("PostalCode", Types.NVARCHAR),
                        new SqlParameter("Address", Types.NVARCHAR),
                        new SqlParameter("Phone", Types.NVARCHAR),
                        new SqlParameter("Fax", Types.NVARCHAR),
                        new SqlParameter("Ip", Types.NVARCHAR),
                        new SqlParameter("ComputerName", Types.NVARCHAR));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("UserCallerId", UserUtil.getCurrentUser().getUserId());
        sqlParameterSource.addValue("AccountId", entity.getShopId());
        sqlParameterSource.addValue("PSPRequestId", entity.getPspRequestId());
        sqlParameterSource.addValue("ShopName", entity.getShopName());
        sqlParameterSource.addValue("ShopNameEn", entity.getShopNameEn());
        sqlParameterSource.addValue("OwnershipType", entity.getOwnershipType());
        sqlParameterSource.addValue("GuildId", entity.getGuildId());
        sqlParameterSource.addValue("ContractNo", entity.getContractNo());
        sqlParameterSource.addValue("ContractExpiryDate", DateConverterUtil
                .convertStringWithAndWithoutDelimiterToDate(entity.getContractExpiryDate()));
        sqlParameterSource.addValue("LicenseNo", entity.getLicenseNo());
        sqlParameterSource.addValue("LicenseIssuanceDate", DateConverterUtil
                .convertStringWithAndWithoutDelimiterToDate(entity.getLicenseIssuanceDate()));
        sqlParameterSource.addValue("LicenseValidityDate", DateConverterUtil
                .convertStringWithAndWithoutDelimiterToDate(entity.getLicenseValidityDate()));
        sqlParameterSource.addValue("CountryNameAbbr", entity.getCountryNameAbbr());
        sqlParameterSource.addValue("CountryName", entity.getCountryName());
        sqlParameterSource.addValue("StateId", entity.getStateId());
        sqlParameterSource.addValue("CityId", entity.getCityId());
        sqlParameterSource.addValue("MunicipalityNo", entity.getMunicipalityNo());
        sqlParameterSource.addValue("PostalCode", entity.getPostalCode());
        sqlParameterSource.addValue("Address", entity.getAddress());
        sqlParameterSource.addValue("Phone", entity.getPhone());
        sqlParameterSource.addValue("Fax", entity.getFax());
        sqlParameterSource.addValue("Ip", UserUtil.getCurrentUser().getIp());
        sqlParameterSource.addValue("ComputerName", UserUtil.getCurrentUser().getComputerName());

        Map<String, Object> resultExecuted = simpleJdbcCall.execute(sqlParameterSource);
        return (Integer) resultExecuted.get("#update-count-1") == 1;
    }

    @Override
    public List<PSPShop> search(PSPShop entity) throws SQLException, DataAccessException
    {
        return null;
    }
}
