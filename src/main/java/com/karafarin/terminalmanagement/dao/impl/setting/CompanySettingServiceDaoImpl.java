package com.karafarin.terminalmanagement.dao.impl.setting;

import com.karafarin.terminalmanagement.config.jdbc.SqlConnectionConfiguration;
import com.karafarin.terminalmanagement.dao.intf.setting.CompanySettingServiceDao;
import com.karafarin.terminalmanagement.dao.mapper.setting.CompanySettingServiceMapper;
import com.karafarin.terminalmanagement.dto.setting.CompanySettingService;
import com.karafarin.terminalmanagement.util.UserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository
public class CompanySettingServiceDaoImpl implements CompanySettingServiceDao
{
    @Autowired
    private SqlConnectionConfiguration sqlConnectionConfiguration;

    private JdbcTemplate jdbcTemplate = null;
    private SimpleJdbcCall simpleJdbcCall;

    @PostConstruct
    public void postConstruct()
    {
        if (jdbcTemplate == null)
            jdbcTemplate = sqlConnectionConfiguration.getJdbcTemplate();
    }

    @Override
    public Long add(CompanySettingService entity) throws SQLException, DataAccessException
    {
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_CompanySettingServiceIns")
                .declareParameters(
                        new SqlParameter("UserCallerId", Types.BIGINT),
                        new SqlParameter("CompanySettingId", Types.BIGINT),
                        new SqlParameter("ServiceId", Types.BIGINT),
                        new SqlParameter("IsActive", Types.BOOLEAN),
                        new SqlParameter("Ip", Types.NVARCHAR),
                        new SqlParameter("ComputerName", Types.NVARCHAR));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("UserCallerId", UserUtil.getCurrentUser().getUserId());
        sqlParameterSource.addValue("CompanySettingId", entity.getCompanySettingId());
        sqlParameterSource.addValue("ServiceId", entity.getServiceId());
        sqlParameterSource.addValue("IsActive", entity.isActive());
        sqlParameterSource.addValue("Ip", UserUtil.getCurrentUser().getIp());
        sqlParameterSource.addValue("ComputerName", UserUtil.getCurrentUser().getComputerName());

        Map<String, Object> resultExecuted = simpleJdbcCall.execute(sqlParameterSource);
        return (Long) resultExecuted.get("RETURN_VALUE");
    }

    @Override
    public CompanySettingService findById(Long id) throws SQLException, DataAccessException
    {
        CompanySettingService result = new CompanySettingService();
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_CompanySettingServiceSel")
                .declareParameters(
                        new SqlParameter("CompanySettingServiceId", Types.BIGINT));
        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("CompanySettingServiceId", id);

        Map<String, Object> resultExecuted = simpleJdbcCall.
                returningResultSet("records", new CompanySettingServiceMapper()).
                execute(sqlParameterSource);
        if (((List) resultExecuted.get("records")).size() > 0)
            result = ((List<CompanySettingService>) resultExecuted.get("records")).get(0);

        return result;
    }

    @Override
    public List<CompanySettingService> findAll() throws SQLException, DataAccessException
    {
        List<CompanySettingService> result = new ArrayList<>();
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_CompanySettingServiceSel")
                .declareParameters(
                        new SqlParameter("CompanySettingServiceId", Types.BIGINT));
        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("CompanySettingServiceId", -1);

        Map<String, Object> resultExecuted = simpleJdbcCall.
                returningResultSet("records", new CompanySettingServiceMapper()).
                execute(sqlParameterSource);
        if (((List) resultExecuted.get("records")).size() > 0)
            result = (List<CompanySettingService>) resultExecuted.get("records");

        return result;
    }

    @Override
    public boolean delete(Long id) throws SQLException, DataAccessException
    {
        return false;
    }

    @Override
    public boolean update(CompanySettingService entity) throws SQLException, DataAccessException
    {
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_CompanySettingServiceUpd")
                .declareParameters(
                        new SqlParameter("UserCallerId", Types.BIGINT),
                        new SqlParameter("CompanySettingServiceId", Types.BIGINT),
                        new SqlParameter("ServiceId", Types.BIGINT),
                        new SqlParameter("IsActive", Types.BOOLEAN),
                        new SqlParameter("Ip", Types.NVARCHAR),
                        new SqlParameter("ComputerName", Types.NVARCHAR));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("UserCallerId", UserUtil.getCurrentUser().getUserId());
        sqlParameterSource.addValue("CompanySettingServiceId", entity.getCompanySettingServiceId());
        sqlParameterSource.addValue("ServiceId", entity.getServiceId());
        sqlParameterSource.addValue("IsActive", entity.isActive());
        sqlParameterSource.addValue("Ip", UserUtil.getCurrentUser().getIp());
        sqlParameterSource.addValue("ComputerName", UserUtil.getCurrentUser().getComputerName());

        Map<String, Object> resultExecuted = simpleJdbcCall.execute(sqlParameterSource);
        return (Integer) resultExecuted.get("#update-count-1") == 1;
    }

    @Override
    public List<CompanySettingService> search(CompanySettingService entity) throws SQLException, DataAccessException
    {
        return null;
    }

    @Override
    public List<CompanySettingService> findByCompanySettingId(Long id) throws SQLException, DataAccessException
    {
        List<CompanySettingService> result = new ArrayList<>();
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_CompanySettingServiceByMasterSel")
                .declareParameters(
                        new SqlParameter("CompanySettingId", Types.BIGINT));
        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("CompanySettingId", id);

        Map<String, Object> resultExecuted = simpleJdbcCall.
                returningResultSet("records", new CompanySettingServiceMapper()).
                execute(sqlParameterSource);
        if (((List) resultExecuted.get("records")).size() > 0)
            result = (List<CompanySettingService>) resultExecuted.get("records");

        return result;
    }
}
