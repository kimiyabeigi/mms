package com.karafarin.terminalmanagement.dao.impl.base;

import com.karafarin.terminalmanagement.config.jdbc.SqlConnectionConfiguration;
import com.karafarin.terminalmanagement.dao.intf.base.BaseStateDao;
import com.karafarin.terminalmanagement.dao.mapper.base.BaseStateMapper;
import com.karafarin.terminalmanagement.dto.base.BaseState;
import com.karafarin.terminalmanagement.util.UserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository
public class BaseStateDaoImpl implements BaseStateDao
{

    @Autowired
    private SqlConnectionConfiguration sqlConnectionConfiguration;

    private JdbcTemplate jdbcTemplate = null;
    private SimpleJdbcCall simpleJdbcCall;

    @PostConstruct
    public void postConstruct()
    {
        if (jdbcTemplate == null)
            jdbcTemplate = sqlConnectionConfiguration.getJdbcTemplate();
    }

    @Override
    public Long add(BaseState entity) throws SQLException, DataAccessException
    {
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_BaseStateIns")
                .declareParameters(
                        new SqlParameter("UserCallerId", Types.BIGINT),
                        new SqlParameter("StateId", Types.BIGINT),
                        new SqlParameter("StateCode", Types.NVARCHAR),
                        new SqlParameter("Name", Types.NVARCHAR),
                        new SqlParameter("NameEN", Types.NVARCHAR),
                        new SqlParameter("Ip", Types.NVARCHAR),
                        new SqlParameter("IsActive", Types.BIT),
                        new SqlParameter("ComputerName", Types.NVARCHAR));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();

        sqlParameterSource.addValue("UserCallerId", UserUtil.getCurrentUser().getUserId());
        sqlParameterSource.addValue("StateId", entity.getStateId());
        sqlParameterSource.addValue("StateCode", entity.getStateCode());
        sqlParameterSource.addValue("Name", entity.getName());
        sqlParameterSource.addValue("NameEN", entity.getNameEN());
        sqlParameterSource.addValue("Ip", UserUtil.getCurrentUser().getIp());
        sqlParameterSource.addValue("IsActive", entity.isActive());
        sqlParameterSource.addValue("ComputerName", UserUtil.getCurrentUser().getComputerName());

        Map<String, Object> resultExecuted = simpleJdbcCall.execute(sqlParameterSource);
        return (Long) resultExecuted.get("RETURN_VALUE");
    }

    @Override
    public BaseState findById(Long id) throws SQLException, DataAccessException
    {
        BaseState result = new BaseState();
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_BaseStateSel")
                .declareParameters(
                        new SqlParameter("StateId", Types.BIGINT),
                        new SqlParameter("All", Types.BIT));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("StateId", id);
        sqlParameterSource.addValue("All", 1);

        Map<String, Object> resultExecuted = simpleJdbcCall.
                returningResultSet("records", new BaseStateMapper()).
                execute(sqlParameterSource);
        if (((List) resultExecuted.get("records")).size() > 0)
            result = ((List<BaseState>) resultExecuted.get("records")).get(0);

        return result;
    }

    @Override
    public List<BaseState> findAll() throws SQLException, DataAccessException
    {
        return null;
    }

    @Override
    public List<BaseState> findAll(boolean all) throws SQLException, DataAccessException
    {
        List<BaseState> result = new ArrayList<>();
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_BaseStateSel")
                .declareParameters(
                        new SqlParameter("StateId", Types.BIGINT),
                        new SqlParameter("All", Types.BIT));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("StateId", -1);
        sqlParameterSource.addValue("All", all);

        Map<String, Object> resultExecuted = simpleJdbcCall.
                returningResultSet("records", new BaseStateMapper()).
                execute(sqlParameterSource);
        if (((List) resultExecuted.get("records")).size() > 0)
            result = ((List<BaseState>) resultExecuted.get("records"));

        return result;
    }

    @Override
    public boolean delete(Long id) throws SQLException, DataAccessException
    {
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_BaseStateDel")
                .declareParameters(
                        new SqlParameter("UserCallerId", Types.BIGINT),
                        new SqlParameter("StateId", Types.BIGINT),
                        new SqlParameter("Ip", Types.NVARCHAR),
                        new SqlParameter("ComputerName", Types.NVARCHAR));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("UserCallerId", UserUtil.getCurrentUser().getUserId());
        sqlParameterSource.addValue("StateId", id);
        sqlParameterSource.addValue("Ip", UserUtil.getCurrentUser().getIp());
        sqlParameterSource.addValue("ComputerName", UserUtil.getCurrentUser().getComputerName());

        Map<String, Object> resultExecuted = simpleJdbcCall.execute(sqlParameterSource);
        return (Integer) resultExecuted.get("#update-count-1") == 1;
    }

    @Override
    public boolean update(BaseState entity) throws SQLException, DataAccessException
    {
        SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);

        simpleJdbcCall.withProcedureName("usp_BaseStateUpd")
                .declareParameters(
                        new SqlParameter("UserCallerId", Types.BIGINT),
                        new SqlParameter("StateId", Types.BIGINT),
                        new SqlParameter("StateCode", Types.NVARCHAR),
                        new SqlParameter("Name", Types.NVARCHAR),
                        new SqlParameter("NameEN", Types.NVARCHAR),
                        new SqlParameter("IsActive", Types.BIT),
                        new SqlParameter("Ip", Types.NVARCHAR),
                        new SqlParameter("ComputerName", Types.NVARCHAR));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("UserCallerId", UserUtil.getCurrentUser().getUserId());
        sqlParameterSource.addValue("StateId", entity.getStateId());
        sqlParameterSource.addValue("StateCode", entity.getStateCode());
        sqlParameterSource.addValue("Name", entity.getName());
        sqlParameterSource.addValue("NameEN", entity.getNameEN());
        sqlParameterSource.addValue("IsActive", entity.isActive());
        sqlParameterSource.addValue("Ip", UserUtil.getCurrentUser().getIp());
        sqlParameterSource.addValue("ComputerName", UserUtil.getCurrentUser().getComputerName());

        Map<String, Object> resultExecuted = simpleJdbcCall.execute(sqlParameterSource);
        return (Integer) resultExecuted.get("#update-count-1") == 1;
    }

    @Override
    public List<BaseState> search(BaseState entity) throws SQLException, DataAccessException
    {
        return null;
    }
}
