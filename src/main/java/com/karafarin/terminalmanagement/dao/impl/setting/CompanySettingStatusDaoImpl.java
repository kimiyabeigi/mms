package com.karafarin.terminalmanagement.dao.impl.setting;

import com.karafarin.terminalmanagement.config.jdbc.SqlConnectionConfiguration;
import com.karafarin.terminalmanagement.dao.intf.setting.CompanySettingStatusDao;
import com.karafarin.terminalmanagement.dao.mapper.setting.CompanySettingStatusMapper;
import com.karafarin.terminalmanagement.dto.setting.CompanySettingStatus;
import com.karafarin.terminalmanagement.util.UserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository
public class CompanySettingStatusDaoImpl implements CompanySettingStatusDao
{
    @Autowired
    private SqlConnectionConfiguration sqlConnectionConfiguration;

    private JdbcTemplate jdbcTemplate = null;
    private SimpleJdbcCall simpleJdbcCall;

    @PostConstruct
    public void postConstruct()
    {
        if (jdbcTemplate == null) jdbcTemplate = sqlConnectionConfiguration.getJdbcTemplate();
    }

    @Override
    public Long add(CompanySettingStatus entity) throws SQLException, DataAccessException
    {
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_CompanySettingStatusIns")
                      .declareParameters(new SqlParameter("UserCallerId", Types.BIGINT),
                                         new SqlParameter("CompanySettingId", Types.BIGINT),
                                         new SqlParameter("ServiceId", Types.BIGINT),
                                         new SqlParameter("IndexId", Types.BIGINT),
                                         new SqlParameter("StatusCode", Types.NVARCHAR),
                                         new SqlParameter("StatusTypeId", Types.BIGINT),
                                         new SqlParameter("Ip", Types.NVARCHAR),
                                         new SqlParameter("ComputerName", Types.NVARCHAR));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("UserCallerId", UserUtil.getCurrentUser().getUserId());
        sqlParameterSource.addValue("CompanySettingId", entity.getCompanySettingId());
        sqlParameterSource.addValue("ServiceId", entity.getServiceId());
        sqlParameterSource.addValue("IndexId", entity.getIndexId());
        sqlParameterSource.addValue("StatusCode", entity.getStatusCode());
        sqlParameterSource.addValue("StatusTypeId", entity.getStatusTypeId());
        sqlParameterSource.addValue("Ip", UserUtil.getCurrentUser().getIp());
        sqlParameterSource.addValue("ComputerName", UserUtil.getCurrentUser().getComputerName());

        Map<String, Object> resultExecuted = simpleJdbcCall.execute(sqlParameterSource);
        return (Long) resultExecuted.get("RETURN_VALUE");
    }

    @Override
    public CompanySettingStatus findById(Long id) throws SQLException, DataAccessException
    {
        CompanySettingStatus result = new CompanySettingStatus();
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_CompanySettingStatusSel")
                      .declareParameters(new SqlParameter("CompanySettingStatusId", Types.BIGINT));
        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("CompanySettingStatusId", id);

        Map<String, Object> resultExecuted = simpleJdbcCall.
                                                                   returningResultSet("records",
                                                                                      new CompanySettingStatusMapper()).
                                                                   execute(sqlParameterSource);
        if (((List) resultExecuted.get("records")).size() > 0)
            result = ((List<CompanySettingStatus>) resultExecuted.get("records")).get(0);

        return result;

    }

    @Override
    public List<CompanySettingStatus> findAll() throws SQLException, DataAccessException
    {
        List<CompanySettingStatus> result = new ArrayList<>();
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_CompanySettingStatusSel")
                      .declareParameters(new SqlParameter("CompanySettingStatusId", Types.BIGINT));
        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("CompanySettingStatusId", -1);

        Map<String, Object> resultExecuted = simpleJdbcCall.
                                                                   returningResultSet("records",
                                                                                      new CompanySettingStatusMapper()).
                                                                   execute(sqlParameterSource);
        if (((List) resultExecuted.get("records")).size() > 0)
            result = (List<CompanySettingStatus>) resultExecuted.get("records");

        return result;
    }

    @Override
    public boolean delete(Long id) throws SQLException, DataAccessException
    {
        return false;
    }

    @Override
    public boolean update(CompanySettingStatus entity) throws SQLException, DataAccessException
    {
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_CompanySettingStatusUpd")
                      .declareParameters(new SqlParameter("UserCallerId", Types.BIGINT),
                                         new SqlParameter("CompanySettingStatusId", Types.BIGINT),
                                         new SqlParameter("ServiceId", Types.BIGINT),
                                         new SqlParameter("IndexId", Types.BIGINT),
                                         new SqlParameter("StatusCode", Types.NVARCHAR),
                                         new SqlParameter("StatusTypeId", Types.BIGINT),
                                         new SqlParameter("Ip", Types.NVARCHAR),
                                         new SqlParameter("ComputerName", Types.NVARCHAR));

        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("UserCallerId", UserUtil.getCurrentUser().getUserId());
        sqlParameterSource.addValue("CompanySettingStatusId", entity.getCompanySettingStatusId());
        sqlParameterSource.addValue("ServiceId", entity.getServiceId());
        sqlParameterSource.addValue("IndexId", entity.getIndexId());
        sqlParameterSource.addValue("StatusCode", entity.getStatusCode());
        sqlParameterSource.addValue("StatusTypeId", entity.getStatusTypeId());
        sqlParameterSource.addValue("Ip", UserUtil.getCurrentUser().getIp());
        sqlParameterSource.addValue("ComputerName", UserUtil.getCurrentUser().getComputerName());

        Map<String, Object> resultExecuted = simpleJdbcCall.execute(sqlParameterSource);
        return (Integer) resultExecuted.get("#update-count-1") == 1;
    }

    @Override
    public List<CompanySettingStatus> search(CompanySettingStatus entity) throws SQLException, DataAccessException
    {
        return null;
    }

    @Override
    public List<CompanySettingStatus> findByCompanySettingId(Long id) throws SQLException, DataAccessException
    {
        List<CompanySettingStatus> result = new ArrayList<>();
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_CompanySettingStatusByMasterSel")
                      .declareParameters(new SqlParameter("CompanySettingId", Types.BIGINT));
        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("CompanySettingId", id);

        Map<String, Object> resultExecuted = simpleJdbcCall.
                                                                   returningResultSet("records",
                                                                                      new CompanySettingStatusMapper()).
                                                                   execute(sqlParameterSource);
        if (((List) resultExecuted.get("records")).size() > 0)
            result = (List<CompanySettingStatus>) resultExecuted.get("records");

        return result;
    }

    @Override
    public Long findStatusId(String statusCode, Long serviceId, String companyCode) throws SQLException,
                                                                                           DataAccessException
    {
        Long result = 3L;
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_StatusTerminalReqSel")
                      .declareParameters(new SqlParameter("StatusCode", Types.NVARCHAR),
                                         new SqlParameter("ServiceId", Types.BIGINT),
                                         new SqlParameter("CompanyCode", Types.NVARCHAR));
        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("StatusCode", statusCode);
        sqlParameterSource.addValue("ServiceId", serviceId);
        sqlParameterSource.addValue("CompanyCode", companyCode);

        Map<String, Object> resultExecuted = simpleJdbcCall.
                                                                   returningResultSet("records",
                                                                                      new CompanySettingStatusMapper()).
                                                                   execute(sqlParameterSource);
        if (((List) resultExecuted.get("records")).size() > 0)
            result = (Long) resultExecuted.get("records");

        return result;
    }
}
