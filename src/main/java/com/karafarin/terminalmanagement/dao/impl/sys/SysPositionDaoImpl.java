package com.karafarin.terminalmanagement.dao.impl.sys;

import com.karafarin.terminalmanagement.config.jdbc.SqlConnectionConfiguration;
import com.karafarin.terminalmanagement.dao.intf.sys.SysPositionDao;
import com.karafarin.terminalmanagement.dao.mapper.sys.SysPositionMapper;
import com.karafarin.terminalmanagement.dto.sys.SysPosition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.sql.SQLException;
import java.sql.Types;
import java.util.List;
import java.util.Map;

@Repository
public class SysPositionDaoImpl implements SysPositionDao
{
    @Autowired
    private SqlConnectionConfiguration sqlConnectionConfiguration;

    private JdbcTemplate jdbcTemplate = null;
    private SimpleJdbcCall simpleJdbcCall;

    @PostConstruct
    public void postConstruct()
    {
        if (jdbcTemplate == null) jdbcTemplate = sqlConnectionConfiguration.getJdbcTemplate();
    }

    @Override
    public Long add(SysPosition entity) throws SQLException, DataAccessException
    {
        return null;
    }

    @Override
    public SysPosition findById(Long id) throws SQLException, DataAccessException
    {
        SysPosition result = new SysPosition();
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
        simpleJdbcCall.withProcedureName("usp_sysPositionSel")
                      .declareParameters(new SqlParameter("PositionId", Types.BIGINT));
        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("PositionId", id);

        Map<String, Object> resultExecuted = simpleJdbcCall.
                                                                   returningResultSet("records",
                                                                                      new SysPositionMapper()).
                                                                   execute(sqlParameterSource);
        if (((List) resultExecuted.get("records")).size() > 0)
            result = ((List<SysPosition>) resultExecuted.get("records")).get(0);

        return result;
    }

    @Override
    public List<SysPosition> findAll() throws SQLException, DataAccessException
    {
        return null;
    }

    @Override
    public boolean delete(Long id) throws SQLException, DataAccessException
    {
        return false;
    }

    @Override
    public boolean update(SysPosition entity) throws SQLException, DataAccessException
    {
        return false;
    }

    @Override
    public List<SysPosition> search(SysPosition entity) throws SQLException, DataAccessException
    {
        return null;
    }
}
