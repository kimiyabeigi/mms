package com.karafarin.terminalmanagement.dao.mapper.psp;

import com.karafarin.terminalmanagement.dto.psp.PSPStatusBatch;
import com.karafarin.terminalmanagement.util.date.DateConverterUtil;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PSPStatusBatchMapper implements RowMapper<PSPStatusBatch>
{
    @Override
    public PSPStatusBatch mapRow(ResultSet resultSet, int rowNumber) throws SQLException
    {
        PSPStatusBatch pspStatusBatch = new PSPStatusBatch();

        pspStatusBatch.setAccountNo(resultSet.getString("AccountNo"));
        pspStatusBatch.setClientNumber(resultSet.getString("ClientNumber"));
        pspStatusBatch.setBranchName(resultSet.getString("BranchName"));
        pspStatusBatch.setRequestNumber(resultSet.getLong("RequestNumber"));
        pspStatusBatch.setPspTrackingCode(resultSet.getString("PSPTrackingCode"));
        pspStatusBatch.setPspAcceptanceCode(resultSet.getString("PSPAcceptanceCode"));
        pspStatusBatch.setPspTerminalCode(resultSet.getString("PSPTerminalCode"));
        pspStatusBatch.setRequestStatusId(resultSet.getLong("RequestStatusId"));
        pspStatusBatch.setCreateDate(DateConverterUtil
                                             .getAsStringWithSeparator(resultSet.getDate("CreateDate")));
        pspStatusBatch.setStatus(resultSet.getString("Status"));
        pspStatusBatch.setPspRequestId(resultSet.getLong("PSPRequestId"));
        pspStatusBatch.setCompanyCode(resultSet.getString("CompanyCode"));

        return pspStatusBatch;
    }
}
