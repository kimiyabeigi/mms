package com.karafarin.terminalmanagement.dao.mapper.setting;

import com.karafarin.terminalmanagement.dto.setting.CompanySetting;
import com.karafarin.terminalmanagement.dto.setting.CompanySettingDetail;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CompanySettingMapper implements ResultSetExtractor
{
    @Override
    public Object extractData(ResultSet resultSet) throws SQLException, DataAccessException
    {
        Map<Long, CompanySetting> companyMap = new HashMap<Long, CompanySetting>();

        while (resultSet.next()) {
            Long companySettingId = resultSet.getLong("CompanySettingId");
            CompanySetting company = companyMap.get(companySettingId);
            if(company == null){
                company = new CompanySetting();

                company.setCompanyId(resultSet.getLong("CompanyId"));
                company.setActive(resultSet.getBoolean("IsActive"));
                company.setOrganizationCode(resultSet.getString("OrganizationCode"));
                company.setCompanySettingId(resultSet.getLong("CompanySettingId"));

                companyMap.put(companySettingId, company);
            }

            //Start of adding companySettingDetail
            List companySettingDetails = company.getCompanySettingDetails();
            if(companySettingDetails == null) {
                companySettingDetails = new ArrayList<CompanySettingDetail>();
                company.setCompanySettingDetails(companySettingDetails);
            }
            CompanySettingDetail companySettingDetail = new CompanySettingDetail();
            companySettingDetail.setCompanySettingDetailId(resultSet.getLong("CompanySettingDetailId"));
            companySettingDetail.setCompanySettingId(resultSet.getLong("CompanySettingId"));
            companySettingDetail.setTerminalId(resultSet.getLong("TerminalId"));
            companySettingDetail.setSelected(resultSet.getBoolean("Selected"));

            companySettingDetails.add(companySettingDetail);
            //End of adding companySettingDetail
        }
        return new ArrayList<CompanySetting>(companyMap.values());
    }
}
