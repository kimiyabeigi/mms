package com.karafarin.terminalmanagement.dao.mapper.base;

import com.karafarin.terminalmanagement.dto.base.BaseGuild;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class BaseGuildMapper implements RowMapper<BaseGuild>
{

    @Override
    public BaseGuild mapRow(ResultSet resultSet, int i) throws SQLException
    {

        BaseGuild baseGuild = new BaseGuild();

        baseGuild.setGuildId(resultSet.getLong("GuildId"));
        baseGuild.setGuildCode(resultSet.getString("GuildCode"));
        baseGuild.setDescription(resultSet.getString("Description"));
        baseGuild.setActive(resultSet.getBoolean("IsActive"));

        return baseGuild;
    }
}
