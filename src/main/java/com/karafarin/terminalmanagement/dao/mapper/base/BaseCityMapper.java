package com.karafarin.terminalmanagement.dao.mapper.base;

import com.karafarin.terminalmanagement.dto.base.BaseCity;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class BaseCityMapper implements RowMapper<BaseCity>
{
    @Override
    public BaseCity mapRow(ResultSet resultSet, int rowNumber) throws SQLException
    {
        BaseCity baseCity = new BaseCity();

        baseCity.setCityId(resultSet.getLong("CityId"));
        baseCity.setCityCode(resultSet.getString("CityCode"));
        baseCity.setStateId(resultSet.getLong("StateId"));
        baseCity.setName(resultSet.getNString("Name"));
        baseCity.setNameEN(resultSet.getNString("NameEN"));
        baseCity.setActive(resultSet.getBoolean("IsActive"));

        return baseCity;
    }
}
