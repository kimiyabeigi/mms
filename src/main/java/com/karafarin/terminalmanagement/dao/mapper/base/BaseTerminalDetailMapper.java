package com.karafarin.terminalmanagement.dao.mapper.base;

import com.karafarin.terminalmanagement.dto.base.BaseTerminalDetail;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class BaseTerminalDetailMapper implements RowMapper<BaseTerminalDetail>
{
    @Override
    public BaseTerminalDetail mapRow(ResultSet resultSet, int rowNumber) throws SQLException
    {
        BaseTerminalDetail baseTerminalDetail = new BaseTerminalDetail();

        baseTerminalDetail.setTerminalDetailId(resultSet.getLong("TerminalDetailId"));
        baseTerminalDetail.setTerminalDetailCode(resultSet.getString("TerminalDetailCode"));
        baseTerminalDetail.setTerminalId(resultSet.getLong("TerminalId"));
        baseTerminalDetail.setName(resultSet.getNString("Name"));

        return baseTerminalDetail;
    }
}
