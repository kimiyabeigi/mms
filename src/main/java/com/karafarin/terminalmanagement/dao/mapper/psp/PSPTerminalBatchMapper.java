package com.karafarin.terminalmanagement.dao.mapper.psp;

import com.karafarin.terminalmanagement.dto.psp.PSPTerminalBatch;
import com.karafarin.terminalmanagement.util.date.DateConverterUtil;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PSPTerminalBatchMapper implements RowMapper<PSPTerminalBatch>
{
    @Override
    public PSPTerminalBatch mapRow(ResultSet resultSet, int rowNumber) throws SQLException
    {
        PSPTerminalBatch pspTerminalBatch = new PSPTerminalBatch();

        pspTerminalBatch.setAccountNo(resultSet.getString("AccountNo"));
        pspTerminalBatch.setClientNumber(resultSet.getString("ClientNumber"));
        pspTerminalBatch.setBranchName(resultSet.getString("BranchName"));
        pspTerminalBatch.setRequestNumber(resultSet.getLong("RequestNumber"));
        pspTerminalBatch.setPspTrackingCode(resultSet.getString("PSPTrackingCode"));
        pspTerminalBatch.setPspAcceptanceCode(resultSet.getString("PSPAcceptanceCode"));
        pspTerminalBatch.setPspTerminalCode(resultSet.getString("PSPTerminalCode"));
        pspTerminalBatch.setPspRequestId(resultSet.getLong("PSPRequestId"));
        pspTerminalBatch.setStatus(resultSet.getString("Status"));
        pspTerminalBatch.setCreateDate(DateConverterUtil
                                               .getAsStringWithSeparator(resultSet.getDate("CreateDate")));
        pspTerminalBatch.setRequestStatusId(resultSet.getLong("RequestStatusId"));
        pspTerminalBatch.setPspRequestStatusId(resultSet.getLong("RequestStatusId"));
        pspTerminalBatch.setCompanyCode(resultSet.getString("CompanyCode"));

        return pspTerminalBatch;
    }
}
