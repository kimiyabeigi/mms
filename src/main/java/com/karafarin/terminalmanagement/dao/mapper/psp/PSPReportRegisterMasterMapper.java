package com.karafarin.terminalmanagement.dao.mapper.psp;

import com.karafarin.terminalmanagement.dto.psp.PSPReportRegisterMaster;
import com.karafarin.terminalmanagement.util.date.DateConverterUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PSPReportRegisterMasterMapper implements RowMapper<PSPReportRegisterMaster>
{
    @Override
    public PSPReportRegisterMaster mapRow(ResultSet resultSet, int rowNumber) throws SQLException
    {
        PSPReportRegisterMaster pspReportRegisterMaster = new PSPReportRegisterMaster();

        pspReportRegisterMaster.setPspRequestId(resultSet.getLong("PSPRequestId"));
        pspReportRegisterMaster.setRequestDate(
                DateConverterUtil.getAsStringWithSeparator(resultSet.getDate("RequestDate")));
        pspReportRegisterMaster.setAccountNo(resultSet.getString("AccountNo"));
        pspReportRegisterMaster.setClientNumber(resultSet.getString("ClientNumber"));
        pspReportRegisterMaster.setBranch(resultSet.getString("Branch"));
        pspReportRegisterMaster.setCompanyName(resultSet.getString("CompanyName"));
        pspReportRegisterMaster.setRequestStatus(resultSet.getString("RequestStatus"));
        pspReportRegisterMaster.setRequestNumber(resultSet.getLong("RequestNumber"));
        pspReportRegisterMaster.setPspTrackingCode(resultSet.getString("PSPTrackingCode") != null ?
                                                   resultSet.getString("PSPTrackingCode") : "");
        pspReportRegisterMaster.setPspAcceptanceCode(resultSet.getString("PSPAcceptanceCode") != null ?
                                                     resultSet.getString("PSPAcceptanceCode") : "");
        pspReportRegisterMaster.setPspTerminalCode(resultSet.getString("PSPTerminalCode") != null ?
                                                   resultSet.getString("PSPTerminalCode") : "");

        return pspReportRegisterMaster;
    }
}
