package com.karafarin.terminalmanagement.dao.mapper.psp;

import com.karafarin.terminalmanagement.dto.psp.PSPIPG;
import com.karafarin.terminalmanagement.util.date.DateConverterUtil;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PSPIPGMapper implements RowMapper<PSPIPG>
{
    @Override
    public PSPIPG mapRow(ResultSet resultSet, int rowNumber) throws SQLException
    {
        PSPIPG pspipg = new PSPIPG();

        pspipg.setIPGId(resultSet.getLong("IPGId"));
        pspipg.setPspRequestId(resultSet.getLong("PSPRequestId"));
        pspipg.seteNamadId(resultSet.getLong("ENamadId"));
        pspipg.setSiteAddress(resultSet.getString("SiteAddress"));
        pspipg.setIssuanceDate(DateConverterUtil
                .getAsStringWithSeparator(resultSet.getDate("IssuanceDate")));
        pspipg.setEmail(resultSet.getString("Email"));
        pspipg.setExpiryDate(DateConverterUtil
                .getAsStringWithSeparator(resultSet.getDate("ExpiryDate")));
        pspipg.setFirstName(resultSet.getString("FirstName"));
        pspipg.setLastName(resultSet.getString("LastName"));
        pspipg.setPhone(resultSet.getString("Phone"));
        pspipg.setMobilePhone(resultSet.getString("MobilePhone"));
        pspipg.setTerminalDetailId(resultSet.getLong("TerminalDetailId"));

        return pspipg;
    }
}
