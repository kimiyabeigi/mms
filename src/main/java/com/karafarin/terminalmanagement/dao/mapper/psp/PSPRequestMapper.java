package com.karafarin.terminalmanagement.dao.mapper.psp;


import com.karafarin.terminalmanagement.dto.psp.PSPRequest;
import com.karafarin.terminalmanagement.util.date.DateConverterUtil;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PSPRequestMapper implements RowMapper<PSPRequest>
{
    @Override
    public PSPRequest mapRow(ResultSet resultSet, int rowNumber) throws SQLException
    {
        PSPRequest pspRequest = new PSPRequest();

        pspRequest.setPspRequestId(resultSet.getLong("PSPRequestId"));
        pspRequest.setCompanyId(resultSet.getLong("CompanyId"));
        pspRequest.setTerminalId(resultSet.getLong("TerminalId"));
        pspRequest.setRequestStatusId(resultSet.getLong("RequestStatusId"));
        pspRequest.setPspAcceptanceCode(resultSet.getString("PSPAcceptanceCode"));
        pspRequest.setPspTerminalCode(resultSet.getString("PSPTerminalCode"));
        pspRequest.setPspTrackingCode(resultSet.getString("PSPTrackingCode"));
        pspRequest.setIpAddress(resultSet.getString("IPAddress"));
        pspRequest.setComputerName(resultSet.getString("ComputerName"));
        pspRequest.setRequestDate(DateConverterUtil
                .getAsStringWithSeparator(resultSet.getDate("RequestDate")));
        pspRequest.setUserId(resultSet.getLong("UserId"));
        pspRequest.setBranchId(resultSet.getLong("BranchId"));
        pspRequest.setRequestNumber(resultSet.getLong("RequestNumber"));

        return pspRequest;
    }
}
