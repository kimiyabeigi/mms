package com.karafarin.terminalmanagement.dao.mapper.auth;

import com.karafarin.terminalmanagement.dto.auth.User;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserMapper implements RowMapper<User>
{
    @Override
    public User mapRow(ResultSet resultSet, int rowNumber) throws SQLException
    {
        User user = new User();

        user.setUserId(resultSet.getLong("UserId"));
        user.setUserCode(resultSet.getString("UserCode"));
        user.setFirstName(resultSet.getString("FirstName"));
        user.setLastName(resultSet.getString("LastName"));
        user.setUserName(resultSet.getString("UserName"));
        user.setPassword(resultSet.getString("Password"));
        user.setRoleId(resultSet.getLong("RoleId"));
        user.setBranchId(resultSet.getLong("BranchId"));
        user.setActive(resultSet.getBoolean("IsActive"));
        user.setFirstLogin(resultSet.getBoolean("FirstLogin"));

        return user;
    }
}
