package com.karafarin.terminalmanagement.dao.mapper.sys;

import com.karafarin.terminalmanagement.dto.sys.SysService;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class SysServiceMapper implements RowMapper<SysService>
{
    @Override
    public SysService mapRow(ResultSet resultSet, int rowNumber) throws SQLException
    {
        SysService sysService = new SysService();

        sysService.setServiceId(resultSet.getLong("ServiceId"));
        sysService.setServiceCode(resultSet.getString("ServiceCode"));
        sysService.setName(resultSet.getNString("Name"));

        return sysService;
    }
}
