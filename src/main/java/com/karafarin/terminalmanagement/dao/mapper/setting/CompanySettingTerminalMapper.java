package com.karafarin.terminalmanagement.dao.mapper.setting;

import com.karafarin.terminalmanagement.dto.setting.CompanySettingTerminal;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CompanySettingTerminalMapper implements RowMapper<CompanySettingTerminal>
{
    @Override
    public CompanySettingTerminal mapRow(ResultSet resultSet, int rowNumber) throws SQLException
    {
        CompanySettingTerminal companySettingTerminal = new CompanySettingTerminal();

        companySettingTerminal.setCompanySettingTerminalId(resultSet.getLong("CompanySettingTerminalId"));
        companySettingTerminal.setCompanySettingId(resultSet.getLong("CompanySettingId"));
        companySettingTerminal.setTerminalDetailId(resultSet.getLong("TerminalDetailId"));
        companySettingTerminal.setConnectionCode(resultSet.getString("ConnectionCode"));
        companySettingTerminal.setActive(resultSet.getBoolean("IsActive"));

        return companySettingTerminal;
    }
}
