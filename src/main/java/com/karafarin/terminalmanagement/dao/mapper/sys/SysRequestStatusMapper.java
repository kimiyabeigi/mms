package com.karafarin.terminalmanagement.dao.mapper.sys;

import com.karafarin.terminalmanagement.dto.sys.SysRequestStatus;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class SysRequestStatusMapper implements RowMapper<SysRequestStatus>
{
    @Override
    public SysRequestStatus mapRow(ResultSet resultSet, int rowNumber) throws SQLException
    {
        SysRequestStatus sysRequestStatus = new SysRequestStatus();

        sysRequestStatus.setRequestStatusId(resultSet.getLong("RequestStatusId"));
        sysRequestStatus.setRequestStatusCode(resultSet.getString("RequestStatusCode"));
        sysRequestStatus.setDescription(resultSet.getNString("Description"));

        return sysRequestStatus;
    }
}
