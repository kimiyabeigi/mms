package com.karafarin.terminalmanagement.dao.mapper.setting;

import com.karafarin.terminalmanagement.dto.setting.CompanySettingView;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CompanySettingViewMapper implements RowMapper<CompanySettingView>
{
    @Override
    public CompanySettingView mapRow(ResultSet resultSet, int rowNumber) throws SQLException
    {
        CompanySettingView companySettingView = new CompanySettingView();

        companySettingView.setCompanySettingId(resultSet.getLong("CompanySettingId"));
        companySettingView.setCompanyCode(resultSet.getString("CompanyCode"));
        companySettingView.setName(resultSet.getString("Name"));
        companySettingView.setActive(resultSet.getBoolean("IsActive"));

        return companySettingView;
    }
}
