package com.karafarin.terminalmanagement.dao.mapper.auth;

import com.karafarin.terminalmanagement.dto.auth.RoleForm;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class RoleFormMapper implements RowMapper<RoleForm>
{
    @Override
    public RoleForm mapRow(ResultSet resultSet, int i) throws SQLException
    {
        RoleForm roleForm = new RoleForm();

        roleForm.setRoleFormId(resultSet.getLong("RoleFormId"));
        roleForm.setFormId(resultSet.getLong("FormId"));
        roleForm.setRoleId(resultSet.getLong("RoleId"));
        roleForm.setSelected(true);

        return roleForm;
    }
}
