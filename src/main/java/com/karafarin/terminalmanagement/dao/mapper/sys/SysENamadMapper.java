package com.karafarin.terminalmanagement.dao.mapper.sys;

import com.karafarin.terminalmanagement.dto.sys.SysEnamad;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class SysENamadMapper implements RowMapper<SysEnamad>
{
    @Override
    public SysEnamad mapRow(ResultSet resultSet, int rowNumber) throws SQLException
    {
        SysEnamad sysEnamad = new SysEnamad();

        sysEnamad.seteNamadId(resultSet.getLong("ENamadId"));
        sysEnamad.setEnamadCode(resultSet.getString("ENamadCode"));
        sysEnamad.setName(resultSet.getNString("Name"));

        return sysEnamad;
    }
}
