package com.karafarin.terminalmanagement.dao.mapper.psp;

import com.karafarin.terminalmanagement.dto.psp.PSPTraceBatch;
import com.karafarin.terminalmanagement.util.date.DateConverterUtil;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PSPTraceBatchMapper implements RowMapper<PSPTraceBatch>
{
    @Override
    public PSPTraceBatch mapRow(ResultSet resultSet, int rowNumber) throws SQLException
    {
        PSPTraceBatch pspTraceBatch = new PSPTraceBatch();

        pspTraceBatch.setAccountNo(resultSet.getString("AccountNo"));
        pspTraceBatch.setClientNumber(resultSet.getString("ClientNumber"));
        pspTraceBatch.setBranchName(resultSet.getString("BranchName"));
        pspTraceBatch.setRequestNumber(resultSet.getLong("RequestNumber"));
        pspTraceBatch.setPspTrackingCode(resultSet.getString("PSPTrackingCode") != null ?
                                         resultSet.getString("PSPTrackingCode") : "");
        pspTraceBatch.setRequestStatusId(resultSet.getLong("RequestStatusId"));
        pspTraceBatch.setCreateDate(DateConverterUtil
                                            .getAsStringWithSeparator(resultSet.getDate("CreateDate")));
        pspTraceBatch.setStatus(resultSet.getString("Status"));
        pspTraceBatch.setPspRequestId(resultSet.getLong("PSPRequestId"));
        pspTraceBatch.setCompanyCode(resultSet.getString("CompanyCode"));

        return pspTraceBatch;
    }
}
