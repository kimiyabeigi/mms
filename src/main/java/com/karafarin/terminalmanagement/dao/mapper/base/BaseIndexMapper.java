package com.karafarin.terminalmanagement.dao.mapper.base;

import com.karafarin.terminalmanagement.dto.base.BaseIndex;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class BaseIndexMapper implements RowMapper<BaseIndex>
{
    @Override
    public BaseIndex mapRow(ResultSet resultSet, int rowNumber) throws SQLException
    {
        BaseIndex baseIndex = new BaseIndex();

        baseIndex.setIndexId(resultSet.getLong("IndexId"));
        baseIndex.setIndexCode(resultSet.getString("IndexCode"));
        baseIndex.setName(resultSet.getNString("Name"));

        return baseIndex;

    }
}
