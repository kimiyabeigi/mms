package com.karafarin.terminalmanagement.dao.mapper.auth;

import com.karafarin.terminalmanagement.dto.auth.Role;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class RoleMapper implements RowMapper<Role>
{
    @Override
    public Role mapRow(ResultSet resultSet, int rowNumber) throws SQLException
    {
        Role role = new Role();

        role.setRoleId(resultSet.getLong("RoleId"));
        role.setName(resultSet.getString("Name"));
        role.setNameEN(resultSet.getString("NameEN"));
        role.setDescription(resultSet.getString("Description"));

        return role;
    }
}
