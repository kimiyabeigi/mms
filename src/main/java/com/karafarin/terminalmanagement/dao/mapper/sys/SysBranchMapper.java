package com.karafarin.terminalmanagement.dao.mapper.sys;

import com.karafarin.terminalmanagement.dto.sys.SysBranch;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class SysBranchMapper implements RowMapper<SysBranch>
{

    @Override
    public SysBranch mapRow(ResultSet resultSet, int rowNumber) throws SQLException
    {
        SysBranch sysService = new SysBranch();

        sysService.setBranchId(resultSet.getLong("BranchId"));
        sysService.setBranchCode(resultSet.getString("BranchCode"));
        sysService.setFromIP(resultSet.getString("FromIP"));
        sysService.setName(resultSet.getString("Name"));
        sysService.setToIP(resultSet.getString("ToIP"));
        sysService.setDepartmentCode(resultSet.getString("DepartmentCode"));
        sysService.setDefaultBranch(resultSet.getBoolean("DefaultBranch"));

        return sysService;
    }
}
