package com.karafarin.terminalmanagement.dao.mapper.base;

import com.karafarin.terminalmanagement.dto.base.BaseState;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class BaseStateMapper implements RowMapper
{
    @Override
    public Object mapRow(ResultSet resultSet, int rowNumber) throws SQLException
    {
        BaseState baseState = new BaseState();

        baseState.setStateId(resultSet.getLong("StateId"));
        baseState.setStateCode(resultSet.getString("StateCode"));
        baseState.setName(resultSet.getString("Name"));
        baseState.setNameEN((resultSet.getString("NameEN")));
        baseState.setActive(resultSet.getBoolean("IsActive"));

        return baseState;
    }
}
