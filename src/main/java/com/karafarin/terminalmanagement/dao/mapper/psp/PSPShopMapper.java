package com.karafarin.terminalmanagement.dao.mapper.psp;

import com.karafarin.terminalmanagement.dto.psp.PSPShop;
import com.karafarin.terminalmanagement.util.date.DateConverterUtil;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PSPShopMapper implements RowMapper<PSPShop>
{
    @Override
    public PSPShop mapRow(ResultSet resultSet, int rowNumber) throws SQLException
    {
        PSPShop pspShop = new PSPShop();

        pspShop.setShopId(resultSet.getLong("ShopId"));
        pspShop.setPspRequestId(resultSet.getLong("PSPRequestId"));
        pspShop.setShopName(resultSet.getString("ShopName"));
        pspShop.setShopNameEn(resultSet.getString("ShopNameEn"));
        pspShop.setOwnershipType(resultSet.getString("OwnershipType"));
        pspShop.setGuildId(resultSet.getLong("GuildId"));
        pspShop.setContractNo(resultSet.getString("ContractNo"));
        pspShop.setContractExpiryDate(DateConverterUtil
                .getAsStringWithSeparator(resultSet.getDate("ContractExpiryDate")));
        pspShop.setLicenseNo(resultSet.getString("LicenseNo"));
        pspShop.setLicenseIssuanceDate(DateConverterUtil
                .getAsStringWithSeparator(resultSet.getDate("LicenseIssuanceDate")));
        pspShop.setLicenseValidityDate(DateConverterUtil
                .getAsStringWithSeparator(resultSet.getDate("LicenseValidityDate")));
        pspShop.setCountryNameAbbr(resultSet.getString("CountryNameAbbr"));
        pspShop.setCountryName(resultSet.getString("CountryName"));
        pspShop.setStateId(resultSet.getLong("StateId"));
        pspShop.setCityId(resultSet.getLong("CityId"));
        pspShop.setMunicipalityNo(resultSet.getString("MunicipalityNo"));
        pspShop.setPostalCode(resultSet.getString("PostalCode"));
        pspShop.setAddress(resultSet.getString("Address"));
        pspShop.setPhone(resultSet.getString("Phone"));
        pspShop.setFax(resultSet.getString("Fax"));

        return pspShop;
    }
}
