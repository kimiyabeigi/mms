package com.karafarin.terminalmanagement.dao.mapper.base;

import com.karafarin.terminalmanagement.dto.base.BaseCompany;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class BaseCompanyMapper implements RowMapper<BaseCompany>
{
    @Override
    public BaseCompany mapRow(ResultSet resultSet, int rowNumber) throws SQLException
    {
        BaseCompany baseCompany = new BaseCompany();

        baseCompany.setCompanyId(resultSet.getLong("CompanyId"));
        baseCompany.setCompanyCode(resultSet.getString("CompanyCode"));
        baseCompany.setName(resultSet.getNString("Name"));

        return baseCompany;

    }
}
