package com.karafarin.terminalmanagement.dao.mapper.sys;

import com.karafarin.terminalmanagement.dto.sys.SysTerminal;
import org.springframework.jdbc.core.RowMapper;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SysTerminalMapper implements RowMapper<SysTerminal> {


    @Override
    public SysTerminal mapRow(ResultSet resultSet, int rowNumber) throws SQLException {

        SysTerminal sysTerminal = new SysTerminal();

        sysTerminal.setTerminalId(resultSet.getLong("TerminalId"));
        sysTerminal.setCode(resultSet.getNString("Code"));

        return sysTerminal;

    }
}
