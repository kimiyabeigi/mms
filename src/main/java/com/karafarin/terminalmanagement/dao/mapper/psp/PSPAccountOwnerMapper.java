package com.karafarin.terminalmanagement.dao.mapper.psp;

import com.karafarin.terminalmanagement.dto.psp.PSPAccountOwner;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PSPAccountOwnerMapper implements RowMapper<PSPAccountOwner>
{
    @Override
    public PSPAccountOwner mapRow(ResultSet resultSet, int rowNumber) throws SQLException
    {
        PSPAccountOwner pspAccountOwner = new PSPAccountOwner();

        pspAccountOwner.setAccountOwnerId(resultSet.getLong("AccountOwnerId"));
        pspAccountOwner.setPspRequestId(resultSet.getLong("PSPRequestId"));
        pspAccountOwner.setClientNumber(resultSet.getString("ClientNumber"));
        pspAccountOwner.setClientName(resultSet.getString("ClientName"));
        pspAccountOwner.setActive(resultSet.getBoolean("IsActive"));
        pspAccountOwner.setClientType(resultSet.getString("ClientType"));

        return pspAccountOwner;

    }
}
