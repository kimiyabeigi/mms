package com.karafarin.terminalmanagement.dao.mapper.psp;

import com.karafarin.terminalmanagement.dto.psp.PSPReportRegisterDetail;
import com.karafarin.terminalmanagement.util.date.DateConverterUtil;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PSPReportRegisterDetailMapper implements RowMapper<PSPReportRegisterDetail>
{
    @Override
    public PSPReportRegisterDetail mapRow(ResultSet resultSet, int i) throws SQLException
    {
        PSPReportRegisterDetail pspReportRegisterDetail = new PSPReportRegisterDetail();

        String createDateTime = DateConverterUtil
                .getAsStringWithTime(resultSet.getTimestamp("CreateDate"), "fa");
        String[] split = createDateTime.split(" ");
        pspReportRegisterDetail.setCreateDate(split[0]);
        pspReportRegisterDetail.setCreateTime(split[1]);
        pspReportRegisterDetail.setBranch(resultSet.getString("Branch"));
        pspReportRegisterDetail.setUserCode(resultSet.getString("UserCode"));
        pspReportRegisterDetail.setService(resultSet.getString("Service"));
        pspReportRegisterDetail.setDescription(resultSet.getString("Description"));
        pspReportRegisterDetail.setPspTrackingCode(resultSet.getString("PSPTrackingCode"));
        pspReportRegisterDetail.setRequestStatus(resultSet.getString("RequestStatus"));
        pspReportRegisterDetail.setRequestNumber(resultSet.getString("RequestNumber"));
        pspReportRegisterDetail.setStatusType(resultSet.getString("StatusType"));
        pspReportRegisterDetail.setPspRequestStatusId(resultSet.getLong("PSPRequestStatusId"));
        pspReportRegisterDetail.setPspRequestId(resultSet.getLong("PSPRequestId"));

        return pspReportRegisterDetail;

    }
}
