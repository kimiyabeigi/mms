package com.karafarin.terminalmanagement.dao.mapper.setting;

import com.karafarin.terminalmanagement.dto.setting.CompanySettingStatus;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CompanySettingStatusMapper implements RowMapper<CompanySettingStatus>
{
    @Override
    public CompanySettingStatus mapRow(ResultSet resultSet, int rowNumber) throws SQLException
    {
        CompanySettingStatus companySettingStatus = new CompanySettingStatus();

        companySettingStatus.setCompanySettingStatusId(resultSet.getLong("CompanySettingStatusId"));
        companySettingStatus.setCompanySettingId(resultSet.getLong("CompanySettingId"));
        companySettingStatus.setServiceId(resultSet.getLong("ServiceId"));
        companySettingStatus.setIndexId(resultSet.getLong("IndexId"));
        companySettingStatus.setStatusCode(resultSet.getString("StatusCode"));
        companySettingStatus.setStatusTypeId(resultSet.getLong("StatusTypeId"));

        return companySettingStatus;

    }
}
