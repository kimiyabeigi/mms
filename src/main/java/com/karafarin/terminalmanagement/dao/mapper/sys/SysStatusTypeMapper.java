package com.karafarin.terminalmanagement.dao.mapper.sys;

import com.karafarin.terminalmanagement.dto.sys.SysStatusType;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class SysStatusTypeMapper implements RowMapper<SysStatusType>
{
    @Override
    public SysStatusType mapRow(ResultSet resultSet, int rowNumber) throws SQLException
    {
        SysStatusType sysStatusType = new SysStatusType();

        sysStatusType.setStatusTypeId(resultSet.getLong("StatusTypeId"));
        sysStatusType.setCode(resultSet.getNString("Code"));
        sysStatusType.setDescription(resultSet.getNString("Description"));

        return sysStatusType;
    }
}
