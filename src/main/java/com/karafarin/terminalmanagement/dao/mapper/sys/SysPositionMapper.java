package com.karafarin.terminalmanagement.dao.mapper.sys;

import com.karafarin.terminalmanagement.dto.sys.SysPosition;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class SysPositionMapper implements RowMapper<SysPosition>
{
    @Override
    public SysPosition mapRow(ResultSet resultSet, int rowNumber) throws SQLException
    {
        SysPosition sysPosition = new SysPosition();

        sysPosition.setPositionId(resultSet.getLong("PositionId"));
        sysPosition.setName(resultSet.getString("Name"));

        return sysPosition;
    }
}
