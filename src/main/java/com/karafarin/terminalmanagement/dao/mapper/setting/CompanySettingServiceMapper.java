package com.karafarin.terminalmanagement.dao.mapper.setting;

import com.karafarin.terminalmanagement.dto.setting.CompanySettingService;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CompanySettingServiceMapper implements RowMapper<CompanySettingService>
{
    @Override
    public CompanySettingService mapRow(ResultSet resultSet, int rowNumber) throws SQLException
    {
        CompanySettingService companySettingService = new CompanySettingService();

        companySettingService.setCompanySettingServiceId(resultSet.getLong("CompanySettingServiceId"));
        companySettingService.setCompanySettingId(resultSet.getLong("CompanySettingId"));
        companySettingService.setServiceId(resultSet.getLong("ServiceId"));
        companySettingService.setActive(resultSet.getBoolean("IsActive"));

        return companySettingService;
    }
}
