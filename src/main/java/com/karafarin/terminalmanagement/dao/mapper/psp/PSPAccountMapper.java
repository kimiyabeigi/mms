package com.karafarin.terminalmanagement.dao.mapper.psp;

import com.karafarin.terminalmanagement.dto.psp.PSPAccount;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PSPAccountMapper implements RowMapper<PSPAccount>
{

    @Override
    public PSPAccount mapRow(ResultSet resultSet, int rowNumber) throws SQLException
    {
        PSPAccount pspAccount = new PSPAccount();

        pspAccount.setAccountId(resultSet.getLong("AccountId"));
        pspAccount.setPspRequestId(resultSet.getLong("PSPRequestId"));
        pspAccount.setAccountNo(resultSet.getString("AccountNo"));
        pspAccount.setAccountType(resultSet.getString("AccountType"));
        pspAccount.setAccountDesc(resultSet.getString("AccountDesc"));
        pspAccount.setBranchId(resultSet.getLong("BranchId"));
        pspAccount.setIbanNo(resultSet.getNString("IbanNo"));

        return pspAccount;
    }
}
