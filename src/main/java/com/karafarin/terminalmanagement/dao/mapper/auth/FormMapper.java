package com.karafarin.terminalmanagement.dao.mapper.auth;

import com.karafarin.terminalmanagement.dto.auth.Form;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class FormMapper implements RowMapper<Form>
{
    @Override
    public Form mapRow(ResultSet resultSet, int rowNumber) throws SQLException
    {
        Form form = new Form();

        form.setFormId(resultSet.getLong("FormId"));
        form.setName(resultSet.getString("Name"));
        form.setURL(resultSet.getString("URL"));
        form.setMethod(resultSet.getString("Method"));

        return form;
    }
}
