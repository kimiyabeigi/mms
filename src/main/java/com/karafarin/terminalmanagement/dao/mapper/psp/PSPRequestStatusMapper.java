package com.karafarin.terminalmanagement.dao.mapper.psp;

import com.karafarin.terminalmanagement.dto.psp.PSPRequestStatus;
import com.karafarin.terminalmanagement.util.date.DateConverterUtil;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PSPRequestStatusMapper implements RowMapper<PSPRequestStatus>
{
    @Override
    public PSPRequestStatus mapRow(ResultSet resultSet, int rowNumber) throws SQLException
    {
        PSPRequestStatus pspRequestStatus = new PSPRequestStatus();

        pspRequestStatus.setPspRequestStatusId(resultSet.getLong("PSPRequestStatusId"));
        pspRequestStatus.setPspRequestId(resultSet.getLong("PSPRequestId"));
        pspRequestStatus.setRequestStatusId(resultSet.getLong("RequestStatusId"));
        pspRequestStatus.setStatusTypeId(resultSet.getLong("StatusTypeId"));
        pspRequestStatus.setServiceId(resultSet.getLong("ServiceId"));
        pspRequestStatus.setPspErrorCode(resultSet.getString("PSPErrorCode"));
        pspRequestStatus.setPspErrorDesc(resultSet.getString("PSPErrorDesc"));
        pspRequestStatus.setTrackingCode(resultSet.getString("TrackingCode"));
        pspRequestStatus.setCreateDate(
                DateConverterUtil.getAsStringWithTime(resultSet.getTimestamp("CreateDate"), "fa"));
        pspRequestStatus.setStatus(resultSet.getString("Status"));

        return pspRequestStatus;
    }
}
