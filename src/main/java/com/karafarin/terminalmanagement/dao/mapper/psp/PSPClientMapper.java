package com.karafarin.terminalmanagement.dao.mapper.psp;

import com.karafarin.terminalmanagement.dto.psp.PSPClient;
import com.karafarin.terminalmanagement.util.date.DateConverterUtil;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PSPClientMapper implements RowMapper<PSPClient>
{
    @Override
    public PSPClient mapRow(ResultSet resultSet, int rowNumber) throws SQLException
    {
        PSPClient pspClient = new PSPClient();

        pspClient.setClientId(resultSet.getLong("ClientId"));
        pspClient.setPspRequestId(resultSet.getLong("PSPRequestId"));
        pspClient.setPositionId(resultSet.getLong("PositionId"));
        pspClient.setClientNumber(resultSet.getString("ClientNumber"));
        pspClient.setSex(resultSet.getString("Sex"));
        pspClient.setLifeStatus(resultSet.getString("LifeStatus"));
        pspClient.setFirstName(resultSet.getString("FirstName"));
        pspClient.setFirstNameEn(resultSet.getString("FirstNameEn"));
        pspClient.setLastName(resultSet.getString("LastName"));
        pspClient.setLastNameEn(resultSet.getString("LastNameEn"));
        pspClient.setFatherName(resultSet.getString("FatherName"));
        pspClient.setFatherNameEn(resultSet.getString("FatherNameEn"));
        pspClient.setNationalCode(resultSet.getString("NationalCode"));
        pspClient.setRegNumber(resultSet.getString("RegNumber"));
        pspClient.setBirthDate(DateConverterUtil
                .getAsStringWithSeparator(resultSet.getDate("BirthDay")));
        pspClient.setCountryNameAbbr(resultSet.getString("CountryNameAbbr"));
        pspClient.setCountryCode(resultSet.getString("CountryCode"));
        pspClient.setCountryDesc(resultSet.getString("CountryDesc"));
        pspClient.setIssuanceDate(DateConverterUtil
                .getAsStringWithSeparator(resultSet.getDate("IssuanceDate")));
        pspClient.setIssuancePlaceCode(resultSet.getString("IssuancePlaceCode"));
        pspClient.setIssuancePlaceDesc(resultSet.getString("IssuancePlaceDesc"));
        pspClient.setMobilePhone(resultSet.getString("MobilePhone"));
        pspClient.setPhone(resultSet.getString("Phone"));
        pspClient.setFax(resultSet.getString("Fax"));
        pspClient.setPostalCode(resultSet.getString("PostalCode"));
        pspClient.setAddress(resultSet.getString("Address"));
        pspClient.setClientType(resultSet.getString("ClientType"));
        pspClient.setClientKind(resultSet.getString("ClientKind"));
        pspClient.setCompanyName(resultSet.getString("CompanyName"));
        pspClient.setCompanyNameEn(resultSet.getString("CompanyNameEn"));
        pspClient.setCommerceCode(resultSet.getString("CommerceCode"));
        pspClient.setForeignCode(resultSet.getString("ForeignCode"));
        pspClient.setPassportNumber(resultSet.getString("PassportNumber"));
        pspClient.setPassportExpiryDate(DateConverterUtil
                .getAsStringWithSeparator(resultSet.getDate("PassportExpiryDate")));

        return pspClient;
    }
}
