package com.karafarin.terminalmanagement.dao.intf.sys;

import com.karafarin.terminalmanagement.dao.intf.GenericDao;
import com.karafarin.terminalmanagement.dto.sys.SysStatusType;

public interface SysStatusTypeDao extends GenericDao<SysStatusType, Long>
{
}
