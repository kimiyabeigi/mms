package com.karafarin.terminalmanagement.dao.intf.base;

import com.karafarin.terminalmanagement.dao.intf.GenericDao;
import com.karafarin.terminalmanagement.dto.base.BaseTerminalDetail;
import org.springframework.dao.DataAccessException;

import java.sql.SQLException;
import java.util.List;

public interface BaseTerminalDetailDao extends GenericDao<BaseTerminalDetail, Long>
{
    List<BaseTerminalDetail> findSelectedTerminalDetails(Long id) throws SQLException, DataAccessException;
    List<BaseTerminalDetail> findByCompanyId(Long id) throws SQLException, DataAccessException;
}
