package com.karafarin.terminalmanagement.dao.intf.psp;

import com.karafarin.terminalmanagement.dao.intf.GenericDao;
import com.karafarin.terminalmanagement.dto.psp.PSPRequest;
import org.springframework.dao.DataAccessException;

import java.sql.SQLException;

public interface PSPRequestDao extends GenericDao<PSPRequest, Long>
{
    boolean updateTrackingCode(Long id, String trackingCode) throws SQLException, DataAccessException;
    boolean updateTerminalById(Long id, String pspAcceptanceCode, String pspTerminalCode) throws SQLException, DataAccessException;
    boolean updateRequestStatusId(Long id, Long requestStatusId) throws SQLException, DataAccessException;
    PSPRequest findByRequestNumber(Long requestNumber) throws SQLException, DataAccessException;
}
