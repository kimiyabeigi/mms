package com.karafarin.terminalmanagement.dao.intf.psp;

import com.karafarin.terminalmanagement.dao.intf.GenericDao;
import com.karafarin.terminalmanagement.dto.psp.PSPTraceBatch;
import org.springframework.dao.DataAccessException;

import java.sql.SQLException;

public interface PSPTraceBatchDao extends GenericDao<PSPTraceBatch, Long>
{
    PSPTraceBatch findByRequestNumber(Long requestNumber) throws SQLException, DataAccessException;
}
