package com.karafarin.terminalmanagement.dao.intf.psp;

import com.karafarin.terminalmanagement.dao.intf.GenericDao;
import com.karafarin.terminalmanagement.dto.psp.PSPTerminalBatch;
import org.springframework.dao.DataAccessException;

import java.sql.SQLException;

public interface PSPTerminalBatchDao extends GenericDao<PSPTerminalBatch, Long>
{
    PSPTerminalBatch findByPSPTrackingCode(String pspTrackingCode) throws SQLException, DataAccessException;
    PSPTerminalBatch findRegisterByPSPTrackingCode(String pspTrackingCode) throws SQLException, DataAccessException;
}
