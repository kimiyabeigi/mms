package com.karafarin.terminalmanagement.dao.intf.sys;

import com.karafarin.terminalmanagement.dao.intf.GenericDao;
import com.karafarin.terminalmanagement.dto.sys.SysService;

public interface SysServiceDao extends GenericDao<SysService, Long>
{
}
