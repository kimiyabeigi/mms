package com.karafarin.terminalmanagement.dao.intf.auth;

import com.karafarin.terminalmanagement.dao.intf.GenericDao;
import com.karafarin.terminalmanagement.dto.auth.Form;

public interface FormDao extends GenericDao<Form, Long>
{
}
