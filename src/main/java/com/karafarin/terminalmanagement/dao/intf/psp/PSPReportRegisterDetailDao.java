package com.karafarin.terminalmanagement.dao.intf.psp;

import com.karafarin.terminalmanagement.dto.psp.PSPReportRegisterDetail;
import org.springframework.dao.DataAccessException;

import java.sql.SQLException;
import java.util.List;

public interface PSPReportRegisterDetailDao
{
    List<PSPReportRegisterDetail> findAll(Long pspRequestId) throws SQLException, DataAccessException;
}
