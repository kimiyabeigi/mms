package com.karafarin.terminalmanagement.dao.intf.setting;

import com.karafarin.terminalmanagement.dao.intf.GenericDao;
import com.karafarin.terminalmanagement.dto.setting.CompanySettingService;
import org.springframework.dao.DataAccessException;

import java.sql.SQLException;
import java.util.List;

public interface CompanySettingServiceDao extends GenericDao<CompanySettingService, Long>
{
    List<CompanySettingService> findByCompanySettingId(Long id) throws SQLException, DataAccessException;;
}
