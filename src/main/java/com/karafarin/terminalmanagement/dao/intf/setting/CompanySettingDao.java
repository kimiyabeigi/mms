package com.karafarin.terminalmanagement.dao.intf.setting;

import com.karafarin.terminalmanagement.dao.intf.GenericDao;
import com.karafarin.terminalmanagement.dto.setting.CompanySetting;
import com.karafarin.terminalmanagement.dto.setting.CompanySettingView;
import org.springframework.dao.DataAccessException;

import java.sql.SQLException;
import java.util.List;

public interface CompanySettingDao extends GenericDao<CompanySetting, Long>
{
    CompanySettingView findViewById(Long id) throws SQLException, DataAccessException;
    List<CompanySettingView> findViewAll() throws SQLException, DataAccessException;
    CompanySetting findByCompanyId(Long id) throws  SQLException, DataAccessException;
}
