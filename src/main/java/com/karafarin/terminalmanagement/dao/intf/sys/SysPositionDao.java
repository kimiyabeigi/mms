package com.karafarin.terminalmanagement.dao.intf.sys;

import com.karafarin.terminalmanagement.dao.intf.GenericDao;
import com.karafarin.terminalmanagement.dto.sys.SysPosition;

public interface SysPositionDao extends GenericDao<SysPosition, Long>
{
}
