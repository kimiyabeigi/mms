package com.karafarin.terminalmanagement.dao.intf.base;

import com.karafarin.terminalmanagement.dao.intf.GenericDao;
import com.karafarin.terminalmanagement.dto.base.BaseCity;
import org.springframework.dao.DataAccessException;

import java.sql.SQLException;
import java.util.List;

public interface BaseCityDao extends GenericDao<BaseCity, Long>
{
    List<BaseCity> findAll(boolean all) throws SQLException, DataAccessException;
    List<BaseCity> findByStateId(Long id) throws SQLException, DataAccessException;
}
