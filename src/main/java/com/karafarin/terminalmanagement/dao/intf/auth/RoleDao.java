package com.karafarin.terminalmanagement.dao.intf.auth;

import com.karafarin.terminalmanagement.dao.intf.GenericDao;
import com.karafarin.terminalmanagement.dto.auth.Role;
import org.springframework.dao.DataAccessException;

import java.sql.SQLException;
import java.util.List;

public interface RoleDao extends GenericDao<Role, Long>
{
    List<Role> findAllRoles() throws SQLException, DataAccessException;
}
