package com.karafarin.terminalmanagement.dao.intf.setting;

import com.karafarin.terminalmanagement.dao.intf.GenericDao;
import com.karafarin.terminalmanagement.dto.setting.CompanySettingStatus;
import org.springframework.dao.DataAccessException;

import java.sql.SQLException;
import java.util.List;

public interface CompanySettingStatusDao extends GenericDao<CompanySettingStatus, Long>
{
    List<CompanySettingStatus> findByCompanySettingId(Long id) throws SQLException, DataAccessException;
    Long findStatusId(String statusCode, Long serviceId, String companyCode) throws SQLException, DataAccessException;
}
