package com.karafarin.terminalmanagement.dao.intf.psp;

import com.karafarin.terminalmanagement.dao.intf.GenericDao;
import com.karafarin.terminalmanagement.dto.psp.PSPClient;

public interface PSPClientDao extends GenericDao<PSPClient, Long>
{
}
