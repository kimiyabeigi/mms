package com.karafarin.terminalmanagement.dao.intf.auth;

import com.karafarin.terminalmanagement.dao.intf.GenericDao;
import com.karafarin.terminalmanagement.dto.auth.RoleForm;
import org.springframework.dao.DataAccessException;

import java.sql.SQLException;
import java.util.List;

public interface RoleFormDao extends GenericDao<RoleForm, Long>
{
    List<RoleForm> findByRoleId(Long id) throws SQLException, DataAccessException;
}
