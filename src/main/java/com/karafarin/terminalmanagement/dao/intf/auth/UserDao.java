package com.karafarin.terminalmanagement.dao.intf.auth;

import com.karafarin.terminalmanagement.dao.intf.GenericDao;
import com.karafarin.terminalmanagement.dto.auth.User;
import com.karafarin.terminalmanagement.model.ChangePassword;
import org.springframework.dao.DataAccessException;

import java.sql.SQLException;
import java.util.List;

public interface UserDao extends GenericDao<User, Long>
{
    User findUserByUserName(String userName) throws SQLException, DataAccessException;
    boolean changePassword(ChangePassword changePassword, boolean isAdmin) throws SQLException, DataAccessException;
    List<User> findAllUsers() throws SQLException, DataAccessException;
}
