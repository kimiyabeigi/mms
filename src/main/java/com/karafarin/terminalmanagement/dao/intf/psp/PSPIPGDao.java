package com.karafarin.terminalmanagement.dao.intf.psp;

import com.karafarin.terminalmanagement.dao.intf.GenericDao;
import com.karafarin.terminalmanagement.dto.psp.PSPIPG;

public interface PSPIPGDao extends GenericDao<PSPIPG, Long>
{
}
