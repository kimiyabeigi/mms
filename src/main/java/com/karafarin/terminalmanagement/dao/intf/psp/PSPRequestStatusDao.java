package com.karafarin.terminalmanagement.dao.intf.psp;

import com.karafarin.terminalmanagement.dao.intf.GenericDao;
import com.karafarin.terminalmanagement.dto.psp.PSPRequestStatus;
import org.springframework.dao.DataAccessException;

import java.sql.SQLException;
import java.util.List;

public interface PSPRequestStatusDao extends GenericDao<PSPRequestStatus, Long>
{
    List<PSPRequestStatus> findByPSPRequestId(Long id) throws SQLException, DataAccessException;
    boolean updateRequestStatusIdById(Long id, Long requestStatusId) throws SQLException, DataAccessException;
    boolean updateStatusById(Long id, String status) throws SQLException, DataAccessException;
}
