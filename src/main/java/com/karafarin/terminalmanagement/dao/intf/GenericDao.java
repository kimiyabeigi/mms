package com.karafarin.terminalmanagement.dao.intf;

import org.springframework.dao.DataAccessException;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.List;

public interface GenericDao<T, PK extends Serializable>
{
    PK add(T entity) throws SQLException, DataAccessException;
    T findById(PK id) throws SQLException, DataAccessException;
    List<T> findAll() throws SQLException, DataAccessException;
    boolean delete(PK id) throws SQLException, DataAccessException;
    boolean update(T entity) throws SQLException, DataAccessException;
    List<T> search(T entity) throws SQLException, DataAccessException;
}
