package com.karafarin.terminalmanagement.dao.intf.sys;

import com.karafarin.terminalmanagement.dao.intf.GenericDao;
import com.karafarin.terminalmanagement.dto.sys.SysEnamad;

public interface SysENamadDao extends GenericDao<SysEnamad, Long>
{
}
