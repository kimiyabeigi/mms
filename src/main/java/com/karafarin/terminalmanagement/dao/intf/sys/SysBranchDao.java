package com.karafarin.terminalmanagement.dao.intf.sys;

import com.karafarin.terminalmanagement.dao.intf.GenericDao;
import com.karafarin.terminalmanagement.dto.sys.SysBranch;
import org.springframework.dao.DataAccessException;

import java.sql.SQLException;

public interface SysBranchDao extends GenericDao<SysBranch, Long>
{
    SysBranch findByBranchCode(String branchCode) throws SQLException, DataAccessException;
    SysBranch findSupervisorBranch() throws SQLException, DataAccessException;
}
