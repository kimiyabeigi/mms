package com.karafarin.terminalmanagement.dao.intf.psp;

import com.karafarin.terminalmanagement.dao.intf.GenericDao;
import com.karafarin.terminalmanagement.dto.psp.PSPAccountOwner;

public interface PSPAccountOwnerDao extends GenericDao<PSPAccountOwner, Long>
{
}
