package com.karafarin.terminalmanagement.dao.intf.sys;


import com.karafarin.terminalmanagement.dao.intf.GenericDao;
import com.karafarin.terminalmanagement.dto.sys.SysTerminal;
import org.springframework.dao.DataAccessException;

import java.sql.SQLException;
import java.util.List;

public interface SysTerminalDao extends GenericDao<SysTerminal, Long>
{
    List<SysTerminal> findTerminalsRequest(Long companyId) throws SQLException, DataAccessException;
    List<SysTerminal> findTerminalsByFilter(Long hideId) throws SQLException, DataAccessException;
}
