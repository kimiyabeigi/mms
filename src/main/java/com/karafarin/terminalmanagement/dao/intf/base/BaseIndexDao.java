package com.karafarin.terminalmanagement.dao.intf.base;

import com.karafarin.terminalmanagement.dao.intf.GenericDao;
import com.karafarin.terminalmanagement.dto.base.BaseIndex;

public interface BaseIndexDao extends GenericDao<BaseIndex, Long>
{
}
