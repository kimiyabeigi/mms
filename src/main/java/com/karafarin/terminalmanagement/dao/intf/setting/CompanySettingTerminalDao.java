package com.karafarin.terminalmanagement.dao.intf.setting;

import com.karafarin.terminalmanagement.dao.intf.GenericDao;
import com.karafarin.terminalmanagement.dto.setting.CompanySettingTerminal;
import org.springframework.dao.DataAccessException;

import java.sql.SQLException;
import java.util.List;

public interface CompanySettingTerminalDao extends GenericDao<CompanySettingTerminal, Long>
{
    List<CompanySettingTerminal> findByCompanySettingId(Long id) throws SQLException, DataAccessException;

    String findConnectionCode(Long companyId, Long terminalId, Long terminalDetailId) throws SQLException,
                                                                                             DataAccessException;
}
