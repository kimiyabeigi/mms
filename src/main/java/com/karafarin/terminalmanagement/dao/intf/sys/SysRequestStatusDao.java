package com.karafarin.terminalmanagement.dao.intf.sys;

import com.karafarin.terminalmanagement.dao.intf.GenericDao;
import com.karafarin.terminalmanagement.dto.sys.SysRequestStatus;

public interface SysRequestStatusDao extends GenericDao<SysRequestStatus, Long>
{
}
