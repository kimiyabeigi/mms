package com.karafarin.terminalmanagement.dao.intf.setting;

import com.karafarin.terminalmanagement.dao.intf.GenericDao;
import com.karafarin.terminalmanagement.dto.setting.CompanySettingDetail;

public interface CompanySettingDetailDao extends GenericDao<CompanySettingDetail, Long>
{
}
