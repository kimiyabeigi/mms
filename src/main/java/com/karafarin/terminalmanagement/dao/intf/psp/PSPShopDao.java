package com.karafarin.terminalmanagement.dao.intf.psp;

import com.karafarin.terminalmanagement.dao.intf.GenericDao;
import com.karafarin.terminalmanagement.dto.psp.PSPShop;

public interface PSPShopDao extends GenericDao<PSPShop, Long>
{
}
