package com.karafarin.terminalmanagement.dao.intf.base;

import com.karafarin.terminalmanagement.dao.intf.GenericDao;
import com.karafarin.terminalmanagement.dto.base.BaseCompany;
import org.springframework.dao.DataAccessException;

import java.sql.SQLException;
import java.util.List;

public interface BaseCompanyDao extends GenericDao<BaseCompany, Long>
{
    List<BaseCompany> findActiveCompaniesForRequest() throws SQLException, DataAccessException;
}
