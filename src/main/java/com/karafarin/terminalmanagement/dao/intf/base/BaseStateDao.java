package com.karafarin.terminalmanagement.dao.intf.base;

import com.karafarin.terminalmanagement.dao.intf.GenericDao;
import com.karafarin.terminalmanagement.dto.base.BaseState;
import org.springframework.dao.DataAccessException;

import java.sql.SQLException;
import java.util.List;

public interface BaseStateDao extends GenericDao<BaseState, Long>
{
    List<BaseState> findAll(boolean all) throws SQLException, DataAccessException;;
}
