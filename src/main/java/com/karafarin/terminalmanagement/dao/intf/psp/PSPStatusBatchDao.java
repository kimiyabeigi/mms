package com.karafarin.terminalmanagement.dao.intf.psp;

import com.karafarin.terminalmanagement.dao.intf.GenericDao;
import com.karafarin.terminalmanagement.dto.psp.PSPStatusBatch;
import org.springframework.dao.DataAccessException;

import java.sql.SQLException;

public interface PSPStatusBatchDao extends GenericDao<PSPStatusBatch, Long>
{
    PSPStatusBatch findByPSPTrackingCode(String pspTrackingCode) throws SQLException, DataAccessException;
}
