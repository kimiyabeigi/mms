package com.karafarin.terminalmanagement.dao.intf.psp;

import com.karafarin.terminalmanagement.dto.psp.PSPReportRegisterMaster;
import com.karafarin.terminalmanagement.model.psp.ParamsReportMaster;
import org.springframework.dao.DataAccessException;

import java.sql.SQLException;
import java.util.List;

public interface PSPReportRegisterMasterDao
{
    List<PSPReportRegisterMaster> findAll(ParamsReportMaster paramsReportMaster) throws SQLException,
                                                                                        DataAccessException;
}
