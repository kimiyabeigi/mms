package com.karafarin.terminalmanagement.dao.intf.base;

import com.karafarin.terminalmanagement.dao.intf.GenericDao;
import com.karafarin.terminalmanagement.dto.base.BaseGuild;
import org.springframework.dao.DataAccessException;

import java.sql.SQLException;
import java.util.List;

public interface BaseGuildDao extends GenericDao<BaseGuild, Long>
{
    List<BaseGuild> findAll(boolean all) throws SQLException, DataAccessException;;
}
