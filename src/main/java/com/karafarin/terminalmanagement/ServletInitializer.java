package com.karafarin.terminalmanagement;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

public class ServletInitializer extends SpringBootServletInitializer {
	private static final Logger logger = LogManager.getLogger(ServletInitializer.class);

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {

		logger.info("###################################");
		logger.info("####### System initialized! #######");
		logger.info("### Starting ServletInitializer ###");
		logger.info("###################################");

		return application.sources(TerminalManagementApplication.class);
	}

}
