package com.karafarin.terminalmanagement.service.security;

import com.karafarin.terminalmanagement.config.jdbc.SqlConnectionConfiguration;
import com.karafarin.terminalmanagement.util.UserUtil;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Map;

@Component("permissionService")
public class PermissionService
{
    private static final Logger logger = LogManager.getLogger(PermissionService.class);

    @Autowired
    private SqlConnectionConfiguration sqlConnectionConfiguration;

    private JdbcTemplate jdbcTemplate = null;
    private SimpleJdbcCall simpleJdbcCall;

    @PostConstruct
    public void postConstruct()
    {
        if (jdbcTemplate == null) jdbcTemplate = sqlConnectionConfiguration.getJdbcTemplate();
    }

    public boolean hasPermission(String api, String method)
    {
        String sql = " SELECT URL, Method, FormName, RoleName, RoleNameEN, RoleId " +
                     " FROM dbo.vw_RolePermission " +
                     " WHERE RoleId = ? AND " +
                     "       URL = ? AND " +
                     "       Method = ?";

        List<Map<String, Object>> maps =
                jdbcTemplate.queryForList(sql, new Object[]{UserUtil.getCurrentUser().getRoleId(), api, method});

        return maps.size() > 0;
    }

}
