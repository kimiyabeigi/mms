package com.karafarin.terminalmanagement.service.security;

import com.karafarin.terminalmanagement.dao.impl.auth.RoleDaoImpl;
import com.karafarin.terminalmanagement.dao.impl.sys.SysBranchDaoImpl;
import com.karafarin.terminalmanagement.dao.impl.auth.UserDaoImpl;
import com.karafarin.terminalmanagement.dto.auth.Role;
import com.karafarin.terminalmanagement.dto.sys.SysBranch;
import com.karafarin.terminalmanagement.dto.auth.User;
import com.karafarin.terminalmanagement.util.WebUtil;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.net.InetAddress;
import java.util.Arrays;

@Component
public class AuthenticationService implements UserDetailsService
{
    private static final Logger logger = LogManager.getLogger(AuthenticationService.class);

    @Autowired
    private UserDaoImpl userDao;
    @Autowired
    private WebUtil webUtil;
    @Autowired
    private SysBranchDaoImpl sysBranchDao;
    @Autowired
    private RoleDaoImpl roleDao;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException
    {
        try
        {
            User user = userDao.findUserByUserName(username);
            if (user != null)
            {
                Role role = roleDao.findById(user.getRoleId());
                SysBranch branch = sysBranchDao.findById(user.getBranchId());
                boolean includeIpRange =
                        isIncludeIpRange(branch.getFromIP(), branch.getToIP(), webUtil.getWorkStation().getIp());
                boolean accountNotExpired = true;
                boolean credentialsNonExpired = true;
                boolean accountNonLocked = true;

                if (includeIpRange)
                {
                    logger.info("Login successful via username [" + username + "] and ip [" +
                                webUtil.getWorkStation().getIp() + "]");
                } else
                {
                    logger.info("Login failed. This ip [" + webUtil.getWorkStation().getIp() + "] for username [" +
                                username + "] is not valid for branch's ip range");
                }

                GrantedAuthority authority = new SimpleGrantedAuthority(role.getNameEN());
                UserDetails userDetails =
                        (UserDetails) new org.springframework.security.core.userdetails.User(user.getUserName(),
                                                                                             user.getPassword(),
                                                                                             includeIpRange,
                                                                                             accountNotExpired,
                                                                                             credentialsNonExpired,
                                                                                             accountNonLocked,
                                                                                             Arrays.asList(authority));

                return userDetails;
            } else
            {
                logger.info("Login failed. Unable to find user with username [" + username + "]");
                throw new UsernameNotFoundException("Unable to find user with username provided!");
            }
        } catch (Exception e)
        {
            logger.error("There is an error at Login with username [" + username + "]", e);
            e.printStackTrace();
        }

        return null;
    }

    private boolean isIncludeIpRange(String firstIp, String lastIp, String remoteIp)
    {
        try
        {
            Long ipLo = ipTolong(InetAddress.getByName(firstIp));
            Long ipHi = ipTolong(InetAddress.getByName(lastIp));
            Long ipToTest = ipTolong(InetAddress.getByName(remoteIp));
            if (ipToTest >= ipLo && ipToTest <= ipHi) return true;
            else return false;
        } catch (Exception e)
        {
            return false;
        }
    }

    private long ipTolong(InetAddress ip)
    {
        byte[] octets = ip.getAddress();
        long result = 0;
        for (byte octet : octets)
        {
            result <<= 8;
            result |= octet & 0xff;
        }
        return result;
    }

}
