package com.karafarin.terminalmanagement.service.intf.base;

import com.karafarin.terminalmanagement.dto.base.BaseCompany;
import com.karafarin.terminalmanagement.message.Response;
import com.karafarin.terminalmanagement.service.intf.ServiceGeneric;

public interface ServiceBaseCompany extends ServiceGeneric<BaseCompany, Long>
{
    Response search(String companyCode, String name);
    Response findActiveCompaniesForRequest();
}
