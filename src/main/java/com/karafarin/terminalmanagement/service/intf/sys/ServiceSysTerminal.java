package com.karafarin.terminalmanagement.service.intf.sys;

import com.karafarin.terminalmanagement.dto.sys.SysTerminal;
import com.karafarin.terminalmanagement.message.Response;
import com.karafarin.terminalmanagement.service.intf.ServiceGeneric;

public interface ServiceSysTerminal extends ServiceGeneric<SysTerminal, Long>
{
    Response findTerminalsRequest(Long companyId);
    Response findTerminalsByFilter(Long hideId);
}
