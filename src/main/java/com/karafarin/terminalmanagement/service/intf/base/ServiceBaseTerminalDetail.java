package com.karafarin.terminalmanagement.service.intf.base;

import com.karafarin.terminalmanagement.dto.base.BaseTerminalDetail;
import com.karafarin.terminalmanagement.message.Response;
import com.karafarin.terminalmanagement.service.intf.ServiceGeneric;

public interface ServiceBaseTerminalDetail extends ServiceGeneric<BaseTerminalDetail, Long>
{
    Response findSelectedTerminalDetails(Long id);
    Response findByCompanyId(Long id);
}
