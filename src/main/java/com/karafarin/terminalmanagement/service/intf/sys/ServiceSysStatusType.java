package com.karafarin.terminalmanagement.service.intf.sys;

import com.karafarin.terminalmanagement.dto.sys.SysStatusType;
import com.karafarin.terminalmanagement.service.intf.ServiceGeneric;

public interface ServiceSysStatusType extends ServiceGeneric<SysStatusType, Long>
{
}
