package com.karafarin.terminalmanagement.service.intf.base;

import com.karafarin.terminalmanagement.dto.base.BaseGuild;
import com.karafarin.terminalmanagement.message.Response;
import com.karafarin.terminalmanagement.service.intf.ServiceGeneric;

public interface ServiceBaseGuild extends ServiceGeneric<BaseGuild, Long>
{
    Response findAll(boolean all);
}
