package com.karafarin.terminalmanagement.service.intf.base;

import com.karafarin.terminalmanagement.dto.base.BaseIndex;
import com.karafarin.terminalmanagement.service.intf.ServiceGeneric;

public interface ServiceBaseIndex extends ServiceGeneric<BaseIndex, Long>
{
}
