package com.karafarin.terminalmanagement.service.intf.base;

import com.karafarin.terminalmanagement.dto.base.BaseCity;
import com.karafarin.terminalmanagement.message.Response;
import com.karafarin.terminalmanagement.service.intf.ServiceGeneric;

public interface ServiceBaseCity extends ServiceGeneric<BaseCity, Long>
{
    Response findAll(boolean all);
    Response findByStateId(Long id);
}
