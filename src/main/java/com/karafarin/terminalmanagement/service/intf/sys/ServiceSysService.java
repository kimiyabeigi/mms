package com.karafarin.terminalmanagement.service.intf.sys;

import com.karafarin.terminalmanagement.dto.sys.SysService;
import com.karafarin.terminalmanagement.service.intf.ServiceGeneric;

public interface ServiceSysService extends ServiceGeneric<SysService, Long>
{
}
