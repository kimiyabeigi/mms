package com.karafarin.terminalmanagement.service.intf.core;

import com.karafarin.terminalmanagement.message.Response;

public interface ServiceCore
{
    Response getClientInfo(String clientNumber);
    Response getAccountInfo(String clientNumber, String clientType);
    Response getAccountOwnersInfo(String accountNumber);
}
