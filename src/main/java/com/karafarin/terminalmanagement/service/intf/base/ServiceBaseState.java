package com.karafarin.terminalmanagement.service.intf.base;

import com.karafarin.terminalmanagement.dto.base.BaseState;
import com.karafarin.terminalmanagement.message.Response;
import com.karafarin.terminalmanagement.service.intf.ServiceGeneric;

import java.sql.SQLException;

public interface ServiceBaseState extends ServiceGeneric<BaseState, Long>
{
    Response findAll(boolean all);
}
