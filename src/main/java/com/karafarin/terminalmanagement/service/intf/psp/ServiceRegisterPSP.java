package com.karafarin.terminalmanagement.service.intf.psp;

import com.karafarin.terminalmanagement.message.Response;
import com.karafarin.terminalmanagement.model.psp.RegisterRequest;

import java.sql.SQLException;

public interface ServiceRegisterPSP
{
    Response registerRequest(RegisterRequest registerRequest) throws Exception;
    boolean createRegisterRequest(RegisterRequest registerRequest) throws Exception;
}
