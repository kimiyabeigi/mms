package com.karafarin.terminalmanagement.service.intf.auth;

import com.karafarin.terminalmanagement.dto.auth.User;
import com.karafarin.terminalmanagement.message.Response;
import com.karafarin.terminalmanagement.model.ChangePassword;
import com.karafarin.terminalmanagement.service.intf.ServiceGeneric;

public interface ServiceUser extends ServiceGeneric<User, Long>
{
    Response findUserByUserNameAndPassword(String userName);
    Response changePassword(ChangePassword changePassword, boolean isAdmin);
}
