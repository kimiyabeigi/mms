package com.karafarin.terminalmanagement.service.intf.auth;

import com.karafarin.terminalmanagement.dto.auth.RoleForm;
import com.karafarin.terminalmanagement.message.Response;
import com.karafarin.terminalmanagement.service.intf.ServiceGeneric;

import java.util.List;

public interface ServiceRoleForm extends ServiceGeneric<RoleForm, Long>
{
    Response addList(List<RoleForm> roleForms);
    Response updateList(List<RoleForm> roleForms);
    Response findByRoleId(Long id);
}
