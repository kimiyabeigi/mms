package com.karafarin.terminalmanagement.service.intf.psp;

import com.karafarin.terminalmanagement.dto.psp.PSPStatusBatch;
import com.karafarin.terminalmanagement.dto.psp.PSPTerminalBatch;
import com.karafarin.terminalmanagement.dto.psp.PSPTraceBatch;
import com.karafarin.terminalmanagement.message.Response;

import java.util.List;

public interface ServiceTracePSP
{
    Response findAllUnknownRequests();
    Response traceUnknownRequest(List<PSPTraceBatch> pspTraceBatches);
    Response findAllAcceptedRequests();
    Response statusAcceptedRequest(List<PSPStatusBatch> pspStatusBatches);
    Response findAllRegisterRequests();
    Response terminalRegisteredRequest(List<PSPTerminalBatch> pspTerminalBatches);
    Response traceRequestByRequestNumber(Long requestNumber);
    Response statusRequestByPSPTrackingCode(String pspTrackingCode);
    Response terminalRequestByPSPTrackingCode(String pspTrackingCode);
}
