package com.karafarin.terminalmanagement.service.intf.setting;

import com.karafarin.terminalmanagement.dto.setting.CompanySettingTerminal;
import com.karafarin.terminalmanagement.message.Response;
import com.karafarin.terminalmanagement.service.intf.ServiceGeneric;

public interface ServiceCompanySettingTerminal extends ServiceGeneric<CompanySettingTerminal, Long>
{
    Response findByCompanySettingId(Long id);
}
