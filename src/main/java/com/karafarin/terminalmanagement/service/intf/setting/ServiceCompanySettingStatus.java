package com.karafarin.terminalmanagement.service.intf.setting;

import com.karafarin.terminalmanagement.dto.setting.CompanySettingStatus;
import com.karafarin.terminalmanagement.message.Response;
import com.karafarin.terminalmanagement.service.intf.ServiceGeneric;

public interface ServiceCompanySettingStatus extends ServiceGeneric<CompanySettingStatus, Long>
{
    Response findByCompanySettingId(Long id);
}
