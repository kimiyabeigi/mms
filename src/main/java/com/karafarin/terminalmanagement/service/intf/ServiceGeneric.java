package com.karafarin.terminalmanagement.service.intf;

import com.karafarin.terminalmanagement.message.Response;

import java.io.Serializable;

public interface ServiceGeneric<T, PK  extends Serializable>
{
    Response add(T entity);
    Response findById(PK id);
    Response findAll();
    Response delete(PK id);
    Response update(T entity);
}
