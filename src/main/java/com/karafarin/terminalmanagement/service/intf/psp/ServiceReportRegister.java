package com.karafarin.terminalmanagement.service.intf.psp;

import com.karafarin.terminalmanagement.message.Response;
import com.karafarin.terminalmanagement.model.psp.ParamsReportMaster;

public interface ServiceReportRegister
{
    Response masterFindAll(ParamsReportMaster paramsReportMaster);
    Response detailFindAll(Long pspRequestId);
}
