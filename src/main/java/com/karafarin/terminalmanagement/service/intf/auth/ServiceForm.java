package com.karafarin.terminalmanagement.service.intf.auth;

import com.karafarin.terminalmanagement.dto.auth.Form;
import com.karafarin.terminalmanagement.service.intf.ServiceGeneric;

public interface ServiceForm extends ServiceGeneric<Form, Long>
{
}
