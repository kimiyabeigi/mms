package com.karafarin.terminalmanagement.service.intf.setting;

import com.karafarin.terminalmanagement.dto.setting.CompanySetting;
import com.karafarin.terminalmanagement.message.Response;
import com.karafarin.terminalmanagement.service.intf.ServiceGeneric;

public interface ServiceCompanySetting extends ServiceGeneric<CompanySetting, Long>
{
    Response findViewById(Long id);
    Response findViewAll();
}
