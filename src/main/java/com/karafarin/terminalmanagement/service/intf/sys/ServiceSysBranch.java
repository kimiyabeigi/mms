package com.karafarin.terminalmanagement.service.intf.sys;

import com.karafarin.terminalmanagement.dto.sys.SysBranch;
import com.karafarin.terminalmanagement.service.intf.ServiceGeneric;

public interface ServiceSysBranch extends ServiceGeneric<SysBranch, Long>
{
}
