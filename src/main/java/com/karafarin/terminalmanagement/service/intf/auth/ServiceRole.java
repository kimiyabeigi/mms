package com.karafarin.terminalmanagement.service.intf.auth;

import com.karafarin.terminalmanagement.dto.auth.Role;
import com.karafarin.terminalmanagement.service.intf.ServiceGeneric;

public interface ServiceRole extends ServiceGeneric<Role, Long>
{
}
