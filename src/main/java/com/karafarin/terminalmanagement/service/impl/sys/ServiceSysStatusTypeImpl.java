package com.karafarin.terminalmanagement.service.impl.sys;

import com.karafarin.terminalmanagement.dao.impl.sys.SysStatusTypeDaoImpl;
import com.karafarin.terminalmanagement.dto.sys.SysStatusType;
import com.karafarin.terminalmanagement.message.Response;
import com.karafarin.terminalmanagement.message.enums.ResponseOwnerEnum;
import com.karafarin.terminalmanagement.message.enums.ResponseStatusEnum;
import com.karafarin.terminalmanagement.model.Messages;
import com.karafarin.terminalmanagement.service.intf.sys.ServiceSysStatusType;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service
public class ServiceSysStatusTypeImpl implements ServiceSysStatusType
{
    @Autowired
    private SysStatusTypeDaoImpl sysStatusTypeDao;
    @Autowired
    private Messages messages;

    private static final Logger logger = LogManager.getLogger(ServiceSysStatusTypeImpl.class);
    
    @Override
    public Response add(SysStatusType entity)
    {
        return null;
    }

    @Override
    public Response findById(Long id)
    {
        Response response = new Response();
        try
        {
            logger.info("Start method [findById] for [SysStatusType] via id [" + id + "]");
            
            SysStatusType sysStatusType = sysStatusTypeDao.findById(id);
            response.setData(sysStatusType);
            if (sysStatusType.getStatusTypeId() != null)
            {
                response.setStatus(ResponseStatusEnum.SUCCESS);
                response.setDescription(messages.get("baseEntity.find.success"));
            }
            else
            {
                response.setStatus(ResponseStatusEnum.ERROR);
                response.setDescription(messages.get("baseEntity.find.error"));
            }
            
            logger.info("End successfully method [findById] for [SysStatusType] via id [" + id + "]");            
        } catch (Exception e)
        {
            logger.error("There is an error in method [findById] for [SysStatusType] via id [" + id + "]", e);
            
            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;

    }

    @Override
    public Response findAll()
    {
        Response response = new Response();
        try
        {
            logger.info("Start method [findAll] for [SysStatusType]");
            
            response.setData(sysStatusTypeDao.findAll());
            response.setStatus(ResponseStatusEnum.SUCCESS);
            response.setDescription(messages.get("baseEntity.find.success"));

            logger.info("End successfully method [findAll] for [SysStatusType]");
        } catch (Exception e)
        {
            logger.error("There is an error in method [findAll] for [SysStatusType]", e);
            
            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }

    @Override
    public Response delete(Long id)
    {
        return null;
    }

    @Override
    public Response update(SysStatusType entity)
    {
        return null;
    }

}
