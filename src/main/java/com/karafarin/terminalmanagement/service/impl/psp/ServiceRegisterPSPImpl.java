package com.karafarin.terminalmanagement.service.impl.psp;

import com.karafarin.terminalmanagement.dao.impl.base.*;
import com.karafarin.terminalmanagement.dao.impl.psp.*;
import com.karafarin.terminalmanagement.dao.impl.setting.CompanySettingDaoImpl;
import com.karafarin.terminalmanagement.dao.impl.setting.CompanySettingTerminalDaoImpl;
import com.karafarin.terminalmanagement.dao.impl.sys.*;
import com.karafarin.terminalmanagement.dto.base.*;
import com.karafarin.terminalmanagement.dto.psp.*;
import com.karafarin.terminalmanagement.dto.setting.CompanySetting;
import com.karafarin.terminalmanagement.dto.sys.*;
import com.karafarin.terminalmanagement.message.Response;
import com.karafarin.terminalmanagement.message.enums.PositionEnum;
import com.karafarin.terminalmanagement.message.enums.RequestStatusEnum;
import com.karafarin.terminalmanagement.message.enums.ResponseStatusEnum;
import com.karafarin.terminalmanagement.message.enums.TerminalEnum;
import com.karafarin.terminalmanagement.model.Messages;
import com.karafarin.terminalmanagement.model.psp.WrapperResponse;
import com.karafarin.terminalmanagement.model.psp.RegisterRequest;
import com.karafarin.terminalmanagement.service.intf.psp.ServiceRegisterPSP;
import com.karafarin.terminalmanagement.util.UserUtil;
import com.karafarin.terminalmanagement.util.date.DateConverterUtil;
import com.karafarin.terminalmanagement.wrapper.enums.PSPEnum;
import com.karafarin.terminalmanagement.wrapper.enums.ServiceEnum;
import com.karafarin.terminalmanagement.wrapper.enums.StatusTypeEnum;
import com.karafarin.terminalmanagement.wrapper.model.req.PosRegisterRequest;
import com.karafarin.terminalmanagement.wrapper.model.resp.PosRegisterResponse;
import com.karafarin.terminalmanagement.wrapper.service.impl.ServicePosRegisterReqImpl;
import com.karafarin.terminalmanagement.wrapper.util.WrapperUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


@Service
@Transactional
public class ServiceRegisterPSPImpl implements ServiceRegisterPSP
{
    private static final Logger logger = LogManager.getLogger(ServiceRegisterPSPImpl.class);

    @Autowired
    private PSPRequestDaoImpl pspRequestDao;
    @Autowired
    private PSPClientDaoImpl clientDao;
    @Autowired
    private PSPAccountDaoImpl accountDao;
    @Autowired
    private PSPAccountOwnerDaoImpl accountOwnerDao;
    @Autowired
    private PSPShopDaoImpl pspShopDao;
    @Autowired
    private PSPIPGDaoImpl pspipgDao;
    @Autowired
    private BaseCompanyDaoImpl baseCompanyDao;
    @Autowired
    private SysTerminalDaoImpl sysTerminalDao;
    @Autowired
    private SysBranchDaoImpl sysBranchDao;
    @Autowired
    private BaseGuildDaoImpl baseGuildDao;
    @Autowired
    private BaseStateDaoImpl baseStateDao;
    @Autowired
    private BaseCityDaoImpl baseCityDao;
    @Autowired
    private SysENamadDaoImpl sysENamadDao;
    @Autowired
    private BaseTerminalDetailDaoImpl baseTerminalDetailDao;
    @Autowired
    private ServicePosRegisterReqImpl servicePosRegisterReq;
    @Autowired
    private SysRequestStatusDaoImpl sysRequestStatusDao;
    @Autowired
    private Messages messages;
    @Autowired
    private PSPRequestStatusDaoImpl pspRequestStatusDao;
    @Autowired
    private SysPositionDaoImpl sysPositionDao;
    @Autowired
    private CompanySettingTerminalDaoImpl companySettingTerminalDao;
    @Autowired
    private CompanySettingDaoImpl companySettingDao;

    @Value("${ik.ipg.connection.type}")
    private String ipgConnectionType;

    @Override
    public Response registerRequest(RegisterRequest registerRequest) throws Exception
    {
        Response response = new Response();
        WrapperResponse wrapperResponse;

        logger.info("Start of method [registerRequest]");

        if (registerRequest.getPspRequestId() == null)
        {
            if (createRegisterRequest(registerRequest))
            {
                wrapperResponse = makeRegisterRequest(registerRequest);

                response.setStatus(ResponseStatusEnum.SUCCESS);
                response.setDescription(messages.get("psp.system.request.success"));
                response.setData(wrapperResponse);
            } else
            {
                response.setStatus(ResponseStatusEnum.ERROR);
                response.setDescription(messages.get("system.error"));
                response.setData(null);
            }
        } else
        {
            wrapperResponse = makeRegisterRequest(registerRequest);

            response.setStatus(ResponseStatusEnum.SUCCESS);
            response.setDescription(messages.get("psp.system.request.success"));
            response.setData(wrapperResponse);
        }

        logger.info("End of method [registerRequest]");

        return response;
    }

    @Override
    public boolean createRegisterRequest(RegisterRequest registerRequest) throws Exception
    {
        boolean result = false;
        logger.info("Start of method [createRegisterRequest]");

        //Starting of adding PSPRequest
        PSPRequest pspRequest = new PSPRequest();
        pspRequest.setBranchId(UserUtil.getCurrentUser().getBranchId());
        pspRequest.setUserId(UserUtil.getCurrentUser().getUserId());
        pspRequest.setComputerName(UserUtil.getCurrentUser().getComputerName());
        pspRequest.setIpAddress(UserUtil.getCurrentUser().getIp());
        pspRequest.setCompanyId(registerRequest.getCompanyId());
        pspRequest.setTerminalId(registerRequest.getTerminalId());
        Long newPSPRequestId = addPSPRequest(pspRequest);
        registerRequest.setPspRequestId(newPSPRequestId);

        pspRequest = findPSPRequestById(newPSPRequestId);
        registerRequest.setRequestDate(pspRequest.getRequestDate());
        registerRequest.setRequestNumber(pspRequest.getRequestNumber());
        //Ending of adding PSPRequest

        //Starting of adding PSPClient
        if (registerRequest.getPspClients() != null)
        {
            for (PSPClient pspClient : registerRequest.getPspClients()) {
                pspClient.setPspRequestId(pspRequest.getPspRequestId());
                pspClient.setClientId(addPSPClient(pspClient));
            }
        }
        //Ending of adding PSPClient

        //Starting of adding PSPAccount
        registerRequest.getPspAccount().setPspRequestId(pspRequest.getPspRequestId());
        registerRequest.getPspAccount().setAccountId(addPSPAccount(registerRequest.getPspAccount()));
        //Ending of adding PSPAccount

        //Starting of adding PSPAccountOwner
        if (registerRequest.getPspAccountOwners() != null)
        {
            for (PSPAccountOwner pspAccountOwner : registerRequest.getPspAccountOwners())
            {
                pspAccountOwner.setPspRequestId(pspRequest.getPspRequestId());
                pspAccountOwner.setAccountOwnerId(addPSPAccountOwner(pspAccountOwner));
            }
        }
        //Ending of adding PSPAccountOwner

        //Starting of adding PSPShop
        if (registerRequest.getTerminalId() != TerminalEnum.MOB.getValue())
        {
            registerRequest.getPspShop().setPspRequestId(pspRequest.getPspRequestId());
            registerRequest.getPspShop().setShopId(addPSPShop(registerRequest.getPspShop()));
        }
        //Ending of adding PSPShop

        //Starting of adding PSPIPG
        if (registerRequest.getTerminalId() != TerminalEnum.MOB.getValue())
        {
            registerRequest.getPspipg().setPspRequestId(pspRequest.getPspRequestId());
            registerRequest.getPspipg().setIPGId(addPSPIPG(registerRequest.getPspipg()));
        }
        //Ending of adding PSPIPG

        result = true;
        logger.info("End of method [createRegisterRequest]");

        return result;
    }

    private WrapperResponse makeRegisterRequest(RegisterRequest registerRequest) throws Exception
    {
        WrapperResponse result = new WrapperResponse();
        SysRequestStatus sysRequestStatus = new SysRequestStatus();

        PosRegisterRequest posRegisterRequest = new PosRegisterRequest();
        BaseCompany baseCompany = findBaseCompanyById(registerRequest.getCompanyId());

        posRegisterRequest.setCompanyCode(baseCompany.getCompanyCode());
        posRegisterRequest.setRegisterFields(makeContentTag(registerRequest));
        PosRegisterResponse posRegisterResponse = servicePosRegisterReq.posRegisterReq(baseCompany.getCompanyCode(),
                                                                                       posRegisterRequest);

        PSPRequestStatus pspRequestStatus = new PSPRequestStatus();
        pspRequestStatus.setPspErrorCode(posRegisterResponse.getErrorCode());
        pspRequestStatus.setPspErrorDesc(posRegisterResponse.getErrorDesc());
        pspRequestStatus.setPspRequestId(registerRequest.getPspRequestId());
        pspRequestStatus.setServiceId(ServiceEnum.REGISTER.getValue());

        result.setErrorDesc(posRegisterResponse.getErrorDesc());

        if (StringUtils.isNotEmpty(posRegisterResponse.getTrackingCode()))
        {
            registerRequest.setPspTrackingCode(posRegisterResponse.getTrackingCode());
            updateTrackingCodePSPRequest(registerRequest.getPspRequestId(), posRegisterResponse.getTrackingCode());
        }

        if (posRegisterResponse.getStatusTypeId().equals(StatusTypeEnum.CHANNEL_ERROR.getValue()) ||
            posRegisterResponse.getStatusTypeId().equals(StatusTypeEnum.EXCEPTION.getValue()))
        {
            updatePSPRequestRequestStatusId(registerRequest.getPspRequestId(), RequestStatusEnum.NOT_REGISTERED.getValue());
            pspRequestStatus.setStatusTypeId(StatusTypeEnum.EXCEPTION.getValue());
            pspRequestStatus.setRequestStatusId(RequestStatusEnum.NOT_REGISTERED.getValue());
            result.setStatusEn(StatusTypeEnum.EXCEPTION.toString());
            sysRequestStatus = findRequestStatusById(RequestStatusEnum.NOT_REGISTERED.getValue());
        } else if (posRegisterResponse.getStatusTypeId().equals(StatusTypeEnum.SUCCESS.getValue()))
        {
            updatePSPRequestRequestStatusId(registerRequest.getPspRequestId(), RequestStatusEnum.REGISTERED.getValue());
            pspRequestStatus.setStatusTypeId(posRegisterResponse.getStatusTypeId());
            result.setStatusEn(StatusTypeEnum.SUCCESS.toString());
            pspRequestStatus.setRequestStatusId(RequestStatusEnum.REGISTERED.getValue());
            sysRequestStatus = findRequestStatusById(RequestStatusEnum.REGISTERED.getValue());
        } else if (posRegisterResponse.getStatusTypeId().equals(StatusTypeEnum.FAIL.getValue()))
        {
            updatePSPRequestRequestStatusId(registerRequest.getPspRequestId(), RequestStatusEnum.REJECT.getValue());
            pspRequestStatus.setStatusTypeId(posRegisterResponse.getStatusTypeId());
            result.setStatusEn(StatusTypeEnum.FAIL.toString());
            pspRequestStatus.setRequestStatusId(RequestStatusEnum.REJECT.getValue());
            sysRequestStatus = findRequestStatusById(RequestStatusEnum.REJECT.getValue());
        } else if (posRegisterResponse.getStatusTypeId().equals(StatusTypeEnum.UNKNOWN.getValue()))
        {
            updatePSPRequestRequestStatusId(registerRequest.getPspRequestId(), RequestStatusEnum.UNKNOWN.getValue());
            pspRequestStatus.setStatusTypeId(posRegisterResponse.getStatusTypeId());
            result.setStatusEn(StatusTypeEnum.UNKNOWN.toString());
            pspRequestStatus.setRequestStatusId(RequestStatusEnum.UNKNOWN.getValue());
            sysRequestStatus = findRequestStatusById(RequestStatusEnum.UNKNOWN.getValue());
        }

        Long newPSPRequestId = addPSPRequestStatus(pspRequestStatus);
        registerRequest.setPspRequestStatusId(newPSPRequestId);

        result.setStatus(sysRequestStatus.getDescription());
        result.setErrorDesc(pspRequestStatus.getPspErrorDesc());
        result.setPspTrackingCode(posRegisterResponse.getTrackingCode());
        result.setRequestNumber(registerRequest.getRequestNumber());
        result.setData(registerRequest);

        return result;
    }

    private Long addPSPRequest(PSPRequest entity) throws Exception
    {
        logger.info("Start method [addPSPRequest] for [PSPRequest]");

        Long newPSPRequestId = pspRequestDao.add(entity);
        entity.setPspRequestId(newPSPRequestId);

        logger.info("End successfully method [addPSPRequest] for [PSPRequest] with new id [" + newPSPRequestId + "]");

        return entity.getPspRequestId();
    }

    private Long addPSPRequestStatus(PSPRequestStatus entity) throws Exception
    {
        logger.info("Start method [addPSPRequestStatus] for [PSPRequestStatus]");

        Long newPSPRequestStatusId = pspRequestStatusDao.add(entity);
        entity.setPspRequestId(newPSPRequestStatusId);

        logger.info("End successfully method [addPSPRequestStatus] for [PSPRequestStatus] with new id [" +
                    newPSPRequestStatusId + "]");

        return entity.getPspRequestId();
    }

    private boolean updatePSPRequestStatus(PSPRequestStatus entity) throws Exception
    {
        logger.info("Start method [updatePSPRequestStatus] for [PSPRequestStatus] via id [" +
                    entity.getPspRequestStatusId() + "]");

        boolean result;
        result = pspRequestStatusDao.update(entity);

        logger.info("End successfully method [updatePSPRequestStatus] for [PSPRequestStatus] via id [" +
                    entity.getPspRequestStatusId() + "]");

        return result;
    }

    private PSPRequest findPSPRequestById(Long id) throws Exception
    {
        PSPRequest result = new PSPRequest();
        logger.info("Start method [findPSPRequestById] for [PSPRequest] via id [" + id + "]");

        result = pspRequestDao.findById(id);

        logger.info("End successfully method [findPSPRequestById] for [PSPRequest] via id [" + id + "]");

        return result;
    }

    private List<PSPRequestStatus> findPSPRequestStatusByPSPRequestId(Long id) throws Exception
    {
        List<PSPRequestStatus> result = new ArrayList<>();
        logger.info("Start method [findPSPRequestStatusByPSPRequestId] for [PSPRequestStatus] via id [" + id + "]");

        result = pspRequestStatusDao.findByPSPRequestId(id);

        logger.info(
                "End successfully method [findPSPRequestStatusByPSPRequestId] for [PSPRequestStatus] via id [" + id +
                "]");

        return result;
    }

    private SysRequestStatus findRequestStatusById(Long id) throws Exception
    {
        SysRequestStatus result = new SysRequestStatus();
        logger.info("Start method [findRequestStatusById] for [SysRequestStatus] via id [" + id + "]");

        result = sysRequestStatusDao.findById(id);

        logger.info("End successfully method [findRequestStatusById] for [SysRequestStatus] via id [" + id + "]");

        return result;
    }

    private boolean updateTrackingCodePSPRequest(Long id, String trackingCode) throws Exception
    {
        boolean result;
        logger.info("Start method [updatePSPRequest] for [PSPRequest] via id [" + id + "]");

        result = pspRequestDao.updateTrackingCode(id, trackingCode);

        logger.info("End successfully method [updatePSPRequest] for [PSPRequest] via id [" + id + "]");

        return result;
    }

    private boolean updatePSPRequestRequestStatusId(Long id, Long requestStatusId) throws Exception
    {
        boolean result;
        logger.info("Start method [updatePSPRequestRequestStatusId] for [PSPRequest] via id [" + id + "]");

        result = pspRequestDao.updateRequestStatusId(id, requestStatusId);

        logger.info("End successfully method [updatePSPRequestRequestStatusId] for [PSPRequest] via id [" + id + "]");

        return result;
    }

    private Long addPSPClient(PSPClient entity) throws Exception
    {
        logger.info("Start method [addPSPClient] for [PSPClient]");

        Long newClientId = clientDao.add(entity);
        entity.setClientId(newClientId);

        logger.info("End successfully method [addPSPClient] for [PSPClient] with new id [" + newClientId + "]");

        return entity.getClientId();
    }

    private Long addPSPAccount(PSPAccount entity) throws Exception
    {
        logger.info("Start method [addPSPAccount] for [PSPAccount]");

        Long newAccountId = accountDao.add(entity);
        entity.setAccountId(newAccountId);

        logger.info("End successfully method [addPSPAccount] for [PSPAccount] with new id [" + newAccountId + "]");

        return entity.getAccountId();
    }

    private Long addPSPAccountOwner(PSPAccountOwner entity) throws Exception
    {
        logger.info("Start method [addPSPAccountOwner] for [PSPAccountOwner]");

        Long newAccountOwnerId = accountOwnerDao.add(entity);
        entity.setAccountOwnerId(newAccountOwnerId);

        logger.info(
                "End successfully method [addPSPAccountOwner] for [PSPAccountOwner] with new id [" + newAccountOwnerId +
                "]");

        return entity.getAccountOwnerId();
    }

    private Long addPSPShop(PSPShop entity) throws Exception
    {
        logger.info("Start method [addPSPShop] for [PSPShop]");

        Long newShopId = pspShopDao.add(entity);
        entity.setShopId(newShopId);

        logger.info("End successfully method [addPSPShop] for [PSPShop] with new id [" + newShopId + "]");

        return entity.getShopId();
    }

    private Long addPSPIPG(PSPIPG entity) throws Exception
    {
        logger.info("Start method [addPSPIPG] for [PSPIPG]");

        Long newIPGId = pspipgDao.add(entity);
        entity.setIPGId(newIPGId);

        logger.info("End successfully method [addPSPIPG] for [PSPIPG] with new id [" + newIPGId + "]");

        return entity.getIPGId();
    }

    private BaseCompany findBaseCompanyById(Long id) throws Exception
    {
        BaseCompany result = new BaseCompany();

        logger.info("Start method [findBaseCompanyById] for [PSPRequest] via id [" + id + "]");

        result = baseCompanyDao.findById(id);

        logger.info("End successfully method [findBaseCompanyById] for [PSPRequest] via id [" + id + "]");

        return result;
    }

    private CompanySetting findCompanySettingByCompanyId(Long id) throws SQLException
    {
        CompanySetting result = new CompanySetting();

        logger.info("Start method [findCompanySettingByCompanyId] for [PSPRequest] via companyId [" + id + "]");

        result = companySettingDao.findByCompanyId(id);

        logger.info("End successfully method [findCompanySettingByCompanyId] for [PSPRequest] via companyId [" + id + "]");

        return result;
    }

    private SysTerminal findSysTerminalById(Long id) throws Exception
    {
        SysTerminal result = new SysTerminal();

        logger.info("Start method [findSysTerminalById] for [SysTerminal] via id [" + id + "]");

        result = sysTerminalDao.findById(id);

        logger.info("End successfully method [findSysTerminalById] for [SysTerminal] via id [" + id + "]");

        return result;
    }

    private SysBranch findSysBranchById(Long id) throws Exception
    {
        SysBranch result = new SysBranch();

        logger.info("Start method [findSysBranchById] for [SysBranch] via id [" + id + "]");

        result = sysBranchDao.findById(id);

        logger.info("End successfully method [findSysBranchById] for [SysBranch] via id [" + id + "]");

        return result;
    }

    private SysBranch findSupervisorBranch() throws Exception
    {
        SysBranch result = new SysBranch();

        logger.info("Start method [findSupervisorBranch] for [SysBranch]");

        result = sysBranchDao.findSupervisorBranch();

        logger.info("End successfully method [findSupervisorBranch] for [SysBranch]");

        return result;
    }

    private BaseGuild findBaseGuildById(Long id) throws Exception
    {
        BaseGuild result = new BaseGuild();

        logger.info("Start method [findBaseGuildById] for [BaseGuild] via id [" + id + "]");

        result = baseGuildDao.findById(id);

        logger.info("End successfully method [findBaseGuildById] for [BaseGuild] via id [" + id + "]");

        return result;
    }

    private BaseState findBaseStateById(Long id) throws Exception
    {
        BaseState result = new BaseState();

        logger.info("Start method [findBaseStateById] for [BaseState] via id [" + id + "]");

        result = baseStateDao.findById(id);

        logger.info("End successfully method [findBaseStateById] for [BaseState] via id [" + id + "]");

        return result;
    }

    private BaseCity findBaseCityById(Long id) throws Exception
    {
        BaseCity result = new BaseCity();

        logger.info("Start method [findBaseCityById] for [BaseCity] via id [" + id + "]");

        result = baseCityDao.findById(id);

        logger.info("End successfully method [findBaseCityById] for [BaseCity] via id [" + id + "]");

        return result;
    }

    private SysEnamad findSysEnamadById(Long id) throws Exception
    {
        SysEnamad result = new SysEnamad();

        logger.info("Start method [findSysEnamadById] for [SysEnamad] via id [" + id + "]");

        result = sysENamadDao.findById(id);

        logger.info("End successfully method [findSysEnamadById] for [SysEnamad] via id [" + id + "]");

        return result;
    }

    private SysPosition findSysPositionById(Long id) throws Exception
    {
        SysPosition result = new SysPosition();

        logger.info("Start method [findSysPositionById] for [SysPosition] via id [" + id + "]");

        result = sysPositionDao.findById(id);

        logger.info("End successfully method [findSysPositionById] for [SysPosition] via id [" + id + "]");

        return result;
    }

    private BaseTerminalDetail findBaseTerminalDetailById(Long id) throws Exception
    {
        BaseTerminalDetail result = new BaseTerminalDetail();

        logger.info("Start method [findBaseTerminalDetailById] for [BaseTerminalDetail] via id [" + id + "]");

        result = baseTerminalDetailDao.findById(id);

        logger.info(
                "End successfully method [findBaseTerminalDetailById] for [BaseTerminalDetail] via id [" + id + "]");

        return result;
    }

    private String makeContentTag(RegisterRequest registerRequest) throws Exception
    {
        try
        {
            StringBuilder contentTag = new StringBuilder();
            BaseCompany baseCompany = findBaseCompanyById(registerRequest.getCompanyId());

            contentTag.append(makeRequestTag(registerRequest, baseCompany.getCompanyCode()));

            return contentTag.toString();
        } catch (Exception e)
        {
            logger.error("There is an error in method [makeContentTag] ", e);
            e.printStackTrace();
            throw e;
        }
    }

    private String makeRequestTag(RegisterRequest registerRequest, String companyCode) throws Exception
    {
        try
        {
            String result = null;
            if (companyCode.equalsIgnoreCase(PSPEnum.IRAN_KISH.getValue()))
            {
                result = makeIKTagContent(registerRequest);
            } else if (companyCode.equalsIgnoreCase(PSPEnum.TEJARAT_PARSIAN.getValue()))
            {
                result = makeTPTagContent(registerRequest);
            }

            return result;
        } catch (Exception e)
        {
            logger.error("There is an error in method [makeRequestTag] ", e);
            e.printStackTrace();
            throw e;
        }
    }

    private String makeIKTagContent(RegisterRequest registerRequest) throws Exception
    {
        try
        {
            StringBuilder result = new StringBuilder();
            List<PSPClient> pspClients = registerRequest.getPspClients();
            PSPAccount pspAccount = registerRequest.getPspAccount();
            PSPClient pspClient = null;
            String terminalModel = "";
            SysEnamad sysEnamad = null;

            for (PSPClient item : pspClients)
            {
                if (item.getClientKind().equalsIgnoreCase("O"))
                {
                    pspClient = item;
                }
            }

            SysBranch sysBranch = findSysBranchById(pspAccount.getBranchId());
            SysTerminal sysTerminal = findSysTerminalById(registerRequest.getTerminalId());
            BaseGuild baseGuild = findBaseGuildById(registerRequest.getPspShop().getGuildId());
            BaseCity baseCity = findBaseCityById(registerRequest.getPspShop().getCityId());

            if (registerRequest.getPspipg().getTerminalDetailId() != null)
            {
                terminalModel = companySettingTerminalDao.findConnectionCode(registerRequest.getCompanyId(),
                                                                             registerRequest.getTerminalId(),
                                                                             registerRequest.getPspipg().getTerminalDetailId());
            }
            else
            {
                terminalModel = ipgConnectionType; //for IPG (IranKish)
            }

            boolean isIndividual = pspClient.getClientType().equalsIgnoreCase("I");
            String customerType;
            if (pspClient.getClientType().equalsIgnoreCase("I"))
            {
                customerType = !pspClient.getCountryNameAbbr().equalsIgnoreCase("IR") ? "3" : "1";
            } else
            {
                customerType = "2";
            }

            PSPShop pspShop = registerRequest.getPspShop();
            PSPIPG pspipg = registerRequest.getPspipg();

            if (pspipg.geteNamadId() != null)
            {
                sysEnamad = sysENamadDao.findById(pspipg.geteNamadId());
            }

            result.append("<ParentAcceptorNo></ParentAcceptorNo>")
                  .append("<ParentTrackingCode></ParentTrackingCode>")
                  .append("<Acceptor>")
                        .append("<BusinessType></BusinessType>")
                        .append("<AcceptorType>").append(sysTerminal.getCode()).append("</AcceptorType>")
                        .append("<ESymbolType>").append(sysEnamad != null ? sysEnamad.getEnamadCode() : "").append("</ESymbolType>")
                        .append("<OwnershipType>")
                            .append(registerRequest.getPspShop().getOwnershipType()).append("</OwnershipType>")
                        .append("<Address>").append(pspClient.getAddress()).append("</Address>")
                        .append("<AddressCode>").append(pspClient.getAddress()).append("</AddressCode>")
                        .append("<BMI>627488</BMI>")
                        .append("<BankId>").append(WrapperUtil.getBankCode()).append("</BankId>")
                        .append("<BusinessCertificateNumber>")
                            .append(registerRequest.getPspShop().getLicenseNo()).append("</BusinessCertificateNumber>")
                        .append("<BussinessCategorySupl_Code>")
                            .append(baseGuild.getGuildCode()).append("</BussinessCategorySupl_Code>")
                        .append("<CertificateExpiryDate>")
                            .append(DateConverterUtil.convertDateToISO8601(DateConverterUtil
                                            .convertStringWithAndWithoutDelimiterToDate(pspShop.getLicenseValidityDate())))
                            .append("</CertificateExpiryDate>")
                        .append("<CertificateIssueDate>")
                            .append(DateConverterUtil.convertDateToISO8601(DateConverterUtil
                                            .convertStringWithAndWithoutDelimiterToDate(pspShop.getLicenseIssuanceDate())))
                        .append("</CertificateIssueDate>")
                        .append("<Country_Code>").append(pspClient.getCountryNameAbbr()).append("</Country_Code>")
                        .append("<City_Code>").append(baseCity.getCityCode()).append("</City_Code>")
                        .append("<ESymbolRegistrationDate>")
                            .append(DateConverterUtil.convertDateToISO8601(
                                    DateConverterUtil.convertStringWithAndWithoutDelimiterToDate(pspipg.getIssuanceDate())))
                            .append("</ESymbolRegistrationDate>")
                        .append("<ESymbolValidityDate>")
                            .append(DateConverterUtil.convertDateToISO8601(
                                    DateConverterUtil.convertStringWithAndWithoutDelimiterToDate(pspipg.getExpiryDate())))
                            .append("</ESymbolValidityDate>")
                        .append("<RentalExpiryDate>")
                            .append(DateConverterUtil.convertDateToISO8601(
                                    DateConverterUtil.convertStringWithAndWithoutDelimiterToDate(pspShop.getContractExpiryDate())))
                            .append("</RentalExpiryDate>")
                        .append("<EmailAddress>").append(pspipg.getEmail()).append("</EmailAddress>")
                        .append("<EnglishName>").append(registerRequest.getPspShop().getShopNameEn()).append("</EnglishName>")
                        .append("<FarsiName>").append(registerRequest.getPspShop().getShopName()).append("</FarsiName>")
                        .append("<HasESymbol>").append(sysEnamad != null ? "1" : "0").append("</HasESymbol>")
                        .append("<PostalCode>").append(pspClient.getPostalCode()).append("</PostalCode>")
                        .append("<RentalContractNumber>").append(registerRequest.getPspShop().getContractNo())
                            .append("</RentalContractNumber>")
                        .append("<TechnicalPersonFirstName>").append(pspipg.getFirstName()).append("</TechnicalPersonFirstName>")
                        .append("<TechnicalPersonLastName>").append(pspipg.getLastName()).append("</TechnicalPersonLastName>")
                        .append("<TechnicalPersonMobil>").append(pspipg.getMobilePhone()).append("</TechnicalPersonMobil>")
                        .append("<TechnicalPersonPhone>").append(pspipg.getPhone()).append("</TechnicalPersonPhone>")
                        .append("<TelephoneNumber>").append(pspClient.getPhone()).append("</TelephoneNumber>")
                        .append("<WebsiteAddress>").append(pspipg.getSiteAddress()).append("</WebsiteAddress>")
                  .append("</Acceptor>")

                  .append("<accounts>")
                        .append("<account>")
                            .append("<IbanNoe>").append(pspAccount.getIbanNo()).append("</IbanNoe>")
                            .append("<AccountType></AccountType>")
                            .append("<AccountNo>").append(pspAccount.getAccountNo()).append("</AccountNo>")
                            .append("<Description></Description>")
                            .append("<Division_Code>").append(sysBranch.getBranchCode()).append("</Division_Code>")
                            .append("<MultiplexPercent></MultiplexPercent>")
                            .append("<people>");
                            for (PSPClient item : pspClients)
                            {
                                result.append("<person>")
                                      .append("<Gender>").append(item.getSex()).append("</Gender>")
                                      .append("<BirthDate>")
                                        .append(DateConverterUtil.convertDateToISO8601(
                                                DateConverterUtil.convertStringWithAndWithoutDelimiterToDate(item.getBirthDate())))
                                      .append("</BirthDate>")
                                      .append("<PassportExpireDate>")
                                        .append(DateConverterUtil.convertDateToISO8601(
                                                DateConverterUtil.convertStringWithAndWithoutDelimiterToDate(item.getPassportExpiryDate())))
                                      .append("</PassportExpireDate>")
                                      .append("<NationalCode>").append(item.getNationalCode()).append("</NationalCode>")
                                      .append("<Address>").append(item.getAddress()).append("</Address>")
                                      .append("<CellPhoneNumber>").append(item.getMobilePhone()).append("</CellPhoneNumber>")
                                      .append("<CountryCode>").append(item.getCountryNameAbbr()).append("</CountryCode>")
                                      .append("<FatherNameEn>").append(item.getFatherNameEn()).append("</FatherNameEn>")
                                      .append("<FatherNameFa>").append(item.getFatherName()).append("</FatherNameFa>")
                                      .append("<FirstNameEn>").append(item.getFirstNameEn()).append("</FirstNameEn>")
                                      .append("<ForeignPervasiveCode>").append(item.getForeignCode()).append("</ForeignPervasiveCode>")
                                      .append("<IsAlive>")
                                        .append(item.getLifeStatus() == null ? false :
                                                item.getLifeStatus().equalsIgnoreCase("N") ? true : false).append("</IsAlive>")
                                      .append("<LastNameEn>").append(item.getLastNameEn()).append("</LastNameEn>")
                                      .append("<LastNameFa>").append(item.getLastName()).append("</LastNameFa>")
                                      .append("<PassportNumber>")
                                        .append(item.getPassportNumber()).append("</PassportNumber>")
                                      .append("<ForeignLicenceNo>").append(item.getForeignLicenseNo()).append("</ForeignLicenceNo>")
                                      .append("<ForeignLicenceIssuer>").append(item.getForeignLicenseIssuer()).append("</ForeignLicenceIssuer>")
                                      .append("<TelephoneNumber>").append(item.getPhone()).append("</TelephoneNumber>")
                                      .append("<ZipCode>").append(item.getPostalCode()).append("</ZipCode>")
                                .append("</person>");
                            }
                            result.append("</people>")
                        .append("</account>")
                  .append("</accounts>")

                  .append("<Customer>")
                        .append("<CustomerType>").append(customerType).append("</CustomerType>")
                        .append("<AddressCode>").append(pspClient.getAddress()).append("</AddressCode>")
                        .append("<City_Code>").append(baseCity.getCityCode()).append("</City_Code>")
                        .append("<City_Id>").append(baseCity.getCityId()).append("</City_Id>")
                        .append("<ComNameFa>").append(pspShop.getShopName()).append("</ComNameFa>")
                        .append("<ComNameEn>").append(pspShop.getShopNameEn()).append("</ComNameEn>")
                        .append("<Fax>").append(pspClient.getFax()).append("</Fax>")
                        .append("<NationalLegalCode>").append(pspClient.getNationalCode()).append("</NationalLegalCode>")
                        .append("<Phone>").append(pspClient.getPhone()).append("</Phone>")
                        .append("<RegisterNumber>")
                            .append(isIndividual ? "" :
                                    DateConverterUtil.convertDateToISO8601(
                                            DateConverterUtil.convertStringWithAndWithoutDelimiterToDate(pspClient.getRegNumber())))
                        .append("</RegisterNumber>")
                        .append("<ZipCode>").append(pspClient.getPostalCode()).append("</ZipCode>")
                        .append("<RegisterDate>")
                            .append(isIndividual ? "" :
                                    DateConverterUtil.convertDateToISO8601(
                                            DateConverterUtil.convertStringWithAndWithoutDelimiterToDate(pspClient.getBirthDate())))
                        .append("</RegisterDate>")
                        .append("<CommercialCode>").append(pspClient.getCommerceCode()).append("</CommercialCode>")
                  .append("</Customer>")

                  .append("<people>");
                        for (PSPClient item : pspClients)
                        {
                            result.append("<Signatory>")
                                  .append("<person>")
                                        .append("<Gender>").append(item.getSex()).append("</Gender>")
                                        .append("<BirthDate>")
                                            .append(DateConverterUtil.convertDateToISO8601(
                                                    DateConverterUtil.convertStringWithAndWithoutDelimiterToDate(item.getBirthDate())))
                                        .append("</BirthDate>")
                                        .append("<PassportExpireDate>")
                                            .append(DateConverterUtil.convertDateToISO8601(
                                                    DateConverterUtil.convertStringWithAndWithoutDelimiterToDate(item.getPassportExpiryDate())))
                                        .append("</PassportExpireDate>")
                                        .append("<NationalCode>").append(item.getNationalCode()).append("</NationalCode>")
                                        .append("<Address>").append(item.getAddress()).append("</Address>")
                                        .append("<CellPhoneNumber>").append(item.getMobilePhone()).append("</CellPhoneNumber>")
                                        .append("<CountryCode>").append(item.getCountryNameAbbr()).append("</CountryCode>")
                                        .append("<FatherNameEn>").append(item.getFatherNameEn()).append("</FatherNameEn>")
                                        .append("<FatherNameFa>").append(item.getFatherName()).append("</FatherNameFa>")
                                        .append("<FirstNameEn>").append(item.getFirstNameEn()).append("</FirstNameEn>")
                                        .append("<FirstNameFa>").append(item.getFirstName()).append("</FirstNameFa>")
                                        .append("<ForeignPervasiveCode>").append(item.getForeignCode()).append("</ForeignPervasiveCode>")
                                        .append("<IsAlive>")
                                            .append(item.getLifeStatus() == null ? false :
                                                    item.getLifeStatus().equalsIgnoreCase("N") ? true : false)
                                        .append("</IsAlive>")
                                        .append("<LastNameEn>").append(item.getLastNameEn()).append("</LastNameEn>")
                                        .append("<LastNameFa>").append(item.getLastName()).append("</LastNameFa>")
                                        .append("<PassportNumber>").append(item.getPassportNumber()).append("</PassportNumber>")
                                        .append("<ForeignLicenceNo>").append(item.getForeignLicenseNo()).append("</ForeignLicenceNo>")
                                        .append("<ForeignLicenceIssuer>").append(item.getForeignLicenseIssuer()).append("</ForeignLicenceIssuer>")
                                        .append("<TelephoneNumber>").append(item.getPhone()).append("</TelephoneNumber>")
                                        .append("<ZipCode>").append(item.getPostalCode()).append("</ZipCode>")
                                  .append("</person>")
                                  .append("<isManager>")
                                        .append(item.getPositionId() != null &&
                                                item.getPositionId().equals(PositionEnum.CEO.getValue()) ? true : false)
                                  .append("</isManager>").append("</Signatory>");
                        }
                  result.append("</people>")

                  .append("<services>")
                    .append("<service>")
                        .append("<Id></Id>")
                        .append("<Code></Code>")
                        .append("<Title></Title>")
                        .append("<DiscountPercent></DiscountPercent>")
                    .append("</service>")
                  .append("</services>")

                  .append("<terminalRequest>")
                        .append("<ClientTrackingCode>").append(registerRequest.getRequestNumber()).append("</ClientTrackingCode>")
                        .append("<ConnectionType_Code>").append(terminalModel).append("</ConnectionType_Code>")
                        .append("<TerminalCount>1</TerminalCount>")
                        .append("<TrackingCode>")
                            .append(registerRequest.getPspTrackingCode() == null ? "" : registerRequest.getPspTrackingCode() )
                        .append("</TrackingCode>")
                  .append("</terminalRequest>");
            return result.toString();
        } catch (Exception e)
        {
            logger.error("There is an error in method [makeIKTagContent] ", e);
            e.printStackTrace();
            throw e;
        }
    }

    private String makeTPTagContent(RegisterRequest registerRequest) throws Exception
    {
        try
        {
            StringBuilder result = new StringBuilder();
            List<PSPClient> pspClients = registerRequest.getPspClients();
            PSPClient pspClient = null;
            PSPClient pspClientCEO = null;
            SysPosition sysPosition = null;

            CompanySetting companySetting = findCompanySettingByCompanyId(registerRequest.getCompanyId());
            BaseState baseState = findBaseStateById(registerRequest.getPspShop().getStateId());
            BaseCity baseCity = findBaseCityById(registerRequest.getPspShop().getCityId());
            BaseGuild baseGuild = findBaseGuildById(registerRequest.getPspShop().getGuildId());
            SysBranch sysBranch = findSysBranchById(registerRequest.getPspAccount().getBranchId());

            for (PSPClient item : pspClients)
            {
                if (item.getClientKind().equalsIgnoreCase("O"))
                {
                    pspClient = item;
                }

                if (item.getPositionId() != null)
                {
                    if (item.getPositionId() != null && item.getPositionId().equals(PositionEnum.CEO.getValue()))
                    {
                        pspClientCEO = item;
                        sysPosition = findSysPositionById(item.getPositionId());
                    }
                }
            }

            if (pspClient != null)
            {
                String ceoName = pspClientCEO != null ? pspClientCEO.getFirstName() : "";
                boolean isIndividual = pspClient.getClientType().equalsIgnoreCase("I");
                String accountType = registerRequest.getPspAccount().getAccountType().equalsIgnoreCase("S") ? "5" : "2";
                String terminalModel = "";

                if (registerRequest.getPspipg().getTerminalDetailId() != null)
                {
                    terminalModel = companySettingTerminalDao.findConnectionCode(registerRequest.getCompanyId(),
                                                                                 registerRequest.getTerminalId(),
                                                                                 registerRequest.getPspipg().getTerminalDetailId());
                }

                result.append("<request>")
                      //Start of Acceptor
                      .append("<CustomerType>").append(isIndividual ? "1" : "2").append("</CustomerType>")
                      .append("<cocu_sex_code>")
                      .append(!isIndividual || pspClient.getSex().equalsIgnoreCase("m") ? "1" : "2")
                      .append("</cocu_sex_code>").append("<cocu_economic_national_code>")
                      .append(!pspClient.getCountryNameAbbr().equalsIgnoreCase("IR") ? pspClient.getForeignCode() :
                              pspClient.getNationalCode()).append("</cocu_economic_national_code>")
                      .append("<cocu_name>").append(pspClient.getFirstName()).append("</cocu_name>")
                      .append("<cocu_family>").append(isIndividual ? pspClient.getLastName() : pspClient.getFirstName())
                      .append("</cocu_family>").append("<cocu_ConcatNAME>")
                      .append(pspClient.getFirstName() + " " + pspClient.getLastName()).append("</cocu_ConcatNAME>")
                      .append("<cocu_name_eng>").append(pspClient.getFirstNameEn()).append("</cocu_name_eng>")
                      .append("<cocu_family_eng>")
                      .append(isIndividual ? pspClient.getLastNameEn() : pspClient.getFirstNameEn())
                      .append("</cocu_family_eng>").append("<cocu_father_name>")
                      .append(isIndividual ? pspClient.getFatherName() : ceoName).append("</cocu_father_name>")
                      .append("<cocu_birth_date>")
                      .append(StringUtils.isNotEmpty(pspClient.getBirthDate()) ?
                              pspClient.getBirthDate().replace("/", "") : "0")
                      .append("</cocu_birth_date>").append("<cocu_id_card_no>")
                      .append(!pspClient.getCountryNameAbbr().equalsIgnoreCase("IR") ? pspClient.getPassportNumber() :
                              pspClient.getRegNumber()).append("</cocu_id_card_no>").append("<cocu_issue_code>")
                      .append(isIndividual ?
                              StringUtils.isNotEmpty(pspClient.getIssuanceDate()) ?
                              pspClient.getIssuanceDate().replace("/", "") : "0" :
                              StringUtils.isNotEmpty(pspClient.getPassportExpiryDate()) ?
                              pspClient.getPassportExpiryDate().replace("/", "") : "0")
                      .append("</cocu_issue_code>").append("<cocu_mobile>").append(pspClient.getMobilePhone())
                      .append("</cocu_mobile>").append("<country_code>").append(pspClient.getCountryCode())
                      .append("</country_code>").append("<state_code>")
                      .append(baseState != null ? baseState.getStateCode() : "").append("</state_code>")
                      .append("<coca_city_code>").append(baseCity != null ? baseCity.getCityCode() : "")
                      .append("</coca_city_code>").append("<coca_address>")
                      .append(registerRequest.getPspShop().getAddress()).append("</coca_address>")
                      .append("<coca_addresscode>").append(registerRequest.getPspShop().getAddress())
                      .append("</coca_addresscode>").append("<coca_fax>").append(pspClient.getFax()).append("</coca_fax>")
                      .append("<coca_pos_box>").append(pspClient.getPostalCode()).append("</coca_pos_box>")
                      .append("<coca_tel>").append(pspClient.getPhone()).append("</coca_tel>")
                      .append("<coca_tel2></coca_tel2>")
                      //End of Acceptor

                      //Start of Organization
                      .append("<organizationid>").append(companySetting.getOrganizationCode()).append("</organizationid>")
                      .append("<requestcode>").append(registerRequest.getRequestNumber()).append("</requestcode>")
                      .append("<termcount>1</termcount>").append("<termModel>")
                      .append(terminalModel).append("</termModel>").append("<comc_stor_name>")
                      .append(registerRequest.getPspShop().getShopName())
                      .append("</comc_stor_name>").append("<comc_stor_namel>")
                      .append(registerRequest.getPspShop().getShopNameEn())
                      .append("</comc_stor_namel>").append("<shaparaktermgroup>")
                      .append(baseGuild.getGuildCode()).append("</shaparaktermgroup>")
                      .append("<CITY_CODE>").append(baseCity.getCityCode()).append("</CITY_CODE>")
                      .append("<comc_city_part_code>")
                      .append(registerRequest.getPspShop().getMunicipalityNo())
                      .append("</comc_city_part_code>").append("<comc_cus_work_postcode>")
                      .append(registerRequest.getPspShop().getPostalCode())
                      .append("</comc_cus_work_postcode>").append("<acc_statement_code>1</acc_statement_code>")
                      .append("<acc_type_code>").append(accountType).append("</acc_type_code>")
                      .append("<ban_bank_code>").append(WrapperUtil.getBankCode()).append("</ban_bank_code>")
                      .append("<ban_branch_code>").append(sysBranch.getBranchCode())
                      .append("</ban_branch_code>").append("<ban_zone_code>")
                      .append(findSupervisorBranch().getBranchCode()).append("</ban_zone_code>")
                      .append("<comc_bank_acc>")
                      .append(registerRequest.getPspAccount().getAccountNo())
                      .append("</comc_bank_acc>").append("<comc_sttlmnt_code>1</comc_sttlmnt_code>")
                      .append("<comc_bank_iban>")
                      .append(registerRequest.getPspAccount().getIbanNo())
                      .append("</comc_bank_iban>")
                      //End of Organization

                      //Start of Person
                      .append("<sign_first_name>")
                      .append(isIndividual ? "" : (pspClientCEO != null ? pspClientCEO.getFirstName() : ""))
                      .append("</sign_first_name>").append("<sign_last_name>")
                      .append(isIndividual ? "" : (pspClientCEO != null ? pspClientCEO.getLastName() : ""))
                      .append("</sign_last_name>")
                      .append("<sign_national_code>")
                      .append(isIndividual ? "" : (pspClientCEO != null ? pspClientCEO.getNationalCode() : ""))
                      .append("</sign_national_code>").append("<sign_position>")
                      .append(isIndividual ? "" : (pspClientCEO != null ? sysPosition.getName() : ""))
                      .append("</sign_position>")
                      .append("<SignBirthDate>")
                      .append(isIndividual ? "0" : (pspClientCEO != null ?
                                                    StringUtils.isNotEmpty(pspClientCEO.getBirthDate()) ?
                                                    pspClientCEO.getBirthDate().replace("/", "") : "0" :
                                                    "0"))
                      .append("</SignBirthDate>")
                      //End of Person
                      .append("</request>");
            }
            return result.toString();
        } catch (Exception e)
        {
            logger.error("There is an error in method [makeTPTagContent] ", e);
            e.printStackTrace();
            throw e;
        }
    }

}
