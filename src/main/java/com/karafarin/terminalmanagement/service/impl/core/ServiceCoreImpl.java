package com.karafarin.terminalmanagement.service.impl.core;

import com.karafarin.terminalmanagement.dto.psp.PSPAccount;
import com.karafarin.terminalmanagement.dto.psp.PSPAccountOwner;
import com.karafarin.terminalmanagement.dto.psp.PSPClient;
import com.karafarin.terminalmanagement.message.enums.ResponseOwnerEnum;
import com.karafarin.terminalmanagement.model.core.ChannelRequest;
import com.karafarin.terminalmanagement.model.core.ChannelResponse;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import com.karafarin.terminalmanagement.dao.impl.sys.SysBranchDaoImpl;
import com.karafarin.terminalmanagement.dto.sys.SysBranch;
import com.karafarin.terminalmanagement.message.Response;
import com.karafarin.terminalmanagement.message.enums.ResponseStatusEnum;
import com.karafarin.terminalmanagement.model.Messages;
import com.karafarin.terminalmanagement.service.intf.core.ServiceCore;
import com.karafarin.terminalmanagement.util.ChannelUtil;
import com.karafarin.terminalmanagement.util.UserUtil;
import com.karafarin.terminalmanagement.util.date.DateConverterUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.w3c.dom.*;

import java.sql.SQLException;
import java.util.*;

@Service
public class ServiceCoreImpl implements ServiceCore
{
    @Autowired
    private SysBranchDaoImpl sysBranchDao;
    @Autowired
    private Messages messages;
    @Autowired
    private ChannelUtil channelUtil;

    private static final Logger logger = LogManager.getLogger(ServiceCoreImpl.class);

    @Override
    public Response getClientInfo(String clientNumber)
    {
        ChannelRequest request = new ChannelRequest();
        Response result = new Response();

        try
        {
            request.setCmd("getClientInfo");
            request.setAppName("RYMMS");
            request.setUserId(String.valueOf(UserUtil.getCurrentUser().getUserId()));
            request.setMsgId("mms_msg_cgi" + UserUtil.getCurrentUser().getUserId() + "_" +
                    DateConverterUtil.getAsStringWithTime(new Date(), "fa").replaceAll("[^a-zA-Z0-9]", ""));
            request.setJobId("GCIJOB");
            request.setBranchCode(String.valueOf(UserUtil.getCurrentUser().getBranchId()));
            request.setContent("<clientNo>" + clientNumber + "</clientNo>" +
                    "<clientModuleList><clientModule>rb</clientModule></clientModuleList>");

            PSPClient client = new PSPClient();
            client = resetClient(client);
            ChannelResponse response = channelUtil.sendRequest(request);

            if (response.getErrorNo() == 0)
            {

                Node content = response.getContent();

                client = makeClientObject(content);
                client.setClientNumber(clientNumber);
                result.setData(client);
                String validationOfClient = checkValidationOfClient(client);

             if (client.getErrorCode().equalsIgnoreCase("000000"))
                {
                    if (StringUtils.isNotEmpty(validationOfClient))
                    {
                        client.setErrorCode("002320");
                        client.setErrorDesc(validationOfClient);
                        result.setDescription(validationOfClient);
                    }

                    result.setStatus(ResponseStatusEnum.SUCCESS);
                    result.setDescription(response.getfDesc());
                    logger.info("Successful result for [getClientInfo] via clientNumber [" + clientNumber + "]");
                }
            }
            else
            {
                result.setStatus(ResponseStatusEnum.ERROR);
                if (response.getErrorNo() == 303002)
                {
                    result.setDescription(messages.get("core.error.3"));
                }
                else
                {
                    result.setDescription(response.getErrorMessage());
                }
                client.setErrorCode(String.valueOf(response.getErrorNo()));
                result.setData(client);
            }
        }
        catch (Exception e)
        {
            result.setStatus(ResponseStatusEnum.ERROR);
            result.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.CORE));
            result.setData(null);
            logger.info("There is an unknown error for [getClientInfo] via clientNumber [" +
                    clientNumber + "] with " + "exception in parsing response XML!", e);
        }

        return result;
    }

    private PSPClient makeClientObject(Node content)
    {
        PSPClient client = new PSPClient();

        Node accountInfoListResp = content.getChildNodes().item(0);
        int childNodeCount = accountInfoListResp.getChildNodes().getLength();
        String errorDesc = "";
        for (int i = 0; i < childNodeCount; i++)
        {
            String nodeName = accountInfoListResp.getChildNodes().item(i).getNodeName();
            String nodeValue = accountInfoListResp.getChildNodes().item(i).getTextContent();

            switch (nodeName) {
                case "clientType":
                    client.setClientType(nodeValue != null && nodeValue.equalsIgnoreCase("1") ? "I" : "L");
                    break;
                case "givenName":
                    client.setFirstName(client.getClientType().equalsIgnoreCase("I") ?
                            nodeValue : null);
                    break;
                case "enGivenName":
                    client.setFirstNameEn(nodeValue);
                    break;
                case "surName":
                    client.setLastName(nodeValue);
                    break;
                case "enSurName":
                    client.setLastNameEn(nodeValue);
                    break;
                case "fatherName":
                    client.setFatherName(nodeValue);
                    break;
                case "enFatherName":
                    client.setFatherNameEn(nodeValue);
                    break;
                case "countryNameAbbr":
                    client.setCountryNameAbbr(nodeValue);
                    break;
                case "countryCode":
                    client.setCountryCode(nodeValue);
                    break;
                case "countryDesc":
                    client.setCountryDesc(nodeValue);
                    break;
                case "sex":
                    client.setSex(nodeValue);
                    break;
                case "isDead":
                    client.setLifeStatus(nodeValue);
                    break;
                case "foreignCode":
                    client.setForeignCode(nodeValue);
                    break;
                case "passportNumber":
                    client.setPassportNumber(nodeValue);
                    break;
                case "passportExpiryDate":
                    client.setPassportExpiryDate(nodeValue);
                    break;
                case "nationalCode":
                    client.setNationalCode(nodeValue);
                    break;
                case "regNumber":
                    client.setRegNumber(nodeValue);
                    break;
                case "birthDate":
                    client.setBirthDate(nodeValue);
                    break;
                case "dateOfIssuance":
                    client.setIssuanceDate(nodeValue);
                    break;
                case "issuePlaceCode":
                    client.setIssuancePlaceCode(nodeValue);
                    break;
                case "issuePlaceDesc":
                    client.setIssuancePlaceDesc(nodeValue);
                    break;
                case "clientName":
                    client.setCompanyName(client.getClientType().equalsIgnoreCase("L") ?
                            nodeValue : null);
                    break;
                case "enCompanyName":
                    client.setCompanyNameEn(nodeValue);
                    break;
                case "commerceCode":
                    client.setCommerceCode(nodeValue);
                    break;
                case "errorCode":
                    client.setErrorCode(nodeValue);
                    break;
                case "errorDesc":
                    errorDesc = nodeValue;
                    break;
                case "clientContactList":
                    Node clientContactList = accountInfoListResp.getChildNodes().item(i);
                    int childCount = clientContactList.getChildNodes().item(0).getChildNodes().getLength();
                    for (int j = 0; j < childCount; j++)
                    {
                        String name = clientContactList.getChildNodes().item(0).getChildNodes().item(j).getNodeName();
                        String value = clientContactList.getChildNodes().item(0).getChildNodes().item(j).getTextContent();
                        switch (name)
                        {
                            case "address":
                                client.setAddress(value);
                                break;
                            case "contactId":
                                client.setPhone(value);
                                break;
                            case "postalCode":
                                client.setPostalCode(value);
                                break;
                        }
                    }
                    break;
            }
        }

        if (!client.getErrorCode().equalsIgnoreCase("000000"))
        {
            client.setErrorDesc(errorDesc);
        }
        else
        {
            client.setErrorDesc("");
        }
        return client;
    }

    @Override
    public Response getAccountInfo(String clientNumber, String clientType)
    {
        ChannelRequest request = new ChannelRequest();
        Response result = new Response();

        try
        {
            request.setCmd("getAcctInfo");
            request.setAppName("RYMMS");
            request.setUserId(String.valueOf(UserUtil.getCurrentUser().getUserId()));
            request.setMsgId("mms_msg_gai" + UserUtil.getCurrentUser().getUserId() + "_" +
                    DateConverterUtil.getAsStringWithTime(new Date(), "fa").replaceAll("[^a-zA-Z0-9]", ""));
            request.setJobId("GCIJOB");
            request.setBranchCode(String.valueOf(UserUtil.getCurrentUser().getBranchId()));
            request.setContent("<clientNo>" + clientNumber + "</clientNo>");

            ChannelResponse response = channelUtil.sendRequest(request);
            if (response.getErrorNo() == 0)
            {
                String errorCode = null;
                String errorDesc = null;

                errorCode = response.getContent().getChildNodes().item(1).getTextContent();
                errorDesc = response.getContent().getChildNodes().item(2).getTextContent();

                if (errorCode.equalsIgnoreCase("000000"))
                {
                    Node content = response.getContent();
                    int accountCount = content.getChildNodes().item(0).getChildNodes().getLength();
                    List<PSPAccount> accounts = new ArrayList<PSPAccount>();

                    for (int i = 0; i < accountCount; i++)
                    {
                        PSPAccount pspAccount =
                                makeAccountObject(content.getChildNodes().item(0).getChildNodes().item(i));

                        if (StringUtils.isNotEmpty(pspAccount.getAccountStatus()) &&
                            pspAccount.getAccountStatus().equalsIgnoreCase("A") &&
                            StringUtils.isNotEmpty(pspAccount.getAccountType()) &&
                           !pspAccount.getAccountType().equalsIgnoreCase("T") &&
                            StringUtils.isNotEmpty(pspAccount.getCcy()) &&
                            pspAccount.getCcy().equalsIgnoreCase("IRR") &&
                            StringUtils.isNotEmpty(pspAccount.getInvalidAccount()) &&
                            pspAccount.getInvalidAccount().equalsIgnoreCase("N"))
                        {
                            if (clientType.equalsIgnoreCase("I") &&
                                StringUtils.isNotEmpty(pspAccount.getOwnershipType()) &&
                                !pspAccount.getOwnershipType().equalsIgnoreCase("AS"))
                            {
                                accounts.add(pspAccount);
                            }
                            else
                            {
                                accounts.add(pspAccount);
                            }
                        }
                    }

                    result.setStatus(ResponseStatusEnum.SUCCESS);
                    result.setDescription(response.getfDesc());
                    result.setData(accounts);
                    logger.info("Successful result for [getAccountInfo] via clientNumber [" + clientNumber +
                                "] and count of account [" + accountCount + "]");
                }
                else
                {
                    result.setStatus(ResponseStatusEnum.ERROR);
                    result.setDescription(errorDesc);
                    result.setData(null);
                }
            }
            else
            {
                result.setStatus(ResponseStatusEnum.ERROR);
                result.setDescription(response.getErrorMessage());
                result.setData(null);
            }
        } catch (Exception e)
        {
            result.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.CORE));
            result.setStatus(ResponseStatusEnum.ERROR);
            e.printStackTrace();
            logger.error("There is an unknown error while getting account info for [getAccountInfo] via clientNumber [" +
                    clientNumber + "] " + e);
        }

        return result;
    }

    private PSPAccount makeAccountObject(Node content) throws SQLException
    {
        PSPAccount account = new PSPAccount();

        int childNodeCount = content.getChildNodes().getLength();
        String branchCode = null;
        for (int i = 0; i < childNodeCount; i++)
        {
            String nodeName = content.getChildNodes().item(i).getNodeName();
            String nodeValue = content.getChildNodes().item(i).getTextContent();

            switch (nodeName)
            {
                case "branchCode":
                    branchCode = nodeValue;
                    break;
                case "accountNo":
                    account.setAccountNo(nodeValue);
                    break;
                case "accountDesc":
                    account.setAccountDesc(nodeValue);
                    break;
                case "depositType":
                    account.setAccountType(nodeValue);
                    break;
                case "iban":
                    account.setIbanNo(nodeValue);
                    break;
                case "accountStatus":
                    account.setAccountStatus(nodeValue);
                    break;
                case "ccy":
                    account.setCcy(nodeValue);
                    break;
                case "invalidAccount":
                    account.setInvalidAccount(nodeValue);
                    break;
                case "ownershipType":
                    account.setOwnershipType(nodeValue);
                    break;
                case "mainClient":
                    account.setMainClient(nodeValue);
                    break;
            }
        }

        if (StringUtils.isNoneEmpty(branchCode))
        {
            SysBranch branch = sysBranchDao.findByBranchCode(branchCode);
            account.setBranchCode(branchCode);
            account.setBranchId(branch != null ? branch.getBranchId() : null);
        }

        return account;
    }

    @Override
    public Response getAccountOwnersInfo(String accountNumber)
    {
        ChannelRequest request = new ChannelRequest();
        Response result = new Response();

        try
        {
            request.setCmd("getAcctOwnersInfo");
            request.setAppName("RYMMS");
            request.setUserId(String.valueOf(UserUtil.getCurrentUser().getUserId()));
            request.setMsgId("mms_msg_gai" + UserUtil.getCurrentUser().getUserId() + "_" +
                    DateConverterUtil.getAsStringWithTime(new Date(), "fa").replaceAll("[^a-zA-Z0-9]", ""));
            request.setJobId("GCIJOB");
            request.setBranchCode(String.valueOf(UserUtil.getCurrentUser().getBranchId()));
            request.setContent("<accountNo>" + accountNumber + "</accountNo>");

            ChannelResponse response = channelUtil.sendRequest(request);
            if (response.getErrorNo() == 0)
            {
                String errorCode = null;
                String errorDesc = null;

                errorCode = response.getContent().getChildNodes().item(1).getTextContent();
                errorDesc = response.getContent().getChildNodes().item(2).getTextContent();

                if (errorCode.equalsIgnoreCase("000000"))
                {
                    Node content = response.getContent();
                    int accountOwnerCount = content.getChildNodes().item(0).getChildNodes().getLength();
                    List<PSPAccountOwner> accountOwners = new ArrayList<PSPAccountOwner>();

                    for (int i = 0; i < accountOwnerCount; i++)
                    {
                        accountOwners.add(makeAccountOwnersObject(content.getChildNodes().item(0).getChildNodes().item(i)));
                    }

                    result.setStatus(ResponseStatusEnum.SUCCESS);
                    result.setDescription(response.getfDesc());
                    result.setData(accountOwners);
                    logger.info("Successful result for [getAccountOwnersInfo] via accountNumber [" + accountNumber +
                                "] and count of accountOwner [" + accountOwnerCount + "]");
                }
                else
                {
                    result.setStatus(ResponseStatusEnum.ERROR);
                    result.setDescription(errorDesc);
                    result.setData(null);
                }
            }
            else
            {
                result.setStatus(ResponseStatusEnum.ERROR);
                result.setDescription(response.getErrorMessage());
                result.setData(null);
            }
        } catch (Exception e)
        {
            result.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.CORE));
            result.setStatus(ResponseStatusEnum.ERROR);
            e.printStackTrace();
            logger.error("There is an unknown error while getting account info for  [getAccountOwnersInfo] via clientNumber [" +
                    accountNumber + "] " + e);
        }

        return result;
    }

    private PSPAccountOwner makeAccountOwnersObject(Node content)
    {
        PSPAccountOwner accountOwner = new PSPAccountOwner();

        int childNodeCount = content.getChildNodes().getLength();
        for (int i = 0; i < childNodeCount; i++)
        {
            String nodeName = content.getChildNodes().item(i).getNodeName();
            String nodeValue = content.getChildNodes().item(i).getTextContent();

            switch (nodeName)
            {
                case "clientNo":
                    accountOwner.setClientNumber(nodeValue);
                    break;
                case "clientName":
                    accountOwner.setClientName(nodeValue);
                    break;
                case "clientType":
                    accountOwner.setClientType(nodeValue != null
                            && nodeValue.equalsIgnoreCase("1") ? "I" : "L");
                    break;
                case "isActive":
                    accountOwner.setActive(nodeValue != null
                            && nodeValue.equalsIgnoreCase("Y") ? true : false);
                    break;
            }
        }

        return accountOwner;
    }

    private String checkValidationOfClient(PSPClient client)
    {
        StringBuilder errorValidation = new StringBuilder();
        if (client.getClientType().equalsIgnoreCase("I") &&
            client.getCountryNameAbbr().equalsIgnoreCase("IR") &&
            (StringUtils.isEmpty(client.getSex()) || StringUtils.isEmpty(client.getLifeStatus()) ||
             StringUtils.isEmpty(client.getFirstName()) || StringUtils.isEmpty(client.getFirstNameEn()) ||
             StringUtils.isEmpty(client.getLastName()) || StringUtils.isEmpty(client.getLastNameEn()) ||
             StringUtils.isEmpty(client.getFatherName()) || StringUtils.isEmpty(client.getFatherNameEn()) ||
             StringUtils.isEmpty(client.getNationalCode()) || StringUtils.isEmpty(client.getRegNumber()) ||
             StringUtils.isEmpty(client.getBirthDate()) || StringUtils.isEmpty(client.getCountryNameAbbr()) ||
             StringUtils.isEmpty(client.getCountryCode()) || StringUtils.isEmpty(client.getCountryDesc()) ||
             StringUtils.isEmpty(client.getIssuanceDate()) || StringUtils.isEmpty(client.getIssuancePlaceCode()) ||
             StringUtils.isEmpty(client.getIssuancePlaceDesc())))
        {
            if (StringUtils.isNotEmpty(errorValidation.toString()))
            {
                errorValidation.append(System.lineSeparator());
            }
            errorValidation.append(messages.get("psp.error.8"));
        }

        if (client.getClientType().equalsIgnoreCase("I") &&
            !client.getCountryNameAbbr().equalsIgnoreCase("IR") &&
            (StringUtils.isEmpty(client.getSex()) || StringUtils.isEmpty(client.getLifeStatus()) ||
             StringUtils.isEmpty(client.getFirstName()) || StringUtils.isEmpty(client.getFirstNameEn()) ||
             StringUtils.isEmpty(client.getLastName()) || StringUtils.isEmpty(client.getLastNameEn()) ||
             StringUtils.isEmpty(client.getFatherName()) || StringUtils.isEmpty(client.getFatherNameEn()) ||
             StringUtils.isEmpty(client.getForeignCode()) || StringUtils.isEmpty(client.getPassportNumber()) ||
             StringUtils.isEmpty(client.getBirthDate()) || StringUtils.isEmpty(client.getCountryNameAbbr()) ||
             StringUtils.isEmpty(client.getCountryCode()) || StringUtils.isEmpty(client.getCountryDesc()) ||
             StringUtils.isEmpty(client.getIssuanceDate()) || StringUtils.isEmpty(client.getIssuancePlaceCode()) ||
             StringUtils.isEmpty(client.getIssuancePlaceDesc()) || StringUtils.isEmpty(client.getPassportExpiryDate())))
        {
            if (StringUtils.isNotEmpty(errorValidation.toString()))
            {
                errorValidation.append(System.lineSeparator());
            }
            errorValidation.append(messages.get("psp.error.8"));
        }

        if (client.getClientType().equalsIgnoreCase("L") &&
            (StringUtils.isEmpty(client.getCompanyName()) || StringUtils.isEmpty(client.getCountryNameAbbr()) ||
             StringUtils.isEmpty(client.getCountryCode()) || StringUtils.isEmpty(client.getCountryDesc()) ||
             StringUtils.isEmpty(client.getCommerceCode()) || StringUtils.isEmpty(client.getCompanyNameEn()) ||
             StringUtils.isEmpty(client.getIssuanceDate()) || StringUtils.isEmpty(client.getIssuancePlaceCode()) ||
             StringUtils.isEmpty(client.getIssuancePlaceDesc()) || StringUtils.isEmpty(client.getNationalCode()) ||
             StringUtils.isEmpty(client.getRegNumber())))
        {
            if (StringUtils.isNotEmpty(errorValidation.toString()))
            {
                errorValidation.append(System.lineSeparator());
            }
            errorValidation.append(messages.get("psp.error.8"));
        }

        if (client.getClientType().equalsIgnoreCase("I") &&
            client.getLifeStatus().equalsIgnoreCase("Y"))
        {
            if (StringUtils.isNotEmpty(errorValidation.toString()))
            {
                errorValidation.append(System.lineSeparator());
            }
            errorValidation.append(messages.get("psp.error.19"));
        }

        if (client.getClientType().equalsIgnoreCase("I") &&
            !client.getCountryNameAbbr().equalsIgnoreCase("IR") &&
            StringUtils.isNotEmpty(client.getForeignCode()) &&
            !UserUtil.isForeignCodeValid(client.getForeignCode()))
        {
            if (StringUtils.isNotEmpty(errorValidation.toString()))
            {
                errorValidation.append(System.lineSeparator());
            }
            errorValidation.append(messages.get("psp.error.21"));
        }

        if (StringUtils.isNotEmpty(client.getNationalCode()) &&
            client.getClientType().equalsIgnoreCase("I") &&
            !UserUtil.validateMelliCode(client.getNationalCode()))
        {
            if (StringUtils.isNotEmpty(errorValidation.toString()))
            {
                errorValidation.append(System.lineSeparator());
            }
            errorValidation.append(messages.get("psp.error.9"));
        }

        if (StringUtils.isNotEmpty(client.getNationalCode()) &&
            client.getClientType().equalsIgnoreCase("L") &&
            !UserUtil.checkLegalNationalCode(client.getNationalCode()))
        {
            if (StringUtils.isNotEmpty(errorValidation.toString()))
            {
                errorValidation.append(System.lineSeparator());
            }
            errorValidation.append(messages.get("psp.error.20"));
        }

        if (client.getClientType().equalsIgnoreCase("L") &&
            StringUtils.isNotEmpty(client.getRegNumber()) &&
            !client.getRegNumber().matches("[0-9]+"))
        {
            if (StringUtils.isNotEmpty(errorValidation.toString()))
            {
                errorValidation.append(System.lineSeparator());
            }
            errorValidation.append(messages.get("psp.error.11"));
        }

        if (StringUtils.isNotEmpty(client.getBirthDate()) &&
            !DateConverterUtil.isValidDateFormat(client.getBirthDate()))
        {
            if (StringUtils.isNotEmpty(errorValidation.toString()))
            {
                errorValidation.append(System.lineSeparator());
            }
            errorValidation.append(messages.get("psp.error.17"));
        }

        if (StringUtils.isNotEmpty(client.getIssuanceDate()) &&
            !DateConverterUtil.isValidDateFormat(client.getIssuanceDate()))
        {
            if (StringUtils.isNotEmpty(errorValidation.toString()))
            {
                errorValidation.append(System.lineSeparator());
            }
            errorValidation.append(messages.get("psp.error.17"));
        }

        if (client.getClientType().equalsIgnoreCase("I") &&
            !client.getCountryNameAbbr().equalsIgnoreCase("IR") &&
            StringUtils.isNotEmpty(client.getPassportExpiryDate()) &&
            !DateConverterUtil.isValidDateFormat(client.getPassportExpiryDate()))
        {
            if (StringUtils.isNotEmpty(errorValidation.toString()))
            {
                errorValidation.append(System.lineSeparator());
            }
            errorValidation.append(messages.get("psp.error.17"));
        }

        return errorValidation.toString();
    }

    private PSPClient resetClient(PSPClient client)
    {
        client.setBirthDate("");
        client.setErrorDesc("");
        client.setErrorCode("");
        client.setClientId(-1L);
        client.setPspRequestId(-1L);
        client.setPositionId(-1L);
        client.setClientNumber("");
        client.setClientType("");
        client.setCommerceCode("");
        client.setCompanyName("");
        client.setCompanyNameEn("");
        client.setCountryCode("");
        client.setCountryDesc("");
        client.setCountryNameAbbr("");
        client.setFatherName("");
        client.setFatherNameEn("");
        client.setFirstName("");
        client.setFirstNameEn("");
        client.setForeignCode("");
        client.setIssuanceDate("");
        client.setSex("");
        client.setLifeStatus("");
        client.setLastName("");
        client.setLastNameEn("");
        client.setClientKind("");
        client.setAddress("");
        client.setPostalCode("");
        client.setFax("");
        client.setPhone("");
        client.setMobilePhone("");
        client.setIssuancePlaceDesc("");
        client.setIssuancePlaceCode("");
        client.setPassportExpiryDate("");
        client.setPassportNumber("");
        client.setRegNumber("");
        client.setNationalCode("");

        return client;
    }
}
