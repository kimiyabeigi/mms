package com.karafarin.terminalmanagement.service.impl.auth;

import com.karafarin.terminalmanagement.dao.impl.auth.FormDaoImpl;
import com.karafarin.terminalmanagement.dto.auth.Form;
import com.karafarin.terminalmanagement.message.Response;
import com.karafarin.terminalmanagement.message.enums.ResponseOwnerEnum;
import com.karafarin.terminalmanagement.message.enums.ResponseStatusEnum;
import com.karafarin.terminalmanagement.model.Messages;
import com.karafarin.terminalmanagement.service.intf.auth.ServiceForm;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service
public class ServiceFormImpl implements ServiceForm
{
    @Autowired
    private FormDaoImpl formDao;
    @Autowired
    private Messages messages;

    private static final Logger logger = LogManager.getLogger(ServiceFormImpl.class);

    @Override
    public Response add(Form entity)
    {
        return null;
    }

    @Override
    public Response findById(Long id)
    {
        Response response = new Response();
        try
        {
            logger.info("Start method [findById] for [Form] via id [" + id + "]");

            Form form = formDao.findById(id);
            response.setData(form);
            if (form.getFormId() != null)
            {
                response.setStatus(ResponseStatusEnum.SUCCESS);
                response.setDescription(messages.get("baseEntity.find.success"));
            }
            else
            {
                response.setStatus(ResponseStatusEnum.ERROR);
                response.setDescription(messages.get("baseEntity.find.error"));
            }

            logger.info("End successfully method [findById] for [Form] via id [" + id + "]");
        } catch (Exception e)
        {
            logger.error("There is an error in method [findById] for [Form] via id [" + id + "]", e);

            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }

    @Override
    public Response findAll()
    {
        Response response = new Response();
        try
        {
            logger.info("Start method [findAll] for [Form]");

            response.setData(formDao.findAll());
            response.setStatus(ResponseStatusEnum.SUCCESS);
            response.setDescription(messages.get("baseEntity.find.success"));

            logger.info("End successfully method [findAll] for [Form]");
        } catch (Exception e)
        {
            logger.error("There is an error in method [findAll] for [Form]", e);

            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }

    @Override
    public Response delete(Long id)
    {
        return null;
    }

    @Override
    public Response update(Form entity)
    {
        return null;
    }
}
