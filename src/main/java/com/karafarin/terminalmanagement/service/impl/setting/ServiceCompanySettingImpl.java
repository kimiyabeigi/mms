package com.karafarin.terminalmanagement.service.impl.setting;

import com.karafarin.terminalmanagement.dao.impl.setting.CompanySettingDaoImpl;
import com.karafarin.terminalmanagement.dao.impl.setting.CompanySettingDetailDaoImpl;
import com.karafarin.terminalmanagement.dto.setting.CompanySetting;
import com.karafarin.terminalmanagement.dto.setting.CompanySettingDetail;
import com.karafarin.terminalmanagement.dto.setting.CompanySettingView;
import com.karafarin.terminalmanagement.message.Response;
import com.karafarin.terminalmanagement.message.enums.ResponseOwnerEnum;
import com.karafarin.terminalmanagement.message.enums.ResponseStatusEnum;
import com.karafarin.terminalmanagement.model.Messages;
import com.karafarin.terminalmanagement.service.intf.setting.ServiceCompanySetting;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ServiceCompanySettingImpl implements ServiceCompanySetting
{
    @Autowired
    private Messages messages;
    @Autowired
    private CompanySettingDaoImpl companyDao;
    @Autowired
    private CompanySettingDetailDaoImpl companySettingDetailDao;

    private static final Logger logger = LogManager.getLogger(ServiceCompanySettingImpl.class);

    @Override
    public Response add(CompanySetting entity)
    {
        Response response = new Response();
        try
        {
            logger.info("Start method [add] for [CompanySetting]");
            
            Long newCompanySettingId = companyDao.add(entity);
            entity.setCompanySettingId(newCompanySettingId);

            //Starting of adding to tbl_CompanySettingTerminal
            List<CompanySettingDetail> companySettingDetails = entity.getCompanySettingDetails();
            if (companySettingDetails != null )
            {
                for (CompanySettingDetail companySettingDetail : companySettingDetails)
                {
                    companySettingDetail.setCompanySettingId(newCompanySettingId);
                    if (companySettingDetail.getCompanySettingDetailId() == null)
                    {
                        logger.info("Starting of adding details for [CompanySettingTerminal] via CompanySettingId [" +
                                newCompanySettingId + "]");

                        Long newCompanySettingDetailId = companySettingDetailDao.add(companySettingDetail);

                        logger.info("End successfully of adding details for [CompanySettingTerminal] via CompanySettingId [" +
                                newCompanySettingId + "] with new id [" + newCompanySettingDetailId + "]");
                    }
                }
            }
            //Ending of adding to tbl_CompanySettingTerminal

            response.setStatus(ResponseStatusEnum.SUCCESS);
            response.setData(entity);
            response.setDescription(messages.get("baseEntity.create.success"));

            logger.info("End successfully method [add] for [CompanySetting] with new id [" +
                    newCompanySettingId + "]");
        } catch (Exception e)
        {
            logger.error("There is an error in method [add] for [CompanySetting]", e);
            
            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }

    @Override
    public Response findById(Long id)
    {
        Response response = new Response();
        try
        {
            logger.info("Start method [findById] for [CompanySetting] via id [" + id + "]");
            
            CompanySetting companySetting = companyDao.findById(id);
            response.setData(companySetting);
            if (companySetting.getCompanySettingId() != null)
            {
                response.setStatus(ResponseStatusEnum.SUCCESS);
                response.setDescription(messages.get("baseEntity.find.success"));
            }
            else
            {
                response.setStatus(ResponseStatusEnum.ERROR);
                response.setDescription(messages.get("baseEntity.find.error"));
            }

            logger.info("End successfully method [findById] for [CompanySetting] via id [" + id + "]");            
        } catch (Exception e)
        {
            logger.error("There is an error in method [findById] for [CompanySetting] via id [" + id + "]", e);
            
            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }

    @Override
    public Response findAll()
    {
        Response response = new Response();
        try
        {
            logger.info("Start method [findAll] for [CompanySetting]");
            
            response.setData(companyDao.findAll());
            response.setStatus(ResponseStatusEnum.SUCCESS);
            response.setDescription(messages.get("baseEntity.find.success"));

            logger.info("End successfully method [findAll] for [CompanySetting]");            

        } catch (Exception e)
        {
            logger.error("There is an error in method [findAll] for [CompanySetting]", e);
            
            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }

    @Override
    public Response delete(Long id)
    {
        return null;
    }

    @Override
    public Response update(CompanySetting entity)
    {
        Response response = new Response();
        response.setData(entity);
        try
        {
            logger.info("Start method [update] for [CompanySetting] via id [" + entity.getCompanySettingId() + "]");
            
            boolean updateResult = companyDao.update(entity);
            if (updateResult)
            {
                //Starting of editing to tbl_CompanySettingDetail
                List<CompanySettingDetail> companySettingDetails = entity.getCompanySettingDetails();
                if (companySettingDetails != null)
                {
                    for (CompanySettingDetail companySettingDetail : companySettingDetails)
                    {
                        if (companySettingDetail.getCompanySettingDetailId() != null)
                        {
                            logger.info("Starting of editing details for [CompanySettingTerminal] via CompanySettingId [" +
                                    entity.getCompanySettingId() + "] and id [" +
                                    companySettingDetail.getCompanySettingDetailId() + "]");

                            companySettingDetailDao.update(companySettingDetail);

                            logger.info("Ending successfully of editing details for [CompanySettingTerminal] via CompanySettingId [" +
                                    entity.getCompanySettingId() + "] and id [" +
                                    companySettingDetail.getCompanySettingDetailId() + "]");
                        } else if (companySettingDetail.getCompanySettingDetailId() == null)
                        {
                            logger.info("Starting of adding details for [CompanySettingTerminal] via CompanySettingId [" +
                                    entity.getCompanySettingId() + "]");

                            Long newCompanySettingDetailId = companySettingDetailDao.add(companySettingDetail);

                            logger.info("End successfully of adding details for [CompanySettingTerminal] via CompanySettingId [" +
                                    entity.getCompanySettingId() + "] with new id [" + newCompanySettingDetailId + "]");
                        }
                    }
                }
                //Ending of editing to tbl_CompanySettingDetail

                response.setStatus(ResponseStatusEnum.SUCCESS);
                response.setDescription(messages.get("baseEntity.update.success"));
            }
            else
            {
                response.setStatus(ResponseStatusEnum.ERROR);
                response.setDescription(messages.get("baseEntity.update.error"));
            }

            logger.info("End successfully method [update] for [CompanySetting] via id [" +
                    entity.getCompanySettingId() + "]");
        } catch (Exception e)
        {
            logger.error("There is an error in method [update] for [CompanySetting] via id [" +
                    entity.getCompanySettingId() + "]", e);
            
            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }

    @Override
    public Response findViewById(Long id)
    {
        Response response = new Response();
        try
        {
            logger.info("Start method [findViewById] for [CompanySetting] via id [" + id + "]");
            CompanySettingView companySettingView = companyDao.findViewById(id);
            response.setData(companySettingView);
            if (companySettingView.getCompanySettingId() != null)
            {
                response.setStatus(ResponseStatusEnum.SUCCESS);
                response.setDescription(messages.get("baseEntity.find.success"));
            }
            else
            {
                response.setStatus(ResponseStatusEnum.ERROR);
                response.setDescription(messages.get("baseEntity.find.error"));
            }

            logger.info("End successfully method [findViewById] for [CompanySetting] via id [" + id + "]");
        } catch (Exception e)
        {
            logger.error("There is an error in method [findViewById] for [CompanySetting] via id [" + id + "]", e);
            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }

    @Override
    public Response findViewAll()
    {
        Response response = new Response();
        try
        {
            logger.info("Start method [findViewAll] for [CompanySetting]");
            
            response.setData(companyDao.findViewAll());
            response.setStatus(ResponseStatusEnum.SUCCESS);
            response.setDescription(messages.get("baseEntity.find.success"));

            logger.info("End successfully method [findViewAll] for [CompanySetting]");
        } catch (Exception e)
        {
            logger.error("There is an error in method [findViewAll] for [CompanySetting]", e);
            
            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }
}
