package com.karafarin.terminalmanagement.service.impl.base;

import com.karafarin.terminalmanagement.dao.impl.base.BaseStateDaoImpl;
import com.karafarin.terminalmanagement.dto.base.BaseState;
import com.karafarin.terminalmanagement.message.Response;
import com.karafarin.terminalmanagement.message.enums.ResponseOwnerEnum;
import com.karafarin.terminalmanagement.message.enums.ResponseStatusEnum;
import com.karafarin.terminalmanagement.model.Messages;
import com.karafarin.terminalmanagement.service.intf.base.ServiceBaseState;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.sql.SQLException;

@Service
public class ServiceBaseStateImpl implements ServiceBaseState
{
    @Autowired
    private BaseStateDaoImpl baseStateDao;
    @Autowired
    private Messages messages;

    private static final Logger logger = LogManager.getLogger(ServiceBaseStateImpl.class);

    @Override
    public Response add(BaseState entity)
    {
        Response response = new Response();
        try
        {
            logger.info("Start method [add] for [BaseState]");

            Long newStateId = baseStateDao.add(entity);
            entity.setStateId(newStateId);
            response.setStatus(ResponseStatusEnum.SUCCESS);
            response.setData(entity);
            response.setDescription(messages.get("baseEntity.create.success"));

            logger.info("End successfully method [add] for [BaseState] with new id [" + newStateId + "]");
        } catch (Exception e)
        {
            logger.error("There is an error in method [add] for [BaseState]", e);

            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }

    @Override
    public Response findById(Long id)
    {
        Response response = new Response();
        try
        {
            logger.info("Start method [findById] for [BaseState] via id [" + id + "]");

            BaseState baseState = baseStateDao.findById(id);
            response.setData(baseState);
            if (baseState.getStateId() != null)
            {
                response.setStatus(ResponseStatusEnum.SUCCESS);
                response.setDescription(messages.get("baseEntity.find.success"));
            }
            else
            {
                response.setStatus(ResponseStatusEnum.ERROR);
                response.setDescription(messages.get("baseEntity.find.error"));
            }

            logger.info("End successfully method [findById] for [BaseState] via id [" + id + "]");
        } catch (Exception e)
        {
            logger.error("There is an error in method [findById] for [BaseState] via id [" + id + "]", e);

            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }

    @Override
    public Response findAll()
    {
        return null;
    }

    @Override
    public Response findAll(boolean all)
    {
        Response response = new Response();
        try
        {
            logger.info("Start method [findAll] for [BaseState]");

            response.setData(baseStateDao.findAll(all));
            response.setStatus(ResponseStatusEnum.SUCCESS);
            response.setDescription(messages.get("baseEntity.find.success"));

            logger.info("End successfully method [findAll] for [BaseState]");
        } catch (Exception e)
        {
            logger.error("There is an error in method [findAll] for [BaseState]", e);

            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }

    @Override
    public Response delete(Long id)
    {
        Response response = new Response();
        response.setData(null);
        try
        {
            logger.info("Start method [delete] for [BaseState] via id [" + id + "]");

            if (baseStateDao.delete(id))
            {
                response.setStatus(ResponseStatusEnum.SUCCESS);
                response.setDescription(messages.get("baseEntity.delete.success"));
            }
            else
            {
                response.setStatus(ResponseStatusEnum.ERROR);
                response.setDescription(messages.get("baseEntity.delete.error"));
            }

            logger.info("End successfully method [delete] for [BaseState] via id [" + id + "]");
        } catch (Exception e)
        {
            logger.error("There is an error in method [delete] for [BaseState] via id [" + id + "]", e);

            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }

    @Override
    public Response update(BaseState entity)
    {
        Response response = new Response();
        response.setData(entity);
        try
        {
            logger.info("Start method [update] for [BaseState] via id [" + entity.getStateId() + "]");

            if (baseStateDao.update(entity))
            {
                response.setStatus(ResponseStatusEnum.SUCCESS);
                response.setDescription(messages.get("baseEntity.update.success"));
            }
            else
            {
                response.setStatus(ResponseStatusEnum.ERROR);
                response.setDescription(messages.get("baseEntity.update.error"));
            }

            logger.info("End successfully method [update] for [BaseState] via id [" +
                    entity.getStateId() + "]");
        } catch (Exception e)
        {
            logger.error("There is an error in method [update] for [BaseState] via id [" +
                    entity.getStateId() + "]", e);

            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }
}
