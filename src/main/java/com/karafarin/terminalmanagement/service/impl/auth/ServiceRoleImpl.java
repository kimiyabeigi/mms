package com.karafarin.terminalmanagement.service.impl.auth;

import com.karafarin.terminalmanagement.dao.impl.auth.RoleDaoImpl;
import com.karafarin.terminalmanagement.dto.auth.Role;
import com.karafarin.terminalmanagement.message.Response;
import com.karafarin.terminalmanagement.message.enums.ResponseOwnerEnum;
import com.karafarin.terminalmanagement.message.enums.ResponseStatusEnum;
import com.karafarin.terminalmanagement.model.Messages;
import com.karafarin.terminalmanagement.service.intf.auth.ServiceRole;
import com.karafarin.terminalmanagement.util.UserUtil;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service
public class ServiceRoleImpl implements ServiceRole
{
    @Autowired
    private RoleDaoImpl roleDao;
    @Autowired
    private Messages messages;

    private static final Logger logger = LogManager.getLogger(ServiceRoleImpl.class);

    @Override
    public Response add(Role entity)
    {
        Response response = new Response();
        try
        {
            logger.info("Start method [add] for [Role]");

            Long newRoleId = roleDao.add(entity);
            entity.setRoleId(newRoleId);
            UserUtil.addRoleCache(entity);
            response.setStatus(ResponseStatusEnum.SUCCESS);
            response.setData(entity);
            response.setDescription(messages.get("baseEntity.create.success"));

            logger.info("End successfully method [add] for [Role] with new id [" + newRoleId + "]");
        } catch (Exception e)
        {
            logger.error("There is an error in method [add] for [Role]", e);

            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }

    @Override
    public Response findById(Long id)
    {
        Response response = new Response();
        try
        {
            logger.info("Start method [findById] for [Role] via id [" + id + "]");

            Role role = roleDao.findById(id);
            response.setData(role);
            if (role.getRoleId() != null)
            {
                response.setStatus(ResponseStatusEnum.SUCCESS);
                response.setDescription(messages.get("baseEntity.find.success"));
            }
            else
            {
                response.setStatus(ResponseStatusEnum.ERROR);
                response.setDescription(messages.get("baseEntity.find.error"));
            }

            logger.info("End successfully method [findById] for [Role] via id [" + id + "]");
        } catch (Exception e)
        {
            logger.error("There is an error in method [findById] for [Role] via id [" + id + "]", e);

            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }

    @Override
    public Response findAll()
    {
        Response response = new Response();
        try
        {
            logger.info("Start method [findAll] for [Role]");

            response.setData(roleDao.findAll());
            response.setStatus(ResponseStatusEnum.SUCCESS);
            response.setDescription(messages.get("baseEntity.find.success"));

            logger.info("End successfully method [findAll] for [Role]");
        } catch (Exception e)
        {
            logger.error("There is an error in method [findAll] for [Role]", e);

            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }

    @Override
    public Response delete(Long id)
    {
        Response response = new Response();
        response.setData(null);
        try
        {
            logger.info("Start method [delete] for [Role] via id [" + id + "]");

            if (roleDao.delete(id))
            {
                UserUtil.removeRoleCache(id);
                response.setStatus(ResponseStatusEnum.SUCCESS);
                response.setDescription(messages.get("baseEntity.delete.success"));
            }
            else
            {
                response.setStatus(ResponseStatusEnum.ERROR);
                response.setDescription(messages.get("baseEntity.delete.error"));
            }

            logger.info("End successfully method [delete] for [Role] via id [" + id + "]");
        } catch (Exception e)
        {
            logger.error("There is an error in method [delete] for [Role] via id [" + id + "]", e);

            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }

    @Override
    public Response update(Role entity)
    {
        Response response = new Response();
        response.setData(entity);
        try
        {
            logger.info("Start method [update] for [Role] via id [" + entity.getRoleId() + "]");

            if (roleDao.update(entity))
            {
                response.setStatus(ResponseStatusEnum.SUCCESS);
                response.setDescription(messages.get("baseEntity.update.success"));
            }
            else
            {
                response.setStatus(ResponseStatusEnum.ERROR);
                response.setDescription(messages.get("baseEntity.update.error"));
            }

            logger.info("End successfully method [update] for [Role] via id [" + entity.getRoleId() + "]");
        } catch (Exception e)
        {
            logger.error("There is an error in method [update] for [Role] via id [" + entity.getRoleId() + "]", e);

            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }
}
