package com.karafarin.terminalmanagement.service.impl.sys;

import com.karafarin.terminalmanagement.dao.impl.sys.SysBranchDaoImpl;
import com.karafarin.terminalmanagement.dto.sys.SysBranch;
import com.karafarin.terminalmanagement.message.Response;
import com.karafarin.terminalmanagement.message.enums.ResponseOwnerEnum;
import com.karafarin.terminalmanagement.message.enums.ResponseStatusEnum;
import com.karafarin.terminalmanagement.model.Messages;
import com.karafarin.terminalmanagement.service.intf.sys.ServiceSysBranch;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service
public class ServiceSysBranchImpl implements ServiceSysBranch
{
    @Autowired
    private SysBranchDaoImpl sysBranchDao;
    @Autowired
    private Messages messages;

    private static final Logger logger = LogManager.getLogger(ServiceSysBranchImpl.class);
    
    @Override
    public Response add(SysBranch entity)
    {
        return null;
    }

    @Override
    public Response findById(Long id)
    {
        Response response = new Response();
        try
        {
            logger.info("Start method [findById] for [SysBranch] via id [" + id + "]");
            
            SysBranch sysBranch = sysBranchDao.findById(id);
            response.setData(sysBranch);
            if (sysBranch.getBranchId() != null)
            {
                response.setStatus(ResponseStatusEnum.SUCCESS);
                response.setDescription(messages.get("baseEntity.find.success"));
            }
            else
            {
                response.setStatus(ResponseStatusEnum.ERROR);
                response.setDescription(messages.get("baseEntity.find.error"));
            }

            logger.info("End successfully method [findById] for [SysBranch] via id [" + id + "]");            
        } catch (Exception e)
        {
            logger.info("There is an error in method [findById] for [SysBranch] via id [" + id + "]", e);
            
            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }

    @Override
    public Response findAll()
    {
        Response response = new Response();
        try
        {
            logger.info("Start method [findAll] for [SysBranch]");
            
            response.setData(sysBranchDao.findAll());
            response.setStatus(ResponseStatusEnum.SUCCESS);
            response.setDescription(messages.get("baseEntity.find.success"));

            logger.info("End successfully method [findAll] for [SysBranch]");
        } catch (Exception e)
        {
            logger.info("There is an error in method [findAll] for [SysBranch]", e);
            
            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }

    @Override
    public Response delete(Long id)
    {
        return null;
    }

    @Override
    public Response update(SysBranch entity)
    {
        return null;
    }
}
