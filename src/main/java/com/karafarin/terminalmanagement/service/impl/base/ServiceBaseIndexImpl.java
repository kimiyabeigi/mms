package com.karafarin.terminalmanagement.service.impl.base;

import com.karafarin.terminalmanagement.dao.impl.base.BaseIndexDaoImpl;
import com.karafarin.terminalmanagement.dto.base.BaseIndex;
import com.karafarin.terminalmanagement.message.Response;
import com.karafarin.terminalmanagement.message.enums.ResponseOwnerEnum;
import com.karafarin.terminalmanagement.message.enums.ResponseStatusEnum;
import com.karafarin.terminalmanagement.model.Messages;
import com.karafarin.terminalmanagement.service.intf.base.ServiceBaseIndex;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service
public class ServiceBaseIndexImpl implements ServiceBaseIndex
{
    @Autowired
    BaseIndexDaoImpl baseIndexDao;

    @Autowired
    Messages messages;

    private static final Logger logger = LogManager.getLogger(ServiceBaseIndexImpl.class);

    @Override
    public Response add(BaseIndex entity)
    {
        Response response = new Response();
        try
        {
            logger.info("Start method [add] for [BaseIndex]");

            Long newIndexId = baseIndexDao.add(entity);
            entity.setIndexId(newIndexId);
            response.setStatus(ResponseStatusEnum.SUCCESS);
            response.setDescription(messages.get("baseEntity.create.success"));

            logger.info("End successfully method [add] for [BaseIndex] with new id [" + newIndexId + "]");
        } catch (Exception e)
        {
            logger.error("There is an error in method [add] for [BaseIndex]", e);

            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }

    @Override
    public Response findById(Long id)
    {
        Response response = new Response();
        try
        {
            logger.info("Start method [findById] for [BaseIndex] via id [" + id + "]");

            BaseIndex baseIndex = baseIndexDao.findById(id);
            response.setData(baseIndex);
            if (baseIndex.getIndexId() != null)
            {
                response.setStatus(ResponseStatusEnum.SUCCESS);
                response.setDescription(messages.get("baseEntity.find.success"));
            }
            else
            {
                response.setStatus(ResponseStatusEnum.ERROR);
                response.setDescription(messages.get("baseEntity.find.error"));
            }

            logger.info("End successfully method [findById] for [BaseIndex] via id [" + id + "]");
        } catch (Exception e)
        {
            logger.error("There is an error in method [findById] for [BaseIndex] via id [" + id + "]", e);

            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }

    @Override
    public Response findAll()
    {
        Response response = new Response();
        try
        {
            logger.info("Start method [findAll] for [BaseIndex]");

            response.setData(baseIndexDao.findAll());
            response.setStatus(ResponseStatusEnum.SUCCESS);
            response.setDescription(messages.get("baseEntity.find.success"));

            logger.info("End successfully method [findAll] for [BaseIndex]");

        } catch (Exception e)
        {
            logger.error("There is an error in method [findAll] for [BaseIndex]", e);

            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }

    @Override
    public Response update(BaseIndex entity)
    {
        Response response = new Response();
        response.setData(entity);
        try
        {
            logger.info("Start method [update] for [BaseIndex] via id [" + entity.getIndexId() + "]");

            if (baseIndexDao.update(entity))
            {
                response.setStatus(ResponseStatusEnum.SUCCESS);
                response.setDescription(messages.get("baseEntity.update.success"));
            }
            else
            {
                response.setStatus(ResponseStatusEnum.ERROR);
                response.setDescription(messages.get("baseEntity.update.error"));
            }

            logger.info("End successfully method [update] for [BaseIndex] via id [" + entity.getIndexId() + "]");
        } catch (Exception e)
        {
            logger.error("There is an error in method [update] for [BaseIndex] via id [" +
                    entity.getIndexId() + "]", e);

            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }

    @Override
    public Response delete(Long id)
    {
        Response response = new Response();
        response.setData(null);
        try
        {
            logger.info("Start method [delete] for [BaseIndex] via id [" + id + "]");

            if (baseIndexDao.delete(id))
            {
                response.setStatus(ResponseStatusEnum.SUCCESS);
                response.setDescription(messages.get("baseEntity.delete.success"));
            }
            else
            {
                response.setStatus(ResponseStatusEnum.ERROR);
                response.setDescription(messages.get("baseEntity.delete.error"));
            }

            logger.info("End successfully method [delete] for [BaseIndex] via id [" + id + "]");
        } catch (Exception e)
        {
            logger.error("There is an error in method [delete] for [BaseIndex] via id [" + id + "]", e);

            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }

}
