package com.karafarin.terminalmanagement.service.impl.psp;

import com.karafarin.terminalmanagement.dao.impl.psp.*;
import com.karafarin.terminalmanagement.dao.impl.sys.SysRequestStatusDaoImpl;
import com.karafarin.terminalmanagement.dto.psp.*;
import com.karafarin.terminalmanagement.dto.sys.SysRequestStatus;
import com.karafarin.terminalmanagement.message.Response;
import com.karafarin.terminalmanagement.message.enums.RequestStatusEnum;
import com.karafarin.terminalmanagement.message.enums.ResponseOwnerEnum;
import com.karafarin.terminalmanagement.message.enums.ResponseStatusEnum;
import com.karafarin.terminalmanagement.model.Messages;
import com.karafarin.terminalmanagement.model.psp.WrapperResponse;
import com.karafarin.terminalmanagement.service.intf.psp.ServiceTracePSP;
import com.karafarin.terminalmanagement.wrapper.enums.ServiceEnum;
import com.karafarin.terminalmanagement.wrapper.enums.StatusTypeEnum;
import com.karafarin.terminalmanagement.wrapper.model.req.PosStatusRequest;
import com.karafarin.terminalmanagement.wrapper.model.req.PosTerminalRequest;
import com.karafarin.terminalmanagement.wrapper.model.req.PosTraceRequest;
import com.karafarin.terminalmanagement.wrapper.model.resp.PosStatusResponse;
import com.karafarin.terminalmanagement.wrapper.model.resp.PosTerminalResponse;
import com.karafarin.terminalmanagement.wrapper.model.resp.PosTraceResponse;
import com.karafarin.terminalmanagement.wrapper.service.impl.ServicePosStatusReqImpl;
import com.karafarin.terminalmanagement.wrapper.service.impl.ServicePosTerminalReqImpl;
import com.karafarin.terminalmanagement.wrapper.service.impl.ServicePosTraceReqImpl;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import sun.security.provider.certpath.OCSPResponse;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Service
public class ServiceTracePSPImpl implements ServiceTracePSP
{
    private static final Logger logger = LogManager.getLogger(ServiceTracePSPImpl.class);

    @Autowired
    private Messages messages;
    @Autowired
    private PSPTraceBatchDaoImpl pspTraceBatchDao;
    @Autowired
    private ServicePosTraceReqImpl servicePosTraceReq;
    @Autowired
    private PSPRequestStatusDaoImpl pspRequestStatusDao;
    @Autowired
    private PSPRequestDaoImpl pspRequestDao;
    @Autowired
    private PSPStatusBatchDaoImpl pspStatusBatchDao;
    @Autowired
    private ServicePosStatusReqImpl servicePosStatusReq;
    @Autowired
    private SysRequestStatusDaoImpl sysRequestStatusDao;
    @Autowired
    private PSPTerminalBatchDaoImpl pspTerminalBatchDao;
    @Autowired
    private ServicePosTerminalReqImpl servicePosTerminalReq;

    @Override
    public Response findAllUnknownRequests()
    {
        Response response = new Response();
        try
        {
            logger.info("Start method [findAllUnknownRequests] for [PSPTraceBatch]");

            response.setData(pspTraceBatchDao.findAll());
            response.setStatus(ResponseStatusEnum.SUCCESS);
            response.setDescription(messages.get("baseEntity.find.success"));

            logger.info("End successfully method [findAllUnknownRequests] for [PSPTraceBatch]");
        } catch (Exception e)
        {
            logger.error("There is an error in method [findAllUnknownRequests] for [PSPTraceBatch]", e);

            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }

    @Override
    public Response traceUnknownRequest(List<PSPTraceBatch> pspTraceBatches)
    {
        Response response = new Response();
        SysRequestStatus sysRequestStatus = new SysRequestStatus();
        List<WrapperResponse> wrapperResponses = new ArrayList<>();

        logger.info("Start of method [traceRequest]");

        try
        {
            for (PSPTraceBatch pspTraceBatch : pspTraceBatches)
            {
                if (pspTraceBatch.isSelected())
                {
                    WrapperResponse wrapperResponse = new WrapperResponse();

                    PosTraceRequest posTraceRequest = new PosTraceRequest();
                    posTraceRequest.setRequestNumber(pspTraceBatch.getRequestNumber());
                    posTraceRequest.setCompanyCode(pspTraceBatch.getCompanyCode());
                    posTraceRequest.setTrackingCode(pspTraceBatch.getPspTrackingCode());

                    wrapperResponse.setRequestNumber(pspTraceBatch.getRequestNumber());
                    Long requestStatusId =
                            findPSPRequestRequestStatusIdById(pspTraceBatch.getPspRequestId());
                    if (requestStatusId != 4)
                    {
                        wrapperResponse.setStatus(ResponseStatusEnum.ERROR.toString());
                        wrapperResponse.setErrorDesc(messages.get("psp.error.22"));
                    }
                    else
                    {
                        PosTraceResponse posTraceResponse =
                                servicePosTraceReq.posTraceReq(pspTraceBatch.getCompanyCode(), posTraceRequest);
                        wrapperResponse.setStatus(ResponseStatusEnum.SUCCESS.toString());
                        wrapperResponse.setPspTrackingCode(posTraceResponse.getTrackingCode());
                        wrapperResponse.setErrorDesc(posTraceResponse.getErrorDesc());

                        PSPRequestStatus pspRequestStatus = new PSPRequestStatus();
                        pspRequestStatus.setPspErrorCode(posTraceResponse.getErrorCode());
                        pspRequestStatus.setPspErrorDesc(posTraceResponse.getErrorDesc());
                        pspRequestStatus.setPspRequestId(pspTraceBatch.getPspRequestId());
                        if (posTraceResponse.getStatusTypeId().equals(StatusTypeEnum.SUCCESS.getValue()))
                        {
                            updatePSPRequestRequestStatusId(pspTraceBatch.getPspRequestId(), RequestStatusEnum.REGISTERED.getValue());
                            updateTrackingCodePSPRequest(pspTraceBatch.getPspRequestId(), posTraceResponse.getTrackingCode());
                            pspRequestStatus.setRequestStatusId(RequestStatusEnum.REGISTERED.getValue());
                            pspRequestStatus.setStatusTypeId(posTraceResponse.getStatusTypeId());
                            sysRequestStatus = findRequestStatusById(RequestStatusEnum.REGISTERED.getValue());
                        } else if (posTraceResponse.getStatusTypeId().equals(StatusTypeEnum.FAIL.getValue()))
                        {
                            updatePSPRequestRequestStatusId(pspTraceBatch.getPspRequestId(), RequestStatusEnum.REJECT.getValue());
                            pspRequestStatus.setRequestStatusId(RequestStatusEnum.REJECT.getValue());
                            pspRequestStatus.setStatusTypeId(posTraceResponse.getStatusTypeId());
                            sysRequestStatus = findRequestStatusById(RequestStatusEnum.REJECT.getValue());
                        } else if (posTraceResponse.getStatusTypeId().equals(StatusTypeEnum.UNKNOWN.getValue()) ||
                                   posTraceResponse.getStatusTypeId().equals(StatusTypeEnum.EXCEPTION.getValue()))
                        {
                            sysRequestStatus = findRequestStatusById(pspTraceBatch.getPspRequestId());
                            pspRequestStatus.setRequestStatusId(requestStatusId);
                            pspRequestStatus.setStatusTypeId(posTraceResponse.getStatusTypeId());
                        }

                        pspRequestStatus.setRequestStatusId(posTraceResponse.getStatusTypeId());
                        pspRequestStatus.setServiceId(ServiceEnum.TRACE.getValue());

                        addPSPRequestStatus(pspRequestStatus);

                        wrapperResponse.setStatus(sysRequestStatus.getDescription());
                        wrapperResponse.setStatusEn(sysRequestStatus.getRequestStatusCode());
                    }
                    wrapperResponses.add(wrapperResponse);
                }
            }

            response.setStatus(ResponseStatusEnum.SUCCESS);
            response.setDescription(messages.get("psp.trace.success"));
            response.setData(wrapperResponses);
            logger.info("End of method [traceRequest]");
        } catch (Exception e)
        {
            logger.error("There is an error in method [traceRequest] ", e);

            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }

    @Override
    public Response findAllAcceptedRequests()
    {
        Response response = new Response();
        try
        {
            logger.info("Start method [findAllAcceptedRequests] for [PSPStatusBatch]");

            response.setData(pspStatusBatchDao.findAll());
            response.setStatus(ResponseStatusEnum.SUCCESS);
            response.setDescription(messages.get("baseEntity.find.success"));

            logger.info("End successfully method [findAllAcceptedRequests] for [PSPStatusBatch]");
        } catch (Exception e)
        {
            logger.error("There is an error in method [findAllAcceptedRequests] for [PSPStatusBatch]", e);

            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }

    @Override
    public Response statusAcceptedRequest(List<PSPStatusBatch> pspStatusBatches)
    {
        Response response = new Response();
        SysRequestStatus sysRequestStatus = new SysRequestStatus();
        List<WrapperResponse> wrapperResponses = new ArrayList<>();

        logger.info("Start of method [statusAcceptedRequest]");

        try
        {
            for (PSPStatusBatch pspStatusBatch : pspStatusBatches)
            {
                if (pspStatusBatch.isSelected())
                {
                    WrapperResponse wrapperResponse = new WrapperResponse();

                    wrapperResponse.setRequestNumber(pspStatusBatch.getRequestNumber());
                    wrapperResponse.setPspTrackingCode(pspStatusBatch.getPspTrackingCode());

                    Long requestStatusId =
                            findPSPRequestRequestStatusIdById(pspStatusBatch.getPspRequestId());

                    if (requestStatusId != 2 && requestStatusId != 5)
                    {
                        wrapperResponse.setStatus(ResponseStatusEnum.ERROR.toString());
                        wrapperResponse.setErrorDesc(messages.get("psp.error.22"));
                    }
                    else
                    {
                        PosStatusRequest posStatusRequest = new PosStatusRequest();
                        posStatusRequest.setRequestNumber(pspStatusBatch.getRequestNumber());
                        posStatusRequest.setCompanyCode(pspStatusBatch.getCompanyCode());
                        posStatusRequest.setTrackingCode(pspStatusBatch.getPspTrackingCode());

                        PosStatusResponse posStatusResponse =
                                servicePosStatusReq.posStatusReq(pspStatusBatch.getCompanyCode(), posStatusRequest);

                        wrapperResponse.setErrorDesc(posStatusResponse.getErrorDesc());
                        wrapperResponse.setStatus(posStatusResponse.getStatus());

                        PSPRequestStatus pspRequestStatus = new PSPRequestStatus();
                        pspRequestStatus.setPspErrorCode(posStatusResponse.getErrorCode());
                        pspRequestStatus.setPspErrorDesc(posStatusResponse.getErrorDesc());
                        pspRequestStatus.setPspRequestId(pspStatusBatch.getPspRequestId());
                        pspRequestStatus.setServiceId(ServiceEnum.STATUS.getValue());
                        pspRequestStatus.setStatusTypeId(posStatusResponse.getStatusTypeId());

                        if (posStatusResponse.getStatusTypeId().equals(StatusTypeEnum.SUCCESS.getValue()))
                        {
                            Long pspRequestStatusId = findPSPRequestRequestStatusIdById(pspStatusBatch.getPspRequestId());
                            pspRequestStatus.setRequestStatusId(pspRequestStatusId);
                            pspRequestStatus.setStatus(posStatusResponse.getStatus());
                            wrapperResponse.setStatus(posStatusResponse.getStatus());

                        } else if (posStatusResponse.getStatusTypeId().equals(StatusTypeEnum.FAIL.getValue()))
                        {
                            updatePSPRequestRequestStatusId(pspStatusBatch.getPspRequestStatusId(), RequestStatusEnum.REJECT.getValue());
                            pspRequestStatus.setRequestStatusId(RequestStatusEnum.REJECT.getValue());
                            pspRequestStatus.setStatus(posStatusResponse.getStatus());
                            sysRequestStatus = findRequestStatusById(RequestStatusEnum.REJECT.getValue());
                            wrapperResponse.setStatus(sysRequestStatus.getDescription());
                            wrapperResponse.setStatusEn(sysRequestStatus.getRequestStatusCode());
                        } else if (posStatusResponse.getStatusTypeId().equals(StatusTypeEnum.UNKNOWN.getValue()))
                        {
                            Long pspRequestStatusId = findPSPRequestRequestStatusIdById(pspStatusBatch.getPspRequestId());
                            pspRequestStatus.setRequestStatusId(pspRequestStatusId);
                            pspRequestStatus.setStatus(posStatusResponse.getStatus());
                        } else if (posStatusResponse.getStatusTypeId().equals(StatusTypeEnum.EXCEPTION.getValue()))
                        {
                            pspRequestStatus.setRequestStatusId(requestStatusId);
                            pspRequestStatus.setStatus(posStatusResponse.getStatus());
                        }

                        addPSPRequestStatus(pspRequestStatus);
                    }
                    wrapperResponses.add(wrapperResponse);
                }
            }

            response.setStatus(ResponseStatusEnum.SUCCESS);
            response.setDescription(messages.get("psp.trace.success"));
            response.setData(wrapperResponses);
            logger.info("End of method [statusAcceptedRequest]");
        } catch (Exception e)
        {
            logger.error("There is an error in method [statusAcceptedRequest] ", e);

            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }

    @Override
    public Response findAllRegisterRequests()
    {
        Response response = new Response();
        try
        {
            logger.info("Start method [findAllRegisterRequests] for [PSPTerminalBatch]");

            response.setData(pspTerminalBatchDao.findAll());
            response.setStatus(ResponseStatusEnum.SUCCESS);
            response.setDescription(messages.get("baseEntity.find.success"));

            logger.info("End successfully method [findAllRegisterRequests] for [PSPTerminalBatch]");
        } catch (Exception e)
        {
            logger.error("There is an error in method [findAllRegisterRequests] for [PSPTerminalBatch]", e);

            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }

    @Override
    public Response terminalRegisteredRequest(List<PSPTerminalBatch> pspTerminalBatches)
    {
        Response response = new Response();
        SysRequestStatus sysRequestStatus = new SysRequestStatus();
        List<WrapperResponse> wrapperResponses = new ArrayList<>();

        logger.info("Start of method [terminalRegisteredRequest]");

        try
        {
            for (PSPTerminalBatch pspTerminalBatch : pspTerminalBatches)
            {
                if (pspTerminalBatch.isSelected())
                {
                    WrapperResponse wrapperResponse = new WrapperResponse();

                    wrapperResponse.setRequestNumber(pspTerminalBatch.getRequestNumber());
                    wrapperResponse.setPspTrackingCode(pspTerminalBatch.getPspTrackingCode());

                    Long requestStatusId =
                            findPSPRequestRequestStatusIdById(pspTerminalBatch.getPspRequestId());

                    if (requestStatusId != 2)
                    {
                        wrapperResponse.setStatus(ResponseStatusEnum.ERROR.toString());
                        wrapperResponse.setErrorDesc(messages.get("psp.error.22"));
                    }
                    else
                    {
                        PosTerminalRequest posTerminalRequest = new PosTerminalRequest();
                        posTerminalRequest.setRequestNumber(pspTerminalBatch.getRequestNumber());
                        posTerminalRequest.setCompanyCode(pspTerminalBatch.getCompanyCode());
                        posTerminalRequest.setTrackingCode(pspTerminalBatch.getPspTrackingCode());

                        PosTerminalResponse posTerminalResponse = servicePosTerminalReq
                                .posTerminalReq(pspTerminalBatch.getCompanyCode(), posTerminalRequest);

                        wrapperResponse.setErrorDesc(posTerminalResponse.getErrorDesc());
                        wrapperResponse.setPSPAcceptanceCode(posTerminalResponse.getAcceptanceCode());
                        wrapperResponse.setPSPTerminalCode(posTerminalResponse.getTerminalCode());

                        PSPRequestStatus pspRequestStatus = new PSPRequestStatus();
                        pspRequestStatus.setPspErrorCode(posTerminalResponse.getErrorCode());
                        pspRequestStatus.setPspErrorDesc(posTerminalResponse.getErrorDesc());
                        pspRequestStatus.setPspRequestId(pspTerminalBatch.getPspRequestId());
                        pspRequestStatus.setServiceId(ServiceEnum.TERMINAL.getValue());

                        if (posTerminalResponse.getStatusTypeId().equals(StatusTypeEnum.SUCCESS.getValue()))
                        {
                            updatePSPRequestRequestStatusId(pspTerminalBatch.getPspRequestId(), RequestStatusEnum.ACCEPTED.getValue());
                            pspRequestStatus.setRequestStatusId(RequestStatusEnum.ACCEPTED.getValue());
                            pspRequestStatus.setStatusTypeId(posTerminalResponse.getStatusTypeId());

                            updateTerminalById(pspTerminalBatch.getPspRequestId(), posTerminalResponse.getAcceptanceCode(),
                                               posTerminalResponse.getTerminalCode());
                            sysRequestStatus = findRequestStatusById(RequestStatusEnum.ACCEPTED.getValue());

                        } else if (posTerminalResponse.getStatusTypeId().equals(StatusTypeEnum.FAIL.getValue()))
                        {
                            updatePSPRequestRequestStatusId(pspTerminalBatch.getPspRequestId(), RequestStatusEnum.REJECT.getValue());
                            pspRequestStatus.setRequestStatusId(RequestStatusEnum.REJECT.getValue());
                            pspRequestStatus.setStatusTypeId(posTerminalResponse.getStatusTypeId());

                            sysRequestStatus = findRequestStatusById(RequestStatusEnum.REJECT.getValue());
                        } else if (posTerminalResponse.getStatusTypeId().equals(StatusTypeEnum.UNKNOWN.getValue()) ||
                                   posTerminalResponse.getStatusTypeId().equals(StatusTypeEnum.EXCEPTION.getValue()))
                        {
                            pspRequestStatus.setRequestStatusId(requestStatusId);
                            pspRequestStatus.setStatusTypeId(posTerminalResponse.getStatusTypeId());

                            sysRequestStatus = findRequestStatusById(pspTerminalBatch.getRequestStatusId());
                        }

                        addPSPRequestStatus(pspRequestStatus);
                        wrapperResponse.setStatusEn(sysRequestStatus.getRequestStatusCode());
                        wrapperResponse.setStatus(sysRequestStatus.getDescription());
                    }
                    wrapperResponses.add(wrapperResponse);
                }
            }

            response.setStatus(ResponseStatusEnum.SUCCESS);
            response.setDescription(messages.get("psp.trace.success"));
            response.setData(wrapperResponses);
            logger.info("End of method [terminalRegisteredRequest]");
        } catch (Exception e)
        {
            logger.error("There is an error in method [terminalRegisteredRequest] ", e);

            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }

    @Override
    public Response traceRequestByRequestNumber(Long requestNumber)
    {
        Response response = new Response();
        try
        {
            logger.info("Start method [traceRequestByRequestNumber] for [PSPTraceBatch]");

            PSPTraceBatch pspTraceBatch = findPSPTraceBatchByRequestNumber(requestNumber);

            if (pspTraceBatch.getPspRequestId() == null)
            {
                response.setData(null);
                response.setStatus(ResponseStatusEnum.ERROR);
                response.setDescription(messages.get("psp.error.24"));
            } else if (!pspTraceBatch.getRequestStatusId().equals(RequestStatusEnum.UNKNOWN.getValue()))
            {
                response.setData(null);
                response.setStatus(ResponseStatusEnum.ERROR);
                response.setDescription(messages.get("psp.error.22"));
            } else
            {
                List<PSPTraceBatch> pspTraceBatches = new ArrayList<>();
                pspTraceBatch.setSelected(true);
                pspTraceBatches.add(pspTraceBatch);
                response = traceUnknownRequest(pspTraceBatches);
            }

            logger.info("End successfully method [traceRequestByRequestNumber] for [PSPTraceBatch]");
        } catch (Exception e)
        {
            logger.error("There is an error in method [traceRequestByRequestNumber] for [PSPTraceBatch]", e);

            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }

    @Override
    public Response statusRequestByPSPTrackingCode(String pspTrackingCode)
    {
        Response response = new Response();
        try
        {
            logger.info("Start method [statusRequestByPSPTrackingCode] for [PSPTraceBatch]");

            PSPStatusBatch pspStatusBatch = findPSPStatusBatchByPSPTrackingCode(pspTrackingCode);

            if (pspStatusBatch.getPspRequestId() == null)
            {
                response.setData(null);
                response.setStatus(ResponseStatusEnum.ERROR);
                response.setDescription(messages.get("psp.error.25"));
            } else if (!pspStatusBatch.getRequestStatusId().equals(RequestStatusEnum.REGISTERED.getValue()) &&
                       !pspStatusBatch.getRequestStatusId().equals(RequestStatusEnum.ACCEPTED.getValue()))
            {
                response.setData(null);
                response.setStatus(ResponseStatusEnum.ERROR);
                response.setDescription(messages.get("psp.error.22"));
            } else
            {
                List<PSPStatusBatch> pspStatusBatches = new ArrayList<>();
                pspStatusBatch.setSelected(true);
                pspStatusBatches.add(pspStatusBatch);
                response = statusAcceptedRequest(pspStatusBatches);
            }

            logger.info("End successfully method [statusRequestByPSPTrackingCode] for [PSPTraceBatch]");
        } catch (Exception e)
        {
            logger.error("There is an error in method [statusRequestByPSPTrackingCode] for [PSPTraceBatch]", e);

            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }

    @Override
    public Response terminalRequestByPSPTrackingCode(String pspTrackingCode)
    {
        Response response = new Response();
        try
        {
            logger.info("Start method [terminalRequestByPSPTrackingCode] for [PSPTerminalBatch]");

            PSPTerminalBatch pspTerminalBatch = findPSPTerminalBatchByPSPTrackingCode(pspTrackingCode);

            if (pspTerminalBatch.getPspRequestId() == null)
            {
                response.setData(null);
                response.setStatus(ResponseStatusEnum.ERROR);
                response.setDescription(messages.get("psp.error.25"));
            } else if (!pspTerminalBatch.getRequestStatusId().equals(RequestStatusEnum.REGISTERED.getValue()))
            {
                response.setData(null);
                response.setStatus(ResponseStatusEnum.ERROR);
                response.setDescription(messages.get("psp.error.22"));
            } else
            {
                List<PSPTerminalBatch> pspTerminalBatches = new ArrayList<>();
                pspTerminalBatch.setSelected(true);
                pspTerminalBatches.add(pspTerminalBatch);
                response = terminalRegisteredRequest(pspTerminalBatches);
            }

            logger.info("End successfully method [terminalRequestByPSPTrackingCode] for [PSPTerminalBatch]");
        } catch (Exception e)
        {
            logger.error("There is an error in method [terminalRequestByPSPTrackingCode] for [PSPTerminalBatch]", e);

            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }

    private PSPTerminalBatch findPSPTerminalBatchByPSPTrackingCode(String pspTrackingCode) throws SQLException
    {
        PSPTerminalBatch result;
        logger.info(
                "Start method [findPSPTerminalBatchByPSPTrackingCode] for [PSPTerminalBatch] via pspTrackingCode [" +
                pspTrackingCode + "]");

        result = pspTerminalBatchDao.findRegisterByPSPTrackingCode(pspTrackingCode);

        logger.info(
                "End successfully method [findPSPTerminalBatchByPSPTrackingCode] for [PSPTerminalBatch] via pspTrackingCode [" +
                pspTrackingCode + "]");

        return result;
    }

    private PSPStatusBatch findPSPStatusBatchByPSPTrackingCode(String pspTrackingCode) throws SQLException
    {
        PSPStatusBatch result;
        logger.info("Start method [findPSPStatusBatchByPSPTrackingCode] for [PSPStatusBatch] via pspTrackingCode [" +
                    pspTrackingCode + "]");

        result = pspStatusBatchDao.findByPSPTrackingCode(pspTrackingCode);

        logger.info(
                "End successfully method [findPSPStatusBatchByPSPTrackingCode] for [PSPStatusBatch] via pspTrackingCode [" +
                pspTrackingCode + "]");

        return result;
    }

    private PSPTraceBatch findPSPTraceBatchByRequestNumber(Long requestNumber) throws SQLException
    {
        PSPTraceBatch result;
        logger.info("Start method [findPSPTraceBatchByRequestNumber] for [PSPTraceBatch] via requestNumber [" +
                    requestNumber + "]");

        result = pspTraceBatchDao.findByRequestNumber(requestNumber);

        logger.info(
                "End successfully method [findPSPTraceBatchByRequestNumber] for [PSPTraceBatch] via requestNumber [" +
                requestNumber + "]");

        return result;
    }

    private SysRequestStatus findRequestStatusById(Long id) throws SQLException
    {
        SysRequestStatus result;
        logger.info("Start method [findRequestStatusById] for [SysRequestStatus] via id [" + id + "]");

        result = sysRequestStatusDao.findById(id);

        logger.info("End successfully method [findRequestStatusById] for [SysRequestStatus] via id [" + id + "]");

        return result;
    }

    private boolean updateStatusById(Long id, String status) throws SQLException
    {
        logger.info("Start method [updateStatusById] for [PSPRequestStatus] via id [" + id + "]");

        boolean result;
        result = pspRequestStatusDao.updateStatusById(id, status);

        logger.info("End successfully method [updateStatusById] for [PSPRequestStatus] via id [" + id + "]");

        return result;
    }

    private boolean updateRequestStatusIdById(Long id, Long requestStatusId) throws SQLException
    {
        logger.info("Start method [updateRequestStatusIdById] for [PSPRequestStatus] via id [" + id + "]");

        boolean result;
        result = pspRequestStatusDao.updateRequestStatusIdById(id, requestStatusId);

        logger.info("End successfully method [updateRequestStatusIdById] for [PSPRequestStatus] via id [" + id + "]");

        return result;
    }

    private Long addPSPRequestStatus(PSPRequestStatus entity) throws SQLException
    {
        logger.info("Start method [addPSPRequestStatus] for [PSPRequestStatus]");

        Long newPSPRequestStatusId = pspRequestStatusDao.add(entity);
        entity.setPspRequestId(newPSPRequestStatusId);

        logger.info("End successfully method [addPSPRequestStatus] for [PSPRequestStatus] with new id [" +
                    newPSPRequestStatusId + "]");

        return entity.getPspRequestId();
    }

    private boolean updateTrackingCodePSPRequest(Long id, String trackingCode) throws SQLException
    {
        boolean result;
        logger.info("Start method [updatePSPRequest] for [PSPRequest] via id [" + id + "]");

        result = pspRequestDao.updateTrackingCode(id, trackingCode);

        logger.info("End successfully method [updatePSPRequest] for [PSPRequest] via id [" + id + "]");

        return result;
    }

    private boolean updatePSPRequestRequestStatusId(Long id, Long requestStatusId) throws SQLException
    {
        boolean result;
        logger.info("Start method [updatePSPRequestRequestStatusId] for [PSPRequest] via id [" + id + "]");

        result = pspRequestDao.updateRequestStatusId(id, requestStatusId);

        logger.info("End successfully method [updatePSPRequestRequestStatusId] for [PSPRequest] via id [" + id + "]");

        return result;
    }

    private boolean updateTerminalById(Long id, String pspAcceptanceCode, String pspTerminalCode) throws SQLException
    {
        boolean result;
        logger.info("Start method [updateTerminalById] for [PSPRequest] via id [" + id + "]");

        result = pspRequestDao.updateTerminalById(id, pspAcceptanceCode, pspTerminalCode);

        logger.info("End successfully method [updateTerminalById] for [PSPRequest] via id [" + id + "]");

        return result;
    }

    private Long findPSPRequestRequestStatusIdById(Long id) throws SQLException
    {
        Long result;
        logger.info("Start method [findPSPRequestRequestStatusIdById] for [PSPRequest] via id [" + id + "]");

        PSPRequest pspRequest = pspRequestDao.findById(id);
        result = pspRequest.getRequestStatusId();

        logger.info("End successfully method [findPSPRequestRequestStatusIdById] for [PSPRequest] via id [" + id + "]");

        return result;
    }
}
