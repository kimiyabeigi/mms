package com.karafarin.terminalmanagement.service.impl.base;

import com.karafarin.terminalmanagement.dao.impl.base.BaseCityDaoImpl;
import com.karafarin.terminalmanagement.dto.base.BaseCity;
import com.karafarin.terminalmanagement.message.Response;
import com.karafarin.terminalmanagement.message.enums.ResponseOwnerEnum;
import com.karafarin.terminalmanagement.message.enums.ResponseStatusEnum;
import com.karafarin.terminalmanagement.model.Messages;
import com.karafarin.terminalmanagement.service.intf.base.ServiceBaseCity;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service
public class ServiceBaseCityImpl implements ServiceBaseCity
{
    @Autowired
    private BaseCityDaoImpl baseCityDao;
    @Autowired
    private Messages messages;

    private static final Logger logger = LogManager.getLogger(ServiceBaseCityImpl.class);

    @Override
    public Response add(BaseCity entity)
    {
        Response response = new Response();
        try
        {
            logger.info("Start method [add] for [BaseCity]");

            Long newCityId = baseCityDao.add(entity);
            entity.setCityId(newCityId);
            response.setStatus(ResponseStatusEnum.SUCCESS);
            response.setData(entity);
            response.setDescription(messages.get("baseEntity.create.success"));

            logger.info("End successfully method [add] for [BaseCity] with new id [" + newCityId + "]");
        } catch (Exception e)
        {
            logger.error("There is an error in method [add] for [BaseCity]", e);

            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }

    @Override
    public Response findById(Long id)
    {
        Response response = new Response();
        try
        {
            logger.info("Start method [findById] for [BaseCity] via id [" + id + "]");

            BaseCity baseCity = baseCityDao.findById(id);
            response.setData(baseCity);
            if (baseCity.getCityId() != null)
            {
                response.setStatus(ResponseStatusEnum.SUCCESS);
                response.setDescription(messages.get("baseEntity.find.success"));
            }
            else
            {
                response.setStatus(ResponseStatusEnum.ERROR);
                response.setDescription(messages.get("baseEntity.find.error"));
            }

            logger.info("End successfully method [findById] for [BaseCity] via id [" + id + "]");
        } catch (Exception e)
        {
            logger.error("There is an error in method [findById] for [BaseCity] via id [" + id + "]", e);

            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }

    @Override
    public Response findAll()
    {
        return null;
    }

    @Override
    public Response findAll(boolean all)
    {
        Response response = new Response();
        try
        {
            logger.info("Start method [findAll] for [BaseCity]");

            response.setData(baseCityDao.findAll(all));
            response.setStatus(ResponseStatusEnum.SUCCESS);
            response.setDescription(messages.get("baseEntity.find.success"));

            logger.info("End successfully method [findAll] for [BaseCity]");
        } catch (Exception e)
        {
            logger.error("There is an error in method [findAll] for [BaseCity]", e);

            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }

    @Override
    public Response delete(Long id)
    {
        Response response = new Response();
        response.setData(null);
        try
        {
            logger.info("Start method [delete] for [BaseCity] via id [" + id + "]");

            if (baseCityDao.delete(id))
            {
                response.setStatus(ResponseStatusEnum.SUCCESS);
                response.setDescription(messages.get("baseEntity.delete.success"));
            }
            else
            {
                response.setStatus(ResponseStatusEnum.ERROR);
                response.setDescription(messages.get("baseEntity.delete.error"));
            }

            logger.info("End successfully method [delete] for [BaseCity] via id [" + id + "]");
        } catch (Exception e)
        {
            logger.error("There is an error in method [delete] for [BaseCity] via id [" + id + "]", e);

            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }

    @Override
    public Response update(BaseCity entity)
    {
        Response response = new Response();
        response.setData(entity);
        try
        {
            logger.info("Start method [update] for [BaseCity] via id [" + entity.getCityId() + "]");

            if (baseCityDao.update(entity))
            {
                response.setStatus(ResponseStatusEnum.SUCCESS);
                response.setDescription(messages.get("baseEntity.update.success"));
            }
            else
            {
                response.setStatus(ResponseStatusEnum.ERROR);
                response.setDescription(messages.get("baseEntity.update.error"));
            }

            logger.info("End successfully method [update] for [BaseCity] via id [" + entity.getCityId() + "]");
        } catch (Exception e)
        {
            logger.error("There is an error in method [update] for [BaseCity] via id [" + entity.getCityId() + "]", e);

            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }

    @Override
    public Response findByStateId(Long id)
    {
        Response response = new Response();
        try
        {
            logger.info("Start method [findByStateId] for [BaseCity] via id [" + id + "]");

            response.setData(baseCityDao.findByStateId(id));
            response.setStatus(ResponseStatusEnum.SUCCESS);
            response.setDescription(messages.get("baseEntity.find.success"));

            logger.info("End successfully method [findByStateId] for [BaseCity] via id [" + id + "]");

        } catch (Exception e)
        {
            logger.error("There is an error in method [findByStateId] for [BaseCity] via id [" + id + "]", e);

            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }
}
