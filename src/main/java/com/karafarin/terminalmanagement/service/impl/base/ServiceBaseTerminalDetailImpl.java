package com.karafarin.terminalmanagement.service.impl.base;

import com.karafarin.terminalmanagement.dao.impl.base.BaseTerminalDetailDaoImpl;
import com.karafarin.terminalmanagement.dto.base.BaseTerminalDetail;
import com.karafarin.terminalmanagement.message.Response;
import com.karafarin.terminalmanagement.message.enums.ResponseOwnerEnum;
import com.karafarin.terminalmanagement.message.enums.ResponseStatusEnum;
import com.karafarin.terminalmanagement.model.Messages;
import com.karafarin.terminalmanagement.service.intf.base.ServiceBaseTerminalDetail;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service
public class ServiceBaseTerminalDetailImpl implements ServiceBaseTerminalDetail
{
    @Autowired
    private BaseTerminalDetailDaoImpl baseTerminalDetailDao;
    @Autowired
    private Messages messages;

    private static final Logger logger = LogManager.getLogger(ServiceBaseTerminalDetailImpl.class);

    @Override
    public Response add(BaseTerminalDetail entity) {

        Response response = new Response();
        try
        {
            logger.info("Start method [add] for [BaseTerminalDetail]");

            Long newTerminalDetailId = baseTerminalDetailDao.add(entity);
            entity.setTerminalDetailId(newTerminalDetailId);
            response.setStatus(ResponseStatusEnum.SUCCESS);
            response.setDescription(messages.get("baseEntity.create.success"));

            logger.info("End successfully method [add] for [BaseTerminalDetail] with new id [" +
                    newTerminalDetailId + "]");

        } catch (Exception e)
        {
            logger.error("There is an error in method [add] for [BaseTerminalDetail]", e);

            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }

    @Override
    public Response findById(Long id) {

        Response response = new Response();
        try
        {
            logger.info("Start method [findById] for [BaseTerminalDetail] via id [" + id + "]");

            BaseTerminalDetail baseTerminalDetail = baseTerminalDetailDao.findById(id);
            response.setData(baseTerminalDetail);
            if (baseTerminalDetail.getTerminalDetailId() != null)
            {
                response.setStatus(ResponseStatusEnum.SUCCESS);
                response.setDescription(messages.get("baseEntity.find.success"));
            }
            else
            {
                response.setStatus(ResponseStatusEnum.ERROR);
                response.setDescription(messages.get("baseEntity.find.error"));
            }

            logger.info("End successfully method [findById] for [BaseTerminalDetail] via id [" + id + "]");
        } catch (Exception e)
        {
            logger.error("There is an error in method [findById] for [BaseTerminalDetail] via id [" + id + "]", e);

            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }

    @Override
    public Response findAll() {

        Response response = new Response();
        try
        {
            logger.info("Start method [findAll] for [BaseTerminalDetail]");

            response.setData(baseTerminalDetailDao.findAll());
            response.setStatus(ResponseStatusEnum.SUCCESS);
            response.setDescription(messages.get("baseEntity.find.success"));

            logger.info("End successfully method [findAll] for [BaseTerminalDetail]");
        } catch (Exception e)
        {
            logger.error("There is an error in method [findAll] for [BaseTerminalDetail]", e);

            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }

    @Override
    public Response update(BaseTerminalDetail entity)
    {
        Response response = new Response();
        response.setData(entity);
        try
        {
            logger.info("Start method [update] for [BaseTerminalDetail] via id [" +
                    entity.getTerminalDetailId() + "]");

            if (baseTerminalDetailDao.update(entity))
            {
                response.setStatus(ResponseStatusEnum.SUCCESS);
                response.setDescription(messages.get("baseEntity.update.success"));
            }
            else
            {
                response.setStatus(ResponseStatusEnum.ERROR);
                response.setDescription(messages.get("baseEntity.update.error"));
            }

            logger.info("End successfully method [update] for [BaseTerminalDetail] via id [" +
                    entity.getTerminalDetailId() + "]");
        } catch (Exception e)
        {
            logger.error("There is an error in method [update] for [BaseTerminalDetail] via id [" +
                    entity.getTerminalDetailId() + "]", e);

            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }

    @Override
    public Response delete(Long id) {

        Response response = new Response();
        response.setData(null);
        try
        {
            logger.info("Start method [delete] for [BaseTerminalDetail] via id [" + id + "]");

            if (baseTerminalDetailDao.delete(id))
            {
                response.setStatus(ResponseStatusEnum.SUCCESS);
                response.setDescription(messages.get("baseEntity.delete.success"));
            }
            else
            {
                response.setStatus(ResponseStatusEnum.ERROR);
                response.setDescription(messages.get("baseEntity.delete.error"));
            }

            logger.info("End successfully method [delete] for [BaseTerminalDetail] via id [" + id + "]");
        } catch (Exception e)
        {
            logger.error("There is an error in method [delete] for [BaseTerminalDetail] via id [" + id + "]", e);

            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }

    @Override
    public Response findSelectedTerminalDetails(Long id)
    {
        Response response = new Response();
        try
        {
            logger.info("Start method [findSelectedTerminalDetails] for [BaseTerminalDetail] via id [" + id + "]");

            response.setData(baseTerminalDetailDao.findSelectedTerminalDetails(id));
            response.setStatus(ResponseStatusEnum.SUCCESS);
            response.setDescription(messages.get("baseEntity.find.success"));

            logger.info("End successfully method [findSelectedTerminalDetails] for [BaseTerminalDetail] via id [" +
                    id + "]");

        } catch (Exception e)
        {
            logger.error("There is an error in method [findSelectedTerminalDetails] for [BaseTerminalDetail] via id [" +
                    id + "]", e);

            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }

    @Override
    public Response findByCompanyId(Long id)
    {
        Response response = new Response();
        try
        {
            logger.info("Start method [findByCompanyId] for [BaseTerminalDetail] via id [" + id + "]");

            response.setData(baseTerminalDetailDao.findByCompanyId(id));
            response.setStatus(ResponseStatusEnum.SUCCESS);
            response.setDescription(messages.get("baseEntity.find.success"));

            logger.info("End successfully method [findByCompanyId] for [BaseTerminalDetail] via id [" +
                    id + "]");

        } catch (Exception e)
        {
            logger.error("There is an error in method [findByCompanyId] for [BaseTerminalDetail] via id [" +
                    id + "]", e);

            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }
}
