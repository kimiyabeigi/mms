package com.karafarin.terminalmanagement.service.impl.auth;

import com.karafarin.terminalmanagement.dao.impl.auth.UserDaoImpl;
import com.karafarin.terminalmanagement.dto.auth.User;
import com.karafarin.terminalmanagement.message.Response;
import com.karafarin.terminalmanagement.message.enums.ResponseOwnerEnum;
import com.karafarin.terminalmanagement.message.enums.ResponseStatusEnum;
import com.karafarin.terminalmanagement.model.ChangePassword;
import com.karafarin.terminalmanagement.model.Messages;
import com.karafarin.terminalmanagement.service.intf.auth.ServiceUser;
import com.karafarin.terminalmanagement.util.UserUtil;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service
public class ServiceUserImpl implements ServiceUser
{
    @Autowired
    private UserDaoImpl userDao;
    @Autowired
    private Messages messages;

    private static final Logger logger = LogManager.getLogger(ServiceUserImpl.class);

    @Override
    public Response findUserByUserNameAndPassword(String userName)
    {
        Response response = new Response();
        try
        {
            logger.info("Start method [findUserByUserNameAndPassword] for [User] via userName [" + userName + "]");

            User user = userDao.findUserByUserName(userName);
            response.setData(user);
            if (user.getUserId() != null)
            {
                response.setStatus(ResponseStatusEnum.SUCCESS);
                response.setDescription(messages.get("baseEntity.find.success"));
            }
            else
            {
                response.setStatus(ResponseStatusEnum.ERROR);
                response.setDescription(messages.get("baseEntity.find.error"));
            }

            logger.info("End successfully method [findUserByUserNameAndPassword] for [User] via userName [" +
                    userName + "]");
        } catch (Exception e)
        {
            logger.error("There is an error in method [findUserByUserNameAndPassword] for [User] via userName [" +
                    userName + "]", e);

            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }

    @Override
    public Response changePassword(ChangePassword changePassword, boolean isAdmin)
    {
        Response response = new Response();
        try
        {
            logger.info("Start method [changePassword] for [User] via id [" +
                        changePassword.getUserId() + "]");

            User user = userDao.findById(changePassword.getUserId());

            if (user.getPassword().equals(UserUtil.getPasswordEncoded(changePassword.getNewPassword())))
            {
                response.setStatus(ResponseStatusEnum.ERROR);
                response.setDescription(messages.get("change.password.duplicate"));
            }
            else if (userDao.changePassword(changePassword, isAdmin))
            {
                UserUtil.editUserCache(user);
                response.setStatus(ResponseStatusEnum.SUCCESS);
                response.setDescription(messages.get("change.password.success"));
            }
            else
            {
                response.setStatus(ResponseStatusEnum.ERROR);
                response.setDescription(messages.get("change.password.error"));
            }

            logger.info("End successfully method [changePassword] for [User] via id [" +
                        changePassword.getUserId() + "]");
        } catch (Exception e)
        {
            logger.error("There is an error in method [changePassword] for [User] via id [" +
                         changePassword.getUserId() + "]", e);

            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }
        return response;
    }

    @Override
    public Response add(User entity)
    {
        Response response = new Response();
        Long newUserId = -1L;
        try
        {
            logger.info("Start method [add] for [User]");

            if (entity.getRoleId() == null ||
                entity.getBranchId() == null)
            {
                response.setStatus(ResponseStatusEnum.ERROR);

                if (entity.getBranchId() == null)
                {
                    response.setDescription(messages.get("validate.user.branch.mandatory"));
                }
                else
                {
                    response.setDescription(messages.get("validate.user.role.mandatory"));
                }
            }
            else
            {
                newUserId = userDao.add(entity);
                entity.setUserId(newUserId);
                UserUtil.addUserCache(entity);
                response.setStatus(ResponseStatusEnum.SUCCESS);
                response.setDescription(messages.get("baseEntity.create.success"));
            }

            response.setData(entity);
            logger.info("End successfully method [add] for [User] with new id [" + newUserId + "]");
        } catch (Exception e)
        {
            logger.error("There is an error in method [add] for [User]", e);

            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }

    @Override
    public Response findById(Long id)
    {
        Response response = new Response();
        try
        {
            logger.info("Start method [findById] for [User] via id [" + id + "]");

            User user = userDao.findById(id);
            response.setData(user);
            if (user.getUserId() != null)
            {
                response.setStatus(ResponseStatusEnum.SUCCESS);
                response.setDescription(messages.get("baseEntity.find.success"));
            }
            else
            {
                response.setStatus(ResponseStatusEnum.ERROR);
                response.setDescription(messages.get("baseEntity.find.error"));
            }

            logger.info("End successfully method [findById] for [User] via id [" + id + "]");
        } catch (Exception e)
        {
            logger.error("There is an error in method [findById] for [User] via id [" + id + "]", e);

            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }

    @Override
    public Response findAll()
    {
        Response response = new Response();
        try
        {
            logger.info("Start method [findAll] for [User]");

            response.setData(userDao.findAll());
            response.setStatus(ResponseStatusEnum.SUCCESS);
            response.setDescription(messages.get("baseEntity.find.success"));

            logger.info("End successfully method [findAll] for [User]");
        } catch (Exception e)
        {
            logger.error("There is an error in method [findAll] for [User]", e);

            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }

    @Override
    public Response delete(Long id)
    {
        Response response = new Response();
        response.setData(null);
        try
        {
            logger.info("Start method [delete] for [User] via id [" + id + "]");

            User user = userDao.findById(id);
            if (userDao.delete(id))
            {
                UserUtil.removeUserCache(user);
                response.setStatus(ResponseStatusEnum.SUCCESS);
                response.setDescription(messages.get("baseEntity.delete.success"));
            }
            else
            {
                response.setStatus(ResponseStatusEnum.ERROR);
                response.setDescription(messages.get("baseEntity.delete.error"));
            }

            logger.info("End successfully method [delete] for [User] via id [" + id + "]");
        } catch (Exception e)
        {
            logger.error("There is an error in method [delete] for [User] via id [" + id + "]", e);

            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }

    @Override
    public Response update(User entity)
    {
        Response response = new Response();
        response.setData(entity);
        try
        {
            logger.info("Start method [update] for [User] via id [" + entity.getUserId() + "]");

            if (entity.getRoleId() == null ||
                entity.getBranchId() == null)
            {
                response.setStatus(ResponseStatusEnum.ERROR);

                if (entity.getBranchId() == null)
                {
                    response.setDescription(messages.get("validate.user.branch.mandatory"));
                }
                else
                {
                    response.setDescription(messages.get("validate.user.role.mandatory"));
                }
            }
            else if (userDao.update(entity))
            {
                UserUtil.editUserCache(entity);
                response.setStatus(ResponseStatusEnum.SUCCESS);
                response.setDescription(messages.get("baseEntity.update.success"));
            }
            else
            {
                response.setStatus(ResponseStatusEnum.ERROR);
                response.setDescription(messages.get("baseEntity.update.error"));
            }

            logger.info("End successfully method [update] for [User] via id [" + entity.getUserId() + "]");
        } catch (Exception e)
        {
            logger.error("There is an error in method [update] for [User] via id [" + entity.getUserId() + "]", e);
            
            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }
}
