package com.karafarin.terminalmanagement.service.impl.setting;

import com.karafarin.terminalmanagement.dao.impl.setting.CompanySettingTerminalDaoImpl;
import com.karafarin.terminalmanagement.dto.setting.CompanySettingTerminal;
import com.karafarin.terminalmanagement.message.Response;
import com.karafarin.terminalmanagement.message.enums.ResponseOwnerEnum;
import com.karafarin.terminalmanagement.message.enums.ResponseStatusEnum;
import com.karafarin.terminalmanagement.model.Messages;
import com.karafarin.terminalmanagement.service.intf.setting.ServiceCompanySettingTerminal;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service
public class ServiceCompanySettingTerminalImpl implements ServiceCompanySettingTerminal
{
    @Autowired
    private Messages messages;
    @Autowired
    private CompanySettingTerminalDaoImpl companySettingTerminalDao;

    private static final Logger logger = LogManager.getLogger(ServiceCompanySettingTerminalImpl.class);
    
    @Override
    public Response add(CompanySettingTerminal entity)
    {
        Response response = new Response();
        try
        {
            logger.info("Start method [add] for [CompanySettingTerminal]");
            
            Long newCompanySettingTerminalId = companySettingTerminalDao.add(entity);
            entity.setCompanySettingTerminalId(newCompanySettingTerminalId);

            response.setStatus(ResponseStatusEnum.SUCCESS);
            response.setData(entity);
            response.setDescription(messages.get("baseEntity.create.success"));

            logger.info("End successfully method [add] for [CompanySettingTerminal] with new id [" + 
                    newCompanySettingTerminalId + "]");            

        } catch (Exception e)
        {
            logger.error("There is an error in method [add] for [CompanySettingTerminal]", e);
            
            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }

    @Override
    public Response findById(Long id)
    {
        Response response = new Response();
        try
        {
            logger.info("Start method [findById] for [CompanySettingTerminal] via id [" + id + "]");
            
            CompanySettingTerminal companySettingTerminal = companySettingTerminalDao.findById(id);
            response.setData(companySettingTerminal);
            if (companySettingTerminal.getCompanySettingTerminalId() != null)
            {
                response.setStatus(ResponseStatusEnum.SUCCESS);
                response.setDescription(messages.get("baseEntity.find.success"));
            }
            else
            {
                response.setStatus(ResponseStatusEnum.ERROR);
                response.setDescription(messages.get("baseEntity.find.error"));
            }

            logger.info("End successfully method [findById] for [CompanySettingTerminal] via id [" + id + "]");            
        } catch (Exception e)
        {
            logger.error("There is an error in method [findById] for [CompanySettingTerminal] via id [" + id + "]", e);
            
            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }

    @Override
    public Response findAll()
    {
        Response response = new Response();
        try
        {
            logger.info("Start method [findAll] for [CompanySettingTerminal]");
            
            response.setData(companySettingTerminalDao.findAll());
            response.setStatus(ResponseStatusEnum.SUCCESS);
            response.setDescription(messages.get("baseEntity.find.success"));

            logger.info("End successfully method [findAll] for [CompanySettingTerminal]");
        } catch (Exception e)
        {
            logger.error("There is an error in method [findAll] for [CompanySettingTerminal]", e);
            
            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }

    @Override
    public Response delete(Long id)
    {
        return null;
    }

    @Override
    public Response update(CompanySettingTerminal entity)
    {
        Response response = new Response();
        response.setData(entity);
        try
        {
            logger.info("Start method [update] for [CompanySettingTerminal] via id [" + entity.getCompanySettingTerminalId() + "]");
            
            if (companySettingTerminalDao.update(entity))
            {
                response.setStatus(ResponseStatusEnum.SUCCESS);
                response.setDescription(messages.get("baseEntity.update.success"));
            }
            else
            {
                response.setStatus(ResponseStatusEnum.ERROR);
                response.setDescription(messages.get("baseEntity.update.error"));
            }

            logger.info("End successfully method [update] for [CompanySettingTerminal] via id [" + 
                    entity.getCompanySettingTerminalId() + "]");            
        } catch (Exception e)
        {
            logger.error("There is an error in method [update] for [CompanySettingTerminal] via id [" +
                    entity.getCompanySettingTerminalId() + "]", e);
            
            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }

    @Override
    public Response findByCompanySettingId(Long id)
    {
        Response response = new Response();
        try
        {
            logger.info("Start method [findByCompanySettingId] for [CompanySettingTerminal] via id [" + id + "]");
            
            response.setData(companySettingTerminalDao.findByCompanySettingId(id));
            response.setStatus(ResponseStatusEnum.SUCCESS);
            response.setDescription(messages.get("baseEntity.find.success"));

            logger.info("End successfully method [findByCompanySettingId] for [CompanySettingTerminal] via id [" + id + "]");            
        } catch (Exception e)
        {
            logger.error("There is an error in method [findByCompanySettingId] for [CompanySettingTerminal] via id [" + id + "]", e);
            
            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }

}
