package com.karafarin.terminalmanagement.service.impl.setting;

import com.karafarin.terminalmanagement.dao.impl.setting.CompanySettingServiceDaoImpl;
import com.karafarin.terminalmanagement.dto.setting.CompanySettingService;
import com.karafarin.terminalmanagement.message.Response;
import com.karafarin.terminalmanagement.message.enums.ResponseOwnerEnum;
import com.karafarin.terminalmanagement.message.enums.ResponseStatusEnum;
import com.karafarin.terminalmanagement.model.Messages;
import com.karafarin.terminalmanagement.service.intf.setting.ServiceCompanySettingService;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service
public class ServiceCompanySettingServiceImpl implements ServiceCompanySettingService
{
    @Autowired
    private Messages messages;
    @Autowired
    private CompanySettingServiceDaoImpl companySettingServiceDao;

    private static final Logger logger = LogManager.getLogger(ServiceCompanySettingServiceImpl.class);
    
    @Override
    public Response add(CompanySettingService entity)
    {
        Response response = new Response();
        try
        {
            logger.info("Start method [add] for [CompanySettingService]");
            
            Long newCompanySettingServiceId = companySettingServiceDao.add(entity);
            entity.setCompanySettingServiceId(newCompanySettingServiceId);

            response.setStatus(ResponseStatusEnum.SUCCESS);
            response.setData(entity);
            response.setDescription(messages.get("baseEntity.create.success"));

            logger.info("End successfully method [add] for [CompanySettingService] with new id [" +
                    newCompanySettingServiceId + "]");

        } catch (Exception e)
        {
            logger.error("There is an error in method [add] for [CompanySettingService]", e);
            
            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }

    @Override
    public Response findById(Long id)
    {
        Response response = new Response();
        try
        {
            logger.info("Start method [findById] for [CompanySettingService] via id [" + id + "]");
            
            CompanySettingService companySettingService = companySettingServiceDao.findById(id);
            response.setData(companySettingService);
            if (companySettingService.getCompanySettingServiceId() != null)
            {
                response.setStatus(ResponseStatusEnum.SUCCESS);
                response.setDescription(messages.get("baseEntity.find.success"));
            }
            else
            {
                response.setStatus(ResponseStatusEnum.ERROR);
                response.setDescription(messages.get("baseEntity.find.error"));
            }

            logger.info("End successfully method [findById] for [CompanySettingService] via id [" + id + "]");            
        } catch (Exception e)
        {
            logger.error("There is an error in method [findById] for [CompanySettingService] via id [" + id + "]", e);
            
            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }

    @Override
    public Response findAll()
    {
        Response response = new Response();
        try
        {
            logger.info("Start method [findAll] for [CompanySettingService]");
            
            response.setData(companySettingServiceDao.findAll());
            response.setStatus(ResponseStatusEnum.SUCCESS);
            response.setDescription(messages.get("baseEntity.find.success"));

            logger.info("End successfully method [findAll] for [CompanySettingService]");            

        } catch (Exception e)
        {
            logger.error("There is an error in method [findAll] for [CompanySettingService]", e);
            
            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }

    @Override
    public Response delete(Long id)
    {
        return null;
    }

    @Override
    public Response update(CompanySettingService entity)
    {
        Response response = new Response();
        response.setData(entity);
        try
        {
            logger.info("Start method [update] for [CompanySettingService] via id [" + entity.getCompanySettingServiceId() + "]");
            
            boolean updateResult = companySettingServiceDao.update(entity);
            if (updateResult)
            {
                response.setStatus(ResponseStatusEnum.SUCCESS);
                response.setDescription(messages.get("baseEntity.update.success"));
            }
            else
            {
                response.setStatus(ResponseStatusEnum.ERROR);
                response.setDescription(messages.get("baseEntity.update.error"));
            }

            logger.info("End successfully method [update] for [CompanySettingService] via id [" +
                    entity.getCompanySettingServiceId() + "]");
        } catch (Exception e)
        {
            logger.error("There is an error in method [update] for [CompanySettingService] via id [" +
                    entity.getCompanySettingServiceId() + "]", e);
            
            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }

    @Override
    public Response findByCompanySettingId(Long id)
    {
        Response response = new Response();
        try
        {
            logger.info("Start method [findByCompanySettingId] for [CompanySettingService] via id [" + id + "]");
            
            response.setData(companySettingServiceDao.findByCompanySettingId(id));
            response.setStatus(ResponseStatusEnum.SUCCESS);
            response.setDescription(messages.get("baseEntity.find.success"));

            logger.info("End successfully method [findByCompanySettingId] for [CompanySettingService] via id [" +
                    id + "]");
        } catch (Exception e)
        {
            logger.error("There is an error in method [findByCompanySettingId] for [CompanySettingService] via id [" +
                    id + "]", e);
            
            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }
}
