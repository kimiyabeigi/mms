package com.karafarin.terminalmanagement.service.impl.sys;

import com.karafarin.terminalmanagement.dao.impl.sys.SysServiceDaoImpl;
import com.karafarin.terminalmanagement.dto.sys.SysService;
import com.karafarin.terminalmanagement.message.Response;
import com.karafarin.terminalmanagement.message.enums.ResponseOwnerEnum;
import com.karafarin.terminalmanagement.message.enums.ResponseStatusEnum;
import com.karafarin.terminalmanagement.model.Messages;
import com.karafarin.terminalmanagement.service.intf.sys.ServiceSysService;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service
public class ServiceSysServiceImpl implements ServiceSysService
{
    @Autowired
    private SysServiceDaoImpl sysServiceDao;
    @Autowired
    private Messages messages;

    private static final Logger logger = LogManager.getLogger(ServiceSysServiceImpl.class);
    
    @Override
    public Response add(SysService entity)
    {
        return null;
    }

    @Override
    public Response findById(Long id)
    {
        Response response = new Response();
        try
        {
            logger.info("Start method [findById] for [SysService] via id [" + id + "]");
            
            SysService sysService = sysServiceDao.findById(id);
            response.setData(sysService);
            if (sysService.getServiceId() != null)
            {
                response.setStatus(ResponseStatusEnum.SUCCESS);
                response.setDescription(messages.get("baseEntity.find.success"));
            }
            else
            {
                response.setStatus(ResponseStatusEnum.ERROR);
                response.setDescription(messages.get("baseEntity.find.error"));
            }

            logger.info("End successfully method [findById] for [SysService] via id [" + id + "]");            
        } catch (Exception e)
        {
            logger.error("There is an error in method [findById] for [SysService] via id [" + id + "]", e);
            
            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }

    @Override
    public Response findAll()
    {
        Response response = new Response();
        try
        {
            logger.info("Start method [findAll] for [SysService]");
            
            response.setData(sysServiceDao.findAll());
            response.setStatus(ResponseStatusEnum.SUCCESS);
            response.setDescription(messages.get("baseEntity.find.success"));

            logger.info("End successfully method [findAll] for [SysService]");
        } catch (Exception e)
        {
            logger.error("There is an error in method [findAll] for [SysService]", e);
            
            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }

    @Override
    public Response update(SysService entity)
    {
        return null;
    }

    @Override
    public Response delete(Long id)
    {
        return null;
    }
}
