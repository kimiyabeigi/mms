package com.karafarin.terminalmanagement.service.impl.base;

import com.karafarin.terminalmanagement.dao.impl.base.BaseGuildDaoImpl;
import com.karafarin.terminalmanagement.dto.base.BaseGuild;
import com.karafarin.terminalmanagement.message.Response;
import com.karafarin.terminalmanagement.message.enums.ResponseOwnerEnum;
import com.karafarin.terminalmanagement.message.enums.ResponseStatusEnum;
import com.karafarin.terminalmanagement.model.Messages;
import com.karafarin.terminalmanagement.service.intf.base.ServiceBaseGuild;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service
public class ServiceBaseGuildImpl implements ServiceBaseGuild
{
    @Autowired
    private BaseGuildDaoImpl baseGuildDao;
    @Autowired
    private Messages messages;

    private static final Logger logger = LogManager.getLogger(ServiceBaseGuildImpl.class);

    @Override
    public Response add(BaseGuild entity) {

        Response response = new Response();
        try
        {
            logger.info("Start method [add] for [BaseGuild]");

            Long newGuildId = baseGuildDao.add(entity);
            entity.setGuildId(newGuildId);
            response.setStatus(ResponseStatusEnum.SUCCESS);
            response.setData(entity);
            response.setDescription(messages.get("baseEntity.create.success"));

            logger.info("End successfully method [add] for [BaseGuild] with new id [" + newGuildId + "]");

        } catch (Exception e)
        {
            logger.error("There is an error in method [add] for [BaseGuild]", e);

            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }

    @Override
    public Response findById(Long id) {

        Response response = new Response();
        try
        {
            logger.info("Start method [findById] for [BaseGuild] via id [" + id + "]");

            BaseGuild baseGuild = baseGuildDao.findById(id);
            response.setData(baseGuild);
            if (baseGuild.getGuildId() != null)
            {
                response.setStatus(ResponseStatusEnum.SUCCESS);
                response.setDescription(messages.get("baseEntity.find.success"));
            }
            else
            {
                response.setStatus(ResponseStatusEnum.ERROR);
                response.setDescription(messages.get("baseEntity.find.error"));
            }

            logger.info("End successfully method [findById] for [BaseGuild] via id [" + id + "]");
        } catch (Exception e)
        {
            logger.error("There is an error in method [findById] for [BaseGuild] via id [" + id + "]", e);

            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }

    @Override
    public Response findAll()
    {
        return null;
    }

    @Override
    public Response findAll(boolean all) {

        Response response = new Response();
        try
        {
            logger.info("Start method [findAll] for [BaseGuild]");

            response.setData(baseGuildDao.findAll(all));
            response.setStatus(ResponseStatusEnum.SUCCESS);
            response.setDescription(messages.get("baseEntity.find.success"));

            logger.info("End successfully method [findAll] for [BaseGuild]");

        } catch (Exception e)
        {
            logger.error("There is an error in method [findAll] for [BaseGuild]", e);

            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;

    }

    @Override
    public Response delete(Long id) {

        Response response = new Response();
        response.setData(null);
        try
        {
            logger.info("Start method [delete] for [BaseGuild] via id [" + id + "]");

            if (baseGuildDao.delete(id))
            {
                response.setStatus(ResponseStatusEnum.SUCCESS);
                response.setDescription(messages.get("baseEntity.delete.success"));
            }
            else
            {
                response.setStatus(ResponseStatusEnum.ERROR);
                response.setDescription(messages.get("baseEntity.delete.error"));
            }

            logger.info("End successfully method [delete] for [BaseGuild] via id [" + id + "]");
        } catch (Exception e)
        {
            logger.error("There is an error in method [delete] for [BaseGuild] via id [" + id + "]", e);

            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }

    @Override
    public Response update(BaseGuild entity) {

        Response response = new Response();
        response.setData(entity);
        try
        {
            logger.info("Start method [update] for [BaseGuild] via id [" + entity.getGuildId() + "]");

            if (baseGuildDao.update(entity))
            {
                response.setStatus(ResponseStatusEnum.SUCCESS);
                response.setDescription(messages.get("baseEntity.update.success"));
            }
            else
            {
                response.setStatus(ResponseStatusEnum.ERROR);
                response.setDescription(messages.get("baseEntity.update.error"));
            }

            logger.info("End successfully method [update] for [BaseGuild] via id [" + entity.getGuildId() + "]");
        } catch (Exception e)
        {
            logger.error("There is an error in method [update] for [BaseGuild] via id [" +
                    entity.getGuildId() + "]", e);

            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;

    }
}
