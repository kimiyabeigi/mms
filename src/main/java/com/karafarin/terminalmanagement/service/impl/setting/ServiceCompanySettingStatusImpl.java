package com.karafarin.terminalmanagement.service.impl.setting;

import com.karafarin.terminalmanagement.dao.impl.setting.CompanySettingStatusDaoImpl;
import com.karafarin.terminalmanagement.dto.setting.CompanySettingStatus;
import com.karafarin.terminalmanagement.message.Response;
import com.karafarin.terminalmanagement.message.enums.ResponseOwnerEnum;
import com.karafarin.terminalmanagement.message.enums.ResponseStatusEnum;
import com.karafarin.terminalmanagement.model.Messages;
import com.karafarin.terminalmanagement.service.intf.setting.ServiceCompanySettingStatus;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service
public class ServiceCompanySettingStatusImpl implements ServiceCompanySettingStatus
{
    @Autowired
    private Messages messages;
    @Autowired
    private CompanySettingStatusDaoImpl companySettingStatusDao;

    private static final Logger logger = LogManager.getLogger(ServiceCompanySettingStatusImpl.class);

    @Override
    public Response add(CompanySettingStatus entity)
    {
        Response response = new Response();
        try
        {
            logger.info("Start method [add] for [CompanySettingStatus]");
            
            Long newCompanySettingStatusId = companySettingStatusDao.add(entity);
            entity.setCompanySettingId(newCompanySettingStatusId);

            response.setStatus(ResponseStatusEnum.SUCCESS);
            response.setData(entity);
            response.setDescription(messages.get("baseEntity.create.success"));

            logger.info("End successfully method [add] for [CompanySettingStatus] with new id [" +
                    newCompanySettingStatusId + "]");

        } catch (Exception e)
        {
            logger.error("There is an error in method [add] for [CompanySettingStatus]", e);
            
            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }

    @Override
    public Response findById(Long id)
    {
        Response response = new Response();
        try
        {
            logger.info("Start method [findById] for [CompanySettingStatus] via id [" + id + "]");
            
            CompanySettingStatus companySettingStatus = companySettingStatusDao.findById(id);
            response.setData(companySettingStatus);
            if (companySettingStatus.getCompanySettingStatusId() != null)
            {
                response.setStatus(ResponseStatusEnum.SUCCESS);
                response.setDescription(messages.get("baseEntity.find.success"));
            }
            else
            {
                response.setStatus(ResponseStatusEnum.ERROR);
                response.setDescription(messages.get("baseEntity.find.error"));
            }

            logger.info("End successfully method [findById] for [CompanySettingStatus] via id [" + id + "]");            
        } catch (Exception e)
        {
            logger.error("There is an error in method [findById] for [CompanySettingStatus] via id [" + id + "]", e);
            
            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }

    @Override
    public Response findAll()
    {
        Response response = new Response();
        try
        {
            logger.info("Start method [findAll] for [CompanySettingStatus]");
            
            response.setData(companySettingStatusDao.findAll());
            response.setStatus(ResponseStatusEnum.SUCCESS);
            response.setDescription(messages.get("baseEntity.find.success"));

            logger.info("End successfully method [findAll] for [CompanySettingStatus]");
        } catch (Exception e)
        {
            logger.error("There is an error in method [findAll] for [CompanySettingStatus]", e);
            
            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }

    @Override
    public Response delete(Long id)
    {
        return null;
    }

    @Override
    public Response update(CompanySettingStatus entity)
    {
        Response response = new Response();
        response.setData(entity);
        try
        {
            logger.info("Start method [update] for [CompanySettingStatus] via id [" + entity.getCompanySettingStatusId() + "]");
            
            boolean updateResult = companySettingStatusDao.update(entity);
            if (updateResult)
            {
                response.setStatus(ResponseStatusEnum.SUCCESS);
                response.setDescription(messages.get("baseEntity.update.success"));
            }
            else
            {
                response.setStatus(ResponseStatusEnum.ERROR);
                response.setDescription(messages.get("baseEntity.update.error"));
            }

            logger.info("End successfully method [update] for [CompanySettingStatus] via id [" +
                    entity.getCompanySettingStatusId() + "]");
        } catch (Exception e)
        {
            logger.error("There is an error in method [update] for [CompanySettingStatus] via id [" +
                    entity.getCompanySettingStatusId() + "]", e);
            
            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }

    @Override
    public Response findByCompanySettingId(Long id)
    {
        Response response = new Response();
        try
        {
            logger.info("Start method [findByCompanySettingId] for [CompanySettingStatus] via id [" + id + "]");
            
            response.setData(companySettingStatusDao.findByCompanySettingId(id));
            response.setStatus(ResponseStatusEnum.SUCCESS);
            response.setDescription(messages.get("baseEntity.find.success"));

            logger.info("End successfully method [findByCompanySettingId] for [CompanySettingStatus] via id [" + id + "]");

        } catch (Exception e)
        {
            logger.error("There is an error in method [findByCompanySettingId] for [CompanySettingStatus] via id [" +
                    id + "]", e);
            
            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }
}
