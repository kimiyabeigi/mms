package com.karafarin.terminalmanagement.service.impl.psp;

import com.karafarin.terminalmanagement.dao.impl.psp.PSPReportRegisterDetailImpl;
import com.karafarin.terminalmanagement.dao.impl.psp.PSPReportRegisterMasterImpl;
import com.karafarin.terminalmanagement.dto.psp.PSPReportRegisterDetail;
import com.karafarin.terminalmanagement.message.Response;
import com.karafarin.terminalmanagement.message.enums.ResponseOwnerEnum;
import com.karafarin.terminalmanagement.message.enums.ResponseStatusEnum;
import com.karafarin.terminalmanagement.model.Messages;
import com.karafarin.terminalmanagement.model.psp.ParamsReportMaster;
import com.karafarin.terminalmanagement.service.intf.psp.ServiceReportRegister;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ServiceReportRegisterImpl implements ServiceReportRegister
{
    private static final Logger logger = LogManager.getLogger(ServiceReportRegisterImpl.class);

    @Autowired
    private PSPReportRegisterMasterImpl pspReportRegisterMaster;
    @Autowired
    private Messages messages;
    @Autowired
    private PSPReportRegisterDetailImpl pspReportRegisterDetail;

    @Override
    public Response masterFindAll(ParamsReportMaster paramsReportMaster)
    {
        Response response = new Response();
        try
        {
            logger.info("Start method [masterFindAll] for [PSPReportRegisterMaster]");

            response.setData(pspReportRegisterMaster.findAll(paramsReportMaster));
            response.setStatus(ResponseStatusEnum.SUCCESS);
            response.setDescription(messages.get("baseEntity.find.success"));

            logger.info("End successfully method [masterFindAll] for [PSPReportRegisterMaster]");
        } catch (Exception e)
        {
            logger.error("There is an error in method [masterFindAll] for [PSPReportRegisterMaster]", e);

            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }

    @Override
    public Response detailFindAll(Long pspRequestId)
    {
        Response response = new Response();
        try
        {
            logger.info("Start method [detailFindAll] for [PSPReportRegisterDetail]");

            response.setData(pspReportRegisterDetail.findAll(pspRequestId));
            response.setStatus(ResponseStatusEnum.SUCCESS);
            response.setDescription(messages.get("baseEntity.find.success"));

            logger.info("End successfully method [detailFindAll] for [PSPReportRegisterDetail]");
        } catch (Exception e)
        {
            logger.error("There is an error in method [detailFindAll] for [PSPReportRegisterDetail]", e);

            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }

}
