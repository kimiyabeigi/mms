package com.karafarin.terminalmanagement.service.impl.sys;

import com.karafarin.terminalmanagement.dao.impl.sys.SysTerminalDaoImpl;
import com.karafarin.terminalmanagement.dto.sys.SysTerminal;
import com.karafarin.terminalmanagement.message.Response;
import com.karafarin.terminalmanagement.message.enums.ResponseOwnerEnum;
import com.karafarin.terminalmanagement.message.enums.ResponseStatusEnum;
import com.karafarin.terminalmanagement.model.Messages;
import com.karafarin.terminalmanagement.service.intf.sys.ServiceSysTerminal;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service
public class ServiceSysTerminalImpl implements ServiceSysTerminal
{
    @Autowired
    private SysTerminalDaoImpl sysTerminalDao;
    @Autowired
    private Messages messages;

    private static final Logger logger = LogManager.getLogger(ServiceSysTerminalImpl.class);
    
    @Override
    public Response add(SysTerminal entity) {
        return null;
    }

    @Override
    public Response findById(Long id) {
        Response response = new Response();
        try
        {
            logger.info("Start method [findById] for [SysTerminal] via id [" + id + "]");
            
            SysTerminal sysTerminal = sysTerminalDao.findById(id);
            response.setData(sysTerminal);
            if (sysTerminal.getTerminalId() != null)
            {
                response.setStatus(ResponseStatusEnum.SUCCESS);
                response.setDescription(messages.get("baseEntity.find.success"));
            }
            else
            {
                response.setStatus(ResponseStatusEnum.ERROR);
                response.setDescription(messages.get("baseEntity.find.error"));
            }

            logger.info("End successfully method [findById] for [SysTerminal] via id [" + id + "]");            
        } catch (Exception e)
        {
            logger.error("There is an error in method [findById] for [SysTerminal] via id [" + id + "]", e);
            
            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }

    @Override
    public Response findAll() {
        Response response = new Response();
        try
        {
            logger.info("Start method [findAll] for [SysTerminal]");
            
            response.setData(sysTerminalDao.findAll());
            response.setStatus(ResponseStatusEnum.SUCCESS);
            response.setDescription(messages.get("baseEntity.find.success"));

            logger.info("End successfully method [findAll] for [SysTerminal]");            
        } catch (Exception e)
        {
            logger.error("There is an error in method [findAll] for [SysTerminal]", e);
            
            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }

    @Override
    public Response findTerminalsByFilter(Long hideId)
    {
        Response response = new Response();
        try
        {
            logger.info("Start method [findTerminalsByFilter] for [SysTerminal]");

            response.setData(sysTerminalDao.findTerminalsByFilter(hideId));
            response.setStatus(ResponseStatusEnum.SUCCESS);
            response.setDescription(messages.get("baseEntity.find.success"));

            logger.info("End successfully method [findTerminalsByFilter] for [SysTerminal]");
        } catch (Exception e)
        {
            logger.error("There is an error in method [findTerminalsByFilter] for [SysTerminal]", e);

            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }

    @Override
    public Response update(SysTerminal entity)
    {
        return null;
    }

    @Override
    public Response delete(Long id) {
        return null;
    }

    @Override
    public Response findTerminalsRequest(Long companyId)
    {
        Response response = new Response();
        try
        {
            logger.info("Start method [findTerminalsRequest] for [SysTerminal] via companyId [" + companyId + "]");
            
            response.setData(sysTerminalDao.findTerminalsRequest(companyId));
            response.setStatus(ResponseStatusEnum.SUCCESS);
            response.setDescription(messages.get("baseEntity.find.success"));

            logger.info("End successfully method [findTerminalsRequest] for [SysTerminal] via companyId [" + companyId + "]");
        } catch (Exception e)
        {
            logger.error("There is an error in method [findTerminalsRequest] for [SysTerminal] via companyId [" +
                    companyId + "]", e);
            
            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }
}
