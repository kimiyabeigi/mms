package com.karafarin.terminalmanagement.service.impl.auth;

import com.karafarin.terminalmanagement.dao.impl.auth.RoleFormDaoImpl;
import com.karafarin.terminalmanagement.dto.auth.RoleForm;
import com.karafarin.terminalmanagement.message.Response;
import com.karafarin.terminalmanagement.message.enums.ResponseOwnerEnum;
import com.karafarin.terminalmanagement.message.enums.ResponseStatusEnum;
import com.karafarin.terminalmanagement.model.Messages;
import com.karafarin.terminalmanagement.service.intf.auth.ServiceRoleForm;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ServiceRoleFormImpl implements ServiceRoleForm
{
    @Autowired
    private RoleFormDaoImpl roleFormDao;
    @Autowired
    private Messages messages;

    private static final Logger logger = LogManager.getLogger(ServiceRoleFormImpl.class);

    @Override
    public Response add(RoleForm entity)
    {
        return null;
    }

    @Override
    public Response findById(Long id)
    {
        Response response = new Response();
        try
        {
            logger.info("Start method [findById] for [RoleForm] via id [" + id + "]");

            RoleForm roleForm = roleFormDao.findById(id);
            response.setData(roleForm);
            if (roleForm.getRoleId() != null)
            {
                response.setStatus(ResponseStatusEnum.SUCCESS);
                response.setDescription(messages.get("baseEntity.find.success"));
            }
            else
            {
                response.setStatus(ResponseStatusEnum.ERROR);
                response.setDescription(messages.get("baseEntity.find.error"));
            }

            logger.info("End successfully method [findById] for [RoleForm] via id [" + id + "]");
        } catch (Exception e)
        {
            logger.error("There is an error in method [findById] for [RoleForm] via id [" + id + "]", e);

            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }

    @Override
    public Response findAll()
    {
        Response response = new Response();
        try
        {
            logger.info("Start method [findAll] for [RoleForm]");

            response.setData(roleFormDao.findAll());
            response.setStatus(ResponseStatusEnum.SUCCESS);
            response.setDescription(messages.get("baseEntity.find.success"));

            logger.info("End successfully method [findAll] for [RoleForm]");
        } catch (Exception e)
        {
            logger.error("There is an error in method [findAll] for [RoleForm]", e);

            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }

    @Override
    public Response delete(Long id)
    {
        return null;
    }

    @Override
    public Response update(RoleForm entity)
    {
        return null;
    }

    @Override
    public Response addList(List<RoleForm> roleForms)
    {
        Response response = new Response();
        try
        {
            logger.info("Start method [add] for [RoleForm]");

            for (RoleForm roleForm: roleForms)
            {
                Long newRoleFormId = roleFormDao.add(roleForm);
                roleForm.setRoleId(newRoleFormId);

                logger.info("End successfully method [add] for [RoleForm] with new id [" + newRoleFormId + "]");
            }

            response.setStatus(ResponseStatusEnum.SUCCESS);
            response.setData(roleForms);
            response.setDescription(messages.get("baseEntity.create.success"));

            logger.info("End method [add] for [RoleForm]");
        } catch (Exception e)
        {
            logger.error("There is an error in method [add] for [RoleForm]", e);

            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }

    @Override
    public Response updateList(List<RoleForm> roleForms)
    {
        Response response = new Response();
        logger.info("Start method [updateList] for [RoleForm]");

        try
        {
            for (RoleForm roleform : roleForms)
            {
                if (roleform.getRoleFormId() != null && !roleform.isSelected())
                {
                    roleFormDao.delete(roleform.getRoleFormId());
                    logger.info("End successfully delete in method [updateList] via id [" +
                               roleform.getRoleFormId() + "]");
                } else if (roleform.getRoleFormId() == null && roleform.isSelected())
                {
                    Long newRoleFormId = roleFormDao.add(roleform);
                    roleform.setRoleFormId(newRoleFormId);
                    logger.info("End successfully add in method [updateList] via id [" +
                                roleform.getRoleFormId() + "]");
                }
            }
            response.setStatus(ResponseStatusEnum.SUCCESS);
            response.setData(roleForms);
            response.setDescription(messages.get("baseEntity.update.success"));

            logger.info("End method [updateList] for [RoleForm]");
        } catch (Exception e)
        {
            logger.error("There is an error in method [updateList] for [RoleForm]", e);

            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }

    @Override
    public Response findByRoleId(Long id)
    {
        Response response = new Response();
        try
        {
            logger.info("Start method [findByRoleId] for [RoleForm] via RoleId [" + id + "]");

            List<RoleForm> roleForms = roleFormDao.findByRoleId(id);
            response.setData(roleForms);
            if (roleForms.size() > 0)
            {
                response.setStatus(ResponseStatusEnum.SUCCESS);
                response.setDescription(messages.get("baseEntity.find.success"));
            }
            else
            {
                response.setStatus(ResponseStatusEnum.ERROR);
                response.setDescription(messages.get("baseEntity.find.error"));
            }

            logger.info("End successfully method [findByRoleId] for [RoleForm] via RoleId [" + id + "]");
        } catch (Exception e)
        {
            logger.error("There is an error in method [findByRoleId] for [RoleForm] via RoleId [" + id + "]", e);

            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }
}
