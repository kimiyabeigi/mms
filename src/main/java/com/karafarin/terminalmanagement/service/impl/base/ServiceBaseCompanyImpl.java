package com.karafarin.terminalmanagement.service.impl.base;

import com.karafarin.terminalmanagement.dao.impl.base.BaseCompanyDaoImpl;
import com.karafarin.terminalmanagement.dto.base.BaseCompany;
import com.karafarin.terminalmanagement.message.Response;
import com.karafarin.terminalmanagement.message.enums.ResponseOwnerEnum;
import com.karafarin.terminalmanagement.message.enums.ResponseStatusEnum;
import com.karafarin.terminalmanagement.model.Messages;
import com.karafarin.terminalmanagement.service.intf.base.ServiceBaseCompany;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ServiceBaseCompanyImpl implements ServiceBaseCompany
{
    @Autowired
    BaseCompanyDaoImpl baseCompanyDao;
    @Autowired
    Messages messages;

    private static final Logger logger = LogManager.getLogger(ServiceBaseCompanyImpl.class);

    @Override
    public Response add(BaseCompany entity)
    {
        Response response = new Response();
        try
        {
            logger.info("Start method of add for [BaseCompany]");

            Long newCompanyId = baseCompanyDao.add(entity);
            entity.setCompanyId(newCompanyId);
            response.setStatus(ResponseStatusEnum.SUCCESS);
            response.setData(entity);
            response.setDescription(messages.get("baseEntity.create.success"));

            logger.info("End successfully method of add for [BaseCompany] with new id [" + newCompanyId + "]");

        } catch (Exception e)
        {
            logger.error("There is an error in method of add for [BaseCompany]", e);

            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }

    @Override
    public Response findById(Long id)
    {
        Response response = new Response();
        try
        {
            logger.info("Start method of findById for [BaseCompany] via id [" + id + "]");

            BaseCompany baseCompany = baseCompanyDao.findById(id);
            response.setData(baseCompany);
            if (baseCompany.getCompanyId() != null)
            {
                response.setStatus(ResponseStatusEnum.SUCCESS);
                response.setDescription(messages.get("baseEntity.find.success"));
            }
            else
            {
                response.setStatus(ResponseStatusEnum.ERROR);
                response.setDescription(messages.get("baseEntity.find.error"));
            }

            logger.info("End successfully method of findById for [BaseCompany] via id [" + id + "]");
        } catch (Exception e)
        {
            logger.error("There is an error in method of findById for [BaseCompany] via id [" + id + "]", e);

            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }

    @Override
    public Response findAll()
    {
        Response response = new Response();
        try
        {
            logger.info("Start method of findAll for [BaseCompany]");

            response.setData(baseCompanyDao.findAll());
            response.setStatus(ResponseStatusEnum.SUCCESS);
            response.setDescription(messages.get("baseEntity.find.success"));

            logger.info("End successfully method of findAll for [BaseCompany]");

        } catch (Exception e)
        {
            logger.error("There is an error in method of findAll for [BaseCompany]", e);

            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }

    @Override
    public Response update(BaseCompany entity)
    {
        Response response = new Response();
        response.setData(entity);
        try
        {
            logger.info("Start method of update for [BaseCompany] via id [" + entity.getCompanyId() + "]");

            if (baseCompanyDao.update(entity))
            {
                response.setStatus(ResponseStatusEnum.SUCCESS);
                response.setDescription(messages.get("baseEntity.update.success"));
            }
            else
            {
                response.setStatus(ResponseStatusEnum.ERROR);
                response.setDescription(messages.get("baseEntity.update.error"));
            }

            logger.info("End successfully method of update for [BaseCompany] via id [" +
                    entity.getCompanyId() + "]");

        } catch (Exception e)
        {
            logger.error("There is an error in method of update for [BaseCompany] via id [" +
                    entity.getCompanyId() + "]", e);

            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }

    @Override
    public Response search(String companyCode, String name)
    {
        Response response = new Response();
        try
        {
            logger.info("Start method of search for [BaseCompany] via companyCode [" +
                    companyCode + "] and name [" + name + "]");

            BaseCompany baseCompany = new BaseCompany();
            baseCompany.setCompanyCode(companyCode);
            baseCompany.setName(name);

            List<BaseCompany> baseCompanies = baseCompanyDao.search(baseCompany);
            response.setData(baseCompanies);
            response.setStatus(ResponseStatusEnum.SUCCESS);
            response.setDescription(messages.get("baseEntity.find.success"));

            logger.info("End successfully method of search for [BaseCompany] via companyCode [" +
                    companyCode + "] and name [" + name + "]");
        } catch (Exception e)
        {
            logger.error("There is an error in method of search for [BaseCompany] via companyCode [" +
                    companyCode + "] and name [" + name + "]", e);

            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }

    @Override
    public Response findActiveCompaniesForRequest()
    {
        Response response = new Response();
        try
        {
            logger.info("Start method of findActiveCompaniesForRequest for [BaseCompany]");

            response.setData(baseCompanyDao.findActiveCompaniesForRequest());
            response.setStatus(ResponseStatusEnum.SUCCESS);
            response.setDescription(messages.get("baseEntity.find.success"));

            logger.info("End successfully method of findActiveCompaniesForRequest for [BaseCompany]");
        } catch (Exception e)
        {
            logger.error("There is an error in method of findActiveCompaniesForRequest for [BaseCompany]", e);

            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }

    @Override
    public Response delete(Long id)
    {
        Response response = new Response();
        response.setData(null);
        try
        {
            logger.info("Start method [delete] for [BaseCompany] via id [" + id + "]");

            if (baseCompanyDao.delete(id))
            {
                response.setStatus(ResponseStatusEnum.SUCCESS);
                response.setDescription(messages.get("baseEntity.delete.success"));
            }
            else
            {
                response.setStatus(ResponseStatusEnum.ERROR);
                response.setDescription(messages.get("baseEntity.delete.error"));
            }

            logger.info("End successfully method [delete] for [BaseCompany] via id [" + id + "]");
        } catch (Exception e)
        {
            logger.error("There is an error in method [delete] for [BaseCompany] via id [" + id + "]", e);

            response.setDescription(messages.getExceptionDescription(e, ResponseOwnerEnum.SYSTEM));
            response.setStatus(ResponseStatusEnum.ERROR);

            e.printStackTrace();
        }

        return response;
    }

}
