package com.karafarin.terminalmanagement.viewresolver;

import com.karafarin.terminalmanagement.dto.psp.PSPReportRegisterDetail;
import com.karafarin.terminalmanagement.dto.psp.PSPReportRegisterMaster;
import com.karafarin.terminalmanagement.util.UserUtil;
import com.karafarin.terminalmanagement.util.date.DateConverterUtil;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.*;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.servlet.view.document.AbstractXlsView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.nio.file.Files;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class ExcelView extends AbstractXlsView
{
    private static final Logger logger = LogManager.getLogger(ExcelView.class);

    @Override
    protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request,
                                      HttpServletResponse response) throws Exception
    {
        logger.info("Start of method [buildExcelDocument] via userId [" +
                    UserUtil.getCurrentUser().getUserId() + "]");
        // change the file name
        response.setHeader("Content-Disposition", "attachment; filename=\"mms-report.xls\"");
        int logoColumn = 5;
        int columnWidth = 10;

        @SuppressWarnings("unchecked") List<PSPReportRegisterDetail> details =
                (List<PSPReportRegisterDetail>) model.get("details");
        @SuppressWarnings("unchecked") List<PSPReportRegisterMaster> masters =
                (List<PSPReportRegisterMaster>) model.get("masters");
        @SuppressWarnings("unchecked") Map<String, String> message = (Map<String, String>) model.get("message");

        if (details != null)
        {
            logoColumn = 6;
            columnWidth = 18;
        } else if (masters != null)
        {
            logoColumn = 8;
            columnWidth = 10;
        }

        // create excel xls sheet
        Sheet sheet = workbook.createSheet("MMS");
        sheet.setDefaultColumnWidth(columnWidth);
        sheet.getPrintSetup().setLandscape(true);
        sheet.setMargin(Sheet.LeftMargin, 0.25);
        sheet.setMargin(Sheet.RightMargin, 0.25);
        sheet.setMargin(Sheet.TopMargin, 0.75);
        sheet.setMargin(Sheet.BottomMargin, 0.75);
        sheet.setMargin(Sheet.HeaderMargin, 0.3);
        sheet.setMargin(Sheet.FooterMargin, 0.3);

        File logo = new ClassPathResource("/static/images/logo-karafarin.jpg").getFile();
        final CreationHelper helper = workbook.getCreationHelper();
        final Drawing drawing = sheet.createDrawingPatriarch();
        final ClientAnchor anchor = helper.createClientAnchor();
        anchor.setAnchorType(ClientAnchor.MOVE_AND_RESIZE);
        final int pictureIndex = workbook.addPicture(Files.readAllBytes(logo.toPath()), Workbook.PICTURE_TYPE_JPEG);

        anchor.setCol1(logoColumn - 1);
        anchor.setRow1(1);
        anchor.setRow2(1);
        anchor.setCol2(logoColumn);
        final Picture pict = drawing.createPicture(anchor, pictureIndex);
        pict.resize();

        Row dateRow = sheet.createRow(5);
        dateRow.createCell(logoColumn - 3).setCellValue(message.get("excel.create.date") +
                                           DateConverterUtil.getAsStringWithSeparator(new Date()));

        // create style for header cells
        CellStyle style = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setFontName("Arial ");
        style.setFillForegroundColor(HSSFColor.SEA_GREEN.index);
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        font.setBoldweight(Font.BOLDWEIGHT_BOLD);
        font.setColor(HSSFColor.WHITE.index);
        style.setFont(font);

        if (details != null)
        {
            detailGenerateExcel(sheet, style, message, details);
        } else if (masters != null)
        {
            masterGenerateExcel(sheet, style, message, masters);
        }

        logger.info("End successfully of method [buildExcelDocument] via userId [" +
                    UserUtil.getCurrentUser().getUserId() + "]");
    }

    private void masterGenerateExcel(Sheet sheet, CellStyle style, Map<String, String> message,
                                     List<PSPReportRegisterMaster> masters)
    {
        // create header row
        Row header = sheet.createRow(8);
        header.createCell(9).setCellValue(message.get("excel.master.date"));
        header.getCell(9).setCellStyle(style);
        header.createCell(8).setCellValue(message.get("excel.master.account.no"));
        header.getCell(8).setCellStyle(style);
        header.createCell(7).setCellValue(message.get("excel.master.client.no"));
        header.getCell(7).setCellStyle(style);
        header.createCell(6).setCellValue(message.get("excel.master.branch"));
        header.getCell(6).setCellStyle(style);
        header.createCell(5).setCellValue(message.get("excel.master.company.name"));
        header.getCell(5).setCellStyle(style);
        header.createCell(4).setCellValue(message.get("excel.master.request.status"));
        header.getCell(4).setCellStyle(style);
        header.createCell(3).setCellValue(message.get("excel.master.request.no"));
        header.getCell(3).setCellStyle(style);
        header.createCell(2).setCellValue(message.get("excel.master.psp.tracking.code"));
        header.getCell(2).setCellStyle(style);
        header.createCell(1).setCellValue(message.get("excel.master.psp.acceptance.code"));
        header.getCell(1).setCellStyle(style);
        header.createCell(0).setCellValue(message.get("excel.master.psp.terminal.code"));
        header.getCell(0).setCellStyle(style);

        int rowCount = 9;

        for (PSPReportRegisterMaster master : masters)
        {
            Row detailRow = sheet.createRow(rowCount++);
            detailRow.createCell(9).setCellValue(master.getRequestDate());
            detailRow.createCell(8).setCellValue(master.getAccountNo());
            detailRow.createCell(7).setCellValue(master.getClientNumber());
            detailRow.createCell(6).setCellValue(master.getBranch());
            detailRow.createCell(5).setCellValue(master.getCompanyName());
            detailRow.createCell(4).setCellValue(master.getRequestStatus());
            detailRow.createCell(3).setCellValue(master.getRequestNumber());
            detailRow.createCell(2).setCellValue(master.getPspTrackingCode());
            detailRow.createCell(1).setCellValue(master.getPspAcceptanceCode());
            detailRow.createCell(0).setCellValue(master.getPspTerminalCode());
        }

    }

    private void detailGenerateExcel(Sheet sheet, CellStyle style, Map<String, String> message,
                                     List<PSPReportRegisterDetail> details)
    {
        // create header row
        Row header = sheet.createRow(8);
        header.createCell(9).setCellValue(message.get("excel.detail.date"));
        header.getCell(9).setCellStyle(style);
        header.createCell(8).setCellValue(message.get("excel.detail.time"));
        header.getCell(8).setCellStyle(style);
        header.createCell(7).setCellValue(message.get("excel.detail.branch"));
        header.getCell(7).setCellStyle(style);
        header.createCell(6).setCellValue(message.get("excel.detail.user"));
        header.getCell(6).setCellStyle(style);
        header.createCell(5).setCellValue(message.get("excel.detail.request.status"));
        header.getCell(5).setCellStyle(style);
        header.createCell(4).setCellValue(message.get("excel.detail.request.number"));
        header.getCell(4).setCellStyle(style);
        header.createCell(3).setCellValue(message.get("excel.detail.tracking.code"));
        header.getCell(3).setCellStyle(style);
        header.createCell(2).setCellValue(message.get("excel.detail.service"));
        header.getCell(2).setCellStyle(style);
        header.createCell(1).setCellValue(message.get("excel.detail.status.type"));
        header.getCell(1).setCellStyle(style);
        header.createCell(0).setCellValue(message.get("excel.detail.description"));
        header.getCell(0).setCellStyle(style);

        int rowCount = 9;

        for (PSPReportRegisterDetail detail : details)
        {
            Row detailRow = sheet.createRow(rowCount++);
            detailRow.createCell(9).setCellValue(detail.getCreateDate());
            detailRow.createCell(8).setCellValue(detail.getCreateTime());
            detailRow.createCell(7).setCellValue(detail.getBranch());
            detailRow.createCell(6).setCellValue(detail.getUserCode());
            detailRow.createCell(5).setCellValue(detail.getRequestStatus());
            detailRow.createCell(4).setCellValue(detail.getRequestNumber());
            detailRow.createCell(3).setCellValue(detail.getPspTrackingCode());
            detailRow.createCell(2).setCellValue(detail.getService());
            detailRow.createCell(1).setCellValue(detail.getStatusType());
            detailRow.createCell(0).setCellValue(detail.getDescription());
        }
    }
}
