package com.karafarin.terminalmanagement.config.jdbc;

import org.apache.tomcat.jdbc.pool.DataSource;
import org.springframework.jdbc.core.JdbcTemplate;

public interface CommonConnectionConfiguration
{
    public static final String DRIVER = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
    public static final String DB_TYPE = "sqlserver";

    public DataSource getDataSource();
    public JdbcTemplate getJdbcTemplate();
}
