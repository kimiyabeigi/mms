package com.karafarin.terminalmanagement.config.jdbc;

import com.karafarin.terminalmanagement.config.jdbc.impl.CommonConnectionConfigurationImpl;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "sql")
public class SqlConnectionConfiguration extends CommonConnectionConfigurationImpl
{
}
