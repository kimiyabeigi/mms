package com.karafarin.terminalmanagement.config.jdbc.impl;

import com.karafarin.terminalmanagement.config.jdbc.CommonConnectionConfiguration;
import org.apache.tomcat.jdbc.pool.DataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.core.JdbcTemplate;

public class CommonConnectionConfigurationImpl implements CommonConnectionConfiguration
{
    public String username;
    public String password;
    public String host;
    public String port;
    public String instanceName;
    public String databaseName;
    public String url;
    public DataSource dataSource = null;
    public JdbcTemplate jdbcTemplate = null;

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getHost()
    {
        return host;
    }

    public void setHost(String host)
    {
        this.host = host;
    }

    public String getPort()
    {
        return port;
    }

    public void setPort(String port)
    {
        this.port = port;
    }

    public String getInstanceName()
    {
        return instanceName;
    }

    public void setInstanceName(String instanceName)
    {
        this.instanceName = instanceName;
    }

    public String getDatabaseName()
    {
        return databaseName;
    }

    public void setDatabaseName(String databaseName)
    {
        this.databaseName = databaseName;
    }

    public String getUrl()
    {
        url = "jdbc:" + DB_TYPE + "://" + getHost() + ":" + getPort() + ";instanceName=" + getInstanceName() +
                ";databaseName=" + getDatabaseName();
        return url;
    }

    @Override
    @Bean(destroyMethod = "close")
    public DataSource getDataSource()
    {
        if (dataSource == null) {
            dataSource = new DataSource();
            dataSource.setUsername(getUsername());
            dataSource.setPassword(getPassword());
            dataSource.setDriverClassName(DRIVER);
            dataSource.setUrl(getUrl());
        }
        return dataSource;
    }

    @Override
    @Bean
    public JdbcTemplate getJdbcTemplate()
    {
        if (jdbcTemplate == null) {
            jdbcTemplate = new JdbcTemplate(getDataSource());
        }
        return jdbcTemplate;
    }
}
