package com.karafarin.terminalmanagement.config;

import com.karafarin.terminalmanagement.model.CurrentUser;
import com.karafarin.terminalmanagement.util.UserUtil;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class AuthorizationInterceptor extends HandlerInterceptorAdapter
{
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception
    {
        if (request.getUserPrincipal() != null && request.getUserPrincipal().getName() != null &&
            !request.getUserPrincipal().getName().equalsIgnoreCase("anonymousUser"))
        {
            CurrentUser user = UserUtil.getCurrentUser();
            if (user != null && user.isFirstLogin())
            {
                if (!request.getRequestURL().toString().contains("/changepassword") &&
                    !request.getRequestURL().toString().contains("/api"))
                {
                    response.sendRedirect("changepassword");
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return true;
            }
        }
        return true;
    }
}
