package com.karafarin.terminalmanagement.config;

import com.karafarin.terminalmanagement.viewresolver.ExcelViewResolver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.http.MediaType;
import org.springframework.web.accept.ContentNegotiationManager;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.*;
import org.springframework.web.servlet.view.ContentNegotiatingViewResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import java.util.ArrayList;
import java.util.List;

@Configuration
public class WebConfiguration extends WebMvcConfigurerAdapter
{
    @Override
    public void addViewControllers(ViewControllerRegistry registry)
    {
        registry.addViewController("/").setViewName("index");
        registry.addViewController("/listcompany").setViewName("listcompany");
        registry.addViewController("/listservice").setViewName("listservice");
        registry.addViewController("/listfactor").setViewName("listfactor");
        registry.addViewController("/listterminal").setViewName("listterminal");
        registry.addViewController("/listcity").setViewName("listcity");
        registry.addViewController("/liststate").setViewName("liststate");
        registry.addViewController("/listguild").setViewName("listguild");
        registry.addViewController("/managecompany").setViewName("managecompany");
        registry.addViewController("/registerterminal").setViewName("registerterminal");
        registry.addViewController("/searchtrace").setViewName("searchtrace");
        registry.addViewController("/traceaccept").setViewName("traceaccept");
        registry.addViewController("/traceregister").setViewName("traceregister");
        registry.addViewController("/moredipeygiri").setViewName("moredipeygiri");
        registry.addViewController("/usermanager").setViewName("usermanager");
        registry.addViewController("/rolemanage").setViewName("rolemanage");
        registry.addViewController("/userrole").setViewName("userrole");
        registry.addViewController("/login").setViewName("login");
        registry.addViewController("/changepassword").setViewName("changepassword");
        registry.addViewController("/changepasswordadmin").setViewName("changepasswordadmin");
        registry.addViewController("/report").setViewName("report");
        registry.addViewController("/report_de").setViewName("report_de");
        registry.setOrder(Ordered.HIGHEST_PRECEDENCE);
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry
                .addResourceHandler("/app-assets/**")
                .addResourceLocations("/app-assets/");
    }

    @Bean
    public InternalResourceViewResolver jspViewResolver() {
        InternalResourceViewResolver resolver = new InternalResourceViewResolver();
        resolver.setPrefix("/WEB-INF/view/");
        resolver.setSuffix(".jsp");
        return resolver;
    }

    @Bean
    public ViewResolver excelViewResolver() {
        return new ExcelViewResolver();
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new AuthorizationInterceptor());

    }
}
