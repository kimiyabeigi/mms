package com.karafarin.terminalmanagement.config.error;

import com.karafarin.terminalmanagement.message.Response;
import com.karafarin.terminalmanagement.message.enums.ResponseStatusEnum;
import com.karafarin.terminalmanagement.model.Messages;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.List;
import java.util.stream.Collectors;

@ControllerAdvice
public class ErrorAdvice
{
    @Autowired
    private Messages messages;

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public @ResponseBody
    Response badRequest(MethodArgumentNotValidException e) {
        Response result = new Response();
        List<ErrorMessage> errorMessages = e.getBindingResult().getAllErrors().stream()
                .map(err -> new ErrorMessage(err))
                .collect(Collectors.toList());
        result.setStatus(ResponseStatusEnum.ERROR);
        result.setDescription(messages.get("system.validate.error"));
        result.setData(errorMessages);
        return result;
    }

}
