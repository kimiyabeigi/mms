package com.karafarin.terminalmanagement.config.error;

import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

public class ErrorMessage
{
    String message, code, field, objectName;

    public ErrorMessage(ObjectError objectError){
        message = objectError.getDefaultMessage();
        code = objectError.getCode();
        objectName = objectError.getObjectName();
        if (objectError instanceof FieldError)
        {
            field = ((FieldError) objectError).getField();
        }
    }

    public String getMessage()
    {
        return message;
    }

    public String getCode()
    {
        return code;
    }

    public String getField()
    {
        return field;
    }

    public String getObjectName()
    {
        return objectName;
    }
}
