package com.karafarin.terminalmanagement.util;

import com.karafarin.terminalmanagement.dao.impl.auth.RoleDaoImpl;
import com.karafarin.terminalmanagement.dao.impl.auth.UserDaoImpl;
import com.karafarin.terminalmanagement.dto.auth.Role;
import com.karafarin.terminalmanagement.dto.auth.User;
import com.karafarin.terminalmanagement.model.CurrentUser;
import com.karafarin.terminalmanagement.model.WorkStation;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.validation.constraints.NotNull;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class UserUtil
{
    private static final Logger logger = LogManager.getLogger(UserUtil.class);
    private static Map<String, User> users = null;
    private static Map<Long, Role> roles = null;
    private static UserDaoImpl userDao;
    private static WebUtil webUtil;
    private static RoleDaoImpl roleDao;
    @Autowired
    private UserDaoImpl autoUserDao;
    @Autowired
    private WebUtil autoWebUtil;
    @Autowired
    private RoleDaoImpl autoRoleDao;

    @PostConstruct
    public void init() {
        UserUtil.userDao = autoUserDao;
        UserUtil.webUtil = autoWebUtil;
        UserUtil.roleDao = autoRoleDao;
    }

    public static Map<String, User> getAllUsers()
    {
        if (users == null)
        {
            try
            {
                List<User> userList = userDao.findAllUsers();
                users = new HashMap<>();
                for (User user: userList)
                {
                    users.put(user.getUserName(), user);
                }
            } catch (SQLException e)
            {
                logger.error("There is an error in method [getAllUsers] for [User]", e);
                e.printStackTrace();
            }
        }

        return users;
    }

    public static Map<Long, Role> getAllRoles()
    {
        if (roles == null)
        {
            try
            {
                List<Role> roleList = roleDao.findAllRoles();
                roles = new HashMap<>();
                for (Role role: roleList)
                {
                    roles.put(role.getRoleId(), role);
                }
            } catch (SQLException e)
            {
                logger.error("There is an error in method [getAllRoles] for [Role]", e);
                e.printStackTrace();
            }
        }

        return roles;
    }

    public static void addUserCache(User user)
    {
        if (users.get(user.getUserName()) == null)
        {
            users.put(user.getUserName(), user);
        }
    }

    public static void editUserCache(User user)
    {
        if (users.get(user.getUserName()) != null)
        {
            users.replace(user.getUserName(), user);
        }
    }

    public static void removeUserCache(User user)
    {
        if (users.get(user.getUserName()) != null)
        {
            users.remove(user.getUserName());
        }
    }

    public static void addRoleCache(Role role)
    {
        if (roles.get(role.getRoleId()) == null)
        {
            roles.put(role.getRoleId(), role);
        }
    }

    public static void removeRoleCache(Long roleId)
    {
        if (roles.get(roleId) != null)
        {
            roles.remove(roleId);
        }
    }

    public static CurrentUser getCurrentUser()
    {
        Map<String, User> users = getAllUsers();
        Map<Long, Role> roles = getAllRoles();
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = users.get(authentication.getName());

        if (user != null)
        {
            CurrentUser result = new CurrentUser();
            Role role = roles.get(user.getRoleId());

            WorkStation workStation = webUtil.getWorkStation();

            result.setUserId(user.getUserId());
            result.setFirstName(user.getFirstName());
            result.setLastName(user.getLastName());
            result.setUserName(user.getUserName());
            result.setPassword(user.getPassword());
            result.setRoleId(user.getRoleId());
            result.setRoleName(role.getName());
            result.setRoleNameEN(role.getNameEN());
            result.setBranchId(user.getBranchId());
            result.setComputerName(workStation.getName());
            result.setIp(workStation.getIp());
            result.setFirstLogin(user.isFirstLogin());

            return result;
        }
        else
        {
            return null;
        }
    }

    public static String getPasswordEncoded(String password)
    {
        String result;
        ShaPasswordEncoder encoder = new ShaPasswordEncoder();
        result = encoder.encodePassword(password, null);
        return result;
    }

    public static boolean validateMelliCode(String melliCode) {

        String[] identicalDigits = {"0000000000", "1111111111", "2222222222", "3333333333", "4444444444", "5555555555",
                                    "6666666666", "7777777777", "8888888888", "9999999999"};

        if (StringUtils.isEmpty(melliCode)) {
            return false; // Melli Code is empty
        } else if (melliCode.length() != 10) {
            return false; // Melli Code is less or more than 10 digits
        } else if (Arrays.asList(identicalDigits).contains(melliCode)) {
            return false; // Fake Melli Code
        } else {
            int sum = 0;

            for (int i = 0; i < 9; i++) {
                sum += Character.getNumericValue(melliCode.charAt(i)) * (10 - i);
            }

            int lastDigit;
            int divideRemaining = sum % 11;

            if (divideRemaining < 2) {
                lastDigit = divideRemaining;
            } else {
                lastDigit = 11 - (divideRemaining);
            }

            if (Character.getNumericValue(melliCode.charAt(9)) == lastDigit) {
                return true;
            } else {
                return false; // Invalid MelliCode
            }
        }
    }

    public static boolean checkLegalNationalCode(String nationalCode) {

        if (nationalCode.length() < 11 || nationalCode.equalsIgnoreCase("0"))
            return false;

        if (Integer.parseInt(nationalCode.substring(3, 9)) == 0)
            return false;

        int c = Integer.parseInt(nationalCode.substring(10, 11));
        int d = Integer.parseInt(nationalCode.substring(9, 10)) + 2;
        int[] z = new int[] { 29, 27, 23, 19, 17 };
        int s = 0;

        for (byte i = 0; i < 10; i++)
            s += (d + Integer.parseInt(nationalCode.substring(i, i + 1))) * z[i % 5];

        s = s % 11;

        if (s == 10)
            s = 0;

        return (c == s);
    }

    public static boolean isForeignCodeValid(String code)
    {
        String digitsOnly = getDigitsOnly(code);
        int sum = 0;
        int digit = 0;
        int addend = 0;
        boolean timesTwo = false;

        for (int i = digitsOnly.length() - 1; i >= 0; i--)
        {
            digit = Integer.parseInt(digitsOnly.substring(i, i + 1));
            if (timesTwo)
            {
                addend = digit * 2;
                if (addend > 9)
                { addend -= 9; }
            }
            else
            { addend = digit; }
            sum += addend;
            timesTwo = !timesTwo;
        }

        int modulus = sum % 10;
        return modulus == 0;
    }

    private static String getDigitsOnly(String s)
    {
        StringBuffer digitsOnly = new StringBuffer();
        char c;
        for (int i = 0; i < s.length(); i++)
        {
            c = s.charAt(i);
            if (Character.isDigit(c))
            { digitsOnly.append(c); }
        }
        return digitsOnly.toString();
    }

}
