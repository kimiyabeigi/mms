package com.karafarin.terminalmanagement.util.date;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import java.util.Calendar;
import java.util.Date;

public class CalendarDateConverter implements Converter
{
	private static final String SEPERATOR = "/";

	public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String string)
			throws ConverterException
	{
		if ((string == null) || (string.length() == 0))
			return null;

		FacesMessage msg = new FacesMessage("Date or it's Pattern is not valid", "Date or it's Pattern is not valid");
		int firstSeperatorIndex = string.indexOf(SEPERATOR);
		if (firstSeperatorIndex < 2)
			throw new ConverterException(msg);
		int secondSeperatorIndex = string.indexOf(SEPERATOR, firstSeperatorIndex + 1);
		if (secondSeperatorIndex < 4)
			throw new ConverterException(msg);
		try
		{
			int year = Integer.parseInt(string.substring(0, firstSeperatorIndex));
			int month = Integer.parseInt(string.substring(firstSeperatorIndex + 1, secondSeperatorIndex));
			int day = Integer.parseInt(string.substring(secondSeperatorIndex + 1));

			if (year < 1500)
			{
				if (year < 1300)
					throw new Exception();

				if (month > 12 || month < 1)
					throw new Exception();

				if ((month > 6 && day > 30) || (month < 7 && day > 31))
					throw new Exception();
				return convertSolarDate(year, month, day);
			}
			else
			{
				Calendar cal = Calendar.getInstance();
				if (year > 2500 || year < 1800)
					throw new Exception();

				if (month > 12 || month < 1)
					throw new Exception();

				cal.set(year, month - 1, 32);
				int maxAllowdedDaysInMonth = 32 - cal.get(Calendar.DAY_OF_MONTH);

				if (day < 1 || day > maxAllowdedDaysInMonth)
					throw new Exception();

				cal.set(year, month - 1, day);
				return cal.getTime();
			}
		}
		catch (Exception e)
		{
			throw new ConverterException(msg);
		}
	}

	public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object object)
			throws ConverterException
	{
		if (object == null)
			return "";

		try
		{
			Date date = (Date) object;
			String lang = FacesContext.getCurrentInstance().getViewRoot().getLocale().getLanguage();
			Calendar cal = Calendar.getInstance();
			cal.setTime(date);
			int year = cal.get(Calendar.YEAR);
			int month = cal.get(Calendar.MONTH) + 1;
			int day = cal.get(Calendar.DATE);

			Calendar cale = Calendar.getInstance();
			cale.setTime(date);

			String yearStr = DateConverter.create2DigitNumber(year);
			String monthStr = DateConverter.create2DigitNumber(month);
			String dayStr = DateConverter.create2DigitNumber(day);

			if (lang.equalsIgnoreCase("fa"))
			{
				DateConverterUtil util = new DateConverterUtil();
				util.setGregorianDate(year, month, day);
				return DateConverter.create2DigitNumber(util.getIranianYear()) + SEPERATOR +
				       DateConverter.create2DigitNumber(util.getIranianMonth()) + SEPERATOR +
				       DateConverter.create2DigitNumber(util.getIranianDay());
			}
			else
			{
				return yearStr + SEPERATOR + monthStr + SEPERATOR + dayStr;
			}
		}
		catch (Exception e)
		{
			FacesMessage msg = new FacesMessage("invalid objet as date", "invalid objet as date");
			throw new ConverterException(msg);
		}
	}

	private Date convertSolarDate(int sYear, int sMonth, int sDay)
	{
		DateConverterUtil util = new DateConverterUtil();
		util.setIranianDate(sYear, sMonth, sDay);
		Calendar cal = Calendar.getInstance();
		cal.set(util.getGregorianYear(), util.getGregorianMonth() - 1, util.getGregorianDay());
		return cal.getTime();
	}
}
