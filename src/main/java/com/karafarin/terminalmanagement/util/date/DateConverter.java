package com.karafarin.terminalmanagement.util.date;


import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.convert.ConverterException;
import java.util.Calendar;
import java.util.Date;

public class DateConverter
{

	private static final String SEPERATOR = "/";

	public String getAsString(String lang, UIComponent uiComponent, Object object)
			throws ConverterException
	{
		if (object == null)
			return "";

		try
		{
			Date date = (Date) object;
			Calendar cal = Calendar.getInstance();
			cal.setTime(date);
			int year = cal.get(Calendar.YEAR);
			int month = cal.get(Calendar.MONTH) + 1;
			int day = cal.get(Calendar.DATE);

			Calendar cale = Calendar.getInstance();
			cale.setTime(date);
			int hour = cale.get(Calendar.HOUR_OF_DAY);
			int minute = cale.get(Calendar.MINUTE);

			String yearStr = create2DigitNumber(year);
			String monthStr = create2DigitNumber(month);
			String dayStr = create2DigitNumber(day);
			String hourStr = create2DigitNumber(hour);
			String minuteStr = create2DigitNumber(minute);

			if (lang.equalsIgnoreCase("fa"))
			{
				DateConverterUtil util = new DateConverterUtil();
				util.setGregorianDate(year, month, day);
				return util.getIranianYear() + SEPERATOR + create2DigitNumber(util.getIranianMonth()) + SEPERATOR +
				       create2DigitNumber(util.getIranianDay()) + " "
				       + hourStr + ":" + minuteStr;
			}
			else
			{
				return yearStr + SEPERATOR + monthStr + SEPERATOR + dayStr
				       + " " + hourStr + ":" + minuteStr;
			}
		}
		catch (Exception e)
		{
			FacesMessage msg = new FacesMessage("invalid objet as date", "invalid objet as date");
			throw new ConverterException(msg);
		}
	}

	private Date convertSolarDate(int sYear, int sMonth, int sDay)
	{
		DateConverterUtil util = new DateConverterUtil();
		util.setIranianDate(sYear, sMonth, sDay);
		Calendar cal = Calendar.getInstance();
		cal.set(util.getGregorianYear(), util.getGregorianMonth() - 1, util.getGregorianDay());
		return cal.getTime();
	}

	public static String create2DigitNumber(int number)
	{
		if (number > 9)
			return String.valueOf(number);
		return "0" + number;
	}
}
