package com.karafarin.terminalmanagement.util;

import com.karafarin.terminalmanagement.config.ChannelConfiguration;
import com.karafarin.terminalmanagement.model.core.ChannelRequest;
import com.karafarin.terminalmanagement.model.core.ChannelResponse;
import com.karafarin.terminalmanagement.model.Messages;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.w3c.dom.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

@Component
public class ChannelUtil
{
    @Autowired
    private Messages messages;
    @Autowired
    private ChannelConfiguration channelConfiguration;

    private static final Logger logger = LogManager.getLogger(ChannelUtil.class);

    public ChannelResponse sendRequest(ChannelRequest channelRequest)
    {
        Socket socket = null;
        try
        {
            String requestCommand = makeChannelRequestCommand(channelRequest);
            logger.info("Sending cmd message to channel : " + "\n" + requestCommand);

            socket = new Socket(channelConfiguration.getIp(), channelConfiguration.getPort());
            socket.getOutputStream().write(requestCommand.getBytes(channelConfiguration.getCharset()));
            socket.getOutputStream().flush();
            BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream(),
                    channelConfiguration.getCharset()));
            String response = new String(br.readLine());
            socket.close();

            logger.info("Response message from channel : " + "\n" + response);

            return parseChannelResponse(response, channelRequest);
        } catch (IOException e)
        {
            logger.error("There is an error in send/receive message to channel. Maybe channel is down. ", e);
        }

        return null;
    }

    private String makeChannelRequestCommand(ChannelRequest channelRequest)
    {
        StringBuilder command = new StringBuilder();

        command.append("<request>")
                .append("<cmd>").append(channelRequest.getCmd()).append("</cmd>")
                .append("<appname>").append(channelRequest.getAppName()).append("</appname>")
                .append("<jobid>").append(channelRequest.getJobId()).append("</jobid>")
                .append("<msgid>").append(channelRequest.getMsgId()).append("</msgid>")
                .append("<branchid>").append(channelRequest.getBranchCode()).append("</branchid>")
                .append("<userid>").append(channelRequest.getUserId()).append("</userid>")
                .append("<content>").append(channelRequest.getContent()).append("</content>")
                .append("</request>")
                .append("\n");

        return command.toString();
    }

    private ChannelResponse parseChannelResponse(String response, ChannelRequest channelRequest)
    {
        Element element = null;
        ChannelResponse result = new ChannelResponse();
        try
        {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            StringBuilder xmlStringBuilder = new StringBuilder();
            xmlStringBuilder.append(response);
            ByteArrayInputStream input = new ByteArrayInputStream(xmlStringBuilder.toString().getBytes("UTF-8"));
            Document doc = dBuilder.parse(input);
            doc.getDocumentElement().normalize();
            NodeList nList = doc.getElementsByTagName("response");
            Node nNode = nList.item(0);
            element = (Element) nNode;

            result.setResult(element.getElementsByTagName("result").item(0).getTextContent());
            result.setCmd(element.getElementsByTagName("cmd").item(0).getTextContent());
            result.setContent(element.getElementsByTagName("content").item(0));
            result.setDesc(element.getElementsByTagName("desc").item(0).getTextContent());
            result.setfDesc(element.getElementsByTagName("fdesc").item(0).getTextContent());
            result.setErrorNo(Integer.valueOf(element.getElementsByTagName("errno").item(0).getTextContent()));
            result.setJobId(element.getElementsByTagName("jobid").item(0).getTextContent());
            result.setMsgId(element.getElementsByTagName("msgid").item(0).getTextContent());
            if (result.getErrorNo() != 0)
                result.setErrorMessage(result.getfDesc());
            return result;

        } catch (Exception e)
        {
            e.printStackTrace();
            logger.error("Exception in method [parseChannelResponse] ", e);
            return null;
        }
    }

}
