package com.karafarin.terminalmanagement;

import com.karafarin.terminalmanagement.dao.impl.base.BaseTerminalDetailDaoImpl;
import com.karafarin.terminalmanagement.dao.impl.setting.CompanySettingDaoImpl;
import com.karafarin.terminalmanagement.dao.impl.setting.CompanySettingTerminalDaoImpl;
import com.karafarin.terminalmanagement.dto.psp.PSPClient;
import com.karafarin.terminalmanagement.model.CurrentUser;
import com.karafarin.terminalmanagement.model.psp.RegisterRequest;
import com.karafarin.terminalmanagement.service.impl.psp.ServiceRegisterPSPImpl;
import com.karafarin.terminalmanagement.service.security.PermissionService;
import com.karafarin.terminalmanagement.util.UserUtil;
import com.karafarin.terminalmanagement.util.date.DateConverterUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class CompanyDaoImplTest
{
    @Autowired
    private CompanySettingDaoImpl companyDao;
    @Autowired
    private BaseTerminalDetailDaoImpl detailDao;
    @Autowired
    private ServiceRegisterPSPImpl serviceRegisterPSP;
    @Autowired
    private PermissionService permissionService;
    @Autowired
    private CompanySettingTerminalDaoImpl companySettingTerminalDao;

    @Test
    public void testIsThisDateValid() {

/*
        assertTrue(DateConverterUtil.isValidDateFormat("1340/06/12"));
        assertFalse(DateConverterUtil.isValidDateFormat("40/06/12"));
*/
    }

    @Test
    public void testConnectionCode(){
/*
        try
        {
            String connectionCode = companySettingTerminalDao.findConnectionCode(3L, 1L, 5L);
            assertEquals(connectionCode, "12");
        } catch (SQLException e)
        {
            e.printStackTrace();
        }
*/
    }

    @Test
    public void testFindAll(){
/*
        CurrentUser currentUser = new CurrentUser();
        currentUser.setIp("127.0.0.1");
        currentUser.setComputerName("ws2572");
        currentUser.setBranchId(1L);
        currentUser.setUserId(3L);
        currentUser.setRoleId(1L);

        boolean b = permissionService.hasPermission("/api/base/city1", "GET1");
        assertEquals(false, b);
*/
    }
}
